% % % % Irina Grigorescu
% % % % Takes nii files and plots them nicely

% Prerequisites
addpath(genpath('~/Tools/MatlabStuff/'))

%%

indices = [10 11 500]; %[10 11 12 13 20 157  500];
dataAbs = zeros(64,64,1,size(indices,2));
idx = 1;

for i = indices
    filename = ['~/simdir/outImage/image_epib_phantom_64x64_Np501_', ...
             num2str(i), '_abs.nii.gz'];
        
    iniData = load_nii(filename, ...
              [], [], [], [], [], 0.5);
	
	dataAbs(:,:,1,idx) = rot90(iniData.img,-2);
    idx = idx+1;
end   

maxAbs = max(max(max(max(dataAbs))));
dataAbs = dataAbs./maxAbs;

% Plot a series of images
figure
xImage = [-0.5 0.5; -0.5 0.5];   %# The x data for the image corners
yImage = [ 0   0  ;  0   0  ];   %# The y data for the image corners
zImage = [0.5 0.5; -0.5 -0.5];   %# The z data for the image corners
for i = 1:size(indices,2)
    view(42,6);
    temp = squeeze(dataAbs(:,:,:,i));
    surf(xImage,yImage,zImage,...    %# Plot the surface
         'CData', temp, ...
         'FaceColor', 'texturemap'); hold on
    if i == size(indices,2)-1
        xImage = xImage + 1.2;
    else
        xImage = xImage + 1.05;
    end
end
% caxis([0 maxAbs])
colormap gray
axis equal
axis off

%%
% Plot them one next to the other one
figure
for i = 1:size(indices,2)
   subplot(1, size(indices,2),i)
   imagesc(squeeze(dataAbs(:,:,1,i)))
   colormap gray
   axis equal
   axis off
end

