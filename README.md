Magnetic Resonance Fingerprinting
============================

This software is developed by Irina Grigorescu 
------------------------------------------------------

Email: <irina.grigorescu.15@ucl.ac.uk>

This work is based on the paper by [Ma D et al, Magnetic resonance fingerprinting, Nature 2013](http://www.nature.com/nature/journal/v495/n7440/full/nature11971.html)

### Some notes about the structure of the code
```bash
.
├── plots
└── src
    ├── algs
    │   ├── perlin
    │   └── preprocess
    ├── data
    ├── exchanged       # Contains files exchanged with Gary
    ├── helpers 
    │   └── misc        # Contains files not written by me
    └── test
        ├── 1_test1block
        ├── 2_test2blocks
        └── 3_testNblocks
```

### Unanswered Questions
- [x] Replicate the dictionary generation as described in the paper
- [ ] Test the vectorized version more thoroughly (look at the SS scripts)
- [ ] Investigate the concept of spin-coherence pathways

### Notes to self:
- Converting images to videos (More info [here](http://hamelot.io/visualization/using-ffmpeg-to-convert-a-set-of-images-into-a-video/)):
```bash
ffmpeg -framerate 24 -i image_%d.png output.mp4
```


