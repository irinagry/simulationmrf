#!/bin/bash

rm MRFReport.aux MRFReport.log
pdflatex MRFReport.tex
pdflatex MRFReport.tex
pdflatex MRFReport.tex
rm MRFReport.aux MRFReport.log
rm chapters/*.aux chapters/*.log
rm chapters/pseudocodeSnippets/*.aux chapters/pseudocodeSnippets/*.log
