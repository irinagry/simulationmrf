%%%%%%%%%%%
%%% SECTION 4 - Voxel 1 in POSSUM
\newpage

% % % % Voxel 1 algorithm from POSSUM
\begin{algorithm}
\setstretch{1.35}
    \LinesNumberedHidden
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}

    \nonl \underline{function $voxel1$} (firstVoxelFlag, r0x, r0y, r0z, xdim, ydim, zdim, tissueProperties, den, eventMatrix, nreadp, table\_slcprof, dslcp, dslcp\_first, Nslc, RFtrans\_inho, B0\_inho, dB0dx\_inho, dB0dy\_inho, dB0dz\_inho, outputname, sreal, simag, save\_kcoordFlag, testPrintFlag, nospeedupFlag)\;
    \Input{
        \textbf{firstVoxelFlag} internal voxel index number (used to check if it is the first voxel) \\
        \textbf{r0x, r0y, r0z} coordinates at the centre of the voxel (in m) \\
        \textbf{xdim, ydim, zdim} the dimensions of the voxel in the x, y and z directions (in m) \\
        \textbf{tissueProperties} the tissue properties for the current voxel ($T_1, T_2, \rho, \chi$) (in s, s, -, ppm) \\
        \textbf{den} variable that stores $den = p(r_0) \times L_x \times L_y \times L_z \times \textit{ RFrec\_inho }$ where \\
            \quad $p(r_0)$ is the proportion of the current tissue type present in the voxel \\
            \quad $L_x, L_y, L_z$ are the x, y, z dimensions of the object voxel and \\
            \quad $RFrec\_inho$ is the inhomogeneity in the receive coil (values in $[0,1]$) \\
        \textbf{eventMatrix} the pulse sequence event matrix holding values for the RF pulses and gradients at \\
            \quad each time step defined in the sequence \\
        \textbf{nreadp} number of readout points \\
        \textbf{table\_slcprof} the array specifying the slice profile for the RF excitation \\
            \quad (it is the second column of the slcprof file, which represents the amplitude in \% of the slice profile) \\
        \textbf{dslcp} step size (df) for slice profile \\
            \quad (calculated from the first column of the slcprof file, which represents the frequency in \% \\
            \quad of the slice profile) \\
        \textbf{dslcp\_first} the first element in the slice profile \\
            \quad (it is the first element in the second column of the slcprof file, which represents the amplitude in \% \\
            \quad of the slice profile) \\
        \textbf{Nslc} the number of elements in the slice profile \\
        \textbf{RFtrans\_inho} the transmit coil inhomogeneities (this will affect the RF pulse's flip angle) \\
        \textbf{B0\_inho} the inhomogeneities induced in the static $B_0$ field due to susceptibility or chemical shift (in T) \\
        \textbf{dB0dx\_inho, dB0dy\_inho, dB0dz\_inho} values of gradients across the voxel of the $B_0$ inhomogeneity \\
            \quad field (in T/m) \\
        \textbf{outputname} name of file to store the output signal in \\
        \textbf{sreal, simag} the real and the imaginary channels of the signal output \\
        \textbf{save\_kcoordFlag} a flag used to see save the coordinates of the     k-space in addition to saving the signal \\
            \quad mainly used to save the (distorted) coordinates in the k-space as: \\
            \quad \quad coord(1, readstep) = $\frac{\gamma}{2 \pi}  (G_x t + \frac{\partial \Delta B0}{\partial x} t)$ \\
            \quad \quad coord(2, readstep) = $\frac{\gamma}{2 \pi}  (G_y t + \frac{\partial \Delta B0}{\partial y} t)$ \\
            \quad \quad coord(3, readstep) = $\frac{\gamma}{2 \pi}  (G_z t + \frac{\partial \Delta B0}{\partial z} t)$ \\
        \textbf{testPrintFlag} a flag used for the print outs throughout the code \\
        \textbf{nospeedupFlag} a flag used for speedup. If set to 0 then speedup is assumed which means it will not do \\
            \quad readout if no RF pulse was previously encountered
    }
    \Output{\textbf{sreal, simag} the real and the imaginary channels of the signal output }
    
\end{algorithm}   
\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Continuation of the algorithm  
\begin{algorithm}
\setstretch{1.35}
    \LinesNumberedHidden
    
    %% INITIALISATION (SETUP) LOCAL
    INITIALIZATION of local variables;

    %% First voxel?    
    \If{first voxel in simulation}{
        %% INITIALISATION (SETUP) GLOBAL
        ALLOCATE memory for global variables; \\
        POPULATE global tables for \texttt{sinc/sin/cos};
    }
        
    %% MAIN LOOP
    \ForEach{\texttt{event} $\in$ pulse sequence event matrix } {
        READ sequence variables at current \texttt{idx} \;
        \eIf{first voxel in simulation}{
            POPULATE global tables for \texttt{gradients};
        }{
            RETRIEVE global tables for \texttt{gradients};
        }
        % calculate the cumulative variables
        CALCULATE cumulative integrals (call \texttt{i1(G2, G1, t2, t1)});\\
        CALCULATE \texttt{timeSinceLastRF}; \\
        CALCULATE \texttt{phaseAccumulated}:  $\phi \gets \gamma G_x \, t \, r_{0x} + \gamma G_y \, t \, r_{0y} +  \gamma G_z \, t \, r_{0z} + \gamma \Delta B t + \gamma \chi t$\\
        % RF PULSE
        \If{\texttt{event} is RF PULSE}{
            CALCULATE the real RF pulse flip angle; \\
            \If{\texttt{real FA} is above 0}{
                DO RF pulse: \texttt{doRFPulse()};\\
                CALCULATE magnitude of transverse components of the magnetic moment vector \\
                    \nonl \quad $m_{00} \gets \sqrt{m_x^2 + m_y^2}$ \\
                CALCULATE cumulative gradient integrals (up until RF pulse); \\
            } % end real FA
        }     % end RF pulse
        


        \If{\texttt{event} is READOUT}{
            ROstep $\gets$ ROstep + 1; \\
            \If{first voxel \texttt{AND} save kspace coordinates flag set}{
                CALCULATE k-space coordinates at the current readout step;
            }
            DO read-out: signalPartial $\gets$ \texttt{doReadOut()};\\
            CALCULATE signal \\
            \nonl \quad $s_{real} \gets s_{real} +$ signalPartial calculatedCOS ;\\
            \nonl \quad $s_{imag} \gets s_{imag} +$ signalPartial calculatedSIN ;\\
        }
    
    }
\end{algorithm}   
\clearpage

\begin{algorithm}
    \setstretch{1.35}
    \LinesNumberedHidden
    \nonl \underline{function $doRFPulse$} \\
    CALCULATE dephasing across voxel on the x, y and z directions; \\
    PERFORM dephasing across voxel on the magnetic moment vector transverse components; \\
    \nonl \quad $m_x \gets m_x \, sinc[\frac{\gamma}{2 \pi} L_x (\int_{t_{RF}}^{tp} G_x + \frac{\partial \Delta B_0}{\partial x} dt)] \, sinc[\frac{\gamma}{2 \pi} L_y (\int_{t_{RF}}^{tp} G_y + \frac{\partial \Delta B_0}{\partial y} dt)] \, sinc[\frac{\gamma}{2 \pi} L_z (\int_{t_{RF}}^{tp} G_z + \frac{\partial \Delta B_0}{\partial z} dt)] $ \\
    \nonl \quad $m_y \gets m_y \, sinc[\frac{\gamma}{2 \pi} L_x (\int_{t_{RF}}^{tp} G_x + \frac{\partial \Delta B_0}{\partial x} dt)] \, sinc[\frac{\gamma}{2 \pi} L_y (\int_{t_{RF}}^{tp} G_y + \frac{\partial \Delta B_0}{\partial y} dt)] \, sinc[\frac{\gamma}{2 \pi} L_z (\int_{t_{RF}}^{tp} G_z + \frac{\partial \Delta B_0}{\partial z} dt)] $ \\
    PERFORM the RF pulse as an instantaneous rotation through the real flip angle; \\
        \nonl \quad $\bm{M} \gets Rot_{+x}(\alpha) \, \bm{M}$ 
    \caption{Function that does RF pulse}
\end{algorithm}

\begin{algorithm}
    \setstretch{1.35}
    \LinesNumberedHidden
    \nonl \underline{function $doReadOut$} \\
    CALCULATE dephasing across voxel on the x, y and z directions; \\
    \eIf{\texttt{phaseAccumulated} ($\phi$) is in the SINC table domain}{
        RETRIEVE values of sinc functions form the global tables; \\
        CALCULATE part of the final signal\\
            \nonl \quad signalPartial $\gets$ $m_{00} \, e^{- t / T_2 + actint} \, table\_sinc(\phi_x) \, table\_sinc(\phi_y) \, table\_sinc(\phi_z)$ \\
            \nonl \quad ($\phi_r$ is the phase accumulated accross the voxel in the r direction)
    }{
        CALCULATE part of the final signal\\
            \nonl \quad signalPartial $\gets$ $m_{00} \, e^{- t / T_2 + actint} \, sinc(\phi_x) \, sinc(\phi_y) \, sinc(\phi_z)$ \\
            \nonl \quad ($\phi_r$ is the phase accumulated accross the voxel in the r direction)
    }
    BRING accumulated phase in the $[0, 2 \pi]$ domain; \\
    CALCULATE cosines and sines based on the global tables;\\
    \caption{Function that does Read Out}
\end{algorithm}