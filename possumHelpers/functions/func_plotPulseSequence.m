function func_plotPulseSequence(pulseSequence, figHand, titleOfPlot)
% % % % IRINA GRIGORESCU
% % % % This function plots a possum pulse sequence

[N, M] = size(pulseSequence);

titles = {'time (ms)', ...
          'RF angle (deg)', ...
          'RF freq bw (kHz)', ...
          'RF centre freq (kHz)', ...
          'RO', ...
          'Gx (mT/m)', ...
          'Gy (mT/m)', ...
          'Gz (mT/m)'};

startIdx =   1;
endIdx   =   N; %200;

figure(figHand)

% % 1. ADC
subplot(4,2,1)
scatter(pulseSequence(startIdx:endIdx, 1).*1e+03, ... % 1 is for timepoints
        pulseSequence(startIdx:endIdx, 5), 'r.');           % 5 is ADC
xlabel(titles{1}); ylabel(titles{5});
title(titleOfPlot)
grid on

% % 2.1 RF angle
subplot(4,2,3)
plot(pulseSequence(startIdx:endIdx, 1).*1e+03, ... % 1 is for timepoints
     rad2deg(pulseSequence(startIdx:endIdx, 2)), 'b');% 2 is for RF angle
xlabel(titles{1}); ylabel(titles{2});
grid on

% % 2.2 RF freq BW
subplot(4,2,5)
plot(pulseSequence(startIdx:endIdx, 1).*1e+03, ...   % 1 is for timepoints
     pulseSequence(startIdx:endIdx, 3)./1e+03, 'b'); % 3 is for RF BW
xlabel(titles{1}); ylabel(titles{3});
grid on

% % 2.3 RF centre freq
subplot(4,2,7)
plot(pulseSequence(startIdx:endIdx, 1).*1e+03, ...   % 1 is for timepoints
     pulseSequence(startIdx:endIdx, 4)./1e+03, 'b'); % 4 is for centre freq
xlabel(titles{1}); ylabel(titles{4});
grid on

% % 3 Gx
subplot(4,2,2)
plot(pulseSequence(startIdx:endIdx, 1).*1e+03, ... % 1 is for timepoints
     pulseSequence(startIdx:endIdx, 6).*1e+03);    % 6 is for Gx
xlabel(titles{1}); ylabel(titles{6});
grid on

% % 4 Gy
subplot(4,2,4)
plot(pulseSequence(startIdx:endIdx, 1).*1e+03, ... % 1 is for timepoints
     pulseSequence(startIdx:endIdx, 7).*1e+03);    % 7 is for Gy
xlabel(titles{1}); ylabel(titles{7});
grid on

% % 5 Gz
subplot(4,2,6)
plot(pulseSequence(startIdx:endIdx, 1).*1e+03, ... % 1 is for timepoints
     pulseSequence(startIdx:endIdx, 8).*1e+03);    % 8 is for Gz
xlabel(titles{1}); ylabel(titles{8});
grid on

% Last subplot is made up of all gradients put together
subplot(4,2,8)
plot(pulseSequence(startIdx:endIdx,1).*1e+03, ...
     pulseSequence(startIdx:endIdx, 6).*1e+03)
hold on
plot(pulseSequence(startIdx:endIdx,1).*1e+03, ...
     pulseSequence(startIdx:endIdx, 7).*1e+03)
hold on
plot(pulseSequence(startIdx:endIdx,1).*1e+03, ...
     pulseSequence(startIdx:endIdx, 8).*1e+03)
hold on
xlabel('time (ms)')
ylabel('G (mT/m)')
legend('G_x', 'G_y', 'G_z')


end