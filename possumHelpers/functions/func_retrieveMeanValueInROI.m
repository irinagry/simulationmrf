function [ meanValuesReal , ...
           meanValuesSimu , ...
            stdValuesSimu ] = ...
           func_retrieveMeanValueInROI(tissueMapReal, tissueMapSimu, ...
                                     centres, radii, toPlot)
% % 
% % This function retrieves the mean value in ROI
% % 
% % INPUT:
% % 
% % OUTPUT:
% % 
% % Author: Irina Grigorescu, irina.grigorescu.15@ucl.ac.uk

nROIs = size(centres, 1);

meanValuesReal = zeros(nROIs,1);
meanValuesSimu = zeros(nROIs,1);
stdValuesSimu  = zeros(nROIs,1);

if toPlot == 1
    figure
end

for idxROI = 1:nROIs
    % Create mask for ROI
    maskROI = createCirclesMask(tissueMapReal, ...
                                centres(idxROI,:), radii(idxROI));
    % Calculate mean of values within mask
    meanValuesReal(idxROI) = sum(sum(tissueMapReal.*maskROI)) ./ ...
                             sum(sum(maskROI));       
    meanValuesSimu(idxROI) = sum(sum(tissueMapSimu.*maskROI)) ./ ...
                             sum(sum(maskROI));       
	temp = tissueMapSimu.*maskROI;
    temp = temp(:);
	stdValuesSimu(idxROI)  = std(temp);
        
    maskROI = maskROI+0.1;
    if toPlot == 1
        subplot(1,2,1)
        imagesc(tissueMapReal.*maskROI)
        title(num2str(meanValuesReal(idxROI)))
        axis off; axis equal, axis square; 
        colormap hot; colorbar
        caxis([0 370])

        subplot(1,2,2)
        imagesc(tissueMapSimu.*maskROI)
        title(num2str(meanValuesSimu(idxROI)))
        axis off; axis equal, axis square; 
        colormap hot; colorbar
        caxis([0 370])

        pause 
    end
end