function [realT1, realT2, maskBigCircle, maskHollow, maskOnlyCircles] = ...
          func_createRealT1T2Maps(myPhantomT1Values, myPhantomT2Values, ...
                                 Nx, Ny, centres, radii)
% % 
% % This function creates the real T1 and T2 maps for a given simulation
% % 
% % INPUT:
% % 
% % OUTPUT:
% % 
% % Author: Irina Grigorescu, irina.grigorescu.15@ucl.ac.uk


% To make a hollow circle of zeros
radiiHollow = radii + 0.5; %1; %0.5;

% All small circles
maskAllCircles = createCirclesMask([Nx,Ny], ...
                                   centres(1:end-1,:), radii(1:end-1));
% The big circle
maskBigCircle  = createCirclesMask([Nx,Ny], ...
                                   centres(end,:), radii(end));
% The no value circles around the filled value circles
maskOnlyCircles = createCirclesMask([Nx,Ny], ...
                               centres(1:end-1,:), radiiHollow(1:end-1));
maskHollow = -(maskOnlyCircles-maskAllCircles)+1;

% The big circle without the small circles
masksOfCircles = maskBigCircle - maskAllCircles;
realT1     = (maskBigCircle - maskAllCircles).*myPhantomT1Values(1);
realT2     = (maskBigCircle - maskAllCircles).*myPhantomT2Values(1);

idx = 1:12;
for i = 2 : 13
    masksOfCircles(:,:,i) = createCirclesMask([Nx,Ny], ...
                                   centres(idx(i-1),:), radii(idx(i-1)));
                               
    % Save T1 and T2 relaxation parameters from the file
    realT1 = realT1 + ...
      createCirclesMask([Nx,Ny], centres(idx(i-1),:), radii(idx(i-1))) .* ...
                                               myPhantomT1Values(i);
    realT2 = realT2 + ...
      createCirclesMask([Nx,Ny], centres(idx(i-1),:), radii(idx(i-1))) .* ...
                                               myPhantomT2Values(i);
end 


% % Plot masks
% figure
% 
% subplot(1,2,1)
% imagesc(realT1)
% title('Real T_1')
% axis off; axis equal, axis square; 
% colormap hot; colorbar
% caxis([0 max(myPhantomT1Values)])
% 
% subplot(1,2,2)
% imagesc(realT2)
% title('Real T_2')
% axis off; axis equal, axis square; 
% colormap hot; colorbar
% caxis([0 max(myPhantomT2Values)+100])

end