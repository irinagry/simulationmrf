% % % % IRINA GRIGORESCU
% % % % Date created: 31-08-2017
% % % % Date updated: 31-08-2017
% % % %
% % % % All-in-one script that compares simulations from 
% % % % POSSUM images, with my alg and EPG dictionary generation
% % % % 

clear all;
close all;
clc

% Add paths:
run('runPathsForPOSSUMandEPG');

% % % % SOME VARIABLES
NPulses = 501; Nx = 64; Ny = 64;
toPlotQualitative = 0;


%%
% % % % % % % % % % % LOAD POSSUM IMAGES
seqName  = 'epib';
imgSize  = [num2str(Nx), 'x', num2str(Ny)];
npulses  = ['Np', num2str(NPulses)];
objName  = 'phantom';

% % % % Load Files
imagesP        = zeros(Nx,Nx,2*NPulses);
imagesPComplex = zeros(Nx,Nx,  NPulses);

% % % % For each TR block:
for i = 1:NPulses
    filenameImg = ['~/simdir/outKspace/', ...          % folder
                   'kspace_', ...                      % core name
                    seqName, '_', ...                 % sequence
                    objName, '_', ...                 % objName
                    imgSize, '_', ...                 % size of image
                    npulses, '_', ...                 % number of pulses
                    num2str(i)];                      % image number
                
	% Load in the real and the imaginary channels
    imageNIIreal = load_nii([filenameImg, '_real.nii.gz'], ...
                        [], [], [], [], [], 0.5);
    imageNIIimag = load_nii([filenameImg, '_imag.nii.gz'], ...
        [], [], [], [], [], 0.5);
    
    % Store real and imaginary channels
    % in a matrix like this:
    % [real1 real2 ... realNP imag1 imag2 ... imagNP]
    imagesP(:,:,i)         = flipud(squeeze(fftshift(ifft2(imageNIIreal.img(:,:)))'));
    imagesP(:,:,i+NPulses) = flipud(squeeze(fftshift(ifft2(imageNIIimag.img(:,:)))'));

    % Store the images in complex form as well
    imagesPComplex(:,:,i) = imagesP(:,:,i) + 1i.*imagesP(:,:,i+NPulses);
end

% % % % Calculate maximum voxel value accross all images
% % % % in order to normalize them to this value
maxVoxelP      = max(max(max(imagesPComplex)));
imagesPComplex = imagesPComplex./maxVoxelP;

% % % % Reshape from 3D matrix into a 2D matrix of nx*ny X 2xNpulses
imagesPReshaped  = reshape(imagesP, [Nx*Ny, 2*NPulses]);

% % % % Normalize signals across the temporal domain
[imagesPNormReshaped2Ch, imagesPNormReshaped] = ...
                         normalizeSignals(imagesPReshaped);

% % % % Reshape them back from 2D to 3D
imagesPNorm = reshape(imagesPNormReshaped2Ch, [Nx, Ny, 2*NPulses]);

% % % % Calculate absolute normalized images
% % % % These are needed for matching and for display
imagesPNormAbs = abs( squeeze(imagesPNorm(:, :,         1 : NPulses)) + ...
                  1i.*squeeze(imagesPNorm(:, :, NPulses+1 : end)) ) ; 
              
%%
% % % % % % % % % % % Load EPG Dictionary
% Load data from dictionary file 
varToLoad = load(['/Users/irina/OneDrive - University College London/', ...
                  'Work/Simulation/simulationMRF/src/', ...'
                  'data_EPGDictionary/', ...
                  'epgdictionary-BSSFP-IR-sequenceListMaryia.mat']);
% Get values from loaded variable
F0states = varToLoad.F0states; 
% Material tuples
materialTuplesEPG = varToLoad.materialTuples;

% Create 2 channel data (real + imag)
F0statesReIm = [ real(F0states), imag(F0states)];

% Normalize signals
[dictionaryMRFFISPNorm2Ch, dictionaryMRFFISPNorm] = ...
        normalizeSignals(F0statesReIm);

% Calculate absolute signal
sigMRFFISPAbsNorm = abs(dictionaryMRFFISPNorm(:, 1:end));


%%
% % % % % % % % % % % Generate dictionary with old ALGORITHM
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Save them in simdir
seqList = dlmread('~/simdir/sequenceListN501MaryiaIR');
                % '~/simdir/sequenceListN10');
                % '~/simdir/sequenceListN500Maryia');
                % '~/simdir/sequenceListN300TRPerlinFASinusoid');
                % '~/simdir/sequenceListN300TRPerlinFASinusoid');
                % '~/simdir/sequenceListN50ConstantTR12FA40');
                % '~/simdir/sequenceListN300TRPerlinFASinusoid');
                % '~/simdir/sequenceListN500TRPerlinFASinusoid'
                % '~/simdir/sequenceListConstantN50TR12FA40'

% % % % Custom Sequence
FA_mrf  =  seqList(:, 2);
PhA_mrf =  zeros(NPulses,1);
TR_mrf  =  seqList(:, 1).*1000;
RO_mrf  =  TR_mrf./2;

% % % % Custom Material
T1_mrf = [20:10:3000, 3250:250:5000];
T2_mrf = [10: 5: 300,  350: 50:2200];
df_mrf =   0;

% % % % Generate dictionary
[ ~, ...                     % material properties
  sequenceProperties, ...    % sequence properties
  materialTuples, ...        % material tuples
  M, ...                     % number of material tuples
  ~, ...                     % dictionary MxNx3
  dictionaryMRFComplex, ...  % 
  dictionaryMRFNorm, ...     % dictionary normalised MxN
  dictionaryMRFNorm2Ch, ...  % dictionary normalised Mx2N
  signalDictionaryMRF] = ... % dictionary normalised MxN and absolute val
    func_generateCustomSignals(NPulses, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG);
                
%%
% % % % % % % % % % % % Dictionary Matching
% % % % Matching ALGORITHM 
% % % % % % Match with dictionary
[valuesOfMatchP, indicesMatchP, matTuplesP] = ...
                         matching(signalDictionaryMRF(:,2:end), ...
                                  imagesPNormReshaped(:,2:end), ...
                                  materialTuples, 0);
mapMatchValuesP = reshape(valuesOfMatchP, [Nx,Ny]);


% % % % Match POSSUM with EPG
[valuesOfMatchE, indicesMatchE, matTuplesE] = ...
                         matching(sigMRFFISPAbsNorm(:,2:end), ...
                                  imagesPNormReshaped(:,2:end), ...
                                  materialTuplesEPG, 0);
mapMatchValuesE = reshape(valuesOfMatchE, [Nx,Ny]);                                          

%%   
% % % % % % % % % % % % Plot results of matching
% % % % Which results to plot
toPlotEPGResults = 0;
% % % % MASK 64x64
maskForImages = createCirclesMask(ones(Nx,Ny), [33, 32], 25);
% % % % Maps of T1 and T2 for both epg and possum
mapT1P = reshape(matTuplesP.T1, [Nx,Ny]);
mapT2P = reshape(matTuplesP.T2, [Nx,Ny]);
mapT1E = reshape(matTuplesE.T1, [Nx,Ny]);
mapT2E = reshape(matTuplesE.T2, [Nx,Ny]);
                    
% % % % Results of matching
if toPlotEPGResults == 1
    mapToPlot = struct;
    mapToPlot.T1 = mapT1E;
    mapToPlot.T2 = mapT2E;
    mapToPlot.Match = mapMatchValuesE;
else
    mapToPlot = struct;
    mapToPlot.T1 = mapT1P;
    mapToPlot.T2 = mapT2P;
    mapToPlot.Match = mapMatchValuesP;
end


figure('Position', [10,10,1400,1000])
subplot(1,2,1)
imagesc(mapToPlot.T1 .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 2000])
title('T_1 map')

subplot(1,2,2)
imagesc(mapToPlot.T2 .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 500])
title('T_2 map')

figure('Position', [10,10,1400,1000])
imagesc(mapToPlot.Match .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0.9 1])
title('Values of Matching')



%% 
% % % % % % % % % % Plot acquired vs matched signals 
% % For a randomly chosen voxel
rndIDX = 870; %3226;%870;

% Plot image and location of voxel
figure('Position', [800,600,400,400])
temp = squeeze(imagesPReshaped(:,10));
temp(rndIDX) = max(temp);
temp = reshape(temp, [Nx, Ny]);
imagesc(rot90(abs(temp)))
colormap gray

% Plot signal of image and matched signal
figure('Position', [10,10,800,600])
% % % % SIGNAL FROM DATA
plot(1:NPulses-1, abs(imagesPNormReshaped(rndIDX,2:end)),'LineWidth',2)
hold on
% % % % SIGNAL FROM DICTIONARY
plot(1:NPulses-1, abs(signalDictionaryMRF(indicesMatchP(rndIDX),2:end)),'--', ...
     'LineWidth',2)
title(['Matching Score: ' , num2str(valuesOfMatchP(rndIDX))])
xlabel('#image'); ylabel('normalized signal intensity (a.u.)');
legend('phantom','dictionary')


% % For the best match
% Best match
[bestMatchVal, bestMatchIdx] = max(reshape(mapMatchValuesP, [Nx*Ny,1]));
% Plot image and location of voxel
figure('Position', [800,600,400,400])
temp = squeeze(imagesPReshaped(:,10));
temp(bestMatchIdx) = max(temp);
temp = reshape(temp, [Nx, Ny]);
imagesc(rot90(abs(temp)))
colormap gray

% Plot signal of image and matched signal
figure('Position', [10,10,800,600])
% % % % SIGNAL FROM DATA
plot(1:NPulses-1, abs(imagesPNormReshaped(bestMatchIdx,2:end)),'LineWidth',2)
hold on
% % % % SIGNAL FROM DICTIONARY
plot(1:NPulses-1, abs(signalDictionaryMRF(indicesMatchP(bestMatchIdx),2:end)),'--', ...
     'LineWidth',2)
title(['Matching Score: ' , num2str(valuesOfMatchP(bestMatchIdx))])
xlabel('#image'); ylabel('normalized signal intensity (a.u.)');
legend('phantom','dictionary')


% % For the worst match
% Best match
[worsttMatchVal, worstMatchIdx] = min(reshape(mapMatchValuesP, [Nx*Ny,1]));
% Plot image and location of voxel
figure('Position', [800,600,400,400])
temp = squeeze(imagesPReshaped(:,10));
temp(worstMatchIdx) = max(temp);
temp = reshape(temp, [Nx, Ny]);
imagesc(rot90(abs(temp)))
colormap gray

% Plot signal of image and matched signal
figure('Position', [10,10,800,600])
% % % % SIGNAL FROM DATA
plot(1:NPulses-1, abs(imagesPNormReshaped(worstMatchIdx,2:end)),'LineWidth',2)
hold on
% % % % SIGNAL FROM DICTIONARY
plot(1:NPulses-1, abs(signalDictionaryMRF(indicesMatchP(worstMatchIdx),2:end)),'--', ...
     'LineWidth',2)
title(['Matching Score: ' , num2str(valuesOfMatchP(worstMatchIdx))])
xlabel('#image'); ylabel('normalized signal intensity (a.u.)');
legend('phantom','dictionary')

% % % % %%
% % % % temp = reshape(abs(imagesPComplex), [Nx*Ny, 501]);
% % % % temp(100,:) = -0.5;
% % % % figure, imagesc(fliplr(abs(reshape(temp(:,10), [Nx,Ny]))))
% % % % colorbar
% % % % colormap gray
% % % % 
% % % % 
% % % % %%
% % % % % % % % REAL VALUES
% % % % realMRPar         = dlmread('~/simdir/MRparIri');
% % % % realMRPar(:, 1:2) = realMRPar(:,1:2).*1000;         % from s to ms
% % % % 
% % % % realMapsT1 = zeros(Nx,Ny);         realMapsT2 = zeros(Nx,Ny);
% % % % realMapsPD = zeros(Nx,Ny);
% % % % % For Nx=Ny=32
% % % % % % % % % CSF
% % % % realMapsT1( 6: 10,  8: 12) = realMRPar(3,1); 
% % % % realMapsT2( 6: 10,  8: 12) = realMRPar(3,2);
% % % % realMapsPD( 6: 10,  8: 12) = realMRPar(3,3);
% % % % % % % % % WM
% % % % realMapsT1(15: 19, 16: 19) = realMRPar(2,1);  
% % % % realMapsT2(15: 19, 16: 19) = realMRPar(2,2);  
% % % % realMapsPD(15: 19, 16: 19) = realMRPar(2,3);
% % % % % % % % % GM
% % % % realMapsT1(25: 27, 23: 25) = realMRPar(1,1);  
% % % % realMapsT2(25: 27, 23: 25) = realMRPar(1,2);
% % % % realMapsPD(25: 27, 23: 25) = realMRPar(1,3);  
% % % % 
% % % % % % % % PLOT
% % % % 
% % % % figure('Position', [10,10,1200,1200])
% % % % 
% % % % % POSSUM
% % % % subplot(3,4,1)                        
% % % % imagesc(mapT1P.*maskForImages)
% % % % axis off, axis square
% % % % colormap hot
% % % % colorbar, caxis([0, max(realMRPar(:,1))])
% % % % title('POSSUM matched with dict T_1')
% % % % 
% % % % subplot(3,4,2)
% % % % imagesc(mapT2P.*maskForImages)
% % % % axis off, axis square
% % % % colormap hot
% % % % colorbar, caxis([0, max(realMRPar(:,2))])
% % % % title('POSSUM matched with dict T_2')
% % % % 
% % % % % EPG
% % % % subplot(3,4,5)
% % % % imagesc(mapT1E.*maskForImages)
% % % % axis off, axis square
% % % % colormap hot
% % % % colorbar, caxis([0, max(realMRPar(:,1))])
% % % % title('POSSUM matched with EPG T_1')
% % % % 
% % % % subplot(3,4,6)
% % % % imagesc(mapT2E.*maskForImages)
% % % % axis off, axis square
% % % % colormap hot
% % % % colorbar, caxis([0, max(realMRPar(:,2))])
% % % % title('POSSUM matched with EPG T_2')
% % % % 
% % % % 
% % % % % REAL
% % % % subplot(3,4,9)
% % % % imagesc(realMapsT1)
% % % % axis off, axis square
% % % % colormap hot
% % % % colorbar, caxis([0, max(realMRPar(:,1))])
% % % % title('Real T_1')
% % % % 
% % % % subplot(3,4,10)
% % % % imagesc(realMapsT2)
% % % % axis off, axis square
% % % % colormap hot
% % % % colorbar, caxis([0, max(realMRPar(:,2))])
% % % % title('Real T_2')
% % % % 
% % % %               
% % % % % DIFFERENCE MAPS
% % % % subplot(3,4,3)
% % % % imagesc(realMapsT1-mapT1P.*maskForImages)
% % % % colormap hot
% % % % colorbar
% % % % axis square
% % % % title('Real T_1 - POSSUM T_1')
% % % %      
% % % % subplot(3,4,4)
% % % % imagesc(realMapsT2-mapT2P.*maskForImages)
% % % % colormap hot
% % % % colorbar
% % % % axis square
% % % % title('Real T_2 - POSSUM T_2')
% % % %               
% % % % subplot(3,4,7)
% % % % imagesc(realMapsT1-mapT1E.*maskForImages)
% % % % colormap hot
% % % % colorbar
% % % % axis square
% % % % title('Real T_1 - POSSUM T_1')
% % % %      
% % % % subplot(3,4,8)
% % % % imagesc(realMapsT2-mapT2E.*maskForImages)
% % % % colormap hot
% % % % colorbar
% % % % axis square
% % % % title('Real T_2 - POSSUM T_2')         
% % % %               
% % % % 
% % % % 


% % % % 
% % % % COMPARISON WITH SIGNALS
% % % % 
% % % % 
% % % % GMidx  = 1; % 5149;%1;   4904;
% % % % WMidx  = 5; % 2730;%5;   2695; 
% % % % CSFidx = 9; %24281;%9;   
% % % % 
% % % % %%
% % % % % % % % % % % % % % % Comparison with Dictionary generation
% % % % 
% % % % colors = ['r','b','g'];
% % % % figure('Position', [10,10,1200,800]);
% % % %         
% % % % % % % % % % CSF 
% % % % subplot(3,1,1)
% % % % plot(1:NPulses, squeeze(imagesPNormAbs(roix(1), roiy(1), :)), ...
% % % %     [colors(1),'.-'])                                 % POSSUM
% % % % hold on
% % % % plot(1:NPulses, signalDictionaryMRF(CSFidx,:), 'ko-')% DICTIONARY
% % % % % plot(1:NPulses, sigMRFFISPAbsNorm(CSFidx,:), 'go--')  % EPG
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('normalized intensity')
% % % % legend('Possum-CSF', 'Dictionary-CSF', 'EPG-CSF')
% % % % title('Comparison with POSSUM')
% % % % 
% % % % % % % % % % WM
% % % % subplot(3,1,2)
% % % % plot(1:NPulses, squeeze(imagesPNormAbs(roix(2), roiy(2), :)), ...
% % % %     [colors(1),'.-'])                                 % POSSUM
% % % % hold on   
% % % % plot(1:NPulses, signalDictionaryMRF(WMidx,:), 'ko-') % DICTIONARY
% % % % % plot(1:NPulses, sigMRFFISPAbsNorm(WMidx,:), 'go--')  % EPG
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('normalized intensity');
% % % % legend('Possum-WM', 'Dictionary-WM', 'EPG-WM')
% % % % 
% % % % % % % % % % GM
% % % % subplot(3,1,3)
% % % % plot(1:NPulses, squeeze(imagesPNormAbs(roix(3), roiy(3), :)), ...
% % % %     [colors(1),'.-'])                                 % POSSUM
% % % % hold on
% % % % plot(1:NPulses, signalDictionaryMRF(GMidx,:), 'ko-') % DICTIONARY
% % % % % plot(1:NPulses, sigMRFFISPAbsNorm(GMidx,:), 'go--')  % EPG
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('normalized intensity')
% % % % legend('Possum-GM', 'Dictionary-GM', 'EPG-GM')
% % % % 
% % % % %%
% % % % % % % % % % % % % % % MORE Comparison with Dictionary generation
% % % % colors = ['r','b','g'];
% % % % figure('Position', [10,10,1200,800]);
% % % %         
% % % % % % % % % % CSF 
% % % % subplot(3,2,1)
% % % % plot(0:0.1:1, 0:0.1:1, 'k') % identity line
% % % % hold on
% % % % plot(signalDictionaryMRF(CSFidx,:), ...
% % % %     squeeze(imagesPNormAbs(roix(1), roiy(1), :)), ...
% % % %     [colors(1),'o'])                              % DICT vs POSSUM
% % % % grid on
% % % % axis square
% % % % xlabel({'normalized intensity', 'Dictionary-CSF'});
% % % % ylabel({'normalized intensity', 'POSSUM-CSF'});
% % % % % % % % % % CSF difference
% % % % subplot(3,2,2)
% % % % plot(1:NPulses, signalDictionaryMRF(CSFidx,:) - ...
% % % %     squeeze(imagesPNormAbs(roix(1), roiy(1), :))', ...
% % % %     [colors(1),'-'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('Dictionary-CSF - POSSUM-CSF')
% % % % title('Comparison of signals between POSSUM and dictionary generation')
% % % % 
% % % % % % % % % % WM
% % % % subplot(3,2,3)
% % % % plot(0:0.1:1, 0:0.1:1, 'k') % identity line
% % % % hold on
% % % % axis square
% % % % plot(signalDictionaryMRF(WMidx,:), ...
% % % %     squeeze(imagesPNormAbs(roix(2), roiy(2), :)), ...
% % % %     [colors(1),'o'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel({'normalized intensity', 'Dictionary-WM'});
% % % % ylabel({'normalized intensity', 'POSSUM-WM'});
% % % % % % % % % % WM difference
% % % % subplot(3,2,4)
% % % % plot(1:NPulses, signalDictionaryMRF(WMidx,:) - ...
% % % %     squeeze(imagesPNormAbs(roix(2), roiy(2), :))', ...
% % % %     [colors(1),'-'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('Dictionary-WM - POSSUM-WM')
% % % % 
% % % % % % % % % % GM
% % % % subplot(3,2,5)
% % % % plot(0:0.1:1, 0:0.1:1, 'k') % identity line
% % % % hold on
% % % % axis square
% % % % plot(signalDictionaryMRF(GMidx,:), ...
% % % %     squeeze(imagesPNormAbs(roix(3), roiy(3), :)), ...
% % % %     [colors(1),'o'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel({'normalized intensity', 'Dictionary-GM'});
% % % % ylabel({'normalized intensity', 'POSSUM-GM'});
% % % % % % % % % % GM difference
% % % % subplot(3,2,6)
% % % % plot(1:NPulses, signalDictionaryMRF(GMidx,:) - ...
% % % %     squeeze(imagesPNormAbs(roix(3), roiy(3), :))', ...
% % % %     [colors(1),'-'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('Dictionary-GM - POSSUM-GM')
% % % % 
% % % % 
% % % % %%
% % % % % % % % % % % % % % % MORE Comparison with EPG
% % % % colors = ['r','b','g'];
% % % % figure('Position', [10,10,1200,800]);
% % % %         
% % % % % % % % % % CSF 
% % % % subplot(3,2,1)
% % % % plot(0:0.1:1, 0:0.1:1, 'k') % identity line
% % % % hold on
% % % % plot(sigMRFFISPAbsNorm(CSFidx,:), ...
% % % %     squeeze(imagesPNormAbs(roix(1), roiy(1), :)), ...
% % % %     [colors(1),'o'])                              % DICT vs POSSUM
% % % % grid on
% % % % axis square
% % % % xlabel({'normalized intensity', 'EPG-CSF'});
% % % % ylabel({'normalized intensity', 'POSSUM-CSF'});
% % % % % % % % % % CSF difference
% % % % subplot(3,2,2)
% % % % plot(1:NPulses, sigMRFFISPAbsNorm(CSFidx,:) - ...
% % % %     squeeze(imagesPNormAbs(roix(1), roiy(1), :))', ...
% % % %     [colors(1),'-'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('EPG-CSF - POSSUM-CSF')
% % % % title('Comparison of signals between POSSUM and EPG')
% % % % 
% % % % % % % % % % WM
% % % % subplot(3,2,3)
% % % % plot(0:0.1:1, 0:0.1:1, 'k') % identity line
% % % % hold on
% % % % axis square
% % % % plot(sigMRFFISPAbsNorm(WMidx,:), ...
% % % %     squeeze(imagesPNormAbs(roix(2), roiy(2), :)), ...
% % % %     [colors(1),'o'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel({'normalized intensity', 'EPG-WM'});
% % % % ylabel({'normalized intensity', 'POSSUM-WM'});
% % % % % % % % % % WM difference
% % % % subplot(3,2,4)
% % % % plot(1:NPulses, sigMRFFISPAbsNorm(WMidx,:) - ...
% % % %     squeeze(imagesPNormAbs(roix(2), roiy(2), :))', ...
% % % %     [colors(1),'-'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('EPG-WM - POSSUM-WM')
% % % % 
% % % % % % % % % % GM
% % % % subplot(3,2,5)
% % % % plot(0:0.1:1, 0:0.1:1, 'k') % identity line
% % % % hold on
% % % % axis square
% % % % plot(sigMRFFISPAbsNorm(GMidx,:), ...
% % % %     squeeze(imagesPNormAbs(roix(3), roiy(3), :)), ...
% % % %     [colors(1),'o'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel({'normalized intensity', 'EPG-GM'});
% % % % ylabel({'normalized intensity', 'POSSUM-GM'});
% % % % % % % % % % GM difference
% % % % subplot(3,2,6)
% % % % plot(1:NPulses, sigMRFFISPAbsNorm(GMidx,:) - ...
% % % %     squeeze(imagesPNormAbs(roix(3), roiy(3), :))', ...
% % % %     [colors(1),'-'])                              % DICT vs POSSUM
% % % % grid on
% % % % xlabel('image number');
% % % % ylabel('EPG-GM - POSSUM-GM')
% % % % 
% % % %               