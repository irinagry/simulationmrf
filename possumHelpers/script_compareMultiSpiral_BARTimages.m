% 
% Irina Grigorescu
% Date created: 13-12-2017
% 
% Script to reconstruct a JEMRIS multi-pulse spiral simulation with BART
% 

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% 1. Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/spiral/spiralFullySampledConstant/';

% Set number of repetitions to get to the fully sampled
NReps = 1;
% Set number of TR blocks
NTRs  = 1; 
% Set effective image size
Nx   = 256; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 6000;%Nx*Ny;

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NReps, NTRs);
kspace_z = zeros(nROPoints*NReps, 1);

% Create the spiral k-space
Td = 6; %ms
t = linspace(0,Td,nROPoints);
spiralAmplitude = 0.2; 
spiralFrequency = 40;
kkk_x = spiralAmplitude .* t .* cos(spiralFrequency .* t);
kkk_y = spiralAmplitude .* t .* sin(spiralFrequency .* t);


% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure(111)
for i = 1:NTRs
    
    % Populate the spiral k-spaces
    kspace_x(:,i) = kkk_x.';
    kspace_y(:,i) = kkk_y.';
    
    % Plot the coordinates before scaling
    subplot(1,2,1); xl = 1;
    scatter(kspace_x(:, i),kspace_y(:, i),'.'), axis equal, axis square;
    xlabel('k_x rad mm^{-1}'); ylabel('k_y rad mm^{-1}');
    title(['Spiral ', num2str(i)])
    xlim([-xl xl]); ylim([-xl xl]);

    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,i) - kspace_x(end,i) ).^2 + ... 
                     (kspace_y(1,i) - kspace_y(end,i) ).^2 ) ;
    traj_spiral = [ kspace_x(:,i), ...
                    kspace_y(:,i), ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, i) = traj_spiral2(1,:);
    kspace_y(:, i) = traj_spiral2(2,:);
    
    % Plot the coordinates after scaling
    subplot(1,2,2)
    scatter(kspace_x(:, i), kspace_y(:, i),'.'), axis equal, axis square;
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(i)])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

    drawnow
end



%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs

for i = 1:NReps
    % Get signal values from JEMRIS simulation
    %filenameSignal = [folderName, 'signals', num2str(i),'.h5']; 
    filenameSignal = [folderName, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Get timings of signal readout
    timingRO = h5read(filenameSignal, '/signal/times');
    % % % Readout points
    Nro = size(timingRO, 1);
    % % % Sort them and take signal in sorted order
    [timingRO,J] = sort(timingRO); Mvecs = A(:,:);
    % % % Calculate timings between each line of k-space and
    % % % between different slices
    d = diff(diff(timingRO));
    d(d<1e-5) = 0;

    % % % And store them in the solumn vector of indices I
    % % % together with index 0 and last index
    I = [0; find(d) + 1; length(timingRO)];

    % Get kspace as one signal
    for j = 1:NTRs
        kspaces(:,i,j) = Mvecs((j-1)*nROPoints+1 : j*nROPoints, 1).' + ...
              sqrt(-1) * Mvecs((j-1)*nROPoints+1 : j*nROPoints, 2).';

    end
end

% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints*NReps, NTRs]);

%% 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);

% Save signal from image space from one voxel from the image
signalImage = zeros(NTRs, 1);

figure(222)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = squeeze(kspace_x(:, i));
    currentKspace_y = squeeze(kspace_y(:, i));
    currentKspaceToReco = squeeze(kspaces(:, i)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    % zeropad to full size
    igrid = bart(['resize -c 0 ', num2str(Nx),' 1 ', num2str(Ny)], ...
                      igrid);
    imagesBART(:, :, i) = flipud(rot90(igrid));
    toc

    % Plot images
    subplot(2,2,1), imagesc(abs(igrid))
    title('BART reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,2,2), imagesc(real(igrid))
    title('BART reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,2,4), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    pause(0.01)
end


%% 5. Compare the reconstruction
% % % % Normalize the reconstruction
% BART
[~, imagesBART] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBART = reshape(imagesBART, [Nx, Ny, NTRs]);
           
%%

figure('Position', [10,10,1200,1200]);
lima = 0.055;

for i = 1:NTRs
    % BART RECONSTRUCTION
    % REAL
    subplot(1,3,1)
    imagesc(real(squeeze(imagesBART(:,:,i))));
    title('Normalised BART reco (real)')
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % IMAG
    subplot(1,3,2)
    imagesc(angle(squeeze(imagesBART(:,:,i))));
    title(['Normalised BART reco (imag) ', num2str(i)])
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % ABS
    subplot(1,3,3)
    imagesc(abs(squeeze(imagesBART(:,:,i))));
    title('Normalised BART reco (abs)')
    axis square; axis off; colorbar
%     caxis([-lima lima])

    
    pause(0.01)
end

%% Comparison with dictionary
% % % % % % % % % % % Generate dictionary with my ALGORITHM
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Sequence
FA_mrf  =  repmat(90, [NTRs,1]);
PhA_mrf =  zeros(NTRs,1);
TR_mrf  =  zeros(NTRs,1) + 15;
RO_mrf  =  zeros(NTRs,1) +  6;

% % % % Custom Material
T1_mrf = [800, 1000, 1200, 1400, 1600, 2000];
T2_mrf = [ 500,  600, 700];
df_mrf =   0;

% % % % Generate dictionary
% % % % Custom Sequence
customSequence.RF.FA  =  FA_mrf;
customSequence.RF.PhA =  PhA_mrf;
customSequence.TR     =  TR_mrf;
customSequence.RO     =  RO_mrf;

% % % % Custom Material
customMaterial.T1     = T1_mrf;
customMaterial.T2     = T2_mrf;
customMaterial.offRes = df_mrf;
tic
[materialProperties, sequenceProperties, ...
    materialTuples, M, ...
    dictionaryMRF] = ...
                 simulateMRF(NTRs, FLAGPLOT, ...
                 SEQFLAG, customSequence, ...
                 MATFLAG, customMaterial, 1); % flag IR on
t=toc;
% Get the x and y values so that you have it as complex
dictionaryMRFComplex = [squeeze(dictionaryMRF(:,:,1)), ...
                        squeeze(dictionaryMRF(:,:,2))];
% % % % Normalize dictionary:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[dictionaryMRFNorm2Ch, dictionaryMRFNorm] = ...
        normalizeSignals(dictionaryMRFComplex);
   
%%
% % % % % % % % % % % % Dictionary Matching
% % % % Matching ALGORITHM 
% % % % Match BART with dictionary
imagesBARTReshaped = reshape(imagesBART, [Nx*Ny, NTRs]);

%%
[valuesOfMatchB, indicesMatchB, matTuplesB] = ...
                         matching(dictionaryMRFNorm, ...
                                  imagesBARTReshaped, ...
                                  materialTuples, 0);
mapMatchValuesB = reshape(valuesOfMatchB, [Nx,Ny]);                                          
   
%% Plot dictionary signal vs image signal
idxToPlot = (Nx/2)*Ny+(Nx/2);
nLin = 2; nCol = 3;

figure
% % % % % % % % % % BART
subplot(nLin,nCol,1), hold on
plot(1:NTRs, abs(imagesBARTReshaped(idxToPlot,:))) 
plot(1:NTRs, abs(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'--')
legend('abs(BART)','abs(dictionary)')

subplot(nLin,nCol,2), hold on
plot(1:NTRs, real(imagesBARTReshaped(idxToPlot,:))) 
plot(1:NTRs, real(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'--')
legend('real(BART)','real(dictionary)')

subplot(nLin,nCol,3), hold on
plot(1:NTRs, imag(imagesBARTReshaped(idxToPlot,:))) 
plot(1:NTRs, imag(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'--')
legend('imag(BART)','imag(dictionary)')

% % % % % % % % % % BART DIFFERENCES
subplot(nLin,nCol,4), hold on
plot(1:NTRs, abs(imagesBARTReshaped(idxToPlot,:)) - ...
             abs(dictionaryMRFNorm(indicesMatchB(idxToPlot),:))) 
legend('abs(BART)-abs(dictionary)')

subplot(nLin,nCol,5), hold on
plot(1:NTRs, real(imagesBARTReshaped(idxToPlot,:)) - ...
             real(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)))
legend('real(BART)-real(dictionary)')

subplot(nLin,nCol,6), hold on
plot(1:NTRs, imag(imagesBARTReshaped(idxToPlot,:)) - ...
             imag(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)))
legend('imag(BART)-imag(dictionary)')



%%   
% % % % % % % % % % % % Plot results of matching
% % % % MASK Nx x Ny
maskForImages = createCirclesMask(ones(Nx,Ny), [Nx/2+1, Ny/2+1], 5);
% % % % Maps of T1 and T2 for both epg and possum
mapT1B = reshape(matTuplesB.T1, [Nx,Ny]);
mapT2B = reshape(matTuplesB.T2, [Nx,Ny]);
mapMaB = reshape(valuesOfMatchB, [Nx,Ny]);

% % % % PLOT MAPS
figure('Position', [10,10,1400,1000])
subplot(3,1,1)
imagesc(mapT1B .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 2000])
title('T_1 map (BART)')

subplot(3,1,2)
imagesc(mapT2B .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 1000])
title('T_2 map (BART)')

% % % % Match results
subplot(3,1,3)
imagesc(mapMaB .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0.9 1])
title('Values of Matching (BART)')

   