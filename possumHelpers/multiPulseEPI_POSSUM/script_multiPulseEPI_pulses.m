% Irina Grigorescu
% 
% This script looks at the multi-pulse epi sequences
% created by POSSUM
close all;clear all;clc
% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);

% Path to POSSUM pulse output 
folderName = '/Users/irina/simdirTestMultiEPI/outPulses/';
fileName   = 'pulse_epiMultiPulse1TRShotTestKspace';


% Read the pulse Sequence
pulseSequence0 = read_pulse([folderName, fileName, '0']);
pulseSequence1 = read_pulse([folderName, fileName, '1']);
pulseSequence2 = read_pulse([folderName, fileName, '2']);
pulseSequence3 = read_pulse([folderName, fileName, '3']);
pulseSequence4 = read_pulse([folderName, fileName, '4']);


% Read the k-space coordinates
kspaceCoord0 = read_pulse([folderName,fileName,num2str(0),'_kcoord']);
kspaceCoord1 = read_pulse([folderName,fileName,num2str(1),'_kcoord']);
kspaceCoord2 = read_pulse([folderName,fileName,num2str(2),'_kcoord']);
kspaceCoord3 = read_pulse([folderName,fileName,num2str(3),'_kcoord']);
kspaceCoord4 = read_pulse([folderName,fileName,num2str(4),'_kcoord']);
minKsp = min(min(min(kspaceCoord1,kspaceCoord4)));
maxKsp = max(max(max(kspaceCoord1,kspaceCoord4)));

figure(2)
for i = 1:size(kspaceCoord1,2)
    scatter(kspaceCoord1(1,i), kspaceCoord1(2,i), 'ro'); hold on
    scatter(kspaceCoord2(1,i), kspaceCoord2(2,i), 'b.'); 
    scatter(kspaceCoord3(1,i), kspaceCoord3(2,i), 'go'); 
    scatter(kspaceCoord4(1,i), kspaceCoord4(2,i), 'k.'); 
    xlim([minKsp maxKsp]); ylim([minKsp maxKsp]); 
    pause(0.01)
end
for i = 1:size(kspaceCoord0,2)
    scatter(kspaceCoord0(1,i), kspaceCoord0(2,i), 'y*'); hold on
    xlim([minKsp maxKsp]); ylim([minKsp maxKsp]); 
    pause(0.01)
end

%% Plot the pulse sequence
pulseSequence = pulseSequence1;
titles = {'time (s)', ...
          'RF angle (deg)', ...
          'RF freq bw (Hz)', ...
          'RF centre freq (Hz)', ...
          'RO', ...
          'Gx (T/m)', ...
          'Gy (T/m)', ...
          'Gz (T/m)'};
indices = [2,6,3,7,4,8,5];
startIdx =   1;
endIdx   =   size(pulseSequence,1);

figure(1)
hold on
for i = 1:7
    subplot(4,2,i)
    
    % Plot each subplot
    % If 1 change radians to degrees
    if i == 1
        plot(pulseSequence(startIdx:endIdx,1), ...
             rad2deg(pulseSequence(startIdx:endIdx, indices(i))))
         
        hold on
    end
    
    % Plot readout
    if indices(i) == 5
        stem(pulseSequence(startIdx:endIdx,1), ...
             pulseSequence(startIdx:endIdx, indices(i)), 'r.')
	% Else leave as it is
    else
        plot(pulseSequence(startIdx:endIdx,1), ...
             pulseSequence(startIdx:endIdx, indices(i)))
        hold on    
    end 
    
    % Labels
    xlabel('time (s)')
    ylabel(titles{indices(i)})
end
% Last subplot is made up of all gradients put together
subplot(4,2,8)
plot(pulseSequence(startIdx:endIdx,1), pulseSequence(startIdx:endIdx, 6))
hold on
plot(pulseSequence(startIdx:endIdx,1), pulseSequence(startIdx:endIdx, 7))
hold on
plot(pulseSequence(startIdx:endIdx,1), pulseSequence(startIdx:endIdx, 8))
hold on
xlabel('time (s)')
ylabel('G (T/m)')
legend('G_x', 'G_y', 'G_z')
