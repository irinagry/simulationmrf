% Irina Grigorescu
% 
% This script looks at the image created by a fully sampled k-space
% vs
% the image created by combining the k-spaces coming from multiple
% acquisitions

close all; clear all; clc

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);

% Path to POSSUM pulse output 
folderName = '/Users/irina/simdirTestMultiEPI/outKspace/';
fileName   = '_epiMultiPulse10TRShotTestKspace';

%%
% % % % % % % % % % % LOAD POSSUM IMAGES from multi pulse acquisition
% % % % % % % % % % %                and from singl pulse acquisition
Nx = 32; Ny = 8;
RAcc = Nx / Ny;
NPulses = 50;

% % % % Store kspace
kspaceP = zeros(Nx, RAcc*Ny, NPulses);

% % % % Store images
imagesP            = zeros(Nx,RAcc*Ny,2*NPulses);
imagesPComplex     = zeros(Nx,RAcc*Ny,  NPulses);
imagesPSinglePulse = zeros(Nx,RAcc*Ny,  NPulses);

% % % % For each multipulse block:
% We need to retrieve the k-spaces for each multipulse block to make one
% big k-space
for idxTR = 1:NPulses
    for idxAcc = 1:RAcc
        filenameKsp = [ folderName, ...             % folder
                        'kspace',  ...              % core name
                        fileName,   ...             % sequence
                        num2str(idxAcc), '_', ...   % multishot number
                        num2str(idxTR)];  % TR block         

        % Load in the real and the imaginary channels
        kspaceNIIreal = load_nii([filenameKsp, '_real.nii.gz'], ...
                            [], [], [], [], [], 0.5);
        kspaceNIIimag = load_nii([filenameKsp, '_imag.nii.gz'], ...
            [], [], [], [], [], 0.5);

        % Make one big k-space
        kspaceP(:, idxAcc:RAcc:end, idxTR) = kspaceNIIreal.img + ...
                                       1i .* kspaceNIIimag.img ;
    end
end
    
% % % % For each TR block:  
for idxTR = 1:NPulses
    % Store real and imaginary channels
    % in a matrix like this:
    % [real1 real2 ... realNP imag1 imag2 ... imagNP]
    imagesP(:,:,idxTR)         = flipud(squeeze(fftshift(ifft2(...
                                         real(kspaceP(:,:,idxTR))))'));
    imagesP(:,:,idxTR+NPulses) = flipud(squeeze(fftshift(ifft2(...
                                         imag(kspaceP(:,:,idxTR))))'));

    % Store the images in complex form as well
    imagesPComplex(:,:,idxTR) = squeeze(imagesP(:,:,idxTR)) + ...
                           1i .* squeeze(imagesP(:,:,idxTR+NPulses));
end


% Get the single pulse acquisition too
for idxTR = 1:NPulses
    filenameKsp = [ folderName, ...          % folder
                    'kspace',  ...           % core name
                    fileName,   ...          % sequence
                    num2str(0), '_', ...     % multishot number
                    num2str(idxTR)];  % TR block       

    % Load in the real and the imaginary channels
    kspaceNIIreal = load_nii([filenameKsp, '_real.nii.gz'], ...
                        [], [], [], [], [], 0.5);
    kspaceNIIimag = load_nii([filenameKsp, '_imag.nii.gz'], ...
        [], [], [], [], [], 0.5);

    % Get image from single pulse acquisition
    imagesPSinglePulse(:,:,idxTR) = ...
            flipud(squeeze(fftshift(ifft2(kspaceNIIreal.img(:,:)))')) + ...
      1i .* flipud(squeeze(fftshift(ifft2(kspaceNIIimag.img(:,:)))'));
end
      
% Plot the 2 side by side and difference
figure
for idxTR = 1:NPulses
    subplot(3,3,1)
    imagesc(abs(imagesPComplex(:,:,idxTR))), colorbar
    title('abs multi')
    subplot(3,3,2)
    imagesc(abs(imagesPSinglePulse(:,:,idxTR))), colorbar
    title({'abs single',num2str(idxTR)})
    subplot(3,3,3)
    imagesc(abs(imagesPComplex(:,:,idxTR)) - ...
            abs(imagesPSinglePulse(:,:,idxTR))), colorbar
    title('diff abs')

    subplot(3,3,4)
    imagesc(real(imagesPComplex(:,:,idxTR))), colorbar
    title('real multi')
    subplot(3,3,5)
    imagesc(real(imagesPSinglePulse(:,:,idxTR))), colorbar
    title('real single')
    subplot(3,3,6)
    imagesc(real(imagesPComplex(:,:,idxTR)) - ...
            real(imagesPSinglePulse(:,:,idxTR))), colorbar
    title('diff real')

    subplot(3,3,7)
    imagesc(imag(imagesPComplex(:,:,idxTR))), colorbar
    title('imag multi')
    subplot(3,3,8)
    imagesc(imag(imagesPSinglePulse(:,:,idxTR))), colorbar
    title('imag single')
    subplot(3,3,9)
    imagesc(imag(imagesPComplex(:,:,idxTR)) - ...
            imag(imagesPSinglePulse(:,:,idxTR))), colorbar
    title('diff imag')
   
    pause
end