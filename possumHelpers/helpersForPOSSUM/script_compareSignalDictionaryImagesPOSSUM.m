% % % % IRINA GRIGORESCU
% % % % Date created: 31-07-2017
% % % % Date updated: 31-07-2017
% % % %
% % % % All-in-one script that compares simulations from 
% % % % POSSUM images and dictionary generation
% % % % 
% % % % PLEASE PAY ATTENTION AT THIS SCRIPT AS IT HAS THE PROPER 
% % % % SIGNAL NORMALIZATION
% % % % USE AS REFERENCE FOR CODE, BUT NOT WORKING PROPERLY

clear all
close all

% Add paths:
run('runPathsForPOSSUMandEPG');

% % % % SOME VARIABLES
NPulses = 300; Nx = 32; Ny = 32;
toPlotQualitative = 0;

% Nx=Ny=32
roix = [ 8, 17, 26]; %32 % CSF | WM | GM
roiy = [ 9, 18, 24]; %32 % CSF | WM | GM

%%
% % % % % % % % % % % LOAD POSSUM IMAGES
seqName  = 'epib';
imgSize  = [num2str(Nx), 'x', num2str(Ny)];
npulses  = ['Np', num2str(NPulses)];%, '_noGz'];
objName  = 'WMGMCSF';

% % % % Load Files
imagesP        = zeros(Nx,Nx,2*NPulses);
imagesPComplex = zeros(Nx,Nx,  NPulses);

% % % % For each TR block:
for i = 1:NPulses
    filenameImg = ['~/simdir/outImage/', ...          % folder
                   'image_', ...                      % core name
                    seqName, '_', ...                 % sequence
                    objName, '_', ...                 % objName
                    imgSize, '_', ...                 % size of image
                    npulses, '_', ...                 % number of pulses
                    num2str(i)];                      % image number
                
	% Load in the real and the imaginary channels
    imageNIIreal = load_nii([filenameImg, '_real.nii.gz'], ...
                        [], [], [], [], [], 0.5);
    imageNIIimag = load_nii([filenameImg, '_imag.nii.gz'], ...
        [], [], [], [], [], 0.5);
    
    % Store real and imaginary channels
    % in a matrix like this:
    % [real1 real2 ... realNP imag1 imag2 ... imagNP]
    imagesP(:,:,i)         = flipud(squeeze(imageNIIreal.img(:,:))');
    imagesP(:,:,i+NPulses) = flipud(squeeze(imageNIIimag.img(:,:))');

    % Store the images in complex form as well
    imagesPComplex(:,:,i) = imagesP(:,:,i) + 1i.*imagesP(:,:,i+NPulses);
end

% % % % Calculate maximum voxel value accross all images
% % % % in order to normalize them to this value
maxVoxelP      = max(max(max(imagesPComplex)));
imagesPComplex = imagesPComplex./maxVoxelP;

% % % % Reshape from 3D matrix into a 2D matrix
imagesPReshaped  = reshape(imagesP, [Nx*Ny, 2*NPulses]);

% % % % Normalize signals across the temporal domain
[imagesPNormReshaped, ~] = normalizeSignals(imagesPReshaped);

% % % % Reshape them back from 2D to 3D
imagesPNorm = reshape(imagesPNormReshaped, [Nx, Ny, 2*NPulses]);

% % % % Calculate absolute normalized images
% % % % These are needed for matching and for display
imagesPNormAbs = abs( squeeze(imagesPNorm(:, :,         1 : NPulses)) + ...
                  1i.*squeeze(imagesPNorm(:, :, NPulses+1 : end)) ) ; 

%%
% % % % % % % % % % % Generate dictionary
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Save them in simdir
seqList = dlmread('~/simdir/sequenceListN300TRPerlinFASinusoid');
                % '~/simdir/sequenceListN300TRPerlinFASinusoid');
                % '~/simdir/sequenceListN50ConstantTR12FA40');
                % '~/simdir/sequenceListN300TRPerlinFASinusoid');
                % '~/simdir/sequenceListN500TRPerlinFASinusoid'
                % '~/simdir/sequenceListConstantN50TR12FA40'

% % % % Custom Sequence
FA_mrf  =  seqList(:, 2);
PhA_mrf =  zeros(NPulses,1);
TR_mrf  =  seqList(:, 1).*1000;
RO_mrf  =  TR_mrf./2;


% % % % Custom Material
T1_mrf = [20:10:3000, 3000:200:5000]; %[950, 600, 4500, 0.1]; % GM | WM | CSF
T2_mrf = [10: 5: 300,  300: 50:2200]; %[100,  80, 2200, 0.1];
df_mrf =   0;

% % % % Generate dictionary
% % NOTE: THIS IS NOT CORRECT
[ materialProperties, sequenceProperties, ...
  materialTuples, M, ...
  dictionaryMRF, signalDictionaryMRF, ...
  dictionaryMRFComplex] = ...
    func_generateCustomSignals(NPulses, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG);
                
% M = [(sequenceProperties.TR + 1)./1000, sequenceProperties.RF.FA];
% dlmwrite('~/simdir/sequenceListN300TRPerlinFASinusoid', M, ' ');

GMidx  =  1; WMidx  =  5; CSFidx =  9;

%%
% % % % % % % % % % % Comparison with Dictionary generation

colors = ['r','b','g'];
figure('Position', [10,10,1200,800]);
        
        % % % % % % CSF 
        subplot(3,1,1)
        plot(1:NPulses, squeeze(imagesPNormAbs(roix(1), roiy(1), :)), ...
            [colors(1),'.-'])                                 % POSSUM
        hold on
        plot(1:NPulses, signalDictionaryMRF(CSFidx,:), 'ko--')% DICTIONARY
        grid on
        xlabel('image number');
        ylabel('normalized intensity')
        legend('Possum-CSF', 'Dictionary-CSF')
        title('Comparison with POSSUM')

        % % % % % % WM
        subplot(3,1,2)
        plot(1:NPulses, squeeze(imagesPNormAbs(roix(2), roiy(2), :)), ...
            [colors(1),'.-'])                                 % POSSUM
        hold on   
        plot(1:NPulses, signalDictionaryMRF(WMidx,:), 'ko--') % DICTIONARY
        grid on
        xlabel('image number');
        ylabel('normalized intensity');
        legend('Possum-WM', 'Dictionary-WM')

        % % % % % % GM
        subplot(3,1,3)
        plot(1:NPulses, squeeze(imagesPNormAbs(roix(3), roiy(3), :)), ...
            [colors(1),'.-'])                                 % POSSUM
        hold on
        plot(1:NPulses, signalDictionaryMRF(GMidx,:), 'ko--') % DICTIONARY
        grid on
        xlabel('image number');
        ylabel('normalized intensity')
        legend('Possum-GM', 'Dictionary-GM')


%%
% % % % % % % % % % % MORE Comparison with Dictionary generation
colors = ['r','b','g'];
figure('Position', [10,10,1200,800]);
        
        % % % % % % CSF 
        subplot(3,2,1)
        plot(0:0.1:1, 0:0.1:1, 'k') % identity line
        hold on
        plot(signalDictionaryMRF(CSFidx,:), ...
            squeeze(imagesPNormAbs(roix(1), roiy(1), :)), ...
            [colors(1),'o'])                              % DICT vs POSSUM
        grid on
        axis square
        xlabel({'normalized intensity', 'Dictionary-CSF'});
        ylabel({'normalized intensity', 'POSSUM-CSF'});
        % % % % % % CSF difference
        subplot(3,2,2)
        plot(1:NPulses, signalDictionaryMRF(CSFidx,:) - ...
            squeeze(imagesPNormAbs(roix(1), roiy(1), :))', ...
            [colors(1),'-'])                              % DICT vs POSSUM
        grid on
        xlabel('image number');
        ylabel('Dictionary-CSF - POSSUM-CSF')
        
        % % % % % % WM
        subplot(3,2,3)
        plot(0:0.1:1, 0:0.1:1, 'k') % identity line
        hold on
        axis square
        plot(signalDictionaryMRF(WMidx,:), ...
            squeeze(imagesPNormAbs(roix(2), roiy(2), :)), ...
            [colors(1),'o'])                              % DICT vs POSSUM
        grid on
        xlabel({'normalized intensity', 'Dictionary-WM'});
        ylabel({'normalized intensity', 'POSSUM-WM'});
        % % % % % % WM difference
        subplot(3,2,4)
        plot(1:NPulses, signalDictionaryMRF(WMidx,:) - ...
            squeeze(imagesPNormAbs(roix(2), roiy(2), :))', ...
            [colors(1),'-'])                              % DICT vs POSSUM
        grid on
        xlabel('image number');
        ylabel('Dictionary-WM - POSSUM-WM')

        % % % % % % GM
        subplot(3,2,5)
        plot(0:0.1:1, 0:0.1:1, 'k') % identity line
        hold on
        axis square
        plot(signalDictionaryMRF(GMidx,:), ...
            squeeze(imagesPNormAbs(roix(3), roiy(3), :)), ...
            [colors(1),'o'])                              % DICT vs POSSUM
        grid on
        xlabel({'normalized intensity', 'Dictionary-GM'});
        ylabel({'normalized intensity', 'POSSUM-GM'});
        % % % % % % GM difference
        subplot(3,2,6)
        plot(1:NPulses, signalDictionaryMRF(GMidx,:) - ...
            squeeze(imagesPNormAbs(roix(3), roiy(3), :))', ...
            [colors(1),'-'])                              % DICT vs POSSUM
        grid on
        xlabel('image number');
        ylabel('Dictionary-GM - POSSUM-GM')




%%
% % % % % % % % % % % % Dictionary Matching

maskForImages = ones(Nx,Ny);
% For Nx=Ny=16
% maskForImages( 3: 5, 4: 6) = 1;  
% maskForImages( 8:10, 9:10) = 1;
% maskForImages(12:14,12:13) = 1;
% For Nx=Ny=32
maskForImages( 6: 10,  8: 12) = 1;  
maskForImages(15: 19, 16: 19) = 1;
maskForImages(25: 27, 23: 25) = 1;


% % % % Match POSSUM
possumToMatch = reshape(imagesPNormAbs, [Nx*Ny,NPulses]);

[indicesMatchP, matTuplesP] = matchingWithAbs(signalDictionaryMRF, ...
                                              possumToMatch, ...
                                              materialTuples);
indicesMatchP = reshape(indicesMatchP, [Nx,Ny]);
                                          
mapT1P = reshape(matTuplesP.T1, [Nx,Ny]);
mapT2P = reshape(matTuplesP.T2, [Nx,Ny]);
mapPDP = zeros(size(mapT1P));

% % % The proton density map is the scaling factor between the measured
% % % signal and the matched dictionary signal
for i = 1:Nx
    for j = 1:Ny
        % % % The Complex valued 'measured' signal
        % % and the dictionary match
        mapPDP(i,j) = squeeze(abs(imagesPComplex(i,j,:))).' / ...
                      abs(dictionaryMRFComplex(indicesMatchP(i,j),:));

%         if (i == roix(1) && j == roiy(1))
%             figure
%             plot(1:NPulses, squeeze(abs(imagesPComplex(i,j,:))))
%             hold on
%             plot(1:NPulses, abs(dictionaryMRFComplex(indicesMatchP(i,j),:)))
%             legend('possum','dictionary')
%         end
    end
end

mapPDP = abs(mapPDP).*maskForImages(:,:,1);
maskForImages(mapPDP<0.05) = 0;

figure,
subplot(2,3,1)
imagesc(mapT1P.*maskForImages)
colormap hot
colorbar
axis square
title('POSSUM T_1')

subplot(2,3,2)
imagesc(mapT2P.*maskForImages)
colormap hot
colorbar
axis square
title('POSSUM T_2')

subplot(2,3,3)
imagesc(mapPDP.*maskForImages, [0 1])
colormap hot
colorbar
axis square
title('POSSUM PD')


% % % % REAL VALUES
realMRPar         = dlmread('~/simdir/MRparIri');
realMRPar(:, 1:2) = realMRPar(:,1:2).*1000;         % from s to ms

realMapsT1 = zeros(Nx,Ny);         realMapsT2 = zeros(Nx,Ny);
realMapsPD = zeros(Nx,Ny);
% For Nx=Ny=32
% % % % % CSF
realMapsT1( 6: 10,  8: 12) = realMRPar(3,1); 
realMapsT2( 6: 10,  8: 12) = realMRPar(3,2);
realMapsPD( 6: 10,  8: 12) = realMRPar(3,3);
% % % % % WM
realMapsT1(15: 19, 16: 19) = realMRPar(2,1);  
realMapsT2(15: 19, 16: 19) = realMRPar(2,2);  
realMapsPD(15: 19, 16: 19) = realMRPar(2,3);
% % % % % GM
realMapsT1(25: 27, 23: 25) = realMRPar(1,1);  
realMapsT2(25: 27, 23: 25) = realMRPar(1,2);
realMapsPD(25: 27, 23: 25) = realMRPar(1,3);  


subplot(2,3,4)
imagesc(realMapsT1)
colormap hot
colorbar
axis square
title('Real T_1')

subplot(2,3,5)
imagesc(realMapsT2)
colormap hot
colorbar
axis square
title('Real T_2')

subplot(2,3,6)
imagesc(realMapsPD, [0 1])
colormap hot
colorbar
axis square
title('Real PD')

%%
% % % % % % % % % % % Qualitative comparison of images
if toPlotQualitative
    figure
    for i = 1:10
        currentP = squeeze(abs(imagesPComplex(:,:,i))); 
        % % POSSUM - aimagesPbs

        imagesc(currentP, [0 1]), colormap gray
        axis square
        title({'POSSUM', ['image no ', num2str(i)]})
        colorbar


        pause(0.2)
    end
end

