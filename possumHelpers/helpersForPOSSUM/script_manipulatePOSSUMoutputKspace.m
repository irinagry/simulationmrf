% % % % IRINA GRIGORESCU
% % % % This script will manipulate the output of a possum simulation
% % % % 
% % % % Does not work anymore
% % % % 
% % % % 

% Prerequisites
clear all; close all; clc


%% Load files
% filename = '/Users/irina/simdir/outKspace/kspace_';
filenameImg = '/Users/irina/simdir/outImage/image_';
filenameMag = '/Users/irina/simdir/outMagn/brainmagn_';

TRs = 50;

% Allocating memory
kspaceData = struct;
kspaceData.real = zeros(64,64,TRs);
kspaceData.imag = zeros(64,64,TRs);
kspaceData.sumreal = zeros(1,TRs);
kspaceData.sumimag = zeros(1,TRs);

% magnComp  = zeros(3,TRs);
imagesAbs = zeros(1,TRs);

for tr = 1:TRs
    % %%%%%%%% Load data from magnetisation component object
    % x = 73; y = 31; z = 47;
    x = 90; y = 108; z = 90;
%     magnCompLoad = load_nii(...
%             [filenameMag, num2str(tr+1), '.nii.gz'], ...
%             [], [], [], [], [], 0.5);
%     
% 	magnComp(1, tr) = magnCompLoad.img(x,y,z,1);
%     magnComp(2, tr) = magnCompLoad.img(x,y,z,2);
%     magnComp(3, tr) = magnCompLoad.img(x,y,z,3);
        
    % %%%%%%%% Load images image_1_abs.nii.gz % 38/13
    imageAbs = load_nii(...
                    [filenameImg, num2str(tr), '_abs.nii.gz'], ...
                    [], [], [], [], [], 0.5);
	imagesAbs(1,tr) = imageAbs.img(33,33); % middle
	

%     % %%%%%%%% Load kspace images
%     kspaceDataReal = load_nii(...
%             [filename, num2str(tr), '_real.nii.gz'], ...
%               [], [], [], [], [], 0.5);
% 	
% 	kspaceDataImag = load_nii(...
%             [filename, num2str(tr), '_imag.nii.gz'], ...
%               [], [], [], [], [], 0.5);
%           
% 	% Save data in variables
%     kspaceData.real(:,:,tr) = kspaceDataReal.img;
%     kspaceData.imag(:,:,tr) = kspaceDataImag.img;
%     kspaceData.sumreal(1,tr) = sum(sum(kspaceDataReal.img(32,32)));
%     kspaceData.sumimag(1,tr) = sum(sum(kspaceDataImag.img(32,32)));

    disp(['----- DONE WITH BLOCK ', num2str(tr), ...
                        '/', num2str(TRs), ' -----']);
end

%% Plot their absolute value
% just middle kspace
% posx = 33; posy = 33;



signal = sqrt(magnComp(1,:).^2 + magnComp(2,:).^2);

% signal = abs (    kspaceData.sumreal(1,:).^2 + ...
%               1i.*kspaceData.sumimag(1,:).^2);

imageSpace = imagesAbs;


%%
% Compare with MRF simulation
N = TRs;      % timepoints for FAs and TRs of the sequence

% % % % Custom
% % % % 
% % % % Sequence and Material parameters
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial
% % % % % % % %

% % % % Custom Sequence
customSequence.RF.FA  =     45;
customSequence.RF.PhA =      0;
customSequence.TR     =     30;
customSequence.RO     =     30;

% % % % Custom Material
customMaterial.T1     = 600;  %[240 260 600 940 5000];
customMaterial.T2     =  80;  %[ 60  80 100  800];
customMaterial.offRes =   0;   %[0 0.01 -0.001 -0.03 0.04];

tic
[materialProperties, sequenceProperties, ...
    materialTuples, M, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, ...
                 SEQFLAG, customSequence, ...
                 MATFLAG, customMaterial, 0); % 0 flag from no IR
toc


% Create dictionary of signal values (M x N)
tic
signalDictionaryMRF = abs(      squeeze(dictionaryMRF(1:M, :, 1)) + ...
                          1i .* squeeze(dictionaryMRF(1:M, :, 2)));
toc


%% PLOTS
close all


%% PLOT COMPONENT-WISE MAGNETIC VECTOR
figure(1)
col = {'k','r','g'};
for i = 1:3
    % POSSUM x and y compoentns
    plot(1:TRs, magnComp(i,:), ['o-', col{i}])
    hold on
    
    % IRINA x and y components
    plot(1:TRs, squeeze(dictionaryMRF(1:M, :, i)), ['d--',col{i}])
    hold on
    
    pause
end

xlabel('TR index')
ylabel('M_i')
legend('M_x(P)','M_x(I)','M_y(P)','M_y(I)','M_z(P)','M_z(I)')


%% PLOT SIGNAL 
% POSSUM signal
figure(2)
plot(1:TRs, signal, 'o-r')
%plot(1:TRs, imageSpace, 'o-r')
hold on

% IRINA signal
figure(2)
hold on
plot(1:N, signalDictionaryMRF, 'd--g')
xlabel('TR index')
ylabel('Signal')
legend('Possum', 'Irina')



%% Test rotation matrix

figure

createAxis();

mu0 = [0 0 1]';

vec = plot3([0, 0], ...
            [0, 0], ...
            [0, 1], 'r');
tip = scatter3(mu0(1), mu0(2), mu0(3), 'rd');

hold on

mu0 = Rotx(-deg2rad(90)) * mu0;

vec = plot3([0, mu0(1)], ...
            [0, mu0(2)], ...
            [0, mu0(3)], 'r');
tip = scatter3(mu0(1), mu0(2), mu0(3), 'rd');

axis equal
view(143, 15);















