% % % % IRINA GRIGORESCU
% % % % Date created: 05-06-2017
% % % % Date updated: 05-06-2017
% % % %
% % % % This script is meant to open and display the matrix
% % % % file
% % % % Very elementary
% % % % 

clear all
close all


addpath ../helpers/misc/
addpath ../algs/    % Algs functions
addpath ../preprocess/ % Preprocessing steps
addpath ../helpers/ % Helper functions
addpath ~/possumdevirina/

%% Read in the file

fname = '~/possumdevirina/collectedData/matrix';
[m] = read_pulse(fname);


size(m)
figure
for i = 2:19
    subplot(3,6,i-1)
    plot(m(:,1), m(:,i))
end

