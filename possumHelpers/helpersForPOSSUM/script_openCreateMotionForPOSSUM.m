% % % % IRINA GRIGORESCU
% % % % Date created: 29-07-2017
% % % % Date updated: 29-07-2017
% % % %
% % % % This script is meant to open and display the motion file
% % % % 
% % % % Additionally, this script will change the motion file according 
% % % % to different needs
% % % % 

% clear all
% close all

addpath ../src/helpers/
addpath ../src/helpers/misc/
addpath ~/possumdevirina/

toSave = 1;
toCreateOwnMotion = 1;

%% Read file
filename = '~/simdir/motion';


% % Create own motion
if toCreateOwnMotion
    M = zeros(3,7);
    M(2,:) = [0.5 0  0 0 0 0 0];
    M(3,:) = [3   0 -0.05 0 0 0 0];
% % Open already existing motion
else
    M  = dlmread(filename);
end
tp = M(:,1);
Tx = M(:,2);
Ty = M(:,3);
Tz = M(:,4);
Rx = M(:,5);
Ry = M(:,6);
Rz = M(:,7);


% % % % % % % % 
figure,
subplot(1,2,1)
plot(tp, Tx.*1000, 'r'), hold on
plot(tp, Ty.*1000, 'g')
plot(tp, Tz.*1000, 'b')
legend('tx','ty','tz')
xlabel('sec'); ylabel('mm');

subplot(1,2,2)
plot(tp, rad2deg(Rx), 'r'), hold on
plot(tp, rad2deg(Ry), 'g')
plot(tp, rad2deg(Rz), 'b')
legend('rx','ry','rz')
xlabel('sec'); ylabel('deg');



% 
if toSave
    dlmwrite([filename, ''], M, ...
        'delimiter',' ','precision', 5);
end

