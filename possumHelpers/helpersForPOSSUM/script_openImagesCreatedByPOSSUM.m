% % % % IRINA GRIGORESCU
% % % % Date created: 27-07-2017
% % % % Date updated: 27-07-2017
% % % %
% % % % This script is meant to open and display the images
% % % % created with POSSUM
% % % % 

% % % % !!!!!!! Have a look at the comparison code with JEMRIS
% % % %         and dictionary to properly normalize the signals.
% % % %    Needs refactoring.

% clear all
% close all

addpath ../src/helpers/
addpath ../src/helpers/misc/
addpath ../src/test/
addpath ../src/algs/
addpath ../src/preprocess/
addpath ../src/perlin/
addpath ~/possumdevirina/
addpath ~/Tools/MatlabStuff/matlabnifti/

% Test for balanced vs unbalanced
testBalaced = 0;       

% Test against dictionary
testDictionaryMRF = 1; 
plotComparison    = 1;

% Plot images and ROI
plotImagesAndROI  = 1;
videoFlag         = 0; nameVideo = 'bSSFP';

%%
% % % % % % % % % % % LOOK AT multipulse EPI images

% % % % SOME VARIABLES
NPulses = 10; Nx = 32; Ny = Nx;

seqName  = 'epib';
imgSize  = [num2str(Nx), 'x', num2str(Ny)];
npulses  = ['Np', num2str(NPulses)];


% % % % Load Files
images     = zeros(Nx,Nx,NPulses);
imagesNorm = zeros(Nx,Nx,NPulses);
for i = 1:NPulses
    filenameImg = ['~/simdir/outImage/', ...          % folder
                   'image_', ...                      % core name
                    seqName, '_', ...                 % sequence
                    imgSize, '_', ...                 % size of image
                    npulses, '_', ...                 % number of pulses
                    num2str(i)];                      % image number
                
    imageNII = load_nii([filenameImg, '_abs.nii.gz'], ...
                        [], [], [], [], [], 0.5);
	images(:,:,i) = flipud(squeeze(imageNII.img(:,:))');
end

% % % % NORMALIZE IMAGES in the temporal dimension
maxVoxelIntensity = max(max(max(images)));
for i = 1:Nx
    for j = 1:Ny
        imagesNorm(i,j,:) = squeeze(images(i,j,:)) ./ ...
                       norm(squeeze(images(i,j,:)));
    end
end



%%
% % % % % % % % PLOT POSSUM IMAGES AND ROI NORMALISED INTENSITY
% opposite from values from imagesc
% roix = struct; roiy = struct;
% roix  = 16; roiy.wm  = 17; % WM
% roix.gm  = 17; roiy.gm  = 18; % GM
% roix.wm  = 26; roiy.wm  = 24; % WM
% roix.csf =  8; roiy.csf =  9; % CSF

% % Make video if flag set to 1
if videoFlag == 1
    % Make a frame by frame movie
    % need to set this up
    videoClass = VideoWriter([nameVideo, '.mp4'], ...
                             'MPEG-4'); % #need this for video

    frameRate = 25;                        % optional
    videoClass.set('FrameRate',frameRate); % optional
    open(videoClass)                       % #need this for video
end

roix = [ 6, 17, 27]; %[8, 26, 17]; % CSF | WM | GM
roiy = [ 8, 18, 25]; %[9, 24, 18]; % CSF | WM | GM

nLines = 5; nCols = 4; 

titles = {'CSF', 'WM', 'GM'};
colors = 'brg';

if plotImagesAndROI
    figHand = figure('Position', [10,10,1200,800]); % #need this for video
    pause
    % % % (TEMPORAL SIGNAL) Intensity of voxel 
    for j = 1:3 % WM | GM | CSF
        subplot(nLines, nCols, [3+(j-1)*8, 4+(j-1)*8])
        plot(1:NPulses, squeeze(imagesNorm(roix(j), roiy(j), :)), ...
                    colors(j))                            
        hold on
    
%         axis square
        grid on
        title(titles{j})
        xlabel('image number');
        ylabel('normalized intensity of voxel')
        xlim([0 NPulses])
        ylim([0 1])
    end

    for i = 1:NPulses
        % % % (BRAINS)
        subplot(nLines, nCols, [5, 14])

        scalingFactor = 1;
        imageToShow = squeeze(images(:,:,i)./maxVoxelIntensity).*scalingFactor;

        imagesc(imageToShow, [0 scalingFactor])
        axis square
        colormap gray
        colorbar
        title(['Image number ', num2str(i)])

        % % % (TEMPORAL SIGNAL) Intensity of voxel 
        for j = 1:3 % WM | GM | CSF
            subplot(nLines, nCols, [3+(j-1)*8, 4+(j-1)*8])
            
            imageNormalised = squeeze(imagesNorm(:,:,i));
            scatter(i, imageNormalised(roix(j), roiy(j), :), ...
                        colors(j), 'filled')                            
            hold on

        end


        if (i <= 5)
            pause(0.1)
        else
            pause(0.01)
        end
        
        % % Save into video if flag set to 1
        if videoFlag== 1
            frame = getframe(figHand);                 % #need this for video
            writeVideo(videoClass,frame)               % #need this for video
        end
    end
end

% % Close video handle if video flag set to 1
if videoFlag == 1
    close(videoClass)                              % #need this for video
end

%% 
if testDictionaryMRF
    % % Generate dictionary for a predefined set of tissue properties
    N = 10;      % timepoints for FAs and TRs of the sequence

    % % % % Custom

    % % % % Flags
    FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
    SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
    MATFLAG  = 3; % if == 3 need to provide customMaterial

    % % % % Custom Sequence
    FA_mrf  =  [40:5:85]'; %80+10.*rand(50,1); %90;
    PhA_mrf =  zeros(N,1); % 0;
    TR_mrf  =  [50:10:145]'; %50+10.*rand(50,1); %60
    RO_mrf  =  TR_mrf./2;
    
    % % % % Save them in simdir
    dlmwrite('~/simdir/sequenceList', [TR_mrf./1000, FA_mrf], ...
             'delimiter', ' ');

    % % % % Custom Material
    T1_mrf = [950, 600, 4500]; % GM | WM | CSF
    T2_mrf = [100,  80, 2200];
    df_mrf =   0;

    [materialProperties, sequenceProperties, materialTuples, M, ...
     dictionaryMRF, signalDictionaryMRF] = ...
        func_generateCustomSignals(N, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                        T1_mrf, T2_mrf, df_mrf, ...
                        FLAGPLOT, SEQFLAG, MATFLAG);

    
%     for i = 1:M
%         signalDictionaryMRF(i, :) = signalDictionaryMRF(i, :) ./ ...
%                                norm(signalDictionaryMRF(i, :));
%     end
    
    % % % % % % % PLOT COMPARISON
    if plotComparison
        figure('Position', [10,10,1200,800]);
        
        % % % % % % CSF
        subplot(3,1,1)
        plot(1:NPulses, squeeze(imagesNorm(roix(1), roiy(1), :)), ...
            [colors(1),'.-'])
        hold on
        plot(1:NPulses, signalDictionaryMRF(7,:), 'ko--')  % CSF
        grid on
        xlabel('image number');
        ylabel('normalized intensity')
        legend('Possum-WM', 'Dictionary-CSF')
        title('Comparison with POSSUM')

        % % % % % % WM
        subplot(3,1,2)
        plot(1:NPulses, squeeze(imagesNorm(roix(2), roiy(2), :)), ...
            [colors(2),'.-'])
        hold on
        plot(1:NPulses, signalDictionaryMRF(1,:), 'k.--') % WM
        grid on
        xlabel('image number');
        ylabel('normalized intensity');
        legend('Possum-WM', 'Dictionary-WM')

        % % % % % % GM
        subplot(3,1,3)
        plot(1:NPulses, squeeze(imagesNorm(roix(3), roiy(3), :)), ...
            [colors(3),'.-'])
        hold on
        plot(1:NPulses, signalDictionaryMRF(4,:), 'k.--') % GM
        grid on
        xlabel('image number');
        ylabel('normalized intensity')
        legend('Possum-GM', 'Dictionary-GM')
        
    end

end
%%
% % % % % % % % % % % TESTING BALANCED EPI VS NON BALANCED ARE SIMILAR

returnFilename = @(seqname,imgsz,npuls) ...
                   ['~/simdir/outImage/', ...          % folder
                    'image_', ...                      % core name
                     seqname, '_', ...                 % sequence
                     imgsz, '_', ...                   % size of image
                     npuls];                           % number of pulses

imgSize  = '64x64';
npulses  = 'Np1';

if testBalaced
    
    % Load files for balanced
    filenameImgB  = returnFilename('epib', imgSize, npulses);
    filenameImgNB = returnFilename('epi',  imgSize, npulses);                 

    % % % Image 1
    imageAbsPb = load_nii([filenameImgB, '_abs.nii.gz'], ...
                        [], [], [], [], [], 0.5);
    imageAbsPb = flipud(squeeze(imageAbsPb.img(:,:))');

    % % % Image 2
    imageAbsP  = load_nii([filenameImgNB, '_abs.nii.gz'], ...
                        [], [], [], [], [], 0.5);
    imageAbsP  = flipud(squeeze(imageAbsP.img(:,:))');


    % Plot figures
    figure
    subplot(1,3,1)
    imagesc(imageAbsPb)
    title('EPI balanced')
    colormap gray
    colorbar
    axis square

    subplot(1,3,2)
    imagesc(imageAbsP)
    title('EPI')
    colormap gray
    colorbar
    axis square

    subplot(1,3,3)
    imagesc(imageAbsPb - imageAbsP)
    title('Difference')
    colormap gray
    colorbar
    axis square
end
