% % % % IRINA GRIGORESCU
% % % % Date created: 09-06-2017
% % % % Date updated: 09-06-2017
% % % %
% % % % This script is meant to open and display the generated signal file
% % % % 
% % % % 

%clear all
close all

addpath ../simulationMRF/src/helpers/
addpath ../simulationMRF/src/helpers/misc/
addpath ~/possumdevirina/

%% Read file             100RF | 50RF | onlyRO
TRs = 100;

signal  = read_pulse(['~/simdir/outSignal/signal_', num2str(TRs)]);
signalO = read_pulse(['~/simdir/outSignal/signal_', num2str(TRs), 'orig']);

signalAbs  =  abs(signal(1,:) + 1i.*signal(2,:));
signalAbsO =  abs(signalO(1,:).^2 + 1i.*signalO(2,:));

TRs = 100;
plot(1:TRs, signalAbs(1:TRs), 'k--o'), hold on
% plot(1:TRs, signalAbsO(1:TRs), 'g--o'), hold on


% plot(1:TRs, signal(1, 1:TRs), 'r'), hold on
% plot(1:TRs, signal(2, 1:TRs), 'r--'), hold on

xlabel('number of RO points')
ylabel('signal magnitude')


%% Compare with MRF simulation
% % N = TRs;      % timepoints for FAs and TRs of the sequence
% % 
% % % % % % Custom
% % % % % % 
% % % % % % Sequence and Material parameters
% % FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
% % SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
% % MATFLAG  = 3; % if == 3 need to provide customMaterial
% % % % % % % % % %
% % 
% % % % % % Custom Sequence
% % customSequence.RF.FA  =     90;
% % customSequence.RF.PhA =      0;
% % customSequence.TR     =     60;
% % customSequence.RO     =     30;
% % 
% % % % % % Custom Material
% % customMaterial.T1     = 600;  %[240 260 600 940 5000];
% % customMaterial.T2     =  80;  %[ 60  80 100  800];
% % customMaterial.offRes =   0;   %[0 0.01 -0.001 -0.03 0.04];
% % 
% % tic
% % [materialProperties, sequenceProperties, ...
% %     materialTuples, M, ...
% %     dictionaryMRF] = ...
% %                  simulateMRF(N, FLAGPLOT, ...
% %                  SEQFLAG, customSequence, ...
% %                  MATFLAG, customMaterial, 0); % 0 flag from no IR
% % toc
% % 
% % 
% % % Create dictionary of signal values (M x N)
% % tic
% % signalx = squeeze(dictionaryMRF(1:M, :, 1));
% % signaly = squeeze(dictionaryMRF(1:M, :, 2));
% % signalDictionaryMRF = abs(      squeeze(dictionaryMRF(1:M, :, 1)) + ...
% %                           1i .* squeeze(dictionaryMRF(1:M, :, 2)));
% % toc
% % 
% % hold on
% % plot(1:N, signaly);
% % 
% % legend('possum','iri')








