% % % % IRINA GRIGORESCU
% % % % Creates an object to be given as input to POSSUM
% % % % You can customize it as you wish


% Prerequisites
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/helpers/'))
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/possumHelpers/'))
addpath(genpath('~/Tools/MatlabStuff/matlabnifti/'))

toSave = 0;
toCreateANewImage = 0;

%% Load file
filename = '~/OneDrive - University College London/Work/POSSUMsimulations/simdir/brainWM.nii.gz';
iniData = load_nii(filename, ...
              [], [], [], [], [], 0.5);

sizeOfData = iniData.original.hdr.dime.dim(2:5);

%% Create a new object resembling the phantom from Mariya
% Size of enclosing box in every direction
[m,n,p,q] = size(iniData.img);
n = m; p = m;

% % Remember all centres and radii of all circles
centres = zeros(13,2); 
radii   = zeros(13,1); 

% % Radius of big circle
R = 76;
posx =  91; 
posy =  91; 
posz =  91; 
th = 0:pi/50:2*pi;
yunitBig = R * cos(th) + posx;
xunitBig = R * sin(th) + posy;
centres(13,1) = posy; centres(13,2) = posx;
radii(13,1)   = R; 

% % Radius of small circles
r = 10;
% 1. 2 circles from the above
% Circle1
centres(1,2) = posx - R/2 - R/4 + r/2; 
centres(1,1) = posy - R/4;
radii(1,1)   = r; 
yunit1 = radii(1,1) * cos(th) + centres(1,2); 
xunit1 = radii(1,1) * sin(th) + centres(1,1);
% Circle2
centres(2,2) = posx - R/2 - R/4 + r/2; 
centres(2,1) = posy + R/4;
radii(2,1)   = r; 
yunit2 = radii(2,1) * cos(th) + centres(2,2); 
xunit2 = radii(2,1) * sin(th) + centres(2,1);

% 2. 4 circles from above middle line
%Circle3
centres(3,2) = posx - R/4; 
centres(3,1) = posy - R/2 - R/4 + r/2;
radii(3,1)   = r; 
yunit3 = radii(3,1) * cos(th) + centres(3,2); 
xunit3 = radii(3,1) * sin(th) + centres(3,1);
%Circle4
centres(4,2) = posx - R/4; 
centres(4,1) = posy - R/4;
radii(4,1)   = r; 
yunit4 = radii(4,1) * cos(th) + centres(4,2); 
xunit4 = radii(4,1) * sin(th) + centres(4,1); 
%Circle5
centres(5,2) = posx - R/4; 
centres(5,1) = posy + R/4;
radii(5,1)   = r; 
yunit5 = radii(5) * cos(th) + centres(5,2); 
xunit5 = radii(5) * sin(th) + centres(5,1);
%Circle6
centres(6,2) = posx - R/4; 
centres(6,1) = posy + R/2 + R/4 - r/2;
radii(6,1)   = r; 
yunit6 = radii(6,1) * cos(th) + centres(6,2); 
xunit6 = radii(6,1) * sin(th) + centres(6,1);

% 3. 4 circles from below middle line
%Circle7
centres(7,2) = posx + R/4; 
centres(7,1) = posy - R/2 - R/4 + r/2;
radii(7,1)   = r; 
yunit7 = radii(7,1) * cos(th) + centres(7,2); 
xunit7 = radii(7,1) * sin(th) + centres(7,1); 
%Circle8
centres(8,2) = posx + R/4; 
centres(8,1) = posy - R/4;
radii(8,1)   = r; 
yunit8 = radii(8,1) * cos(th) + centres(8,2); 
xunit8 = radii(8,1) * sin(th) + centres(8,1); 
%Circle9
centres(9,2) = posx + R/4; 
centres(9,1) = posy + R/4;
radii(9,1)   = r; 
yunit9 = radii(9,1) * cos(th) + centres(9,2); 
xunit9 = radii(9,1) * sin(th) + centres(9,1);
%Circle10
centres(10,2) = posx + R/4; 
centres(10,1) = posy + R/2 + R/4 - r/2;
radii(10,1)   = r; 
yunit10 = radii(10,1) * cos(th) + centres(10,2);
xunit10 = radii(10,1) * sin(th) + centres(10,1);

% 4. 2 circles from below
%Circle10
centres(11,2) = posx + 3*R/4 - r/2; 
centres(11,1) = posy - R/4;
radii(11,1)   = r; 
yunit11= radii(11,1) * cos(th) + centres(11,2); 
xunit11= radii(11,1) * sin(th) + centres(11,1);
%Circle12
centres(12,2) = posx + 3*R/4 - r/2; 
centres(12,1) = posy + R/4;
radii(12,1)   = r; 
yunit12= radii(12,1) * cos(th) + centres(12,2); 
xunit12= radii(12,1) * sin(th) + centres(12,1);

% To make a hollow circle of zeros
radiiHollow = radii + 2;

% Plot the data
figure,
% 1. Plot the middle slice
Slice = squeeze(iniData.img(1:m,1:n,posz,1));
imagesc(Slice), hold on
% 2. Plot the big circle
h  = plot(xunitBig, yunitBig, 'k');
% 3. Cirles on the first line
h1 = plot(xunit1, yunit1, 'k');
h2 = plot(xunit2, yunit2, 'k');
% 4. Cirles above the middle line
h3 = plot(xunit3, yunit3, 'k');
h4 = plot(xunit4, yunit4, 'k');
h5 = plot(xunit5, yunit5, 'k');
h6 = plot(xunit6, yunit6, 'k');
% 5. Cirles above the middle line
h7 = plot(xunit7, yunit7, 'k');
h8 = plot(xunit8, yunit8, 'k');
h9 = plot(xunit9, yunit9, 'k');
h10= plot(xunit10, yunit10, 'k');
% 6. Cirles on the first line
h11 = plot(xunit11, yunit11, 'k');
h12 = plot(xunit12, yunit12, 'k');
% Labels
xlabel(num2str(n)); ylabel(num2str(m));
axis equal

% % % % % % Create Masks
masks = zeros(m,n,p,13);
masks2D_T1 = zeros(m,n,13);
masks2D_T2 = zeros(m,n,13);
% All small circles
maskAllCircles = createCirclesMask(Slice, ...
                                   centres(1:end-1,:), radii(1:end-1,1));
% The big circle
maskBigCircle  = createCirclesMask(Slice, ...
                                   centres(end,:), radii(end,1));
% The no value circles around the filled value circles
maskHollow = createCirclesMask(Slice, ...
                               centres(1:end-1,:), radiiHollow(1:end-1,1));
maskHollow = -(maskHollow-maskAllCircles)+1;
                               
% Read in the MR parameters
mrparphantom = dlmread('~/OneDrive - University College London/Work/POSSUMsimulations/simdirWithJEMRISseq/MRparPhantomAll13').*1000;
                               
% The big circle without the small circles
masks(:,:,posz,1) = maskBigCircle - maskAllCircles;
masks2D_T1(:,:,1) = (maskBigCircle - maskAllCircles).*mrparphantom(1,1).*maskHollow;
masks2D_T2(:,:,1) = (maskBigCircle - maskAllCircles).*mrparphantom(1,2).*maskHollow;
for i = 2 : 13
    masks(:,:,posz,i) = createCirclesMask(Slice, ...
                                   centres(i-1,:), radii(i-1));
	
    % Save T1 and T2 relaxation parameters from the file
    masks2D_T1(:,:,i) = createCirclesMask(Slice, ...
                                   centres(i-1,:), radii(i-1)) .* ...
                        mrparphantom(i,1);
    masks2D_T2(:,:,i) = createCirclesMask(Slice, ...
                                   centres(i-1,:), radii(i-1)) .* ...
                        mrparphantom(i,2);
end

figure
for i = 1:13
    imagesc(squeeze(masks(:,:,posz,i)))
    axis equal
    pause %(0.1)
end



%%
figure
subplot(1,2,1)
imagesc(fliplr(rot90(sum(masks2D_T1,3))))
axis off
colormap hot
caxis([0 1700])
axis square
colorbar
title('T_1')

subplot(1,2,2)
imagesc(fliplr(rot90(sum(masks2D_T2,3))))
axis off
colormap hot
caxis([0 400])
axis square
colorbar
title('T_2')

% % % % % % Save phantom
outputData = iniData;
outputData.img = zeros(size(iniData.img));
outputData.fileprefix = '~/OneDrive - University College London/Work/POSSUMsimulations/simdir/phantomAll13.nii.gz';


outputData.hdr.dime.dim(5) =  13;             % temporal dim = 13
outputData.hdr.dime.dim(3) = 181;             %        y dim = 181
outputData.original.hdr.dime.dim(5) =  13;    % temporal dim = 13
outputData.original.hdr.dime.dim(3) = 181;    %        y dim = 13
outputData.img = masks(:,:,:,1:13);
% SAVE
% % % save_nii(outputData, outputData.fileprefix);



%% Create a new object called 3circles.nii.gz
% % which will be a rectangle of dx x dy x dz in the middle

outputData = iniData;
outputData.fileprefix = '~/OneDrive - University College London/Work/POSSUMsimulations/simdir/3circles.nii.gz';

outputData.img = ones(96,96,3,3);
outputData.hdr.dime.dim          = [4 96 96 3 3 1 1 1];
outputData.original.hdr.dime.dim = [4 96 96 3 3 1 1 1];

figure,
for i = 1
    imagesc(squeeze(outputData.img(:,:,i,2)));
    axis equal
    title(['slice ', num2str(i), ' / ' , num2str(p)])
    pause(1)
end

% SAVE
save_nii(outputData, '~/OneDrive - University College London/Work/POSSUMsimulations/simdir/3circles.nii.gz');




%% Create a new object called brainWM_6mmthick.nii.gz
% % which will be a rectangle of dx x dy x dz in the middle

outputData = iniData;
outputData.img = zeros(size(iniData.img));
outputData.fileprefix = '~/simdir/brainWM1mmthick.nii.gz';

[m,n,p,q] = size(outputData.img);
% size of box in every direction
dx = -1; dy = 1; dz = 1;
% put it in the middle
posx =  88; %floor((m-dx)/2)+1;
posy = 109; %floor((n-dy)/2)+1;
posz =  91; %floor((p-dz)/2)+4;
outputData.img(posx:-1:posx+dx+1, ...
               posy:posy+dy-1, ...
               posz:posz+dz-1,  2) = 1;

figure,
for i = posz-1:posz+dz
    imagesc(squeeze(outputData.img(:,:,i,2)))
    axis equal
    title(['slice ', num2str(i), ' / ' , num2str(p)])
    pause(1)
end

% SAVE
save_nii(outputData, '~/simdir/brainWM1mmthick.nii.gz');



% % % %% See brain
% % % brainTissues = {'GM', 'WM', 'CSF'};
% % % for i = 1:sizeOfData(4)
% % %     plotWithMontage(squeeze(outputData.img(:,:,:, i)), ...
% % %                     sizeOfData(1), ...
% % %                     sizeOfData(2), ...
% % %                     sizeOfData(3), ...
% % %                     15);
% % % 	title(brainTissues{i});
% % %     pause(5)
% % % end

%% Create an object for JEMRIS
GM = struct; WM = struct; CSF = struct;

GM.T1  = squeeze(iniData.img( :, :, :, 1)).*950;
WM.T1  = squeeze(iniData.img( :, :, :, 2)).*600;
CSF.T1 = squeeze(iniData.img( :, :, :, 3)).*4500;

GM.T2  = squeeze(iniData.img( :, :, :, 1)).*100;
WM.T2  = squeeze(iniData.img( :, :, :, 2)).*80;
CSF.T2 = squeeze(iniData.img( :, :, :, 3)).*2200;

GM.T2s  = squeeze(iniData.img( :, :, :, 1)).*0;
WM.T2s  = squeeze(iniData.img( :, :, :, 2)).*0;
CSF.T2s = squeeze(iniData.img( :, :, :, 3)).*0;

GM.M0  = squeeze(iniData.img( :, :, :, 1)).*1;
WM.M0  = squeeze(iniData.img( :, :, :, 2)).*1;
CSF.M0 = squeeze(iniData.img( :, :, :, 3)).*1;

GM.DB  = squeeze(iniData.img( :, :, :, 1)).*0;
WM.DB  = squeeze(iniData.img( :, :, :, 2)).*0;
CSF.DB = squeeze(iniData.img( :, :, :, 3)).*0;

load('/usr/local/share/jemris/matlab/userSample.mat')

T1 = GM.T1 + WM.T1 + CSF.T1;
T2 = GM.T2 + WM.T2 + CSF.T2;
T2s = GM.T2s + WM.T2s + CSF.T2s;
M0 = GM.M0 + WM.M0 + CSF.M0;
DB = GM.DB + WM.DB + CSF.DB;

save('~/jemrisSims/mySequences/myUserSample', ...
     'T1','T2','T2s','M0','DB','OFFSET','RES');

%% Create an image with several circles for POSSUM
if toCreateANewImage
    phantom = zeros(sizeOfData(1), sizeOfData(2));

    BW = BW1+BW2+BW3;
    figure, imshow(rot90(BW',2));
    figure, imshow(BW1);

    GMphantom  = phantom+BW1; % 1
    WMphantom  = phantom+BW2; % 2
    CSFphantom = phantom+BW3; % 3

    outputData = iniData;
    outputData.img = zeros(sizeOfData);
    outputData.img( :, :, 91, 1) = GMphantom;
    outputData.img( :, :, 91, 2) = WMphantom;
    outputData.img( :, :, 91, 3) = CSFphantom;
    if toSave
        outputData.fileprefix = '~/simdir/brainWMGMCSF.nii.gz';
        save_nii(outputData, '~/simdir/brainWMGMCSF.nii.gz');
    end

    T1 = [950, 600, 4500]; % gm wm csf
    T2 = [100,  80, 2200]; 

    gmT1  = GMphantom  .* T1(1);
    gmT2  = GMphantom  .* T2(1);
    wmT1  = WMphantom  .* T1(2);
    wmT2  = WMphantom  .* T2(2);
    csfT1 = CSFphantom .* T1(3);
    csfT2 = CSFphantom .* T2(3);
end
%%
figure, 
subplot(1,2,1)
imagesc(rot90((gmT1+wmT1+csfT1)',2))
colormap parula
colorbar
axis square
title('T_1')

subplot(1,2,2)
imagesc(rot90((gmT2+wmT2+csfT2)',2))
colormap parula
colorbar
axis square
title('T_2')


%% Create empty 4D volume of the same size and properties as the 
%  loaded file, but with just one isochromat that is of one type
% x = 90; y = 108; z = 90; % Chosen position (not centered)
x = 91; y = 109; z = 91; % Chosen position (centered)
% Difference between frequency at voxel centre 
% and centre frequecy of RF pulse: 0.0027777577673334975794

outputData = iniData;

% Creates a 4D phantom with just one tissue type at a hardcoded position
outputData.img = zeros(sizeOfData);
outputData.img( x, y, z, 2) = 1;                  
outputData.fileprefix = '~/simdir/brainWM.nii.gz';

if toSave
    save_nii(outputData, '~/simdir/brainWM.nii.gz');
end







