% % % % IRINA GRIGORESCU
% % % % Date created: 04-06-2017
% % % % Date updated: 04-06-2017
% % % %
% % % % This script is meant to open and display the pulse
% % % % sequence file
% % % % 
% % % % Additionally, this script will change the pulse according 
% % % % to different needs
% % % % 

% clear all
% close all 

addpath(genpath('../src/helpers/'))
addpath ~/possumdevirina/

changeThePulseSequence = 0; % set to 1 if you want to change the pulse seq
WRITEFLAG = 0;              % set to 1 if you want to save the new pulse

%% Read file
% pulseSequence = read_pulse('~/simdir/pulseEPI32');
pulseSequenceActual = read_pulse('~/simdir/outPulses/pulse_epif_WM_64x64_Np2');
pulseSequence = pulseSequenceActual;


%% PLOT
[N, M] = size(pulseSequence);

% % % % NO GZ
% pulseSequence(:,8) = zeros(N,1);
% write_pulse('~/simdir/outPulses/pulse_epib_32x32_Np10_noGz', pulseSequence, 1);
% % % % END NO GZ

% TE from readme file
TE = 0.005; 

% Find TE
for j = 1:N
    if (pulseSequence(j,1) - TE) < 1e-05
        TEidx = j;
    end
end

titles = {'time (s)', ...
          'RF angle (deg)', ...
          'RF freq bw (Hz)', ...
          'RF centre freq (Hz)', ...
          'RO', ...
          'Gx (T/m)', ...
          'Gy (T/m)', ...
          'Gz (T/m)'};
indices = [2,6,3,7,4,8,5];
startIdx =   1;%321;%  1;%1;
endIdx   =   N;%N;%321;%N; %200;


figure
hold on
for i = 1:7
    subplot(4,2,i)
    
    % Plot each subplot
    % If 1 change radians to degrees
    if i == 1
        plot(pulseSequence(startIdx:endIdx,1), ...
             rad2deg(pulseSequence(startIdx:endIdx, indices(i))))
         
        hold on
    
%         % Plot TE
%         plot(repmat(pulseSequence(TEidx,1), [50 1]), ...
%             linspace(min(rad2deg(pulseSequence(:, indices(i)))), ...
%                      max(rad2deg(pulseSequence(:, indices(i)))), 50), '--');
	
    end
    if indices(i) == 5
        stem(pulseSequence(startIdx:endIdx,1), ...
             pulseSequence(startIdx:endIdx, indices(i)), 'r.')
	% Else leave as it is
    else
        plot(pulseSequence(startIdx:endIdx,1), ...
             pulseSequence(startIdx:endIdx, indices(i)))
             
        hold on
    
%         % Plot TE
%         plot(repmat(pulseSequence(TEidx,1), [50 1]), ...
%             linspace(min(pulseSequence(:, indices(i))), ...
%                      max(pulseSequence(:, indices(i))), 50), '--');
    end 
    
    % Labels
    xlabel('time (s)')
    ylabel(titles{indices(i)})
end
% Last subplot is made up of all gradients put together
subplot(4,2,8)
plot(pulseSequence(startIdx:endIdx,1), pulseSequence(startIdx:endIdx, 6))
hold on
plot(pulseSequence(startIdx:endIdx,1), pulseSequence(startIdx:endIdx, 7))
hold on
plot(pulseSequence(startIdx:endIdx,1), pulseSequence(startIdx:endIdx, 8))
hold on
xlabel('time (s)')
ylabel('G (T/m)')
legend('G_x', 'G_y', 'G_z')

max(pulseSequence(:, 8))
min(pulseSequence(:, 8))


%% Change the pulse sequence
pulseSequenceActual = read_pulse('~/simdir/outPulses/pulse_epif_WM_64x64_Np2');
pulseSequence = pulseSequenceActual;
pulseSequence2 = pulseSequenceActual;

% Clear all Gradients except GZ
pulseSequence2(:, 5)   = 0;
pulseSequence2(:, 6)   = 0;
pulseSequence2(:, 7)   = 0;
pulseSequence2([1:7,4307:4313], 8) = 0;

% Keep only the important bits
pulseSequence = [pulseSequence2(1:5,:) ; pulseSequence2(4302:4306,:)];
rfPulseTime = pulseSequence(3,1);
pulseSequence(6,1) = 0.0385;
pulseSequence(6,5) = 1; % Readout
pulseSequence(:,6) = pulseSequence(:,8); %0.002935650540159699 - jemris
pulseSequence(:,8) = 0;                  %0.002599624451250    - possum

% Change GX amplitude if you wish
pulseSequence(pulseSequence(:,6)~=0,6) =  0.015641146077971; % 0.001;
% pulseSequence(pulseSequence(:,6)~=0,7) =  0.015656802880852;
                       % 0.011742602160638800;
                       % 0.015656802880852/10;
                       % 1.739644764539082e-04;
                       % 0.015656802880852l
                       %0.001304733573404;
                       %0.015656802880852; %0.011742602160638800;
                       %0.015656802880852; %0.005218934293617; %

% Change timings
TE = 0.005;
TR = 0.015;
pulseSequence( 6,1) = TE;
pulseSequence( 7,1) = TR-0.0020; %-0.0020; % To make square gradient
pulseSequence( 8,1) = TR-0.0015;
pulseSequence( 9,1) = TR-0.0005;  %just TR here
pulseSequence(10,1) = TR;

% Repeat sequence how many times you want
pulseSequenceNew = pulseSequence;
for np = 1:19
    pulseSequenceTemp      =  pulseSequence;
    pulseSequenceTemp(:,1) = pulseSequenceTemp(:,1) + TR*np;
    pulseSequenceNew  = [pulseSequenceNew ; pulseSequenceTemp];
end
pulseSequence = pulseSequenceNew;
% 
write_pulse('~/simdir/outPulses/pulse_epif_WM_64x64_Np20', pulseSequence, 1);

%% Calculate moments of gradients:
% % Gradients
tp     = pulseSequence(:, 1);
Gxgrad = pulseSequence(:, 6);
Gygrad = pulseSequence(:, 7);
Gzgrad = pulseSequence(:, 8);

% % Apply trapz for Gx
momentGx         =    trapz(tp, Gxgrad);
cumMomentZeroGx  = cumtrapz(tp, Gxgrad);
cumMomentFirstGx = cumtrapz(tp, Gxgrad.*tp);


% % Apply trapz for Gy
momentGy         =    trapz(tp, Gygrad);
cumMomentZeroGy  = cumtrapz(tp, Gygrad);
cumMomentFirstGy = cumtrapz(tp, Gygrad.*tp);


% % Apply trapz for Gz
momentGz         =    trapz(tp, Gzgrad);
cumMomentZeroGz  = cumtrapz(tp, Gzgrad);
cumMomentFirstGz = cumtrapz(tp, Gzgrad.*tp);


figure,
% % % % % Gx
subplot(3,3,1)
plot(tp, Gxgrad, 'b', 'LineWidth', 1); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('G_x (T/m)');
title('Gradient waveform')
% % 
subplot(3,3,2)
plot(tp, cumMomentZeroGx, 'b'); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('\int_0^{t} G_x t'' dt'' (Ts/m)');
title('Zeroth order gradient moment')
% % 
subplot(3,3,3)
plot(tp, cumMomentFirstGx, 'b'); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('\int_0^{t} t G_x t'' dt'' (Ts^2/m)');
title('First order gradient moment')

% % % % % Gy
subplot(3,3,4)
plot(tp, Gygrad, 'b', 'LineWidth', 1); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('G_y (T/m)');
% % 
subplot(3,3,5)
plot(tp, cumMomentZeroGy, 'b'); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('\int_0^{t} G_y t'' dt'' (Ts/m)');
% % 
subplot(3,3,6)
plot(tp, cumMomentFirstGy, 'b'); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('\int_0^{t} t G_y t'' dt'' (Ts^2/m)');

% % % % % Gz
subplot(3,3,7)
plot(tp, Gzgrad, 'b', 'LineWidth', 1); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('G_z (T/m)');
% % 
subplot(3,3,8)
plot(tp, cumMomentZeroGz, 'b'); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('\int_0^{t} G_z t'' dt'' (Ts/m)');
% % 
subplot(3,3,9)
plot(tp, cumMomentFirstGz, 'b'); hold on
plot(tp, zeros(1,size(tp,1)), 'k--'); 
grid on
xlabel('time (s)'); ylabel('\int_0^{t} t G_z t'' dt'' (Ts^2/m)');

% % %% Change the pulse sequence to have no Gz and no Gy,
% % % % but 1 RF pulse and read out
% % 
% % [N, M] = size(pulseSequence);
% % pulseSequenceNew = zeros(N, M);
% % 
% % % % 1. timepoints
% % pulseSequenceNew(:, 1) = linspace(0, 0.06, N);
% % 
% % % % 2. RF angle
% % pulseSequenceNew(3, 2) = deg2rad(45);
% % 
% % % % 3. RF frequency BW
% % pulseSequenceNew(:, 3) = pulseSequence(:, 3);
% % 
% % % % 4. RF centre frequency 
% % pulseSequenceNew(:, 4) = pulseSequence(:, 4);
% % 
% % % % 5. Signal Read Out
% % pulseSequenceNew(10 : 10 + (64*64) - 1, 5) = 1; 
% % 
% % % % Sampling time 
% % Ts = pulseSequenceNew(10 + (64*64) - 1, 1) - pulseSequenceNew(10, 1);
% % dt  = 1.3950e-05;
% % dt2 = dt*500;
% % 
% % % % 6. Gx
% % pulseSequenceNew(10 : 10 + (64*64) - 1, 6) = 0; %0.01;     %(T/m) RO
% % 
% % % % % Repeat to have 2 TRs
% % % pulseSequenceNew = [pulseSequenceNew ; pulseSequenceNew];
% % % pulseSequenceNew(:, 1) = linspace(0, 0.06*2, 2*N);



%% CHANGING THE PULSE SEQUENCE
if changeThePulseSequence
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Change the pulse sequence to have 50 RF pulses

    % % % pulseSequence
    % % % 
    % % % tp      | RF (rad) | RF BW        | RF ctrfreq | RO | Gx | Gy | Gz
    % % %  0        0           0                 0        0     0    0    0
    % % % 0.000028  0           0                 0        0     0    0    0.00712799979373813
    % % % 0.002028  1.570   30351.0253906250 -12292.16     0     0    0    0.00712799979373813

    % % % 
    % % % 
    TRs = 3; % number of TRs that you want

    [N, M] = size(pulseSequence);
    % % IF i want no gradients, just RF + RO
    pulseSequenceNew = zeros(2*TRs+1, M); % 1 + 2 times number of blocks
                                          % for each TR block we need RF pulse
                                          % line and RO line

    % % 1. timepoints
    pulseSequenceNew(:, 1) = zeros(1,2*TRs+1); % zeroes for now

    % % 2. RF angle
    pulseSequenceNew(2:2:end, 2) = deg2rad(FA_mrf(1:TRs)); % every other line

    % % 3. RF frequency BW (copy from pulseSequence)
    pulseSequenceNew(2:2:end, 3) = repmat(pulseSequence(3, 3), 1, TRs);

    % % 4. RF centre frequency (copy from pulseSequence)
    pulseSequenceNew(2:2:end, 4) = repmat(pulseSequence(3, 4), 1, TRs);

    % % 5. Signal Read Out
    pulseSequenceNew(3:2:end, 5) = 1; % instead of N/2, do readout immediately after
                                      % so rf is at 3 and ro is at 4
    dt = 0.0001; % seconds
    % % Add the timepoints
    for i = 2:2:(2*TRs+1)
        % Add the different time steps for each RF Pulse 
        if i == 2
            pulseSequenceNew(i,1)   = dt;             % RF pulse
            pulseSequenceNew(i+1,1) = dt + RO_mrf(1)./1000; % RO (make it sec)
        else
            pulseSequenceNew(i,1)   = dt + sum(TR_mrf(1:((i/2)-1)))./1000; % RF
            pulseSequenceNew(i+1,1) = dt + sum(TR_mrf(1:((i/2)-1)))./1000 + ...
                                           RO_mrf(i/2)./1000; % RO (make it sec)
        end
    end


    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Figure with the new pulse sequence
    endIdx = size(pulseSequenceNew,1);

    titles = {'time (s)', ...
              'RF angle (deg)', ...
              'RF freq bw (Hz)', ...
              'RF centre freq (Hz)', ...
              'RO', ...
              'Gx (T/m)', ...
              'Gy (T/m)', ...
              'Gz (T/m)'};
    indices = [2,8,7,6,5,3,4];


    figure
    for i = 1:7

        subplot(5,2,i);

        % Plot each subplot
        % If 1 change radians to degrees
        if i == 1
            stem(pulseSequenceNew(startIdx:endIdx,1), ...
                 rad2deg(pulseSequenceNew(startIdx:endIdx, indices(i))), '.')

            hold on

        % Else leave as it is
        else
            stem(pulseSequenceNew(startIdx:endIdx,1), ...
                 pulseSequenceNew(startIdx:endIdx, indices(i)), '.')

            hold on

        end 

        % If it's a gradient
        if (indices(i) == 6 || indices(i) == 7 || indices(i) == 8)
            ylim([0,0.05])
        end

        % Labels
        xlabel('time (s)')
        ylabel(titles{indices(i)})
    end

    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Save the pulse
    % It's not balanced on Gz, maybe not even on Gx or Gy, so look into that
    if WRITEFLAG == 1
        % % This pulse was for N TR blocks with equidistant timings 
    % %     write_pulse(['~/simdir/pulseROmid', num2str(TRs)], pulseSequenceNew, 1);
        write_pulse(['~/simdir/pulseRND_1_', num2str(TRs)], pulseSequenceNew, 1);
    end

    if WRITEFLAG == 1
       SeqType = 1;
       TE  = pulseSequenceNew(3,1); % RO is found in the event matrix 
                                    % immediately after RF pulse
       TR  = pulseSequenceNew(4,1);
       TRslc = pulseSequenceNew(end,1);
       Nx = 32; 
       Ny = 32; 
       dx = 0.001000000047; 
       dy = 0.001000000047; 
       maxG = 0.0549999997;
       RiseT = 0.0002200000017;
       BW = 100000;
       Nvol = 1; 
       Nslc = 1;
       SlcThk = 0.1000000015;
       SlcDir = 1;
       Gap = 0;
       zstart = 0;
       FA = 45;
       phasedir = 2;
       readdir = 3;
       kspace_coverage = 100;
       startkspace = 1;

       fileID = fopen(['~/simdir/pulseRND_1_', num2str(TRs), '.info'],'w');
       fprintf(fileID, '%.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f\n', ...
           SeqType, TE, TR, TRslc, Nx, Ny, dx, dy, ... % 8
           maxG, RiseT, BW, Nvol, Nslc, SlcThk, SlcDir, Gap, ... % 8
           zstart, FA, phasedir, readdir, kspace_coverage, startkspace); % 6
    end
end % end if changeThePulseSequence
 
% % % %% Change the pulse sequence to make it balanced
% % % %
% % % % % % % Gx
% % % idxToDisplay = 6;
% % % pulseSequenceNew = pulseSequence;
% % % 
% % % figure
% % % % Plot Gx
% % % plot(pulseSequenceNew(startIdx:endIdx,1), ...
% % %      pulseSequenceNew(startIdx:endIdx, idxToDisplay))
% % %              
% % % hold on  
% % % % Plot TE
% % % plot(repmat(pulseSequenceNew(TEidx,1), [50 1]), ...
% % %      linspace(min(pulseSequenceNew(:, idxToDisplay)), ...
% % %               max(pulseSequenceNew(:, idxToDisplay)), 50), '--');
% % % % Labels
% % % xlabel('time (s)')
% % % ylabel(titles{idxToDisplay})
% % % 
% % % % % Change Gx it to be balanced
% % % dtp1p2 = pulseSequenceNew(11, 1) - pulseSequenceNew(10, 1);
% % % dtp2p3 = pulseSequenceNew(12, 1) - pulseSequenceNew(11, 1);
% % % pulseSequenceNew(4299, 6) = - pulseSequenceNew(11, 6);
% % % pulseSequenceNew(4300, 6) =   0;
% % % pulseSequenceNew(4301, 6) =   0;
% % % pulseSequenceNew(4299, 1) = pulseSequenceNew(4298, 1) + dtp1p2;
% % % pulseSequenceNew(4300, 1) = pulseSequenceNew(4299, 1) + dtp2p3;
% % % 
% % % % Plot changed Gx
% % % plot(pulseSequenceNew(startIdx:endIdx,1), ...
% % %      pulseSequenceNew(startIdx:endIdx, idxToDisplay), '--')
% % % 
% % % 
% % % %%
% % % % % % % Gy
% % % idxToDisplay = 7;
% % % 
% % % figure
% % % % Plot Gy
% % % plot(pulseSequence(startIdx:endIdx,1), ...
% % %      pulseSequence(startIdx:endIdx, idxToDisplay))
% % %              
% % % hold on  
% % % % Plot TE
% % % plot(repmat(pulseSequenceNew(TEidx,1), [50 1]), ...
% % %      linspace(min(pulseSequenceNew(:, idxToDisplay)), ...
% % %               max(pulseSequenceNew(:, idxToDisplay)), 50), '--');
% % % % Labels
% % % xlabel('time (s)')
% % % ylabel(titles{idxToDisplay})
% % % 
% % % % % Change Gy it to be balanced
% % % pulseSequenceNew(4299, 7) = - pulseSequenceNew(9, 7);
% % % pulseSequenceNew(4300, 7) =   0;
% % % pulseSequenceNew(4301, 7) =   0;
% % % 
% % % % Plot changed Gy
% % % plot(pulseSequenceNew(startIdx:endIdx,1), ...
% % %      pulseSequenceNew(startIdx:endIdx, idxToDisplay), '--')
% % % 
% % % 
% % % 
% % % %%
% % % % % % % Gz
% % % idxToDisplay = 8;
% % % 
% % % figure
% % % % Plot Gz
% % % plot(pulseSequence(startIdx:endIdx,1), ...
% % %      pulseSequence(startIdx:endIdx, idxToDisplay))
% % %              
% % % hold on  
% % % % Plot TE
% % % plot(repmat(pulseSequenceNew(TEidx,1), [50 1]), ...
% % %      linspace(min(pulseSequenceNew(:, idxToDisplay)), ...
% % %               max(pulseSequenceNew(:, idxToDisplay)), 50), '--');
% % % % Labels
% % % xlabel('time (s)')
% % % ylabel(titles{idxToDisplay})
% % % 
% % % % % Change Gz it to be balanced
% % % pulseSequenceNew(4299, 8) = - pulseSequenceNew(6, 8);
% % % pulseSequenceNew(4300, 8) =   0;
% % % pulseSequenceNew(4301, 8) =   0;
% % % 
% % % % Plot changed Gz
% % % plot(pulseSequenceNew(startIdx:endIdx,1), ...
% % %      pulseSequenceNew(startIdx:endIdx, idxToDisplay), '--')
% % %  