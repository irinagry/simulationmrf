% % % % IRINA GRIGORESCU
% % % % Date created: 04-06-2017
% % % % Date updated: 04-06-2017
% % % %
% % % % This script is meant to open and display the slice
% % % % profile file
% % % % 
% % % % Additionally, this script will change the slice profile according 
% % % % to different needs
% % % % 

clear all
close all


addpath ../helpers/misc/
addpath ../algs/    % Algs functions
addpath ../preprocess/ % Preprocessing steps
addpath ../helpers/ % Helper functions
addpath ~/possumdevirina/

WRITEFLAG = 0;

%% Read in the file
fileID = fopen('~/simdir/slcprofrealistic', 'r');
A = fscanf(fileID, '%f   %f');
fclose(fileID);

% Realistic slice profile
xslc = A(1:2:end);
yslc = A(2:2:end);

% Ideal slice profile
xslcnew = xslc;
yslcnew = yslc;

yslcnew(1:49)    = 0;
yslcnew(50:113)  = 1;
yslcnew(114:end) = 0;


% Plot the 2 slice profiles
figure,
plot(xslc,yslc)
hold on
plot(xslcnew,yslcnew)
xlabel('Frequency (%)')
ylabel('Amplitude (%)')
legend('realistic', 'ideal')


%% Save the ideal slice profile
B = zeros(size(A));
B(1:2:end) = xslcnew;
B(2:2:end) = yslcnew;

fileID2 = fopen('~/simdir/slcprofideal', 'w');
fprintf(fileID2, '%f   %f\n', B);
fclose(fileID2);
