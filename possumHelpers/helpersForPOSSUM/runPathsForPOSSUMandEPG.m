% IRINA GRIGORESCU
% This script generates paths 

% Addpaths for EPG
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/extendedPhaseGraph/'))
% Addpaths for possum helpers
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/possumHelpers/functions/'))
% Addpaths for helpers
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/helpers/'))
% Addpaths for algs
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/algs/'))
% Addpaths for preprocess
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/preprocess/'))
% Addpaths for perlin
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/perlin/'))
% Addpaths for Possum
addpath(genpath('~/possumdevirina/'))
% Addpaths for matlab nifti
addpath(genpath('~/Tools/MatlabStuff/matlabnifti/'))
