% 
% Irina Grigorescu
% Date created: 09-01-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps
% 

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% 1. Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/jan2018/epi/';

% Set number of repetitions to get to the fully sampled
NReps = 1;
% Set number of TR blocks
NTRs  = 50; 
% Set effective image size
Nx   = 32; Ny = Nx; 
% Set number of redout points per TR
nROPoints = Nx*Ny;

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints,  NReps, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints,  NReps, NTRs);
kspace_z = zeros(nROPoints * NReps, 1);

% Create kspace grid for one epi readout
[kspyEPI, kspxEPI] = meshgrid(floor(-Nx/2) : floor(Nx/2)-1 , ...
                              floor(-Ny/2) : floor(Ny/2)-1 );
% Flip left-right the even rows
kspxEPI(:, 2:2:end) = flipud(kspxEPI(:, 1:2:end)); % to go the other way 
                                                   % every other line
kspyEPI             = fliplr(kspyEPI);             % to go from + to -

% Kspace coordinates
kspxEPI = kspxEPI(:);
kspyEPI = kspyEPI(:);

traj_cart = bart(['traj -x', num2str(Nx), ' -y', num2str(Ny)]);

% % Create kspace coordinates for all repetition times
for i = 1:NReps
    for j = 1:NTRs
        % NROpoints x Nrepetitions x NTRs
        kspace_x(:,i,j) = kspxEPI(:)+1;
        kspace_y(:,i,j) = kspyEPI(:)+1;
    end
end



%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs

for i = 1:NReps
    % Get signal values from JEMRIS simulation
    %filenameSignal = [folderName, 'signals', num2str(i),'.h5']; 
    filenameSignal = [folderName, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Get timings of signal readout
    timingRO = h5read(filenameSignal, '/signal/times');
    % % % Readout points
    Nro = size(timingRO, 1);
    % % % Sort them and take signal in sorted order
    [timingRO,J] = sort(timingRO); Mvecs = A(:,:);
    % % % Calculate timings between each line of k-space and
    % % % between different slices
    d = diff(diff(timingRO));
    d(d<1e-5) = 0;

    % % % And store them in the solumn vector of indices I
    % % % together with index 0 and last index
    I = [0; find(d) + 1; length(timingRO)];

    % Get kspace as one signal
    for j = 1:NTRs
        kspaces(:,i,j) = Mvecs((j-1)*nROPoints+1 : j*nROPoints, 1).' + ...
              sqrt(-1) * Mvecs((j-1)*nROPoints+1 : j*nROPoints, 2).';

    end
end

% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints*NReps, NTRs]);

%% 4. Get the reconstructed image from the JEMRIS simulation
imagesJEMRIS = func_openAllImagesFromSimulationJEMRIS(Nx, Ny, ...
                                    folderName, 'signals.h5');
                                
% % 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);

% Save signal from image space from one voxel from the image
signalImage = zeros(NTRs, 1);

figure(222)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = squeeze(kspace_x(:, i));
    currentKspace_y = squeeze(kspace_y(:, i));
    currentKspaceToReco = (squeeze(kspaces(:, i))).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    imagesBART(:, :, i) = igrid;
    toc

    % Plot images (ABS)
    subplot(2,3,1), imagesc(abs(igrid))
    title('BART reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,4), imagesc(abs(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,2), imagesc(real(igrid))
    title('BART reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,3), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,5), imagesc(real(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,6), imagesc(imag(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    %pause(0.01)
end

           

%% 5. Compare the 2 reconstructions
% % % % Normalize both types of reconstructed images
% JEMRIS
[~, imagesJEMRIS] = normalizeSignals( ...
                    [ real(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ]);
imagesJEMRIS = (reshape(imagesJEMRIS, [Nx, Ny, NTRs]));
% BART
[~, imagesBART] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBART = (reshape(imagesBART, [Nx, Ny, NTRs]));
           


figure('Position', [10,10,1200,1200]);
lima = 0.5;

for i = 1:NTRs
    % BART RECONSTRUCTION
    % REAL
    subplot(3,3,1)
    imagesc(real(imagesBART(:,:,i)));
    title('Normalised BART reco (real)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % IMAG
    subplot(3,3,2)
    imagesc(angle(imagesBART(:,:,i)));
    title('Normalised BART reco (imag)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % ABS
    subplot(3,3,3)
    imagesc(abs(imagesBART(:,:,i)));
    title('Normalised BART reco (abs)')
    axis square; axis off; colorbar
    % caxis([-lima lima])

    % JEMRIS RECONSTRUCTION
    % REAL
    subplot(3,3,4)
    imagesc(real(imagesJEMRIS(:,:,i)));
    title('Normalised JEMRIS reco (real)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % IMAG
    subplot(3,3,5)
    imagesc(angle(imagesJEMRIS(:,:,i)));
    title('Normalised JEMRIS reco (imag)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % ABS
    subplot(3,3,6)
    imagesc(abs(imagesJEMRIS(:,:,i)));
    title('Normalised JEMRIS reco (abs)')
    axis square; axis off; colorbar
    % caxis([-lima lima])


    % DIFFERENCE IN RECONSTRUCTIONS
    % REAL
    subplot(3,3,7)
    imagesc(real(imagesJEMRIS(:,:,i))-real(imagesBART(:,:,i)));
    title('JEMRIS - BART reco (real)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % IMAG
    subplot(3,3,8)
    imagesc(imag(imagesJEMRIS(:,:,i))-imag(imagesBART(:,:,i)));
    title('JEMRIS - BART reco (imag)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % ABS
    subplot(3,3,9)
    imagesc(abs(imagesJEMRIS(:,:,i))-abs(imagesBART(:,:,i)));
    title('JEMRIS - BART reco (abs)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    
    %pause(0.01)
end

%% Simulate dictionary
% % % % % % % % % % % Generate dictionary with old ALGORITHM
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Sequence
FA_mrf  =  [90 80 70 60 50 60 70 80 90 80 90 80 70 60 50 60 70 80 90 80 90 80 70 60 50 60 70 80 90 80 90 80 70 60 50 60 70 80 90 80 90 80 70 60 50 60 70 80 90 80].';
%[90 80 70 60 50 60 70 80 90 80].'; %zeros(NTRs, 1) +  90;
PhA_mrf =  zeros(NTRs, 1) ;
TR_mrf  =  zeros(NTRs, 1) + 120;
RO_mrf  =  TR_mrf./2;

% % % % Custom Material
T1_mrf = [500, 600, 950, 1000, 1200, 3000, 4000, 4500];
T2_mrf = [ 70,  80, 100,  110,  200, 1000, 2000, 2200];
df_mrf =   0;

% % % % Generate dictionary
[ ~, ...                     % material properties
  sequenceProperties, ...    % sequence properties
  materialTuples, ...        % material tuples
  M, ...                     % number of material tuples
  ~, ...                     % dictionary MxNx3
  dictionaryMRFComplex, ...  % 
  dictionaryMRFNorm, ...     % dictionary normalised MxN
  dictionaryMRFNorm2Ch, ...  % dictionary normalised Mx2N
  signalDictionaryMRF] = ... % dictionary normalised MxN and absolute val
    func_generateCustomSignals(NTRs, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG);

%% Dictionary matching
imagesJEMRISReshaped = [ real(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ...
                         imag(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ];

imagesBARTReshaped = [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                       imag(reshape(imagesBART, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesJEMRISReshaped);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% Jemris
[valuesOfMatchJ1, indicesToMatchJ1, matTuplesMatchedJ1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesJEMRISReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% JEMRIS
[valuesOfMatchJ2, indicesToMatchJ2, matTuplesMatchedJ2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesJEMRISReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% JEMRIS
[valuesOfMatchJ3, indicesToMatchJ3, matTuplesMatchedJ3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesJEMRISReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% JEMRIS
[valuesOfMatchJ4, indicesToMatchJ4, matTuplesMatchedJ4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesJEMRISReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

% Merge results for JEMRIS:
resultsJ = matTuplesMatchedJ1;
resultsJ.score  = [valuesOfMatchJ1, valuesOfMatchJ2, ...
                   valuesOfMatchJ3, valuesOfMatchJ4];
resultsJ.scoremap = reshape(resultsJ.score, [Nx, Ny]);
resultsJ.indices = [indicesToMatchJ1, indicesToMatchJ2, ...
                    indicesToMatchJ3, indicesToMatchJ4]; 
resultsJ.indicesmap = reshape(resultsJ.indices, [Nx, Ny]); 

resultsJ.T1 = [matTuplesMatchedJ1.T1, matTuplesMatchedJ2.T1, ...
               matTuplesMatchedJ3.T1, matTuplesMatchedJ4.T1];
resultsJ.T1map = reshape(resultsJ.T1, [Nx, Ny]);
resultsJ.T2 = [matTuplesMatchedJ1.T2, matTuplesMatchedJ2.T2, ...
               matTuplesMatchedJ3.T2, matTuplesMatchedJ4.T2];
resultsJ.T2map = reshape(resultsJ.T2, [Nx, Ny]);
                   
% Merge results for BART:
resultsB = matTuplesMatchedB1;
resultsB.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                            valuesOfMatchB3, valuesOfMatchB4];
resultsB.scoremap = reshape(resultsB.score, [Nx, Ny]);                      
resultsB.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB.indicesmap = reshape(resultsB.indices, [Nx, Ny]); 
resultsB.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB.T1map = reshape(resultsB.T1, [Nx, Ny]);
resultsB.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB.T2map = reshape(resultsB.T2, [Nx, Ny]);

                
%% Create MAPS
% % By using <<createCirclesMask>>
% cr = 2;
% maskAllCircles = createCirclesMask(zeros([Nx, Ny]), ...
%                                    [12, 22; 21 22; 21 12], [cr, cr, cr]);
% maskWM  = createCirclesMask(zeros([Nx, Ny]), [12, 22], cr);
% maskGM  = createCirclesMask(zeros([Nx, Ny]), [21, 22], cr);
% maskCSF = createCirclesMask(zeros([Nx, Ny]), [21, 12], cr);
% 

% % By manually choosing the mask
cr = 2;
maskWM = zeros(Nx, Ny); maskGM = zeros(Nx, Ny); maskCSF = zeros(Nx, Ny);
maskWM(10:13, 20:23)  = 1; % WM
maskGM(20:23, 20:23)  = 1; % GM
maskCSF(20:23, 10:13) = 1; % CSF

maskAllCircles = maskWM + maskGM + maskCSF;

% % % % % Real maps of T1 and T2
realMap.T1map = ones(Nx,Ny) .* maskWM  .*  600 + ... 
                ones(Nx,Ny) .* maskGM  .*  950 + ...
                ones(Nx,Ny) .* maskCSF .* 4500;
realMap.T2map = ones(Nx,Ny) .* maskWM  .*   80 + ... 
                ones(Nx,Ny) .* maskGM  .*  100 + ...
                ones(Nx,Ny) .* maskCSF .* 2200;

% % % % % Reconstructed maps of T1 and T2        and GT maps
figure
% GT T1
subplot(3,3,1)
imagesc(realMap.T1map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([min(T1_mrf), 1200])
title('T_1 Real')
% GT T2
subplot(3,3,2)
imagesc(realMap.T2map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([min(T2_mrf), 150])
title('T_2 Real')

% JEMRIS - T1
subplot(3,3,4)
imagesc(resultsJ.T1map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([min(T1_mrf), 1200])
title('T_1 JEMRIS')

% JEMRIS - T2
subplot(3,3,5)
imagesc(resultsJ.T2map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([min(T2_mrf), 150])
title('T_2 JEMRIS')
   
% JEMRIS - Score
subplot(3,3,6)
imagesc(resultsJ.scoremap.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([0.9 1])
title('Score JEMRIS')
   
% BART - T1
subplot(3,3,7)
imagesc(resultsB.T1map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([min(T1_mrf), 1200])
title('T_1 BART')

% BART - T2
subplot(3,3,8)
imagesc(resultsB.T2map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([min(T2_mrf), 150])
title('T_2 BART')

% BART - Score
subplot(3,3,9)   
imagesc(resultsB.scoremap.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([0.9 1])
title('Score BART')

% % % % % Difference maps of T1 and T2 between reconstructed and GT
figure
% JEMRIS - T1
subplot(2,2,1)
imagesc((resultsJ.T1map - realMap.T1map) .* maskAllCircles)
colormap hot; colorbar, axis square
title('T_1 JEMRIS - T_1 Ground Truth')

% JEMRIS - T2
subplot(2,2,2)
imagesc((resultsJ.T2map - realMap.T2map) .* maskAllCircles)
colormap hot; colorbar, axis square
title('T_2 JEMRIS - T_2 Ground Truth')
      
% BART - T1
subplot(2,2,3)
imagesc((resultsB.T1map - realMap.T1map) .* maskAllCircles)
colormap hot; colorbar, axis square
title('T_1 BART - T_1 Ground Truth')

% BART - T2
subplot(2,2,4)
imagesc((resultsB.T2map - realMap.T2map) .* maskAllCircles )
colormap hot; colorbar, axis square
title('T_2 BART - T_2 Ground Truth')


%% Have a look at individual signals
% Jemris

[maxScoreValJ, maxScoreIdxJ] = max(resultsJ.score);
[minScoreValJ, minScoreIdxJ] = min(resultsJ.score);
rndScoreIdxJ = (12-1)*Nx+23;
rndScoreValJ = resultsJ.score(rndScoreIdxJ);

scoreIdxJ = [maxScoreIdxJ, minScoreIdxJ, rndScoreIdxJ];
scoreValJ = [maxScoreValJ, minScoreValJ, rndScoreValJ];

% Bart
[maxScoreValB, maxScoreIdxB] = max(resultsB.score);
[minScoreValB, minScoreIdxB] = min(resultsB.score);
rndScoreIdxB = rndScoreIdxJ;
rndScoreValB = resultsB.score(rndScoreIdxB);

scoreIdxB = [maxScoreIdxB, minScoreIdxB, rndScoreIdxB];
scoreValB = [maxScoreValB, minScoreValB, rndScoreValB];

% Titles
titles = {'Best', 'Worst', 'Random'};

for i = 1:3
    
    figure
    % % JEMRIS
    % ABS
    subplot(4,1,1)
    plot(1:NTRs, abs(dictionaryMRFNorm2Ch(resultsJ.indices(scoreIdxJ(i)), 1:NTRs) + ...
                 1i.*dictionaryMRFNorm2Ch(resultsJ.indices(scoreIdxJ(i)), NTRs+1:end)), 'o-')
    hold on
    plot(1:NTRs, abs(imagesJEMRISReshaped(scoreIdxJ(i), 1:NTRs) + ...
                 1i.*imagesJEMRISReshaped(scoreIdxJ(i), NTRs+1:end)), '.-')
    legend('dictionary', 'image')
	title(['JEMRIS (abs) ',  titles{i}, ' Score = ', num2str(scoreValJ(i))])
    % BY CHANNELS
    subplot(4,1,2)
    plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsJ.indices(scoreIdxJ(i)), :), 'o-')
    hold on
    plot(1:2*NTRs, imagesJEMRISReshaped(scoreIdxJ(i), :), '.-')
    legend('dictionary', 'image')
    title(['JEMRIS (by channels) ',  titles{i}, ' Score = ', num2str(scoreValJ(i))])


    % % BART
    % ABS
    subplot(4,1,3)
    plot(1:NTRs, abs(dictionaryMRFNorm2Ch(resultsB.indices(scoreIdxB(i)), 1:NTRs) + ...
                 1i.*dictionaryMRFNorm2Ch(resultsB.indices(scoreIdxB(i)), NTRs+1:end)), 'o-')
    hold on
    plot(1:NTRs, abs(imagesBARTReshaped(scoreIdxB(i), 1:NTRs) + ...
                 1i.*imagesBARTReshaped(scoreIdxB(i), NTRs+1:end)), '.-')
    legend('dictionary', 'image')
    title(['BART (abs) ',  titles{i}, ' Score = ', num2str(scoreValB(i))])
    % BY CHANNELS
    subplot(4,1,4)
    plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsB.indices(scoreIdxB(i)), :), 'o-')
    hold on
    plot(1:2*NTRs, imagesBARTReshaped(scoreIdxB(i), :), '.-')
    legend('dictionary', 'image')
    title(['BART (by channels) ',  titles{i}, ' Score = ', num2str(scoreValB(i))])

end


%% Compare between 2 similar dictionary signals
idx1 = 13; %51; %13; %51;
idx2 = 14; %50; %18; %43;

figure
subplot(2,1,1)
plot(1:2*NTRs, dictionaryMRFNorm2Ch(idx1, :), 'o-b')
hold on
plot(1:2*NTRs, dictionaryMRFNorm2Ch(idx2, :), 'o-r')
% legend('t1=4500,t2=2200', 't1=4000,t2=2200');
% legend('t1=950,t2=100', 't1=1000,t2=100');
% legend('t1=4500,t2=2200', 't1=4500,t2=2000');
legend('t1=950,t2=100', 't1=950,t2=110');

subplot(2,1,2)
plot(1:2*NTRs, dictionaryMRFNorm2Ch(idx1, :) - ...
               dictionaryMRFNorm2Ch(idx2, :), 'o-r')
           











   