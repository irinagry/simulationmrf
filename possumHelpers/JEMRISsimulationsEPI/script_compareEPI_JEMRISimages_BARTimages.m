% 
% Irina Grigorescu
% Date created: 13-11-2017
% 
% Script to reconstruct a JEMRIS simulation with BART
% 

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% 1. Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/jan2018/epi/';

% Set number of repetitions to get to the fully sampled
NReps = 1;
% Set number of TR blocks
NTRs  = 50; 
% Set effective image size
Nx   = 32; Ny = Nx; 
% Set number of redout points per TR
nROPoints = Nx*Ny;

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NReps, NTRs);
kspace_z = zeros(nROPoints*NReps, 1);

% Create kspace grid for one epi readout
[kspyEPI, kspxEPI] = meshgrid(floor(-Nx/2) : floor(Nx/2)-1 , ...
                              floor(-Ny/2) : floor(Ny/2)-1 );
% Flip left-right the even rows
kspxEPI(:, 2:2:end) = flipud(kspxEPI(:, 2:2:end)); % to go the other way 
                                                   % every other line
kspyEPI             = fliplr(kspyEPI);             % to go from + to -

% Kspace coordinates
kspxEPI = kspxEPI(:);
kspyEPI = kspyEPI(:);

% % Create kspace coordinates for all repetition times
for i = 1:NReps
    for j = 1:NTRs
        % NROpoints x Nrepetitions x NTRs
        kspace_x(:,i,j) = kspxEPI(:)+1;
        kspace_y(:,i,j) = kspyEPI(:)+1;
    end
end

% See the direction of k-space traversal in EPI
% % 
% % for i = 1:1%NReps
% %     % Get K-space coordinates from JEMRIS simulation
% %     % along with other sequence information
% %     filenameSequence = [folderName, 'seq.h5']; 
% %     t   = h5read(filenameSequence,'/seqdiag/T');    % Timepoints
% %     RXP = h5read(filenameSequence,'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
% %     KX  = h5read(filenameSequence,'/seqdiag/KX');   % KX (rad/mm)
% %     KY  = h5read(filenameSequence,'/seqdiag/KY');   % KY (rad/mm)
% %     KZ  = h5read(filenameSequence,'/seqdiag/KZ');   % KZ (rad/mm)
% %    
% %     % Calculate number of readout points - this will give you 300
% %     if nROPoints ~= (size(RXP(RXP==0),1) / NTRs)
% %         disp('Warning - unexpected number of READOUT points');
% %     end
% % 
% %     % Retrieve all k-space coordinates where READOUT has happened
% %     kspx = KX(RXP==0); kspy = KY(RXP==0);
% % end
% % 

%
% % Calculate the grid points from the k-space trajectory taken from JEMRIS
% % % % for i = 1:NReps
% % % %     % Get K-space coordinates from JEMRIS simulation
% % % %     % along with other sequence information
% % % %     %filenameSequence = [folderName, 'seq', num2str(i),'.h5']; 
% % % %     filenameSequence = [folderName, 'seq.h5']; 
% % % %     t   = h5read(filenameSequence,'/seqdiag/T');    % Timepoints
% % % %     RXP = h5read(filenameSequence,'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
% % % %     KX  = h5read(filenameSequence,'/seqdiag/KX');   % KX (rad/mm)
% % % %     KY  = h5read(filenameSequence,'/seqdiag/KY');   % KY (rad/mm)
% % % %     KZ  = h5read(filenameSequence,'/seqdiag/KZ');   % KZ (rad/mm)
% % % %    
% % % %     % Calculate number of readout points - this will give you 300
% % % %     if nROPoints ~= (size(RXP(RXP==0),1) / NTRs)
% % % %         disp('Warning - unexpected number of READOUT points');
% % % %     end
% % % % 
% % % %     % Retrieve all k-space coordinates where READOUT has happened
% % % %     kspx = KX(RXP==0); kspy = KY(RXP==0);
% % % % 
% % % %     % For each TR block calculate the kspace coordinates by bringing them back
% % % %     % to 1/cm from rad/mm
% % % %     % My signals will be in the form of NRO x NReps x NTRs 
% % % %     % for example, my signals will be   300 x  48   x  5  
% % % %     for j = 1:NTRs
% % % %         % NROpoints x Nrepetitions x NTRs
% % % %         kspace_x(:,i,j) = (kspx((j-1)*nROPoints+1 : j*nROPoints));
% % % %                            %./ (2*pi)) .* 1E+01; %cm^-1
% % % %         kspace_y(:,i,j) = (kspy((j-1)*nROPoints+1 : j*nROPoints));
% % % %                            %./ (2*pi)) .* 1E+01; %cm^-1
% % % %     end
% % % %     
% % % % end
% % % % 
% % % % % Reshape to fully sampled data
% % % % kspace_x = reshape(kspace_x, [nROPoints*NReps, NTRs]);
% % % % kspace_y = reshape(kspace_y, [nROPoints*NReps, NTRs]);
% % % % 
% % % % % Plot them for each spiral turn
% % % % figure(111)
% % % % for i = 1:NTRs
% % % %     % Plot the coordinates before scaling
% % % %     subplot(1,2,1); xl = 4.5;
% % % %     scatter(kspace_x(:, i),kspace_y(:, i),'.'), axis equal, axis square;
% % % %     hold on
% % % %     scatter(kspace_x(64, i),kspace_y(64, i),'o'), axis equal, axis square;
% % % %     scatter(kspace_x( 1, i),kspace_y( 1, i),'o'), axis equal, axis square;
% % % %     xlabel('k_x rad mm^{-1}'); ylabel('k_y rad mm^{-1}');
% % % %     title(['EPI ', num2str(i)])
% % % %     xlim([-xl xl]); ylim([-xl xl]);
% % % % 
% % % %     % Transform the kspace coordinates to the desired effective imaging matrix
% % % %     % by scaling the values so that they fit into the matrix
% % % %     dist_ksp_x = sqrt(min(kspace_x(:,i) - max(kspace_x(:,i))).^2);
% % % %     dist_ksp_y = sqrt(min(kspace_y(:,i) - max(kspace_y(:,i))).^2);
% % % %     
% % % %     % Scale the values
% % % %     kspace_x(:,i) = (Nx-1) .* (kspace_x(:,i) ./ dist_ksp_x);
% % % %     kspace_y(:,i) = (Ny-1) .* (kspace_y(:,i) ./ dist_ksp_y);
% % % %     
% % % %     % Shifts in x and y
% % % %     shift_x = min(kspace_x(kspace_x > 0));
% % % %     shift_y = min(kspace_y(kspace_y > 0));
% % % %     
% % % %     % Shift the values
% % % %     kspace_x(:,i) = round(kspace_x(:,i) - shift_x + 1);
% % % %     kspace_y(:,i) = round(kspace_y(:,i) - shift_y);
% % % %     
% % % %     % Plot the coordinates after scaling
% % % %     subplot(1,2,2)
% % % %     scatter(kspace_x(:, i), kspace_y(:, i),'.'), axis equal, axis square;
% % % %     xlabel('N_x'); ylabel('N_y');
% % % %     title(['EPI ', num2str(i)])
% % % %     xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);
% % % % 
% % % %     drawnow
% % % % end
% % % % 
% % % % % % Compare the two
% % % % figure 
% % % % for i = 1:size(kspx,1)
% % % %     subplot(1,2,1)
% % % %     scatter(kspxEPI(i),kspyEPI(i),'.'); hold on
% % % %     xlim([min(kspxEPI) max(kspxEPI)]);
% % % %     ylim([min(kspyEPI) max(kspyEPI)]);
% % % %     
% % % %     subplot(1,2,2)
% % % %     scatter(kspace_x(i),kspace_y(i),'.'); hold on
% % % %     xlim([min(kspace_x) max(kspace_x)]);
% % % %     ylim([min(kspace_y) max(kspace_y)]);
% % % %     
% % % %     pause(0.01)
% % % % end

%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs

for i = 1:NReps
    % Get signal values from JEMRIS simulation
    %filenameSignal = [folderName, 'signals', num2str(i),'.h5']; 
    filenameSignal = [folderName, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Get timings of signal readout
    timingRO = h5read(filenameSignal, '/signal/times');
    % % % Readout points
    Nro = size(timingRO, 1);
    % % % Sort them and take signal in sorted order
    [timingRO,J] = sort(timingRO); Mvecs = A(:,:);
    % % % Calculate timings between each line of k-space and
    % % % between different slices
    d = diff(diff(timingRO));
    d(d<1e-5) = 0;

    % % % And store them in the solumn vector of indices I
    % % % together with index 0 and last index
    I = [0; find(d) + 1; length(timingRO)];

    % Get kspace as one signal
    for j = 1:NTRs
        kspaces(:,i,j) = Mvecs((j-1)*nROPoints+1 : j*nROPoints, 1).' + ...
              sqrt(-1) * Mvecs((j-1)*nROPoints+1 : j*nROPoints, 2).';

    end
end

% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints*NReps, NTRs]);

%% 4. Get the reconstructed image from the JEMRIS simulation
imagesJEMRIS = func_openAllImagesFromSimulationJEMRIS(Nx, Ny, ...
                                    folderName, 'signals.h5');

                                
                                
% % 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);

% Save signal from image space from one voxel from the image
signalImage = zeros(NTRs, 1);

figure(222)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = squeeze(kspace_x(:, i));
    currentKspace_y = squeeze(kspace_y(:, i));
    currentKspaceToReco = squeeze(kspaces(:, i)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    imagesBART(:, :, i) = igrid;
    toc

    % Plot images (ABS)
    subplot(2,3,1), imagesc(abs(igrid))
    title('BART reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,4), imagesc(abs(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,2), imagesc(real(igrid))
    title('BART reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,3), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,5), imagesc(real(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,6), imagesc(imag(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    pause(0.1)
end

 
           

%% 5. Compare the 2 reconstructions
% % % % Normalize both types of reconstructed images
% JEMRIS
[~, imagesJEMRIS] = normalizeSignals( ...
                    [ real(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ]);
imagesJEMRIS = (reshape(imagesJEMRIS, [Nx, Ny, NTRs]));
% BART
[~, imagesBART] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBART = (reshape(imagesBART, [Nx, Ny, NTRs]));
           

%%
figure('Position', [10,10,1200,1200]);
lima = 0.5;

for i = 1:NTRs
    % BART RECONSTRUCTION
    % REAL
    subplot(3,3,1)
    imagesc(real(imagesBART(:,:,i)));
    title('Normalised BART reco (real)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % IMAG
    subplot(3,3,2)
    imagesc(angle(imagesBART(:,:,i)));
    title('Normalised BART reco (imag)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % ABS
    subplot(3,3,3)
    imagesc(abs(imagesBART(:,:,i)));
    title('Normalised BART reco (abs)')
    axis square; axis off; colorbar
    % caxis([-lima lima])

    % JEMRIS RECONSTRUCTION
    % REAL
    subplot(3,3,4)
    imagesc(real(imagesJEMRIS(:,:,i)));
    title('Normalised JEMRIS reco (real)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % IMAG
    subplot(3,3,5)
    imagesc(angle(imagesJEMRIS(:,:,i)));
    title('Normalised JEMRIS reco (imag)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % ABS
    subplot(3,3,6)
    imagesc(abs(imagesJEMRIS(:,:,i)));
    title('Normalised JEMRIS reco (abs)')
    axis square; axis off; colorbar
    % caxis([-lima lima])


    % DIFFERENCE IN RECONSTRUCTIONS
    % REAL
    subplot(3,3,7)
    imagesc(real(imagesJEMRIS(:,:,i))-real(imagesBART(:,:,i)));
    title('JEMRIS - BART reco (real)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % IMAG
    subplot(3,3,8)
    imagesc(imag(imagesJEMRIS(:,:,i))-imag(imagesBART(:,:,i)));
    title('JEMRIS - BART reco (imag)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    % ABS
    subplot(3,3,9)
    imagesc(abs(imagesJEMRIS(:,:,i))-abs(imagesBART(:,:,i)));
    title('JEMRIS - BART reco (abs)')
    axis square; axis off; colorbar
    % caxis([-lima lima])
    
    pause(0.01)
end

%% Comparison of signals between the 3 reconstructions
gmPos  = (21-1)*Nx+21;
wmPos  = (21-1)*Nx+12;
csfPos = (12-1)*Nx+21;
indices = [gmPos, wmPos, csfPos];
titles  = {'GM', 'WM', 'CSF'}; 
imagesBARTVector   = reshape(imagesBART,   [Nx*Ny, NTRs]);
imagesJEMRISVector = reshape(imagesJEMRIS, [Nx*Ny, NTRs]);

for i = 1:3
    figure
    % % REAL
    % Plot one next to the other 
    subplot(3,2,1)
    plot(1:NTRs, real(imagesBARTVector(indices(i),:)), 'r-o'); hold on
    plot(1:NTRs, real(imagesJEMRISVector(indices(i),:)), 'b-o');
    xlabel('TR index'); ylabel('Re [ Signal normalised ] ');
    legend('BART','JEMRIS')
    title(['Plot JEMRIS vs BART for ',titles{i}]);
    
    % Plot difference between them
    subplot(3,2,2)
    plot(1:NTRs, real(imagesJEMRISVector(indices(i),:)) - ...
                 real(imagesBARTVector(indices(i),:)), 'k-o'); 
	xlabel('TR index'); ylabel('Re[Signal_{JEMRIS} - Signal_{BART}]');
    title(['Plot difference between JEMRIS and BART for ',titles{i}]);
    
    % % IMAG
    % Plot one next to the other 
    subplot(3,2,3)
    plot(1:NTRs, imag(imagesBARTVector(indices(i),:)), 'r-o'); hold on
    plot(1:NTRs, imag(imagesJEMRISVector(indices(i),:)), 'b-o');
    xlabel('TR index'); ylabel('Im [ Signal normalised ] ');
    legend('BART','JEMRIS')
    title(['Plot JEMRIS vs BART for ',titles{i}]);
    
    % Plot difference between them
    subplot(3,2,4)
    plot(1:NTRs, imag(imagesJEMRISVector(indices(i),:)) - ...
                 imag(imagesBARTVector(indices(i),:)), 'k-o'); 
	xlabel('TR index'); ylabel('Im[Signal_{JEMRIS} - Signal_{BART}]');
    title(['Plot difference between JEMRIS and BART for ',titles{i}]);
    
    % % ABS
    % Plot one next to the other 
    subplot(3,2,5)
    plot(1:NTRs, abs(imagesBARTVector(indices(i),:)), 'r-o'); hold on
    plot(1:NTRs, abs(imagesJEMRISVector(indices(i),:)), 'b-o');
    xlabel('TR index'); ylabel('ABS [ Signal normalised ] ');
    legend('BART','JEMRIS')
    title(['Plot JEMRIS vs BART for ',titles{i}]);
    
    % Plot difference between them
    subplot(3,2,6)
    plot(1:NTRs, abs(imagesJEMRISVector(indices(i),:)) - ...
                 abs(imagesBARTVector(indices(i),:)), 'k-o'); 
	xlabel('TR index'); ylabel('ABS[Signal_{JEMRIS}] - ABS[Signal_{BART}]');
    title(['Plot difference between JEMRIS and BART for ',titles{i}]);
end
    

% % % Test to see if chosen voxels are within the objects
% circlesChosVoxels = zeros(Nx,Ny);
% circlesChosVoxels = circlesChosVoxels(:);
% circlesChosVoxels(wmPos) = 2;
% circlesChosVoxels(gmPos) = 2;
% circlesChosVoxels(csfPos) = 2;
% circlesChosVoxels = reshape(circlesChosVoxels,[Nx,Ny]);
% figure
% imagesc(squeeze(abs(igrid(:,:,1)))); 
% hold on; 
% imagesc(circlesChosVoxels,'AlphaData', .1); colormap gray


%%   
figure
subplot(1,3,1)
hist(abs(imagesBART(:)),500)

subplot(1,3,2)
hist(abs(imagesJEMRIS(:)),500)

subplot(1,3,3)
hist(abs(imagesJEMRIS(:)-imagesBART(:)),500)
   
   
   
   
%%
   