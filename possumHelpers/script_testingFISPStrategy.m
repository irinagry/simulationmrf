% % IRINA GRIGORESCU
% % 
% % In this script I am looking at how the FISP signal is an average over
% % the bSSFP signal accross a voxel from 0 to 2pi
% % 

% Observations:
% A bunch of spins dephasing in a voxel between 0 and 2*pi.
% 
% You take their net magnetization and create a signal from there
% 
% It is why in JEMRIS this works, because you can choose the amount of
% spins you want to simulate for and it solves the Bloch equations for each
% of them and takes the net magnetization within each voxel. 
% 
% Think of a way to relate that to the signal ...
% 
% 
% 
% 
% 


% % % % Preprocessing
% Addpaths for EPG comparison
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/extendedPhaseGraph/'))

% Addpaths for helpers
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/helpers/'))
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/preprocess/'))

% Addpaths for algorithms (such as normalize signals)
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/algs/'))

%% Prerequisites
% We have a material with high T1 and T2 such that relaxation is not
% something to consider

% Materials:
T1 = 4500; 
T2 = 2200;
% And a sequence with consecutive pulses of 90 degrees
N  = 200;
PA = zeros(N,1);
FA = zeros(N,1) + 90;
TR = zeros(N,1) +  1;
TE = zeros(N,1) +  0;
% % % % Generate bSSFP signal
% % % % SEQUENCE
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
customValuesSequence = struct;
customValuesSequence.RF.FA  = FA;
customValuesSequence.RF.PhA = PA;
customValuesSequence.TR     = TR;
customValuesSequence.RO     = TE;
% % % % 
% % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial = struct;
customValuesMaterial.T1     = T1;
customValuesMaterial.T2     = T2;

%% Generate EPG signal
% Run EPG
[ChiF, ChiZ] = epgMatrix(N, PA, FA, TR, T1, T2, 1, 1); % M0, flagDephase

F0states = ChiF(N, :) .* exp(-TE ./ T2).';
sigEPG = abs(F0states);


%% Generate bSSFP signal
Nsim = 150;
signalNet = zeros(Nsim,N);
signalSSD = zeros(Nsim,1);                              
Nspins    = zeros(Nsim,1);

% Try for increasing number of spins
for i = 1:Nsim
    Nspins(i) = 2*i;    % number of spins from 0 to Nspins-1
    % Choose amount of precession during TR 
    % We are choosing between 0 and 2*pi thus simulating what would 
    % happen in reality
    betaPrecessionTR   = linspace(-pi, pi - 2*pi/Nspins(i), Nspins(i)); 
                       % spins with 0, 45, 90, 135, 180, 225, 270, 315
                       %linspace(-pi,pi,80); % values for the angle (in deg) of
                       % precession during TR
    % Transforming angle into off-resonance frequency per TR
    customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                     ./ (2*pi * customValuesSequence.TR(1));
    
	% Running simulation                                   
    tic
    [materialProperties, sequenceProperties, ...            
              materialTuples, Mmaterials, ...
              dictionaryMRF] = simulateMRF(N, 0, ...
                                    SEQFLAG, customValuesSequence, ...
                                    MATFLAG, customValuesMaterial, ...
                                    0);
    toc
    
    % Calculating net magnetization
    netMagnetization = squeeze(sum(dictionaryMRF, 1))./Nspins(i);
    signalNet(i,:) = abs(netMagnetization(:,1) + 1i.*netMagnetization(:,2));
    
    % Calculating difference between net magnetization and EPG
    signalSSD(i) = sum((signalNet(i,:) - sigEPG).^2);
end


%%
figure('Position', [5,5,1400,1200])

% Signal EPG and signal dictionary
subplot(3,2,[1,2])
plot(1:N, sigEPG, 'r'); hold on
plot(1:N, signalNet(end,:), 'g--'); 
legend('EPG', 'netSig')
title({['EPG vs Net Magnetization of ', ...
        num2str(Nspins(end)), ' spins'], ...
        ['T_1 = ', num2str(T1), ' T_2 = ', num2str(T2)], ...
        ['TR = ', num2str(TR(1)), ' ms TE = ', num2str(TE(1)), ' ms']})
xlabel('TR block number'); ylabel('signal');

% Signal EPG and signal dictionary by channels
subplot(3,2,[3,4])
plot(1:2*N, [real(F0states) imag(F0states)], 'r'); hold on
plot(1:2*N, [netMagnetization(:,1).' -netMagnetization(:,2).'], 'g--'); 
legend('EPG', 'netSig')
xlabel('TR block number'); ylabel('signal');

% Difference between signals
subplot(3,2,5)
plot(1:N, sigEPG-signalNet(end,:), 'r'); hold on
xlabel('TR block number'); ylabel('signal_{EPG} - signal_{net}');

% Number of spins
subplot(3,2,6)
% semilogy(Nspins, signalSSD)
semilogy(Nspins, signalSSD, '-o')
xlabel('number of spins');
ylabel('\Sigma (signal_{EPG} - signal_{net})^2');




% % %% Failed experiment: I was trying to see if I can calculate the 
% % %  signal in a different way, by knowing the vector magnitude at each time
% % %  point and then using the complex form to somehow see if I can retrieve
% % %  the signal. Needs more thinking
% % % For each of the Nspins 
% % M = zeros(Nspins(end), N); % magnitude of the magnetization vector 
% %                            % for each spin i at each sequence time point
% % for i = 1:Nspins(end)
% %     M(i,:) = sqrt(squeeze(dictionaryMRF(i, :, 1)).^2 + ... % M_x component of spin i
% %                   squeeze(dictionaryMRF(i, :, 2)).^2);     % M_y component of spin i
% % end
% % 
% % % Each spin accrues the same amount of phi in each TR
% % phiSpins = repmat(betaPrecessionTR.', [1, 20]) .* ...
% %            repmat((1:N), [Nspins(end), 1]) ;
% % % In complex notation that gives you Mcomplex           
% % Mcomplex = M .* exp(1i .* phiSpins); % M+_j(t) = |M|_j e^(i phi_j(t))
% % 
% % % Net magnetization (sum over all spins)
% % Mnet = sum(Mcomplex, 1);%./Nspins(end);



% % % plot(1:N, squeeze(dictionaryMRF(spinNo,:,1)), 'r.-'), hold on
% % % plot(1:N, squeeze(dictionaryMRF(spinNo,:,2)), 'b.-')
% % % legend('M_x', 'M_y')




% % % % 
% % % % % dictionaryMRFSignal = sqrt(dictionaryMRF(:,:,1).^2 + dictionaryMRF(:,:,2).^2);
% % % % dictionaryMRFSignalAverageX = zeros(N,1);
% % % % dictionaryMRFSignalAverageY = zeros(N,1);
% % % % for i = 1:N
% % % %     dictionaryMRFSignalAverageX(i) = sum(squeeze(dictionaryMRF(:,i,1)));
% % % %     dictionaryMRFSignalAverageY(i) = sum(squeeze(dictionaryMRF(:,i,2)));
% % % % end
% % % % dictionaryMRFSignalAverageX = dictionaryMRFSignalAverageX ./ ... 
% % % %                               length(betaPrecessionTR);
% % % % dictionaryMRFSignalAverageY = dictionaryMRFSignalAverageY ./ ... 
% % % %   length(betaPrecessionTR);
% % % % % = mean(squeeze(dictionaryMRF(:,:,1)), 1);
% % % % dictionaryMRFSignalAverage  = abs(dictionaryMRFSignalAverageX + ...
% % % %                               1i.*dictionaryMRFSignalAverageY);
% % % % 
% % % % %% Plot EPG against bSSFP
% % % % 
% % % % figure
% % % % 
% % % % plot(1:N, sigEPG, 'k', 'LineWidth', 2); hold on
% % % % plot(1:N, dictionaryMRFSignalAverage, 'b--', 'LineWidth', 2);
% % % % plot(1:N, sigPOSSUM, 'r--', 'LineWidth', 2);
% % % % plot(1:N, sigJEMRIS, 'g--', 'LineWidth', 2);
% % % % 
% % % % legend('EPG','bSSFP-avg','POSSUM','JEMRIS')
% % % % 
% % % % 
% % % % 
% % % % 
% % % % 
% % % % 
