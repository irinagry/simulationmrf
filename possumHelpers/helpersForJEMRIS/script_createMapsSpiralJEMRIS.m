% % % % 
% % % % This script handles a simulation made with JEMRIS
% % % % reconstruct with BART
% % % % and then creates the T1 T2 maps
% % % % 

% Set environment variable for BART
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
addpath(genpath('~/Tools/MatlabStuff/'))
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/possumHelpers/functions/'))
addpath(genpath('~/Tools/bart-0.3.01/'))
% Addpaths for EPG
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/extendedPhaseGraph/'))
% Addpaths for possum helpers
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/possumHelpers/functions/'))
% Addpaths for helpers
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/helpers/'))
% Addpaths for algs
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/algs/'))
% Addpaths for preprocess
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/preprocess/'))
% Addpaths for perlin
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/perlin/'))

%% Get k-space coordinates from JEMRIS simulation

% JEMRIS simulation
% folderName = '~/jemrisSims/spiral/spiralFull1TR48Rot12TubesR5Spins972/' ; %spiralFull500TR48Rot12TubesR3Spins348aggregated/'; %testFullySampled/';
% folderName = '~/jemrisSims/spiral/spiralFull10TR48Rot4TubesR5Spins/';
% folderName = '~/jemrisSims/spiral/spiralFull10TR1Rot4TubesR5Spins196/';
folderName = '~/jemrisSims/epi/';

% Set number of repetitions to get to the fully sampled
NReps = 1;
% Set number of redout points per TR
nROPoints = 4096; %300; %125; %6000;
% Set number of TR blocks
NTRs  = 30; %20; %5;
% Set effective image size
Nx   = 64; Ny = Nx; 

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NReps, NTRs);
kspace_z = zeros(nROPoints*NReps, 1);

for i = 1:NReps
    % Get K-space coordinates from JEMRIS simulation
    % along with other sequence information
    filenameSequence = [folderName, 'seq', num2str(i),'.h5']; 
    t   = h5read(filenameSequence,'/seqdiag/T');    % Timepoints
    RXP = h5read(filenameSequence,'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
    %TXM = h5read(filenameSequence,'/seqdiag/TXM');  % RF pulse FA  in rad
    %TXP = h5read(filenameSequence,'/seqdiag/TXP');  % RF pulse PhA in rad
    %GX  = h5read(filenameSequence,'/seqdiag/GX');   % GX (mT/m)
    %GY  = h5read(filenameSequence,'/seqdiag/GY');   % GY (mT/m)
    %GZ  = h5read(filenameSequence,'/seqdiag/GZ');   % GZ (mT/m)
    KX  = h5read(filenameSequence,'/seqdiag/KX');   % KX (rad/mm)
    KY  = h5read(filenameSequence,'/seqdiag/KY');   % KY (rad/mm)
    KZ  = h5read(filenameSequence,'/seqdiag/KZ');   % KZ (rad/mm)
    %B   = h5read(filenameSequence,'/seqdiag/META');
    %pulseSequenceJ = [t./1000, TXM      , zeros(size(TXM)), ...
    %                                      zeros(size(TXM)), ...
    %                  RXP+1   , GX./1000, ...
    %                            GY./1000, ...
    %                            GZ./1000];

    % Calculate number of readout points - this will give you 300
    if nROPoints ~= (size(RXP(RXP==0),1)./NTRs)
        disp('Warning - unexpected number of READOUT points');
    end

    % Retrieve all k-space coordinates where READOUT has happened
    % for each sequence, meaning it will be 300 x 5 (NROPoints x NTRs)
    kspx = KX(RXP==0); kspy = KY(RXP==0);

    % For each TR block calculate the kspace coordinates by bringing them back
    % to 1/cm from rad/mm
    % My signals will be in the form of NRO x NReps x NTRs 
    % for example, my signals will be   300 x  48   x  5  
    for j = 1:NTRs
        % NROpoints x Nrepetitions x NTRs
        kspace_x(:,i,j) = (kspx((j-1)*nROPoints+1 : j*nROPoints) ...
                           ./ (2*pi)) .* 1E+01; %cm^-1
        kspace_y(:,i,j) = (kspy((j-1)*nROPoints+1 : j*nROPoints) ...
                           ./ (2*pi)) .* 1E+01; %cm^-1
    end
    
end

% Reshape to fully sampled data
kspace_x = reshape(kspace_x, [nROPoints*NReps, NTRs]);
kspace_y = reshape(kspace_y, [nROPoints*NReps, NTRs]);

% Plot them for each spiral turn
figure(111)
for i = 1:NTRs
    % Plot the coordinates before scaling
    subplot(1,2,1); xl = 4.5;
    scatter(kspace_x(:, i),kspace_y(:, i),'.'), axis equal, axis square;
    hold on
    scatter(kspace_x(64, i),kspace_y(64, i),'o'), axis equal, axis square;
    scatter(kspace_x( 1, i),kspace_y( 1, i),'o'), axis equal, axis square;
    xlabel('k_x cm^{-1}'); ylabel('k_y cm^{-1}');
    title(['Spiral ', num2str(i)])
    xlim([-xl xl]); ylim([-xl xl]);

    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt(   (kspace_x(64,i) - kspace_x(1,i)).^2 + ...
                       (kspace_y(64,i) - kspace_y(1,i)).^2  );
    dist_ksp_adj = sqrt(   (kspace_x(2,i) - kspace_x(1,i)).^2 + ...
                           (kspace_y(2,i) - kspace_y(1,i)).^2  );
    kspace_x(:,i) = kspace_x(:,i) .* Nx./(dist_ksp) - dist_ksp_adj;
    kspace_y(:,i) = kspace_y(:,i) .* Ny./(dist_ksp) - dist_ksp_adj;

    % Plot the coordinates after scaling
    subplot(1,2,2)
    scatter(kspace_x(:, i), kspace_y(:, i),'.'), axis equal, axis square;
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(i)])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

    drawnow
end


%%  GET THE SIGNAL VALUES
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs

for i = 1:NReps
    % Get signal values from JEMRIS simulation
    filenameSignal = [folderName, 'signals', num2str(i),'.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Get timings of signal readout
    timingRO = h5read(filenameSignal, '/signal/times');
    % % % Readout points
    Nro = size(timingRO, 1);
    % % % Sort them and take signal in sorted order
    [timingRO,J] = sort(timingRO); Mvecs = A(:,:);
    % % % Calculate timings between each line of k-space and
    % % % between different slices
    d = diff(diff(timingRO));
    d(d<1e-5) = 0;

    % % % And store them in the solumn vector of indices I
    % % % together with index 0 and last index
    I = [0; find(d) + 1; length(timingRO)];

    % Get kspace as one signal
    for j = 1:NTRs
        kspaces(:,i,j) = Mvecs((j-1)*nROPoints+1 : j*nROPoints, 1).' + ...
              sqrt(-1) * Mvecs((j-1)*nROPoints+1 : j*nROPoints, 2).';

    end
end

% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints*NReps, NTRs]);


%% BART reconstruction
% inverse gridding

% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);

% Save signal from image space from one voxel from the image
signalImage = zeros(NTRs, 1);

figure(222)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = squeeze(kspace_x(:, i));
    currentKspace_y = squeeze(kspace_y(:, i));
    currentKspaceToReco = squeeze(kspaces(:, i)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].'; 
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    
    % % Rotate image
    % igrid_rot = imrotate( igrid , 8 );
    
    % Zero pad to full size
    igrid_padded = bart(['resize -c 0 ', num2str(Nx), ...
                                  ' 1 ', num2str(Ny)], ...
                                         igrid);
	
	%reco1 = bart('rss 1', igrid_padded);
    toc
    
    % Keep the image stored
    allReconstructedImages(:,i) = igrid_padded(:);
    
    % Save value of signal at a randomly chosen point in k-space
    signalImage(i) = igrid_padded(Nx/2+1, Ny/2+1); %igrid(32, 32); %Nx/2, Ny/2));
    
    % Plot images
    subplot(1,2,1), imagesc(real(igrid_padded))
    title(['Spiral reconstruction ', num2str(i)])
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(1,2,2), imagesc(imag(igrid_padded))
    title(['Spiral reconstruction ', num2str(i)])
    axis square; axis off;
    colormap gray;
    colorbar;
    
    pause(0.1)
end

% Workaround because of that nasty last image being weird
% allReconstructedImages(:, NTRs) = allReconstructedImages(:, NTRs);

%%
% % % % % Signals from phantom
% Normalise signals from phantom
phantomSignals = [ real(allReconstructedImages) , ...
                   imag(allReconstructedImages) ] ; 
               
% Normalize signals to norm(signal) = 1
[phantomSignalsNorm2Ch, phantomSignalsNorm] = ...
        normalizeSignals(phantomSignals);
    
% Reshape signals back into Nx x Ny x NTRs matrix
phantomSignalsNorm2D    = reshape(phantomSignalsNorm, [Nx, Ny, NTRs]);
phantomSignalsNorm2Ch2D = reshape(phantomSignalsNorm2Ch, [Nx, Ny, 2*NTRs]);

%%
% % % % % % % % % % % Load EPG Dictionary
% Load data from dictionary file 
varToLoad = load(['/Users/irina/OneDrive - University College London/', ...
                  'Work/Simulation/simulationMRF/src/', ...'
                  'data_EPGDictionary/', ...
                  'epgdictionary-BSSFP-IR-sequenceListMaryia.mat']);
                  %'dictionaryBSSFPTR500Mariya.mat']);
                  %'groundTruthPhantom.mat']);
                  %'dictionaryBSSFPTR500Mariya.mat']);
% Get values from loaded variable
F0states = varToLoad.F0states(:,2:NTRs+1); % 2:end because first is after IR 
% Material tuples
materialTuples = varToLoad.materialTuples;
M = size(materialTuples, 2);

% Create 2 channel data (real + imag)
F0statesReIm = [ real(F0states), imag(F0states)];

% Normalize signals
[dictionaryMRFFISPNorm2Ch, dictionaryMRFFISPNorm] = ...
        normalizeSignals(F0statesReIm);

% Calculate absolute signal
sigMRFFISPAbsNorm = abs(dictionaryMRFFISPNorm(:, 1:end));

%%
% % % % % Look at the signal vs dictionary signal
% idxT1T2 = 5514;
idxT1 = find(materialTuples(1,:) == 1000);
idxT2 = find(materialTuples(2,:) ==  100);
idxT1T2 = intersect(idxT1, idxT2);

[~, signalImage2] = normalizeSignals([ real(signalImage.') , ...
                                       imag(signalImage.') ] );

figure
subplot(2,1,1)
plot(1:NTRs, abs(signalImage2), 'o-')
hold on
plot(1:NTRs, sigMRFFISPAbsNorm(idxT1T2,:), 'o-')
xlabel('TR block number');
ylabel('normalised signals (a.u.)');
legend('jemris', 'epg')

subplot(2,1,2)
plot(1:NTRs, abs(signalImage2) - sigMRFFISPAbsNorm(idxT1T2,:), 'o-')
xlabel('TR block number');
ylabel('Jemris - Dictionary difference');



%% MATCHING
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(phantomSignals);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
[valuesOfMatch1, indicesToMatch1, matTuplesMatched1] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
[valuesOfMatch2, indicesToMatch2, matTuplesMatched2] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
[valuesOfMatch3, indicesToMatch3, matTuplesMatched3] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
[valuesOfMatch4, indicesToMatch4, matTuplesMatched4] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

% Merge results:
valuesOfMatch  = [valuesOfMatch1, valuesOfMatch2, ...
                  valuesOfMatch3, valuesOfMatch4];
indicesToMatch = [indicesToMatch1, indicesToMatch2, ...
                  indicesToMatch3, indicesToMatch4]; 
matTuplesMatched = matTuplesMatched1;
matTuplesMatched.T1 = [matTuplesMatched1.T1, matTuplesMatched2.T1, ...
                       matTuplesMatched3.T1, matTuplesMatched4.T1];
matTuplesMatched.T2 = [matTuplesMatched1.T2, matTuplesMatched2.T2, ...
                       matTuplesMatched3.T2, matTuplesMatched4.T2];
                   



                   
                   
%% PLOT MAPS
mapT1 = reshape(matTuplesMatched.T1, [Nx, Ny]);
mapT2 = reshape(matTuplesMatched.T2, [Nx, Ny]);
mapMatchValues = reshape(valuesOfMatch, [Nx, Ny]);

maskA = createCirclesMask(ones(Nx, Ny), [Nx/2, Ny/2], 6);
% maskA = ones(Nx, Ny);
% createCirclesMask(ones(Nx, Ny), ...
%                           [118 ,  73 ; ...
%                            153 ,  78 ; ...
%                             78 , 104 ; ...
%                            113 , 109 ; ...
%                            149 , 113 ; ...
%                            184 , 118 ; ...
%                             73 , 139 ; ...
%                            108 , 144 ; ...
%                            144 , 149 ; ...
%                            179 , 153 ; ...
%                            104 , 179 ; ...
%                            139 , 184 ] , ...
%                            ones(12,1).*6).';
%                           [54 , 9 ; ...
%                            89 , 14; ...
%                            14 , 40 ; ...
%                            89 , 14; ...
%                            49 , 45; ...
%                            85 , 49; ...
%                           120 , 54; ...
%                             9 , 75; ...
%                            45 , 80; ...
%                            80 , 84; ...
%                           115 , 89; ...
%                            40 , 115; ...
%                            75 , 120], ...
%                            ones(12,1).*3);

% % % % Plot maps of T1 and T2
figure('Position', [10,10,1400,1000])
subplot(1,2,1)
imagesc(fliplr(mapT1).*maskA)
axis off, axis square
colormap hot
colorbar, caxis([0 1700])
title('T_1 map')

subplot(1,2,2)
imagesc(fliplr(mapT2).*maskA)
axis off, axis square
colormap hot
colorbar, caxis([0 500])
title('T_2 map')

% % % % Values of matching
figure
imagesc(fliplr(mapMatchValues).*maskA)
axis off, axis square
colormap hot
colorbar, caxis([0.9 1])
title('Values of matching')






%%









%%

idxTissue1 = 11787; %  950|100
idxTissue2 =  6253; %  600| 80
idxTissue3 = 10987; %  900|50
idxTissue4 = 15757; % 1200|200

firstImage = reshape(allReconstructedImages(:,1),[Nx,Ny]);
posidx2 = (32-1)*Nx+32;
posidx1 = (33-1)*Nx+33;


figure, hold on

imagesc(abs(firstImage))

pause

secondImage = reshape(firstImage, [Nx*Ny, 1]);
secondImage(posidx2) = -1;

imagesc(abs(reshape(secondImage,[Nx Ny])));

[valuesOfMatch11, indicesToMatch11, matTuplesMatched11] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(posidx2,:), ...
                                 materialTuples, 0);

figure
plot(1:NTRs, abs(phantomSignalsNorm(posidx2,:)), 'r'), hold on
plot(1:NTRs, sigMRFFISPAbsNorm(idxTissue1,:)), 
% plot(1:NTRs, sigMRFFISPAbsNorm(idxTissue2,:)), 
% plot(1:NTRs, sigMRFFISPAbsNorm(idxTissue3,:)), 
% plot(1:NTRs, sigMRFFISPAbsNorm(idxTissue4,:)), 
legend('phantom','tissue1','tissue2','tissue3','tissue4')
title(['Matched value = ', num2str(valuesOfMatch11)])






