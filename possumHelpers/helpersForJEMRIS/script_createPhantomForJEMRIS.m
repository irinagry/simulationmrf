% IRINA GRIGORESCU
% THIS script is to create a custom object for JEMRIS

% Prerequisites
addpath(genpath('~/Tools/MatlabStuff/'))

% First, let's load the custom object mat file from the JEMRIS examples
load('/usr/local/share/jemris/matlab/userSample.mat')





% For future reference
% My calculated previously phantom Values
myCalculatedPhantomT1 = [1040, 450, 1320, 1060, 600, 300, 1580, 1200, 760, 460, 1350, 1580];
myCalculatedPhantomT2 = [185, 135, 155, 145, 120, 70, 155, 185, 140, 50, 180, 300];
% My chosen phantom values based on tube table for 3T and 296 K temperature
% where first values are big circle values
% myPhantomT1Values = [100, 900, 50, 70, 90]; %600, 1200, 950]; %[500, 1078, 496, 1422, 1007, 666, 334, 1576, 1153, 831, 516, 1407, 1615];
% myPhantomT2Values = [  1,  50, 25, 35, 45]; % 80,  200, 100]; %[360, 212, 151, 190, 128, 129, 70, 161, 156, 149, 49, 171, 373];

myPhantomT1Values = [500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
myPhantomT2Values = [360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];


% Size of phantom: 180 x 180 x 5
% Create Phantom 
% Size of enclosing box in every direction
m =  181; %144; % in x-direction
n =  181; %144; % in y-direction
p =  181; %  5; % in z-direction

% % Remember all centres and radii of all circles
centres = zeros(5, 2); 
radii   = zeros(5, 1); 

% % Radius of big circle
R = 10; %56;
% % Center of object
posx = m/2; 
posy = n/2; 
posz = ceil(p/2); 
th = 0:pi/50:2*pi;
yunitBig = R * cos(th) + posx;
xunitBig = R * sin(th) + posy;
centres(13,1) = posy; centres(13,2) = posx;
radii(13,1)   = R; 

% % Radius of small circles
r = 8;
% 1. 2 circles from the above
% Circle1
centres(1,2) = posx - R/2 - R/4 + r/2; 
centres(1,1) = posy - R/4;
radii(1,1)   = r; 
yunit1 = radii(1,1) * cos(th) + centres(1,2); 
xunit1 = radii(1,1) * sin(th) + centres(1,1);
% Circle2
centres(2,2) = posx - R/2 - R/4 + r/2; 
centres(2,1) = posy + R/4;
radii(2,1)   = r; 
yunit2 = radii(2,1) * cos(th) + centres(2,2); 
xunit2 = radii(2,1) * sin(th) + centres(2,1);

% 2. 4 circles from above middle line
%Circle3
centres(3,2) = posx - R/4; 
centres(3,1) = posy - R/2 - R/4 + r/2;
radii(3,1)   = r; 
yunit3 = radii(3,1) * cos(th) + centres(3,2); 
xunit3 = radii(3,1) * sin(th) + centres(3,1);
%Circle4
centres(4,2) = posx - R/4; 
centres(4,1) = posy - R/4;
radii(4,1)   = r; 
yunit4 = radii(4,1) * cos(th) + centres(4,2); 
xunit4 = radii(4,1) * sin(th) + centres(4,1); 
%Circle5
centres(5,2) = posx - R/4; 
centres(5,1) = posy + R/4;
radii(5,1)   = r; 
yunit5 = radii(5) * cos(th) + centres(5,2); 
xunit5 = radii(5) * sin(th) + centres(5,1);
%Circle6
centres(6,2) = posx - R/4; 
centres(6,1) = posy + R/2 + R/4 - r/2;
radii(6,1)   = r; 
yunit6 = radii(6,1) * cos(th) + centres(6,2); 
xunit6 = radii(6,1) * sin(th) + centres(6,1);

% 3. 4 circles from below middle line
%Circle7
centres(7,2) = posx + R/4; 
centres(7,1) = posy - R/2 - R/4 + r/2;
radii(7,1)   = r; 
yunit7 = radii(7,1) * cos(th) + centres(7,2); 
xunit7 = radii(7,1) * sin(th) + centres(7,1); 
%Circle8
centres(8,2) = posx + R/4; 
centres(8,1) = posy - R/4;
radii(8,1)   = r; 
yunit8 = radii(8,1) * cos(th) + centres(8,2); 
xunit8 = radii(8,1) * sin(th) + centres(8,1); 
%Circle9
centres(9,2) = posx + R/4; 
centres(9,1) = posy + R/4;
radii(9,1)   = r; 
yunit9 = radii(9,1) * cos(th) + centres(9,2); 
xunit9 = radii(9,1) * sin(th) + centres(9,1);
%Circle10
centres(10,2) = posx + R/4; 
centres(10,1) = posy + R/2 + R/4 - r/2;
radii(10,1)   = r; 
yunit10 = radii(10,1) * cos(th) + centres(10,2);
xunit10 = radii(10,1) * sin(th) + centres(10,1);

% 4. 2 circles from below
%Circle10
centres(11,2) = posx + 3*R/4 - r/2; 
centres(11,1) = posy - R/4;
radii(11,1)   = r; 
yunit11= radii(11,1) * cos(th) + centres(11,2); 
xunit11= radii(11,1) * sin(th) + centres(11,1);
%Circle12
centres(12,2) = posx + 3*R/4 - r/2; 
centres(12,1) = posy + R/4;
radii(12,1)   = r; 
yunit12= radii(12,1) * cos(th) + centres(12,2); 
xunit12= radii(12,1) * sin(th) + centres(12,1);

% To make a hollow circle of zeros
radiiHollow = radii + 2;

% Plot the data
figure,
% 1. Plot the middle slice
Slice = zeros(m,n);
imagesc(Slice), hold on
% 2. Plot the big circle
h  = plot(xunitBig, yunitBig, 'k');
% 3. Cirles on the first line
h1 = plot(xunit1, yunit1, 'k');
h2 = plot(xunit2, yunit2, 'k');
% 4. Cirles above the middle line
h3 = plot(xunit3, yunit3, 'k');
h4 = plot(xunit4, yunit4, 'k');
h5 = plot(xunit5, yunit5, 'k');
h6 = plot(xunit6, yunit6, 'k');
% 5. Cirles above the middle line
h7 = plot(xunit7, yunit7, 'k');
h8 = plot(xunit8, yunit8, 'k');
h9 = plot(xunit9, yunit9, 'k');
h10= plot(xunit10, yunit10, 'k');
% 6. Cirles on the first line
h11 = plot(xunit11, yunit11, 'k');
h12 = plot(xunit12, yunit12, 'k');
% Labels
clear h h1 h2 h3 h4 h5 h6 h7 h8 h9 h10 h11 h12;
xlabel(num2str(n)); ylabel(num2str(m));
axis equal

% % % % % % Create Masks
masksOfCircles = zeros(m,n);    % keep all masks here
phantom_T1  = zeros(m,n,p);       % phantom with T1 values
phantom_T2  = zeros(m,n,p);       % phantom with T2 values
phantom_T2s = zeros(m,n,p);       % phantom with T2s values
% All small circles
maskAllCircles = zeros(181,181);%createCirclesMask(Slice, ...
                                 %  centres(1:end-1,:), radii(1:end-1,1));
% The big circle
maskBigCircle  = createCirclesMask(Slice, ...
                                   centres(end,:), radii(end,1));
% The no value circles around the filled value circles
maskHollow = createCirclesMask(Slice, ...
                               centres(1:end-1,:), radiiHollow(1:end-1,1));
maskHollow = -(maskHollow-maskAllCircles)+1;

% The big circle without the small circles
masksOfCircles(:,:) = maskBigCircle - maskAllCircles;
phantom_T1(:,:,posz) = (maskBigCircle - maskAllCircles).*myPhantomT1Values(1);
phantom_T2(:,:,posz) = (maskBigCircle - maskAllCircles).*myPhantomT2Values(1);

idx = 1:12; %[4,5,8,9];

% for i = 2 : 13
%     masksOfCircles(:,:,i) = createCirclesMask(Slice, ...
%                                    centres(idx(i-1),:), radii(idx(i-1)));
%                                
%     % Save T1 and T2 relaxation parameters from the file
%     phantom_T1(:,:,posz) = phantom_T1(:,:,1) + ...
%                createCirclesMask(Slice, centres(idx(i-1),:), radii(idx(i-1))) .* ...
%                                                myPhantomT1Values(i);
%     phantom_T2(:,:,posz) = phantom_T2(:,:,1) + ...
%                createCirclesMask(Slice, centres(idx(i-1),:), radii(idx(i-1))) .* ...
%                                                myPhantomT2Values(i);
% end 
% 
% % Make the hollowness around the circles
% phantom_T1(:,:,posz) = phantom_T1(:,:,1).*maskHollow;
% phantom_T2(:,:,posz) = phantom_T2(:,:,1).*maskHollow;

% % % % Make sure the values are the same across the slices
% % % for i = 2:p
% % %     phantom_T1(:,:,i) = squeeze(phantom_T1(:,:,1));
% % %     phantom_T2(:,:,i) = squeeze(phantom_T2(:,:,1));
% % % end

% Plot masks
figure
subplot(1,2,1)
imagesc(squeeze(phantom_T1(:,:,posz)))
title('Real T_1')
axis off; axis equal, axis square; 
colormap hot; colorbar
caxis([0 1700])


subplot(1,2,2)
imagesc(squeeze(phantom_T2(:,:,posz)))
title('Real T_2')
axis off; axis equal, axis square; 
colormap hot; colorbar
caxis([0 500])




% Things that I need to save in my user defined phantom
% 1. DB = deltaB = 0 everywhere (m x n x p)
DB = zeros(m,n,p);
% 2. M0 will just be 1 everywhere except outside of circle phantom (m x n x p)
% M0 = repmat(maskBigCircle.*1 , [1,1,p]); %maskAllCircles.*1
M0 = zeros(m,n,p);
M00 = phantom_T1(:,:,posz); %maskAllCircles.*1;
M00(M00~=0) = 1;
M0(:,:,posz) = M00;
% 3. OFFSET = [ 0 0 0 ] (1 x 3)
OFFSET = [0 0 0];
% 4. RES = resolution = 1 (1 x 1)
RES = 1;
% 5. T1 = values of T1 wherever (m x n x p)
T1  = phantom_T1;
% 6. T2 = values of T2 wherever (m x n x p)
T2  = phantom_T2;
% 7. T2s = values of T2s wherever (m x n x p)
T2s = phantom_T2s;

% % % save('/Users/irina/OneDrive - University College London/Work/JEMRISSimulations/phantomsForJEMRIS/phantom1Circle181x181x181R10Spins316.mat', 'DB', 'M0', 'OFFSET', 'RES', 'T1', 'T2', 'T2s');

disp(['Number of spins: ', num2str(sum(sum(sum(M0))))])

%% Create the Shepp-Logan phantom

P = phantom('Modified Shepp-Logan', 200);
figure, imagesc(P)
axis equal


%% Create a brain slice
close all

bSlice = load('/usr/local/share/jemris/matlab/MNIbrain.mat');
BrainSlice = squeeze(bSlice.BRAIN(:,:,89)); 

% Get rid of fat and other stuff which are not WM,GM,CSF
BrainSlice(BrainSlice > 3) = 0;

BrainSlice = BrainSlice(1:2:end,1:2:end);
BrainSlice = BrainSlice(:,10:102);

% Properties of the phantom
[m, n] = size(BrainSlice);
p = 5;

figure
imagesc(rot90(BrainSlice,1))
axis equal
colorbar


% Properties of the tissue of the phantom
BrainSlice_T1 = BrainSlice; 
BrainSlice_T2 = BrainSlice;

BrainSlice_T1(BrainSlice_T1==3) =  600; % WM T1
BrainSlice_T2(BrainSlice_T2==3) =   80; % WM T2
BrainSlice_T1(BrainSlice_T1==2) =  950; % GM T1
BrainSlice_T2(BrainSlice_T2==2) =  100; % GM T2
BrainSlice_T1(BrainSlice_T1==1) = 4500; % CSF T1
BrainSlice_T2(BrainSlice_T2==1) = 2200; % CSF T2

% Things that I need to save in my user defined phantom
% 1. DB = deltaB = 0 everywhere (m x n x p)
DB = zeros(m,n,p);
% 2. M0 will just be 1 everywhere except outside of circle phantom (m x n x p)
% M0 = repmat(maskBigCircle.*1 , [1,1,p]); %maskAllCircles.*1
M0 = zeros(m,n,p);
M00 = BrainSlice;
M00(M00~=0) = 1;
M0(:,:,3) = M00;
% 3. OFFSET = [ 0 0 0 ] (1 x 3)
OFFSET = [0 0 0];
% 4. RES = resolution = 1 (1 x 1)
RES = 2;
% 5. T1 = values of T1 wherever (m x n x p)
T1  = zeros(m,n,p); T1(:,:,3) = BrainSlice_T1;
% 6. T2 = values of T2 wherever (m x n x p)
T2  = zeros(m,n,p); T2(:,:,3) = BrainSlice_T2;
% 7. T2s = values of T2s wherever (m x n x p)
T2s = zeros(m,n,p);

% save('/Users/irina/jemrisSims/phantomsForJEMRIS/phantomBrain90x93Spins4864.mat', 'DB', 'M0', 'OFFSET', 'RES', 'T1', 'T2', 'T2s');

disp(['Number of spins: ', num2str(sum(sum(sum(M0))))])


%% Create a simple square object for JEMRIS
nSpinsPerSide = [1,2,5,8,10,14,20,30];
m = 40; n = 40;
for idxSpin = 1:8
    % Things that I need to save in my user defined phantom
    
    % 1. DB = deltaB = 0 everywhere (m x n x p)
    DB = zeros(m,n,1);
    
    % 2. M0 will just be 1 inside the square of spins
    M0 = zeros(m,n,1);
    M0(5 : 4 + nSpinsPerSide(idxSpin) , ...
       5 : 4 + nSpinsPerSide(idxSpin) , 1) = 1;
    
    % 3. OFFSET = [ 0 0 0 ] (1 x 3)
    OFFSET = [0 0 0];
    
    % 4. RES = resolution = 1 (1 x 1)
    RES = 1;
    
    % 5. T1 = values of T1 wherever (m x n x p)
    T1  = M0; T1(T1~=0) = 1000;
    
    % 6. T2 = values of T2 wherever (m x n x p)
    T2  = M0; T2(T2~=0) =  100;
    
    % 7. T2s = values of T2s wherever (m x n x p)
    T2s = zeros(m, n, 1);

    numberOfSpins = sum(sum(sum(M0)));
    disp(['Number of spins: ', num2str(numberOfSpins)]);
    
%     save(['/Users/irina/jemrisSims/jan2018/bottleneckROtest/square', ...
%            num2str(numberOfSpins), '.mat'], ...
%           'DB', 'M0', 'OFFSET', 'RES', 'T1', 'T2', 'T2s');

end




%% Create a simple square object for JEMRIS to compare with MRISIMUL
nSpinsPerSide = 1162;
m = 40; n = 40;
for idxSpin = 1:1
    % Things that I need to save in my user defined phantom
    
    % 1. DB = deltaB = 0 everywhere (m x n x p)
    DB = zeros(m,n,1);
    
    % 2. M0 will just be 1 inside the square of spins
    M0 = zeros(m,n,1);
    M0(5 : 4 + nSpinsPerSide(idxSpin) , ...
       5 : 4 + nSpinsPerSide(idxSpin) , 1) = 1;
    
    % 3. OFFSET = [ 0 0 0 ] (1 x 3)
    OFFSET = [0 0 0];
    
    % 4. RES = resolution = 1 (1 x 1)
    RES = 1;
    
    % 5. T1 = values of T1 wherever (m x n x p)
    T1  = M0; T1(T1~=0) = 1000;
    
    % 6. T2 = values of T2 wherever (m x n x p)
    T2  = M0; T2(T2~=0) =  100;
    
    % 7. T2s = values of T2s wherever (m x n x p)
    T2s = zeros(m, n, 1);

    numberOfSpins = sum(sum(sum(M0)));
    disp(['Number of spins: ', num2str(numberOfSpins)]);
    
%     save(['/Users/irina/jemrisSims/jan2018/bottleneckROtest/square', ...
%            num2str(numberOfSpins), '.mat'], ...
%           'DB', 'M0', 'OFFSET', 'RES', 'T1', 'T2', 'T2s');

end




