% % % % IRINA GRIGORESCU
% % % % This script displays all imagesc created by a JEMRIS simulation
% % % % 

% clear all 
% close all
%%

% % % % Size of images
Nx = 32; Ny = 32;

% % % Folder location
folderName = '/Users/irina/jemrisSims/mySequences/';

% % % File name
% filename = 'sig_100EPIblocks_motion.h5';
fileName = 'signals.h5';

% % % Motion used:
% motion trajectory used:
% motiontraj=[0 0 0 0 0 0 0; 3000 0 0 0 0 0 0 ; 3010  10 0 0 0 0 0 ;  3020 20 0 0 0 0 0 ;  3030 30 0 0 0 0 0 ]



%% Get images
imagesAll = func_openAllImagesFromSimulationJEMRIS(Nx, Ny, ...
                                folderName, fileName);
[~, ~, Nimgs] = size(imagesAll);

%% Plot signal from ROI

% % % Plot signal taken from middle of image
figure
roiFromImage = squeeze(imagesAll(17,15,:));
plot(1:Nimgs, roiFromImage ./ ...
              norm(roiFromImage))
hold on
plot(1:Nimgs, signalDictionaryMRF(1:Nimgs) ./ ...
              norm(signalDictionaryMRF(1:Nimgs)), '--')
legend('jemris', 'me')
xlabel('image number')
ylabel('normalised magnitude')

%% Plot images
idx_img = 1;

% % % Display an image
figure
imageToDisplay = squeeze(imagesAll(:,:,idx_img));
imagesc(imageToDisplay);
colormap gray
colorbar
title(['Image no ', num2str(idx_img)])

% % % Display more than one image (normalised)
figure
for idx_img = 1:20
    imgToDisp = squeeze(imagesAll(:,:,idx_img));
    imagesc(imgToDisp, [0 250]);
    colormap gray
    colorbar
    title(['Image no ', num2str(idx_img)])
    pause(0.1)
end

