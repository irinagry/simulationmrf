% % % % IRINA GRIGORESCU
% % % % Date created: 31-07-2017
% % % % Date updated: 31-07-2017
% % % %
% % % % All-in-one script that compares simulations from JEMRIS images
% % % % POSSUM images and dictionary generation
% % % % 
% % % % PLEASE PAY ATTENTION AT THIS SCRIPT AS IT HAS THE PROPER 
% % % % SIGNAL NORMALIZATION

% % % % PLEASE SEE script_compareSignalDictionaryImagesPOSSUM.M
% % % % FOR POSSUM ONLY

% clear all
% close all

addpath ~/'OneDrive - University College London'/Work/Simulation/simulationMRF/possumHelpers
addpath ~/'OneDrive - University College London'/Work/Simulation/simulationMRF/src/helpers/
addpath ~/'OneDrive - University College London'/Work/Simulation/simulationMRF/src/helpers/misc/
addpath ~/'OneDrive - University College London'/Work/Simulation/simulationMRF/src/test/
addpath ~/'OneDrive - University College London'/Work/Simulation/simulationMRF/src/algs/
addpath ~/'OneDrive - University College London'/Work/Simulation/simulationMRF/src/preprocess/
addpath ~/'OneDrive - University College London'/Work/Simulation/simulationMRF/src/perlin/
addpath ~/possumdevirina/
addpath ~/Tools/MatlabStuff/matlabnifti/

% % % % SOME VARIABLES
NPulses = 50; Nx = 32; Ny = 32;

% Nx=Ny=16
% roix = [4,  9, 13]; %16 % CSF | WM | GM
% roiy = [5, 10, 13]; %16 % CSF | WM | GM

% Nx=Ny=32
roix = [ 8, 17, 26]; %32 % CSF | WM | GM
roiy = [ 9, 18, 24]; %32 % CSF | WM | GM

%%
% % % % % % % % % % % LOAD JEMRIS IMAGES
% % % Folder location
folderName = '/Users/irina/jemrisSims/mySequences/';

% % % File name
fileName = 'signalsEPI32IncreasingN50TR12FA40.h5';
          %'signalsEPI32ConstantN50TR12FA40.h5';

% % % Get images
imagesJ = func_openAllImagesFromSimulationJEMRIS(Nx, Ny, ...
                                folderName, fileName);
imagesJ = rot90(imagesJ,2);  

[~, ~, Nimgs] = size(imagesJ);

imagesJ = cat(3, real(imagesJ), imag(imagesJ)); %% concatenate real | imag

% % % Normalize to having the same sum of squared magnitude
imagesJReshaped  = reshape(imagesJ, [Nx*Ny, 2*NPulses]);

[imagesJNormReshaped, ~] = normalizeSignals(imagesJReshaped);

imagesJNorm = reshape(imagesJNormReshaped, [Nx, Ny, 2*NPulses]);
imagesJAbs  = abs( squeeze(imagesJNorm(:, :,         1 : NPulses)) + ...
               1i.*squeeze(imagesJNorm(:, :, NPulses+1 : end)) ) ; 


maxVoxelJ = max(max(max(imagesJ)));

%%
% % % % % % % % % % % LOAD POSSUM IMAGES
seqName  = 'epib';
imgSize  = [num2str(Nx), 'x', num2str(Ny)];
npulses  = ['Np', num2str(NPulses)];%, '_noGz'];

% % % % Load Files
imagesP     = zeros(Nx,Nx,2*NPulses);

for i = 1:NPulses
    filenameImg = ['~/simdir/outImage/', ...          % folder
                   'image_', ...                      % core name
                    seqName, '_', ...                 % sequence
                    imgSize, '_', ...                 % size of image
                    npulses, '_', ...                 % number of pulses
                    num2str(i)];                      % image number
                
    imageNIIreal = load_nii([filenameImg, '_real.nii.gz'], ...
                        [], [], [], [], [], 0.5);
    imageNIIimag = load_nii([filenameImg, '_imag.nii.gz'], ...
        [], [], [], [], [], 0.5);
    
    % Store real and imaginary channels
    imagesP(:,:,i)         = flipud(squeeze(imageNIIreal.img(:,:))');
    imagesP(:,:,i+NPulses) = flipud(squeeze(imageNIIimag.img(:,:))');
end

maxVoxelP = max(max(max(imagesP)));
imagesP = imagesP ./ maxVoxelP;

% Reshape from 3D matrix into a 2D matrix
imagesPReshaped  = reshape(imagesP, [Nx*Ny, 2*NPulses]);

% Normalize signals across the temporal domain
[imagesPNormReshaped, ~] = normalizeSignals(imagesPReshaped);

imagesPNorm = reshape(imagesPNormReshaped, [Nx, Ny, 2*NPulses]);
imagesPAbs  = abs( squeeze(imagesPNorm(:, :,         1 : NPulses)) + ...
               1i.*squeeze(imagesPNorm(:, :, NPulses+1 : end)) ) ; 


%%
% % % % % % % % % % % Generate dictionary
% % % % Flags
FLAGPLOT = 1; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Save them in simdir
seqList = dlmread('~/simdir/sequenceListConstantN50TR12FA40');
                % '~/simdir/sequenceListN500TRPerlinFASinusoid'

% % % % Custom Sequence
FA_mrf  =  seqList(:, 2);
PhA_mrf =  zeros(NPulses,1);
TR_mrf  =  seqList(:, 1).*1000;
RO_mrf  =  TR_mrf./2;


% % % % Custom Material
T1_mrf = [950, 600, 4500, 0.1]; % GM | WM | CSF
T2_mrf = [100,  80, 2200, 0.1];
df_mrf =   0;

GMidx  =  1;
WMidx  =  5;
CSFidx =  9;
CSTidx =  11;

indicesOfInterest = [GMidx WMidx CSFidx CSTidx];

[materialProperties, sequenceProperties, materialTuples, M, ...
 dictionaryMRF, dictionaryMRFComplex, dictionaryMRFComplexNorm, ...
 signalDictionaryMRF] = ...
    func_generateCustomSignals(NPulses, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG, ...
                    indicesOfInterest);
                
% M = [(sequenceProperties.TR + 1)./1000, sequenceProperties.RF.FA];
% dlmwrite('~/simdir/sequenceListN300TRPerlinFASinusoid', M, ' ');

GMidx  =  1;
WMidx  =  2;%5;
CSFidx =  3;%9;

%%
% % % % % % % % % % % Comparison with Dictionary generation
colors = ['r','b','g'];
figure('Position', [10,10,1200,800]);
        
        % % % % % % CSF 
        subplot(3,1,1)
        plot(1:NPulses, squeeze(imagesPAbs(roix(1), roiy(1), :)), ...
            [colors(1),'.-'])                                 % POSSUM
        hold on
        plot(1:NPulses, signalDictionaryMRF(CSFidx,:), 'ko--')% DICTIONARY
%         plot(1:NPulses, squeeze(imagesJAbs(roix(1), roiy(1), :)), ...
%            [colors(2),'.--'])                               % JEMRIS
        grid on
        xlabel('image number');
        ylabel('normalized intensity')
        legend('Possum-CSF', 'Dictionary-CSF')%, 'Jemris-CSF')
        title('Comparison with POSSUM')% and JEMRIS')

        % % % % % % WM
        subplot(3,1,2)
        plot(1:NPulses, squeeze(imagesPAbs(roix(2), roiy(2), :)), ...
            [colors(1),'.-'])                                 % POSSUM
        hold on   
        plot(1:NPulses, signalDictionaryMRF(WMidx,:), 'ko--') % DICTIONARY
%         plot(1:NPulses, squeeze(imagesJAbs(roix(2), roiy(2), :)), ...
%            [colors(2),'.--'])                               % JEMRIS
        grid on
        xlabel('image number');
        ylabel('normalized intensity');
        legend('Possum-WM', 'Dictionary-WM')%, 'JEMRIS-WM')

        % % % % % % GM
        subplot(3,1,3)
        plot(1:NPulses, squeeze(imagesPAbs(roix(3), roiy(3), :)), ...
            [colors(1),'.-'])                                 % POSSUM
        hold on
        plot(1:NPulses, signalDictionaryMRF(GMidx,:), 'ko--') % DICTIONARY
%         plot(1:NPulses, squeeze(imagesJAbs(roix(3), roiy(3), :)), ...
%            [colors(2),'.--'])                               % JEMRIS
        grid on
        xlabel('image number');
        ylabel('normalized intensity')
        legend('Possum-GM', 'Dictionary-GM')%, 'JEMRIS-GM')


%%
% % % % % % % % % % % MORE Comparison with Dictionary generation
colors = ['r','b','g'];
figure('Position', [10,10,1200,800]);
        
        % % % % % % CSF 
        subplot(3,2,1)
        plot(0:0.1:1, 0:0.1:1, 'k') % identity line
        hold on
        plot(signalDictionaryMRF(CSFidx,:), ...
            squeeze(imagesPAbs(roix(1), roiy(1), :)), ...
            [colors(1),'o'])                              % DICT vs POSSUM
        grid on
        axis square
        xlabel({'normalized intensity', 'Dictionary-CSF'});
        ylabel({'normalized intensity', 'POSSUM-CSF'});
        % % % % % % CSF difference
        subplot(3,2,2)
        plot(1:NPulses, signalDictionaryMRF(CSFidx,:) - ...
            squeeze(imagesPAbs(roix(1), roiy(1), :))', ...
            [colors(1),'-'])                              % DICT vs POSSUM
        grid on
        xlabel('image number');
        ylabel('Dictionary-CSF - POSSUM-CSF')
        
        % % % % % % WM
        subplot(3,2,3)
        plot(0:0.1:1, 0:0.1:1, 'k') % identity line
        hold on
        axis square
        plot(signalDictionaryMRF(WMidx,:), ...
            squeeze(imagesPAbs(roix(2), roiy(2), :)), ...
            [colors(1),'o'])                              % DICT vs POSSUM
        grid on
        xlabel({'normalized intensity', 'Dictionary-WM'});
        ylabel({'normalized intensity', 'POSSUM-WM'});
        % % % % % % WM difference
        subplot(3,2,4)
        plot(1:NPulses, signalDictionaryMRF(WMidx,:) - ...
            squeeze(imagesPAbs(roix(2), roiy(2), :))', ...
            [colors(1),'-'])                              % DICT vs POSSUM
        grid on
        xlabel('image number');
        ylabel('Dictionary-WM - POSSUM-WM')

        % % % % % % GM
        subplot(3,2,5)
        plot(0:0.1:1, 0:0.1:1, 'k') % identity line
        hold on
        axis square
        plot(signalDictionaryMRF(GMidx,:), ...
            squeeze(imagesPAbs(roix(3), roiy(3), :)), ...
            [colors(1),'o'])                              % DICT vs POSSUM
        grid on
        xlabel({'normalized intensity', 'Dictionary-GM'});
        ylabel({'normalized intensity', 'POSSUM-GM'});
        % % % % % % GM difference
        subplot(3,2,6)
        plot(1:NPulses, signalDictionaryMRF(GMidx,:) - ...
            squeeze(imagesPAbs(roix(3), roiy(3), :))', ...
            [colors(1),'-'])                              % DICT vs POSSUM
        grid on
        xlabel('image number');
        ylabel('Dictionary-GM - POSSUM-GM')




%%
% % % % % % % % % % % % Dictionary Matching

maskForImages = zeros(Nx,Ny,NPulses);
% For Nx=Ny=16
% maskForImages( 3: 5, 4: 6) = 1;  
% maskForImages( 8:10, 9:10) = 1;
% maskForImages(12:14,12:13) = 1;
% For Nx=Ny=32
maskForImages( 6: 10,  8: 12) = 1;  
maskForImages(15: 19, 16: 19) = 1;
maskForImages(25: 27, 23: 25) = 1;


% % % % Match POSSUM
possumToMatch = reshape(imagesPAbs, [Nx*Ny,NPulses]);

[~, matTuplesP] = matchingWithAbs(signalDictionaryMRF, ...
                                  possumToMatch, ...
                                  materialTuples);
                                           
mapT1P = reshape(matTuplesP.T1, [Nx,Ny]).*squeeze(maskForImages(:,:,1));
mapT2P = reshape(matTuplesP.T2, [Nx,Ny]).*squeeze(maskForImages(:,:,1));


figure,
subplot(3,2,1)
imagesc(mapT1P)
colormap jet
colorbar
axis square
title('POSSUM T_1')

subplot(3,2,2)
imagesc(mapT2P)
colormap jet
colorbar
axis square
title('POSSUM T_2')

% % % % Match JEMRIS
jemrisToMatch = reshape(imagesJAbs, [Nx*Ny,NPulses]);
[indicesJ, matTuplesJ] = matchingWithAbs(signalDictionaryMRF, ...
                                         jemrisToMatch, ...
                                         materialTuples);

mapT1J = reshape(matTuplesJ.T1, [Nx,Ny]).*squeeze(maskForImages(:,:,1));
mapT2J = reshape(matTuplesJ.T2, [Nx,Ny]).*squeeze(maskForImages(:,:,1));

subplot(3,2,3)
imagesc(mapT1J)
colormap jet
colorbar
axis square
title('JEMRIS T_1')

subplot(3,2,4)
imagesc(mapT2J)
colormap jet
colorbar
axis square
title('JEMRIS T_2')

% % % % REAL VALUES
realMapsT1 = zeros(Nx,Ny);      realMapsT2 = zeros(Nx,Ny);
% For Nx=Ny=16
% realMapsT1( 3: 5, 4: 6) = 4500; realMapsT2( 3: 5, 4: 6) = 2200;  % CSF
% realMapsT1( 8:10, 9:10) =  600; realMapsT2( 8:10, 9:10) =   80;  % WM  
% realMapsT1(12:14,12:13) =  950; realMapsT2(12:14,12:13) =  100;  % GM
% For Nx=Ny=32
realMapsT1( 6: 10,  8: 12) = 4500; realMapsT2( 6: 10,  8: 12) = 2200; % CSF
realMapsT1(15: 19, 16: 19) =  600; realMapsT2(15: 19, 16: 19) =   80; % WM  
realMapsT1(25: 27, 23: 25) =  950; realMapsT2(25: 27, 23: 25) =  100; % GM


subplot(3,2,5)
imagesc(realMapsT1)
colormap jet
colorbar
axis square
title('Real T_1')

subplot(3,2,6)
imagesc(realMapsT2)
colormap jet
colorbar
axis square
title('Real T_2')


%%
% % % % % % % % % % % Qualitative comparison of images
figure

pause

for i = 1:NPulses
    currentP = squeeze(abs(imagesP(:,:,i)./maxVoxelP)); 
%     currentJ = squeeze(abs(imagesJ(:,:,i)./maxVoxelJ)); 
    
    % % JEMRIS
    subplot(1,3,1)
    imagesc(currentP, [0 1]), colormap gray
    axis square
    title({'JEMRIS', ['image no ', num2str(i)]})
    colorbar
    
    % % POSSUM
    subplot(1,3,2)
    imagesc(currentP, [0 1]), colormap gray
    axis square
    title({'POSSUM', ['image no ', num2str(i)]})
    colorbar
    
    % % DIFFERENCE
    subplot(1,3,3)
    diffPJ = currentP - currentP;
    imagesc(diffPJ), colormap gray
    axis square
    title({'POSSUM-JEMRIS', ['image no ', num2str(i)]})
    colorbar
    
    pause(0.1)
end


