% % % % IRINA GRIGORESCU
% % % % This script opens and edits a sequence produced by JEMRIS
% % % % and saves it as a sequence that can be used by POSSUM

WRITEFLAG = 1;
addpath ~/possumdevirina/

addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/possumHelpers/functions/'))

%%
for seqNo = 1:1 %28:30 %1:1 %1:27 %48 %31:48

% Pulse sequence created by JEMRIS
% filenameJ = ['~/jemrisSims/spiral/spiralFull1TR48Rot12TubesR3Spins348Sequences/seq', num2str(seqNo), '.h5']; %'seq1EPI.h5';
% filenameJ = ['~/jemrisSims/spiral/spiralFull500TR48Rot12TubesR3Spins348aggregated/seq', num2str(seqNo), '.h5']; %'seq1EPI.h5';
filenameJ = '/Users/irina/OneDrive - University College London/Work/JEMRISSimulations/feb2018/seqForPOSSUM/seq.h5';

t   = h5read(filenameJ,'/seqdiag/T');    % Timepoints (ms)
RXP = h5read(filenameJ,'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
TXM = h5read(filenameJ,'/seqdiag/TXM');  % RF pulse FA  in rad
TXP = h5read(filenameJ,'/seqdiag/TXP');  % RF pulse PhA in rad
GX  = h5read(filenameJ,'/seqdiag/GX');   % GX (mT/m)
GY  = h5read(filenameJ,'/seqdiag/GY');   % GY (mT/m)
GZ  = h5read(filenameJ,'/seqdiag/GZ');   % GZ (mT/m)
KX  = h5read(filenameJ,'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(filenameJ,'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(filenameJ,'/seqdiag/KZ');   % KZ (rad/mm)
B   = h5read(filenameJ,'/seqdiag/META');
pulseSequenceJ = [t./1000, TXM      , zeros(size(TXM)), ...
                                      zeros(size(TXM)), ...
                  RXP+1   , GX./1000, ...
                            GY./1000, ...
                            GZ./1000];

% Pulse sequence for POSSUM
% 8 columns = tp(s), RFangle(rad), RFfreq(Hz), RFcentreFreq(Hz), R/O,
%             Gx, Gy, Gz
% 1. Timepoints
pulseSequencePOSSUM = zeros(size(t,1),8); 
pulseSequencePOSSUM(:, 1) = t./1000;      % tp (s)
% 2. RF pulses
% The rf pulses are always 3 consecutive number
% you want to keep just middle number
nonZeroIndices = find((TXM)~=0);
indicesOfValuesToBeSetToZero = [nonZeroIndices(1:3:end) ; ...
                                nonZeroIndices(3:3:end)];
pulseSequencePOSSUM(:, 2) = TXM./10;      % RF angle (rad)
pulseSequencePOSSUM(indicesOfValuesToBeSetToZero, 2) = 0;

% 3. 4. RF frequency BW (Hz) and RFcentreFreq (Hz)
nonZeroIndices = find((pulseSequencePOSSUM(:, 2))~=0);
pulseSequencePOSSUM(nonZeroIndices, 3) = 1; %1.821061401367188e+03; %3.035102539062500e+02; %
pulseSequencePOSSUM(nonZeroIndices, 4) = 0; %7.587756347656250e+02; %0.000000072079815e+02; %
% 5. Readout
readoutIdx = find(RXP==0);
pulseSequencePOSSUM(readoutIdx, 5) = 1;
% 6. Gx
pulseSequencePOSSUM(:, 6) = GX./1000;
% 7. Gy
pulseSequencePOSSUM(:, 7) = GY./1000;
% 8. Gz
pulseSequencePOSSUM(:, 8) = GZ./1000;

figHand22 = figure(222);
func_plotPulseSequence(pulseSequencePOSSUM, figHand22, 'POSSUM');

% Save the sequence
write_pulse(['/Users/irina/simdirWithJEMRISseq/outPulses/', ...
             'pulse_spiralFull1TR'], ...
              pulseSequencePOSSUM, 1);
         
end

% % % % %%
% % % % % Pulse sequence created by POSSUM
% % % % filenameP = 'pulse_spiralFull500TR48RotSeq45';
% % % % 
% % % % pulseSequenceP = read_pulse(['~/simdirSpiral/outPulses/', filenameP]);
% % % % 
% % % % % Plot Pulse Sequences BEFORE
% % % % figHand1 = figure(11);
% % % % func_plotPulseSequence(pulseSequenceP, figHand1, 'POSSUM');
% % % % 
% % % % % % % % Make Jemris pulse sequence similar to possum one
% % % % % % % pulseSequenceJ(2, 2:4) = pulseSequenceP(3, 2:4); % copy the rf stuff (No G)
% % % % % % % pulseSequenceJ(3, 2:8) = zeros(1,7);
% % % % % % % pulseSequenceJ(4, 2:8) = zeros(1,7);
% % % % % % % 
% % % % % % % % Plot Pulse Sequences AFTER
% % % % % % % figHand3 = figure;
% % % % % % % func_plotPulseSequence(pulseSequenceP, figHand3);
% % % % % % % figHand4 = figure;
% % % % % % % func_plotPulseSequence(pulseSequenceJ, figHand4);


%% Save this new pulse 
% It's not balanced on Gz, maybe not even on Gx or Gy, so look into that
trial = 1;
% fileNameNewPulse = ['~/simdir/pulseEPI32_J', num2str(trial)];
fileNameNewPulse = ['/Users/irina/simdirWithJEMRISseq/outPulses/', ...
                    'pulse_spiralFull1TR'];

% % 1. Write pulse sequence
if WRITEFLAG == 1
    write_pulse(fileNameNewPulse, pulseSequenceJ, 1);
end
 
% % 2. Write pulse.info
if WRITEFLAG == 1
   SeqType = 3;
   TE  = 0.06; % (s)
   TR  = 0.15; % (s)
   TRslc = 0.06;  % (s)
   Nx = 60; 
   Ny = 100; 
   dx = 0.001; 
   dy = 0.001; 
   maxG = 500; %0.055;
   RiseT = 0.00022;
   BW = 100000;
   Nvol = 1; 
   Nslc = 1;
   SlcThk = 0.1;
   SlcDir = 1;
   Gap = 0;
   zstart = 0;
   FA = 90;
   phasedir = 2;
   readdir = 3;
   kspace_coverage = 100;
   startkspace = 1;
   nPulses = 10;
   
   fileID = fopen([fileNameNewPulse, '.info'],'w');
   fprintf(fileID, '%.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f\n', ...
       SeqType, TE, TR, TRslc, Nx, Ny, dx, dy, ... % 8
       maxG, RiseT, BW, Nvol, Nslc, SlcThk, SlcDir, Gap, ... % 8
       zstart, FA, phasedir, readdir, kspace_coverage, startkspace, ... % 6 
       nPulses); % npulse
   fclose(fileID);
end

% % 3. Write pulse.readme
if WRITEFLAG == 1
    fileID = fopen([fileNameNewPulse, '.readme'],'w');
    
    fprintf(fileID, '1. SeqType = epib\n');
    fprintf(fileID, '2. TE = +6.000000e-02(s)\n');
    fprintf(fileID, '3. TR = +15.000000e-02(s)\n');
    fprintf(fileID, '4. TRslc = +6.000000e-02(s)\n');
    fprintf(fileID, '5. Nread = +60\n');
    fprintf(fileID, '6. Nphase = +100\n');
    fprintf(fileID, '7. dread = +1.000000e-03(m)\n');
    fprintf(fileID, '8. dphase = +1.000000e-03(m)\n');
    fprintf(fileID, '9. maxG = +50000.00000e-02(T/m)\n');
    fprintf(fileID, '10. RiseT = +2.200000e-04(s)\n');
    fprintf(fileID, '11. BWrec = +1.000000e+05(Hz)\n');
    fprintf(fileID, '12. Nvol = +1\n');
    fprintf(fileID, '13. Nslc = +1\n');
    fprintf(fileID, '14. SlcThk = +1.000000e-01(m)\n');
    fprintf(fileID, '15. SlcDir = z+\n');
    fprintf(fileID, '16. Gap = +0.000000e+00(m)\n');
    fprintf(fileID, '17. zstart = +0.000000e+00(m)\n');
    fprintf(fileID, '18. FlipAngle = +9.0e+01(degrees)\n');
    fprintf(fileID, '19. PhaseDir = y+\n');
    fprintf(fileID, '20. ReadDir = x+\n');
    fprintf(fileID, '21. kspace coverage (phase) = +1.000000e+02(%%)\n');
    fprintf(fileID, '22. first non-zero phase line = +1\n');
    fprintf(fileID, '23. Number of pulses = 10\n');
end


% % 4. Write the .posx, .posy, .posz files
if WRITEFLAG == 1
    posx = [-0.09  -0.089  -0.088  -0.087  -0.086  -0.085  -0.084  -0.083  -0.082  -0.081  -0.08  -0.079  -0.078  -0.077  -0.076  -0.075  -0.074  -0.073  -0.072  -0.071  -0.07  -0.069  -0.068  -0.067  -0.066  -0.065  -0.064  -0.063  -0.062  -0.061  -0.06  -0.059  -0.058  -0.057  -0.056  -0.055  -0.054  -0.053  -0.052  -0.051  -0.05  -0.049  -0.048  -0.047  -0.046  -0.045  -0.044  -0.043  -0.042  -0.041  -0.04  -0.039  -0.038  -0.037  -0.036  -0.035  -0.034  -0.033  -0.032  -0.031  -0.03  -0.029  -0.028  -0.027  -0.026  -0.025  -0.024  -0.023  -0.022  -0.021  -0.02  -0.019  -0.018  -0.017  -0.016  -0.015  -0.014  -0.013  -0.012  -0.011  -0.01  -0.009  -0.008  -0.007  -0.006  -0.005  -0.004  -0.003  -0.002  -0.001  0  0.001  0.002  0.003  0.004  0.005  0.006  0.007  0.008  0.009  0.01  0.011  0.012  0.013  0.014  0.015  0.016  0.017  0.018  0.019  0.02  0.021  0.022  0.023  0.024  0.025  0.026  0.027  0.028  0.029  0.03  0.031  0.032  0.033  0.034  0.035  0.036  0.037  0.038  0.039  0.04  0.041  0.042  0.043  0.044  0.045  0.046  0.047  0.048  0.049  0.05  0.051  0.052  0.053  0.054  0.055  0.056  0.057  0.058  0.059  0.06  0.061  0.062  0.063  0.064  0.065  0.066  0.067  0.068  0.069  0.07  0.071  0.072  0.073  0.074  0.075  0.076  0.077  0.078  0.079  0.08  0.081  0.082  0.083  0.084  0.085  0.086  0.087  0.088  0.089  0.09];
    posy = [-0.108  -0.107  -0.106  -0.105  -0.104  -0.103  -0.102  -0.101  -0.1  -0.099  -0.098  -0.097  -0.096  -0.095  -0.094  -0.093  -0.092  -0.091  -0.09  -0.089  -0.088  -0.087  -0.086  -0.085  -0.084  -0.083  -0.082  -0.081  -0.08  -0.079  -0.078  -0.077  -0.076  -0.075  -0.074  -0.073  -0.072  -0.071  -0.07  -0.069  -0.068  -0.067  -0.066  -0.065  -0.064  -0.063  -0.062  -0.061  -0.06  -0.059  -0.058  -0.057  -0.056  -0.055  -0.054  -0.053  -0.052  -0.051  -0.05  -0.049  -0.048  -0.047  -0.046  -0.045  -0.044  -0.043  -0.042  -0.041  -0.04  -0.039  -0.038  -0.037  -0.036  -0.035  -0.034  -0.033  -0.032  -0.031  -0.03  -0.029  -0.028  -0.027  -0.026  -0.025  -0.024  -0.023  -0.022  -0.021  -0.02  -0.019  -0.018  -0.017  -0.016  -0.015  -0.014  -0.013  -0.012  -0.011  -0.01  -0.009  -0.008  -0.007  -0.006  -0.005  -0.004  -0.003  -0.002  -0.001  0  0.001  0.002  0.003  0.004  0.005  0.006  0.007  0.008  0.009  0.01  0.011  0.012  0.013  0.014  0.015  0.016  0.017  0.018  0.019  0.02  0.021  0.022  0.023  0.024  0.025  0.026  0.027  0.028  0.029  0.03  0.031  0.032  0.033  0.034  0.035  0.036  0.037  0.038  0.039  0.04  0.041  0.042  0.043  0.044  0.045  0.046  0.047  0.048  0.049  0.05  0.051  0.052  0.053  0.054  0.055  0.056  0.057  0.058  0.059  0.06  0.061  0.062  0.063  0.064  0.065  0.066  0.067  0.068  0.069  0.07  0.071  0.072  0.073  0.074  0.075  0.076  0.077  0.078  0.079  0.08  0.081  0.082  0.083  0.084  0.085  0.086  0.087  0.088  0.089  0.09  0.091  0.092  0.093  0.094  0.095  0.096  0.097  0.098  0.099  0.1  0.101  0.102  0.103  0.104  0.105  0.106  0.107  0.108];
    posz = [-0.09  -0.089  -0.088  -0.087  -0.086  -0.085  -0.084  -0.083  -0.082  -0.081  -0.08  -0.079  -0.078  -0.077  -0.076  -0.075  -0.074  -0.073  -0.072  -0.071  -0.07  -0.069  -0.068  -0.067  -0.066  -0.065  -0.064  -0.063  -0.062  -0.061  -0.06  -0.059  -0.058  -0.057  -0.056  -0.055  -0.054  -0.053  -0.052  -0.051  -0.05  -0.049  -0.048  -0.047  -0.046  -0.045  -0.044  -0.043  -0.042  -0.041  -0.04  -0.039  -0.038  -0.037  -0.036  -0.035  -0.034  -0.033  -0.032  -0.031  -0.03  -0.029  -0.028  -0.027  -0.026  -0.025  -0.024  -0.023  -0.022  -0.021  -0.02  -0.019  -0.018  -0.017  -0.016  -0.015  -0.014  -0.013  -0.012  -0.011  -0.01  -0.009  -0.008  -0.007  -0.006  -0.005  -0.004  -0.003  -0.002  -0.001  0  0.001  0.002  0.003  0.004  0.005  0.006  0.007  0.008  0.009  0.01  0.011  0.012  0.013  0.014  0.015  0.016  0.017  0.018  0.019  0.02  0.021  0.022  0.023  0.024  0.025  0.026  0.027  0.028  0.029  0.03  0.031  0.032  0.033  0.034  0.035  0.036  0.037  0.038  0.039  0.04  0.041  0.042  0.043  0.044  0.045  0.046  0.047  0.048  0.049  0.05  0.051  0.052  0.053  0.054  0.055  0.056  0.057  0.058  0.059  0.06  0.061  0.062  0.063  0.064  0.065  0.066  0.067  0.068  0.069  0.07  0.071  0.072  0.073  0.074  0.075  0.076  0.077  0.078  0.079  0.08  0.081  0.082  0.083  0.084  0.085  0.086  0.087  0.088  0.089  0.09];
    
    dlmwrite([fileNameNewPulse, '.posx'], posx, ' ');
    dlmwrite([fileNameNewPulse, '.posy'], posy, ' ');
    dlmwrite([fileNameNewPulse, '.posz'], posz, ' ');
end









