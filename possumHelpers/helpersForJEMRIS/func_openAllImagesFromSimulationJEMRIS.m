function imagesAll = func_openAllImagesFromSimulationJEMRIS(Nx, Ny, ...
            folderName, fileName)
% % % % IRINA GRIGORESCU
% % % % Date created: 24-07-2017
% % % % Date updated: 24-07-2017
% % % % 
% % % % This function manipulates the output in image space of a JEMRIS
% % % % simulation. 
% % % % At the moment, it takes only channel 00 (single channel) 
% % % % 
% % % % INPUT:
% % % %     Nx/Ny = image size (because jemris output is just N x 3)
% % % %             where N is number of total ADC points
% % % % 
% % % %     folderName/fileName = folder and file name of simulation output
% % % % 
% % % % OUTPUT: 
% % % %     imagesAll = (Nx x Ny x Nimg) set of images 
% % % % 

% % THIS CODE IS TAKEN FROM JEMRIS MATLAB CODE

% % % Take everything from simulation output
% % % will be of size N x 3 (mx,my,mz)
A = (h5read([folderName, fileName], '/signal/channels/00'))';

% % % Get size of output (Nx3)
[N chs] = size(A); 
% % % Calculate total number of images
Nimgs   = N / (Nx*Ny); 

% % % Get timings of signal readout
t = h5read([folderName, fileName], '/signal/times');
% % % Sort them and take signal in sorted order
[t,J] = sort(t); M = A(J,:);

% % % Calculate timings between each line of k-space and 
% % % between different slices
d = diff(diff(t));
d(d<1e-5) = 0;
% % % And store them in the solumn vector of indices I
% % % together with index 0 and last index
I = [0; find(d) + 1; length(t)];

%copy + paste from single channel:       
S = [];
for i = 1:length(I)-1
    J = [I(i)+1:I(i+1)]';%[i length(J)]
    
    S(:,i) = M(J,1) + sqrt(-1)*M(J,2);
end

if ( size(S,2)>size(S,1) && mod(size(S,2),size(S,1)) == 0  ) 
    Nimg = size(S,2)/size(S,1);
    S = reshape(S,size(S,1),size(S,1),Nimg);
end

imagesAll = zeros(size(S));

%check for multiple images
for img_num = 1:Nimgs
    
    Ss = S(:,:,img_num);
    normal = Ss;
    
    Ss(:,1:2:end) = flipud(Ss(:,1:2:end));
   
    FS=(fft2(ifftshift(Ss))); %fliplr
    
    imagesAll(:,:,img_num) = fftshift((FS)); % deleted abs(FS)
   
%     figure
%     subplot(2,4,1)
%     imagesc(abs(normal)); axis square; colormap gray; colorbar
%     title('normal')
%     subplot(2,4,2)
%     imagesc(abs(Ss)); axis square; colormap gray; colorbar
%     title('flipped ud')
%     subplot(2,4,3)
%     imagesc(abs(Ss')); axis square; colormap gray; colorbar
%     title('transpose')
%     subplot(2,4,4)
%     imagesc(abs(ifftshift(Ss'))); axis square; colormap gray; colorbar
%     title('ifftshift')
%     subplot(2,4,5)
%     imagesc(abs(fft2(ifftshift(Ss')))); axis square; colormap gray; colorbar
%     title('fft2')
%     subplot(2,4,6)
%     imagesc(abs(fliplr(fft2(ifftshift(Ss'))))); axis square; colormap gray; colorbar
%     title('fliplr')
%     subplot(2,4,7)
%     imagesc(abs(fftshift(fliplr(fft2(ifftshift(Ss')))))); axis square; colormap gray; colorbar
%     title('fliplr')
%     
%     disp('done')
end

disp('done');

end 