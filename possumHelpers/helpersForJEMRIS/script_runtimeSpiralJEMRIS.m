% 
% IRINA GRIGORESCU, irina.grigorescu.15@ucl.ac.uk
% 
% In ~/jemrisSims/jan2018/spiralROtestTony/ you will find a script
% called createAFullySampledSpiralTestVersion.sh which can be run for 
% different numbers of TRs.
% 
% I ran this script for increasing number of TRs and timed its runtime:
% for i in 10 20 ; 
%  do ./createAFullySampledSpiralTestVersion.sh aFullySampledSpiral.xml $i ;
%  time jemris -f signals simu.xml ; 
%  echo -e "---> for $i" ; 
% done
% 
% This experiment was run for a homogeneous object of 45 spins.
% 
% The results are stored in this script.
% 

% % % % % % % % % % % % % % % % % % % % %
% % % Number of TRs considered for each experiment
N_TR  = [1  , 5,    10, 25,   50,   100, 150, 200, 250, 300, 350, 400, 450, 500];

% % % % % % % % % % % % % % % % % % % % % 
% % % After improvements from Tony
yTimeImproved45Spins  = [1.2,  3.3 ,  6,   14.7, 29.2,  61,  87, 108, 147, 181, 196, 227, 237, 283  ];
yTimeImproved147Spins = [2.22, 7.54, 15.3, 38.9, 87.1, 146, 202, 268, 334, 403, 466, 534, 595, 657];

% % % % % % % % % % % % % % % % % % % % % 
% % % Before improvements from Tony
yTime45Spins = [1.5, 8.86, 16, 42.8, 81, 166, 256, 328, 411, 522, 629, 665, 769, 840 ];


%%
% Plot figure with runtime results
figure

% % % 
% % % First line: before and after improvements
% % % 
subplot(2,2,1)
% After improvement
plot(N_TR, yTimeImproved45Spins, 'o-b'); hold on
% Before improvement
plot(N_TR, yTime45Spins, 'o-r')
% Legend
legend('After improvement', 'Before improvement');
% Properties of axis
xlim([0, 500]); ylim([0 1000]);
xlabel('#pulses'); ylabel('runtime (s)');

% % % Plot runtime in seconds divided by the respective N 
% % % for each N TRs simulated (for liniar should be a constant value)
subplot(2,2,2)
% After improvement
plot(N_TR, yTimeImproved45Spins./N_TR, 'o-b'); hold on
% Before improvement
plot(N_TR, yTime45Spins./N_TR, 'o-r')
legend('After improvement', 'Before improvement');
% Properties of axis
xlim([0, 500]);
xlabel('#pulses'); ylabel('runtime (s) / #pulses');


% % % 
% % % Second line: after improvements 45 spins vs 147 spins
% % % 
subplot(2,2,3)
%  45 spins
plot(N_TR, yTimeImproved45Spins, 'o-b'); hold on
% 147 spins
plot(N_TR, yTimeImproved147Spins, 'o-k')
% Legend
legend('45 spins', '147 spins');
% Properties of axis
xlim([0, 500]); ylim([0 1000]);
xlabel('#pulses'); ylabel('runtime (s)');


% % % 
subplot(2,2,4)
%  45 spins
plot(N_TR, yTimeImproved45Spins./N_TR, 'o-b'); hold on
% 147 spins
plot(N_TR, yTimeImproved147Spins./N_TR, 'o-r')
legend('45 spins', '147 spins');
% Properties of axis
xlim([0, 500]);
xlabel('#pulses'); ylabel('runtime (s) / #pulses');

