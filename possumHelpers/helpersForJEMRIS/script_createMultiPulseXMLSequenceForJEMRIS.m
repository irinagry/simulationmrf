% % % IRINA GRIGORESCU
% % % SCRIPT to create a multi-pulse sequence xml file for JEMRIS
% % % 

fileID = fopen('~/jemrisSims/mySequences/10RFBlocksRND1.xml','w');

header1 = '<?xml version="1.0" encoding="utf-8"?>';
header2 = ['<Parameters FOVx="256" FOVy="256" GradMaxAmpl="1" ', ... 
           'GradSlewRate="10" Name="P" Nx="1000" Ny="0" TE="1000" TR="1000">'];
header3 = '<ConcatSequence Name="C0" Repetitions="1">';
footer1 = '</ConcatSequence>';
footer2 = '</Parameters>';

idx = 1;
line0 = ['<ConcatSequence Name="C',num2str(idx),'" Repetitions="1">'];
line1 = ['<AtomicSequence Name="A',num2str(idx),'">'];
line2 = ['<HARDRFPULSE Axis="RF" Duration="0.001" FlipAngle="', ...
          num2str(45),'" InitialPhase="0" Name="P', num2str(idx),'"/>'];
line3 = '</AtomicSequence>';
line4 = ['<ConcatSequence Name="CC',num2str(idx),'">'];
line5 = ['<AtomicSequence Name="AA',num2str(idx),'">'];
line6 = ['<EMPTYPULSE ADCs="1" Axis="NONE" Duration="', ...
         num2str(60),'" InitialDelay="0" Name="PP',num2str(idx),'"/>'];
line7 = '</AtomicSequence>';
line8 = '</ConcatSequence>';
line9 = ['<DELAYATOMICSEQUENCE Delay="0" DelayType="B2E" Name="D', ...
          num2str(idx),'"/>'];
line10 = '</ConcatSequence>';

% % WRITE HEADER
fprintf(fileID, '%s\n%s\n%s\n', ...
       header1, header2, header3);

M = dlmread('~/simdir/sequenceList');
TRs = M(:,1).*1000;
FAs = M(:,2);
   
% % REPEAT BLOCKS
for idx = 1:10
    % PREPARE LINES
    line0 = ['<ConcatSequence Name="C',num2str(idx),'" Repetitions="1">'];
    line1 = ['<AtomicSequence Name="A',num2str(idx),'">'];
    line2 = ['<HARDRFPULSE Axis="RF" Duration="0.001" FlipAngle="', ...
              num2str(FAs(idx)), ...
              '" InitialPhase="0" Name="P', num2str(idx),'"/>'];
    line3 = '</AtomicSequence>';
    line4 = ['<ConcatSequence Name="CC',num2str(idx),'">'];
    line5 = ['<AtomicSequence Name="AA',num2str(idx),'">'];
    line6 = ['<EMPTYPULSE ADCs="1" Axis="NONE" Duration="', ...
             num2str(TRs(idx)), ...
             '" InitialDelay="0" Name="PP',num2str(idx),'"/>'];
    line7 = '</AtomicSequence>';
    line8 = '</ConcatSequence>';
    line9 = ['<DELAYATOMICSEQUENCE Delay="0" DelayType="B2E" Name="D', ...
              num2str(idx),'"/>'];
    line10 = '</ConcatSequence>';          

	% WRITE LINES
    fprintf(fileID, '%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n', ...
       line0, line1, line2, line3, ...
       line4, line5, line6, ...
       line7, line8, line9, line10);

end


% % WRITE FOOTER
fprintf(fileID, '%s\n%s\n', ...
       footer1, footer2);

    
   
   
   
   