% 
% Irina Grigorescu
% Date created: 13-12-2017
% 
% Script to reconstruct a JEMRIS multi-pulse simulation with BART
% 

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% 1. Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/epi/';

% Set number of repetitions to get to the fully sampled
NReps = 1;
% Set number of TR blocks
NTRs  = 50; 
% Set effective image size
Nx   = 32; Ny = Nx; 
% Set number of redout points per TR
nROPoints = Nx*Ny;

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NReps, NTRs);
kspace_z = zeros(nROPoints*NReps, 1);

for i = 1:1 %NReps
    % Get K-space coordinates from JEMRIS simulation
    % along with other sequence information
    %filenameSequence = [folderName, 'seq', num2str(i),'.h5']; 
    filenameSequence = [folderName, 'seq.h5']; 
    RXP = h5read(filenameSequence,'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
    KX  = h5read(filenameSequence,'/seqdiag/KX');   % KX (rad/mm)
    KY  = h5read(filenameSequence,'/seqdiag/KY');   % KY (rad/mm)
    KZ  = h5read(filenameSequence,'/seqdiag/KZ');   % KZ (rad/mm)
   
    % Calculate number of readout points - this will give you 300
    if nROPoints ~= (size(RXP(RXP==0),1) / NTRs)
        disp('Warning - unexpected number of READOUT points');
    end

    % Retrieve all k-space coordinates where READOUT has happened
    kspx = KX(RXP==0); kspy = KY(RXP==0);

    % For each TR block calculate the kspace coordinates by bringing them back
    % to 1/cm from rad/mm
    % My signals will be in the form of NRO x NReps x NTRs 
    % for example, my signals will be   300 x  48   x  5  
    for j = 1:NTRs
        % NROpoints x Nrepetitions x NTRs
        kspace_x(:,i,j) = (kspx((j-1)*nROPoints+1 : j*nROPoints));
                           %./ (2*pi)) .* 1E+01; %cm^-1
        kspace_y(:,i,j) = (kspy((j-1)*nROPoints+1 : j*nROPoints));
                           %./ (2*pi)) .* 1E+01; %cm^-1
    end
    
end


% Reshape to fully sampled data
kspace_x = reshape(kspace_x, [nROPoints*NReps, NTRs]);
kspace_y = reshape(kspace_y, [nROPoints*NReps, NTRs]);

% Plot them for each spiral turn
figure(111)
for i = 1:NTRs
    % Plot the coordinates before scaling
    subplot(1,2,1); xl = 4.5;
    scatter(kspace_x(:, i),kspace_y(:, i),'.'), axis equal, axis square;
    hold on
    scatter(kspace_x(64, i),kspace_y(64, i),'o'), axis equal, axis square;
    scatter(kspace_x( 1, i),kspace_y( 1, i),'o'), axis equal, axis square;
    xlabel('k_x rad mm^{-1}'); ylabel('k_y rad mm^{-1}');
    title(['EPI ', num2str(i)])
    xlim([-xl xl]); ylim([-xl xl]);

    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp_x = sqrt(min(kspace_x(:,i) - max(kspace_x(:,i))).^2);
    dist_ksp_y = sqrt(min(kspace_y(:,i) - max(kspace_y(:,i))).^2);
    
    % Scale the values
    kspace_x(:,i) = (Nx-1) .* (kspace_x(:,i) ./ dist_ksp_x);
    kspace_y(:,i) = (Ny-1) .* (kspace_y(:,i) ./ dist_ksp_y);
    
    % Shifts in x and y
    shift_x = min(kspace_x(kspace_x > 0));
    shift_y = min(kspace_y(kspace_y > 0));
    
    % Shift the values
    kspace_x(:,i) = round(kspace_x(:,i) - shift_x);
    kspace_y(:,i) = round(kspace_y(:,i) - shift_y - 1);
    
    % Plot the coordinates after scaling
    subplot(1,2,2)
    scatter(kspace_x(:, i), kspace_y(:, i),'.'), axis equal, axis square;
    xlabel('N_x'); ylabel('N_y');
    title(['EPI ', num2str(i)])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

    drawnow
end

kkkx = kspace_x(:,1);
kkky = kspace_y(:,1);

%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs

for i = 1:NReps
    % Get signal values from JEMRIS simulation
    %filenameSignal = [folderName, 'signals', num2str(i),'.h5']; 
    filenameSignal = [folderName, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Get timings of signal readout
    timingRO = h5read(filenameSignal, '/signal/times');
    % % % Readout points
    Nro = size(timingRO, 1);
    % % % Sort them and take signal in sorted order
    [timingRO,J] = sort(timingRO); Mvecs = A(:,:);
    % % % Calculate timings between each line of k-space and
    % % % between different slices
    d = diff(diff(timingRO));
    d(d<1e-5) = 0;

    % % % And store them in the solumn vector of indices I
    % % % together with index 0 and last index
    I = [0; find(d) + 1; length(timingRO)];

    % Get kspace as one signal
    for j = 1:NTRs
        kspaces(:,i,j) = Mvecs((j-1)*nROPoints+1 : j*nROPoints, 1).' + ...
              sqrt(-1) * Mvecs((j-1)*nROPoints+1 : j*nROPoints, 2).';

    end
end

% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints*NReps, NTRs]);

%% I want to see the order of acquisition
figure
for i = 1:Nx*Ny
    scatter(kkkx(i),kkky(i)); hold on
    xlim([-16 16]); ylim([-16 16]);
    pause(0.01);
end


%% 4. Get the reconstructed image from the JEMRIS simulation
imagesJEMRIS = func_openAllImagesFromSimulationJEMRIS(Nx, Ny, ...
                                    folderName, 'signals.h5');

%% 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);

% Save signal from image space from one voxel from the image
signalImage = zeros(NTRs, 1);

figure(222)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = squeeze(kspace_x(:, i));
    currentKspace_y = squeeze(kspace_y(:, i));
    currentKspaceToReco = squeeze(kspaces(:, i)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    imagesBART(:, :, i) = flipud(rot90(igrid));
    toc

    % Plot images BART
    subplot(2,3,1), imagesc(abs(igrid))
    title(['BART reconstruction ', num2str(i), ' (abs)'])
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,2), imagesc(real(igrid))
    title('BART reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,3), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    
    % Plot images JEMRIS
    subplot(2,3,4), imagesc(abs(imagesJEMRIS(:,:,i)))
    title(['JEMRIS reconstruction ', num2str(i), ' (abs)'])
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,5), imagesc(real(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,6), imagesc(imag(imagesJEMRIS(:,:,i)))
    title('JEMRIS reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    pause(0.01)
end

 

%% 5. Compare the 2 reconstructions
% % % % Normalize both types of reconstructed images
% JEMRIS
[~, imagesJEMRISNorm] = normalizeSignals( ...
                    [ real(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesJEMRIS, [Nx*Ny, NTRs])) ]);
imagesJEMRISNorm = (reshape(imagesJEMRISNorm, [Nx, Ny, NTRs])); %conj


% BART
[~, imagesBARTNorm] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBARTNorm = reshape(imagesBARTNorm, [Nx, Ny, NTRs]);
           
%%

figure('Position', [10,10,1200,1200]);
lima = 0.055;

for i = 1:NTRs
    % BART RECONSTRUCTION
    % REAL
    subplot(3,3,1)
    imagesc(real(squeeze(imagesBARTNorm(:,:,i))));
    title('Normalised BART reco (real)')
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % IMAG
    subplot(3,3,2)
    imagesc(angle(squeeze(imagesBARTNorm(:,:,i))));
    title(['Normalised BART reco (imag) ', num2str(i)])
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % ABS
    subplot(3,3,3)
    imagesc(abs(squeeze(imagesBARTNorm(:,:,i))));
    title('Normalised BART reco (abs)')
    axis square; axis off; colorbar
%     caxis([-lima lima])

    % JEMRIS RECONSTRUCTION
    % REAL
    subplot(3,3,4)
    imagesc(real(squeeze(imagesJEMRISNorm(:,:,i))));
    title('Normalised JEMRIS reco (real)')
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % IMAG
    subplot(3,3,5)
    imagesc(angle(squeeze(imagesJEMRISNorm(:,:,i))));
    title('Normalised JEMRIS reco (imag)')
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % ABS
    subplot(3,3,6)
    imagesc(abs(squeeze(imagesJEMRISNorm(:,:,i))));
    title('Normalised JEMRIS reco (abs)')
    axis square; axis off; colorbar
%     caxis([-lima lima])


    % DIFFERENCE IN RECONSTRUCTIONS
    % REAL
    subplot(3,3,7)
    imagesc(real(squeeze(imagesJEMRISNorm(:,:,i))) - ...
            real(squeeze(imagesBARTNorm(:,:,i))));
    title('JEMRIS - BART reco (real)')
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % IMAG
    subplot(3,3,8)
    imagesc(imag(squeeze(imagesJEMRISNorm(:,:,i))) - ...
            imag(squeeze(imagesBARTNorm(:,:,i))));
    title('JEMRIS - BART reco (imag)')
    axis square; axis off; colorbar
%     caxis([-lima lima])
    % ABS
    subplot(3,3,9)
    imagesc(abs(squeeze(imagesJEMRISNorm(:,:,i))) - ...
            abs(squeeze(imagesBARTNorm(:,:,i))));
    title('JEMRIS - BART reco (abs)')
    axis square; axis off; colorbar
%     caxis([-lima lima])
    
    pause(0.01)
end

figure
subplot(1,3,1)
hist(real(imagesJEMRISNorm(:))-real(imagesBARTNorm(:)),Nx*Ny)
title('JEMRIS - BART reco (real)')


subplot(1,3,2)
hist(imag(imagesJEMRISNorm(:))-imag(imagesBARTNorm(:)),Nx*Ny)
title('JEMRIS - BART reco (imag)')

subplot(1,3,3)
hist(abs(imagesJEMRISNorm(:))-abs(imagesBARTNorm(:)),Nx*Ny)
title('JEMRIS - BART reco (abs)')   


%% Comparison with dictionary
% % % % % % % % % % % Generate dictionary with my ALGORITHM
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Sequence
FA_mrf  =  zeros(NTRs,1) +  90;
PhA_mrf =  zeros(NTRs,1);
TR_mrf  =  zeros(NTRs,1) + 120;
RO_mrf  =  zeros(NTRs,1) +  60;

% % % % Custom Material
T1_mrf = [800, 1000, 1200, 1400, 1600, 1800, 2000];
T2_mrf = [ 500,  600];
df_mrf =   0;

% % % % Generate dictionary
% % % % Custom Sequence
customSequence.RF.FA  =  FA_mrf;
customSequence.RF.PhA =  PhA_mrf;
customSequence.TR     =  TR_mrf;
customSequence.RO     =  RO_mrf;

% % % % Custom Material
customMaterial.T1     = T1_mrf;
customMaterial.T2     = T2_mrf;
customMaterial.offRes = df_mrf;
tic
[materialProperties, sequenceProperties, ...
    materialTuples, M, ...
    dictionaryMRF] = ...
                 simulateMRF(NTRs, FLAGPLOT, ...
                 SEQFLAG, customSequence, ...
                 MATFLAG, customMaterial, 0);
t=toc;
% Get the x and y values so that you have it as complex
dictionaryMRFComplex = [squeeze(dictionaryMRF(:,:,1)), ...
                        squeeze(dictionaryMRF(:,:,2))];
% % % % Normalize dictionary:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[dictionaryMRFNorm2Ch, dictionaryMRFNorm] = ...
        normalizeSignals(dictionaryMRFComplex);
   
%%
% % % % % % % % % % % % Dictionary Matching
% % % % Matching ALGORITHM 
% % % % % % Match JEMRIS with dictionary
imagesJEMRISReshaped = reshape(imagesJEMRISNorm, [Nx*Ny, NTRs]);
[valuesOfMatchJ, indicesMatchJ, matTuplesJ] = ...
                         matching(dictionaryMRFNorm, ...
                                  imagesJEMRISReshaped, ...
                                  materialTuples, 0);
mapMatchValuesJ = reshape(valuesOfMatchJ, [Nx,Ny]);


% % % % Match BART with dictionary
imagesBARTReshaped = reshape(imagesBARTNorm, [Nx*Ny, NTRs]);
[valuesOfMatchB, indicesMatchB, matTuplesB] = ...
                         matching(dictionaryMRFNorm, ...
                                  imagesBARTReshaped, ...
                                  materialTuples, 0);
mapMatchValuesB = reshape(valuesOfMatchB, [Nx,Ny]);                                          
   
%% Plot dictionary signal vs image signal
idxToPlot = (Nx/2)*Ny+(Nx/2);
nLin = 4; nCol = 3;

figure
% % % % % % % % % % BART
subplot(nLin,nCol,1), hold on
plot(1:NTRs, abs(imagesBARTReshaped(idxToPlot,:))) 
plot(1:NTRs, abs(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'--')
legend('abs(BART)','abs(dictionary)')

subplot(nLin,nCol,2), hold on
plot(1:NTRs, real(imagesBARTReshaped(idxToPlot,:))) 
plot(1:NTRs, real(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'--')
legend('real(BART)','real(dictionary)')

subplot(nLin,nCol,3), hold on
plot(1:NTRs, imag(imagesBARTReshaped(idxToPlot,:))) 
plot(1:NTRs, imag(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'--')
legend('imag(BART)','imag(dictionary)')

% % % % % % % % % % BART DIFFERENCES
subplot(nLin,nCol,4), hold on
plot(1:NTRs, abs(imagesBARTReshaped(idxToPlot,:)) - ...
             abs(dictionaryMRFNorm(indicesMatchB(idxToPlot),:))) 
legend('abs(BART)-abs(dictionary)')

subplot(nLin,nCol,5), hold on
plot(1:NTRs, real(imagesBARTReshaped(idxToPlot,:)) - ...
             real(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)))
legend('real(BART)-real(dictionary)')

subplot(nLin,nCol,6), hold on
plot(1:NTRs, imag(imagesBARTReshaped(idxToPlot,:)) - ...
             imag(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)))
legend('imag(BART)-imag(dictionary)')


% % % % % % % % % % JEMRIS
subplot(nLin,nCol,7), hold on
plot(1:NTRs, abs(imagesJEMRISReshaped(idxToPlot,:))) 
plot(1:NTRs, abs(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'.--')
legend('abs(JEMRIS)','abs(dictionary)')

subplot(nLin,nCol,8), hold on
plot(1:NTRs, real(imagesJEMRISReshaped(idxToPlot,:))) 
plot(1:NTRs, real(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'.--')
legend('real(JEMRIS)','real(dictionary)')

subplot(nLin,nCol,9), hold on
plot(1:NTRs, imag(imagesJEMRISReshaped(idxToPlot,:))) 
plot(1:NTRs, imag(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)),'.--')
legend('imag(JEMRIS)','imag(dictionary)')

% % % % % % % % % % JEMRIS DIFFERENCES
subplot(nLin,nCol,10), hold on
plot(1:NTRs, abs(imagesJEMRISReshaped(idxToPlot,:)) - ... 
             abs(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)))
legend('abs(JEMRIS)-abs(dictionary)')

subplot(nLin,nCol,11), hold on
plot(1:NTRs, real(imagesJEMRISReshaped(idxToPlot,:)) - ...
	         real(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)))
legend('real(JEMRIS)-real(dictionary)')

subplot(nLin,nCol,12), hold on
plot(1:NTRs, imag(imagesJEMRISReshaped(idxToPlot,:)) - ...
             imag(dictionaryMRFNorm(indicesMatchB(idxToPlot),:)))
legend('imag(JEMRIS)-imag(dictionary)')

%%   
% % % % % % % % % % % % Plot results of matching
% % % % MASK Nx x Ny
maskForImages = createCirclesMask(ones(Nx,Ny), [Nx/2, Ny/2], 10);
% % % % Maps of T1 and T2 for both epg and possum
mapT1J = reshape(matTuplesJ.T1, [Nx,Ny]);
mapT2J = reshape(matTuplesJ.T2, [Nx,Ny]);
mapMaJ = reshape(valuesOfMatchJ, [Nx,Ny]);
mapT1B = reshape(matTuplesB.T1, [Nx,Ny]);
mapT2B = reshape(matTuplesB.T2, [Nx,Ny]);
mapMaB = reshape(valuesOfMatchB, [Nx,Ny]);

% % % % PLOT MAPS
figure('Position', [10,10,1400,1000])
subplot(2,2,1)
imagesc(mapT1J .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 2000])
title('T_1 map (JEMRIS)')

subplot(2,2,2)
imagesc(mapT2J .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 1000])
title('T_2 map (JEMRIS)')

subplot(2,2,3)
imagesc(mapT1B .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 2000])
title('T_1 map (BART)')

subplot(2,2,4)
imagesc(mapT2B .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0 1000])
title('T_2 map (BART)')

% % % % Match results
figure('Position', [10,10,1400,1000])
subplot(1,2,1)
imagesc(mapMaJ .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0.9 1])
title('Values of Matching (JEMRIS)')

subplot(1,2,2)
imagesc(mapMaB .* maskForImages)
axis off, axis square
colormap hot
colorbar, caxis([0.9 1])
title('Values of Matching (BART)')

   