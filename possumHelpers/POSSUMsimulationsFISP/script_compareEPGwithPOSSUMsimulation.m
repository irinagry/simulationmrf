% % IRINA GRIGORESCU
% % 
% % In this script I am comparing a POSSUM fisp simulation with an EPG
% % simulation
% % 

% % % % Preprocessing
% Addpaths for EPG comparison
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/extendedPhaseGraph/'))

% Addpaths for helpers
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/helpers/'))
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/preprocess/'))

% Addpaths for algorithms (such as normalize signals)
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/algs/'))

ROOTFOLDER = '/Users/irina/OneDrive - University College London/Work/POSSUMSimulations/';


%% Prerequisites
% We have a material with high T1 and T2 such that relaxation is not
% something to consider

% Materials:
T1 = 500; 
T2 = 360;
% And a sequence with consecutive pulses of 90 degrees
NTRs  = 10;
PA    = zeros(NTRs,1);
FA    = zeros(NTRs,1) + 90;
TR    = zeros(NTRs,1) + 15;
TE    = zeros(NTRs,1) +  6;


%% Generate EPG signal
% Run EPG
[ChiF, ChiZ] = epgMatrix(NTRs, PA, FA, TR, T1, T2, 1, 1); % M0, flagDephase

F0states = ChiF(NTRs, :) .* exp(-TE ./ T2).';
sigEPG = abs(F0states);


%% Get the possum signal
filenamePOSSUM = [ROOTFOLDER, 'simdirFISP/outSignal/signal_fisp_'];

sigPOSSUMCplx = zeros(1, NTRs);

% % % Get kspace as one signal
for idxTR = 1:NTRs
    % % % % % % Get signal values from POSSUM simulation
    filenameSignal = [filenamePOSSUM, num2str(idxTR)]; 
    Mvecs = read_pulse(filenameSignal);
    
    % Get signal values
    sigPOSSUMCplx(:,idxTR) = Mvecs(1, :) + sqrt(-1) * Mvecs(2, :);
    
end

sigPOSSUMCplx = sigPOSSUMCplx ./ 1201; %90601; %301;

figure
plot(real(sigPOSSUMCplx(:))) ; hold on
plot(imag(sigPOSSUMCplx(:))) ;
legend('real','imag')

sigPOSSUM = abs(sigPOSSUMCplx);


%%
figure('Position', [5,5,1400,1200])

% Signal EPG and signal dictionary
subplot(4,2,[1,2])
plot(1:NTRs, sigEPG, 'r'); hold on
plot(1:NTRs, sigPOSSUM, 'g--'); 
legend('EPG', 'netSig')
title({['EPG vs Net Magnetization of '], ...
        ['T_1 = ', num2str(T1), ' T_2 = ', num2str(T2)], ...
        ['TR = ', num2str(TR(1)), ' ms TE = ', num2str(TE(1)), ' ms']})
xlabel('TR block number'); ylabel('signal');

% Signal EPG and signal dictionary by channels
subplot(4,2,[3,4])
plot(1:2*NTRs, [real(F0states) imag(F0states)], 'r'); hold on
plot(1:2*NTRs, [real(sigPOSSUMCplx) imag(sigPOSSUMCplx)], 'g--'); 
legend('EPG', 'netSig')
xlabel('TR block number'); ylabel('signal');

% Difference between signals
subplot(4,2,[5,6])
plot(1:NTRs, sigEPG-sigPOSSUM, 'r'); hold on
xlabel('TR block number'); ylabel('signal_{EPG} - signal_{net}');

% Angle
subplot(4,2,[7,8])
plot(1:NTRs, angle(F0states), 'r'); hold on
plot(1:NTRs, angle(sigPOSSUMCplx), 'g--'); 
xlabel('TR block number'); ylabel('phase of signal');
