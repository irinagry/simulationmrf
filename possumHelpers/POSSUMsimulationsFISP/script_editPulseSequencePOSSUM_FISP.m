% % % % IRINA GRIGORESCU
% % % % This script edits a possum pulse sequence to make it just RO


addpath ~/possumdevirina/

addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/possumHelpers/functions/'))

ROOTFOLDER = '/Users/irina/OneDrive - University College London/Work/POSSUMSimulations/';

gama = 2.68*1e+08;

%% % Pulse sequence created by POSSUM
filenamePOSSUM = [ROOTFOLDER, 'simdirFISP/outPulses/pulse_fisp1TR'];

% Read the sequence
pulseSequencePOSSUM = read_pulse(filenamePOSSUM);
[nL, nC] = size(pulseSequencePOSSUM);

% Set GX and GY to 0 everywhere
pulseSequencePOSSUM(:,6) = 0;
pulseSequencePOSSUM(:,7) = 0;
pulseSequencePOSSUM(1:100,8) = 0;
pulseSequencePOSSUM(:,6) = pulseSequencePOSSUM(:,8);
pulseSequencePOSSUM(:,8) = 0;

% Set centre freq to 0
pulseSequencePOSSUM(:,4) = 0;

% Set Gx to a different value
pulseSequencePOSSUM(:,6) = pulseSequencePOSSUM(:,6).*1.0019;

% Keep only first 10 steps
pulseSequencePOSSUM = [ pulseSequencePOSSUM(    1:   8, :); ...
                        pulseSequencePOSSUM( nL-5: end, :)];
pulseSequencePOSSUM(9,1) = 0.006;
pulseSequencePOSSUM(9,5) = 1;

% Repeat this 10 times
TR = 0.015 + pulseSequencePOSSUM(3,1);
NTRs = 10;
pulseSeqTemp = pulseSequencePOSSUM;
for i = 2 : NTRs
    pulseSeqTemp = [pulseSeqTemp ; ...
                    [ pulseSequencePOSSUM(:,1) + (i-1)*TR , ...
                      pulseSequencePOSSUM(:,2:8) ] ];
end
pulseSequencePOSSUM = pulseSeqTemp;

% % % % % 
% % Plot the sequence
figHand22 = figure;
func_plotPulseSequence(pulseSequencePOSSUM, figHand22, 'POSSUM');

% % % % % 
% % Plot the sequence
NPoints = ceil(size(pulseSequencePOSSUM,1)./(NTRs/4)); %1017;
figure('Position',[50,500,1400,400])
timePoints = pulseSequencePOSSUM(1:NPoints, 1);
% RF Pulse
stem(timePoints, rad2deg(pulseSequencePOSSUM(1:NPoints, 2)), 'k', 'filled'), hold on
% Readout
scatter(timePoints, rad2deg(pulseSequencePOSSUM(1:NPoints, 5)), 'r.')
% Gz
plot(timePoints, pulseSequencePOSSUM(1:NPoints, 6).*10000, 'b')

xlabel('time (s)');
legend('RF pulse', 'RO', 'G_x')



%%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % Save this new pulse 
% It's not balanced on Gz, maybe not even on Gx or Gy, so look into that
WRITEFLAG = 1;

fileNameNewPulse = [ROOTFOLDER, 'simdirFISP/outPulses/pulse_fisp'];

NR =   1;
NP =   1;

% % 0. Write pulse
if WRITEFLAG == 1
   write_pulse(fileNameNewPulse, pulseSequencePOSSUM, 1);
end
    
% % 1. Write pulse.info
if WRITEFLAG == 1
   SeqType = 4;
   TE  = 0.006; % (s)
   TR  = 0.015; % (s)
   TRslc = 0.015;  % (s)
   Nx = NR; 
   Ny = NP; 
   dx = 0.001; 
   dy = 0.001; 
   maxG = 5.000000e-01; 
   RiseT = 1.000000e-05;
   BW = 5.000000e+05; 
   Nvol = 1; 
   Nslc = 1;
   SlcThk = 0.003;
   SlcDir = 1;
   Gap = 0;
   zstart = 0.09;
   FA = 90;
   phasedir = 2;
   readdir = 3;
   kspace_coverage = 100;
   startkspace = 1;
   nPulses = NTRs;
   
   fileID = fopen([fileNameNewPulse, '.info'],'w');
   fprintf(fileID, '%.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f %.11f\n', ...
       SeqType, TE, TR, TRslc, Nx, Ny, dx, dy, ... % 8
       maxG, RiseT, BW, Nvol, Nslc, SlcThk, SlcDir, Gap, ... % 8
       zstart, FA, phasedir, readdir, kspace_coverage, startkspace, ... % 6 
       nPulses); % npulse
   fclose(fileID);
end

% % 2. Write pulse.readme
if WRITEFLAG == 1
    fileID = fopen([fileNameNewPulse, '.readme'],'w');
    
    fprintf(fileID, '1. SeqType = epif\n');
    fprintf(fileID, '2. TE = +6.000000e-03(s)\n');
    fprintf(fileID, '3. TR = +15.000000e-03(s)\n');
    fprintf(fileID, '4. TRslc = +15.000000e-03(s)\n');
    fprintf(fileID, ['5. Nread = +', num2str(NR), '\n']);
    fprintf(fileID, ['6. Nphase = +', num2str(NP), '\n']);
    fprintf(fileID, '7. dread = +1.000000e-03(m)\n');
    fprintf(fileID, '8. dphase = +1.000000e-03(m)\n');
    fprintf(fileID, '9. maxG = +5.000000e-01(T/m)\n');
    fprintf(fileID, '10. RiseT = +1.000000e-05(s)\n');
    fprintf(fileID, '11. BWrec = +5.000000e+05(Hz)\n');
    fprintf(fileID, '12. Nvol = +1\n');
    fprintf(fileID, '13. Nslc = +1\n');
    fprintf(fileID, '14. SlcThk = +3.000000e-03(m)\n');
    fprintf(fileID, '15. SlcDir = z+\n');
    fprintf(fileID, '16. Gap = +0.000000e+00(m)\n');
    fprintf(fileID, '17. zstart = +0.000000e+00(m)\n');
    fprintf(fileID, '18. FlipAngle = +9.0e+01(degrees)\n');
    fprintf(fileID, '19. PhaseDir = y+\n');
    fprintf(fileID, '20. ReadDir = x+\n');
    fprintf(fileID, '21. kspace coverage (phase) = +1.000000e+02(%%)\n');
    fprintf(fileID, '22. first non-zero phase line = +1\n');
    fprintf(fileID, ['23. Number of pulses = ', num2str(nPulses),'\n']);
end


% % 3. Write the .posx, .posy, .posz files
if WRITEFLAG == 1
    
    % Z direction
    posz = [-0.09  -0.089  -0.088  -0.087  -0.086  -0.085  -0.084  -0.083  -0.082  -0.081  -0.08  -0.079  -0.078  -0.077  -0.076  -0.075  -0.074  -0.073  -0.072  -0.071  -0.07  -0.069  -0.068  -0.067  -0.066  -0.065  -0.064  -0.063  -0.062  -0.061  -0.06  -0.059  -0.058  -0.057  -0.056  -0.055  -0.054  -0.053  -0.052  -0.051  -0.05  -0.049  -0.048  -0.047  -0.046  -0.045  -0.044  -0.043  -0.042  -0.041  -0.04  -0.039  -0.038  -0.037  -0.036  -0.035  -0.034  -0.033  -0.032  -0.031  -0.03  -0.029  -0.028  -0.027  -0.026  -0.025  -0.024  -0.023  -0.022  -0.021  -0.02  -0.019  -0.018  -0.017  -0.016  -0.015  -0.014  -0.013  -0.012  -0.011  -0.01  -0.009  -0.008  -0.007  -0.006  -0.005  -0.004  -0.003  -0.002  -0.001  0  0.001  0.002  0.003  0.004  0.005  0.006  0.007  0.008  0.009  0.01  0.011  0.012  0.013  0.014  0.015  0.016  0.017  0.018  0.019  0.02  0.021  0.022  0.023  0.024  0.025  0.026  0.027  0.028  0.029  0.03  0.031  0.032  0.033  0.034  0.035  0.036  0.037  0.038  0.039  0.04  0.041  0.042  0.043  0.044  0.045  0.046  0.047  0.048  0.049  0.05  0.051  0.052  0.053  0.054  0.055  0.056  0.057  0.058  0.059  0.06  0.061  0.062  0.063  0.064  0.065  0.066  0.067  0.068  0.069  0.07  0.071  0.072  0.073  0.074  0.075  0.076  0.077  0.078  0.079  0.08  0.081  0.082  0.083  0.084  0.085  0.086  0.087  0.088  0.089  0.09];
    
    % X direction
    % Need to choose posz positions such that the spins lie inside the 3mm
    % slice thickness 
    % I have 301 spins surrounded by 25 emtpy object voxels
    %       1201 spins
    dz = SlcThk / 2 / 600;
    % The positions will go from 0 to dz*150 (for the spins) + 25 for the
    % empty object voxels
    posx = 0:dz:dz*(600+25);
    posx = [fliplr(-posx(2:end)) posx];
    
    % Y direction
    posy = posz;
    
    dlmwrite([fileNameNewPulse, '.posx'], posx, ' ');
    dlmwrite([fileNameNewPulse, '.posy'], posy, ' ');
    dlmwrite([fileNameNewPulse, '.posz'], posz, ' ');
end

