
% % % % IRINA GRIGORESCU
% % % % Creates an object to be given as input to POSSUM

% I have calculated the dephasing gradient's amplitude and duration based
% on: Area_dephasingGrad = dphi / (gamma * dz); where
% dphi = n*pi (rad) of dephasing accross voxel size
% dz   = voxel size (m)
% gamma = gyromagnetic ratio (rad * Hz / T)

% Prerequisites
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/helpers/'))
addpath(genpath('~/Tools/MatlabStuff/matlabnifti/'))

ROOTFOLDER = '~/OneDrive - University College London/Work/POSSUMsimulations/';

%% Load file
filename = [ROOTFOLDER, 'simdirWithJEMRISseq/phantomOneCircle.nii.gz'];
iniData = load_nii(filename, ...
              [], [], [], [], [], 0.5);

sizeOfData = iniData.original.hdr.dime.dim(2:5);

%% Create a new object that is just one line of same isochromats
outputData = iniData;
m = 1251; n = 181; p = 181;
outputData.img = zeros(m,n,p);
outputData.img(26:1226,91,91) = 1;
outputData.fileprefix = [ROOTFOLDER, '/simdirFISP/phantomOneLine.nii.gz'];

% % % % % % Save phantom
outputData.hdr.dime.dim(2) =  m;            % x dimension = 351
outputData.hdr.dime.dim(3) =  n;            % y dimension = 181
outputData.hdr.dime.dim(4) =  p;            % z dimension = 181
outputData.original.hdr.dime.dim(2) = m;    % x dimension = 351
outputData.original.hdr.dime.dim(3) = n;    % y dimension = 181
outputData.original.hdr.dime.dim(4) = p;    % z dimension = 181


% SAVE
save_nii(outputData, outputData.fileprefix);


