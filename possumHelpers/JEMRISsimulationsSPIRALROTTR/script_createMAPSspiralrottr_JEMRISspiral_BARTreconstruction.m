% 
% Irina Grigorescu
% Date created: 24-01-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps
% 
% This script is looking at the scenario where the spiral rotates from one
% TR to the next TR
% 

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% 0. Prerequisites (PROPERTIES OF THE SEQUENCE)
% Number of TR blocks
NTRs  = 3; 
% Effective image size
Nx   = 32; Ny = Nx; 
% Number of redout points per TR
nROPoints = 6000; 

% % % % % % % % % % % % % % % % % % % % % CHOOSE CASE HERE
% % % % My three cases for the spirals:
casesIdx    = 3; % choose a case
casesNames  = {'00', '33', '03'}; 
casesAngles = [ 0 0 0 ; 3.1416 3.1416 3.1416 ; 0 0 3.1416];
casesNum    = size(casesNames,2);

%% 1a. Create spiral in matlab
Td = 6; %ms
dt = Td / nROPoints;
t1 = 0:dt:Td-dt;
spiralPower = 1;

spiralAmplitude =  0.35; 
spiralFrequency = 20;
rotAngleConsecutiveTR = casesAngles(casesIdx, :);

k_x = spiralAmplitude .* (t1.^spiralPower) .* cos(spiralFrequency .* t1);
k_y = spiralAmplitude .* (t1.^spiralPower) .* sin(spiralFrequency .* t1);
kk_x_M = zeros(nROPoints, NTRs); kk_y_M = zeros(nROPoints, NTRs);
for i = 1:NTRs
    % Calculate k-space trajectory for each TR
    kk_x_M(:,i) = (cos(rotAngleConsecutiveTR(i)) .* k_x) - ...
                  (sin(rotAngleConsecutiveTR(i)) .* k_y) ;
    
    kk_y_M(:,i) = (sin(rotAngleConsecutiveTR(i)) .* k_x) + ... 
                  (cos(rotAngleConsecutiveTR(i)) .* k_y);
end

% % 1b. Get k-space coordinates from a JEMRIS simulation
% Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralFullySampledConstantNTRRotTR/';

% Get sequence values
nameOfFile = [folderName, 'seq', casesNames{casesIdx}, '.h5'];
t1   = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
% Get k-space trajectory
t1  =  t1(RXP==0);
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure(111)

for idxTR = 1:NTRs
    % Populate the spiral k-spaces
    kspace_x(:,idxTR) = KX(((idxTR-1)*nROPoints+1) : idxTR*nROPoints); %kk_x_M(:,idxTR); %
    kspace_y(:,idxTR) = KY(((idxTR-1)*nROPoints+1) : idxTR*nROPoints); %kk_y_M(:,idxTR); %
    
    % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
    subplot(2,3,1); xl = 3;
    % Coordinates from JEMRIS
    scatter(kspace_x(:, idxTR), kspace_y(:, idxTR), '.');
	axis equal, axis square;
    xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
    title(['Spiral coords ', num2str(idxTR), ' (JEMRIS)'])
    xlim([-xl xl]); ylim([-xl xl]);
    
    % % % % % % % Plot the coordinates from matlab and jemris
    subplot(2,3,2)
    % Jemris
    sb1 = scatter(kspace_x(:, idxTR), kspace_y(:, idxTR), '.r'); 
    hold on
    % Matlab
    sb2 = scatter(kk_x_M(:,idxTR), kk_y_M(:,idxTR), '.b');
    legend('JEMRIS', 'MATLAB');
	axis equal; axis square; xlim([-xl xl]); ylim([-xl xl]);
    xlabel('k_x'); ylabel('k_y');
    title(['Spiral coords ', num2str(idxTR), ' (JEMRIS vs MATLAB)'])

    % % % % % % % Plot the difference in coordinates 
    subplot(2,3,[4,6])
    sb3 = plot(kspace_x(:,idxTR)-(kk_x_M(:,idxTR)), 'r.-'); hold on
    sb4 = plot(kspace_y(:,idxTR)-(kk_y_M(:,idxTR)), 'b.-');
    legend('k_x_J - k_x_M','k_y_J - k_y_M')
    
    % % % % % % % Transform the kspace coordinates to the 
    % % % % % % % desired effective imaging matrix
    % % % % % % % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
    x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:) + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:) + y0_ksp;
    
    % % % % % % % Plot the coordinates after scaling
    subplot(2,3,3)
    % Jemris
    scatter(kspace_x(:, idxTR), kspace_y(:, idxTR), '.r'); 
	axis equal; axis square; 
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(i), ' (JEMRIS scaled for reco)'])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);
    
    pause
    
    if idxTR ~= NTRs
        delete(sb1); delete(sb2); delete(sb3); delete(sb4); 
    end
end

%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderName, 'signals', casesNames{casesIdx}, '.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
% % % Store the signal
Mvecs = A;

% % % Get kspace as one signal
for i = 1:NTRs
    % Get signal values
    disp([num2str(i), ' - from: ', num2str((i-1)*nROPoints+1), ' to ', num2str(i*nROPoints)]);
    kspaces(:,i) = ( Mvecs(((i-1)*nROPoints+1) : i*nROPoints, 1) + ...
          sqrt(-1) * Mvecs(((i-1)*nROPoints+1) : i*nROPoints, 2) ).';
end

% % 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);

figure(223)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = kspace_x(:, i);
    currentKspace_y = kspace_y(:, i);
    currentKspaceToReco = kspaces(:, i);
    
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco.');
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART(:, :, i) = igrid;
    toc

    % Plot images (ABS)
    subplot(2,3,1), imagesc(abs(igrid))
    title('BART reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,2), imagesc(real(igrid))
    title(['BART reconstruction (real) ', num2str(i)])
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,3), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    % First 50 signal values
    subplot(2,3,4)
    sb1 = scatter(1:50, real(currentKspaceToReco(1:50)), 'r.'); hold on
    sb2 = scatter(1:50, imag(currentKspaceToReco(1:50)), 'b.');
    
    % First 50 trajectory coordinates
    subplot(2,3,5)
    scatter(currentKspace_x(1:50), currentKspace_y(1:50), '.');
    
    
    % Spiral trajectory used
    subplot(2,3,6)
    scatter(currentKspace_x, currentKspace_y, '.');
    title('Spiral traj used')
    axis square; 
    
    pause
    
    if i ~= NTRs
        delete(sb1); delete(sb2);
    end
end

%%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % 3. Comparison of spirals
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % a. Compare Case 1 S0 a) with Case 3 S0            (Case1S0a vs Case3S0)

% % % % % % % GET K-SPACE COORDINATES
% Get k-space coordinates from the JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralFullySampledConstantNTRRotTR/';
% Cases to compare
caseToCmpA = '00'; % first nROPoints values
caseToCmpB = '03'; % first nROPoints values
% Get sequence values for case 1
nameOfFile = [folderName, 'seq', caseToCmpA, '.h5'];
t1  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP1 = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
GX1  = h5read(nameOfFile, '/seqdiag/GX');   % GX
GY1  = h5read(nameOfFile, '/seqdiag/GY');   % GY
GZ1  = h5read(nameOfFile, '/seqdiag/GZ');   % GZ
FA1  = h5read(nameOfFile, '/seqdiag/TXM');  % FA
KX1  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY1  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ1  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
KX1 = KX1(RXP1==0); KY1 = KY1(RXP1==0); KZ1 = KZ1(RXP1==0);
KX_caseToCmpA = KX1(1:nROPoints); % first nROPoints values
KY_caseToCmpA = KY1(1:nROPoints);
KZ_caseToCmpA = KZ1(1:nROPoints);
% Get sequence values for case 2
nameOfFile = [folderName, 'seq', caseToCmpB, '.h5'];
t2  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP2 = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
GX2  = h5read(nameOfFile, '/seqdiag/GX');   % GX
GY2  = h5read(nameOfFile, '/seqdiag/GY');   % GY
GZ2  = h5read(nameOfFile, '/seqdiag/GZ');   % GZ
FA2  = h5read(nameOfFile, '/seqdiag/TXM');  % FA
KX2  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY2  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ2  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
KX2 = KX2(RXP2==0); KY2 = KY2(RXP2==0); KZ2 = KZ2(RXP2==0);
KX_caseToCmpB = KX2(1:nROPoints); % first nROPoints values
KY_caseToCmpB = KY2(1:nROPoints);
KZ_caseToCmpB = KZ2(1:nROPoints);

% % % % % % % GET SIGNAL VALUES
% Get signal values for CASE 1
% % % % % % Get signal values from JEMRIS simulation
file_caseToCmpA = [folderName, 'signals', caseToCmpA, '.h5']; 
file_caseToCmpB  = [folderName, 'signals', caseToCmpB , '.h5']; 
% % % Read in the vector values
A_caseToCmpA = (h5read(file_caseToCmpA, '/signal/channels/00'))';
A_caseToCmpB  = (h5read(file_caseToCmpB , '/signal/channels/00'))';
% % % Store the signals
Mvecs_caseToCmpA = A_caseToCmpA(1:nROPoints,:);
Mvecs_caseToCmpB = A_caseToCmpB(1:nROPoints,:);

romax = 1000;

% % % % % % % % PLOT SPIRALS + DIFFERENCE IN KSPACE + DIFFERENCE IN SIGNALS
% Plot both of the cases
figure

% Case 1a
subplot(4,2,1)
scatter(KX_caseToCmpA, KY_caseToCmpA, '.')
axis equal
title('Case 1a with \theta = 0')

% Case 3
subplot(4,2,2)
scatter(KX_caseToCmpB, KY_caseToCmpB, '.')
axis equal
title('Case 3 with \theta = 0')

% Case 1a - 3
subplot(4,2,[3,4])
scatter(1:nROPoints, KX_caseToCmpA - KX_caseToCmpB, 'r.'); hold on
scatter(1:nROPoints, KY_caseToCmpA - KY_caseToCmpB, 'b.');
ylabel('Difference between them');
legend('K_x_1 - K_x_3', 'K_x_y - K_y_3')
title('Case 1a - Case 3 with \theta = 0')

% Signals 
subplot(4,2,[5,6])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1),'r.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2),'k.-'); 
% from case 3
plot(1:romax, Mvecs_caseToCmpB(1:romax,1),'b--'); hold on
plot(1:romax, Mvecs_caseToCmpB(1:romax,2),'g--'); 
ylabel('M_i');
legend('M_{x1}','M_{y1}','M_{x3}','M_{y3}')
title('Case 1a and Case 3 with \theta = 0')

% Signals from case 3
subplot(4,2,[7,8])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1)-Mvecs_caseToCmpB(1:romax,1),'c.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2)-Mvecs_caseToCmpB(1:romax,2),'k.-');
xlabel('#RO point'); ylabel('M_{i1} - M_{i3}');
legend('M_{x1}-M_{x3}','M_{y1}-M_{y3}')
title('Difference between Case 1a and Case 3 with \theta = 0')


% % % % % % % % PLOT SEQUENCE FOR BOTH CASES
% Plot both of the cases
figure
% Case 1 a - Case 3
subplot(3,1,1)
%t1(RXP1==0)
plot(1:nROPoints*NTRs, GX1(RXP1==0) - GX2(RXP2==0), 'b.'); hold on
plot(1:nROPoints*NTRs, GY1(RXP1==0) - GY2(RXP2==0), 'r--');
title('Difference between G_{x/y} of the cases');
legend('G_{x1}-G_{x2}','G_{y1}-G_{y2}');

% Signals case 1a vs Signals case 3 for x
subplot(3,1,2)
plot(1:nROPoints*NTRs, A_caseToCmpA(:,1), 'r.'); hold on
plot(1:nROPoints*NTRs, A_caseToCmpB(:,1), 'b--');
title('M_x collected by both cases');
legend('M_{x1}', 'M_{x2}');

% Signals case 1a vs Signals case 3 for y
subplot(3,1,3)
plot(1:nROPoints*NTRs, A_caseToCmpA(:,2), 'r.'); hold on
plot(1:nROPoints*NTRs, A_caseToCmpB(:,2), 'b--');
title('M_y collected by both cases');
legend('M_{y1}', 'M_{y2}');


%%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % b. Compare Case 1 S0 b) with Case 3 S0            (Case1S0b vs Case3S0)

% % % % % % % GET K-SPACE COORDINATES
% Get k-space coordinates from the JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralFullySampledConstantNTRRotTR/';
% Cases to compare
caseToCmpA = '00'; % last nROPoints values
caseToCmpB = '03'; % first nROPoints values
% Get sequence values for case 1
nameOfFile = [folderName, 'seq', caseToCmpA, '.h5'];
t1  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
t1  =  t1(RXP==0); KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);
t1  =  t1(1:nROPoints);
KX_caseToCmpA = KX(nROPoints+1:end); % last nROPoints values
KY_caseToCmpA = KY(nROPoints+1:end);
KZ_caseToCmpA = KZ(nROPoints+1:end);
% Get sequence values for case 3
nameOfFile = [folderName, 'seq', caseToCmpB, '.h5'];
t2  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
t2  =  t2(RXP==0); KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);
t2  =  t2(1:nROPoints);
KX_caseToCmpB = KX(1:nROPoints); % first nROPoints values
KY_caseToCmpB = KY(1:nROPoints);
KZ_caseToCmpB = KZ(1:nROPoints);

% % % % % % % GET SIGNAL VALUES
% Get signal values for CASE 1
% % % % % % Get signal values from JEMRIS simulation
file_caseToCmpA = [folderName, 'signals', caseToCmpA, '.h5']; 
file_caseToCmpB  = [folderName, 'signals', caseToCmpB , '.h5']; 
% % % Read in the vector values
A_caseToCmpA = (h5read(file_caseToCmpA, '/signal/channels/00'))';
A_caseToCmpB  = (h5read(file_caseToCmpB , '/signal/channels/00'))';
% % % Store the signals
Mvecs_caseToCmpA = A_caseToCmpA(nROPoints+1:end,:); % last ro points
Mvecs_caseToCmpB = A_caseToCmpB(1:nROPoints,:);     % first ro points

romax = 1000;

% Plot both of the cases
figure

% Case 1a
subplot(4,2,1)
scatter(KX_caseToCmpA, KY_caseToCmpA, '.')
axis equal
title('Case 1b with \theta = 0')

% Case 3
subplot(4,2,2)
scatter(KX_caseToCmpB, KY_caseToCmpB, '.')
axis equal
title('Case 3 with \theta = 0')

% Case 1a - 3
subplot(4,2,[3,4])
scatter(1:nROPoints, KX_caseToCmpA - KX_caseToCmpB, 'r.'); hold on
scatter(1:nROPoints, KY_caseToCmpA - KY_caseToCmpB, 'b.');
ylabel('Difference between them');
legend('K_x_1 - K_x_3', 'K_x_y - K_y_3')
title('Case 1b - Case 3 with \theta = 0')

% Signals 
subplot(4,2,[5,6])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1),'r.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2),'k.-'); 
% from case 3
plot(1:romax, Mvecs_caseToCmpB(1:romax,1),'b--'); hold on
plot(1:romax, Mvecs_caseToCmpB(1:romax,2),'g--'); 
ylabel('M_i');
legend('M_{x1}','M_{y1}','M_{x3}','M_{y3}')
title('Case 1 and Case 3 with \theta = 0')

% Signals from case 3
subplot(4,2,[7,8])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1)-Mvecs_caseToCmpB(1:romax,1),'c.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2)-Mvecs_caseToCmpB(1:romax,2),'k.-');
xlabel('#RO point'); ylabel('M_{i1} - M_{i3}');
legend('M_{x1}-M_{x3}','M_{y1}-M_{y3}')
title('Difference between Case 1b and Case 3 with \theta = 0')



%%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % c. Compare Case 2 S180 a) with Case 3 S180  (Case2S180a vs Case3S180)

% % % % % % % GET K-SPACE COORDINATES
% Get k-space coordinates from the JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralFullySampledConstantNTRRotTR/';
% Cases to compare
caseToCmpA = '33'; % first nROPoints values
caseToCmpB = '03'; % last nROPoints values
% Get sequence values for case 1
nameOfFile = [folderName, 'seq', caseToCmpA, '.h5'];
t1  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP1 = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
GX1  = h5read(nameOfFile, '/seqdiag/GX');   % GX
GY1  = h5read(nameOfFile, '/seqdiag/GY');   % GY
GZ1  = h5read(nameOfFile, '/seqdiag/GZ');   % GZ
FA1  = h5read(nameOfFile, '/seqdiag/TXM');  % FA
KX1  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY1  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ1  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
KX1 = KX1(RXP1==0); KY1 = KY1(RXP1==0); KZ1 = KZ1(RXP1==0);
KX_caseToCmpA = KX1(1:nROPoints); % first nROPoints values
KY_caseToCmpA = KY1(1:nROPoints);
KZ_caseToCmpA = KZ1(1:nROPoints);
% Get sequence values for case 2
nameOfFile = [folderName, 'seq', caseToCmpB, '.h5'];
t2  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP2 = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
GX2  = h5read(nameOfFile, '/seqdiag/GX');   % GX
GY2  = h5read(nameOfFile, '/seqdiag/GY');   % GY
GZ2  = h5read(nameOfFile, '/seqdiag/GZ');   % GZ
FA2  = h5read(nameOfFile, '/seqdiag/TXM');  % FA
KX2  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY2  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ2  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
KX2 = KX2(RXP2==0); KY2 = KY2(RXP2==0); KZ2 = KZ2(RXP2==0);
KX_caseToCmpB = KX2(nROPoints+1:end); % last nROPoints values
KY_caseToCmpB = KY2(nROPoints+1:end);
KZ_caseToCmpB = KZ2(nROPoints+1:end);


% % % % % % % GET SIGNAL VALUES
% Get signal values for CASE 1
% % % % % % Get signal values from JEMRIS simulation
file_caseToCmpA = [folderName, 'signals', caseToCmpA, '.h5']; 
file_caseToCmpB  = [folderName, 'signals', caseToCmpB , '.h5']; 
% % % Read in the vector values
A_caseToCmpA = (h5read(file_caseToCmpA, '/signal/channels/00'))';
A_caseToCmpB = (h5read(file_caseToCmpB, '/signal/channels/00'))';
% % % Store the signals
Mvecs_caseToCmpA = A_caseToCmpA(1:nROPoints,:); % first ro points
Mvecs_caseToCmpB = A_caseToCmpB(nROPoints+1:end,:);     % last  ro points

romax = 1000;

% Plot both of the cases
figure

% Case 1a
subplot(4,2,1)
scatter(KX_caseToCmpA, KY_caseToCmpA, '.')
axis equal
title('Case 2a with \theta = 180')

% Case 3
subplot(4,2,2)
scatter(KX_caseToCmpB, KY_caseToCmpB, '.')
axis equal
title('Case 3 with \theta = 180')

% Case 1a - 3
subplot(4,2,[3,4])
scatter(1:nROPoints, KX_caseToCmpA - KX_caseToCmpB, 'r.'); hold on
scatter(1:nROPoints, KY_caseToCmpA - KY_caseToCmpB, 'b.');
ylabel('Difference between them');
legend('K_x_1 - K_x_3', 'K_x_y - K_y_3')
title('Case 2a - Case 3 with \theta = 180')

% Signals 
subplot(4,2,[5,6])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1),'r.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2),'k.-'); 
% from case 3
plot(1:romax, Mvecs_caseToCmpB(1:romax,1),'b--'); hold on
plot(1:romax, Mvecs_caseToCmpB(1:romax,2),'g--'); 
ylabel('M_i');
legend('M_{x1}','M_{y1}','M_{x3}','M_{y3}')
title('Case 2a and Case 3 with \theta = 180')

% Signals from case 3
subplot(4,2,[7,8])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1)-Mvecs_caseToCmpB(1:romax,1),'c.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2)-Mvecs_caseToCmpB(1:romax,2),'k.-');
xlabel('#RO point'); ylabel('M_{i1} - M_{i3}');
legend('M_{x1}-M_{x3}','M_{y1}-M_{y3}')
title('Difference between Case 2a and Case 3 with \theta = 180')

% % % % % % % % PLOT SEQUENCE FOR BOTH CASES
% Plot both of the cases
figure
% Case 1 a - Case 3
subplot(3,1,1)
%t1(RXP1==0)
plot(1:nROPoints*2, GX1(RXP1==0) - GX2(RXP2==0), 'b.'); hold on
plot(1:nROPoints*2, GY1(RXP1==0) - GY2(RXP2==0), 'r--');
title('Difference between G_{x/y} of the cases');
legend('G_{x1}-G_{x2}','G_{y1}-G_{y2}');

% Signals case 1a vs Signals case 3 for x
subplot(3,1,2)
plot(1:nROPoints*2, A_caseToCmpA(:,1), 'r.'); hold on
plot(1:nROPoints*2, A_caseToCmpB(:,1), 'b--');
title('M_x collected by both cases');
legend('M_{x1}', 'M_{x2}');

% Signals case 1a vs Signals case 3 for y
subplot(3,1,3)
plot(1:nROPoints*2, A_caseToCmpA(:,2), 'r.'); hold on
plot(1:nROPoints*2, A_caseToCmpB(:,2), 'b--');
title('M_y collected by both cases');
legend('M_{y1}', 'M_{y2}');


%%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % d. Compare Case 2 S180 b) with Case 3 S180  (Case2S180b vs Case3S180)

% % % % % % % GET K-SPACE COORDINATES
% Get k-space coordinates from the JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralFullySampledConstantNTRRotTR/';
% Cases to compare
caseToCmpA = '33'; % last nROPoints values
caseToCmpB = '03'; % last nROPoints values
% Get sequence values for case 1
nameOfFile = [folderName, 'seq', caseToCmpA, '.h5'];
t1  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
t1  =  t1(RXP==0); KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);
t1  =  t1(1:nROPoints);
KX_caseToCmpA = KX(nROPoints+1:end); % last nROPoints values
KY_caseToCmpA = KY(nROPoints+1:end);
KZ_caseToCmpA = KZ(nROPoints+1:end);
% Get sequence values for case 3
nameOfFile = [folderName, 'seq', caseToCmpB, '.h5'];
t2  = h5read(nameOfFile, '/seqdiag/T');    % timings
RXP = h5read(nameOfFile, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read(nameOfFile, '/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(nameOfFile, '/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(nameOfFile, '/seqdiag/KZ');   % KZ (rad/mm)
t2  =  t2(RXP==0); KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);
t2  =  t2(1:nROPoints);
KX_caseToCmpB = KX(nROPoints+1:end); % last nROPoints values
KY_caseToCmpB = KY(nROPoints+1:end);
KZ_caseToCmpB = KZ(nROPoints+1:end);

% % % % % % % GET SIGNAL VALUES
% Get signal values for CASE 1
% % % % % % Get signal values from JEMRIS simulation
file_caseToCmpA = [folderName, 'signals', caseToCmpA, '.h5']; 
file_caseToCmpB  = [folderName, 'signals', caseToCmpB , '.h5']; 
% % % Read in the vector values
A_caseToCmpA = (h5read(file_caseToCmpA, '/signal/channels/00'))';
A_caseToCmpB = (h5read(file_caseToCmpB, '/signal/channels/00'))';
% % % Store the signals
Mvecs_caseToCmpA = A_caseToCmpA(nROPoints+1:end,:); % first ro points
Mvecs_caseToCmpB = A_caseToCmpB(nROPoints+1:end,:);     % last  ro points

romax = 1000;

% Plot both of the cases
figure

% Case 1a
subplot(4,2,1)
scatter(KX_caseToCmpA, KY_caseToCmpA, '.')
axis equal
title('Case 2b with \theta = 180')

% Case 3
subplot(4,2,2)
scatter(KX_caseToCmpB, KY_caseToCmpB, '.')
axis equal
title('Case 3 with \theta = 180')

% Case 1a - 3
subplot(4,2,[3,4])
scatter(1:nROPoints, KX_caseToCmpA - KX_caseToCmpB, 'r.'); hold on
scatter(1:nROPoints, KY_caseToCmpA - KY_caseToCmpB, 'b.');
ylabel('Difference between them');
legend('K_x_1 - K_x_3', 'K_x_y - K_y_3')
title('Case 2b - Case 3 with \theta = 180')

% Signals 
subplot(4,2,[5,6])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1),'r.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2),'k.-'); 
% from case 3
plot(1:romax, Mvecs_caseToCmpB(1:romax,1),'b--'); hold on
plot(1:romax, Mvecs_caseToCmpB(1:romax,2),'g--'); 
ylabel('M_i');
legend('M_{x1}','M_{y1}','M_{x3}','M_{y3}')
title('Case 2b and Case 3 with \theta = 180')

% Signals from case 3
subplot(4,2,[7,8])
% from case 1
plot(1:romax, Mvecs_caseToCmpA(1:romax,1)-Mvecs_caseToCmpB(1:romax,1),'c.-'); hold on
plot(1:romax, Mvecs_caseToCmpA(1:romax,2)-Mvecs_caseToCmpB(1:romax,2),'k.-');
xlabel('#RO point'); ylabel('M_{i1} - M_{i3}');
legend('M_{x1}-M_{x3}','M_{y1}-M_{y3}')
title('Difference between Case 2b and Case 3 with \theta = 180')














