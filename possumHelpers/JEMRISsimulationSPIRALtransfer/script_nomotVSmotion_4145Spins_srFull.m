% 
% Irina Grigorescu
% Date created: 04-02-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps
% 
% NO MOTION CASE FOR FULLY SAMPLED SPIRAL


% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
% COMMON ROOT FOLDER 
commonRoot = '~/OneDrive - University College London/Work/JEMRISSimulations/'; 

 
% SEQUENCE PARAMETERS
folderNameSeq      = [commonRoot, 'feb2018/nomot_500TRs/']; %~/jemrisSims/

% NO MOTION FOLDER
folderNameNOMOTION = [commonRoot, 'feb2018/nomot_500TRs/']; %~/jemrisSims/

% MOTION FOLDER
folderNameMOTION   = [commonRoot, 'feb2018/mot_rot_beg/']; %~/jemrisSims
 
%% K-space coordinates
% % % % % PROPERTIES OF THE SEQUENCE
% Set number of TR blocks
NTRs = 500; 
% Set effective image size
Nx   = 96; Ny = Nx; %64
% Set number of redout points per TR
nROPoints = 6000; 

% % % % % PROPERTIES OF SPIRAL (MATLAB)
% Create the spiral k-space in MATLAB
Td = 6; %ms
dt = Td / nROPoints;
t = dt:dt:Td;
spiralPower = 1;

spiralAmplitude =    0.4; 
spiralFrequency =  120;  
rotAngleConsecutiveTR = repmat([0 0], [1, NTRs]); 

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);

kk_x_M = zeros(nROPoints, NTRs); kk_y_M = zeros(nROPoints, NTRs);
for idxTR = 1:NTRs
    % Calculate k-space trajectory for each TR
    kk_x_M(:,idxTR) = cos(rotAngleConsecutiveTR(idxTR)) * k_x - ...
                      sin(rotAngleConsecutiveTR(idxTR)) * k_y ;
    
    kk_y_M(:,idxTR) = sin(rotAngleConsecutiveTR(idxTR)) * k_x + ... 
                      cos(rotAngleConsecutiveTR(idxTR)) * k_y;
end
clear k_x k_y 
clear rotAngleConsecutiveTR spiralAmplitude spiralFrequency spiralPower

% %
% % % % % PROPERTIES OF THE SPIRAL TAKEN FROM JEMRIS

% Get sequence values
RXP = h5read([folderNameSeq,'seq.h5'],'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KZ');   % KZ (rad/mm)
GX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GX');   % GX
GY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GY');   % GY
FA  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/TXM');  % FA
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure(111)
for idxTR = 1:NTRs
    
    % Populate the spiral k-spaces
    kspace_x(:,idxTR) = KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints); %kk_x_M(:,i); %
    kspace_y(:,idxTR) = KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints); %kk_y_M(:,i); %
    
    % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
    subplot(2,2,1); xl = 2;
    % Coordinates from JEMRIS
    scatter(kk_x_M((idxTR-1)*nROPoints+1 : idxTR*nROPoints), ...
            kk_y_M((idxTR-1)*nROPoints+1 : idxTR*nROPoints), '.');
	axis equal, axis square;
    xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
    title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
    xlim([-xl xl]); ylim([-xl xl]);

    % % % % % % Plot the difference between the coordinates
    subplot(2,2,[3,4]);
    % k_x differences between jemris and matlab
    x1 = plot(1:nROPoints, KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints) - ...
                           kk_x_M(:, idxTR), 'r.-');
    hold on
    % k_y differences between jemris and matlab
    x2 = plot(1:nROPoints, KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints) - ...
                           kk_y_M(:, idxTR), 'b.-');
    xlabel('#ro points'); ylabel('Difference between k-space coord'); 
    title(['Spiral ', num2str(idxTR), ' (JEMRIS - MATLAB)'])
    legend('k_x', 'k_y')
    
    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
	x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:) + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:) + y0_ksp;
    
    % Plot the coordinates after scaling
    subplot(2,2,2)
    scatter(kspace_x(:, idxTR), kspace_y(:, idxTR),'.'), axis equal, axis square;
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(idxTR), ' (JEMRIS scaled for reco)'])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

    if mod(idxTR,50) == 0 || (idxTR == 1)
        drawnow
    end
    
    if idxTR ~= NTRs
        delete(x1); delete(x2);
    end
end

%% SIMULATE THE DICTIONARY
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
% SEQFLAG  = 6; % sinusoidal FA, 0 PhA, fixed TRs, fixed ROs
SEQFLAG  = 5; % if == 5 need to provide customSequence 
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Material

% % T1 ranges from 100ms to 5000ms with different timesteps
% T1_mrf = 100:20:2000;            % from 100ms to 2000ms dt = 20ms
% T1_mrf = [T1_mrf 2300:300:5000]; % from 2300ms to 5000ms dt = 300ms
% T1_mrf = [T1_mrf, 225, 335, 330, 1010, 1615, 675, 665, 830, 1150];
T1_mrf = [ 500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
T1_mrf = sort(T1_mrf);

% % 
% % T2 ranges from 20ms to 3000ms with different timesteps
% T2_mrf = 20:5:100;          % from 20ms to 100ms dt = 5ms
% T2_mrf = [T2_mrf 110:10:200];   % from 110ms to 200ms dt = 10ms
% T2_mrf = [T2_mrf 400:200:3000]; % from 400ms to 3000ms dt = 200ms
% T2_mrf = [T2_mrf, 360,  370,  155];
T2_mrf = [ 360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];
T2_mrf = sort(T2_mrf);

df_mrf =   0;

flagIR =   1;

% % % % Custom Material
customMaterial.T1     = T1_mrf;
customMaterial.T2     = T2_mrf;
customMaterial.offRes = df_mrf;

% % % % Custom Sequence
customSequence        = struct;
customSequence.RF.FA  = dlmread([...
  '/Users/irina/OneDrive - University College London/Work/',...
  'Simulation/simulationMRF/src/preprocess/FAsMaryia']).';
                                           %FAsMaryia
                                           %FAsForExperimentsOnCluster
customSequence.RF.FA  = customSequence.RF.FA(1:NTRs);
customSequence.RF.PhA = zeros(NTRs,1);
customSequence.TR     = zeros(NTRs,1) + 15;
customSequence.RO     = zeros(NTRs,1) +  6;


tic

[ materialProperties, sequenceProperties, ...
  materialTuples, M, ...
  dictionaryMRF] = simulateMRF(NTRs, FLAGPLOT, ...
                                     SEQFLAG, customSequence, ...
                                     MATFLAG, customMaterial, flagIR);
t=toc; 

% % % % All dictionary signals are transformed into a dictionary matrix
% % % % where both real and imaginary channels are stored
dictionaryMRFComplex = [ squeeze(dictionaryMRF(1:M, :, 1))  , ...
                         squeeze(dictionaryMRF(1:M, :, 2)) ];

% % % % Normalize signals:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[dictionaryMRFNorm2Ch, dictionaryMRFNorm] = ...
                                 normalizeSignals(dictionaryMRFComplex);
                             
% Create dictionary of signal values (M x N) from the normalized signals
signalDictionaryMRF        = abs(dictionaryMRFNorm);

figure, plot(1:NTRs, sequenceProperties.RF.FA, '.-')

% % Example dictionary
try
    figure
    for i = 1:100:900
        plot(1:NTRs, abs(dictionaryMRFNorm(1+(i-1)*4,:)),'LineWidth',1.4), hold on
    end
    plot(1:NTRs, abs(dictionaryMRFNorm(163,:)),'LineWidth',1.4)
    xlabel('TR index');
    ylabel('Signal (a.u.)')
    title('Example fingerprints')
    grid on
catch
    warning('You are using SMALL DICTIONARY SIZE.');
end
    


%% Create GROUND TRUTH MAPS

% For the 96x96 phantom with 4145 spins
centres = [41,27 ; 56,27 ; ...
           27,41 ; 41,41 ; 56,41 ; 70,41 ; ...
           27,56 ; 41,56 ; 56,56 ; 70,56 ; ...
           41,69 ; 56,69 ; ...
           49,49 ]; % big circle
radii   = [zeros(1,12) + 4 , 32];

realMap = struct;
myPhantomT1Values = [500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
myPhantomT2Values = [360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];

[realMap.T1map, realMap.T2map, maskAllCircles, maskHollow] = ...
         func_createRealT1T2Maps(myPhantomT1Values, myPhantomT2Values, ...
                                 Nx, Ny, centres, radii);


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % NO MOTION
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderNameNOMOTION, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end

 
% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints, NTRs]);


% % 3. Reconstruct with BART
% Save all images
imagesBART_nomot = zeros(Nx, Ny, NTRs);

figure(115)
for idxTR = 1:NTRs
    tic
    disp(['Image number ', num2str(idxTR)]);
    % Retrieve current values for current reconstruction
    currentKspace_x     = kspace_x(:, idxTR);
    currentKspace_y     = kspace_y(:, idxTR);
    currentKspaceToReco = (kspaces(:, idxTR)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';

                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART_nomot(:, :, idxTR) = igrid;
    toc

    % Plot images (ABS)
    if mod(idxTR, 25) == 0 || (idxTR == 1)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) ', num2str(idxTR)])
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;
    end
    
    
    pause(0.01)
    
    clear igrid traj_kspace
end

% % % % Normalize the reconstructed images
[~, imagesBART_nomot] = normalizeSignals( ...
                    [ real(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ]);
imagesBART_nomot = (reshape(imagesBART_nomot, [Nx, Ny, NTRs]));


%% DICTIONARY MATCHING FOR NO MOTION
imagesBARTReshaped_nomot = [ real(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ...
                             imag(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped_nomot);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB_nomot = matTuplesMatchedB1;
resultsB_nomot.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                         valuesOfMatchB3, valuesOfMatchB4];
resultsB_nomot.scoremap = reshape(resultsB_nomot.score, [Nx, Ny]);                      
resultsB_nomot.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB_nomot.indicesmap = reshape(resultsB_nomot.indices, [Nx, Ny]); 
resultsB_nomot.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB_nomot.T1map = reshape(resultsB_nomot.T1, [Nx, Ny]);
resultsB_nomot.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB_nomot.T2map = reshape(resultsB_nomot.T2, [Nx, Ny]);                
   


%% % % % % Reconstructed maps of T1 and T2 and the real (ground truth) maps
figure
% T1 real
subplot(2,3,1)
imagesc(realMap.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(realMap.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(resultsB_nomot.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Reconstructed')

% BART - T2
subplot(2,3,5)
imagesc(resultsB_nomot.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Reconstructed')

% BART - Score
subplot(2,3,6)   
imagesc(resultsB_nomot.scoremap.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0 1])
axis off
title('Matching Scores')

%%
% % % % % Difference maps of T1 and T2 between reconstructed and GT

figure
% BART - T1
subplot(1,2,1)
imagesc((resultsB_nomot.T1map-realMap.T1map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_1 Reconstructed - T_1 Ground Truth')
caxis([-500 500])

% BART - T2
subplot(1,2,2)
imagesc((resultsB_nomot.T2map-realMap.T2map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_2 Reconstructed - T_2 Ground Truth')
caxis([-150 150])




%%
% % % % % PERCENTAGE ERROR NO MOTION
figure
% BART - T1
subplot(1,2,1)
normMatrix = realMap.T1map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_nomot.T1map-realMap.T1map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_1)')

% BART - T2
subplot(1,2,2)
normMatrix = realMap.T2map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_nomot.T2map-realMap.T2map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_2)')


%% Have a look at individual signals NO MOTION

imagesBARTReshaped1Ch_nomot = reshape(imagesBART_nomot, [Nx*Ny, NTRs ]);

% Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(34-1)*Nx+49, ... %big circle
             (57-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+27, ... %c1
             (40-1)*Nx+41];    %c4
                       
scoreValB = resultsB_nomot.score(scoreIdxB);
% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4'};

% % % % Is this the voxel I was looking for?
figure, 
llPhantom = realMap.T1map.*maskAllCircles;
llPhantom = llPhantom(:);
llPhantom(scoreIdxB(1)) = 0; 
colormap hot; colorbar
llPhantom = reshape(llPhantom, [Nx, Ny]);
imagesc(llPhantom), axis square


for idxTR = 1:size(scoreIdxB,2)
    
    figure('Position', [50,10,1200,800])    
    % The voxel we are looking for
    subplot(4,2,1)
    llPhantom = squeeze(abs(imagesBART_nomot(:,:,1)));
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray
    
    % BY CHANNELS
    subplot(4,2,[3,4])
    plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsB_nomot.indices(scoreIdxB(idxTR)), :), '.--')
    hold on
    plot(1:2*NTRs, imagesBARTReshaped_nomot(scoreIdxB(idxTR), :), '.-')
    legend('dictionary', 'image')
    title(['BART (by channels) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

    % DIFFERENCE
    subplot(4,2,[5,6])
    plot(1:2*NTRs, ...
         dictionaryMRFNorm2Ch(resultsB_nomot.indices(scoreIdxB(idxTR)), :) - ...
         imagesBARTReshaped_nomot(scoreIdxB(idxTR), :), '.-')
    title(['BART (by channels) difference ',  ...
        titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
    
    % ABS
    subplot(4,2,[7,8])
    plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_nomot.indices(scoreIdxB(idxTR)), :)), 'r-')
    hold on
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_nomot(scoreIdxB(idxTR), :)), 'b--')
    legend('dictionary', 'phantom')
    xlabel('#image'); ylabel('normalised signal intensity (a.u.)');
    %title(['BART (abs) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

end









return
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % MOTION
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderNameMOTION, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end

 
% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints, NTRs]);


% % 3. Reconstruct with BART
% Save all images
imagesBART_mot = zeros(Nx, Ny, NTRs);

figure(117)
for idxTR = 1:NTRs
    tic
    disp(['Image number ', num2str(idxTR)]);
    % Retrieve current values for current reconstruction
    currentKspace_x     = kspace_x(:, idxTR);
    currentKspace_y     = kspace_y(:, idxTR);
    currentKspaceToReco = (kspaces(:, idxTR)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';

                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART_mot(:, :, idxTR) = igrid;
    toc

    % Plot images (ABS)
    if mod(idxTR, 25) == 0 || (idxTR == 1)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) ', num2str(idxTR)])
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;
    end
    
    
    pause(0.01)
    
    clear igrid traj_kspace
end

% % % % Normalize the reconstructed images
[~, imagesBART_mot] = normalizeSignals( ...
                    [ real(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ]);
imagesBART_mot = (reshape(imagesBART_mot, [Nx, Ny, NTRs]));


%% DICTIONARY MATCHING FOR MOTION
imagesBARTReshaped_mot = [ real(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ...
                           imag(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped_mot);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB_mot = matTuplesMatchedB1;
resultsB_mot.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                         valuesOfMatchB3, valuesOfMatchB4];
resultsB_mot.scoremap = reshape(resultsB_mot.score, [Nx, Ny]);                      
resultsB_mot.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB_mot.indicesmap = reshape(resultsB_mot.indices, [Nx, Ny]); 
resultsB_mot.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB_mot.T1map = reshape(resultsB_mot.T1, [Nx, Ny]);
resultsB_mot.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB_mot.T2map = reshape(resultsB_mot.T2, [Nx, Ny]);                
   


%% % % % % Reconstructed maps of T1 and T2 and the real (ground truth) maps
figure
% T1 real
subplot(2,3,1)
imagesc(resultsB_nomot.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(resultsB_nomot.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(resultsB_mot.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Reconstructed')

% BART - T2
subplot(2,3,5)
imagesc(resultsB_mot.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Reconstructed')

% BART - Score
subplot(2,3,6)   
imagesc(resultsB_mot.scoremap.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0 1])
axis off
title('Matching Scores')

%%
% % % % % Difference maps of T1 and T2 between reconstructed and GT
figure
% BART - T1
subplot(1,2,1)
imagesc((resultsB_mot.T1map-realMap.T1map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_1 Reconstructed - T_1 Ground Truth')
caxis([-500 500])

% BART - T2
subplot(1,2,2)
imagesc((resultsB_mot.T2map-realMap.T2map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_2 Reconstructed - T_2 Ground Truth')
caxis([-150 150])

%%
% % % % % PERCENTAGE ERROR MAPS MOTION
figure
% BART - T1
subplot(1,2,1)
normMatrix = realMap.T1map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_mot.T1map-realMap.T1map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_1)')

% BART - T2
subplot(1,2,2)
normMatrix = realMap.T2map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_mot.T2map-realMap.T2map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_2)')



%% Have a look at individual signals MOTION

imagesBARTReshaped1Ch_mot = reshape(imagesBART_mot, [Nx*Ny, NTRs ]);

% Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(49-1)*Nx+49, ... %big circle %34/49 to the left of centref
             (57-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+27, ... %c1
             (40-1)*Nx+41, ... %c4
             (26-1)*Nx+38];    %c3
                       
scoreValB = resultsB_mot.score(scoreIdxB);
scoreValB_nomot = resultsB_nomot.score(scoreIdxB);
% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4', 'c3'};

% % % % Is this the voxel I was looking for?
figure, 
llPhantom = realMap.T1map.*maskAllCircles;
llPhantom = llPhantom(:);
llPhantom(scoreIdxB(1)) = 0; 
colormap hot; colorbar
llPhantom = reshape(llPhantom, [Nx, Ny]);
imagesc(llPhantom), axis square


for idxTR = 5:7%1:size(scoreIdxB,2)
    
    figure('Position', [50,10,1200,800])    
    % The voxel we are looking for
    subplot(4,2,1)
    llPhantom = squeeze(abs(imagesBART_nomot(:,:,1)));
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray
    
    % BY CHANNELS
    subplot(4,2,[3,4])
    plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsB_mot.indices(scoreIdxB(idxTR)), :), '.--')
    hold on
    plot(1:2*NTRs, imagesBARTReshaped_mot(scoreIdxB(idxTR), :), '.-')
    legend('dictionary', 'image')
    title(['BART (by channels) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

    % DIFFERENCE
    subplot(4,2,[5,6])
    plot(1:2*NTRs, ...
         dictionaryMRFNorm2Ch(resultsB_mot.indices(scoreIdxB(idxTR)), :) - ...
         imagesBARTReshaped_mot(scoreIdxB(idxTR), :), '.-')
    title(['BART (by channels) difference ',  ...
        titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
    
    % ABS
    subplot(4,2,[7,8])
    % dictionary signal it matched to
    plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_mot.indices(scoreIdxB(idxTR)), :)), 'r-')
    hold on
    % voxel signal motion
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_mot(scoreIdxB(idxTR), :)), 'b--')
    % dictionary signal it should have matched to
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_nomot(scoreIdxB(idxTR), :)), 'k-')
    % voxel signal no motion
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_nomot(scoreIdxB(idxTR), :)), 'g--')
    
    %plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_nomot.indices(scoreIdxB(idxTR)), :)), 'g--')
    %plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_nomot.indices(scoreIdxB(idxTR)), :)), 'g--')
    legend('dictionary (matched)', 'phantom (motion)', 'dictionary (matched no motion)', 'phantom (no motion)')
    xlabel('#image'); ylabel('normalised signal intensity (a.u.)');
    %title(['BART (abs) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

end





















return
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
%% Have a look at individual signals

imagesBARTReshaped1Ch_nomot = reshape(imagesBART_nomot, [Nx*Ny, NTRs ]);
imagesBARTReshaped1Ch_mot   = reshape(imagesBART_mot, [Nx*Ny, NTRs ]);

% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4'};


% Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(34-1)*Nx+49, ... %big circle
             (57-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+27, ... %c1
             (40-1)*Nx+41];    %c4        


% % % % Is this the voxel I was looking for?
figure, 
llPhantom = realMap.T1map.*maskAllCircles;
llPhantom = llPhantom(:);
llPhantom(scoreIdxB(1)) = 0; 
colormap hot; colorbar
llPhantom = reshape(llPhantom, [Nx, Ny]);
imagesc(llPhantom), axis square





%% % This plots the difference between the two signals of same voxel

imagesBARTReshaped1Ch_motll = [ imagesBARTReshaped1Ch_mot(:,1:104) , ...
                                imagesBARTReshaped1Ch_mot(:,106:NTRs) ];

[~, imagesBARTReshaped1Ch_motll] = normalizeSignals( ...
                     [real(imagesBARTReshaped1Ch_motll), ...
                      imag(imagesBARTReshaped1Ch_motll)] );

for idxTR = 1:4%size(scoreIdxB,2)
    
    
    figure
    
    % % % % Is this the voxel I was looking for?
    subplot(2,2,1)
    llPhantom = squeeze(abs(imagesBART_nomot(:,:,1))).*maskAllCircles;%realMap.T1map
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray

    % % % % Plot abs value of both motion free and motion corrupted data
    subplot(2,2,[3,4])
    % Motion corruputed signal
    plot(1:NTRs-1, abs(imagesBARTReshaped1Ch_motll(resultsB_mot.indices(scoreIdxB(idxTR)), :)), '.-', ...
         'LineWidth', 1.1)
    hold on
    % Motion free signal
    plot(1:NTRs-1, [ abs(imagesBARTReshaped1Ch_nomot(resultsB_nomot.indices(scoreIdxB(idxTR)),1:67)), ...
                     abs(imagesBARTReshaped1Ch_nomot(resultsB_nomot.indices(scoreIdxB(idxTR)),69:NTRs)) ], '.-', ...
                     'LineWidth', 1.1)
                 
    legend('motion', 'no motion')
    title(['Looking at: ',  titles{idxTR}])
    xlabel('TR index'); ylabel('Signal (a.u.)');
end













