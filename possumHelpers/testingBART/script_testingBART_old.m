% 
% Irina Grigorescu
% Date created: 01-12-2017
% 
% Script to test BART reconstruction
% 
clear all; close all; clc

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% % % % %  Testing Scenarios

% % % % % % 1. KSPACE VALUES ARE CONSTANT
% % % % Grid scenarios:
%%
% An integer asymmetric grid (-N/2 to N/2-1)
%                            (e.g. -4 -3 -2 -1 0 1 2 3) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'asymint', 'constant');
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
    
%%
% An integer symmetric grid (-N/2 to N/2 )
%                            (e.g. -4 -3 -2 -1 0 1 2 3 4) = N+1

% Run test:
[kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symint', 'constant');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

%%
% A non-integer symmetric grid (-(N/2-0.5) to +(N/2-0.5))
%                           (e.g. -3.5 -2.5 -1.5 -0.5 0.5 1.5 2.5 3.5) = N

% Run test:
[kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symint', 'constant');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

             
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %              
% % % % % % 2. KSPACE VALUES ARE DELTA FUNCTION (middle)
% % % % Grid scenarios:

%%
% An integer asymmetric grid (-N/2 to N/2-1)
%                            (e.g. -4 -3 -2 -1 0 1 2 3) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'asymint', 'deltamid');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
    
%%
% An integer symmetric grid (-N/2 to N/2 )
%                            (e.g. -4 -3 -2 -1 0 1 2 3 4) = N+1

% Run test:
[kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symint', 'deltamid');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

%%
% A non-integer symmetric grid (-(N/2-0.5) to +(N/2-0.5))
%                           (e.g. -3.5 -2.5 -1.5 -0.5 0.5 1.5 2.5 3.5) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symnint', 'deltamid');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %              
% % % % % % 3. KSPACE VALUES ARE DELTA FUNCTION (+x)
% % % % Grid scenarios:

%%
% An integer asymmetric grid (-N/2 to N/2-1)
%                            (e.g. -4 -3 -2 -1 0 1 2 3) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'asymint', 'deltax');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
    
%%
% An integer symmetric grid (-N/2 to N/2 )
%                            (e.g. -4 -3 -2 -1 0 1 2 3 4) = N+1

% Run test:
[kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symint', 'deltax');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

%%
% A non-integer symmetric grid (-(N/2-0.5) to +(N/2-0.5))
%                           (e.g. -3.5 -2.5 -1.5 -0.5 0.5 1.5 2.5 3.5) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symnint', 'deltax');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

             
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %              
% % % % % % 4. KSPACE VALUES ARE DELTA FUNCTION (+y)
% % % % Grid scenarios:
%%
% An integer asymmetric grid (-N/2 to N/2-1)
%                            (e.g. -4 -3 -2 -1 0 1 2 3) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'asymint', 'deltay');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
    
%%
% An integer symmetric grid (-N/2 to N/2 )
%                            (e.g. -4 -3 -2 -1 0 1 2 3 4) = N+1

% Run test:
[kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symint', 'deltay');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

%%
% A non-integer symmetric grid (-(N/2-0.5) to +(N/2-0.5))
%                           (e.g. -3.5 -2.5 -1.5 -0.5 0.5 1.5 2.5 3.5) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symnint', 'deltay');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

             
             
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %              
% % % % % % 5. KSPACE VALUES ARE DELTA FUNCTION (+xy)
% % % % Grid scenarios:
%%
% An integer asymmetric grid (-N/2 to N/2-1)
%                            (e.g. -4 -3 -2 -1 0 1 2 3) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'asymint', 'deltaxy');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
    
%%
% An integer symmetric grid (-N/2 to N/2 )
%                            (e.g. -4 -3 -2 -1 0 1 2 3 4) = N+1

% Run test:
[kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symint', 'deltaxy');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

%%
% A non-integer symmetric grid (-(N/2-0.5) to +(N/2-0.5))
%                           (e.g. -3.5 -2.5 -1.5 -0.5 0.5 1.5 2.5 3.5) = N

% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'symnint', 'deltaxy');
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = 'lala.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues, imageSpace, reconstructedBART, ...
                 saveFigureInfo);

             
             
             
% % % % %%
% % % % % 1. Create a k-space
% % % % Nx = 8; Ny = Nx;
% % % % kSpaceValues = zeros(Nx,Ny)+1;
% % % % % % kSpaceConstant(floor(Nx/2)+1, floor(Ny/2)+1) = 1;
% % % % % % kSpaceConstant(Nx/2+1, Ny/2)   = 1;
% % % % % % kSpaceConstant(Nx/2,   Ny/2+1) = 1;
% % % % % % kSpaceConstant(floor(Nx/2)+2, floor(Ny/2)+2) = 1;
% % % % 
% % % % % 2. Create a phantom image
% % % % imageSpace = ifftshift(ifft2(ifftshift(kSpaceValues)));
% % % % 
% % % % if (mod(Nx,2) ~= 0)
% % % %     imageSpace = imageSpace(2:end, 2:end);
% % % % end
% % % % 
% % % % % 3. Create a Nx x Ny grid centered around (0,0)
% % % % [coord_x, coord_y] = meshgrid(-floor(Nx/2) : floor(Nx/2)-1, ...
% % % %                               -floor(Ny/2) : floor(Ny/2)-1 );
% % % % sz = size(coord_x, 1);
% % % % coord_x = reshape(coord_x, [sz*sz, 1])+0.5;
% % % % coord_y = reshape(coord_y, [sz*sz, 1])+0.5;
% % % % coord_z = zeros(size(coord_x));
% % % % kspace_coord = [coord_x , coord_y , coord_z].';
% % % % 
% % % % 
% % % % % 4. Reshape the kspace values into a vector
% % % % kspace_values = reshape(kSpaceValues, [1, Nx*Ny]);
% % % % 
% % % % 
% % % % % 5. Reconstruct with BART
% % % % tic
% % % % igrid = bart('nufft -i -t', kspace_coord, kspace_values);
% % % % reconstructedBART = igrid.';
% % % % toc
% % % % 
% % % % 
% % % % % % PLOT
% % % % fh1 = figure('Position', [10,10,1200,800]);
% % % % nLin = 4;
% % % % nCol = 4;
% % % % 
% % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % PLOT KSPACE
% % % % figure(fh1)
% % % % subplot(nLin,nCol,4)
% % % % imagesc(abs(kSpaceValues))
% % % % title({'We start from:', 'abs(KSPACE)'})
% % % % axis square; %axis off;
% % % % colormap gray; colorbar
% % % % 
% % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % RECONSTRUCTIONS
% % % % % % % PLOT RECONSTRUCTED MATLAB (ANGLE)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,2)
% % % % imagesc(angle(imageSpace))
% % % % title('2D-iFFT (MATLAB) (phase)')
% % % % axis square; %axis off;
% % % % colormap gray; colorbar
% % % % % % % PLOT RECONSTRUCTED BART (ANGLE)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,1)
% % % % imagesc(angle(reconstructedBART))
% % % % title('2D-iFFT (BART) (phase)')
% % % % axis square; %axis off;
% % % % colormap gray; colorbar
% % % % 
% % % % 
% % % % % % % PLOT RECONSTRUCTED MATLAB (REAL)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,6)
% % % % imagesc(real(imageSpace))
% % % % title('2D-iFFT (MATLAB) (real)')
% % % % axis square; %axis off;
% % % % caxis([-max(abs(real(imageSpace(:)))) max(abs(real(imageSpace(:))))])
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT RECONSTRUCTED MATLAB (IMAG)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,10)
% % % % imagesc(imag(imageSpace))
% % % % title('2D-iFFT (MATLAB) (imag)')
% % % % axis square; %axis off;
% % % % caxis([-max(abs(imag(imageSpace(:)))) max(abs(imag(imageSpace(:))))])
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT RECONSTRUCTED MATLAB (ABS)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,14)
% % % % imagesc(abs(imageSpace))
% % % % title('2D-iFFT (MATLAB) (abs)')
% % % % axis square; %axis off;
% % % % caxis([-max(abs((imageSpace(:)))) max(abs((imageSpace(:))))])
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT RECONSTRUCTED BART (REAL)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,5)
% % % % imagesc(real(reconstructedBART))
% % % % title('Reconstruction (BART) (real)')
% % % % axis square; %axis off;
% % % % % caxis([min(abs(real(reconstructedBART(:)))) max(abs(real(reconstructedBART(:))))])
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT RECONSTRUCTED BART (IMAG)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,9)
% % % % imagesc(imag(reconstructedBART))
% % % % title('Reconstruction (BART) (imag)')
% % % % axis square; %axis off;
% % % % % caxis([min(abs(imag(reconstructedBART(:)))) max(abs(imag(reconstructedBART(:))))])
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT RECONSTRUCTED BART (ABS)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,13)
% % % % imagesc(abs(reconstructedBART))
% % % % title('Reconstruction (BART) (abs)')
% % % % axis square; %axis off;
% % % % % caxis([min(abs((reconstructedBART(:)))) max(abs((reconstructedBART(:))))])
% % % % colormap gray; colorbar
% % % % 
% % % % 
% % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % DIFFERENCES
% % % % % % 7. Normalize both reconstructions
% % % % % IMAGE
% % % % imageSpace = imageSpace ./ norm(norm(imageSpace));
% % % % % BART
% % % % reconstructedBART = reconstructedBART ./ norm(norm(reconstructedBART));
% % % % 
% % % % % % % PLOT DIFFERENCES IN ANGLE
% % % % figure(fh1)
% % % % subplot(nLin,nCol,3)
% % % % imagesc(angle(imageSpace) - angle(reconstructedBART))
% % % % title('2D-iFFT (BART) (phase)')
% % % % axis square; %axis off;
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT DIFFERENCE IMAGE (REAL)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,7)
% % % % imagesc(real(imageSpace) - real(reconstructedBART))
% % % % title('Difference Classic - BART (REAL)')
% % % % axis square; axis off;
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT DIFFERENCE IMAGE (IMAG)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,11)
% % % % imagesc(imag(imageSpace) - imag(reconstructedBART))
% % % % title('Difference Classic - BART (IMAG)')
% % % % axis square; axis off;
% % % % colormap gray; colorbar
% % % % 
% % % % % % % PLOT DIFFERENCE IMAGE (ABS)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,15)
% % % % imagesc(abs(imageSpace) - abs(reconstructedBART))
% % % % title('Difference Classic - BART (ABS)')
% % % % axis square; axis off;
% % % % colormap gray; colorbar
% % % % 
% % % % % % % % HISTOGRAMS
% % % % % % % PLOT HISTOGRAM OF DIFFERENCES (REAL)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,8)
% % % % hist(real(imageSpace(:)) - real(reconstructedBART(:)))
% % % % title('Difference Classic - BART (REAL)')
% % % % axis square;
% % % % 
% % % % % % % PLOT HISTOGRAM OF DIFFERENCES (IMAG)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,12)
% % % % hist(imag(imageSpace(:)) - imag(reconstructedBART(:)))
% % % % title('Difference Classic - BART (IMAG)')
% % % % axis square;
% % % % 
% % % % % % % PLOT HISTOGRAM OF DIFFERENCES (ABS)
% % % % figure(fh1)
% % % % subplot(nLin,nCol,16)
% % % % hist(abs(imageSpace(:)) - abs(reconstructedBART(:)))
% % % % title('Difference Classic - BART (ABS)')
% % % % axis square;
% % % % 
% % % % % saveas(gcf,'~/Desktop/deltaKspace_mid_asym.png')
