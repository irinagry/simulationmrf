% 
% Irina Grigorescu
% Date created: 01-12-2017
% 
% Script to test BART reconstruction
% 
clear all; close all; clc

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

%% % % % % %  Testing Scenarios for spiral

% % % % % % 1. KSPACE VALUES ARE CONSTANT & angle = 0
% Run test:
[kSpaceValues0, imageSpace0, reconstructedBART0] = ...
            func_testingBARTspiral(8, 1000, 'constant', deg2rad(0));
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = '~/Desktop/spiral_constantKspace.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues0, imageSpace0, reconstructedBART0, ...
                 saveFigureInfo);

%% % % % % % % 2. KSPACE VALUES ARE CONSTANT & angle = 45
% Run test:
[kSpaceValues45, imageSpace45, reconstructedBART45] = ...
            func_testingBARTspiral(8, 1000, 'constant', deg2rad(45));
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = '~/Desktop/spiral_constantKspace.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues45, imageSpace45, reconstructedBART45, ...
                 saveFigureInfo);

%% See the differences between 1 and 2             
figure
subplot(3,1,1)
imagesc(real(reconstructedBART0-reconstructedBART45))
axis equal
colorbar

subplot(3,1,2)
imagesc(imag(reconstructedBART0-reconstructedBART45))
axis equal
colorbar

subplot(3,1,3)
imagesc(abs(reconstructedBART0-reconstructedBART45))            
axis equal
colorbar

%% % % % % %  Testing Scenarios for cartesian

% % % % % % 1. KSPACE VALUES ARE CONSTANT
% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'constant');
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = '~/Desktop/constantKspace_BARTtraj.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);

%%             
% % % % % % 2. KSPACE VALUE ARE ZEROS and 1 in the middle
% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'deltamid');
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = '~/Desktop/midKspace_BARTtraj.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
             
%%             
% % % % % % 3. KSPACE VALUE ARE ZEROS and 1 in +x
% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'deltax');
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = '~/Desktop/deltaxKspace_BARTtraj.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
             
%%             
% % % % % % 4. KSPACE VALUE ARE ZEROS and 1 in +y
% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'deltay');
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = '~/Desktop/deltayKspace_BARTtraj.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);

             
%%             
% % % % % % 5. KSPACE VALUE ARE ZEROS and 1 in +xy
% Run test:
[kSpaceValues, imageSpace, reconstructedBART] = ...
            func_testingBART(8, 'deltaxy');
        
% Plot results
saveFigureInfo = struct;
saveFigureInfo.toSave   = 0;
saveFigureInfo.fileName = '~/Desktop/deltaxyKspace_BARTtraj.png';
fh1 = figure('Position', [10,10,1200,800]);
plot_testingBART(fh1, kSpaceValues,imageSpace, reconstructedBART, ...
                 saveFigureInfo);
             
             
             