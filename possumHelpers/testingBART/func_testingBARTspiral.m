function [kSpaceValuesForFFT, imageSpace, reconstructedBART] = ...
            func_testingBARTspiral(N, nROPoints, testCase, spiralAngle)
% This function tests BART for various scenarios
% 
% INPUT: 
%   N = size of k-space
%   nROPoints = number of readout points
%   testCase = 
%       'constant' - the artificial k-space is filled with a constant value
%       'deltamid' - 
%       'deltax'   - 
%       'deltay'   - 
%       'deltaxy'  - 
%   spiralAngle = angle of starting of spiral
% 
% OUTPUT:
%   kSpaceValues
%   imageSpace
%   reconstructedBART
% 
% Author: Irina Grigorescu, irinagry@gmail.com

% Prerequisites:
if (mod(N,2) ~= 0)
    warning('FFT works better for powers of 2');
    return
end

% 1. Create k-space
kSpaceValuesForFFT = zeros(N, N);
kSpaceValues = zeros(1, nROPoints);

% 2. Populate k-space
switch testCase
    case 'constant'
        kSpaceValuesForFFT = kSpaceValuesForFFT + 1;
        kSpaceValues       = kSpaceValues       + 1;
    case 'deltamid'
        kSpaceValuesForFFT(floor(N/2)+1, floor(N/2)+1) = 1;
    case 'deltax'
        kSpaceValuesForFFT(floor(N/2)+2, floor(N/2)+1) = 1;
	case 'deltay'
        kSpaceValuesForFFT(floor(N/2)+1, floor(N/2)+2) = 1;
    case 'deltaxy'
        kSpaceValuesForFFT(floor(N/2)+2, floor(N/2)+2) = 1;
end

% 3. Calculate the imag
imageSpace = ifftshift(ifft2(ifftshift(kSpaceValuesForFFT)));

% 4. Create the trajectory bart wants
% Create the spiral k-space
Td = 6; %ms
t = linspace(0,Td,nROPoints);
spiralPower = 1;
spiralAmplitude = 0.35; 
spiralFrequency = 20;

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);

kkk_x = cos(spiralAngle) .* k_x - ...
        sin(spiralAngle) .* k_y ;
kkk_y = sin(spiralAngle) .* k_x + ...
        cos(spiralAngle) .* k_y ;
kkk_z = zeros(1, nROPoints);

traj_spiral = [ kkk_x ; kkk_y ; kkk_z ];

dist_ksp = sqrt( (kkk_x(1) - kkk_x(end) ).^2 + ... 
                 (kkk_y(1) - kkk_y(end) ).^2 ) ;

traj_spiral2 = bart(['scale ', num2str(N/(2*dist_ksp))], traj_spiral);

% 5. Reconstruct with BART
tic
igrid = bart('nufft -i -t ', traj_spiral2, kSpaceValues);
igrid = bart(['resize -c 0 ', num2str(N), ' 1 ', num2str(N)], igrid);
reconstructedBART = igrid;
toc

end