function plot_testingBART(fh1, ...
                          kSpaceValues, imageSpace, reconstructedBART, ...
                          saveFigureInfo)
% This function plots the results of the bart test
% INPUT:
%   fh1 = figure handle
%   kSpaceValues
%   imageSpace
%   reconstructedBART
%   saveFigureInfo = structure with 
%                    .toSave   = 1 or 0 to save or not
%                    .fileName = file name to save to
% OUTPUT:
%   - 
% 
% Irina Grigorescu, irinagry@gmail.com
% 

% Figure info
nLin = 4;
nCol = 4;

% % % % % % % % % % % % % % % % % % % % 
% % % PLOT KSPACE
figure(fh1)
subplot(nLin,nCol,4)
imagesc(abs(kSpaceValues))
title({'We start from:', 'abs(KSPACE)'})
axis square; %axis off;
colormap gray; colorbar

% % % % % % % % % % % % % % % % % % % % 
% % % % RECONSTRUCTIONS
% % % PLOT RECONSTRUCTED MATLAB (ANGLE)
figure(fh1)
subplot(nLin,nCol,2)
imagesc(angle(imageSpace))
title('2D-iFFT (MATLAB) (phase)')
axis square; %axis off;
colormap gray; colorbar
% % % PLOT RECONSTRUCTED BART (ANGLE)
figure(fh1)
subplot(nLin,nCol,1)
imagesc(angle(reconstructedBART))
title('2D-iFFT (BART) (phase)')
axis square; %axis off;
colormap gray; colorbar


% % % PLOT RECONSTRUCTED MATLAB (REAL)
figure(fh1)
subplot(nLin,nCol,6)
imagesc(real(imageSpace))
title('2D-iFFT (MATLAB) (real)')
axis square; %axis off;
caxis([-max(abs(real(imageSpace(:)))) max(abs(real(imageSpace(:))))])
colormap gray; colorbar

% % % PLOT RECONSTRUCTED MATLAB (IMAG)
figure(fh1)
subplot(nLin,nCol,10)
imagesc(imag(imageSpace))
title('2D-iFFT (MATLAB) (imag)')
axis square; %axis off;
caxis([-max(abs(imag(imageSpace(:)))) max(abs(imag(imageSpace(:))))])
colormap gray; colorbar

% % % PLOT RECONSTRUCTED MATLAB (ABS)
figure(fh1)
subplot(nLin,nCol,14)
imagesc(abs(imageSpace))
title('2D-iFFT (MATLAB) (abs)')
axis square; %axis off;
caxis([-max(abs((imageSpace(:)))) max(abs((imageSpace(:))))])
colormap gray; colorbar

% % % PLOT RECONSTRUCTED BART (REAL)
figure(fh1)
subplot(nLin,nCol,5)
imagesc(real(reconstructedBART))
title('Reconstruction (BART) (real)')
axis square; 
colormap gray; colorbar

% % % PLOT RECONSTRUCTED BART (IMAG)
figure(fh1)
subplot(nLin,nCol,9)
imagesc(imag(reconstructedBART))
title('Reconstruction (BART) (imag)')
axis square; 
colormap gray; colorbar

% % % PLOT RECONSTRUCTED BART (ABS)
figure(fh1)
subplot(nLin,nCol,13)
imagesc(abs(reconstructedBART))
title('Reconstruction (BART) (abs)')
axis square; 
colormap gray; colorbar


% % % % % % % % % % % % % % % % % % % % 
% % % % DIFFERENCES
% % % PLOT DIFFERENCES IN ANGLE
figure(fh1)
subplot(nLin,nCol,3)
imagesc(angle(imageSpace) - angle(reconstructedBART))
title('2D-iFFT (BART) (phase)')
axis square; %axis off;
colormap gray; colorbar

% % % PLOT DIFFERENCE IMAGE (REAL)
figure(fh1)
subplot(nLin,nCol,7)
imagesc(real(imageSpace) - real(reconstructedBART))
title('Difference Classic - BART (REAL)')
axis square; axis off;
colormap gray; colorbar

% % % PLOT DIFFERENCE IMAGE (IMAG)
figure(fh1)
subplot(nLin,nCol,11)
imagesc(imag(imageSpace) - imag(reconstructedBART))
title('Difference Classic - BART (IMAG)')
axis square; axis off;
colormap gray; colorbar

% % % PLOT DIFFERENCE IMAGE (ABS)
figure(fh1)
subplot(nLin,nCol,15)
imagesc(abs(imageSpace) - abs(reconstructedBART))
title('Difference Classic - BART (ABS)')
axis square; axis off;
colormap gray; colorbar

% % % % HISTOGRAMS
% % % PLOT HISTOGRAM OF DIFFERENCES (REAL)
figure(fh1)
subplot(nLin,nCol,8)
hist(real(imageSpace(:)) - real(reconstructedBART(:)))
title('Difference Classic - BART (REAL)')
axis square;

% % % PLOT HISTOGRAM OF DIFFERENCES (IMAG)
figure(fh1)
subplot(nLin,nCol,12)
hist(imag(imageSpace(:)) - imag(reconstructedBART(:)))
title('Difference Classic - BART (IMAG)')
axis square;

% % % PLOT HISTOGRAM OF DIFFERENCES (ABS)
figure(fh1)
subplot(nLin,nCol,16)
hist(abs(imageSpace(:)) - abs(reconstructedBART(:)))
title('Difference Classic - BART (ABS)')
axis square;

if saveFigureInfo.toSave == 1
    saveas(gcf, saveFigureInfo.fileName)
end