function [kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(N, testCase)
% This function tests BART for various scenarios
% 
% INPUT: 
%   N = size of k-space
%   testCase = 
%       'constant' - the artificial k-space is filled with a constant value
%       'deltamid' - 
%       'deltax'   - 
%       'deltay'   - 
%       'deltaxy'  - 
% 
% OUTPUT:
%   kSpaceValues
%   imageSpace
%   reconstructedBART
% 
% Author: Irina Grigorescu, irinagry@gmail.com

% Prerequisites:
if (mod(N,2) ~= 0)
    warning('FFT works better for powers of 2');
    return
end

% 1. Create k-space
kSpaceValues = zeros(N, N);

% 2. Populate k-space
switch testCase
    case 'constant'
        kSpaceValues = kSpaceValues + 1;
    case 'deltamid'
        kSpaceValues(floor(N/2)+1, floor(N/2)+1) = 1;
    case 'deltax'
        kSpaceValues(floor(N/2)+2, floor(N/2)+1) = 1;
	case 'deltay'
        kSpaceValues(floor(N/2)+1, floor(N/2)+2) = 1;
    case 'deltaxy'
        kSpaceValues(floor(N/2)+2, floor(N/2)+2) = 1;
end

% 3. Calculate the imag
imageSpace = ifftshift(ifft2(ifftshift(kSpaceValues)));

% 4. Create the trajectory bart wants
traj_cart = bart(['traj -x', num2str(N), ' -y', num2str(N)]);

% 5. Reconstruct with BART
tic
igrid = bart('nufft -i -t ', traj_cart, ...
                             reshape(kSpaceValues, [1, N, N]));
reconstructedBART = igrid;
toc

end