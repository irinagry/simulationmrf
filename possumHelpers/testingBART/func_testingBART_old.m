function [kSpaceValues,imageSpace, reconstructedBART] = ...
            func_testingBART(N, flagSymmetry, testCase)
% This function tests BART for various scenarios
% 
% INPUT: 
%   N = size of k-space
%   flagSymmetry = 
%       'asymint' - an integer asymmetric grid (-N/2 to N/2-1)
%                   (e.g. -4 -3 -2 -1 0 1 2 3)                 = N
%       'symint'  - an integer  symmetric grid (-N/2 to N/2  )
%                   (e.g. -4 -3 -2 -1 0 1 2 3 4)               = N+1
%       'symnint' - a non-integer symmetric grid (-(N/2-0.5) to +(N/2-0.5))
%                   (e.g. -3.5 -2.5 -1.5 -0.5 0.5 1.5 2.5 3.5) = N
%   testCase = 
%       'constant' - 
%       'deltamid' - 
%       'deltax'   - 
%       'deltay'   - 
%       'deltaxy'  - 
% 
% OUTPUT:
%   kSpaceValues
%   imageSpace
%   reconstructedBART
% 
% Author: Irina Grigorescu, irinagry@gmail.com

% Prerequisites:
if (mod(N,2) ~= 0)
    warning('FFT works better for powers of 2');
    return
end

% 1. Create k-space
if strcmp(flagSymmetry, 'symint') == 1
    N = N+1;
end
    kSpaceValues = zeros(N, N);

% 2. Populate k-space
switch testCase
    case 'constant'
        kSpaceValues = kSpaceValues + 1;
    case 'deltamid'
        kSpaceValues(floor(N/2)+1, floor(N/2)+1) = 1;
    case 'deltax'
        kSpaceValues(floor(N/2)+2, floor(N/2)+1) = 1;
	case 'deltay'
        kSpaceValues(floor(N/2)+1, floor(N/2)+2) = 1;
    case 'deltaxy'
        kSpaceValues(floor(N/2)+2, floor(N/2)+2) = 1;
end

% 3. Calculate the imag
if strcmp(flagSymmetry, 'symint') == 1
    imageSpace = ifftshift(ifft2(ifftshift(...
                        kSpaceValues(1:end-1, 1:end-1))));
else
    imageSpace = ifftshift(ifft2(ifftshift(kSpaceValues)));
end


% 4. Create a N x N grid centered around (0,0)
% Create symmetric grid
if strcmp(flagSymmetry, 'symint') == 1
    [coord_x, coord_y] = meshgrid(-floor(N/2) : floor(N/2), ...
                                  -floor(N/2) : floor(N/2) );
% Create asymmetric grid
else
    [coord_x, coord_y] = meshgrid(-floor(N/2) : floor(N/2)-1, ...
                                  -floor(N/2) : floor(N/2)-1 );
end
% Create symmetric grid with -3.5 -2.5 -1.5 -0.5 0.5 ...
if strcmp(flagSymmetry, 'symnint') == 1
    coord_x = coord_x + 0.5;
    coord_y = coord_y + 0.5;
end
% Create the k-space coordinates as BART wants them
sz = size(coord_x, 1);
coord_x = reshape(coord_x, [sz*sz, 1]);
coord_y = reshape(coord_y, [sz*sz, 1]);
coord_z = zeros(size(coord_x));
kspace_coord = [coord_y , coord_x , coord_z].';

% 5. Reconstruct with BART
tic
% traj_cart = bart(['traj -x', num2str(N), ' -y', num2str(N)]);
% igrid = bart('nufft -i -t ', traj_cart, ...
%                           reshape(kSpaceValues, [1, N, N]));
igrid = bart('nufft -i -t ', kspace_coord, ...
                             reshape(kSpaceValues, [1, N*N] ));
reconstructedBART = igrid;
toc

end