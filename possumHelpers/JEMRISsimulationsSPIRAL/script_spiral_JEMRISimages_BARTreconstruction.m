% 
% Irina Grigorescu
% Date created: 09-01-2018
% 
% Script to reconstruct a JEMRIS spiral simulation with BART
% and compare with the EPI simulation
% 
% 1. First things first you will need to run:
% /Users/irina/jemrisSims/jan2018/epi/script_compareEPI_JEMRISimages_BARTimages.m
% for 1 TR
% 2. Then you will have to run this for 1 TR and see which one performs
% best

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
imagesJEMRISepi = imagesJEMRIS;
imagesBARTepi   = imagesBART;
 
 
%% 1. Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralFullySampledConstant1TR/';

% Set number of repetitions to get to the fully sampled
NReps = 1;
% Set number of TR blocks
NTRs  = 1; 
% Set effective image size
Nx   = 32; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 6000; 

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NReps, NTRs);
kspace_z = zeros(nROPoints*NReps, 1);

% Create the spiral k-space
Td = 6; %ms
t = linspace(0,Td,nROPoints);
spiralPower = 1;
% % % % CASE 1
% spiralAmplitude = 0.2; 
% spiralFrequency = 40;
% % % % CASE 2
% spiralAmplitude = 0.2; 
% spiralFrequency = 20;
% % % % CASE 3 - prefer this one
% spiralAmplitude = 0.4; 
% spiralFrequency = 20;
% % % % CASE 4
% spiralAmplitude = 0.4; 
% spiralFrequency = 40;
% % % % CASE 5 - seems to reconstruct more like the EPI one
spiralAmplitude = 0.35; 
spiralFrequency = 20;

kkk_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
kkk_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);


% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure(111)
for i = 1:NTRs
    
    % Populate the spiral k-spaces
    kspace_x(:,i) = kkk_x.';
    kspace_y(:,i) = kkk_y.';
    
    % Plot the coordinates before scaling
    subplot(1,2,1); xl = 1;
    scatter(kspace_x(:, i),kspace_y(:, i),'.'), axis equal, axis square;
    xlabel('k_x rad mm^{-1}'); ylabel('k_y rad mm^{-1}');
    title(['Spiral ', num2str(i)])
    xlim([-xl xl]); ylim([-xl xl]);

    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,i) - kspace_x(end,i) ).^2 + ... 
                     (kspace_y(1,i) - kspace_y(end,i) ).^2 ) ;
    traj_spiral = [ kspace_x(:,i), ...
                    kspace_y(:,i), ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, i) = traj_spiral2(1,:);
    kspace_y(:, i) = traj_spiral2(2,:);
    
    % Plot the coordinates after scaling
    subplot(1,2,2)
    scatter(kspace_x(:, i), kspace_y(:, i),'.'), axis equal, axis square;
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(i)])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

    drawnow
end

%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NReps, NTRs); % NROpoints x Nrepetitions x NTRs

for i = 1:NReps
    % Get signal values from JEMRIS simulation
    %filenameSignal = [folderName, 'signals', num2str(i),'.h5']; 
    filenameSignal = [folderName, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Get timings of signal readout
    timingRO = h5read(filenameSignal, '/signal/times');
    % % % Readout points
    Nro = size(timingRO, 1);
    % % % Sort them and take signal in sorted order
    [timingRO,J] = sort(timingRO); Mvecs = A(:,:);
    % % % Calculate timings between each line of k-space and
    % % % between different slices
    d = diff(diff(timingRO));
    d(d<1e-5) = 0;

    % % % And store them in the solumn vector of indices I
    % % % together with index 0 and last index
    I = [0; find(d) + 1; length(timingRO)];

    % % % Get kspace as one signal
    for j = 1:NTRs
        kspaces(:,i,j) = Mvecs((j-1)*nROPoints+1 : j*nROPoints, 1).' + ...
              sqrt(-1) * Mvecs((j-1)*nROPoints+1 : j*nROPoints, 2).';

    end
end

% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints*NReps, NTRs]);


% % 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);

figure(222)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = squeeze(kspace_x(:, i));
    currentKspace_y = squeeze(kspace_y(:, i));
    currentKspaceToReco = (squeeze(kspaces(:, i))).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART(:, :, i) = igrid;
    toc

    % Plot images (ABS)
    subplot(1,3,1), imagesc(abs(igrid))
    title('BART reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(1,3,2), imagesc(real(igrid))
    title('BART reconstruction (real)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(1,3,3), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    pause(0.01)
end


return
%% 4. Plot the EPI and the spiral reconstructions
figure

% % % % EPI RECONSTRUCTIONS
% BART (ABS)
subplot(3,3,1), imagesc(abs(imagesBARTepi))
title('BART epi reconstruction (abs)')
axis square; axis off;
colormap gray;
colorbar;
% BART (real)
subplot(3,3,2), imagesc(real(imagesBARTepi))
title('BART epi reconstruction (real)')
axis square; axis off;
colormap gray;
colorbar;
% BART (imag)
subplot(3,3,3), imagesc(real(imagesJEMRISepi))
title('JEMRIS epi reconstruction (real)')
axis square; axis off;
colormap gray;
colorbar;
% JEMRIS (ABS)
subplot(3,3,4), imagesc(abs(imagesJEMRISepi))
title('JEMRIS epi reconstruction (abs)')
axis square; axis off;
colormap gray;
colorbar;
% JEMRIS (real)
subplot(3,3,5), imagesc(imag(imagesBARTepi))
title('BART epi reconstruction (imag)')
axis square; axis off;
colormap gray;
colorbar;
% JEMRIS (imag)
subplot(3,3,6), imagesc(imag(imagesJEMRISepi))
title('JEMRIS epi reconstruction (imag)')
axis square; axis off;
colormap gray;
colorbar;
% % % % SPIRAL
% BART (ABS)
subplot(3,3,7), imagesc(abs(imagesBART))
title('BART spiral reconstruction (abs)')
axis square; axis off;
colormap gray;
colorbar;
% BART (real)
subplot(3,3,8), imagesc(real(imagesBART))
title('BART spiral reconstruction (real)')
axis square; axis off;
colormap gray;
colorbar;
% BART (imag)
subplot(3,3,9), imagesc(imag(imagesBART))
title('BART spiral reconstruction (imag)')
axis square; axis off;
colormap gray;
colorbar;
