% 
% Irina Grigorescu
% Date created: 09-01-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps
% Trying some things out - not going to use this script

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% 0. Prerequisites (PROPERTIES OF THE SEQUENCE)
% Number of TR blocks
NTRs  = 2; 
% Effective image size
Nx   = 32; Ny = Nx; 
% Number of redout points per TR
nROPoints = 6000; 

% % 1a. Create spiral in matlab
Td = 6; %ms
dt = Td / nROPoints;
t = 0:dt:Td-dt;
spiralPower = 1;

spiralAmplitude = 0.35; 
spiralFrequency = 20;
rotAngleConsecutiveTR = [0 3.1416 0 3.1416 0 3.1416];

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);
kk_x_M = zeros(nROPoints, NTRs); kk_y_M = zeros(nROPoints, NTRs);
for i = 1:NTRs
    % Calculate k-space trajectory for each TR
    kk_x_M(:,i) = cos(rotAngleConsecutiveTR(i)) .* k_x - ...
                  sin(rotAngleConsecutiveTR(i)) .* k_y ;
    
    kk_y_M(:,i) = sin(rotAngleConsecutiveTR(i)) .* k_x + ... 
                  cos(rotAngleConsecutiveTR(i)) .* k_y;
end

%% 1b. Get k-space coordinates from a JEMRIS simulation
% Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralFullySampledConstantNTR/';

% Get sequence values
t   = h5read([folderName,'seq.h5'],'/seqdiag/T');    % timings
RXP = h5read([folderName,'seq.h5'],'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read([folderName,'seq.h5'],'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read([folderName,'seq.h5'],'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read([folderName,'seq.h5'],'/seqdiag/KZ');   % KZ (rad/mm)
% Get k-space trajectory
t  =  t(RXP==0);
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure(111)

for idxTR = 1:NTRs
    % Populate the spiral k-spaces
    kspace_x(:,idxTR) = KX(((idxTR-1)*nROPoints+1) : idxTR*nROPoints); %kk_x_M(:,idxTR); %
    kspace_y(:,idxTR) = KY(((idxTR-1)*nROPoints+1) : idxTR*nROPoints); %kk_y_M(:,idxTR); %
    
    % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
    subplot(2,3,1); xl = 3;
    % Coordinates from JEMRIS
    scatter(kspace_x(:, idxTR), kspace_y(:, idxTR), '.');
	axis equal, axis square;
    xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
    title(['Spiral coords ', num2str(idxTR), ' (JEMRIS)'])
    xlim([-xl xl]); ylim([-xl xl]);
    
    % % % % % % % Plot the coordinates from matlab and jemris
    subplot(2,3,2)
    % Jemris
    sb1 = scatter(kspace_x(:, idxTR), kspace_y(:, idxTR), '.r'); 
    hold on
    % Matlab
    sb2 = scatter(kk_x_M(:,idxTR), kk_y_M(:,idxTR), 'ob');
	axis equal; axis square; xlim([-xl xl]); ylim([-xl xl]);
    xlabel('k_x'); ylabel('k_y');
    title(['Spiral coords ', num2str(idxTR), ' (JEMRIS vs MATLAB)'])

    % % % % % % % Plot the difference in coordinates 
    subplot(2,3,[4,6])
    sb3 = plot(kspace_x(:,idxTR)-(kk_x_M(:,idxTR)), 'r.-'); hold on
    sb4 = plot(kspace_y(:,idxTR)-(kk_y_M(:,idxTR)), 'b.-');
    legend('k_x_J - k_x_M','k_y_J - k_y_M')
    
    % % % % % % % Transform the kspace coordinates to the 
    % % % % % % % desired effective imaging matrix
    % % % % % % % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
    x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:);% + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:);% + y0_ksp;
    
    % % % % % % % Plot the coordinates after scaling
    subplot(2,3,3)
    % Jemris
    scatter(kspace_x(:, idxTR), kspace_y(:, idxTR), '.r'); 
	axis equal; axis square; 
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(i), ' (JEMRIS scaled for reco)'])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);
    
    drawnow
    
    if idxTR ~= NTRs
        delete(sb1); delete(sb2); delete(sb3); delete(sb4); 
    end
end

%%  Calculate the difference in angle (phi of complex representation)
% % between calculated trajectory and jemris trajectory
k_M_cmplx = kk_x_M(:)  + 1i .* kk_y_M(:);     % calculated traj
k_J_cmplx = KX         + 1i .* KY;         % jemris     traj

% Phase difference between the 2
phaseDiff = angle(k_M_cmplx) - angle(k_J_cmplx);

% Phase Correction 
phaseCorrection = -phaseDiff;
phaseCorrection = reshape(phaseCorrection, [nROPoints, NTRs]);

% % % %% Figure out what is wrong with the coords
% % % 
% % % figure, plot(1:nROPoints, kk_x_M(:,2)-KX(nROPoints+1:end),'r.')
% % % 
% % % figure
% % % plot(1:nROPoints, real(kk_x_M(:,1)+1i.*kk_y_M(:,1)), 'r.'); hold on
% % % plot(1:nROPoints, real(KX(1:nROPoints)+1i.*KY(1:nROPoints)), 'b.')
% % % 
% % % %%
% % % figure, plot(KX), title('K_X from jemris')
% % % figure, plot(kk_x_M(:)), title('K_X from matlab')
% % % figure, plot(kk_x_M(:)), hold on, plot(KX), legend('mat','jem')
% % % figure, plot(kk_x_M(:)-KX), title('K_X matlab - K_X jemris')
% % % 
% % % %%
% % % k_M_cmplx = kk_x_M(:,1)+1i.*kk_y_M(:,1);
% % % k_J_cmplx = KX(1:nROPoints)+1i.*KY(1:nROPoints);
% % % figure
% % % subplot(3,1,1)
% % % plot(1:nROPoints, real(k_M_cmplx - k_J_cmplx));
% % % subplot(3,1,2)
% % % plot(1:nROPoints, imag(k_M_cmplx - k_J_cmplx));
% % % subplot(3,1,3)
% % % plot(1:nROPoints, abs(k_M_cmplx - k_J_cmplx));


%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderName, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
% % % Store the signal
Mvecs = A;

% % % Get kspace as one signal
for i = 1:NTRs
    % Get signal values
    disp([num2str(i), ' - from: ', num2str((i-1)*nROPoints+1), ' to ', num2str(i*nROPoints)]);
    kspaces(:,i) = ( Mvecs(((i-1)*nROPoints+1) : i*nROPoints, 1) + ...
          sqrt(-1) * Mvecs(((i-1)*nROPoints+1) : i*nROPoints, 2) ).';
end

% % 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);

figure(223)
for i = 1:NTRs
    tic
    disp(['Image number ', num2str(i)]);
    % Retrieve current values for current reconstruction
    currentKspace_x = kspace_x(:, i);
    currentKspace_y = kspace_y(:, i);
    currentKspaceToReco = kspaces(:, i);
    
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';
                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco.');
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART(:, :, i) = igrid;
    toc

    % Plot images (ABS)
    subplot(2,3,1), imagesc(abs(igrid))
    title('BART reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,2), imagesc(real(igrid))
    title(['BART reconstruction (real) ', num2str(i)])
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,3), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;
    
    subplot(2,3,5)
    scatter(currentKspace_x, currentKspace_y, '.');
    axis square; 
    
    pause(0.01)
end

% % % % Normalize the reconstructed images
[~, imagesBART] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBART = (reshape(imagesBART, [Nx, Ny, NTRs]));

