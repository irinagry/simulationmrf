% 
% Irina Grigorescu
% Date created: 04-02-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps
% 
% NO MOTION CASE FOR FULLY SAMPLED SPIRAL

% % % % % % % % % % % %
% TO GET HERE GO TO:
% cd '/Users/irina/OneDrive - University College London/Work/Simulation/simulationMRF/possumHelpers/JEMRISsimulationsSPIRAL/motionVSnomotion/'
% % % % % % % % % % % %


% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% K-space coordinates
% % % % % PROPERTIES OF THE SEQUENCE
% Set number of TR blocks
NTRs = 250; %250; 
% Set effective image size
Nx   = 96; Ny = Nx; %64
% Set number of redout points per TR
nROPoints = 6000; 

% % % % % PROPERTIES OF SPIRAL (MATLAB)
% Create the spiral k-space in MATLAB
Td = 6; %ms
dt = Td / nROPoints;
t = dt:dt:Td;
spiralPower = 1;

spiralAmplitude =    0.4; 
spiralFrequency =  120;  
rotAngleConsecutiveTR = repmat([0 0], [1, NTRs]); 

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);

kk_x_M = zeros(nROPoints, NTRs); kk_y_M = zeros(nROPoints, NTRs);
for idxTR = 1:NTRs
    % Calculate k-space trajectory for each TR
    kk_x_M(:,idxTR) = cos(rotAngleConsecutiveTR(idxTR)) * k_x - ...
                      sin(rotAngleConsecutiveTR(idxTR)) * k_y ;
    
    kk_y_M(:,idxTR) = sin(rotAngleConsecutiveTR(idxTR)) * k_x + ... 
                      cos(rotAngleConsecutiveTR(idxTR)) * k_y;
end
clear k_x k_y 
clear rotAngleConsecutiveTR spiralAmplitude spiralFrequency spiralPower

%%
% % % % % PROPERTIES OF THE SPIRAL TAKEN FROM JEMRIS
% Get k-space coordinates from JEMRIS simulation
folderName = '~/OneDrive - University College London/Work/JEMRISSimulations/jan2018/spiralFullySampledConstantNTRSmallerPhantom4145Spins/nomotion/';

% Get sequence values
RXP = h5read([folderName,'seq.h5'],'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read([folderName,'seq.h5'],'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read([folderName,'seq.h5'],'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read([folderName,'seq.h5'],'/seqdiag/KZ');   % KZ (rad/mm)
GX  = h5read([folderName,'seq.h5'],'/seqdiag/GX');   % GX
GY  = h5read([folderName,'seq.h5'],'/seqdiag/GY');   % GY
FA  = h5read([folderName,'seq.h5'],'/seqdiag/TXM');  % FA
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure(111)
for idxTR = 1:NTRs
    
    % Populate the spiral k-spaces
    kspace_x(:,idxTR) = KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints); %kk_x_M(:,i); %
    kspace_y(:,idxTR) = KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints); %kk_y_M(:,i); %
    
    % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
    subplot(2,2,1); xl = 2;
    % Coordinates from JEMRIS
    scatter(kk_x_M((idxTR-1)*nROPoints+1 : idxTR*nROPoints), ...
            kk_y_M((idxTR-1)*nROPoints+1 : idxTR*nROPoints), '.');
	axis equal, axis square;
    xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
    title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
    xlim([-xl xl]); ylim([-xl xl]);

    % % % % % % Plot the difference between the coordinates
    subplot(2,2,[3,4]);
    % k_x differences between jemris and matlab
    x1 = plot(1:nROPoints, KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints) - ...
                           kk_x_M(:, idxTR), 'r.-');
    hold on
    % k_y differences between jemris and matlab
    x2 = plot(1:nROPoints, KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints) - ...
                           kk_y_M(:, idxTR), 'b.-');
    xlabel('#ro points'); ylabel('Difference between k-space coord'); 
    title(['Spiral ', num2str(idxTR), ' (JEMRIS - MATLAB)'])
    legend('k_x', 'k_y')
    
    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
	x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:) + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:) + y0_ksp;
    
    % Plot the coordinates after scaling
    subplot(2,2,2)
    scatter(kspace_x(:, idxTR), kspace_y(:, idxTR),'.'), axis equal, axis square;
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(idxTR), ' (JEMRIS scaled for reco)'])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

    if mod(idxTR,50) == 0 || (idxTR == 1)
        drawnow
    end
    
    if idxTR ~= NTRs
        delete(x1); delete(x2);
    end
end

%% Simulate dictionary
% % % % % % % % % % % Generate dictionary with old ALGORITHM
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Sequence
% Maryia's FAs
FA_mrf = [5.7014 6.3232 6.9954 7.7159 8.4826 9.2931 10.145 11.036 11.962 12.921 13.91 14.924 15.961 17.016 18.084 19.162 20.244 21.326 22.402 23.467 24.516 25.544 26.545 27.514 28.445 29.335 30.177 30.968 31.704 32.38 32.994 33.541 34.021 34.431 34.77 35.036 35.23 35.352 35.403 35.383 35.294 35.139 34.92 34.64 34.3 33.906 33.459 32.964 32.423 31.84 31.219 30.563 29.874 29.156 28.412 27.644 26.855 26.048 25.224 24.385 23.533 22.672 21.801 20.924 20.041 19.157 18.271 17.388 16.509 15.637 14.776 13.927 13.094 12.281 11.49 10.725 9.9886 9.2839 8.6137 7.9806 7.3868 6.8344 6.325 5.8597 5.4394 5.0645 4.735 4.4506 4.2102 4.0127 3.8563 3.7391 3.6585 3.612 3.5963 3.6083 3.6444 3.701 3.7742 3.8602 3.955 4.0547 4.1554 4.2533 4.3448 4.4264 4.4949 4.5473 4.5811 4.5938 4.5836 4.549 4.489 4.403 4.2909 4.1531 3.9906 3.8048 3.5974 3.371 3.1281 2.8718 2.6057 2.3333 2.0586 1.7855 1.5181 1.2605 1.0168 0.791 0.5868 0.40785 0.25752 0.13892 0.054855 0.0078284 0 0.033191 0.10887 0.22817 0.39185 0.60034 0.85374 1.1518 1.494 1.8794 2.307 2.7753 3.2828 3.8274 4.4073 5.0202 5.6636 6.3352 7.0323 7.7522 8.4923 9.2498 10.022 10.806 11.599 12.399 13.203 14.009 14.814 15.617 16.415 17.207 17.991 18.766 19.531 20.286 21.031 21.765 22.489 23.204 23.911 24.611 25.306 25.999 26.692 27.386 28.084 28.789 29.503 30.229 30.967 31.72 32.489 33.276 34.08 34.902 35.741 36.597 37.468 38.353 39.25 40.156 41.07 41.988 42.907 43.825 44.739 45.646 46.543 47.427 48.294 49.144 49.972 50.777 51.557 52.308 53.03 53.719 54.375 54.996 55.58 56.126 56.634 57.102 57.531 57.919 58.268 58.578 58.85 59.085 59.285 59.452 59.588 59.696 59.779 59.839 59.879 59.904 59.915 59.916 59.909 59.897 59.883 59.868 59.854 59.843 59.835 59.831 59.831 59.836 59.844 59.856 59.871 59.889 59.907 59.925 59.943 59.96 59.974 59.985 59.994 59.999 60 59.997 59.991 59.981 59.968 59.952 59.933 59.911 59.886 59.859 59.829 59.795 59.758 59.716 59.669 59.614 59.552 59.48 59.397 59.301 59.19 59.062 58.917 58.751 58.563 58.353 58.118 57.858 57.573 57.261 56.923 56.559 56.169 55.755 55.318 54.86 54.381 53.885 53.373 52.849 52.315 51.773 51.226 50.678 50.132 49.589 49.053 48.526 48.01 47.507 47.018 46.546 46.09 45.65 45.228 44.822 44.431 44.054 43.69 43.336 42.99 42.65 42.312 41.974 41.633 41.286 40.93 40.562 40.179 39.78 39.361 38.922 38.46 37.975 37.466 36.933 36.376 35.796 35.193 34.571 33.93 33.273 32.604 31.925 31.241 30.554 29.869 29.19 28.522 27.868 27.234 26.622 26.037 25.482 24.961 24.476 24.03 23.625 23.262 22.943 22.667 22.434 22.244 22.096 21.987 21.917 21.883 21.882 21.912 21.969 22.052 22.156 22.28 22.42 22.574 22.74 22.915 23.098 23.288 23.483 23.681 23.884 24.089 24.297 24.507 24.72 24.935 25.153 25.374 25.597 25.823 26.051 26.282 26.516 26.751 26.988 27.227 27.466 27.705 27.943 28.18 28.414 28.645 28.872 29.093 29.307 29.514 29.712 29.9 30.077 30.243 30.395 30.533 30.657 30.766 30.859 30.935 30.996 31.04 31.067 31.078 31.073 31.052 31.016 30.964 30.896 30.814 30.717 30.605 30.478 30.336 30.179 30.007 29.819 29.616 29.396 29.16 28.908 28.639 28.354 28.052 27.735 27.401 27.052 26.688 26.31 25.918 25.513 25.096 24.667 24.227 23.776 23.315 22.844 22.363 21.872 21.372 20.861 20.341 19.809 19.267 18.712 18.146 17.568 16.977 16.373 15.757 15.128 14.487 13.836 13.175 12.506 11.831 11.152 10.472 9.7952 9.1241 8.4629 7.8158 7.1872 6.5815 6.0035 5.4576 4.9484 4.4801 4.0567 3.6821 3.3596 3.0921 2.8819 2.7313 2.6415 2.6137 2.6485 2.746 2.906 3.1281 3.4113 3.7546 4.1568 4.6164 5.1319].';
FA_mrf = FA_mrf(1:NTRs);

PhA_mrf =  zeros(NTRs, 1) ; 
TR_mrf  =  zeros(NTRs, 1) + 15;
RO_mrf  =  zeros(NTRs, 1) +  6;

% % % % Custom Material
T1_mrf = [500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
% T1_mrf = [ T1_mrf T1_mrf + 100];
% T1_mrf = [T1_mrf [500, 600, 950, 1000, 1200, 3000, 4000, 4500]];
T2_mrf = [360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];
% T2_mrf = [T2_mrf T2_mrf + 100];
% T2_mrf = [T2_mrf [ 70,  80, 100,  110,  200, 1000, 2000, 2200]];
df_mrf =   0;

% % % % Generate dictionary
[ ~, ...                     % material properties
  sequenceProperties, ...    % sequence properties
  materialTuples, ...        % material tuples
  M, ...                     % number of material tuples
  ~, ...                     % dictionary MxNx3
  dictionaryMRFComplex, ...  % 
  dictionaryMRFNorm, ...     % dictionary normalised MxN
  dictionaryMRFNorm2Ch, ...  % dictionary normalised Mx2N
  signalDictionaryMRF] = ... % dictionary normalised MxN and absolute val
    func_generateCustomSignals(NTRs, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG, 1);

% % % % %% Example dictionary
% % % % figure
% % % % for i = 1:20:100
% % % %     plot(1:NTRs, abs(dictionaryMRFNorm(1+(i-1)*9,:)),'LineWidth',1.4), hold on
% % % % end
% % % % plot(1:NTRs, abs(dictionaryMRFNorm(1051,:)),'LineWidth',1.4)
% % % % xlabel('TR index');
% % % % ylabel('Signal (a.u.)')
% % % % grid on



%% Create GROUND TRUTH MAPS

% For the 96x96 phantom with 4145 spins
centres = [41,27 ; 56,27 ; ...
           27,41 ; 41,41 ; 56,41 ; 70,41 ; ...
           27,56 ; 41,56 ; 56,56 ; 70,56 ; ...
           41,69 ; 56,69 ; ...
           49,49 ]; % big circle
radii   = [zeros(1,12) + 4 , 32];

realMap = struct;
myPhantomT1Values = [500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
myPhantomT2Values = [360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];

[realMap.T1map, realMap.T2map, maskAllCircles, maskHollow] = ...
         func_createRealT1T2Maps(myPhantomT1Values, myPhantomT2Values, ...
                                 Nx, Ny, centres, radii);


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % NO MOTION
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 
folderName = '~/OneDrive - University College London/Work/JEMRISSimulations/jan2018/spiralFullySampledConstantNTRSmallerPhantom4145Spins/nomotion/';
% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderName, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end

 
% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints, NTRs]);


% % 3. Reconstruct with BART
% Save all images
imagesBART_nomot = zeros(Nx, Ny, NTRs);

figure(115)
for idxTR = 1:NTRs
    tic
    disp(['Image number ', num2str(idxTR)]);
    % Retrieve current values for current reconstruction
    currentKspace_x     = kspace_x(:, idxTR);
    currentKspace_y     = kspace_y(:, idxTR);
    currentKspaceToReco = (kspaces(:, idxTR)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';

                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART_nomot(:, :, idxTR) = igrid;
    toc

    % Plot images (ABS)
    if mod(idxTR, 25) == 0 || (idxTR == 1)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) ', num2str(idxTR)])
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;
    end
    
    
    pause(0.01)
    
    clear igrid traj_kspace
end

% % % % Normalize the reconstructed images
[~, imagesBART_nomot] = normalizeSignals( ...
                    [ real(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ]);
imagesBART_nomot = (reshape(imagesBART_nomot, [Nx, Ny, NTRs]));


%% DICTIONARY MATCHING FOR NO MOTION
imagesBARTReshaped_nomot = [ real(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ...
                             imag(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped_nomot);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB_nomot = matTuplesMatchedB1;
resultsB_nomot.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                         valuesOfMatchB3, valuesOfMatchB4];
resultsB_nomot.scoremap = reshape(resultsB_nomot.score, [Nx, Ny]);                      
resultsB_nomot.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB_nomot.indicesmap = reshape(resultsB_nomot.indices, [Nx, Ny]); 
resultsB_nomot.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB_nomot.T1map = reshape(resultsB_nomot.T1, [Nx, Ny]);
resultsB_nomot.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB_nomot.T2map = reshape(resultsB_nomot.T2, [Nx, Ny]);                
   


%% % % % % Reconstructed maps of T1 and T2 and the real (ground truth) maps
figure
% T1 real
subplot(2,3,1)
imagesc(realMap.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T1_mrf)])
axis off
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(realMap.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T2_mrf)])
axis off
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(resultsB_nomot.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T1_mrf)])
axis off
title('T_1 Reconstructed')

% BART - T2
subplot(2,3,5)
imagesc(resultsB_nomot.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T2_mrf)])
axis off
title('T_2 Reconstructed')

% BART - Score
subplot(2,3,6)   
imagesc(resultsB_nomot.scoremap.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0 1])
axis off
title('Matching Scores')

%%
% % % % % Difference maps of T1 and T2 between reconstructed and GT

figure
% BART - T1
subplot(1,2,1)
imagesc((resultsB_nomot.T1map-realMap.T1map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_1 Reconstructed - T_1 Ground Truth')
caxis([-500 500])

% BART - T2
subplot(1,2,2)
imagesc((resultsB_nomot.T2map-realMap.T2map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_2 Reconstructed - T_2 Ground Truth')
caxis([-150 150])




%%
% % % % % PERCENTAGE ERROR NO MOTION
figure
% BART - T1
subplot(1,2,1)
normMatrix = realMap.T1map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_nomot.T1map-realMap.T1map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_1)')

% BART - T2
subplot(1,2,2)
normMatrix = realMap.T2map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_nomot.T2map-realMap.T2map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_2)')


%% Have a look at individual signals NO MOTION

imagesBARTReshaped1Ch_nomot = reshape(imagesBART_nomot, [Nx*Ny, NTRs ]);

% Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(34-1)*Nx+49, ... %big circle
             (57-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+27, ... %c1
             (40-1)*Nx+41];    %c4
                       
scoreValB = resultsB_nomot.score(scoreIdxB);
% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4'};

% % % % Is this the voxel I was looking for?
figure, 
llPhantom = realMap.T1map.*maskAllCircles;
llPhantom = llPhantom(:);
llPhantom(scoreIdxB(1)) = 0; 
colormap hot; colorbar
llPhantom = reshape(llPhantom, [Nx, Ny]);
imagesc(llPhantom), axis square


for idxTR = 1:size(scoreIdxB,2)
    
    figure('Position', [50,10,600,800])    
    % BY CHANNELS
    subplot(3,1,1)
    plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsB_nomot.indices(scoreIdxB(idxTR)), :), '.--')
    hold on
    plot(1:2*NTRs, imagesBARTReshaped_nomot(scoreIdxB(idxTR), :), '.-')
    legend('dictionary', 'image')
    title(['BART (by channels) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

    % DIFFERENCE
    subplot(3,1,2)
    plot(1:2*NTRs, ...
         dictionaryMRFNorm2Ch(resultsB_nomot.indices(scoreIdxB(idxTR)), :) - ...
         imagesBARTReshaped_nomot(scoreIdxB(idxTR), :), '.-')
    title(['BART (by channels) difference ',  ...
        titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
    
    % ABS
    subplot(3,1,3)
    plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_nomot.indices(scoreIdxB(idxTR)), :)), 'r-')
    hold on
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_nomot(scoreIdxB(idxTR), :)), 'b--')
    legend('dictionary', 'phantom')
    xlabel('#image'); ylabel('normalised signal intensity (a.u.)');
    %title(['BART (abs) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

end










% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % MOTION
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 
folderName = '~/OneDrive - University College London/Work/JEMRISSimulations/jan2018/spiralFullySampledConstantNTRSmallerPhantom4145Spins/motionOnlyRot/';
% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderName, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end

 
% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints, NTRs]);


% % 3. Reconstruct with BART
% Save all images
imagesBART_mot = zeros(Nx, Ny, NTRs);

figure(115)
for idxTR = 1:NTRs
    tic
    disp(['Image number ', num2str(idxTR)]);
    % Retrieve current values for current reconstruction
    currentKspace_x     = kspace_x(:, idxTR);
    currentKspace_y     = kspace_y(:, idxTR);
    currentKspaceToReco = (kspaces(:, idxTR)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';

                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART_mot(:, :, idxTR) = igrid;
    toc

    % Plot images (ABS)
    if mod(idxTR, 25) == 0 || (idxTR == 1)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) ', num2str(idxTR)])
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;
    end
    
    
    pause(0.01)
    
    clear igrid traj_kspace
end

% % % % Normalize the reconstructed images
[~, imagesBART_mot] = normalizeSignals( ...
                    [ real(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ]);
imagesBART_mot = (reshape(imagesBART_mot, [Nx, Ny, NTRs]));


%% DICTIONARY MATCHING FOR MOTION
imagesBARTReshaped_mot = [ real(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ...
                           imag(reshape(imagesBART_mot, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped_mot);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_mot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB_mot = matTuplesMatchedB1;
resultsB_mot.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                         valuesOfMatchB3, valuesOfMatchB4];
resultsB_mot.scoremap = reshape(resultsB_mot.score, [Nx, Ny]);                      
resultsB_mot.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB_mot.indicesmap = reshape(resultsB_mot.indices, [Nx, Ny]); 
resultsB_mot.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB_mot.T1map = reshape(resultsB_mot.T1, [Nx, Ny]);
resultsB_mot.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB_mot.T2map = reshape(resultsB_mot.T2, [Nx, Ny]);                
   


%% % % % % Reconstructed maps of T1 and T2 and the real (ground truth) maps
figure
% T1 real
subplot(2,3,1)
imagesc(resultsB_nomot.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T1_mrf)])
axis off
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(resultsB_nomot.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T2_mrf)])
axis off
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(resultsB_mot.T1map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T1_mrf)])
axis off
title('T_1 Reconstructed')

% BART - T2
subplot(2,3,5)
imagesc(resultsB_mot.T2map.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(T2_mrf)])
axis off
title('T_2 Reconstructed')

% BART - Score
subplot(2,3,6)   
imagesc(resultsB_mot.scoremap.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0 1])
axis off
title('Matching Scores')

%%
% % % % % Difference maps of T1 and T2 between reconstructed and GT
figure
% BART - T1
subplot(1,2,1)
imagesc((resultsB_mot.T1map-realMap.T1map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_1 Reconstructed - T_1 Ground Truth')
caxis([-500 500])

% BART - T2
subplot(1,2,2)
imagesc((resultsB_mot.T2map-realMap.T2map) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('T_2 Reconstructed - T_2 Ground Truth')
caxis([-150 150])

%%
% % % % % PERCENTAGE ERROR MAPS MOTION
figure
% BART - T1
subplot(1,2,1)
normMatrix = realMap.T1map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_mot.T1map-realMap.T1map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_1)')

% BART - T2
subplot(1,2,2)
normMatrix = realMap.T2map; normMatrix(normMatrix==0) = 1;
imagesc(((abs((resultsB_mot.T2map-realMap.T2map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
colormap hot; colorbar, axis square, axis off
title('PercErr(T_2)')



%% Have a look at individual signals MOTION

imagesBARTReshaped1Ch_mot = reshape(imagesBART_mot, [Nx*Ny, NTRs ]);

% Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(49-1)*Nx+49, ... %big circle %34/49 to the left of centref
             (57-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+27, ... %c1
             (40-1)*Nx+41, ... %c4
             (26-1)*Nx+38];    %c3
                       
scoreValB = resultsB_mot.score(scoreIdxB);
scoreValB_nomot = resultsB_nomot.score(scoreIdxB);
% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4', 'c3'};

% % % % Is this the voxel I was looking for?
figure, 
llPhantom = realMap.T1map.*maskAllCircles;
llPhantom = llPhantom(:);
llPhantom(scoreIdxB(1)) = 0; 
colormap hot; colorbar
llPhantom = reshape(llPhantom, [Nx, Ny]);
imagesc(llPhantom), axis square


for idxTR = 5:7%1:size(scoreIdxB,2)
    
    figure('Position', [50,10,600,800])    
    % BY CHANNELS
    subplot(3,1,1)
    plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsB_mot.indices(scoreIdxB(idxTR)), :), '.--')
    hold on
    plot(1:2*NTRs, imagesBARTReshaped_mot(scoreIdxB(idxTR), :), '.-')
    legend('dictionary', 'image')
    title(['BART (by channels) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

    % DIFFERENCE
    subplot(3,1,2)
    plot(1:2*NTRs, ...
         dictionaryMRFNorm2Ch(resultsB_mot.indices(scoreIdxB(idxTR)), :) - ...
         imagesBARTReshaped_mot(scoreIdxB(idxTR), :), '.-')
    title(['BART (by channels) difference ',  ...
        titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
    
    % ABS
    subplot(3,1,3)
    % dictionary signal it matched to
    plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_mot.indices(scoreIdxB(idxTR)), :)), 'r-')
    hold on
    % voxel signal motion
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_mot(scoreIdxB(idxTR), :)), 'b--')
    % dictionary signal it should have matched to
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_nomot(scoreIdxB(idxTR), :)), 'k-')
    % voxel signal no motion
    plot(1:NTRs, abs(imagesBARTReshaped1Ch_nomot(scoreIdxB(idxTR), :)), 'g--')
    
    %plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_nomot.indices(scoreIdxB(idxTR)), :)), 'g--')
    %plot(1:NTRs, abs(dictionaryMRFNorm(resultsB_nomot.indices(scoreIdxB(idxTR)), :)), 'g--')
    legend('dictionary (matched)', 'phantom (motion)', 'dictionary (matched no motion)', 'phantom (no motion)')
    xlabel('#image'); ylabel('normalised signal intensity (a.u.)');
    %title(['BART (abs) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

end






















% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
%% Have a look at individual signals

imagesBARTReshaped1Ch_nomot = reshape(imagesBART_nomot, [Nx*Ny, NTRs ]);
imagesBARTReshaped1Ch_mot   = reshape(imagesBART_mot, [Nx*Ny, NTRs ]);

% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4'};


% Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(34-1)*Nx+49, ... %big circle
             (57-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+27, ... %c1
             (40-1)*Nx+41];    %c4        


% % % % Is this the voxel I was looking for?
figure, 
llPhantom = realMap.T1map.*maskAllCircles;
llPhantom = llPhantom(:);
llPhantom(scoreIdxB(1)) = 0; 
colormap hot; colorbar
llPhantom = reshape(llPhantom, [Nx, Ny]);
imagesc(llPhantom), axis square





%% % This plots the difference between the two signals of same voxel

imagesBARTReshaped1Ch_motll = [ imagesBARTReshaped1Ch_mot(:,1:104) , ...
                                imagesBARTReshaped1Ch_mot(:,106:NTRs) ];

[~, imagesBARTReshaped1Ch_motll] = normalizeSignals( ...
                     [real(imagesBARTReshaped1Ch_motll), ...
                      imag(imagesBARTReshaped1Ch_motll)] );

for idxTR = 1:4%size(scoreIdxB,2)
    
    
    figure
    
    % % % % Is this the voxel I was looking for?
    subplot(2,2,1)
    llPhantom = squeeze(abs(imagesBART_nomot(:,:,1))).*maskAllCircles;%realMap.T1map
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray

    % % % % Plot abs value of both motion free and motion corrupted data
    subplot(2,2,[3,4])
    % Motion corruputed signal
    plot(1:NTRs-1, abs(imagesBARTReshaped1Ch_motll(resultsB_mot.indices(scoreIdxB(idxTR)), :)), '.-', ...
         'LineWidth', 1.1)
    hold on
    % Motion free signal
    plot(1:NTRs-1, [ abs(imagesBARTReshaped1Ch_nomot(resultsB_nomot.indices(scoreIdxB(idxTR)),1:67)), ...
                     abs(imagesBARTReshaped1Ch_nomot(resultsB_nomot.indices(scoreIdxB(idxTR)),69:NTRs)) ], '.-', ...
                     'LineWidth', 1.1)
                 
    legend('motion', 'no motion')
    title(['Looking at: ',  titles{idxTR}])
    xlabel('TR index'); ylabel('Signal (a.u.)');
end













