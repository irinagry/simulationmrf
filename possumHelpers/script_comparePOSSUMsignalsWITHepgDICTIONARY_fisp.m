% % % % IRINA GRIGORESCU
% % % % Comparing JEMRIS bssfp signals with EPG dictionary
% % % % 

% % % % Observation:
% 
% Have a look at script_comparePOSSUMsignalsWITHepgDICTIONARY_bssfp.m
% That is where code is nice, but this one doesn't work so don't use this
% script but redo it when you make POSSUM - FISP work
% 
% This is not working... Why?
% 1. JEMRIS uses multiple spins (?)
% 2. I am not using the correct gradient amplitude - i am - it accumulates
% 2*pi per each TR
% 3. Use same sequence in POSSUM as in JEMRIS - Did that too

% % % % Preprocessing
% Addpaths for EPG comparison
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/extendedPhaseGraph/'))
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/helpers/'))
addpath(genpath('~/OneDrive - University College London/Work/Simulation/simulationMRF/src/algs/'))
addpath(genpath('~/possumdevirina/'))


%% Calculate area of dephasing gradient
dphi  = 2*pi-0.006283185307180; % rad
gamma = 2*pi * 42.58 * 1e06; % rad * Hz/T
dz    = 1 * 1e-03;           % m
Area = dphi / (gamma * dz);  % T/m * s

durationGrad = 1.5 * 1e-03;  % Duration is 0.002 s 
                             % but is trapezoid which makes it 1.5ms
AmplitGrad   = Area / durationGrad;      % T/m

% In POSSUM I am using a trapezoid gradient

%% Generate EPG signal
% Materials:
T1 = 600; T2 = 80;
% Sequence:
N = 20;
PA = zeros(N,1);
FA = zeros(N,1) + 40; % + (0:1:N-1).';
TR = zeros(N,1) + 15;
TE = zeros(N,1) +  5; %4.99785744;

% Run EPG
[ChiF, ChiZ] = epgMatrix(N, PA, FA, TR, T1, T2, 1, 1); % M0, flagDephase

F0states = ChiF(N, :) .* exp(-TE ./ T2).';

sigEPGnotNorm = [real(F0states) imag(F0states)];
[~, sigEPGNorm] = normalizeSignals(sigEPGnotNorm);
% sigEPG = abs(sigEPGNorm);
sigEPG = abs(F0states);

%% Get POSSUM signal
folderName = '~/simdir/outSignal/';
fileName   = 'signal_epif_WM_64x64_Np20_';
APOSSUM = read_pulse([folderName, fileName, num2str(1)]);

sigPOSSUMnotNorm = [APOSSUM(1,1:N) APOSSUM(2,1:N)];
[~, sigPOSSUMNorm] = normalizeSignals(sigPOSSUMnotNorm);
% sigPOSSUM = abs(sigPOSSUMNorm);
nvox = 1;
sigPOSSUM = abs(APOSSUM(1,1:N) + 1i.*(APOSSUM(2,1:N)./nvox))./nvox;


%% Plot against each other
figure
subplot(2,2,[1,2])
plot(1:N, sigEPG,    'ko-'); hold on
plot(1:N, sigPOSSUM, 'b.-');
xlabel('timepoints'); ylabel('signal');
legend('EPG', 'POSSUM');
title({'EPG vs POSSUM', 'dephasing accross voxel', 'r0x = 0'});

subplot(2,2,3)
plot([0,1],[0,1],'k'); hold on   % identity line
plot(sigEPG, sigPOSSUM, 'bo'); 
xlabel('signal EPG'); ylabel('signal POSSUM');
axis square

subplot(2,2,4)
plot(1:N, sigPOSSUM-sigEPG, 'b.-');
xlabel('timepoints'); ylabel('POSSUM - EPG'); 
axis square




%%
figure
subplot(3,1,1)
plot(1:N, sigPOSSUM, 'k'), hold on
plot(1:N, sigJEMRIS, 'r')
legend('POSSUM','JEMRIS')

subplot(3,1,2)
plot(1:N, APOSSUM(1,1:N)./nvox, 'k-'), hold on
plot(1:N, APOSSUM(2,1:N)./nvox, 'g-')
plot(1:N, AJEMRIS(1:N,1), 'r--')
plot(1:N, AJEMRIS(1:N,2), 'b--')
legend('P_x','P_y','J_x','J_y')

subplot(3,1,3)
plot(1:N, sigPOSSUM-sigJEMRIS, 'k-'), hold on
title('Signal_{POSSUM} - Signal_{JEMRIS}')
