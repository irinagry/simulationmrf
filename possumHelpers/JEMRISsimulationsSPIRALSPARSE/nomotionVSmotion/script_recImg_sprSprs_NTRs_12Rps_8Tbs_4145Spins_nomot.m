% 
% Irina Grigorescu
% Date created: 04-02-2018
% 
% Script to reconstruct images taken from different repetition times of 
% a sparsely sampled spiral acquisition
% 
% Reconstruct from same TR index, but different repetition number
% 
%   NO MOTION CASE - for the 4145 spins 

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% % % % % PROPERTIES OF THE SEQUENCE
% Set number of TR blocks
NTRs  = 1; 
% Set number of repetition times
NReps = 12; 
% Set effective image size
Nx   = 96; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 600; 

%% % % % % K-SPACE TRAJECTORY TAKEN FROM SIMULATION (JEMRIS)
% Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/jan2018/spiralSparselySampledNTRSmallerPhantom4145Spins/nomotion/';

% Allocate memory for k-space coordinates in x,y,z directions
% NROpoints x NTRs x Nrepetitions 
kspace_x = zeros(nROPoints      , NTRs, NReps); 
kspace_y = zeros(nROPoints      , NTRs, NReps);
kspace_z = zeros(nROPoints*NReps, 1);

% For each repetition time
for idxRep = 1:NReps
    % Sequence name:
    seqName = [folderName, 'seq', num2str(idxRep-1), '.h5'];
    
    % Get sequence values
    RXP = h5read(seqName, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
    KX  = h5read(seqName, '/seqdiag/KX');   % KX (rad/mm)
    KY  = h5read(seqName, '/seqdiag/KY');   % KY (rad/mm)
    KZ  = h5read(seqName, '/seqdiag/KZ');   % KZ (rad/mm)
    % Get k-space trajectory 
    KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

    % Plot them for each spiral turn
    figure(111)
    for idxTR = 1:NTRs
        % Populate the spiral k-spaces
        kspace_x(:,idxTR,idxRep) = ...
                         KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints);
        kspace_y(:,idxTR,idxRep) = ...
                         KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints);

        if idxTR == 1
            % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
            subplot(2,2,1); hold on
            xl = 3;
            % Coordinates from JEMRIS
            scatter(kspace_x(:,idxTR,idxRep), ...
                    kspace_y(:,idxTR,idxRep), 'k.');
            axis equal, axis square;
            xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
            title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
            xlim([-xl xl]); ylim([-xl xl]);
        end

            % % % % % % Scale the coordinates onto the grid
            % % % Transform the kspace coordinates to the desired 
            % % % effective imaging matrix
            % % % by scaling the values so that they fit into the matrix
            % Calculate euclidean distance between first and last spiral
            % elements to scale this dimension
            dist_ksp = sqrt( (  kspace_x(1  ,idxTR,idxRep) - ...
                                kspace_x(end,idxTR,idxRep)   ...
                             ).^2 + ... 
                             (  kspace_y(1  ,idxTR,idxRep) - ...
                                kspace_y(end,idxTR,idxRep)   ...
                             ).^2 ) ;
            % The first coordinate of the spiral is not exactly (0,0)
            % Memorize it so that you can shift the spiral to (0,0) before
            % scaling
            x0_ksp = kspace_x(1, idxTR, idxRep); 
            y0_ksp = kspace_y(1, idxTR, idxRep);
            % The spiral trajectory that you need to scale shifted to (0,0)
            traj_spiral = [ squeeze(kspace_x(:,idxTR,idxRep)) - x0_ksp, ...
                            squeeze(kspace_y(:,idxTR,idxRep)) - y0_ksp, ...
                            kspace_z(1:nROPoints)       ].';
            % Scale it using bart
            traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], ...
                                           traj_spiral);
            % Store the scaled version of the trajectory
            kspace_x(:, idxTR, idxRep) = traj_spiral2(1,:) + x0_ksp;
            kspace_y(:, idxTR, idxRep) = traj_spiral2(2,:) + y0_ksp;

        if idxTR == 1
            % % % % % % Plot the coordinates after scaling
            subplot(2,2,2); hold on
            scatter(kspace_x(:, idxTR, idxRep), ...
                    kspace_y(:, idxTR, idxRep), 'k.');
            axis equal, axis square;
            xlabel('N_x'); ylabel('N_y');
            title(['Spiral TR ', num2str(idxTR), ' of REP ', num2str(idxRep)])
            xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

            % Drawnow or pause
            drawnow
        end
    
    end
    
    drawnow
    
end


%% % % % % SIGNAL VALUES TAKEN FROM SIMULATION (JEMRIS)
% Allocate memory for kspace values (NROpoints x NTRs x Nrepetitions)
signalValues = zeros(nROPoints, NTRs, NReps); 

% For each repetition of the spiral
for idxRep = 1:NReps 
    % % % % % % Get signal values from JEMRIS simulation
    filenameSignal = [folderName, 'signals', num2str(idxRep-1), '.h5']; 

    % % % Read in the vector values Mx,My,Mz
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Read in the timings
    t =  h5read(filenameSignal, sprintf('/signal/times'));
    % % % Sort timings
    [t, J] = sort(t); 
    % % % Store the signal
    Mvecs  = A(J,:);

    % % % Get kspace as one signal
    for idxTR = 1:NTRs
        % Get signal values
        signalValues(:,idxTR,idxRep) = ...
                   Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
        sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
    end
end


% % % % % % Reconstruct with BART
% Save all images (Nx x Ny x NTRs x NReps
NRecs = 1 ; % number of reconstructions I want from all the repetitions
imagesBART = zeros(Nx, Ny, NTRs);

figure(115)
% For each TR
for idxTR = 1:NTRs
    % Start with nothing
    currentKspace_x = [];
    currentKspace_y = [];
    currentSignalToReco = [];
    
    % For each repetition gather all data from all repetitions
    for idxRep = 1:NReps
        % Populate current kspace trajectory and signal with the values
        % taken from the reconstruction
        currentKspace_x     = [ currentKspace_x ; ...
                                squeeze(kspace_x(:,idxTR,idxRep))];
        currentKspace_y     = [ currentKspace_y ; ...
                                squeeze(kspace_y(:,idxTR,idxRep))];
        currentSignalToReco = [ currentSignalToReco ; ...
                                (squeeze(signalValues(:,idxTR,idxRep)))];
    end
    
    % % % 
    tic
    disp(['Image TR ', num2str(idxTR), ' REP ', num2str(idxRep)]);
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].'; %(1:nROPoints)


    % Reconstruct with BART
    igrid = bart( 'nufft -i -t', traj_kspace, currentSignalToReco.');
    igrid = bart(['resize -c 0 ', num2str(Nx), ...
                           ' 1 ', num2str(Ny)], igrid);
    imagesBART(:, :, idxTR) = igrid;
    toc

    % Plot images
    if (mod(idxTR,10) == 0) || (idxTR == 1)
        % Plot images (ABS)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) TR ', num2str(idxTR)]);
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;

        pause(0.01)

    end
    
    clear igrid traj_kspace
end

% % % % Normalize the reconstructed images
[~, imagesBART] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBART = (reshape(imagesBART, [Nx, Ny, NTRs]));


return
%% Simulate dictionary
% % % % % % % % % % % Generate dictionary with old ALGORITHM
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Sequence
% Maryia's FAs
FA_mrf = [5.7014 6.3232 6.9954 7.7159 8.4826 9.2931 10.145 ...
          11.036 11.962 12.921 13.91 14.924 15.961 17.016 ...
          18.084 19.162 20.244 21.326 22.402 23.467 24.516 ...
          25.544 26.545 27.514 28.445 29.335 30.177 30.968 ...
          31.704 32.38 32.994 33.541 34.021 34.431 34.77 ...
          35.036 35.23 35.352 35.403 35.383 35.294 35.139 ...
          34.92 34.64 34.3 33.906 33.459 32.964 32.423 31.84 ...
          31.219 30.563 29.874 29.156 28.412 27.644 26.855 ...
          26.048 25.224 24.385 23.533 22.672 21.801 20.924 ...
          20.041 19.157 18.271 17.388 16.509 15.637 14.776 ...
          13.927 13.094 12.281 11.49 10.725 9.9886 9.2839 ...
          8.6137 7.9806 7.3868 6.8344 6.325 5.8597 5.4394 5.0645 4.735 4.4506 4.2102 4.0127 3.8563 3.7391 3.6585 3.612 3.5963 3.6083 3.6444 3.701 3.7742 3.8602 3.955 4.0547 4.1554 4.2533 4.3448 4.4264 4.4949 4.5473 4.5811 4.5938 4.5836 4.549 4.489 4.403 4.2909 4.1531 3.9906 3.8048 3.5974 3.371 3.1281 2.8718 2.6057 2.3333 2.0586 1.7855 1.5181 1.2605 1.0168 0.791 0.5868 0.40785 0.25752 0.13892 0.054855 0.0078284 0 0.033191 0.10887 0.22817 0.39185 0.60034 0.85374 1.1518 1.494 1.8794 2.307 2.7753 3.2828 3.8274 4.4073 5.0202 5.6636 6.3352 7.0323 7.7522 8.4923 9.2498 10.022 10.806 11.599 12.399 13.203 14.009 14.814 15.617 16.415 17.207 17.991 18.766 19.531 20.286 21.031 21.765 22.489 23.204 23.911 24.611 25.306 25.999 26.692 27.386 28.084 28.789 29.503 30.229 30.967 31.72 32.489 33.276 34.08 34.902 35.741 36.597 37.468 38.353 39.25 40.156 41.07 41.988 42.907 43.825 44.739 45.646 46.543 47.427 48.294 49.144 49.972 50.777 51.557 52.308 53.03 53.719 54.375 54.996 55.58 56.126 56.634 57.102 57.531 57.919 58.268 58.578 58.85 59.085 59.285 59.452 59.588 59.696 59.779 59.839 59.879 59.904 59.915 59.916 59.909 59.897 59.883 59.868 59.854 59.843 59.835 59.831 59.831 59.836 59.844 59.856 59.871 59.889 59.907 59.925 59.943 59.96 59.974 59.985 59.994 59.999 60 59.997 59.991 59.981 59.968 59.952 59.933 59.911 59.886 59.859 59.829 59.795 59.758 59.716 59.669 59.614 59.552 59.48 59.397 59.301 59.19 59.062 58.917 58.751 58.563 58.353 58.118 57.858 57.573 57.261 56.923 56.559 56.169 55.755 55.318 54.86 54.381 53.885 53.373 52.849 52.315 51.773 51.226 50.678 50.132 49.589 49.053 48.526 48.01 47.507 47.018 46.546 46.09 45.65 45.228 44.822 44.431 44.054 43.69 43.336 42.99 42.65 42.312 41.974 41.633 41.286 40.93 40.562 40.179 39.78 39.361 38.922 38.46 37.975 37.466 36.933 36.376 35.796 35.193 34.571 33.93 33.273 32.604 31.925 31.241 30.554 29.869 29.19 28.522 27.868 27.234 26.622 26.037 25.482 24.961 24.476 24.03 23.625 23.262 22.943 22.667 22.434 22.244 22.096 21.987 21.917 21.883 21.882 21.912 21.969 22.052 22.156 22.28 22.42 22.574 22.74 22.915 23.098 23.288 23.483 23.681 23.884 24.089 24.297 24.507 24.72 24.935 25.153 25.374 25.597 25.823 26.051 26.282 26.516 26.751 26.988 27.227 27.466 27.705 27.943 28.18 28.414 28.645 28.872 29.093 29.307 29.514 29.712 29.9 30.077 30.243 30.395 30.533 30.657 30.766 30.859 30.935 30.996 31.04 31.067 31.078 31.073 31.052 31.016 30.964 30.896 30.814 30.717 30.605 30.478 30.336 30.179 30.007 29.819 29.616 29.396 29.16 28.908 28.639 28.354 28.052 27.735 27.401 27.052 26.688 26.31 25.918 25.513 25.096 24.667 24.227 23.776 23.315 22.844 22.363 21.872 21.372 20.861 20.341 19.809 19.267 18.712 18.146 17.568 16.977 16.373 15.757 15.128 14.487 13.836 13.175 12.506 11.831 11.152 10.472 9.7952 9.1241 8.4629 7.8158 7.1872 6.5815 6.0035 5.4576 4.9484 4.4801 4.0567 3.6821 3.3596 3.0921 2.8819 2.7313 2.6415 2.6137 2.6485 2.746 2.906 3.1281 3.4113 3.7546 4.1568 4.6164 5.1319].';
FA_mrf = FA_mrf(1:NTRs);

PhA_mrf =  zeros(NTRs, 1) ;
TR_mrf  =  zeros(NTRs, 1) + 15;
RO_mrf  =  zeros(NTRs, 1) +  6;

% % % % Custom Material
T1_mrf = [500, 600, 950, 1000, 1200, 3000, 4000, 4500];
T2_mrf = [ 70,  80, 100,  110,  200, 1000, 2000, 2200];
df_mrf =   0;

% % % % Generate dictionary
[ ~, ...                     % material properties
  sequenceProperties, ...    % sequence properties
  materialTuples, ...        % material tuples
  M, ...                     % number of material tuples
  ~, ...                     % dictionary MxNx3
  dictionaryMRFComplex, ...  % 
  dictionaryMRFNorm, ...     % dictionary normalised MxN
  dictionaryMRFNorm2Ch, ...  % dictionary normalised Mx2N
  signalDictionaryMRF] = ... % dictionary normalised MxN and absolute val
    func_generateCustomSignals(NTRs, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG, 1);

%% Dictionary matching
imagesBARTReshaped = [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                       imag(reshape(imagesBART, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB = matTuplesMatchedB1;
resultsB.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                            valuesOfMatchB3, valuesOfMatchB4];
resultsB.scoremap = reshape(resultsB.score, [Nx, Ny]);                      
resultsB.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB.indicesmap = reshape(resultsB.indices, [Nx, Ny]); 
resultsB.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB.T1map = reshape(resultsB.T1, [Nx, Ny]);
resultsB.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB.T2map = reshape(resultsB.T2, [Nx, Ny]);                
   

%% Create MAPS
% % By using <<createCirclesMask>>
cr = 2;
maskAllCircles = createCirclesMask(zeros([Nx, Ny]), ...
                                   [12, 22; 22 22; 22 12], [cr, cr, cr]);
maskWM  = createCirclesMask(zeros([Nx, Ny]), [22, 12], cr);
maskGM  = createCirclesMask(zeros([Nx, Ny]), [22, 22], cr);
maskCSF = createCirclesMask(zeros([Nx, Ny]), [12, 22], cr);

% % % % How to overlay with transparency:
% % figure
% % imagesc(squeeze(imag(imagesBART(:,:,50)))); 
% % hold on; 
% % imagesc(maskAllCircles,'AlphaData', .1); colormap gray

% % % % By manually choosing the mask
% % maskWM = zeros(Nx, Ny); maskGM = zeros(Nx, Ny); maskCSF = zeros(Nx, Ny);
% % maskWM(9:13, 20:24)  = 1; % WM
% % maskGM(20:24, 20:24)  = 1; % GM
% % maskCSF(20:24, 9:13) = 1; % CSF
% % maskAllCircles = maskWM + maskGM + maskCSF;

% % % % % Ground Truth maps of T1 and T2
realMap.T1map = ones(Nx,Ny) .* maskWM  .*  600 + ... 
                ones(Nx,Ny) .* maskGM  .*  950 + ...
                ones(Nx,Ny) .* maskCSF .* 4500;
realMap.T2map = ones(Nx,Ny) .* maskWM  .*   80 + ... 
                ones(Nx,Ny) .* maskGM  .*  100 + ...
                ones(Nx,Ny) .* maskCSF .* 2200;

% % % % % Reconstructed maps of T1 and T2 and the real (ground truth) maps
figure
% T1 real
subplot(2,3,1)
imagesc(realMap.T1map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([0, max(T1_mrf)])
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(realMap.T2map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([0, max(T2_mrf)])
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(resultsB.T1map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([0, max(T1_mrf)])
title('T_1 BART')

% BART - T2
subplot(2,3,5)
imagesc(resultsB.T2map.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([0, max(T2_mrf)])
title('T_2 BART')

% BART - Score
subplot(2,3,6)   
imagesc(resultsB.scoremap.*maskAllCircles)
colormap hot; colorbar, axis square
caxis([0 1])
title('Score BART')

% % % % % Difference maps of T1 and T2 between reconstructed and GT
figure
% BART - T1
subplot(1,2,1)
imagesc((realMap.T1map - resultsB.T1map) .* maskAllCircles)
colormap hot; colorbar, axis square
title('T_1 Ground Truth - T_1 BART')

% BART - T2
subplot(1,2,2)
imagesc((realMap.T2map - resultsB.T2map) .* maskAllCircles )
colormap hot; colorbar, axis square
title('T_2 Ground Truth - T_2 BART')

% % % % % Difference maps of T1 and T2 between reconstructed and GT
figure
% BART - T1
subplot(1,2,1)
normMatrix = realMap.T1map; normMatrix(normMatrix==0) = 1;
imagesc(((realMap.T1map - resultsB.T1map)./normMatrix.*100) .* maskAllCircles)
colormap hot; colorbar, axis square
title('PercErr(T_1)')

% BART - T2
subplot(1,2,2)
normMatrix = realMap.T2map; normMatrix(normMatrix==0) = 1;
imagesc(((realMap.T2map - resultsB.T2map)./normMatrix.*100) .* maskAllCircles )
colormap hot; colorbar, axis square
title('PercErr(T_2)')

%% Have a look at individual signals
% Bart
[maxScoreValB, maxScoreIdxB] = max(resultsB.score);
[minScoreValB, minScoreIdxB] = min(resultsB.score);
rndScoreIdxB = (12-1)*Nx+21; %(12-1)*Nx+23;
rndScoreValB = resultsB.score(rndScoreIdxB);

scoreIdxB = [maxScoreIdxB, minScoreIdxB, rndScoreIdxB];
scoreValB = [maxScoreValB, minScoreValB, rndScoreValB];

% Titles
titles = {'Best', 'Worst', 'Random'};

for idxTR = 1:3
    
    figure
    
    % % BART
    % ABS
%     subplot(2,1,1)
%     plot(1:NTRs, abs(dictionaryMRFNorm2Ch(resultsB.indices(scoreIdxB(idxTR)), 1:NTRs) + ...
%                  1i.*dictionaryMRFNorm2Ch(resultsB.indices(scoreIdxB(idxTR)), NTRs+1:end)), 'o-')
%     hold on
%     plot(1:NTRs, abs(imagesBARTReshaped(scoreIdxB(idxTR), 1:NTRs) + ...
%                  1i.*imagesBARTReshaped(scoreIdxB(idxTR), NTRs+1:end)), '.-')
%     legend('dictionary', 'image')
%     title(['BART (abs) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
    
    % BY CHANNELS
    subplot(2,1,1)
    plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsB.indices(scoreIdxB(idxTR)), :), 'o-')
    hold on
    plot(1:2*NTRs, imagesBARTReshaped(scoreIdxB(idxTR), :), '.-')
    legend('dictionary', 'image')
    title(['BART (by channels) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])

    % DIFFERENCE BY CHANNELS
    subplot(2,1,2)
    plot(1:2*NTRs, ...
        dictionaryMRFNorm2Ch(resultsB.indices(scoreIdxB(idxTR)), :) - ...
        imagesBARTReshaped(scoreIdxB(idxTR), :), '.-')
    ylabel('dictionary signal - reconstructed signal');
    title(['BART (by channels) difference ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
end


%% Compare between 2 similar dictionary signals
idx1 = 7; %13; %51; %13; %51;
idx2 = 6; %19; %50; %18; %43;

figure
subplot(2,1,1)
plot(1:2*NTRs, dictionaryMRFNorm2Ch(idx1, :), 'o-b')
hold on
plot(1:2*NTRs, dictionaryMRFNorm2Ch(idx2, :), 'o-r')
% legend('t1=4500,t2=2200', 't1=4000,t2=2200');
% legend('t1=950,t2=100', 't1=1000,t2=100');
% legend('t1=4500,t2=2200', 't1=4500,t2=2000');
% legend('t1=950,t2=100', 't1=1000,t2=110');
legend('t1=600,t2=80', 't1=600,t2=70');

subplot(2,1,2)
plot(1:2*NTRs, dictionaryMRFNorm2Ch(idx1, :) - ...
               dictionaryMRFNorm2Ch(idx2, :), 'o-r')
           


return
%%
% % % % % % % % % % % %
% % % % % % % % % % % %
% % % % % % % % % % % % Compare with fully sampled
% % % % % % % % % % % %
% % % % % % % % % % % %
% % Get fully sampled spiral simulation
nROPointsFS = 6000;

% 1. Get k-space trajectory
% Allocate memory for k-space coordinates in x,y,z directions
% NROpoints x NTRs x Nrepetitions 
kspace_x_FS = zeros(nROPointsFS, 1); 
kspace_y_FS = zeros(nROPointsFS, 1);
kspace_z_FS = zeros(nROPointsFS, 1);

% Sequence name:
seqName = [folderName, 'seqFS.h5'];

% Get sequence values
RXP = h5read(seqName, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read(seqName, '/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(seqName, '/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(seqName, '/seqdiag/KZ');   % KZ (rad/mm)
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Plot them for each spiral turn
figure(111)
subplot(2,2,3); 
xl = 3;
% Coordinates from JEMRIS
scatter(KX, KY, '.');
axis equal, axis square;
xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
title('Fully Sampled Spiral')
xlim([-xl xl]); ylim([-xl xl]);

% % % % % % Scale the coordinates onto the grid
% % % Transform the kspace coordinates to the desired 
% % % effective imaging matrix
% % % by scaling the values so that they fit into the matrix
% Calculate euclidean distance between first and last spiral
% elements to scale this dimension
dist_ksp = sqrt( ( KX(1) - KX(end) ).^2 + ... 
                 ( KY(1) - KY(end) ).^2 );
% The first coordinate of the spiral is not exactly (0,0)
% Memorize it so that you can shift the spiral to (0,0) before
% scaling
x0_ksp = KX(1); y0_ksp = KY(1);
% The spiral trajectory that you need to scale shifted to (0,0)
traj_spiral = [ KX - x0_ksp, ...
                KY - y0_ksp, ...
                KZ       ].';
% Scale it using bart
traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], ...
                               traj_spiral);
% Store the scaled version of the trajectory
KX = traj_spiral2(1,:) + x0_ksp;
KY = traj_spiral2(2,:) + y0_ksp;

% % % % % % Plot the coordinates after scaling
subplot(2,2,4); hold on
scatter(KX, KY, '.');
axis equal, axis square;
xlabel('N_x'); ylabel('N_y');
title('Fully Sampled Spiral Scaled')
xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

% Drawnow or pause
drawnow

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% 2. Get signal values from fully sampled simulation

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderName, 'signalsFS.h5']; 

% % % Read in the vector values Mx,My,Mz
A = (h5read(filenameSignal, '/signal/channels/00'))';
% % % Read in the timings
t =  h5read(filenameSignal, sprintf('/signal/times'));
% % % Sort timings
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);

% % % Get kspace as one signal
signalValuesFS = Mvecs(:, 1) + sqrt(-1) * Mvecs(:, 2);


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% 3. Reconstruct with BART

tic
% Prepare trajectory for current image
traj_kspace = [ KX ; KY ; KZ.'] ;

% Reconstruct with BART
igrid = bart( 'nufft -i -t', traj_kspace, signalValuesFS.');
igrid = bart(['resize -c 0 ', num2str(Nx), ...
                       ' 1 ', num2str(Ny)], igrid);
toc

% Plot images (ABS)
figure(115)
subplot(3,3,4), imagesc(abs(igrid))
title('BART FS (abs)')
axis square; axis off;
colormap gray;
colorbar;

% REAL
subplot(3,3,5), imagesc(real(igrid))
title('BART FS (real) ');
axis square; axis off;
colormap gray;
colorbar;

% IMAG
subplot(3,3,6), imagesc(imag(igrid))
title('BART FS (imag)')
axis square; axis off;
colormap gray;
colorbar;

% % % DIFFERENCES between fully sampled and sparsely sampled
% reconstructions
subplot(3,3,7), imagesc(abs(igrid-imagesBART))
title('BART FS - SS (abs)')
axis square; axis off;
colormap gray;
colorbar;

% REAL
subplot(3,3,8), imagesc(real(igrid-imagesBART))
title('BART FS - SS (real) ');
axis square; axis off;
colormap gray;
colorbar;

% IMAG
subplot(3,3,9), imagesc(imag(igrid-imagesBART))
title('BART FS - SS (imag)')
axis square; axis off;
colormap gray;
colorbar;




