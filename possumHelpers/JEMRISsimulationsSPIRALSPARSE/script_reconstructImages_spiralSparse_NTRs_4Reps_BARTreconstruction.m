% 
% Irina Grigorescu
% Date created: 22-01-2018
% 
% Script to reconstruct images taken from different repetition times of 
% a sparsely sampled spiral acquisition
% 
% Reconstruct from same TR index, but different repetition number
% 

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
%% % % % % PROPERTIES OF THE SEQUENCE
% Set number of TR blocks
NTRs  = 1; 
% Set number of repetition times
NReps = 4; 
% Set effective image size
Nx   = 128; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 1500; %6000;

%% % % % % K-SPACE TRAJECTORY TAKEN FROM SIMULATION (JEMRIS)
% Get k-space coordinates from JEMRIS simulation
% folderName = '~/jemrisSims/jan2018/spiralSparselySampled1TR/';
folderName = '~/jemrisSims/jan2018/spiralSparselySampled1TRMediumPhantom/';

% Allocate memory for k-space coordinates in x,y,z directions
% NROpoints x NTRs x Nrepetitions 
kspace_x = zeros(nROPoints      , NTRs, NReps); 
kspace_y = zeros(nROPoints      , NTRs, NReps);
kspace_z = zeros(nROPoints*NReps, 1);

% For each repetition time
for idxRep = 1:NReps
    % Sequence name:
    seqName = [folderName, 'seq', num2str(idxRep-1), '.h5'];
    
    % Get sequence values
    RXP = h5read(seqName, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
    KX  = h5read(seqName, '/seqdiag/KX');   % KX (rad/mm)
    KY  = h5read(seqName, '/seqdiag/KY');   % KY (rad/mm)
    KZ  = h5read(seqName, '/seqdiag/KZ');   % KZ (rad/mm)
    % Get k-space trajectory 
    KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

    % Plot them for each spiral turn
    figure(111)
    for idxTR = 1:NTRs
        % Populate the spiral k-spaces
        kspace_x(:,idxTR,idxRep) = ...
                         KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints);
        kspace_y(:,idxTR,idxRep) = ...
                         KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints);

        % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
        subplot(2,2,1); hold on
        xl = 3;
        % Coordinates from JEMRIS
        scatter(kspace_x(:,idxTR,idxRep), ...
                kspace_y(:,idxTR,idxRep), '.');
        axis equal, axis square;
        xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
        title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
        xlim([-xl xl]); ylim([-xl xl]);
        
        % % % % % % Scale the coordinates onto the grid
        % % % Transform the kspace coordinates to the desired 
        % % % effective imaging matrix
        % % % by scaling the values so that they fit into the matrix
        % Calculate euclidean distance between first and last spiral
        % elements to scale this dimension
        dist_ksp = sqrt( (  kspace_x(1  ,idxTR,idxRep) - ...
                            kspace_x(end,idxTR,idxRep)   ...
                         ).^2 + ... 
                         (  kspace_y(1  ,idxTR,idxRep) - ...
                            kspace_y(end,idxTR,idxRep)   ...
                         ).^2 ) ;
        % The first coordinate of the spiral is not exactly (0,0)
        % Memorize it so that you can shift the spiral to (0,0) before
        % scaling
        x0_ksp = kspace_x(1, idxTR, idxRep); 
        y0_ksp = kspace_y(1, idxTR, idxRep);
        % The spiral trajectory that you need to scale shifted to (0,0)
        traj_spiral = [ squeeze(kspace_x(:,idxTR,idxRep)) - x0_ksp, ...
                        squeeze(kspace_y(:,idxTR,idxRep)) - y0_ksp, ...
                        kspace_z(1:nROPoints)       ].';
        % Scale it using bart
        traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], ...
                                       traj_spiral);
        % Store the scaled version of the trajectory
        kspace_x(:, idxTR, idxRep) = traj_spiral2(1,:) + x0_ksp;
        kspace_y(:, idxTR, idxRep) = traj_spiral2(2,:) + y0_ksp;
        
        % % % % % % Plot the coordinates after scaling
        subplot(2,2,2); hold on
        scatter(kspace_x(:, idxTR, idxRep), ...
                kspace_y(:, idxTR, idxRep), '.');
        axis equal, axis square;
        xlabel('N_x'); ylabel('N_y');
        title(['Spiral TR ', num2str(idxTR), ' of REP ', num2str(idxRep)])
        xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

        % Drawnow or pause
        drawnow
    
    end
    
    pause
    
end


%% % % % % SIGNAL VALUES TAKEN FROM SIMULATION (JEMRIS)
% Allocate memory for kspace values (NROpoints x NTRs x Nrepetitions)
signalValues = zeros(nROPoints, NTRs, NReps); 

% For each repetition of the spiral
for idxRep = 1:NReps 
    % % % % % % Get signal values from JEMRIS simulation
    filenameSignal = [folderName, 'signals', num2str(idxRep-1), '.h5']; 

    % % % Read in the vector values Mx,My,Mz
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    % % % Read in the timings
    t =  h5read(filenameSignal, sprintf('/signal/times'));
    % % % Sort timings
    [t, J] = sort(t); 
    % % % Store the signal
    Mvecs  = A(J,:);

    % % % Get kspace as one signal
    for idxTR = 1:NTRs
        % Get signal values
        signalValues(:,idxTR,idxRep) = ...
                   Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
        sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
    end
end


%% % % % % Reconstruct with BART
% Save all images (Nx x Ny x NTRs x NReps
NRecs = 1 ; % number of reconstructions I want from all the repetitions
imagesBART = zeros(Nx, Ny, NTRs);

figure(115)
% For each TR
for idxTR = 1:NTRs
    % Start with nothing
    currentKspace_x = [];
    currentKspace_y = [];
    currentSignalToReco = [];
    
    % For each repetition gather all data from all repetitions
    for idxRep = 1:NReps
        % Populate current kspace trajectory and signal with the values
        % taken from the reconstruction
        currentKspace_x     = [ currentKspace_x ; ...
                                squeeze(kspace_x(:,idxTR,idxRep))];
        currentKspace_y     = [ currentKspace_y ; ...
                                squeeze(kspace_y(:,idxTR,idxRep))];
        currentSignalToReco = [ currentSignalToReco ; ...
                                (squeeze(signalValues(:,idxTR,idxRep)))];
    end
    
    % % % 
    tic
    disp(['Image TR ', num2str(idxTR), ' REP ', num2str(idxRep)]);
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].'; %(1:nROPoints)


    % Reconstruct with BART
    igrid = bart( 'nufft -i -t', traj_kspace, currentSignalToReco.');
    igrid = bart(['resize -c 0 ', num2str(Nx), ...
                           ' 1 ', num2str(Ny)], igrid);
    imagesBART(:, :, idxTR) = igrid;
    toc

    % Plot images (ABS)
    subplot(3,3,1), imagesc(abs(igrid))
    title('BART reconstruction (abs)')
    axis square; axis off;
    colormap gray;
    colorbar;

    subplot(3,3,2), imagesc(real(igrid))
    title(['BART reconstruction (real) TR ', num2str(idxTR)]);
    axis square; axis off;
    colormap gray;
    colorbar;

    subplot(3,3,3), imagesc(imag(igrid))
    title('BART reconstruction (imag)')
    axis square; axis off;
    colormap gray;
    colorbar;

    pause

    if idxTR ~=NTRs
        delete(x1); delete(x2);
    end

    clear igrid traj_kspace

end

% % % % % Normalize the reconstructed images
% [~, imagesBART] = normalizeSignals( ...
%                     [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
%                       imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
% imagesBART = (reshape(imagesBART, [Nx, Ny, NTRs]));



%% Get fully sampled spiral simulation
nROPointsFS = 6000;

% 1. Get k-space trajectory
% Allocate memory for k-space coordinates in x,y,z directions
% NROpoints x NTRs x Nrepetitions 
kspace_x_FS = zeros(nROPointsFS, 1); 
kspace_y_FS = zeros(nROPointsFS, 1);
kspace_z_FS = zeros(nROPointsFS, 1);

% Sequence name:
seqName = [folderName, 'seqFS.h5'];

% Get sequence values
RXP = h5read(seqName, '/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read(seqName, '/seqdiag/KX');   % KX (rad/mm)
KY  = h5read(seqName, '/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read(seqName, '/seqdiag/KZ');   % KZ (rad/mm)
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Plot them for each spiral turn
figure(111)
subplot(2,2,3); 
xl = 3;
% Coordinates from JEMRIS
scatter(KX, KY, '.');
axis equal, axis square;
xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
title('Fully Sampled Spiral')
xlim([-xl xl]); ylim([-xl xl]);

% % % % % % Scale the coordinates onto the grid
% % % Transform the kspace coordinates to the desired 
% % % effective imaging matrix
% % % by scaling the values so that they fit into the matrix
% Calculate euclidean distance between first and last spiral
% elements to scale this dimension
dist_ksp = sqrt( ( KX(1) - KX(end) ).^2 + ... 
                 ( KY(1) - KY(end) ).^2 );
% The first coordinate of the spiral is not exactly (0,0)
% Memorize it so that you can shift the spiral to (0,0) before
% scaling
x0_ksp = KX(1); y0_ksp = KY(1);
% The spiral trajectory that you need to scale shifted to (0,0)
traj_spiral = [ KX - x0_ksp, ...
                KY - y0_ksp, ...
                KZ       ].';
% Scale it using bart
traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], ...
                               traj_spiral);
% Store the scaled version of the trajectory
KX = traj_spiral2(1,:) + x0_ksp;
KY = traj_spiral2(2,:) + y0_ksp;

% % % % % % Plot the coordinates after scaling
subplot(2,2,4); hold on
scatter(KX, KY, '.');
axis equal, axis square;
xlabel('N_x'); ylabel('N_y');
title('Fully Sampled Spiral Scaled')
xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

% Drawnow or pause
drawnow

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% 2. Get signal values from fully sampled simulation

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderName, 'signalsFS.h5']; 

% % % Read in the vector values Mx,My,Mz
A = (h5read(filenameSignal, '/signal/channels/00'))';
% % % Read in the timings
t =  h5read(filenameSignal, sprintf('/signal/times'));
% % % Sort timings
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);

% % % Get kspace as one signal
signalValuesFS = Mvecs(:, 1) + sqrt(-1) * Mvecs(:, 2);


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% 3. Reconstruct with BART

tic
% Prepare trajectory for current image
traj_kspace = [ KX ; KY ; KZ.'] ;

% Reconstruct with BART
igrid = bart( 'nufft -i -t', traj_kspace, signalValuesFS.');
igrid = bart(['resize -c 0 ', num2str(Nx), ...
                       ' 1 ', num2str(Ny)], igrid);
toc

% Plot images (ABS)
figure(115)
subplot(3,3,4), imagesc(abs(igrid))
title('BART FS (abs)')
axis square; axis off;
colormap gray;
colorbar;

% REAL
subplot(3,3,5), imagesc(real(igrid))
title('BART FS (real) ');
axis square; axis off;
colormap gray;
colorbar;

% IMAG
subplot(3,3,6), imagesc(imag(igrid))
title('BART FS (imag)')
axis square; axis off;
colormap gray;
colorbar;

% % % DIFFERENCES between fully sampled and sparsely sampled
% reconstructions
subplot(3,3,7), imagesc(abs(igrid-imagesBART))
title('BART FS - SS (abs)')
axis square; axis off;
colormap gray;
colorbar;

% REAL
subplot(3,3,8), imagesc(real(igrid-imagesBART))
title('BART FS - SS (real) ');
axis square; axis off;
colormap gray;
colorbar;

% IMAG
subplot(3,3,9), imagesc(imag(igrid-imagesBART))
title('BART FS - SS (imag)')
axis square; axis off;
colormap gray;
colorbar;




