% % % % IRINA GRIGORESCU
% % % % 
% % % % 
% % % % This script is to compare JEMRIS signals vs POSSUM signals on the
% same input object (same number of voxels, same dimensions, same
% relaxation parameters) and the same sequence parameters


% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% POSSUM matlab scripts are here
addpath(genpath('~/possumdevirina/'))

% Prerequisites for JEMRIS AND POSSUM
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);

% % SEQUENCE for both
% folderNameSeq = ['~/OneDrive - University College London/Work/', ...
%                  'JEMRISSimulations/feb2018/seqForPOSSUM/'] ;
             
folderNameSeq = ['~/jemrisSims/seqForPOSSUM/'];             
 
% % SIMULATION for JEMRIS
% folderNameSimJEMRIS = ['/Users/irina/OneDrive - ', ...
%                        'University College London/Work/', ...
%                        'JEMRISSimulations/feb2018/', ...
%                        'seqForPOSSUM/toCompareWithPOSSUM/'] ;
folderNameSimJEMRIS = ['~/jemrisSims/seqForPOSSUM/'];             

% % SIMULATION for POSSUM
folderNameSimPOSSUM = ['/Users/irina/OneDrive - ', ...
                       'University College London/Work/', ...
                       'POSSUMSimulations/simdirWithJEMRISseq/', ...
                       'outSignal/'] ;

                   
% % % % % PROPERTIES OF THE SPIRAL 
% Set number of TR blocks
NTRs =  100;             % 100
% Set number of readout points per TR
nROPoints = 1;      %   1

%% SIGNALS FROM POSSUM
signalsPOSSUM = zeros(nROPoints, NTRs);

experimentFolder = 'toCompareWithJEMRIS/';

% % % Get kspace as one signal
for idxTR = 1:NTRs
    % % % % % % Get signal values from POSSUM simulation
    filenameSignalPOSSUM = [folderNameSimPOSSUM, experimentFolder, ...
                      'signal_rectgradNTR_', num2str(idxTR)]; 
    Mvecs = read_pulse(filenameSignalPOSSUM);
    
    % Get signal values
    signalsPOSSUM(:,idxTR) = Mvecs(1, :) + sqrt(-1) * Mvecs(2, :);
    
end

signalsPOSSUM = signalsPOSSUM ./ 317;

%% SIGNALS FROM JEMRIS
% Allocate memory for kspace values
signalsJEMRIS = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderNameSimJEMRIS, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    signalsJEMRIS(:,idxTR) = ...
                       Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end



%% COMPARE THEM
titleFigure1 = 'POSSUM vs JEMRIS';
titleFigure2 = titleFigure1;

figure('Name', titleFigure1, ...
       'units', 'normalized', 'outerposition', [0 0 1 1])
subplot(3,1,1)
plot(real(signalsPOSSUM(:)), '.-') ; hold on
plot(imag(signalsPOSSUM(:)), '.-') ;
plot(abs(signalsPOSSUM(:)), '.--') ;
legend('realPOSSUM','imagPOSSUM','absPOSSUM')
xlabel('RO point'); ylabel('Signal');

subplot(3,1,2)
plot(real(signalsJEMRIS(:)), '.-') ; hold on
plot(imag(signalsJEMRIS(:)), '.-') ;
plot(abs(signalsJEMRIS(:)), '.--') ;
legend('realJEMRIS','imagJEMRIS','absJEMRIS')
xlabel('RO point'); ylabel('Signal');

subplot(3,1,3)
plot(real(signalsPOSSUM(:)) - real(signalsJEMRIS(:))) ; hold on
plot(imag(signalsPOSSUM(:)) - imag(signalsJEMRIS(:))) ; 
plot(abs(signalsPOSSUM(:)) - abs(signalsJEMRIS(:))) ; 
legend('realPOSSUM - realJEMRIS','imagPOSSUM - imagJEMRIS', ...
       'absPOSSUM - absJEMRIS')
xlabel('RO point'); ylabel('Difference in Signals');

% % % % % % % % % % % % % % % % % % % % 
figure('Name', titleFigure2, ...
       'units', 'normalized', 'outerposition', [0 0 1 1])
subplot(2,1,1)
plot(abs(signalsPOSSUM(:)), '.-') ; hold on
plot(abs(signalsJEMRIS(:)), '.--') ; 
legend('possum','jemris')
xlabel('RO point'); ylabel('Magnitude of Signal');

subplot(2,1,2)
plot(angle(signalsPOSSUM(:)), 'o-') ; hold on
plot(angle(signalsJEMRIS(:)), '.--') ; 
legend('possum','jemris')
xlabel('RO point'); ylabel('Phase of Signal');





return
%% 1. GET THE SEQUENCE
% % % % % PROPERTIES OF THE SPIRAL 
% Set number of TR blocks
NTRs =  2; 
% Set effective image size
Nx   = 128; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 1; 

% % % % % PROPERTIES OF THE SPIRAL TAKEN FROM JEMRIS
% Get sequence values
RXP = h5read([folderNameSeq,'seq.h5'],'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KZ');   % KZ (rad/mm)
GX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GX');   % GX
GY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GY');   % GY
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure('Name', ['Spiral Trajectory from JEMRIS' ], ...
       'units', 'normalized', 'outerposition', [0 0 1 1])
pause(0.5)
for idxTR = 1:NTRs
    
    % Populate the spiral k-spaces
    kspace_x(:,idxTR) = KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints) ./ (2*pi);
    kspace_y(:,idxTR) = KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints) ./ (2*pi);
        
    % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
    if mod(idxTR,50) == 0 || (idxTR <= 10)
        % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
        subplot(1,2,1); xl = 0.5;
        % Coordinates from JEMRIS
        scatter(kspace_x(:,idxTR), ...
                kspace_y(:,idxTR), '.');
        axis equal, axis square;
        xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
        title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
        xlim([-xl xl]); ylim([-xl xl]);

    end

    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
	x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:) + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:) + y0_ksp;
    
    % Plot the coordinates after scaling
    if mod(idxTR,50) == 0 || (idxTR <= 10)
        subplot(1,2,2)
        scatter(kspace_x(:, idxTR), kspace_y(:, idxTR),'.') 
        axis equal, axis square;
        xlabel('N_x'); ylabel('N_y');
        title(['Spiral ', num2str(idxTR), ' (JEMRIS scaled for reco)'])
        xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

        pause(0.01)
    end
    
end

