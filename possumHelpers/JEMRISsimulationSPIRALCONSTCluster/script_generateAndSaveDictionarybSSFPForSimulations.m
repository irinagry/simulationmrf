% % % % Run this script to generate the FAs and TRs like in the bSSFP paper
% % % % Irina Grigorescu


%% Simulate dictionary
% % % % % % % % % % % Generate dictionary 
NTRs = 500;

% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
% SEQFLAG  = 6; % sinusoidal FA, 0 PhA, fixed TRs, fixed ROs
SEQFLAG  = 5; % if == 5 need to provide customSequence 
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Material
% % T1 ranges from 100ms to 5000ms with different timesteps
T1_mrf = 100:20:2000;            % from 100ms to 2000ms dt = 20ms
T1_mrf = [T1_mrf 2300:300:5000]; % from 2300ms to 5000ms dt = 300ms
% 500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580
T1_mrf = [T1_mrf, 225, 335, 330, 1010, 1615, 675, 665, 830, 1150];
T1_mrf = sort(T1_mrf);
% % 
% % T2 ranges from 20ms to 3000ms with different timesteps
T2_mrf = 20:5:100;          % from 20ms to 100ms dt = 5ms
T2_mrf = [T2_mrf 110:10:200];   % from 110ms to 200ms dt = 10ms
T2_mrf = [T2_mrf 400:200:3000]; % from 400ms to 3000ms dt = 200ms
% 360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160
T2_mrf = [T2_mrf, 360,  370,  155];
T2_mrf = sort(T2_mrf);

df_mrf =   0;

flagIR =   1;

% % % % Custom Material
customMaterial.T1     = T1_mrf;
customMaterial.T2     = T2_mrf;
customMaterial.offRes = df_mrf;

% % % % Custom Sequence
customSequence        = struct;
customSequence.RF.FA  = dlmread([...
  '/Users/irina/OneDrive - University College London/Work/',...
  'Simulation/simulationMRF/src/preprocess/FAsForExperimentsOnCluster']).';
customSequence.RF.PhA = zeros(NTRs,1);
customSequence.TR     = zeros(NTRs,1) + 15;
customSequence.RO     = zeros(NTRs,1) +  6;


tic

[ materialProperties, sequenceProperties, ...
  materialTuples, M, ...
  dictionaryMRF] = simulateMRF(NTRs, FLAGPLOT, ...
                                     SEQFLAG, customSequence, ...
                                     MATFLAG, customMaterial, flagIR);
t=toc; 

% % % % All dictionary signals are transformed into a dictionary matrix
% % % % where both real and imaginary channels are stored
dictionaryMRFComplex = [ squeeze(dictionaryMRF(1:M, :, 1))  , ...
                         squeeze(dictionaryMRF(1:M, :, 2)) ];

% % % % Normalize signals:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[dictionaryMRFNorm2Ch, dictionaryMRFNorm] = ...
                                 normalizeSignals(dictionaryMRFComplex);
                             
% Create dictionary of signal values (M x N) from the normalized signals
signalDictionaryMRF        = abs(dictionaryMRFNorm);

figure, plot(1:NTRs, sequenceProperties.RF.FA, '.-')


%%
figure; 

for idxT1 = 1:size(T1_mrf,2)
    
    T1now = T1_mrf(idxT1);
    indicesOfInterest = (materialTuples(1,:) == T1now);

    subplot(1,2,1); hold on
    view(-45,5);
    corrMatrix2 = dictionaryMRFNorm(indicesOfInterest,:) * ...
                  dictionaryMRFNorm(indicesOfInterest,:)';
    s = surf(  corrMatrix2 , ...
              'FaceAlpha', 0.25); grid on
    s.EdgeColor = 'none';
    
    
    subplot(1,2,2)
    s = surf(  corrMatrix2 , ...
              'FaceAlpha', 1); grid on
    s.EdgeColor = 'none';
    view(-5,5);
    
    pause(0.1)
end


% % % % % Keep T2 fixed and calculate correlation between signals for T1s
% % % % subplot(1,2,1)
% % % % for idxT2 = 1:size(T2_mrf,2)
% % % %     
% % % %     
% % % %     T2now = T2_mrf(idxT2);
% % % %     indicesOfInterest = (materialTuples(2,:) == T2now);
% % % %     
% % % %     corrMatrix = signalDictionaryMRF(indicesOfInterest, :)  * ...
% % % %                  signalDictionaryMRF(indicesOfInterest, :)' ;
% % % %     
% % % %     imagesc(  corrMatrix  )
% % % %     xlim([1, sum(indicesOfInterest)]); ylim([1, sum(indicesOfInterest)])
% % % %     
% % % %     
% % % %     title(['T_2 = ', num2str(T2now)])
% % % %     pause(0.01)
% % % % end    
% % % % 
% % % % % Keep T2 fixed and calculate correlation between signals for T1s
% % % % subplot(1,2,2)
% % % % for idxT1 = 1:size(T1_mrf,2)
% % % %     
% % % %     
% % % %     T1now = T1_mrf(idxT1);
% % % %     indicesOfInterest = (materialTuples(1,:) == T1now);
% % % %     
% % % %     corrMatrix = signalDictionaryMRF(indicesOfInterest, :)  * ...
% % % %                  signalDictionaryMRF(indicesOfInterest, :)' ;
% % % %     
% % % %     imagesc(  corrMatrix  )
% % % %     xlim([1, sum(indicesOfInterest)]); ylim([1, sum(indicesOfInterest)])
% % % %     
% % % %     
% % % %     title(['T_1 = ', num2str(T1now)])
% % % %     pause(0.01)
% % % % end    
% % % % 
% % % % % 
% % % % % imagesc(signalDictionaryMRF(:,:)*signalDictionaryMRF(:,:)')
% % % % %                         
% % % % % subplot(1,2,2)
% % % % % imagesc(signalDictionaryMRF(idxT2,:)*signalDictionaryMRF(idxT2,:)')
% % % %                         
% % % %                         
% % % %                         
% % % %                         
% % % %                         
                        