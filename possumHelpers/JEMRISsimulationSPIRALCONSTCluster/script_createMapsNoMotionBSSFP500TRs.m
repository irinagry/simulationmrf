% % % % Run this script to create maps for the 500TRs no motion case
% % % % Irina Grigorescu

% % % % This is for A=0.48, F=120, Rec to 128x128, FAs generated as bssfp

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);

 
% % SEQUENCE
folderNameSeq = ['~/OneDrive - University College London/Work/', ...
              'JEMRISSimulations/feb2018/nomot_500TRs/'] ;
 
folderNameSeq = '/Users/irina/jemrisSims/feb2018/nomot_500TRs/';
          
% % SIMULATION
folderNameSim = ['~/OneDrive - University College London/Work/', ...
                 'JEMRISSimulations/feb2018/nomot_500TRs/'] ;
                                            % mot_rot_end/
                                            % mot_rot_beg/
                                            % mot_rot_end/
                                            
folderNameSim = '/Users/irina/jemrisSims/feb2018/nomot_500TRs/';
 
%% SEQUENCE
% % % % % PROPERTIES OF THE SPIRAL 
% Set number of TR blocks
NTRs =  500; 
% Set effective image size
Nx   = 96; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 6000; 

% % % % % PROPERTIES OF SPIRAL (MATLAB)
% Create the spiral k-space in MATLAB
Td = 6; %ms
dt = Td / nROPoints;
t = dt:dt:Td;
spiralPower = 1;

spiralAmplitude = 0.4; %  0.48;
spiralFrequency = 120; %  120;
rotAngleConsecutiveTR = repmat([0 0], [1, NTRs]); 

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);

kk_x_M = zeros(nROPoints, NTRs); kk_y_M = zeros(nROPoints, NTRs);
for idxTR = 1:NTRs
    % Calculate k-space trajectory for each TR
    kk_x_M(:,idxTR) = cos(rotAngleConsecutiveTR(idxTR)) * k_x - ...
                      sin(rotAngleConsecutiveTR(idxTR)) * k_y ;
    
    kk_y_M(:,idxTR) = sin(rotAngleConsecutiveTR(idxTR)) * k_x + ... 
                      cos(rotAngleConsecutiveTR(idxTR)) * k_y;
end
kk_x_M = kk_x_M ./(2*pi);
kk_y_M = kk_y_M ./(2*pi);

clear k_x k_y 

% % % % % PROPERTIES OF THE SPIRAL TAKEN FROM JEMRIS
% Get sequence values
RXP = h5read([folderNameSeq,'seq.h5'],'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KZ');   % KZ (rad/mm)
GX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GX');   % GX
GY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GY');   % GY
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure('Name', ['Spiral Trajectory A=', num2str(spiralAmplitude), ...
                                 ' f=', num2str(spiralFrequency) ], ...
       'units', 'normalized', 'outerposition', [0 0 1 1])
pause(0.5)
for idxTR = 1:NTRs
    
    % Populate the spiral k-spaces
    kspace_x(:,idxTR) = KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints) ./ (2*pi);
    kspace_y(:,idxTR) = KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints) ./ (2*pi);
        
    % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
    if mod(idxTR,50) == 0 || (idxTR <= 10)
        % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
        subplot(2,2,1); xl = 0.5;
        % Coordinates from JEMRIS
        scatter(kspace_x(:,idxTR), ...
                kspace_y(:,idxTR), '.');
        axis equal, axis square;
        xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
        title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
        xlim([-xl xl]); ylim([-xl xl]);

        % % % % % % Plot the coordinates before scaling (taken from MATLAB)
        subplot(2,2,3); 
        % Coordinates from JEMRIS
        scatter(kk_x_M(:,idxTR), ...
                kk_y_M(:,idxTR), '.');
        axis equal, axis square;
        xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
        title(['Spiral ', num2str(idxTR), ' (MATLAB)'])
        xlim([-xl xl]); ylim([-xl xl]);
    end

    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
	x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:) + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:) + y0_ksp;
    
    % Plot the coordinates after scaling
    if mod(idxTR,250) == 0 || (idxTR <= 10)
        subplot(2,2,2)
        scatter(kspace_x(:, idxTR), kspace_y(:, idxTR),'.') 
        axis equal, axis square;
        xlabel('N_x'); ylabel('N_y');
        title(['Spiral ', num2str(idxTR), ' (JEMRIS scaled for reco)'])
        xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

        pause(0.01)
    end
    
end

 


%% SIGNALS
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderNameSim, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end

 
% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints, NTRs]);


% % 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);


figure('Name', 'Reconstructions', ...
       'units', 'normalized', 'outerposition', [0 0 1 1])
pause(0.5)
for idxTR = 1:NTRs
    tic
    disp(['Image number ', num2str(idxTR)]);
    % Retrieve current values for current reconstruction
    currentKspace_x     = kspace_x(:, idxTR);
    currentKspace_y     = kspace_y(:, idxTR);
    currentKspaceToReco = (kspaces(:, idxTR)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';

                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART(:, :, idxTR) = igrid;
    toc

    if (mod(idxTR, 50) == 0 || (idxTR <= 10))
        % Plot images (ABS)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) ', num2str(idxTR)])
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;
    end   
    
    pause(0.01)
    
    
    clear igrid traj_kspace
end

% % % % Normalize the reconstructed images
[~, imagesBART] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBART = (reshape(imagesBART, [Nx, Ny, NTRs]));


%% SIMULATE THE DICTIONARY
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
% SEQFLAG  = 6; % sinusoidal FA, 0 PhA, fixed TRs, fixed ROs
SEQFLAG  = 5; % if == 5 need to provide customSequence 
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Material

% % T1 ranges from 100ms to 5000ms with different timesteps
T1_mrf = 100:20:2000;            % from 100ms to 2000ms dt = 20ms
T1_mrf = [T1_mrf 2300:300:5000]; % from 2300ms to 5000ms dt = 300ms
T1_mrf = [T1_mrf, 225, 335, 330, 1010, 1615, 675, 665, 830, 1150];
% T1_mrf = [ 500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
T1_mrf = sort(T1_mrf);

% % 
% % T2 ranges from 20ms to 3000ms with different timesteps
T2_mrf = 20:5:100;          % from 20ms to 100ms dt = 5ms
T2_mrf = [T2_mrf 110:10:200];   % from 110ms to 200ms dt = 10ms
T2_mrf = [T2_mrf 400:200:3000]; % from 400ms to 3000ms dt = 200ms
T2_mrf = [T2_mrf, 360,  370,  155];
% T2_mrf = [ 360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];
T2_mrf = sort(T2_mrf);

df_mrf =   0;

flagIR =   1;

% % % % Custom Material
customMaterial.T1     = T1_mrf;
customMaterial.T2     = T2_mrf;
customMaterial.offRes = df_mrf;

% % % % Custom Sequence
customSequence        = struct;
customSequence.RF.FA  = dlmread([...
  '/Users/irina/OneDrive - University College London/Work/',...
  'Simulation/simulationMRF/src/preprocess/FAsMaryia']).';
                                           %FAsMaryia
                                           %FAsForExperimentsOnCluster
customSequence.RF.FA  = customSequence.RF.FA(1:NTRs);
customSequence.RF.PhA = zeros(NTRs,1);
customSequence.TR     = zeros(NTRs,1) + 15;
customSequence.RO     = zeros(NTRs,1) +  6;


tic

[ materialProperties, sequenceProperties, ...
  materialTuples, M, ...
  dictionaryMRF] = simulateMRF(NTRs, FLAGPLOT, ...
                                     SEQFLAG, customSequence, ...
                                     MATFLAG, customMaterial, flagIR);
t=toc; 

% % % % All dictionary signals are transformed into a dictionary matrix
% % % % where both real and imaginary channels are stored
dictionaryMRFComplex = [ squeeze(dictionaryMRF(1:M, :, 1))  , ...
                         squeeze(dictionaryMRF(1:M, :, 2)) ];

% % % % Normalize signals:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[dictionaryMRFNorm2Ch, dictionaryMRFNorm] = ...
                                 normalizeSignals(dictionaryMRFComplex);
                             
% Create dictionary of signal values (M x N) from the normalized signals
signalDictionaryMRF        = abs(dictionaryMRFNorm);

figure, plot(1:NTRs, sequenceProperties.RF.FA, '.-')

% % Example dictionary
try
    figure
    for i = 1:100:900
        plot(1:NTRs, abs(dictionaryMRFNorm(1+(i-1)*4,:)),'LineWidth',1.4), hold on
    end
    plot(1:NTRs, abs(dictionaryMRFNorm(163,:)),'LineWidth',1.4)
    xlabel('TR index');
    ylabel('Signal (a.u.)')
    title('Example fingerprints')
    grid on
catch
    warning('You are using SMALL DICTIONARY SIZE.');
end
    

%% Dictionary matching
imagesBARTReshaped = [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                       imag(reshape(imagesBART, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB = matTuplesMatchedB1;
resultsB.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                            valuesOfMatchB3, valuesOfMatchB4];
resultsB.scoremap = reshape(resultsB.score, [Nx, Ny]);                      
resultsB.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB.indicesmap = reshape(resultsB.indices, [Nx, Ny]); 
resultsB.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB.T1map = reshape(resultsB.T1, [Nx, Ny]);
resultsB.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB.T2map = reshape(resultsB.T2, [Nx, Ny]);                
   

%% CREATE MAPS

% % For the 128x128 phantom with 4145 spins
centres = [56,39 ; 74,39 ; ...
           39,55 ; 56,55 ; 74,55 ; 90,55 ; ...
           39,73 ; 56,74 ; 74,74 ; 90,73 ; ...
           56,90 ; 74,90 ; ...
           65,65 ]; % big circle
radii   = [zeros(1,12) + 5 , 37];

% % % % % For the 96x96 phantom with 4145 spins
% % % centres = [41,27 ; 56,27 ; ...
% % %            27,41 ; 41,41 ; 56,41 ; 70,41 ; ...
% % %            27,56 ; 41,56 ; 56,56 ; 70,56 ; ...
% % %            41,69 ; 56,69 ; ...
% % %            49,49 ]; % big circle
% % % radii   = [zeros(1,12) + 4 , 32];

% 
% realMap = struct;
myPhantomT1Values = [500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
myPhantomT2Values = [360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];

[realMap.T1map, realMap.T2map, maskAllCircles, maskHollow] = ...
         func_createRealT1T2Maps(myPhantomT1Values, myPhantomT2Values, ...
                                 Nx, Ny, centres, radii);
                             
% maskAllCircles = ones(Nx,Ny);
% % Reconstructed maps of T1 and T2 and the real (ground truth) maps
figure
% T1 real
subplot(2,3,1)
imagesc(realMap.T1map)%.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT1Values)])
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(realMap.T2map)%.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT2Values)])
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(resultsB.T1map)%.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT1Values)])
title('T_1 BART')

% BART - T2
subplot(2,3,5)
imagesc(resultsB.T2map)%.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0, max(myPhantomT2Values)])
title('T_2 BART')

% BART - Score
subplot(2,3,6)   
imagesc(resultsB.scoremap)%.*maskAllCircles.*maskHollow)
colormap hot; colorbar, axis square
caxis([0 1])
title('Score BART')

%% % % Mean Values for each block
% % T1
[ meanValuesReal.T1 , meanValuesSimu.T1 ] = ...
    func_retrieveMeanValueInROI(realMap.T1map, resultsB.T1map, ...
                            centres, [radii(1:end-1)-1, 4], 0);
                            %centres(1:end-1,:), radii(1:end-1)-1, 0);
% % T2
[ meanValuesReal.T2 , meanValuesSimu.T2 ] = ...
    func_retrieveMeanValueInROI(realMap.T2map, resultsB.T2map, ...
                            centres, [radii(1:end-1)-1, 4], 0);
                            %centres(1:end-1,:), radii(1:end-1)-1, 0);

figure
subplot(1,2,1)
plot(meanValuesReal.T1 , meanValuesSimu.T1, 'o'); hold on
plot([0, max(meanValuesReal.T1)], [0, max(meanValuesReal.T1)])
axis equal, axis square
xlim([0 max(meanValuesReal.T1)+200])
ylim([0 max(meanValuesReal.T1)+200])
xlabel('Real T_1'); ylabel('Simulated T_1');

subplot(1,2,2)
plot(meanValuesReal.T2 , meanValuesSimu.T2, 'o'); hold on
plot([0, max(meanValuesReal.T2)], [0, max(meanValuesReal.T2)])
axis equal, axis square
xlim([0 max(meanValuesReal.T2)+100])
ylim([0 max(meanValuesReal.T2)+100])
xlabel('Real T_2'); ylabel('Simulated T_2');



% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
%% Have a look at individual signals

imagesBARTReshaped1Ch = reshape(imagesBART, [Nx*Ny, NTRs ]);

% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4', 'c3'};

% % % % % Scores for the 128x128 phantom with 4145 spins
% % % scoreIdxB = [(64-1)*Nx+65, ... %big circle
% % %              (74-1)*Nx+55, ... %c5
% % %              (55-1)*Nx+74, ... %c8
% % %              (74-1)*Nx+90, ... %c12
% % %              (55-1)*Nx+39, ... %c1
% % %              (55-1)*Nx+55];    %c4        

% % Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(49-1)*Nx+49, ... %big circle %34/49 to the left of centref
             (57-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+27, ... %c1
             (40-1)*Nx+41, ... %c4
             (26-1)*Nx+38];    %c3
         
figure
llPhantom = squeeze(abs(imagesBART(:,:,1)));%.*maskAllCircles; %realMap.T1map
llPhantom = llPhantom(:);
llPhantom(scoreIdxB(2)) = 0; 
colormap hot; colorbar
llPhantom = reshape(llPhantom, [Nx, Ny]);
imagesc(llPhantom), axis square, colormap gray


%%
% % % This plots the difference between the two signals of same voxel

for idxTR = 1:size(scoreIdxB,2)
    
    
    figure
    
    % % % % Is this the voxel I was looking for?
    subplot(2,2,1)
    llPhantom = squeeze(abs(imagesBART(:,:,1)));%.*maskAllCircles; %realMap.T1map
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray

    % % % % Plot abs value signal
    subplot(2,2,[3,4])
    % dictionary signal it matched to
    plot(1:NTRs, abs(dictionaryMRFNorm(resultsB.indices(scoreIdxB(idxTR)), :)), 'r-')
    hold on
    % voxel signal motion
    plot(1:NTRs, abs(imagesBARTReshaped1Ch(resultsB.indices(scoreIdxB(idxTR)), :)), '.-', ...
         'LineWidth', 1.1)
                 
    legend('dictionary', 'image')
    title(['Looking at: ',  titles{idxTR}])
    xlabel('TR index'); ylabel('Signal (a.u.)');
    
    pause
end


