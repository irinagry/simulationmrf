% % % % Run this script to find the best spiral for the chosen phantom
% % % % Irina Grigorescu

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);

%% SEQUENCE
% % % % % PROPERTIES OF THE SPIRAL 
% Set number of TR blocks
NTRs =   10; 
% Set effective image size
Nx   = 128; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 6000; 

% % % % % PROPERTIES OF SPIRAL (MATLAB)
% Create the spiral k-space in MATLAB
Td = 6; %ms
dt = Td / nROPoints;
t = dt:dt:Td;
spiralPower = 1;

spiralAmplitude = 0.48; %  0.4;
spiralFrequency = 120; %  120;
rotAngleConsecutiveTR = repmat([0 0], [1, NTRs]); 

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);

kk_x_M = zeros(nROPoints, NTRs); kk_y_M = zeros(nROPoints, NTRs);
for idxTR = 1:NTRs
    % Calculate k-space trajectory for each TR
    kk_x_M(:,idxTR) = cos(rotAngleConsecutiveTR(idxTR)) * k_x - ...
                      sin(rotAngleConsecutiveTR(idxTR)) * k_y ;
    
    kk_y_M(:,idxTR) = sin(rotAngleConsecutiveTR(idxTR)) * k_x + ... 
                      cos(rotAngleConsecutiveTR(idxTR)) * k_y;
end
kk_x_M = kk_x_M ./(2*pi);
kk_y_M = kk_y_M ./(2*pi);

clear k_x k_y 

% % % % % PROPERTIES OF THE SPIRAL TAKEN FROM JEMRIS
% Get k-space coordinates from JEMRIS simulation
folderName = '~/jemrisSims/feb2018/cluster_spiralFullySampledConstant1TR/';

% Get sequence values
RXP = h5read([folderName,'seq.h5'],'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read([folderName,'seq.h5'],'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read([folderName,'seq.h5'],'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read([folderName,'seq.h5'],'/seqdiag/KZ');   % KZ (rad/mm)
GX  = h5read([folderName,'seq.h5'],'/seqdiag/GX');   % GX
GY  = h5read([folderName,'seq.h5'],'/seqdiag/GY');   % GY
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure('Name', ['Spiral Trajectory A=', num2str(spiralAmplitude), ...
                                 ' f=', num2str(spiralFrequency) ], ...
       'units', 'normalized', 'outerposition', [0 0 1 1])
pause(0.5)
for idxTR = 1:NTRs
    
    % Populate the spiral k-spaces
    kspace_x(:,idxTR) = KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints) ./ (2*pi);
    kspace_y(:,idxTR) = KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints) ./ (2*pi);
    
    % % % % % % Plot the coordinates before scaling (taken from JEMRIS)
    subplot(2,2,1); xl = 0.5;
    % Coordinates from JEMRIS
    scatter(kspace_x(:,idxTR), ...
            kspace_y(:,idxTR), '.');
	axis equal, axis square;
    xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
    title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
    xlim([-xl xl]); ylim([-xl xl]);
    
    % % % % % % Plot the coordinates before scaling (taken from MATLAB)
    subplot(2,2,3); 
    % Coordinates from JEMRIS
    scatter(kk_x_M(:,idxTR), ...
            kk_y_M(:,idxTR), '.');
	axis equal, axis square;
    xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
    title(['Spiral ', num2str(idxTR), ' (MATLAB)'])
    xlim([-xl xl]); ylim([-xl xl]);
    

    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
	x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:) + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:) + y0_ksp;
    
    % Plot the coordinates after scaling
    subplot(2,2,2)
    scatter(kspace_x(:, idxTR), kspace_y(:, idxTR),'.') 
    axis equal, axis square;
    xlabel('N_x'); ylabel('N_y');
    title(['Spiral ', num2str(idxTR), ' (JEMRIS scaled for reco)'])
    xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);

    if mod(idxTR,10) == 0 || (idxTR < 10)
        pause(0.01)
    end
    
end

 


%% SIGNALS
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderName, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end

 
% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints, NTRs]);


% % 3. Reconstruct with BART
% Save all images
allReconstructedImages = zeros(Nx*Ny, NTRs);
imagesBART = zeros(Nx, Ny, NTRs);


figure('Name', 'Reconstructions', ...
       'units', 'normalized', 'outerposition', [0 0 1 1])
pause(0.5)
for idxTR = 1:NTRs
    tic
    disp(['Image number ', num2str(idxTR)]);
    % Retrieve current values for current reconstruction
    currentKspace_x     = kspace_x(:, idxTR);
    currentKspace_y     = kspace_y(:, idxTR);
    currentKspaceToReco = (kspaces(:, idxTR)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';

                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART(:, :, idxTR) = igrid;
    toc

    if (mod(idxTR,50) == 0 || (idxTR <= 10))
        % Plot images (ABS)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) ', num2str(idxTR)])
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;
    end   
    
    pause(1)
    
    
    clear igrid traj_kspace
end

% % % % Normalize the reconstructed images
[~, imagesBART] = normalizeSignals( ...
                    [ real(reshape(imagesBART, [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesBART, [Nx*Ny, NTRs])) ]);
imagesBART = (reshape(imagesBART, [Nx, Ny, NTRs]));














