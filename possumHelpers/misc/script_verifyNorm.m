

N = 10;

x = rand(1, N) + 1i.*rand(1, N)

xNorm1 = x / sqrt(conj(x) * x.')
norm(xNorm1)


D      = [ rand(1,N) + 1i.*rand(1, N) ; ...
          rand(1,N) + 1i.*rand(1, N) ]
Dbroke = [real(D) imag(D) ]

Dsq = conj(D) .* D;
      
Dsum  = sum(Dsq, 2)

Dsqrt = sqrt(Dsum)

Dnorm = D ./ repmat(Dsqrt, [1, N])

[~, DnormPrime] = normalizeSignals(Dbroke)