% % % % IRINA GRIGORESCU
% % % % Comparing JEMRIS fisp signals with EPG dictionary
% % % % 

% % % % Observation:
% For this script to work, go to: 
% /Users/irina/jemrisSims/sequencesForMRF/fisp/

% I have created in ~/jemrisSims/sequencesForMRF/bssfp/ and /fisp/
% sequences for bssfp and fisp with only RO and dephasing gradient for FISP

% I have calculated the dephasing gradient's amplitude and duration based
% on: Area_dephasingGrad = dphi / (gamma * dz); where
% dphi = n*pi (rad) of dephasing accross voxel size
% dz   = voxel size (m)
% gamma = gyromagnetic ratio (rad * Hz / T)

% I have observed that the EPG signal and the FISP JEMRIS signal are
% similar with a 10^-3 difference. I am unsure if this is acceptable or
% not. The next step is to try in POSSUM. I will first have to create an
% object with just one tissue type.

% Note to self: JEMRIS uses mT/m for gradients

% % % % Preprocessing
% Add paths:
run('runPathsForPOSSUMandEPG');

%% Calculate area of dephasing gradient
dphi  = 2*pi;                % rad
gamma = 2*pi * 42.58 * 1e06; % rad * Hz/T
dz    = 1 * 1e-03;           % m
Area = dphi / (gamma * dz);  % T/m * s

durationGrad = 1 * 1e-03;    % Duration is 2 ms 
AmplitGrad   = Area / durationGrad * 1000;      % mT/m

% In JEMRIS I am using a rectangle gradient instead of a trapezoid

%% Generate EPG signal
% Materials:
T1 = 100000000; 
T2 = 100000000;
% Sequence:
N = 20;
PA = zeros(N,1);
FA = zeros(N,1) + 90; % + (0:1:N-1).';
TR = zeros(N,1) + 15;
TE = zeros(N,1) +  7;

% Run EPG
[ChiF, ChiZ] = epgMatrix(N, PA, FA, TR, T1, T2, 1, 1); % M0, flagDephase

F0states = ChiF(N, :) .* exp(-TE ./ T2).';


sigEPGnotNorm = [real(F0states) imag(F0states)];
[~, sigEPGNorm] = normalizeSignals(sigEPGnotNorm);
sigEPGNorm = abs(sigEPGNorm);
sigEPG = abs(F0states);

%% Get JEMRIS signal
folderName = '~/jemrisSims/sequencesForMRF/fisp/';
fileName   = 'signalsParallelFISPOnlyRO.h5'; %'signals.h5';
AJEMRIS = (h5read([folderName, fileName], '/signal/channels/00')).';


sigJEMRISnotNorm = [AJEMRIS(1:N,1).' AJEMRIS(1:N,2).'];
[~, sigJEMRISNorm] = normalizeSignals(sigJEMRISnotNorm);
sigJEMRIS = abs(sigJEMRISNorm);
sigJEMRIS = sqrt(AJEMRIS(1:N,1).^2 + AJEMRIS(1:N,2).^2).';

%% Plot against each other
figure('Position',[10,10,800,800])
subplot(2,2,[1,2])
plot(1:N, sigEPG,    'ko-'); hold on
plot(1:N, sigJEMRIS, 'b.-');
xlabel('timepoints'); ylabel('signal');
legend('EPG', 'JEMRIS');
title({'EPG vs JEMRIS'})%, 'dephasing accross voxel', 'r0x = 0.001'});

subplot(2,2,3)
plot([0,1],[0,1],'k'); hold on   % identity line
plot(sigEPG, sigJEMRIS, 'bo'); 
xlabel('signal EPG'); ylabel('signal JEMRIS');
axis square

subplot(2,2,4)
plot(1:N, sigJEMRIS-sigEPG, 'b.-');
xlabel('timepoints'); ylabel('JEMRIS - EPG'); 
axis square



