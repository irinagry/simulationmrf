function [ChiF, ChiZ] = ...
    epgMatrix(N, phi, alpha, TR, T1, T2, M0, flagDephase)
% IRINA GRIGORESCU
% DATE CRAETED: 06-08-2017
% DATE UPDATED: 06-08-2017
% Extended Phase Graphs: Dephasing, RF Pulses and Echoes - Pure and
% Simple, Matthias Weigel
% 
% INPUT:
%     N       = number of RF pulses in the sequence
%     phi     = array of phase angles of RF PULSES (about z axis) 
%               (in deg)
%     alpha   = array of flip angles of RF PULSEs (about x axis)  
%               (in deg)
%     TR      = array of repetition times  (ms)
%     T1      = relaxation time T1 (ms)
%     T2      = relaxation time T2 (ms)
%     M0      = echilibrium magnetization
%     flagDephase = 1 for dephasing before readout
%                   0 for balanced ssfp
% 
% OUTPUT: 
%     ChiF    = (2*N-1 x N) the event matrix for transverse magnetization
%     ChiZ    = (  N   x N) the event matrix for longitudinal magnetization
% 

% % % Prerequisites
phi   = deg2rad(phi);
alpha = deg2rad(alpha);
E1    = exp(-TR./T1);
E2    = exp(-TR./T2);

% % % Create matrices 
ChiF = zeros(2*N-1, N);      % The transverse states immediately after 
                             % the RF pulses
ChiZ = zeros(  N,   N);      % The longitudinal states immediately after 
                             % the RF pulses

OmegaPreRF  = zeros(3, N+1); % State matrix immediately before RF pulse
                             % starts with [0,0,M0]' and therefore it has
                             % N+1 columns
% if flagIR == 1
%     OmegaPreRF(3,1) = -1*M0;
%     % Longitudinal relaxation before FISP acquisition starts 
%     % Delay is TR
%     %OmegaPreRF(3,1) = OmegaPreRF(3,1) .* E1(1,1) + M0.*(1 - E1(1,1));
% else
OmegaPreRF(3,1) =    M0;
% end

OmegaPostRF = zeros(3, N);   % Immediately after RF pulse
                            

% % % Loop over RF pulses
for idPulse = 1:N
    % % % % %
    % % 1. Apply RF pulse to the pre RF pulse state matrix 
    OmegaPostRF(:, 1:idPulse) = ...
          Tmatrix(phi(idPulse), alpha(idPulse)) * OmegaPreRF(:, 1:idPulse);
      
    % % % % %
    % % 2. Store these states in the big states matrices
    % Store first line of OmegaPostRF from 1 to idPulse at 
    % lines N, N-1, N-2, ..., N-(idPulse-1) on idPulse column
    ChiF(N+1 - (1:idPulse), idPulse) =      OmegaPostRF(1, 1:idPulse);
    
    % Store second line of OmegaPostRF from 2 to idPulse at 
    % lines    N+1, N+2, ..., N+(idPulse-1) on idPulse column
    ChiF(N-1 + (2:idPulse), idPulse) = conj(OmegaPostRF(2, 2:idPulse));
    
    % Store third line of OmegaPostRF from 1 to idPulse at
    % lines N, N-1, N-2, ..., N-(idPulse-1) on idPulse column
    ChiZ(N+1 - (1:idPulse), idPulse) =      OmegaPostRF(3, 1:idPulse);
    
    % % % % %
    % % 3. Relaxation
    % Transverse relaxation on all F states
    OmegaPreRF(1:2, 1:idPulse) = OmegaPostRF(1:2, 1:idPulse) * ...
                                 E2(idPulse);
	% Longitudinal relaxation on all Z states with k>0
    OmegaPreRF(  3, 2:idPulse) = OmegaPostRF(  3, 2:idPulse) * ...
                                 E1(idPulse);
                                 
    % Longitudinal recovery on Z states with k==0
    OmegaPreRF(  3, 1        ) = OmegaPostRF(  3, 1        ) * ...
                                 E1(idPulse) + ...
                                 M0.*(1 - E1(idPulse));
	
	% % % % %
    % % 4. Dephasing
    % Dephasing before readout
    if flagDephase ==  1
        % Right shifting (F+2 <= F+1 <= F+0)
        OmegaPreRF(1, 2:(idPulse+1)) = OmegaPreRF(1, 1:idPulse); 
        % Left shifting  (F-0 <= F-1 <= F-2 <= F-3)
        OmegaPreRF(2, 1:idPulse)     = OmegaPreRF(2, 2:(idPulse+1)); 
        % F+0 = (F-0*)*
        OmegaPreRF(1, 1)             = conj(OmegaPreRF(2, 1)); 
    end
                             
end

% % % % % Make nice zeroes
% Taking care of innacuracies 
ChiF(abs(ChiF)<eps*1e3) = 0;
ChiZ(abs(ChiZ)<eps*1e3) = 0;



