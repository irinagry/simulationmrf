% % % % IRINA GRIGORESCU
% % % % DATE CRAETED: 25-06-2017
% % % % DATE UPDATED: 25-06-2017
% % % % Extended Phase Graphs: Dephasing, RF Pulses and Echoes - Pure and
% % % % Simple, Matthias Weigel
% % % % 
% % % % This script takes a closer look at the EPG as presented by Matthias
% % % % 

clear all; 
close all; 
clc;

run('runForPaths.m');


%% 
% % % Load Sequence Properties
% % % FA(deg) | TR(ms) | PhA(deg) | TE(ms)
% seqPropertiesMatrix = dlmread('seqPropTRPerlinFASinusoidPhAAlternate');
% seqPropertiesMatrix = ...
%           csvread(['/Users/irina/OneDrive - University College ', ...
%                    'London/Work/MRF/MaryiaDonevaMRF_ForUCL/', ...
%                    'best_slen500_TE6_TR15.csv'], 13);
seqPropertiesMatrix = zeros(500, 4);
seqPropertiesMatrix(:,1) = 45;
seqPropertiesMatrix(:,2) = 15;
seqPropertiesMatrix(:,3) =  0;
seqPropertiesMatrix(:,4) =  6;

Nro = size(seqPropertiesMatrix, 1);
               
% % TISSUE properties
T1 = 200;
T2 =  80; 
M0 =   1; flagDephase = 1; 

% % SEQUENCE properties
FAsEPG  = [ 180;   seqPropertiesMatrix(:,1)];
PhAsEPG = [   0;   seqPropertiesMatrix(:,3)];
TRsEPG  = [  15;   seqPropertiesMatrix(:,2)];
TEsEPG  = [   6;   seqPropertiesMatrix(:,4)];
Nro = Nro + 1;

% % Run simulation
tic
[ChiFI, ChiZI] = epgMatrix(Nro, PhAsEPG, FAsEPG, TRsEPG, ...
                           T1, T2, M0, flagDephase);
F0statesI = ChiFI(Nro, :) .* exp(-TEsEPG./T2).';
t = toc;
disp(['EPG took ', num2str(t), ' s']);

% % % % Plotting evolution matrices
% % figure
% % subplot(2,2,1)
% % imagesc(real(ChiFI))
% % axis square
% % colormap gray
% % title('Real(X_F) Irina')
% % 
% % subplot(2,2,2)
% % imagesc(imag(ChiFI))
% % colormap gray
% % axis square
% % title('Imag(X_F) Irina')
% % 
% % subplot(2,2,3)
% % imagesc(real(ChiZI))
% % axis square
% % colormap gray
% % title('Real(X_Z) Irina')
% % 
% % subplot(2,2,4)
% % imagesc(imag(ChiZI))
% % colormap gray
% % axis square
% % title('Imag(X_Z) Irina')

%% Do the same with Weigel code

% % % % F0statesM are immediately after RF pulse
[F0statesM, ChiFM, ChiZM] = ssfp_epg_domain_fplus_fminus(Nro, FAsEPG, ...
                                    PhAsEPG, TRsEPG, T1, T2, flagDephase);
% % % % If you want at TE than do some relaxation on these ones
F0statesM = F0statesM.* exp(-TEsEPG./T2).';


% % % % Plotting evolution matrices
% % figure
% % subplot(2,2,1)
% % imagesc(real(ChiFM))
% % axis square
% % colormap gray
% % title('Real(X_F) Matthias')
% % 
% % subplot(2,2,2)
% % imagesc(imag(ChiFM))
% % colormap gray
% % axis square
% % title('Imag(X_F) Matthias')
% % 
% % subplot(2,2,3)
% % imagesc(real(ChiZM))
% % axis square
% % colormap gray
% % title('Real(X_Z) Matthias')
% % 
% % subplot(2,2,4)
% % imagesc(imag(ChiZM))
% % colormap gray
% % axis square
% % title('Imag(X_Z) Matthias')

%% Do the same with dictionary generation
% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Sequence
FA_mrf  =  FAsEPG;
PhA_mrf = PhAsEPG;
TR_mrf  =  TRsEPG;
RO_mrf  =  TEsEPG;

% % % % Custom Material
T1_mrf = T1;
T2_mrf = T2;
df_mrf =  0;

[materialProperties, sequenceProperties, ...
          materialTuples, M, ...
          dictionaryMRF, ~, ~, ~, ~] = ...
	func_generateCustomSignals(Nro, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG, 0, 1); % flagIR = 0

                 
                
%% COMPARISON WITH MRF

sigMRF = abs(squeeze(dictionaryMRF(1,:,1)) + ...
        1i.* squeeze(dictionaryMRF(1,:,2)));
sigEPG = abs(F0statesI(1, 1:Nro));
sigMat = abs(F0statesM(1, 1:Nro));

NtoPlot = Nro;%50;

figure('Position', [10,10,1200,800]);
% % % % Signals
subplot(3,3,[1,3])
plot(1:NtoPlot, sigMRF(1:NtoPlot), 'r.-');
hold on
plot(1:NtoPlot, sigMat(1:NtoPlot), 'g--.');
plot(1:NtoPlot, sigEPG(1:NtoPlot), 'b--.');
xlabel('TR block'); ylabel('signal magnitude');
grid on
legend('MRF_{Irina}','EPG_{Matthias}','EPG_{Irina}')
title({'MRF Dictionary Generation and EPG Formalism for FISP', ...
       ['T_1 = ', num2str(T1), 'ms', ' T_2 = ', num2str(T2), 'ms and ', ...
        'T_R = ', num2str(mean(TRsEPG)), 'ms']})

% % % % Comparison of methods
% % MRF vs EPG-Irina 
subplot(3,3,4)
plot(sigMRF(1:NtoPlot), ...
     sigEPG(1:NtoPlot), 'ro'), hold on
plot(0:0.1:1,0:0.1:1,'b') % identity line
grid on, axis square
xlabel('MRF_{Irina}');
ylabel('EPG_{Irina}');

% % MRF vs EPG Matthias
subplot(3,3,5)
scatter(sigMRF(1:NtoPlot), ...
        sigMat(1:NtoPlot), 'ro'), hold on
plot(0:0.1:1, 0:0.1:1, 'b')
grid on, axis square
title('Magnitude of Signal Comparison')
xlabel('MRF_{Irina}');
ylabel('EPG_{Matthias}');

% % EPG-Irina vs EPG Matthias
subplot(3,3,6)
scatter(sigEPG(1:NtoPlot), ...
        sigMat(1:NtoPlot), 'ro'), hold on
plot(0:0.1:1, 0:0.1:1, 'b')
grid on, axis square
xlabel('EPG_{Irina}');
ylabel('EPG_{Matthias}');


% % % % Difference of methods
% % MRF vs EPG-Irina 
subplot(3,3,7)
stem(1:NtoPlot, sigMRF(1:NtoPlot)-sigEPG(1:NtoPlot), 'r-o'), hold on
plot(1:NtoPlot, zeros(1,NtoPlot), 'b')
grid on
xlabel('TR block'); 
ylabel('MRF_{Irina} - EPG_{Irina}');
axis square

% % MRF vs EPG Matthias
subplot(3,3,8)
stem(1:NtoPlot, sigMRF(1:NtoPlot)-sigMat(1:NtoPlot), 'r-o'), hold on
plot(1:NtoPlot, zeros(1,NtoPlot), 'b')
grid on
xlabel('TR block'); 
ylabel('MRF_{Irina} - EPG_{Matthias}');
title({'Difference between Signals Comparison',''})
axis square

% % EPG-Irina vs EPG Matthias
subplot(3,3,9)
stem(1:NtoPlot, sigEPG(1:NtoPlot)-sigMat(1:NtoPlot), 'r-o'), hold on
plot(1:NtoPlot, zeros(1,NtoPlot), 'b')
grid on
xlabel('TR block'); 
ylabel('EPG_{Irina} - EPG_{Matthias}');
axis square


%%
% Comparison with analytical solution

[Mminus, Mplus] = steadyStateMagnetisationOfbSSFP(T1, T2, ...
                                           TRsEPG(2), deg2rad(FAsEPG), 0);
% Relax for TE
Mplus = Mplus * exp(-TEsEPG(2)/T2);

% Analytical signal
SigA = abs(Mplus(1) + 1i.*Mplus(2));

fprintf('-------------\nVerify simulation with analytical solution');
fprintf('\n\nThe signal at steady state for:\n');
fprintf('EPG          is: %f \n'  , sigEPG(end));
fprintf('MRF-Irina    is: %f \n'  , sigMRF(end));
fprintf('Analytically is: %f \n\n', SigA);


%%
%
% % % % % My old School EPG
% for i = 1:Nro
%     phaseAngle = pha(i);
%     flipAngle  = fa(i);
%     TRdiscr    = TR(i)/4;
%     
%     % % From RF Pulse to a quarter of TR where you sample 
%     [Fb, Zb] = epg(phaseAngle, flipAngle, Fb, Zb, TRdiscr, T1, T2, M0, -flagDephase);
%     
%     % % A quarter of TR to Readout
%     [Fb, Zb] = epg(0, 0, Fb, Zb, TRdiscr, T1, T2, M0,  flagDephase);
%     
%     % % Store F0 states
%     F0index = floor(size(Fb,1)/2)+1;
%     F0statesI(i) = Fb(F0index); %real(Fb(F0index))+imag(Fb(F0index));
%     
%     % % From half TR to 3/4 TR
%     [Fb, Zb] = epg(0, 0, Fb, Zb, TRdiscr, T1, T2, M0,  flagDephase);
%     
%     % % From 3/4 TR to end of TR
%     [Fb, Zb] = epg(0, 0, Fb, Zb, TRdiscr, T1, T2, M0, -flagDephase);
%     
% end

