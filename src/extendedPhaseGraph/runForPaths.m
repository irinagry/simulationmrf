% % IRINA GRIGORESCU
% % This script is to be run when you want to add paths needed for this 
% % folder
% % Andt to start from a clean slate


% Addpaths for EPG comparison
addpath(genpath('~/OneDrive - University College London/Work/Simulation/EPG/'))

% Addpaths for helpers
addpath(genpath('../helpers/'))

% Addpaths for preprocess
addpath(genpath('../preprocess/'))

% Addpaths for algs
addpath(genpath('../algs/'))

% Addpaths for exchanged to compare with analytical signal
addpath(genpath('../exchanged/'))
