function [FstatesAfter, ZstatesAfter] = ...
    epg(phi, alpha, Fstates, Zstates, TR, T1, T2, M0, flagDephase)
% IRINA GRIGORESCU
% DATE CRAETED: 25-06-2017
% DATE UPDATED: 25-06-2017
% Extended Phase Graphs: Dephasing, RF Pulses and Echoes - Pure and
% Simple, Matthias Weigel
% 
% INPUT:
%     phi     = phase angle of RF PULSE (about z axis) (in deg)
%     alpha   = flip angle of RF PULSE (about x axis)  (in deg)
%     Fstates = a column matrix of F states
%               as seen in the paper eq [27a] for time point t_x
%               (Before the N-th RF Pulse there will be 2N-1 Fstates)
%     Zstates = a column matrix of Z states
%               as seen in the paper eq [27b] for time point t_x
%               (Before the N-th RF Pulse there will be 2N-1 Zstates)
%     TR = repetition time of block (or time of dephasing)
%     T1 = relaxation time T1
%     T2 = relaxation time T2
%     M0 = echilibrium magnetization
%     flagDephase = 1/-1 for positive/negative dephasing
% 
% 
% An EPG step does:
% 1. RF pulse as T matrix operator on the states given as input
% 2. Dephasing as S operator on the states after the RF pulse
% 3. Relaxation as E matrix operator on the states after dephasing
%    and relaxation in the z direction of k=0 states
% 


[m, ~]  = size(Fstates); % 2N-1 = m => N = (m+1)/2
phi   = deg2rad(phi);
alpha = deg2rad(alpha);

% % % % % Create STATE MATRIX
% OmegaBefore = [ F+0   F+1   F+2  ... 
%                 F-0*  F-1*  F-2* ...
%                 Z0    Z1    Z2  ... ]
OmegaBefore = [      Fstates(floor(m/2)+1:-1:1).' ; ... 
                conj(Fstates(floor(m/2)+1:end)).' ; ... 
                     Zstates(floor(m/2)+1:-1:1).'];

% % % % % Apply RF PULSE
% OmegaAfter = T(phi,alpha) OmegaBefore 
% 
%
OmegaAfter = Tmatrix(phi,alpha) * OmegaBefore;

% % % % % Dephase
% Omega After = S * OmegaAfter
% with different direction if it's a positive or negative gradient
OmegaAfter = [OmegaAfter zeros(3,1)];
if (flagDephase == 1)
    % F+2 <= F+1 <= F+0
    OmegaAfter(1, end:-1:2) = OmegaAfter(1, end-1:-1:1);
    % F-0 <= F-1 <= F-2
    OmegaAfter(2,  1:end-1) = OmegaAfter(2, 2:end);
    % Correct for F+0 to be as F-0
    OmegaAfter(1,1)         = OmegaAfter(2,1);
else
    % F-0 => F-1 => F-2
    OmegaAfter(2, end:-1:2) = OmegaAfter(2, end-1:-1:1);
    % F+2 => F+1 => F+0
    OmegaAfter(1,  1:end-1) = OmegaAfter(1, 2:end);
    % Correct for F+0 to be as F-0
    OmegaAfter(2,1)         = OmegaAfter(1,1);
end

% % % % % Relax
% OmegaAfter = E * OmegaAfter for k<>0
% OmegaAfter = E * OmegaAfter + [0 0 M0(1-E1)]'  for k==0
OmegaAfter      = Drel( TR, T1, T2) * OmegaAfter; 
OmegaAfter(:,1) = Drelz(TR, T1, M0) + OmegaAfter(:, 1); % k = 0 states

% % % % % Return Fstates and Zstates
% % % NOTE: be careful, irina, as ' means conjugate transpose
FstatesAfter = [OmegaAfter(1, end:-1:1) OmegaAfter(2, 2:end)].'; 
ZstatesAfter = [OmegaAfter(3, end:-1:1) OmegaAfter(3, 2:end)].';

% % % % % Make nice zeroes
% Erase some float point accuracy errors
FstatesAfter(abs(FstatesAfter)<eps*1e3) = 0;
ZstatesAfter(abs(ZstatesAfter)<eps*1e3) = 0;


end