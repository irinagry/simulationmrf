% IRINA GRIGORESCU
% Date created: 24-08-2017
% Date updated: 24-08-2017
%
% This script is to play with the phantom signals and FISP dictionarys
% NOTE: This is an Older script. Use the other ones.

% Preprocessing steps
clear all;
close all;
clc;

% Add needed paths:
run('runForPaths');

% Create dictionary from scratch (1) or 
%              load it from file (0)
toCreate = 1;
toSave   = 0;

%%
% Load the phantom values
filename = ['/Users/irina/OneDrive - University College London/Work/', ...
            'MRF/MaryiaDonevaMRF_ForUCL/NIFTI/', ...
            'Fipri_2D_best_slen500_cart_full_1x1x3mm_fp_.nii.gz'];
        
iniData = load_nii(filename, ...
              [], [], [], [], [], 0.5);
              
% Get information about the file 
dataSize = size(iniData.img);

% Create absolute value image
dataReal = iniData.img(:,:,:,  1:500);
dataImag = iniData.img(:,:,:,501:end);
dataAbs  = abs(dataReal + 1i.*dataImag);
maxAbs   = max(max(max(max(dataAbs))));

%%
% The MRF Dictionary for the MRF-FISP paper
N = 500;

% MATERIAL PROPERTIES
tic
disp('Creating Material Properties');
materialProperties = retrieveMaterialProperties(0, 0, struct);

% Workaround:
materialProperties.T1 = 1000;%, 1200, 1300]; %(1000: 100: 5000);
materialProperties.T2 =  100; %(   1:   5:  200);

% Retrieve set of material properties in a (3xM) form
[materialTuples, M] = createSetOfTuples(materialProperties);
materialTuples = materialTuples(1:2, :);
toc
disp('DONE with Creating Material Properties');

% SEQUENCE PROPERTIES
tic
% disp('Creating Sequence Properties');
% sequenceProperties = retrieveSequenceProperties(N, 1, ...
%                      0, struct);
mrfSequenceProperties = ...
          csvread(['/Users/irina/OneDrive - University College ', ...
                   'London/Work/MRF/MaryiaDonevaMRF_ForUCL/', ...
                   'best_slen500_TE6_TR15.csv'], 13);
FAs  = [180; mrfSequenceProperties(1:N,1)];
PhAs = [  0; mrfSequenceProperties(1:N,3)];
TRs  = [ 15; mrfSequenceProperties(1:N,2)];
TEs  = [  6; mrfSequenceProperties(1:N,4)];
N = N + 1;
toc
disp('DONE with Creating Sequence Properties');

%%
% EPG - MRF-FISP
F0states = zeros(M, N);

if toCreate == 1
    tic;
    percDone = 5;
    fprintf('\n\nStarting dictionary creation (FISP)\n');
    
    for i = 1:M
        T1 = materialTuples(1,i);
        T2 = materialTuples(2,i);

        [ChiF, ChiZ] = epgMatrix(N, PhAs, FAs, TRs, ...
                                   T1, ...    % T_1
                                   T2, ...    % T_2
                                   1, 0);     % M0, flagDephase

        F0states(i, :) = ChiF(N, :) .* exp(-TEs ./ T2).';

        if (mod(i, ceil(M/20)) == 0)
            t = toc;
            fprintf('%3d %% done in %3.2f seconds ... \n', percDone, t);
            percDone = percDone + 5;
        end
    end
    t = toc;
    disp(['Dictionary generation with EPG took ', num2str(t), ' s']);
    
    if toSave == 1
        save('epgFISPdictionary', 'F0states');
    end
else
    varToLoad = load('epgFISPdictionaryIR_WITHrelaxafterIR.mat');
    F0states = varToLoad.F0states(:,1:N);
end

%%
% % % My Dictionary generation - MRF bSSFP

fprintf('\n\nStarting dictionary creation (bSSFP)\n');
[~, ~, materialTuplesBSSFP, ~, ~, ...
 signalDictionaryMRF, ~] = ...
    func_generateCustomSignals(N, FAs, PhAs, TRs, TEs, ...
                    materialProperties.T1, ...     %T1
                    materialProperties.T2, ...     %T2
                    materialProperties.offRes, ... %df
                    0, 5, 3, 1:M, 1); % flag, seq, mat, indices, ir
 

%%
% Plot
sigEPG = abs(F0states(:, 2:end));

idx = 1:200:M;
legendText = {};

figure

for i = 1:size(idx,2)
    plot(1:N-1, sigEPG(idx(i), 1:end))
    hold on

    legendText{i} = ['T_1=',  num2str(materialTuples(1,idx(i))), ...
                     ' T_2=', num2str(materialTuples(2,idx(i)))];
end

legend(legendText);

xlabel('# timepoints');
ylabel('signal magnitude');



%%
% Normalize signals from MRF-FISP dictionary
F0statesReIm = [ real(F0states), imag(F0states)];

[dictionaryMRFFISPNorm2Ch, dictionaryMRFFISPNorm] = ...
        normalizeSignals(F0statesReIm);
    
sigMRFFISPAbsNorm = abs(dictionaryMRFFISPNorm);

% Normalize a signal from phantom study
x = 121; %96; %63; %91; %144; %134; %95; 
y = 115; %92; %69; %47; %182; %141; %91;
sigToMatch = squeeze(cat(4, dataReal(x,y,1,1:N), dataImag(x,y,1,1:N)));

if (ndims(sigToMatch) == 2) && (size(sigToMatch,1) == 2*N)
    disp('yes')
    sigToMatch = sigToMatch.';
end

[sigToMatchNorm2Ch, sigToMatchNorm] = ...
        normalizeSignals(sigToMatch);
    
sigToMatchNormAbs = abs(sigToMatchNorm);
    
% Discard very noisy data
sigMRFFISPAbsNorm = [sigMRFFISPAbsNorm(:,  1:124), ...
                     sigMRFFISPAbsNorm(:,  145:end)];
sigToMatchNormAbs = [sigToMatchNormAbs(:,  1:124), ...
                     sigToMatchNormAbs(:,  145:end)];
signalDictionaryMRF = [signalDictionaryMRF(:, 1:124), ...
                       signalDictionaryMRF(:, 145:end)];

% Match with dictionary (FISP)
[indicesMatchF, matTuplesF] = matchingWithAbs(sigMRFFISPAbsNorm, ...
                                              sigToMatchNormAbs, ...
                                              materialTuples, 0);

% Match with dictionary (bSSFP)
[indicesMatchB, matTuplesB] = matchingWithAbs(signalDictionaryMRF, ...
                                              sigToMatchNormAbs, ...
                                              materialTuples, 0);
    
%%
% Look at the phantom vs dictionary
figure

plot(1:size(sigToMatchNormAbs,2), sigToMatchNormAbs) % phantom signal
hold on

matTuplesF
indicesToPlot = 1; %find(materialTuples(1,:)==1190);

for i = 1:size(indicesToPlot,2)
    gg = plot(1:size(sigMRFFISPAbsNorm,2), ...
              sigMRFFISPAbsNorm(indicesToPlot(i), :), ...
              '--');   % dictionary match FISP
    pause(0.2)
    delete(gg)
    title(['Index = ', num2str(i), ...
           ' T_1 = ', num2str(materialTuples(1,indicesToPlot(i))), 'ms T_2 = ', ...
            num2str(materialTuples(2,indicesToPlot(i))), 'ms'])
end
                                                   
plot(1:size(sigMRFFISPAbsNorm,2), ...
            sigMRFFISPAbsNorm(indicesMatchF, :), ...
            '--');   % dictionary match FISP
        
plot(1:size(signalDictionaryMRF,2), signalDictionaryMRF(indicesMatchB, :)) 
                                                   % dictionary match bSSFP

title(['T_1 = ', num2str(matTuplesF.T1), 'ms T_2 = ', ...
       num2str(matTuplesF.T2), 'ms'])
legend('phantom', 'epg', 'me')







