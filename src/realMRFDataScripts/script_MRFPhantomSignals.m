% IRINA GRIGORESCU
% Date created: 30-08-2017
% Date updated: 30-08-2017
%
% This script is to have a look at the MRF data from the phantom study

% Preprocessing steps
clear all;
close all;
clc;

% Add needed paths:
run('runForPaths');
addpath(genpath('~/Tools/MatlabStuff/'));

% If you want to plot with montage (0 - no)
% These parameters are used at the end of the script to plot some data to
% have a look at it qualitatively
toPlotMontage = 0;               % plots the real data images with montage
toPlotOneSignal = 0;             % plots one signal at a given position
toPlotAllImagesNormalised = 0;   % 

% File name dictionary
filenameDict = ['/Users/irina/OneDrive - University College London/',...
    'Work/Simulation/simulationMRF/src/data_EPGDictionary/', ...
             'epgdictionary-FISP-IR-sequenceListMaryia.mat'];

% Choose a position from where to take a signal
% Take these values from fslview
x = 143;
y = 177;

% Noisy data is between:
noisyL = 124; noisyR = 145;


%% LOAD DATA
% Load phantom values
N = 500; % number of pulses

% Filename of phantom data
filename = ['/Users/irina/OneDrive - University College London/Work/', ...
            'MRF/mrf_image_series.mat'];

% % Using Data directly from Maryia
%['/Users/irina/OneDrive - University College London/Work/', ...
%            'MRF/MaryiaDonevaMRF_ForUCL/NIFTI/', ...
%            'Fipri_2D_best_slen500_cart_full_1x1x3mm_fp_.nii.gz'];
%         
% % Load file 
% iniData = load_nii(filename, ...
%               [], [], [], [], [], 0.5);
  
% Load Data
iniData = load(filename);


% Get information about the file 
dataSize = size(iniData.mrf_image_series);

% % % Make it be like in the scanner
% % for i = 1:N
% %     iniData.mrf_image_series(:,:,i) = ...
% %         rot90(flipud(squeeze(iniData.mrf_image_series(:,:,i))));
% % end

%
% Create absolute value image
dataCplx = iniData.mrf_image_series;
dataReal = real(iniData.mrf_image_series); % Real 
dataImag = imag(iniData.mrf_image_series); % Imag 
dataAbs  = abs(iniData.mrf_image_series);
maxAbs   = max(max(max(max(dataAbs))));

% Get size of image
[szx szy szts] = size(iniData.mrf_image_series);
clear iniData;

%% SIGNALS FROM PHANTOM
% % % % % % % % % % % % Signals from PHANTOM 
% Normalize all signals from phantom

% Reshape signals into szx*szy x 2N matrix
dataRealReshaped = reshape(dataReal, [szx*szy, szts]);
dataImagReshaped = reshape(dataImag, [szx*szy, szts]);
clear dataReal;
clear dataImag;
phantomSignals = [dataRealReshaped , dataImagReshaped]; % real followed 
                                                        % by imaginary
clear dataRealReshaped;
clear dataImagReshaped;
phantomSignalsComplex = phantomSignals(:, 1:N) + ...
                  1i .* phantomSignals(:, N+1:end);

% Normalize signals to norm(signal) = 1
[phantomSignalsNorm2Ch, phantomSignalsNorm] = ...
        normalizeSignals(phantomSignals);
    
% Reshape signals back into szx x szy x N matrix
phantomSignalsNorm2D    = reshape(phantomSignalsNorm, [szx, szy, N]);
phantomSignalsNorm2Ch2D = reshape(phantomSignalsNorm2Ch, [szx, szy, 2*N]);
clear phantomSignalsNorm2Ch;

%% SIGNALS FROM DICTIONARY
% Get signals from FISP dictionary
% Load data from dictionary file 
varToLoad = load(filenameDict);

% F0states - Discard first value which is after IR pulse
F0states = varToLoad.F0states(:, 2:end); 
% Material tuples
materialTuples = varToLoad.materialTuples;
M = size(materialTuples, 2);

% Create 2 channel data (real + imag)
F0statesReIm = [ real(F0states), imag(F0states)];

% Normalize signals
[dictionaryMRFFISPNorm2Ch, dictionaryMRFFISPNorm] = ...
        normalizeSignals(F0statesReIm);

% Calculate absolute signal
sigMRFFISPAbsNorm = abs(dictionaryMRFFISPNorm(:, 1:end));
%                     [ abs(dictionaryMRFFISPNorm(:, 1:end)) ]; %, ...
%                       abs(dictionaryMRFFISPNorm(:, noisyR:end))] ;

%% COMPARISON WITH MARYIA'S DICTIONARY
load('~/OneDrive - University College London/Work/MRF/mrf_signals_t1_100_t2_10_t1_1500_t2_100.mat');

figure
subplot(2,2,1)
% T1 = 100, T2 = 10 Signal comparison
plot(1:500, imag(mrf_signals(:,1)), 'b'), hold on
plot(1:500, imag(dictionaryMRFFISPNorm(81,:)).', 'r--') 
xlabel('#timepoints'); ylabel('signal normalised a.u.');
legend('Mariya', 'Me')
title('T_1 = 100ms, T_2 = 10ms')
% T1 = 100, T2 = 10 Signal difference
subplot(2,2,2)
plot(1:500, imag(mrf_signals(:,1)) - ...
            imag(dictionaryMRFFISPNorm(81,:)).', 'b')
xlabel('#timepoints'); ylabel('signals difference');

subplot(2,2,3)
% T1 = 1500, T2 = 100 Signal comparison
plot(1:500, imag(mrf_signals(:,2)), 'b'), hold on
plot(1:500, imag(dictionaryMRFFISPNorm(9439,:)).', 'r--') 
xlabel('#timepoints'); ylabel('signal normalised a.u.');
legend('Mariya', 'Me')
title('T_1 = 1500ms, T_2 = 100ms')
% T1 = 1500, T2 = 100 Signal difference
subplot(2,2,4)
plot(1:500, imag(mrf_signals(:,2)) - ...
            imag(dictionaryMRFFISPNorm(9439,:)).', 'b')
xlabel('#timepoints'); ylabel('signals difference');

%% MATCHING
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(phantomSignals);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
[valuesOfMatch1, indicesToMatch1, matTuplesMatched1] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
[valuesOfMatch2, indicesToMatch2, matTuplesMatched2] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
[valuesOfMatch3, indicesToMatch3, matTuplesMatched3] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
[valuesOfMatch4, indicesToMatch4, matTuplesMatched4] = ...
                        matching(F0statesReIm, ...
                                 phantomSignals(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

% Merge results:
valuesOfMatch  = [valuesOfMatch1, valuesOfMatch2, ...
                  valuesOfMatch3, valuesOfMatch4];
indicesToMatch = [indicesToMatch1, indicesToMatch2, ...
                  indicesToMatch3, indicesToMatch4]; 
matTuplesMatched = matTuplesMatched1;
matTuplesMatched.T1 = [matTuplesMatched1.T1, matTuplesMatched2.T1, ...
                       matTuplesMatched3.T1, matTuplesMatched4.T1];
matTuplesMatched.T2 = [matTuplesMatched1.T2, matTuplesMatched2.T2, ...
                       matTuplesMatched3.T2, matTuplesMatched4.T2];
                   
%% PLOT MAPS
mapT1 = reshape(matTuplesMatched.T1, [szx, szy]);
mapT2 = reshape(matTuplesMatched.T2, [szx, szy]);
mapMatchValues = reshape(valuesOfMatch, [szx, szy]);

%%
maskA = createCirclesMask(ones(240,240), [111,119], 100);

% % % % Plot maps of T1 and T2
figure('Position', [10,10,1400,1000])
subplot(1,2,1)
imagesc(fliplr(mapT1).*maskA)
axis off, axis square
colormap hot
colorbar, caxis([0 2000])
title('T_1 map')

subplot(1,2,2)
imagesc(fliplr(mapT2).*maskA)
axis off, axis square
colormap hot
colorbar, caxis([0 500])
title('T_2 map')

%%
% % % % Values of matching
figure
imagesc(fliplr(mapMatchValues).*maskA)
axis off, axis square
colormap hot
colorbar, caxis([0.9 1])
title('Values of matching')





%% Plot acquired vs matched signals for a randomly chosen voxel
rndIDX = 34945;

figure
temp = squeeze(phantomSignalsNorm(:,1));
temp(rndIDX) = 1;
temp = reshape(temp, [240, 240]);
imagesc(abs(temp));


figure('Position', [10,10,800,1000])

% % % % SIGNAL FROM DATA
plot(0:N-1, abs(phantomSignalsNorm(rndIDX,:)), ...
     'LineWidth',2)
hold on
% % % % SIGNAL FROM DICTIONARY
plot(0:N-1, abs(dictionaryMRFFISPNorm(indicesToMatch(rndIDX),:)),'--', ...
     'LineWidth',2)
title(['Best Score: ' , num2str(valuesOfMatch(rndIDX))])
xlabel('#image'); ylabel('normalized signal intensity (a.u.)');
legend('phantom','dictionary')


%% PLOT ACQUIRED VS MATCHED SIGNALS
posx = szy-y; posy = szx-x;


figure('Position', [10,10,800,1000])
% Best match
% subplot(3,1,1)
[bestMatchVal, bestMatchIdx] = max(reshape(mapMatchValues, [240*240,1]));
% % % % SIGNAL FROM DATA
plot(0:N-1, abs(phantomSignalsNorm(bestMatchIdx,:)), ...
     'LineWidth',2)
hold on
% % % % SIGNAL FROM DICTIONARY
plot(0:N-1, abs(dictionaryMRFFISPNorm(indicesToMatch(bestMatchIdx),:)),'--', ...
     'LineWidth',2)
title(['Best Score: ' , num2str(valuesOfMatch(bestMatchIdx))])
xlabel('#image'); ylabel('normalized signal intensity (a.u.)');
legend('phantom','dictionary')


%%
% One random chosen signal
subplot(3,1,2)
posReshaped = (posy-1) * szy + posx;
% % % % SIGNAL FROM DATA
plot(0:N-1, abs(phantomSignalsNorm(posReshaped,:)))          
hold on
% % % % SIGNAL FROM DICTIONARY
plot(0:N-1, abs(dictionaryMRFFISPNorm(indicesToMatch(posReshaped),:)))
title(['Random Score: ' , num2str(valuesOfMatch(posReshaped))])
xlabel('#image'); ylabel('normalized signal intensity (a.u.)');
legend('phantom','dictionary')

% Worst match
subplot(3,1,3)
[worstMatchVal, worstMatchIdx] = min(reshape(mapMatchValues, [240*240,1]));
% % % % SIGNAL FROM DATA
plot(0:N-1, abs(phantomSignalsNorm(worstMatchIdx,:)))
hold on
% % % % SIGNAL FROM DICTIONARY
plot(0:N-1, abs(dictionaryMRFFISPNorm(indicesToMatch(worstMatchIdx),:)))
title(['Worst Score: ' , num2str(valuesOfMatch(worstMatchIdx))])
xlabel('#image'); ylabel('normalized signal intensity (a.u.)');
legend('phantom','dictionary')

%%
% % % % % % % Find where the index is coming from:
% Where in the dataset this index is:
temp = phantomSignalsNorm;
temp(worstMatchIdx,:) = -1;
figure, imagesc(fliplr(abs(reshape(temp(:,1), [szx,szy]))))



% mapPD = zeros(szx*szy, 1);
% for i = 1:szx*szy
%     mapPD(i) = phantomSignals(i,:) * ...
%                pinv(F0statesReIm(indicesToMatch(i),:));
% end
% mapPD = reshape(mapPD, [szx, szy]);

% subplot(1,3,3)
% imagesc(abs(mapPD)./max(max(abs(mapPD))))
% axis off, axis square
% colormap hot
% colorbar, caxis([0 1])
% title('PD map')

% % % % % % % 
% % % % % % ABSOLUTE MATCHING
% % %%
% % fprintf(' -----> Start matching with abs.\n');
% % tic;
% % [valuesOfMatchA, indicesToMatchA, matTuplesMatchedA] = ...
% %                         matchingWithAbs(sigMRFFISPAbsNorm, ...
% %                                  abs(phantomSignalsNorm), ...
% %                                  materialTuples, 0);
% % t=toc;
% % fprintf(' -----> Done with matching. Took %3.2f seconds\n', t);
% % 
% % %%
% % mapT1A = reshape(matTuplesMatchedA.T1, [szx, szy]);
% % mapT2A = reshape(matTuplesMatchedA.T2, [szx, szy]);
% % mapMatchValuesA = reshape(valuesOfMatchA, [szx, szy]);
% % 
% % figure('Position', [10,10,1400,1000])
% % subplot(1,3,1)
% % imagesc(fliplr(mapT1A))
% % axis off, axis square
% % colormap hot
% % colorbar, caxis([0 2000])
% % title('T_1 map')
% % 
% % subplot(1,3,2)
% % imagesc(fliplr(mapT2A))
% % axis off, axis square
% % colormap hot
% % colorbar, caxis([0 480])
% % title('T_2 map')
% % 
% % subplot(1,3,3)
% % imagesc(fliplr(mapMatchValuesA))
% % axis off, axis square
% % colormap hot
% % colorbar, caxis([0.6 1])
% % title('Values of matching')

% % %% PLOTTING SOME dictionary signals
% % % % % % 0)
% % % Find the indices of certain T1/T2 pairs
% % % I want a range of T1s
% % T1chosen = [800, 1300, 3800, 400];
% % % and a fixed T2
% % T2chosen = [ 110, 80, 1400, 70];
% % 
% % 
% % % The indices of the dictionary fingerprints will be stored here:
% % indices0 = zeros(1,4);
% % for i = 1:size(indices0,2)
% %     tmp = find(materialTuples(1,:) == T1chosen(i) & ...
% %                        materialTuples(2,:) == T2chosen(i));
% % 	indices0(i) = tmp(1);     
% % end
% % 
% % % % Plot the signals
% % figure
% % legendText = {'WM', 'GM', 'CSF', 'Fat'};
% % for i = 1:size(indices0,2)
% %     plot(1:N, abs(F0states(indices0(i), :)), 'LineWidth', 2)
% %     hold on
% % %     grid on
% % end
% % legend(legendText, 'Location', 'northeast', 'Orientation', 'vertical');
% % xlabel('TR index'); ylabel('signal intensity');
% % xlim([0 500]);% ylim([0 1])

%%


% % % % 1)
% I want a range of T1s
T1chosen = [500, 1000, 1500, 2000];
% and a fixed T2
T2chosen =  300;

% The indices of the dictionary fingerprints will be stored here:
indices1 = zeros(size(T1chosen));
for i = 1:size(T1chosen,2)
    temp = find(materialTuples(1,:) == T1chosen(i) & ...
                       materialTuples(2,:) == T2chosen);
	indices1(i) = temp(1);    
end

% % Plot the signals
figure
subplot(2,1,2)
legendText = cell(1,size(indices1,2));
for i = 1:size(indices1,2)
    p1 = plot(1:N, abs(F0states(indices1(i), :)), 'LineWidth', 1);
    p1.Color(4) = 1-(1/(i+1));
    hold on
    grid on
    legendText{i} = ['T_1 = ', num2str(T1chosen(i)), 'ms'];
end
legend(legendText, 'Location', 'northeast', 'Orientation', 'vertical');
xlabel('TR index'); ylabel('signal intensity');
title(['T_2 = ',num2str(T2chosen),'ms']);

% % % % 2)
% I want a fixed T1
T1chosen = 400;
% and a range of T2s
T2chosen = [50, 100, 150, 200];%, 250, 300];

% The indices of the dictionary fingerprints will be stored here:
indices2 = zeros(size(T2chosen));
for i = 1:size(T2chosen,2)
    index = find(materialTuples(1,:) == T1chosen & ...
                 materialTuples(2,:) == T2chosen(i));
	indices2(i) = index(1);
end

% % Plot the signals
subplot(2,1,1)
legendText = cell(1,size(indices2,2));
for i = 1:size(indices2,2)
    p2 = plot(1:N, abs(F0states(indices2(i), :)), 'LineWidth', 1.1);
    p2.Color(4) = 1-(1/(i+1)); 
    tmp = p2.Color(1);
    p2.Color(1) = p2.Color(2);
    p2.Color(2) = p2.Color(3);
    p2.Color(3) = tmp;
    hold on
    grid on
    legendText{i} = ['T_2 = ', num2str(T2chosen(i)), 'ms'];
end
legend(legendText, 'Location', 'northeast', 'Orientation', 'vertical');
xlabel('TR index'); ylabel('signal intensity');
title(['T_1 = ',num2str(T1chosen),'ms']);

%% PLOTTING TO HAVE A QULITATIVE LOOK AT THE DATA
% Plot one signal at a given position
if toPlotOneSignal == 0
    % Plot one signal
    figure('Position', [10,10,1400,1000])
    plot(0:499, squeeze(dataAbs(szy-y, szx-x, :)));
    xlabel('#timepoints'); ylabel('signal (a.u.)');
    grid on
    
    % Plot it's corresponsing position in the image
    temp = squeeze(dataAbs(:,:,1));
    temp(szy-y, szx-x) = 0;
    figure('Position', [10,10,1400,1000])
    imagesc(temp)
    caxis([0 maxAbs])
    colormap gray
    axis square
    axis off
    colorbar
end
%%
if toPlotOneSignal == 0
    % Plot a series of images
    figure
    xImage = [-0.5 0.5; -0.5 0.5];   %# The x data for the image corners
    yImage = [ 0   0  ;  0   0  ];   %# The y data for the image corners
    zImage = [0.5 0.5; -0.5 -0.5];   %# The z data for the image corners
    indices = [10 200 500] ; %10 11 12 200 300 400 500];
    for i = indices
        temp = fliplr(rot90(squeeze(dataAbs(:,:,i)),-1));
        surf(xImage,yImage,zImage,...    %# Plot the surface
             'CData', temp, ...
             'FaceColor', 'texturemap'); hold on
        if i == indices(end-1)
            xImage = xImage + 1.2;
        else
            xImage = xImage + 1.05;
        end
    end
    view(42,6);
    caxis([0 maxAbs])
    colormap gray
    axis equal
    axis off
    
end

% Plot all images from 1 to N
if toPlotAllImagesNormalised == 1
    figure('Position', [10,10,1400,1000])
    for i = 1%:5:N
        %imagesc(squeeze(abs(phantomSignalsNorm2D(:,:,i))));
        imagesc(fliplr(squeeze(dataAbs(:,:,i))))
        caxis([0 maxAbs])
        title(['Image no ', num2str(i)]);
        colormap gray
        axis square
        axis off
        colorbar
        drawnow
    end
end

% Plot a few images with montage 
% (needs normalization)
if toPlotMontage == 1
    figure,
    montage(dataAbs(:,:,1:150), 'Size', [10,15]);
    caxis([0 maxAbs])
    title('First 150 images')
end


% % % % %% Calculate SSD between a chosen phantom signal and all dictionary values
% % % % onePhantomSignalAbs = ...
% % % %     [ (squeeze(phantomSignalsNorm2D(szy-y, szx-x, 1:end))) ]; %; ...
% % % % %       (squeeze(phantomSignalsNorm2D(szy-y, szx-x, noisyR:end))) ];
% % % % 
% % % % SSDforAll  = zeros(M,1);
% % % % dotProdAll = zeros(M,1);
% % % % for i = 1:M
% % % %     SSDforAll(i) = sum((onePhantomSignalAbs.' - dictionaryMRFFISPNorm(i,:)).^2);
% % % %     dotProdAll(i) = dictionaryMRFFISPNorm(i,:) * onePhantomSignalAbs;
% % % % end
% % % % 
% % % % NtoPlot = size(onePhantomSignalAbs,1);
% % % % 
% % % % figure
% % % % subplot(2,2,1)
% % % % scatter(1:M, real(SSDforAll));
% % % % title('SSD')
% % % % 
% % % % subplot(2,2,2)
% % % % scatter(1:M, real(dotProdAll));
% % % % title('dot-product')
% % % % 
% % % % subplot(2,2,[3,4])
% % % % plot(1:NtoPlot, abs(onePhantomSignalAbs)); hold on
% % % % plot(1:NtoPlot, abs(sigMRFFISPAbsNorm(676,:)));
% % % % title(['T_1 = ', num2str(materialTuples(1,676)), ...
% % % %       ' T_2 = ', num2str(materialTuples(2,676))])
% % % % 



% % % Some old stuff
% % % hold on
% % % for i = 1:100:M
% % %     plot(0:499, sigMRFFISPAbsNorm(i,:), 'k-', ...
% % %         'LineWidth', 0.2);
% % %     alpha(0.5)
% % %     drawnow
% % % end
% % % plot(0:499, squeeze(abs(phantomSignalsNorm2D(szy-y, szx-x, :))), ...
% % %         'LineWidth', 2);
% % % xlabel('timeseries'); ylabel('');
% % % 
% % % %%
% % % % % % % % % % 
% % % 
% % % T1 = [1000, 2000, 3000];
% % % T2 =  100;









