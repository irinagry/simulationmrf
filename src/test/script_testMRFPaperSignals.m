% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 28-03-2017
% % % % Date updated: 25-06-2017
% % % % 
% % % % This script generates a dictionary based on some 
% % % % given values for T1, T2 and off-resonance frequencies
% % % % and plots the signals of known tissue types
% % % % 


% % Preprocessing steps
clear vars; 
close all; 
clc;

% % Add paths:
run('runForPaths.m');

% Run cofig file to have the parameters of interest at hand
configParams = returnPredefinedParameters();

%% Generate dictionary for a predefined set of tissue properties
N = 1000;      % timepoints for FAs and TRs of the sequence

% % % % Nature paper 
% % FLAGPLOT = 1; % Flag for plotting sequence parameters and material prop
% % SEQFLAG  = 3; % 3 = sinusoidal FAs and perlin TRs
% % MATFLAG  = 1; % 1 = The entire range as per MRF paper
% % 
% % % The dictionary is an array of (MxN double)
% % tic
% % disp(['Generating dictionary for ', num2str(N), ' events']);
% % [materialProperties, sequenceProperties, ...
% %     materialTuples, M, ...
% %     dictionaryMRF] = ...
% %                  simulateMRF(N, FLAGPLOT, ...
% %                     SEQFLAG, struct, ...
% %                     MATFLAG, struct, 1);
% % toc

% % % % Custom
% % % % 
% % % % Sequence and Material parameters
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 3; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial
% % % % % % % %

% % % % Custom Material
customMaterial.T1     = [240 250 260 600 940 950 5000];
customMaterial.T2     = [ 50  60  80 100 800];
customMaterial.offRes = [0 0.01 -0.001 -0.03 0.04];

tic
[materialProperties, sequenceProperties, ...
    materialTuples, M, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, ...
                 SEQFLAG, struct, ...
                 MATFLAG, customMaterial, 1);
toc


% Create dictionary of signal values (M x N)
tic
signalDictionaryMRF = abs(      squeeze(dictionaryMRF(1:M, :, 1)) + ...
                          1i .* squeeze(dictionaryMRF(1:M, :, 2)));
toc


%% Plot data 
figure

% 1. Signal for all combinations
% % tuples on y-axis, signal timepoints on x-axis
subplot(2,2,2)
imagesc(signalDictionaryMRF);
xlabel('timepoints');
ylabel('[T_1, T_2, \Delta f]');
axis([1 N 1 M])
colormap jet
colorbar 

% 2. Material properties
% % See material properties space
subplot(2,2,1)
Mm = 1;
set(gcf, 'Renderer', 'OpenGL')      % speed up the rendering process
scatter3(materialTuples(1,1:Mm:M), ...
         materialTuples(2,1:Mm:M), ...
         materialTuples(3,1:Mm:M), ...
         '.'); 
shading interp
xlabel('T_1'), ylabel('T_2'), zlabel('\Delta f');
grid on


subplot(2,2,4)
imagesc(materialTuples');
ylabel('number of material properties');
% ylabel('[T_1, T_2, \Delta f]');
% axis([1 N 1 M])
set(gca, 'XTick', [1, 2, 3]);
set(gca, 'XTickLabel', {'T_1', 'T_2', '\Delta f'});
colormap jet
colorbar 

%% Find fingerprints you want to plot

colToFind = struct;
colToFind.WM    = [ 600 ; 100 ;  0];    %  WM: t1= 600, t2= 80, df=  0Hz
colToFind.WMoff = [ 600 ; 100 ; -0.03]; %  WM: t1= 600, t2= 80, df=-30Hz
colToFind.GM    = [ 950 ; 100 ;  0];    %  GM: t1= 940, t2=100, df=  0Hz
colToFind.CSF   = [5000 ; 800 ;  0];    % CSF: t1=5000, t2=800, df=  0Hz
colToFind.Fat   = [ 250 ;  50 ;  0];    % FAT: t1= 250, t2= 60, df=  0Hz

idFound = struct;
% WM
[~,indx] = ismember(materialTuples', colToFind.WM', 'rows');
idFound.WM = find(indx == 1);
% WM-off
[~,indx] = ismember(materialTuples', colToFind.WMoff', 'rows');
idFound.WMoff = find(indx == 1);
% GM
[~,indx] = ismember(materialTuples', colToFind.GM', 'rows');
idFound.GM = find(indx == 1);
% CSF
[~,indx] = ismember(materialTuples', colToFind.CSF', 'rows');
idFound.CSF = find(indx == 1);
% Fat
[~,indx] = ismember(materialTuples', colToFind.Fat', 'rows');
idFound.Fat = find(indx == 1);

clear indx


%% Plot those fingerprints
colors = {'r','m','g','y','b'};

Nn = N;
figure,

plot(1:Nn, signalDictionaryMRF(idFound.WM, 1:Nn), 'LineWidth', 0.7);%, ...
%            colors{1}); % WM
hold on
plot(1:Nn, signalDictionaryMRF(idFound.WMoff, 1:Nn), 'LineWidth', 0.7);%, ...
%            colors{2}); % WM-off
hold on
plot(1:Nn, signalDictionaryMRF(idFound.GM, 1:Nn), 'LineWidth', 0.7);%, ...
%            colors{3});  % GM
hold on
plot(1:Nn, signalDictionaryMRF(idFound.CSF, 1:Nn), 'LineWidth', 0.7);%, ...
%            colors{4});  % CSF
hold on
plot(1:Nn, signalDictionaryMRF(idFound.Fat, 1:Nn), 'LineWidth', 0.7);%, ...
%            colors{5});  % Fat
           
grid on
legend('WM','WM-off','GM','CSF','Fat');
xlabel('T_R index');
ylabel('signal intensity (a.u.)');




%% Plot one signal against the same signal with noise added

figure,

artificialSignal = signalDictionaryMRF(idFound.WM, 1:Nn) - ...
           (0.25 - (0.5).*rand(1,Nn))./4;
artificialSignal(artificialSignal<0) = 0;

plot(1:Nn, artificialSignal, 'LineWidth', 0.7);% WM+noise
hold on
plot(1:Nn, signalDictionaryMRF(idFound.WM, 1:Nn), 'LineWidth', 2);% WM

xlabel('T_R index');
ylabel('signal intensity (a.u.)');
legend('acquired signal','dictionary match');







