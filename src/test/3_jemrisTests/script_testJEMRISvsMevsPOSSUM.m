% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 12-07-2017
% % % % Date updated: 12-07-2017
% % % % 
% % % % This script tests my dictionary generation against JEMRIS
% % % % 


% % Preprocessing steps
clear vars; 
close all; 
clc;

run('runForPaths.m')

%% FILES 
% % % % % % % % % ME LOAD
% % % Load dictionary for sinusoid FAs and Perlin TRs
load(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/src/data/sequence_sinusoidFAs_perlinTRs_N1000'], ...
    'sequenceProperties', 'materialProperties', 'N' );
% % % Load constant FAs/TRs for 45/60
% load(['~/OneDrive - University College London/Work/Simulation/', ...
%      'simulationMRF/src/data/sequence_45FA_60TR_N1000'], ...
%      'sequenceProperties', 'materialProperties', 'N' );
% % % Load constant FAs/TRs for 90/60
% load(['~/OneDrive - University College London/Work/Simulation/', ...
%      'simulationMRF/src/data/sequence_90FA_60TR_N1000'], ...
%      'sequenceProperties', 'materialProperties', 'N' );
% % % % % With predefined stuff:
FA_mrf  = sequenceProperties.RF.FA;
PhA_mrf = zeros(size(FA_mrf)); %sequenceProperties.RF.PhA;
TR_mrf  = sequenceProperties.TR;
RO_mrf  = sequenceProperties.RO;
T1_mrf  = 600;
T2_mrf  =  80;
df_mrf  =   0;
FLAGPLOT = 1; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % 5 = custom, 3 = sinusoid FAs and Perlin TRs
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % N comes from the loaded file. If no loading, then please provide
    % value
NTRs = N;

% % % % % % % % % JEMRIS
fileName = 'signals_rnd1.h5';

% % % % % % % % % POSSUM
fileNameP  = ['~/simdir/outSignal/signal_rnd_1_', num2str(NTRs)];
fileNamePo = ['~/simdir/outSignal/signal_rnd_1_', num2str(NTRs), 'orig'];


%% % ME: Generate dictionary 
[materialProperties, sequenceProperties, ...
 materialTuples, M, ...
 dictionaryMRF, ...
 ~, ~] = ...
	func_generateCustomSignals(NTRs, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG);
signalDictionaryMRF = sqrt(squeeze(dictionaryMRF(1, :, 1)).^2 + ...
                           squeeze(dictionaryMRF(1, :, 2)).^2);
% save(['~/OneDrive - University College London/Work/Simulation/', ...
%      'simulationMRF/src/data/sequence_90FA_60TR_N1000'], ...
%      'sequenceProperties', 'materialProperties', 'N' );

%% % JEMRIS: Get signals from JEMRIS

signalJ = h5read(fileName, '/signal/channels/00');
signalAbsJ = abs(signalJ(1,1:end) + 1i.*signalJ(2,1:end));

%% % POSSUM: Get signals from POSSUM

signalP  = read_pulse(fileNameP);
signalPo = read_pulse(fileNamePo);

signalAbsP  =  abs( signalP(1,:) + 1i.*signalP(2,:));
signalAbsPo =  abs(signalPo(1,:) + 1i.*signalPo(2,:));


%% % Plot results against JEMRIS results
TRs = 50;


% % % % Figure 1
figHand1 = figure('Position', [10,10,1200,800]); 
% % % % Figure 2
figHand2 = figure('Position', [10,10,1200,800]); 

nLins = 7;
nCols = 4;

% % Event matrix
figure(figHand1)
subplot(nLins, nCols, [1, 4])
stem(cumsum(TR_mrf(1:TRs)),  FA_mrf(1:TRs)); hold on
title({['Sequence with: N = ', num2str(TRs), ' TR blocks'], ...
        ['FA = ', num2str(mean(FA_mrf)),  '^o PhA = ', ...
          num2str(mean(PhA_mrf)), '^o', ...
        ' TR = ', num2str(mean(TR_mrf)), ...
        'ms and RO = ', num2str(mean(RO_mrf)), ' ms' ]});
xlabel('ms'); ylabel('FA (deg)');

% % MAGNITUDE OF SIGNAL
figure(figHand1)
subplot(nLins, nCols, [ 9, 14])
% % % % JEMRIS 
plot(1:TRs, signalAbsJ(1:TRs), 'b'), hold on
% % % % MY STUFF
plot(1:TRs, signalDictionaryMRF(1, 1:TRs), 'r--*')
% % % % POSSUM
plot(1:TRs, signalAbsP(1:TRs), 'k--o'), hold on
xlabel('timepoints')
ylabel('signal magnitude')
legend('Jemris', 'Me', 'POSSUM')
title('Me vs JEMRIS vs POSSUM (Signal magnitude)');
   
% % X-Y-Z COMPONENTS OF SIGNAL
figure(figHand1)
subplot(nLins, nCols, [11, 16])
% % % % JEMRIS 
plot(1:TRs, signalJ(1,1:TRs), 'r'), hold on
plot(1:TRs, signalJ(2,1:TRs), 'k') 
plot(1:TRs, signalJ(3,1:TRs), 'b') 
% % % % MY STUFF
plot(1:TRs, squeeze(dictionaryMRF(1, 1:TRs, 1)), 'r-o'), hold on
plot(1:TRs, squeeze(dictionaryMRF(1, 1:TRs, 2)), 'k-o')
plot(1:TRs, squeeze(dictionaryMRF(1, 1:TRs, 3)), 'b-o')
xlabel('timepoints')
ylabel('M_i')
legend('M_x(Jemris)', 'M_y(Jemris)', 'M_z(Jemris)', 'M_x(Me)', 'M_y(Me)', 'M_z(Me)')
title('Me vs JEMRIS (Component-wise)')

% % DIFFERENCE BETWEEN MAGNITUDES OF SIGNALS
figure(figHand1)
subplot(nLins, nCols, [21, 26])
% % % % Difference between magnitudes
plot(1:TRs, abs(signalAbsP(1:TRs) - signalDictionaryMRF(1, 1:TRs)), 'r--o')
hold on
plot(1:TRs, abs( signalAbsP(1:TRs) - ...
                 signalAbsJ(1:TRs)), 'g-*')
plot(1:TRs, abs(signalDictionaryMRF(1, 1:TRs) - ...
                signalAbsJ(1:TRs)), 'k--*')
xlabel('timepoints')
ylabel('Absolute difference in signal magnitude')
legend('POSSUM vs Me', 'POSSUM vs JEMRIS', 'Me vs JEMRIS')
title({'Me vs JEMRIS vs POSSUM', '(Signal magnitude absolute difference)'})

% % DIFFERENCE BETWEEN X/Y COMPONENTS OF SIGNALS
figure(figHand1)
subplot(nLins, nCols, [23, 28])
% % % % x component
plot(abs(signalP(2,1:TRs)) - abs(squeeze(dictionaryMRF(1, 1:TRs, 1))), ...
         'ro'), hold on % possum vs me
plot( abs(signalP(2,1:TRs)) - abs(signalJ(1,1:TRs)), ...
         'k--')         % possum vs jemris
% % % % y component
plot(abs(signalP(1,1:TRs)) - abs(squeeze(dictionaryMRF(1, 1:TRs, 2))), ...
    'g--*')             % possum vs me
plot( abs(signalP(1,1:TRs)) - abs(signalJ(2,1:TRs)), ...
    'b--')              % possum vs jemris

xlabel('timepoints')
ylabel('Difference in signal components')
legend('POSSUM vs Me (x)', 'POSSUM vs JEMRIS (x)', ...
       'POSSUM vs Me (y)', 'POSSUM vs JEMRIS (y)')
title('JEMRIS vs ME vs POSSUM signal components')


% % % Plotting jemris against me
figure(figHand2)
subplot(1, 2, 1)
plot(0:0.1:1,0:0.1:1) % identity line
hold on
scatter(abs(signalJ(1,1:TRs) + 1i.*signalJ(2,1:TRs)), ... % JEMRIS
        signalDictionaryMRF(1, 1:TRs), 'k')       % Me
grid on
title('Jemris vs Me')
xlabel('Signal magnitude'); ylabel('Signal magnitude');
axis square

% % % Plotting jemris against me
figure(figHand2)
subplot(1, 2, 2)
plot(0:0.1:1,0:0.1:1) % identity line
hold on
scatter(signalAbsP(1:TRs), ...                     % POSSUM
        signalDictionaryMRF(1, 1:TRs), 'r')       % Me
grid on
title('Possum vs Me')
xlabel('Signal magnitude'); ylabel('Signal magnitude');
axis square


%% % Plot comparison between original voxel 1 and modified voxel 1

figure

% % Magnitude of signal
subplot(1,2,1)
plot(0:0.1:1,0:0.1:1,'b') % identity line
hold on
scatter(signalAbsP(1:TRs), signalAbsPo(1:TRs), 'ro')
grid on
axis square
xlabel('signal magnitude')
ylabel('signal magnitude')
xlim([0 1]); ylim([0 1])
title ('iri vs original')

% % Components of signal
subplot(1,2,2)
plot(0:0.1:1,0:0.1:1,'b') % identity line
hold on
scatter(signalP(1,1:TRs), signalPo(1,1:TRs), 'ro')
scatter(signalP(2,1:TRs), signalPo(2,1:TRs), 'g*')
grid on
axis square
xlabel('timepoints')
ylabel('signal components')
xlim([0 1]); ylim([0 1])
legend('identity line','iri_x vs original_x', 'iri_y vs original_y')
title ('iri vs original component-wise')




%% 1. Testing possum vs me vs jemris for reaching steady state
% % % 50 TR blocks, FA = 45, Pha = 0, TR = 60ms, RO = 30ms.
% % % % % % % % %% % ME: Generate dictionary 
% % % % % % % % N   = 100;
% % % % % % % % FA  = 45;
% % % % % % % % PhA =  0;
% % % % % % % % TR  = 60;
% % % % % % % % RO  = 30; %0.000015887026;
% % % % % % % % T1  = 600;
% % % % % % % % T2  =  80;
% % % % % % % % FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
% % % % % % % % SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
% % % % % % % % MATFLAG  = 3; % if == 3 need to provide customMaterial
% % % % % % % % 
% % % % % % % % [materialProperties, sequenceProperties, materialTuples, M, ...
% % % % % % % %  dictionaryMRF, signalDictionaryMRF] = ...
% % % % % % % % 	func_generateCustomSignals(N, FA, PhA, TR, RO, T1, T2);
% % % % % % % % 
% % % % % % % % %% % JEMRIS: Get signals from JEMRIS
% % % % % % % % % % % % % SEQ used: 100RFBlocksMidRO.xml
% % % % % % % % fileName = 'signals_1ss.h5'; 
% % % % % % % % sig = h5read(fileName, '/signal/channels/00');
% % % % % % % % 
% % % % % % % % %% % POSSUM: Get signals from POSSUM
% % % % % % % % NTRs = 100;
% % % % % % % % signal  = read_pulse(['~/simdir/outSignal/signal_ROmid', num2str(NTRs)]);
% % % % % % % % signalO = read_pulse(['~/simdir/outSignal/signal_ROmid', num2str(NTRs), 'orig']);
% % % % % % % % 
% % % % % % % % signalAbs  =  abs( signal(1,:) + 1i.*signal(2,:));
% % % % % % % % signalAbsO =  abs(signalO(1,:) + 1i.*signalO(2,:));


