% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 28-03-2017
% % % % Date updated: 28-03-2017
% % % % 
% % % % Test matrix multiplication
% % % % 

clear all

% Generate big matrix
N = 2000;
A = rand(N,N);
B = rand(N,N);
C = rand(N,N);
v = rand(N,1);

% Store times
M = 10;
t1 = zeros(M,1); t2 = zeros(M,1);

for i = 1:M
    % Elapsed time is  0.231975 seconds. (N=10000)
    tic
    rez2 = (A*(B*(C*v)));
    t2(i) = toc;

    % Elapsed time is 56.276407 seconds. (N=10000)
    tic
    rez = A*B*C*v;
    t1(i) = toc;
end

fprintf('Running (A*(B*(C*v))) takes %d seconds.\n', mean(t2));
fprintf('Running  A* B* C*v    takes %d seconds.\n', mean(t1));

% To see if both results are the same
% figure, imagesc(rez-rez2), colorbar



