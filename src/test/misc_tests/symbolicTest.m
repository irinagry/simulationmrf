% % % % IRINA GRIGORESCU
% % % % Date created: 12-04-2017
% % % % Date updated: 12-04-2017
% % % % 
% % % % Function to test symbolic maths in MATLAB
% % % % 

addpath(genpath('../../helpers/'))

phi = sym('phi');
alpha = sym('alpha');

Rotz(phi) * Rotx(-alpha) * Rotz(-phi)
Rotz(phi)
Rotx(alpha)
