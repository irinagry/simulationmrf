function test_sslocus(N_userDefined)
% % % % IRINA GRIGORESCU
% % % % DATE CREATED: 05-07-2017
% % % % DATE UPDATED: 05-07-2017
% % % %
% % % % This function creates a dictionary for a small set of tissue
% % % % properties to demonstrate the evolution of the 
% % % % magnetic moment vector at TE = 0, TE = TR/2 and TE = TR 
% % % % for beta = [-pi,pi]
% % % % 
% % % % The purpose of this test is to look at how the T1/T2 ratio
% % % % influences the eccentricity of the locus.
% % % % Validation will be done against Gary's analytical solutions
% % % % 
% % % % INPUT: 
% % % %     N_userDefined   = number of time points
% % % % 
% % % % OUTPUT: - 
% % % %     Visually plots the simulated values 

% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % % %
% % % % SEQUENCE
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
% % CUSTOM SEQUENCE for FA = 15
customValuesSequence.RF.FA  = 15;
customValuesSequence.RF.PhA =  0; % B1 field direction +x
customValuesSequence.TR     =  8;
customValuesSequence.RO     =  0;

% % % % 
% % CUSTOM MATERIALS
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
% % material 1 has T2 = 15 TR and T1 =  1 T2
% % material 2 has T2 = 15 TR and T1 =  2 T2
% % material 3 has T2 = 15 TR and T1 =  5 T2
% % material 4 has T2 = 15 TR and T1 = 10 T2

% % material 1:
customValuesMaterial1.T2     =   15 * customValuesSequence.TR; 
customValuesMaterial1.T1     =   1 * customValuesMaterial1.T2;

betaPrecessionTR             = -180:180; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial1.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR);

% % material 2:
customValuesMaterial2    =     customValuesMaterial1;
customValuesMaterial2.T1 = 2 * customValuesMaterial1.T2;
             
% % material 3:
customValuesMaterial3    =     customValuesMaterial1;
customValuesMaterial3.T1 = 5 * customValuesMaterial1.T2;

% % material 4:
customValuesMaterial4    =      customValuesMaterial1;
customValuesMaterial4.T1 = 10 * customValuesMaterial1.T2;
             

% % % % % % % %
% % % % RUN SIMULATIONS FOR N_userDefined timepoints
N = N_userDefined;           % timepoints for FAs and TRs of the sequence
flagIR = 0;                  % == 1 if it starts with an IR pulse

% % Save them: 3 (TEs) x M x N x 3
dictionaryMRF1 = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF2 = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF3 = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF4 = zeros(3, length(betaPrecessionTR), N, 3);

% % % % Run simulations for 3 different TEs (0, TR/2, TR)
for i = 1:3
    % % % % % % % % % % % MATERIAL 1
    tic
    [~, ~, ~, ~, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial1, flagIR);
    toc
    dictionaryMRF1(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % % % % % % % % MATERIAL 2
    tic
    [~, ~, ~, ~, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial2, flagIR);
    toc
    dictionaryMRF2(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % % % % % % % % MATERIAL 3
    tic
    [~, ~, ~, ~, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial3, flagIR);
    toc
    dictionaryMRF3(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % % % % % % % % MATERIAL 4
    tic
    [~, ~, ~, ~, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial4, flagIR);
    toc
    dictionaryMRF4(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    
    
    % % % % Change RO
    customValuesSequence.RO = customValuesSequence.RO   + ...
                              customValuesSequence.TR/2 ;
	
    clear sequenceProperties 
end

% % % % % % % %
% % % % Plot: 
% 1. Locus for different values of T1/T2
% 2. Plot locus analytically

figureIdx = 333;
subplotRows = 2;
subplotCols = 2;
figHandle = figure(figureIdx);
set(figHandle, 'Position', [10, 30, 1000, 800]);


T1 = [customValuesMaterial1.T1 customValuesMaterial2.T1 ...
      customValuesMaterial3.T1 customValuesMaterial4.T1];
T2 = [customValuesMaterial1.T2 customValuesMaterial2.T2 ...
      customValuesMaterial3.T2 customValuesMaterial4.T2];
T1T2ratio = [1 2 5 10];
         
legendText  = {};

flagPlotSS = 1;


% % % % PLOT
for i = 1:4
    
    % Select dictionary
    tempDictionary = dictionaryMRF1;
    switch i 
        case 1
            tempDictionary = dictionaryMRF1;
        case 2
            tempDictionary = dictionaryMRF2;
        case 3
            tempDictionary = dictionaryMRF3;
        case 4
            tempDictionary = dictionaryMRF4;
    end
    
    % Plot locus computationally and analytically
    plot_sslocus(betaPrecessionTR, ...
      squeeze(tempDictionary(1,:,:,:)), ... % M+
      squeeze(tempDictionary(2,:,:,:)), ... % M TE=TR/2
      squeeze(tempDictionary(3,:,:,:)), ... % M-
            T1T2ratio(i), ...         % plotting for a T1/T2 ratio
            T1(i), ...
            T2(i), ...
            customValuesSequence.TR, ...
            customValuesSequence.RF.FA, ...
            figureIdx, ...   %subplotRows,subplotCols,subplotid
            subplotRows, subplotCols, 1, flagPlotSS);  % ...,toPlotSSFlag
    

    % Place legend for second figure
    if flagPlotSS
        legendText = [legendText ...
            {['T_1 / T_2 = ', num2str(T1T2ratio(i))]} ];
        figure(figureIdx+1)
        legend(legendText);
    end
    
    % Pause to see it happening
    pause(2)
      
end
        
disp(['DONE TEST: Locus plotted for different values of T1/T2 ratio']);

  
  
  
        