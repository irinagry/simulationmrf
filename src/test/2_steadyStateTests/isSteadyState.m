function isSteadyState(subsetMRF, betaIdx, ...
                       figureIdx, ...
                       subplotRows, subplotCols, subplotIdx, ...
                       legendText, titlePlot)
% IRINA GRIGORESCU
% DATE CREATED: 04-07-2017
% DATE UPDATED: 04-07-2017
% 
% Function that plots the signal values for a given beta index
% for all events
% 
% Author: Irina Grigorescu, irina.grigorescu.15@ucl.ac.uk

% Get size of mrf dictionary N_df x N_TR events x 3
[~, N_ev, ~] = size(subsetMRF);

% Plot at certain position
figHandle = figure(figureIdx);
set(figHandle, 'Position', [1610, 20, 600, 600]);

% Subplot for current plot
subplot(subplotRows,subplotCols,subplotIdx)
if mod(betaIdx,2) == 0
    lineStyle = '-';
else
    lineStyle = '--';
end
plot( abs( subsetMRF(betaIdx,:,1) + 1i.*subsetMRF(betaIdx,:,2) ), ...
    lineStyle);

title(titlePlot);
legend(legendText);

hold on

grid on
axis square
axis([0 N_ev 0 1])

xlabel('TR Event')
ylabel('Signal Magnitude')

end