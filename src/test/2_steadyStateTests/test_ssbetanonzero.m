function test_ssbetanonzero(N_userDefined, flagIR_userDefined)
% % % % IRINA GRIGORESCU
% % % % DATE CREATED: 05-07-2017
% % % % DATE UPDATED: 05-07-2017
% % % %
% % % % This function creates a dictionary for one set of tissue
% % % % properties to demonstrate the evolution of the 
% % % % magnetic moment vector at TE = 0, TE = TR/2 and TE = TR 
% % % % for beta = [-pi,pi]
% % % % 
% % % % INPUT: 
% % % %     N_userDefined   = number of time points
% % % %     flagIR_userDefined = 1 - starts with IR pulse
% % % %                          0 - starts without an IR pulse
% % % % 
% % % % OUTPUT: - 
% % % %     Visually plots the simulated values 

% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % % %
% % % % SEQUENCE
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
% % CUSTOM SEQUENCE for FA = 15
customValuesSequence.RF.FA  = 15;
customValuesSequence.RF.PhA =  0; % B1 field direction +x
customValuesSequence.TR     =  8;
customValuesSequence.RO     =  0;

% % % % 
% % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
% % material 1 has T2 = 15 TR and T1 =  5 T2, 
% % material 2 has T2 =  2 TR and T1 = 50 T2
customValuesMaterial.T2     = 15 * customValuesSequence.TR; 
customValuesMaterial.T1     =  5 * customValuesMaterial.T2;
betaPrecessionTR            = -180:180; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR);
             

% % % % % % % %
% % % % RUN SIMULATIONS FOR N_userDefined timepoints
N = N_userDefined;           % timepoints for FAs and TRs of the sequence
flagIR = flagIR_userDefined; % == 1 if it starts with an IR pulse

% % Save them: 3 (TEs) x M x N x 3
dictionaryMRFAll = zeros(3, length(betaPrecessionTR), N, 3);

% % % % Run simulations for 3 different TEs (0, TR/2, TR)
for i = 1:3
    % % % % % % % % % % % COLUMN A
    % % % % A1
    tic
    [materialProperties, sequenceProperties, ...
        materialTuples, M, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial, flagIR);
    toc
    dictionaryMRFAll(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    
    
    % % Change RO
    customValuesSequence.RO = customValuesSequence.RO + ...
                               customValuesSequence.TR/2;
    
    % % Clear redundant variables
    clear sequenceProperties 
end

% % % % % % % %
% % % % Plot: 
% 1. If it reached steady state
% 2. The 3d plot of the steady transient signal for a few
% precession angles per TR generated earlier

figureIdx = 222;
subplotRows = 2;
subplotCols = 1;
figure(figureIdx);

N_off = length(betaPrecessionTR);
betaIdx = ceil(N_off/2);

stepBeta = 20;
betaIndices = [betaIdx : stepBeta : N_off  ...
                     1 : stepBeta : betaIdx-stepBeta];
         
legendText  = {};


% % % % PLOT
for i = 1:length(betaIndices)
    
    % Plot phase trajectory and locus and plot magnitude history
    plot_ssbetanonzero(betaPrecessionTR, ...
        squeeze(dictionaryMRFAll(1,:,:,:)), ...
        squeeze(dictionaryMRFAll(2,:,:,:)), ...
        squeeze(dictionaryMRFAll(3,:,:,:)), ...
            betaIndices(i), ...         % plotting for a particular beta
            materialProperties.T1, ...
            materialProperties.T2, ...
            customValuesSequence.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 1);
    

    % Place legend for second figure
    legendText = [legendText ...
        {['\beta = ', num2str(betaPrecessionTR(betaIndices(i))),'^o']} ];
    
    figure(figureIdx+1)
    legend(legendText);
    
    pause(2)
        
end
        
disp(['DONE TEST: Phase trajectories of the total magnetisation ', ...
      'in the presence of repeated RF tipping about the x axis ', ... '
      'for different FA values and for T1/T2 = 5']);
  
  
  
        