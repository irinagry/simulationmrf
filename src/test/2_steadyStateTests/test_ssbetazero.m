function test_ssbetazero(N_userDefined, flagIR_userDefined)
% % % % IRINA GRIGORESCU
% % % % DATE CREATED: 04-07-2017
% % % % DATE UPDATED: 04-07-2017
% % % %
% % % % This function creates a dictionary for a small set of tissue
% % % % properties to demonstrate the evolution of the 
% % % % magnetic moment vector at TE = 0, TE = TR/2 and TE = TR 
% % % % for beta = 0
% % % % 
% % % % INPUT: 
% % % %     N_userDefined   = number of time points
% % % %     flagIR_userDefined = 1 - starts with IR pulse
% % % %                          0 - starts without an IR pulse
% % % % 
% % % % OUTPUT: - 
% % % %     Visually plots the simulated values 


% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % CUSTOM SEQUENCE 
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
% % % %
% % % % SEQUENCES
% % CUSTOM SEQUENCE for FA = 15
customValuesSequenceA.RF.FA  = 15;
customValuesSequenceA.RF.PhA =  0; % B1 field direction +x
customValuesSequenceA.TR     =  8;
customValuesSequenceA.RO     =  0;

% % CUSTOM SEQUENCE for FA = 45
customValuesSequenceB       = customValuesSequenceA;
customValuesSequenceB.RF.FA = 45;

% % CUSTOM SEQUENCE for FA = 90
customValuesSequenceC       = customValuesSequenceA;
customValuesSequenceC.RF.FA = 90;


% % CUSTOM MATERIAL 1 AND 2
% % material 1 has T2 = 15 TR and T1 =  5 T2, 
% % material 2 has T2 =  2 TR and T1 = 50 T2
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial1.T2     = 15 * customValuesSequenceA.TR; 
customValuesMaterial1.T1     =  5 * customValuesMaterial1.T2;

customValuesMaterial2.T2     =  2 * customValuesSequenceA.TR; 
customValuesMaterial2.T1     = 50 * customValuesMaterial2.T2;

betaPrecessionTR             = -1:1; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial1.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequenceA.TR);
customValuesMaterial2.offRes = customValuesMaterial1.offRes;

% % % % % % % %
% % % % RUN SIMULATIONS FOR N_userDefined timepoints
N = N_userDefined;           % timepoints for FAs and TRs of the sequence
flagIR = flagIR_userDefined; % == 1 if it starts with an IR pulse

% % Save them: 3 (TEs) x M x N x 3
dictionaryMRF1A = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF2A = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF1B = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF2B = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF1C = zeros(3, length(betaPrecessionTR), N, 3);
dictionaryMRF2C = zeros(3, length(betaPrecessionTR), N, 3);

% % % % Run simulations for 3 different TEs (0, TR/2, TR)
for i = 1:3
    % % % % % % % % % % % COLUMN A
    % % % % A1
    tic
    [materialProperties1A, sequenceProperties1A, ...
        materialTuples1A, M, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequenceA, ...
                                 MATFLAG, customValuesMaterial1, flagIR);
    toc
    dictionaryMRF1A(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % A2
    tic
    [materialProperties2A, sequenceProperties2A, ...
        materialTuples2A, M, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequenceA, ...
                                 MATFLAG, customValuesMaterial2, flagIR);
    toc
    dictionaryMRF2A(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    
    % % % % % % % % % % % COLUMN B
    % % % % B1
    tic
    [materialProperties1B, sequenceProperties1B, ...
        materialTuples1B, M, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequenceB, ...
                                 MATFLAG, customValuesMaterial1, flagIR);
    toc
    dictionaryMRF1B(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % B2
    tic
    [materialProperties2B, sequenceProperties2B, ...
        materialTuples2B, M, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequenceB, ...
                                 MATFLAG, customValuesMaterial2, flagIR);
    toc
    dictionaryMRF2B(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
   
    % % % % % % % % % % % COLUMN C
    % % % % C1
    tic
    [materialProperties1C, sequenceProperties1C, ...
        materialTuples1C, M, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequenceC, ...
                                 MATFLAG, customValuesMaterial1, flagIR);
    toc
    dictionaryMRF1C(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % C2
    tic
    [materialProperties2C, sequenceProperties2C, ...
        materialTuples2C, M, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequenceC, ...
                                 MATFLAG, customValuesMaterial2, flagIR);
    toc
    dictionaryMRF2C(i,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    
    
    % % Change RO for all sequences
    customValuesSequenceA.RO = customValuesSequenceA.RO + ...
                               customValuesSequenceA.TR/2;
	customValuesSequenceB.RO = customValuesSequenceA.RO;
    customValuesSequenceC.RO = customValuesSequenceA.RO;
    
    
    % % Clear redundant variables
    clear sequenceProperties1A sequenceProperties2A 
    clear materialProperties1B sequenceProperties1B
    clear materialProperties2B sequenceProperties2B 
    clear materialProperties1C sequenceProperties1C
    clear materialProperties2C sequenceProperties2C
end

% % % % % % % %
% % % % Plot: 
% 1. If it reached steady state
% 2. The 3d plot of the transient state signal for beta = 0
% at 3 time points during TR colorcoded differently b-RF-k-r-b-RF-k
% 3. The signal amplitude at TE = TR/2

figureIdx = 111;
subplotRows = 2;
subplotCols = 3;
figure(figureIdx);

% % % % Plot A 1st row
plot_ssbetazero(squeeze(dictionaryMRF1A(1,:,:,:)), ...
                 squeeze(dictionaryMRF1A(2,:,:,:)), ...
                 squeeze(dictionaryMRF1A(3,:,:,:)), ...
            materialProperties1A.T1, ...
            materialProperties1A.T2, ...
            customValuesSequenceA.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 1);       
% % % % Plot A 2nd row
plot_ssbetazero(squeeze(dictionaryMRF2A(1,:,:,:)), ...
                 squeeze(dictionaryMRF2A(2,:,:,:)), ...
                 squeeze(dictionaryMRF2A(3,:,:,:)), ...
            materialProperties2A.T1, ...
            materialProperties2A.T2, ...
            customValuesSequenceA.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 4);        
        

% % % % Plot B 1st row
plot_ssbetazero(squeeze(dictionaryMRF1B(1,:,:,:)), ...
                 squeeze(dictionaryMRF1B(2,:,:,:)), ...
                 squeeze(dictionaryMRF1B(3,:,:,:)), ...
            materialProperties1A.T1, ...
            materialProperties1A.T2, ...
            customValuesSequenceB.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 2);
% % % % Plot B 2nd row
plot_ssbetazero(squeeze(dictionaryMRF2B(1,:,:,:)), ...
                 squeeze(dictionaryMRF2B(2,:,:,:)), ...
                 squeeze(dictionaryMRF2B(3,:,:,:)), ...
            materialProperties2A.T1, ...
            materialProperties2A.T2, ...
            customValuesSequenceB.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 5);
        
        
% % % % Plot C 1st row
plot_ssbetazero(squeeze(dictionaryMRF1C(1,:,:,:)), ...
                squeeze(dictionaryMRF1C(2,:,:,:)), ...
                squeeze(dictionaryMRF1C(3,:,:,:)), ...
            materialProperties1A.T1, ...
            materialProperties1A.T2, ...
            customValuesSequenceC.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 3);
% % % % Plot C 2nd row
plot_ssbetazero(squeeze(dictionaryMRF2C(1,:,:,:)), ...
                squeeze(dictionaryMRF2C(2,:,:,:)), ...
                squeeze(dictionaryMRF2C(3,:,:,:)), ...
            materialProperties2A.T1, ...
            materialProperties2A.T2, ...
            customValuesSequenceC.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 6);

disp(['DONE TEST: Phase trajectories of the total magnetisation ', ...
      'in the presence of repeated RF tipping about the y axis ', ... '
      'for different FA values and for T1/T2 = 5 and = 50']);        
        
        