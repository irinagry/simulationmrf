run('runForPaths.m');

N_userDefined = 1000;
flagIR_userDefined = 0;

% % % %
% % % % SEQUENCE
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
customValuesSequence.RF.FA  =  repmat( 10, [N_userDefined, 1]);
customValuesSequence.RF.PhA =  repmat([0 0]', [N_userDefined/2, 1]); % B1 field direction +x
customValuesSequence.TR     =  repmat(10, [N_userDefined, 1]);
customValuesSequence.RO     =  (customValuesSequence.TR./2);

% % % % 
% % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
% customValuesMaterial.T2     = 100; %     2 * customValuesSequence.TR(1);
% customValuesMaterial.T1     = 950; % 5 * 2 * customValuesSequence.TR(1);

T1 = 950; T2 = 100; % GM
T1 = 250; T2 =  60; % Fat
T1 = 4500; T2 = 220; % CSF

customValuesMaterial.T1 = T1;
customValuesMaterial.T2 = T2;
betaPrecessionTR            = 0:360; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR(1));
             

% % % % % % % %
% % % % RUN SIMULATIONS FOR N_userDefined timepoints
N = N_userDefined;           % timepoints for FAs and TRs of the sequence
flagIR = flagIR_userDefined; % == 1 if it starts with an IR pulse

[materialProperties, sequenceProperties, ...
    materialTuples, M, ...
    dictionaryMRF] = simulateMRF(N, 0, ...
                         SEQFLAG, customValuesSequence, ...
                         MATFLAG, customValuesMaterial, flagIR);

dictionaryMRFComplex = squeeze(dictionaryMRF(:,:,1)) + ...
                      1i.*squeeze(dictionaryMRF(:,:,2));
signalDictionaryMRF = abs(dictionaryMRFComplex);


% % % % 
% figure('Position',[50,50,800,800]); hold on

hold on
% subplot(2,1,1); hold on
plot(deg2rad(betaPrecessionTR), signalDictionaryMRF(:, end))
xlabel({'\beta','angle of precession for each TR (rad)'}); 
ylabel('Signal Intensity (a.u.)');
title('Steady State Signal Intensity for different precession angles ');
xlim([0,2*pi]); grid on


% subplot(2,1,2); hold on
% plot(deg2rad(betaPrecessionTR), angle(dictionaryMRFComplex(:, end)))
% xlabel({'\beta','angle of precession for each TR (rad)'}); 
% ylabel('Signal Phase (^o)');
% title('Steady State Signal Phase for different precession angles ');

