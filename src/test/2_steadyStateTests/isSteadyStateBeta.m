function isSteadyStateBeta(subsetMRF, betaIdx, ...
    figureIdx, ...
    subplotRows, subplotCols, subplotIdx, ...
    legendText, titlePlot)
% % % % IRINA GRIGORESCU
% % % % DATE CREATED: 04-07-2017
% % % % DATE UPDATED: 04-07-2017
% % % %
% % % % Function that plots the signal values for a given beta index
% % % % for all events

[N_off, N_ev, N_vect] = size(subsetMRF);


figHandle = figure(figureIdx);
set(figHandle, 'Position', [500, 20, 600, 600]);

subplot(subplotRows,subplotCols,subplotIdx)
if mod(betaIdx,2) == 0
    lineStyle = '-';
else
    lineStyle = '--';
end
plot( abs( subsetMRF(betaIdx,:,1) + 1i.*subsetMRF(betaIdx,:,2) ), ...
    lineStyle);

title(titlePlot);
legend(legendText);

hold on

grid on
axis square
axis([0 N_ev 0 1])

xlabel('TR Event')
ylabel('Signal Magnitude')

end