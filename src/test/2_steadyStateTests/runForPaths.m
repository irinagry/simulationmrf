% % IRINA GRIGORESCU
% % This script is to be run when you want to add paths needed for this 
% % folder
% % Andt to start from a clean slate

% Addpaths for helpers
addpath(genpath('../../helpers/'))

% Addpaths for preprocess
addpath(genpath('../../preprocess/'))

% Addpaths for algs
addpath(genpath('../../algs/'))

% Addpaths for perlin
addpath(genpath('../../perlin/'))

% % Addpaths for exchanged scripts
addpath(genpath('../../exchanged/'))
