function plot_ssbetanonzero(betaPrecessionTR, ...
            subsetOfMRF1, subsetOfMRF2, subsetOfMRF3, ...
            betaIdx, T1, T2, FA, figureIdx, ...
            subplotRows, subplotCols, subplotIdx)
% % % % IRINA GRIGORESCU
% % % % Date created: 04-07-2017
% % % % Date updated: 04-07-2017
% % % % 
% % % % This function plots in 3D the vector position at transient-state 
% % % % for a range of resonance offset angles
% % % % 
% % % % INPUT:
% % % %     betaPrecessionTR = 1 x N_df array
% % % %         set of resonance offset angles 
% % % % 
% % % %     subsetOfMRF1/2/3 = N_df x N_TRs x 3 array
% % % %         component-wise signal evolution for the entire set of
% % % %         off-res frequencies used and events in the sequence
% % % %         1 => TE = 0
% % % %         2 => TE = TR/2
% % % %         3 => TE = TR
% % % %     betaIdx = index for beta value
% % % %     T1 = relaxation parameter for figure title
% % % %     T2 = relaxation parameter for figure title
% % % %     FA = (double) flip angle
% % % %     figureIdx = figure id for the big figure (the 3d)
% % % %     subplotRows = no rows in subplot
% % % %     subplotCols = no cols in subplot
% % % %     subplotIdx = the subplot id in the figure (the 3d)

    % % Retrieve some needed params
    [N_off, N_ev, N_vect] = size(subsetOfMRF1);

    subsetAll = zeros(N_off, 3*N_ev, N_vect);
    for i = 1 : N_ev
        subsetAll(:,(i-1)*3+1,:) = subsetOfMRF1(:,i,:);
        subsetAll(:,(i-1)*3+2,:) = subsetOfMRF2(:,i,:);
        subsetAll(:,(i-1)*3+3,:) = subsetOfMRF3(:,i,:);
    end
    
    legendText = ['\beta = ', num2str(betaPrecessionTR(betaIdx)),'^o'];
    
	titlePlot = {[ '(T_1 = ', num2str(T1), 'ms, ', ...
                    'T_2 = ', num2str(T2), 'ms, ', ...
                    '\beta = 0^o) '], ...
                 [ '(\alpha = ', num2str(FA), '^o, ', ...
                    'TR = 8ms)' ] };

    % % % % 3D plot
    figHandle = figure(figureIdx);
    set(figHandle, 'Position', [10, 10, 600, 800]);
    subplot(subplotRows, subplotCols, subplotIdx);
    
    [~, ~, ~] = createAxis();
    % Label them
    xlabel('M_x'); ylabel('M_y'); zlabel('M_z');
    % Viewer position
    view([20 70 10]);
    
    % Plot connecting points
    plot3(subsetAll(betaIdx,150:end,1), ...
          subsetAll(betaIdx,150:end,2), ...
          subsetAll(betaIdx,150:end,3), 'k')
    
    % TE = 0
    scatter3(subsetOfMRF1(betaIdx,50:end,1), ...
             subsetOfMRF1(betaIdx,50:end,2), ...
             subsetOfMRF1(betaIdx,50:end,3), 'k'); 
    hold on
    % TE = TR/2
    scatter3(subsetOfMRF2(betaIdx,50:end,1), ...
             subsetOfMRF2(betaIdx,50:end,2), ...
             subsetOfMRF2(betaIdx,50:end,3), 'r'); 
    hold on
    % TE = TR
    scatter3(subsetOfMRF3(betaIdx,50:end,1), ...
             subsetOfMRF3(betaIdx,50:end,2), ...
             subsetOfMRF3(betaIdx,50:end,3), 'b'); 
    axis equal
    axis([-0.5 0.5 -0.5 0.5 -0.2 1])
    title(titlePlot);
    
    % % % % See if it reached steady state
    % New figure
    isSteadyStateBeta(subsetOfMRF2, betaIdx, ...
        figureIdx+1, ...
        1, 1, 1,     ... % subplotRows, subplotCols, subplotIdx
        legendText, titlePlot);
    
    % % % % Plot the locus (just the steady state values
    figure(figureIdx)
    subplot(subplotRows, subplotCols, subplotIdx+1);
    
    [~, ~, ~] = createAxis();
    % Label them
    xlabel('M_x'); ylabel('M_y'); zlabel('M_z');
    % Viewer position
    view([20 70 10]);
    
    scatter3(subsetOfMRF1(betaIdx,N_ev,1), ...
             subsetOfMRF1(betaIdx,N_ev,2), ...
             subsetOfMRF1(betaIdx,N_ev,3), 'k.')
    hold on
    scatter3(subsetOfMRF2(betaIdx,N_ev,1), ...
             subsetOfMRF2(betaIdx,N_ev,2), ...
             subsetOfMRF2(betaIdx,N_ev,3), 'r.')
    hold on
    scatter3(subsetOfMRF3(betaIdx,N_ev,1), ...
             subsetOfMRF3(betaIdx,N_ev,2), ...
             subsetOfMRF3(betaIdx,N_ev,3), 'b.')
    grid on
	axis equal
    axis([-0.8 0.8 -0.8 0.8 -1 1])
    title('Locus');
    
end