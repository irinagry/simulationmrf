function testVisually(FA_userDefined, PhA_userDefined, ...
                      TR_userDefined, RO_userDefined, ...
                      T1_userDefined, T2_userDefined, df_userDefined, ...
                      N_userDefined, flagIR_userDefined, ...
                      titlePlot)
% % % % IRINA GRIGORESCU
% % % % DATE CREATED: 04-07-2017
% % % % DATE UPDATED: 04-07-2017
% % % %
% % % % INPUT: 
% % % %     FA_userDefined  = flip angle (deg)
% % % %     PhA_userDefined = phase angle (deg)
% % % %     TR_userDefined  = TR (ms)
% % % %     RO_userDefined  = RO (ms)
% % % %     T1_userDefined  = T1 (ms)
% % % %     T2_userDefined  = T2 (ms)
% % % %     df_userDefined  = off-resonance frequency (kHz)
% % % %     N_userDefined   = number of time points
% % % %     flagIR_userDefined = 1 - starts with IR pulse
% % % %                          0 - starts without an IR pulse
% % % % 
% % % % OUTPUT: - 
% % % %     Visually plots the simulated values and in between


% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % % % CUSTOM SEQUENCE
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
customValuesSequence.RF.FA  =  FA_userDefined;
customValuesSequence.RF.PhA =  PhA_userDefined;
customValuesSequence.TR     =  TR_userDefined;
customValuesSequence.RO     =  RO_userDefined;
% % % % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial.T1     =  T1_userDefined;
customValuesMaterial.T2     =  T2_userDefined;
betaPrecessionTR            =  df_userDefined;

% df = beta / (2*180*TR) 
customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR); 

% % % % % % % %
% % % % RUN SIMULATION FOR N_userDefined timepoints
N = N_userDefined;           % timepoints for FAs and TRs of the sequence
flagIR = flagIR_userDefined; % == 1 if it starts with an IR pulse

tic
[materialProperties, sequenceProperties, ...
          materialTuples, M, ...
          dictionaryMRF] = simulateMRF(N, 0, ...
                            SEQFLAG, customValuesSequence, ...
                            MATFLAG, customValuesMaterial, ...
                            flagIR);
toc

% % % % % % % %
% % % % PLOT SIMULATION
% Inversion recovery flag
if flagIR == 1
    mu0 = [0 0 -1]';
else
    mu0 = [0 0  1]';
end

% Magnetic moment vector
matType = 1;
mu = squeeze(dictionaryMRF(matType,:,:));

% Material
T1         = materialProperties.T1(matType);
T2         = materialProperties.T2(matType);
betaOffRes = materialProperties.offRes(matType);

% Plot the magnetic moment vector
figure
createAxis(1);
view(0,90); %(140, 25);
title({'Red   - Simulation (at TE)', ...
       'Blue  - RF pulse', ...
       'Black - Relaxation', ...
       titlePlot});
axis square

% % Going through the N blocks
for i = 0:N
    
    % % % If i == 0 plot initial position of magnetic moment vector
    if i == 0
        % Plot the vector and the vector tip
        vec    =    plot3([0 mu0(1)], [0 mu0(2)], [0 mu0(3)], 'r');
        vecTip = scatter3(   mu0(1),     mu0(2),     mu0(3),  'ro');
        
	% % % Else plot the magnetic moment vector calculated
    else
        % Plot the RF pulse discretized into 10 blocks
        for j = 1:10
            % Calculate flip angle and phase angle
            phi = deg2rad(sequenceProperties.RF.PhA(i));
            fa  = deg2rad(sequenceProperties.RF.FA(i));
            
            % Calculate new position of magnetic moment vector
            muTemp = Rotz(phi) * Rotx(-fa * (j/10)) * Rotz(-phi) * mu0;
            
            % Plot the vector and the vector tip during RF
            vec = plot3([0 muTemp(1)], ...
                        [0 muTemp(2)], ...
                        [0 muTemp(3)], 'b');
            vecTip = scatter3(muTemp(1), ...
                              muTemp(2), ...
                              muTemp(3),  'bo');
            pause(0.1)
            delete(vec)
        end
         
        % Update new position of magnetic moment vector
        mu0 = muTemp;
        
        % Plot the relaxation discretized into 10 blocks from the beginning
        % of the RF block to RO time
        for j = 1:10
            % Calculate flip angle and phase angle
            roTime = sequenceProperties.RO(i);
            phiRO  = 2*pi * betaOffRes * roTime; % dw*t

            % Update magnetic moment vector during relaxation
            muTemp = Rotz(-phiRO * (j/10)) * ...
                      Drel(roTime * (j/10), T1, T2) * ...
                        mu0 + Drelz(roTime * (j/10), T1, 1);
            
            % Plot the vector and the vector tip during RF
            vec = plot3([0 muTemp(1)], ...
                        [0 muTemp(2)], ...
                        [0 muTemp(3)], 'k');
            vecTip = scatter3(muTemp(1), ...
                              muTemp(2), ...
                              muTemp(3),  'ko');
            pause(0.2)
            delete(vec)
        end
        
        % % % % SIMULATION
        % Plot the vector and the vector at TE 
        vec    = plot3([0 mu(i,1)], ...
                       [0 mu(i,2)], ...
                       [0 mu(i,3)], 'r');
        vecTip = scatter3(mu(i,1), ...
                          mu(i,2), ...
                          mu(i,3),  'ro');
                      
        % Update new position of magnetic moment vector
        mu0 = mu(i,:)';
                      
        % Plot the relaxation discretized into 10 blocks from the 
        % RO time to the end of the block
        for j = 1:10
            % Calculate flip angle and phase angle
            trTime = sequenceProperties.TR(i) - sequenceProperties.RO(i);
            phiTR  = 2*pi * betaOffRes * roTime; % dw*t

            muTemp = Rotz(-phiTR * (j/10)) * ...
                      Drel(trTime * (j/10), T1, T2) * ...
                        mu0 + Drelz(trTime * (j/10), T1, 1);
            
            % Plot the vector and the vector tip during RF
            vec = plot3([0 muTemp(1)], ...
                        [0 muTemp(2)], ...
                        [0 muTemp(3)], 'k');
            vecTip = scatter3(muTemp(1), ...
                              muTemp(2), ...
                              muTemp(3),  'ko');
            pause(0.2)
            delete(vec)
        end
        
        % Update new position of magnetic moment vector
        mu0 = muTemp;
        
    end
    
    pause(0.2)
    delete(vec); %delete(vecTip);
end
