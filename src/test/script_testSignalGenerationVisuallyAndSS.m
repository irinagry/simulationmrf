% % % % IRINA GRIGORESCU
% % % % THIS WILL TEST THE NEW ALGORITHM

% % Preprocessing steps
clear vars; 
close all; 
clc;

% % Add paths:
run('runForPaths.m');

addpath 1_visualTests/      % Tests for visually inspecting the RF pulses blocks
addpath 2_steadyStateTests/ % Tests for reaching steady state

TEST1VISUALTESTS = 0; % runs tests from 1_visualTests/ 
TEST2STEADYSTATE = 0; % runs tests from 2_steadyStateTests/
RUNMOCKUP        = 0;


%%
% % % % % % % %
% % % % Tests for steady state
if TEST2STEADYSTATE
    % % % % 
    % % % % 1. TESTS BASED ON
    % % % % "Understanding SSFP: A geometric perspective"
    % % % % by Rohan Dharmakumar

    % % % % 
    % % % % a. Steady-State ensemble magnetization when beta = 0
    test_ssbetazero(300, 1);

    
    % % % % 
    % % % % b. Steady-State ensemble magnetization when beta = -180:180

    % % % % b.0 Keep one material property and one sequence design 
    % % % %     plot approach to SS for different betas
    test_ssbetanonzero(300, 1);

    
    % % % %
    % % % % b.1 Having a closer look at the LOCUS (SS Mx,My,Mz for betas)
    % % % %     Eccentricity of the ellipse is related to T1/T2
    test_sslocus(300, 1);

end
%%
% % % % % % % %
% % % % Tests for visually inspecting the blocks
if TEST1VISUALTESTS
    % % 1. For phase angle of rf pulse
    PhA = [0, 45, 90, 135, 180, 225, 270, 315];
    for idx = 1:length(PhA)
        testVisually(60, PhA(idx), ... % FA, PhA
                     40, 20, ... % TR, RO
                     1000, 400, 10, ... % T1(ms), T2(ms), df(kHz)
                     2, 1, ... % Number of timepoints, flag IR
                     ['PhA = ', num2str(PhA(idx)), ' deg']);
        pause
        close all
    end

    % % 2. For flip angle of rf pulse
    FA = [0.1, 45, 90, 135, 180];
    for idx = 1:length(FA)
        testVisually(FA(idx), 0, ... % FA, PhA
                     40, 20, ... % TR, RO
                     1000, 400, 10, ... % T1(ms), T2(ms), df(kHz)
                     2, 1, ... % Number of timepoints, flag IR
                     ['FA = ', num2str(PhA(idx)), ' deg']);
        pause
        close all
    end
end



%%
% % % % % RUN A MOCKUP SIMULATION
if RUNMOCKUP
    % RUN SIMULATION
    %
    % Compare with MRF simulation
    N = 1000;      % timepoints for FAs and TRs of the sequence

    % % % % Custom
    % % % % 
    % % % % Sequence and Material parameters
    FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
    SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
    MATFLAG  = 3; % if == 3 need to provide customMaterial
    % % % % % % % %

    % % CUSTOM SEQUENCE for FA = 15
    customSequence.RF.FA  =   15;
    customSequence.RF.PhA =   180; % B1 field direction -x
    customSequence.TR  =    8;
    customSequence.RO  =    4;

    % % % % Custom Material
    customMaterial.T2     = 2 * customSequence.TR; 
    customMaterial.T1     = 50 * customMaterial.T2;
    betaPrecessionTR      = -180:180; % values for the angle (in deg) of
                                            % precession during TR
    customMaterial.offRes = betaPrecessionTR ... % off resonance values
                     ./ (360 * customSequence.TR);

    tic
    [materialProperties, sequenceProperties, ...
        materialTuples, M, ...
        dictionaryMRF] = ...
                     simulateMRF(N, FLAGPLOT, ...
                     SEQFLAG, customSequence, ...
                     MATFLAG, customMaterial, 0); % 0 flag from no IR
    toc


    % Create dictionary of signal values (M x N)
    tic
    signalDictionaryMRF = abs(      squeeze(dictionaryMRF(1:M, :, 1)) + ...
                              1i .* squeeze(dictionaryMRF(1:M, :, 2)));
    toc



    % PLOT how it reaches steady state for df = 0Hz
    figure(1)

    materialIdx = 181; % position where df = 0
    
    scatter3(squeeze(dictionaryMRF(materialIdx, :, 1)), ...
             squeeze(dictionaryMRF(materialIdx, :, 2)), ...
             squeeze(dictionaryMRF(materialIdx, :, 3)))
    hold on
    for i = 2:N
        plot3([ squeeze(dictionaryMRF(materialIdx, i-1, 1)), ...
                    squeeze(dictionaryMRF(materialIdx, i, 1)) ], ...
              [ squeeze(dictionaryMRF(materialIdx, i-1, 2)), ...
                    squeeze(dictionaryMRF(materialIdx, i, 2)) ], ... 
              [ squeeze(dictionaryMRF(materialIdx, i-1, 3)), ...
                    squeeze(dictionaryMRF(materialIdx, i, 3)) ], ...
              'k-');
    end
    xlim([-1 1]); xlabel('M_x');
    ylim([-1 1]); ylabel('M_y');
    zlim([-1 1]); zlabel('M_z');
    view(-90,0)
    title('Reaching steady-state')
    axis square
    grid on


    % PLOT the Steady-State signal (end) for each beta angle
    figure(2), 
    plot(betaPrecessionTR, signalDictionaryMRF(:,end))
    title('Steady-State signal for each off-resonance angle')
    xlabel('off-resonance angle (deg)'); 
    ylabel('signal magnitude'); 

    % PLOT the signal reaching SS for material with df = 0
    figure(3),
    plot(1:N, signalDictionaryMRF(materialIdx,:))
    title('Reaching Steady-State signal for \Delta f = 0Hz')
    xlabel('TR block number'); 
    ylabel('signal magnitude'); 

    % PLOT the x and y components of the 
    % signal reaching SS for material with df = 0
    figure(4)
    plot(1:N, squeeze(dictionaryMRF(materialIdx, :, 1)))
    hold on
    plot(1:N, squeeze(dictionaryMRF(materialIdx, :, 2)))
    title('Reaching Steady-State components for \Delta f = 0Hz')
    legend('M_x', 'M_y')
    xlabel('TR block number'); 
    ylabel('|M_i|'); 



    % PLOT how it reaches steady state
    figure(5)
    createAxis();
    TRmax = 150;
    
    for i = 1:TRmax
        figure(5)
        vec = plot3([0, squeeze(dictionaryMRF(materialIdx, i, 1))], ...
                [0, squeeze(dictionaryMRF(materialIdx, i, 2))], ...
                [0, squeeze(dictionaryMRF(materialIdx, i, 3))], 'k');
        tip = scatter3(squeeze(dictionaryMRF(materialIdx, i, 1)), ...
                       squeeze(dictionaryMRF(materialIdx, i, 2)), ...
                       squeeze(dictionaryMRF(materialIdx, i, 3)), 'k.');
        figure(5)
        axis square
        xlim([-1 1]); xlabel('M_x');
        ylim([-1 1]); ylabel('M_y');
        zlim([-1 1]); zlabel('M_z');
        figure(5)
        view(90,0);
        grid on
        
        figure(5)
        title(...
         {['Simulation showing how after ', num2str(TRmax), ...
            ' blocks it reaches steady-state'], ... 
          ['for \Delta f = ', num2str(materialTuples(3,materialIdx)), ...
           ' kHz'], ...
          ['Showing block no ', num2str(i), '/', num2str(TRmax)]});

        pause(0.02)
        delete(vec)

    end

end
