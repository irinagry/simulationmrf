% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 11-07-2017
% % % % Date updated: 11-07-2017
% % % % 
% % % % This script generates a dictionary based on some 
% % % % given values for T1, T2 and off-resonance frequencies
% % % % and plots the signal of it
% % % % 


% % Preprocessing steps
clear vars; 
close all; 
clc;

% % Add paths:
run('runForPaths.m');

% Run cofig file to have the parameters of interest at hand
configParams = returnPredefinedParameters();

%% Generate dictionary for a predefined set of tissue properties
N = 1000;      % timepoints for FAs and TRs of the sequence

% % % % Custom

% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Sequence
FA_mrf  =  45;
PhA_mrf =   0;
TR_mrf  =  60;
RO_mrf  =  30;

% % % % Custom Material
T1_mrf = 600;
T2_mrf =  80;
df_mrf =   0;

[materialProperties, sequenceProperties, materialTuples, M, ...
 dictionaryMRF, signalDictionaryMRFNorm] = ...
	func_generateCustomSignals(N, FA_mrf, PhA_mrf, TR_mrf, RO_mrf, ...
                    T1_mrf, T2_mrf, df_mrf, ...
                    FLAGPLOT, SEQFLAG, MATFLAG);

signalDictionaryMRF = sqrt(squeeze(dictionaryMRF(1, :, 1)).^2 + ...
                           squeeze(dictionaryMRF(1, :, 2)).^2);
%% Plot data (I used this to add this generated signal to a different plot)

TRs = 100;
plot(1:TRs, signalDictionaryMRFNorm(1, 1:TRs), 'r--'), hold on
plot(1:TRs, signalDictionaryMRF(1, 1:TRs), 'b--')
xlabel('TR block'); ylabel('signal magnitude');









