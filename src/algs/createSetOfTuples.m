function [materialTuples, M] = createSetOfTuples(matProps)
% % % % IRINA GRIGORESCU
% % % % Date created: 12-04-2017
% % % % Date updated: 21-04-2017
% % % % 
% % % % Update: Add constraint to the creation of material tuples such that
% % % %         T_2 <= T_1. This will affect M which will no longer be
% % % %         N_T1 * N_T2 * N_offRes
% % % % 
% % % % This script creates the set of tuples (T1j, T2j, dfj) j=[1,M]
% % % % 
% % % % INPUT:
% % % %     matProps = (struct) with
% % % %         T1 (N_T1x1 array)
% % % %         T2 (N_T2x1 array)
% % % %         offRes (N_offResx1 array)
% % % % 
% % % % OUTPUT:
% % % %     materialTuples = (3xM double) set of material properties
% % % %     M = cardinal of set 
% % % % 

% Number of T1s, T2s and off-resonance frequencies 
N_T1     = length(matProps.T1); 
N_T2     = length(matProps.T2);
N_offRes = length(matProps.offRes);

% Preallocate memory - MATLAB Likes it
% for the material tuples
materialTuplesTemp = zeros(3, N_T1*N_T2*N_offRes);

% Go through all tissue properties
idx_M = 1;
for idT1 = 1:N_T1                  % T_1
    for idT2 = 1:N_T2              % T_2
        for idOffRes = 1:N_offRes  % df
            % Condition: T_2 <= T_1
            if matProps.T2(idT2) <= matProps.T1(idT1)
                materialTuplesTemp(1, idx_M) = matProps.T1(idT1);
                materialTuplesTemp(2, idx_M) = matProps.T2(idT2);
                materialTuplesTemp(3, idx_M) = matProps.offRes(idOffRes);
                idx_M = idx_M + 1;
            end
        end
    end
end

% Cardinal of set of tissue properties
M = idx_M - 1;

% Create set of tuples
materialTuples = materialTuplesTemp(:, 1:M);  % (3xM) = (T1_j,T2_j,df_j)
