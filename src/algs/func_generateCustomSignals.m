function [materialProperties, sequenceProperties, ...
          materialTuples, M, ...
          dictionaryMRF, ...
          dictionaryMRFComplexBothChannels, ...
          dictionaryMRFNorm, ...
          dictionaryMRFNorm2Ch, ...
          signalDictionaryMRF] = ...
            func_generateCustomSignals(N_ud, FA_ud, PhA_ud, TR_ud, RO_ud, ...
                           T1_ud, T2_ud, df_ud, FLAGPLOT, SEQFLAG, MATFLAG, ...
                           flagIR, indicesOfInterest)
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 12-07-2017
% % % % Date updated: 12-07-2017
% % % % 
% % % % This script generates custom signals
% % % %
% % % % INPUT: 
% % % %     N_ud = number of timepoints user defined
% % % %     FA_ud = flip angle user defined
% % % %     PhA_ud = phase angle user defined
% % % %     TR_ud = TR user defined
% % % %     RO_ud = RO user defined
% % % %     T1_ud = T1 user defined
% % % %     T2_ud = T2 user defined
% % % %
% % % % OUTPUT:
% % % %     materialProperties, sequenceProperties, ...
% % % %     materialTuples, M, ...
% % % %     dictionaryMRF            = M x N x 3 (dict all 3 channels)
% % % %     dictionaryMRFComplex     = 
% % % %     dictionaryMRFNorm        = M x N     (normalised)
% % % %     dictionaryMRFNorm2Ch     = M x 2N    (normalised real and imag
% % % %                                           separated)
% % % %     signalDictionaryMRF      = M x N     (normalised and abs)
% % % %     
% % % % 


N = N_ud;      % timepoints for FAs and TRs of the sequence

% % % % Custom
% % % % 
% % % % Sequence and Material parameters
if (nargin < 8)
    FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
    SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
    MATFLAG  = 3; % if == 3 need to provide customMaterial
end
if (nargin < 12)
    flagIR = 0;
end


% % % % % % % %
% % % % Custom Sequence
customSequence.RF.FA  =  FA_ud;
customSequence.RF.PhA =  PhA_ud;
customSequence.TR     =  TR_ud;
customSequence.RO     =  RO_ud;

% % % % Custom Material
customMaterial.T1     = T1_ud;
customMaterial.T2     = T2_ud;
customMaterial.offRes = df_ud;

tic
[materialProperties, sequenceProperties, ...
    materialTuples, M, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, ...
                 SEQFLAG, customSequence, ...
                 MATFLAG, customMaterial, flagIR);
t=toc;

% % % % Custom indices because
% % % % the dictionary makes a lot of combinations of signals
if (nargin < 13)
    indicesOfInterest = 1:M;
end

% % % % All dictionary signals are transformed into a dictionary matrix
% % % % where both real and imaginary channels are stored
dictionaryMRFComplexAll = [ squeeze(dictionaryMRF(1:M, :, 1))  , ...
                            squeeze(dictionaryMRF(1:M, :, 2)) ];

% % % % Keep only signals of interest
dictionaryMRFComplex = dictionaryMRFComplexAll(indicesOfInterest, :);
% % and materials of interest
materialTuples = materialTuples(:,indicesOfInterest);
% % and recalculate M = number of tissue parameter combinations
M = size(indicesOfInterest,2);

% % % % Normalize signals:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[dictionaryMRFNorm2Ch, dictionaryMRFNorm] = ...
        normalizeSignals(dictionaryMRFComplex);

% Create dictionary of signal values (M x N) from the normalized signals
signalDictionaryMRF        = abs(dictionaryMRFNorm);
dictionaryMRFComplexBothChannels = dictionaryMRFComplex(:,   1 : N) + ...
                               1i.*dictionaryMRFComplex(:, N+1 : end);
dictionaryMRFComplexBothChannels = dictionaryMRFComplexBothChannels ./ ...
                               max(max(dictionaryMRFComplexBothChannels));

disp(['Dictionary Generation took ', num2str(t), ' s']);

