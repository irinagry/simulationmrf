function [Mnext, Mro] = sequenceBlock(Mprev, ...
                            R_FA, R_offRes1, R_offRes2, ...
                            D, Dz)
% IRINA GRIGORESCU
% Date created: 11-04-2017
% Date updated: 12-04-2017
% 
% INPUTS:
%     Mprev = (3xM double)
%             M- magnetisation vector values before RF pulse 
%             where M is the cardinal of the tissue properties set
%     R_FA = (3x3 double) the precomputed rotation matrix for rf
%             pulse
%     R_offRes1 = (struct) with 
%         .RO (3xM double) of the form:
%               [ cos(beta1_RO) cos(beta2_RO) ... cos(betaM_RO) ;
%                 cos(beta1_RO) cos(beta2_RO) ... cos(betaM_RO) ;
%                           1          1     ...     1      ]
%         .TR (3xM double) of the form:
%               [ cos(beta1_TR) cos(beta2_TR) ... cos(betaM_TR) ;
%                 cos(beta1_TR) cos(beta2_TR) ... cos(betaM_TR) ;
%                           1          1     ...     1      ]
% 
%     R_offRes2 = (struct) with
%         .RO (3xM double) 
%               [ -sin(beta1_RO) -sin(beta2_RO) ... -sin(betaM_RO) ;
%                  sin(beta1_RO)  sin(beta2_RO) ...  sin(betaM_RO) ;
%                         1              1     ...          1      ]
%         .TR (3xM double) 
%               [ -sin(beta1_TR) -sin(beta2_TR) ... -sin(betaM_TR) ;
%                  sin(beta1_TR)  sin(beta2_TR) ...  sin(betaM_TR) ;
%                         1              1     ...          1      ]
% 
%     D = (struct) with:
%          .RO (3xM double) relaxation matrix (acting on x,y,z comp)
%          .TR (3xM double) relaxation matrix (acting on x,y,z comp)
% 
%     Dz = (struct) with:
%         .RO (3xM double) relaxatiom matrix (acting on z comp)
%         .TR (3xM double) relaxatiom matrix (acting on z comp)
% 
% OUTPUTS:
%     Mnext = (3xM double) 
%             M- magnetisation vector values before next RF pulse
%     Mro   = (3xM double)
%             M+ magnetisation vector values at RO

% % 1. Do RF pulse
MafterRF = R_FA * Mprev;

% % 2.TR Do off-resonance
Mnext = R_offRes1.TR .*  MafterRF + ...
        R_offRes2.TR .* [MafterRF(2,:) ; MafterRF(1,:) ; MafterRF(3,:)];

% % 3.TR Do relaxation
Mnext = D.TR .* Mnext + Dz.TR;

% % 2.RO Do off-resonance
Mro = R_offRes1.RO .*  MafterRF + ...
      R_offRes2.RO .* [MafterRF(2,:) ; MafterRF(1,:) ; MafterRF(3,:)];

% % 3.RO Do relaxation
Mro = D.RO .* Mro + Dz.RO;


end