function [Dnorm, Dnormcplx] = normalizeSignals(D)
% IRINA GRIGORESCU
% Date created: 06-05-2017
% Date updated: 06-05-2017
% 
% INPUT:
%     D = a (M x 2N) matrix of real numbers 
%         representing a set of M  2N-dimensional signal vectors
% 
% OUTPUT:
%     Dnorm = a (M x 2N) matrix of real numbers 
%         representing a set of M normalized 
%         2N-dimensional signal vectors
%     Dnormcplx = a (M x N) matrix of complex numbers 
%         representing a set of M normalized 
%         N-dimensional signal vectors
% 
% Note to self: This is based on the Trefethen book, page 12, eq 2.2
% It is correct and gives the exact result.

% Prerequisites
[~, N ] = size(D);   % 1->N/2 real N/2+1->N complex

% 0: Turn real/complex pairs into complex numbers
% for dictionary
D =      D(:,       1 : N / 2) + ... % real part
    1i.* D(:, N/2 + 1 : N)   ;       % cplx part

% 1: Calculate Dsq and Ssq
Dsq = conj(D) .* D;   % conj(d_11) d_11 | conj(d_12) d_12 | ...

% 2+3: Calculate Dsum and Ssum Calculate Dsqrt and Ssqrt
Dsqrt = sqrt(sum(Dsq, 2));   % sqrt(sum over rows) -> (Mx1)

% 4. Normalize signals 
Dnormcplx = D ./ repmat(Dsqrt, [1 N/2]);        % M x  N
Dnorm     = [real(Dnormcplx), imag(Dnormcplx)]; % M x 2N

