function [D, Dz] = createD(materialTuples, seqProps, seqID)
% % % % IRINA GRIGORESCU 
% % % % Date created: 12-04-2017
% % % % Date updated: 21-04-2017
% % % % 
% % % % This script creates the D 3xM matrix
% % % % See report for details
% % % % 
% % % % INPUT:
% % % %     materialTuples = (3xM double) set of material properties
% % % %                       M = cardinal of set of material prop
% % % % 
% % % %     seqProps = (struct) with
% % % %         RF = RF pulse (struct) with
% % % %              FA  = flip angle (in deg) (Nx1 array) and 
% % % %              PhA = phase angle (in deg) (Nx1 array)
% % % %         TR = repetition time (in ms) (Nx1 array)
% % % %         RO = read-out time (in ms) (Nx1 array)
% % % % 
% % % %     seqID = (integer) current sequence block ID
% % % % 
% % % % OUTPUT:
% % % %     D = (struct) with:
% % % %         .RO (3xM double) relaxation matrix (acting on x,y,z comp)
% % % %         .TR (3xM double) relaxation matrix (acting on x,y,z comp)
% % % % 
% % % %     Dz = (struct) with:
% % % %         .RO (3xM double) relaxatiom matrix (acting on z comp)
% % % %         .TR (3xM double) relaxatiom matrix (acting on z comp)

%disp('D');
[~, M] = size(materialTuples);

% Create Relaxation matrices
D  = struct;
Dz = struct;

% RO
D.RO = [ exp(-seqProps.RO(seqID)./materialTuples(2,:)) ; ... % exp(-RO/T2)
         exp(-seqProps.RO(seqID)./materialTuples(2,:)) ; ... % exp(-RO/T2)
         exp(-seqProps.RO(seqID)./materialTuples(1,:)) ] ;   % exp(-RO/T1)

Dz.RO      = zeros(3,M);
Dz.RO(3,:) = 1 - D.RO(3,:); % 1-E1
     
% TR
D.TR = [ exp(-seqProps.TR(seqID)./materialTuples(2,:)) ; ... % exp(-TR/T2)
         exp(-seqProps.TR(seqID)./materialTuples(2,:)) ; ... % exp(-TR/T2)
         exp(-seqProps.TR(seqID)./materialTuples(1,:)) ] ;   % exp(-TR/T1)

Dz.TR      = zeros(3,M);
Dz.TR(3,:) = 1 - D.TR(3,:); % 1-E1     
     
