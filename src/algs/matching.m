function [values, indices, matTuples] = matching(Dnorm, S, materialTuples, flagOffRes)
% % % % IRINA GRIGORESCU
% % % % Date created: 05-05-2017
% % % % Date updated: 05-05-2017
% % % % 
% % % % INPUT:
% % % %     D = a (M x 2N) matrix of real numbers 
% % % %         representing the set of M  2N-dimensional signal vectors
% % % % 
% % % %     S = a (K x 2N) matrix of real numbers 
% % % %         representing the set of K  2N-dimensional signal vectors 
% % % %         for which the matching has to be performed
% % % % 
% % % %     materialTuples = (3xM double) set of material properties
% % % % 
% % % % OUTPUT:
% % % %     indices = the indices of the signals in the dictionary 
% % % %               which best match the signals provided for test
% % % % 
% % % %     matTuples = the material tuples associated with the signals 
% % % %                 that best matched the input ones

if (nargin < 4)
    flagOffRes = 1;
end

% Prerequisites
[M, N ] = size(Dnorm); % 1->N/2 real N/2+1->N complex
[K, Nn] = size(S);     % 1->N/2 real N/2+1->N complex
if N ~= Nn
    warning(['Number of timepoints for signals in dictionary does ', ... 
             'not match the number of timepoints for the test signals']);
    return
end

% 1. Normalize dictionary signals and test signals
[~, Dnormcplx] = normalizeSignals(Dnorm);
[~, Snormcplx] = normalizeSignals(S);

% Testing some things:
% aa = Dnorm(24281,:);
% bb = S(233,:);
% 
% a = Dnormcplx(24281,:);
% b = Snormcplx(233,:);

% 2. Calculate the signal matrix with each entry as d_i s_j*
SM = (Dnormcplx) * (Snormcplx');

% 3. Calculate line indices of maximum absolute value for each column
%    My SM should be M x K
[values, indices] = max(abs(SM), [], 1);

% 4. Create material tuples of interest
matTuples = struct;
matTuples.T1     = materialTuples(1, indices);
matTuples.T2     = materialTuples(2, indices);
if flagOffRes == 1
    matTuples.offRes = materialTuples(3, indices);
end






% % % % % % %  TO TEST
% A =      signalDictionaryMRF(1:5,      1:Nn/2) + ... % real part
%     1i.* signalDictionaryMRF(1:5, Nn/2+1:Nn)   ;     % cplx part
% Anorm = A ./ repmat(sqrt(sum(conj(A).*A,2)), [1 Nn/2]);
% 
% B =      signalDictionaryMRF(5,      1:Nn/2) + ... % real part
%     1i.* signalDictionaryMRF(5, Nn/2+1:Nn)   ;     % cplx part
% Bnorm = B ./ repmat(sqrt(sum(conj(B).*B,2)), [1 Nn/2]);
% 
% figure, 
% plot(1:1000, A(1,:)), hold on
% plot(1:1000, Anorm(1,:))
% 
% [maxvals, idLines] = max(abs(Anorm * (Bnorm')),[],1)
