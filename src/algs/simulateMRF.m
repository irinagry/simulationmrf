function [materialProperties, sequenceProperties, ...
          materialTuples, M, ...
          dictionaryMRF] = simulateMRF(N, FLAGPLOT, ...
                                SEQFLAG, customValuesSequence, ...
                                MATFLAG, customValuesMaterial, ...
                                flagIR, filenameDictionary)
% % % % 
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 09-03-2017
% % % % Date updated: 10-04-2017
% % % % 
% % % % This script will create and save the dictionary for a given N
% % % % 
% % % % INPUTS:
% % % %     N = number of timepoints 
% % % %     FLAGPLOT = flag whether to plot the sequence parameters (1/0)
% % % % 
% % % %     SEQFLAG = a flag for how to generate the sequence parameters
% % % %         0 - sinusoidal FA, 0 PhA, perlin TR, RO = 2ms (FISP)
% % % %         1 - urandom FA, alternate PhA, urandom TR, RO = TR./2
% % % %         2 - perlin noise FA, alternate PhA, urandom TRs, RO = TR./2
% % % %         3 - sinusoidal FA, alternate PhA, perlin TRs, RO = TR./2
% % % %         4 - custom FA, alternate PhA, customTRs, custom ROS
% % % %         5 - custom FA, custom PhA, customTRs, custom ROS
% % % %         6 - sinusoidal FA, 0 PhA, fixed TRs, fixed ROs
% % % %     customValuesSequence = (struct) with FA,TR,RO 
% % % % 
% % % %     MATFLAG = a flag for how to generate the material properties
% % % %         0 - The entire range as per MRF-FISP  paper
% % % %         1 - The entire range as per MRF-BSSFP paper
% % % %        11 - Dense  part of MRF
% % % %        22 - Scarce part of MRF
% % % %         2 - Just WM/GM/CSF/Fat for 1.5T and df = custom
% % % %         3 - Custom material properties
% % % %     customValuesMaterial = (struct) with T1,T2,offRes
% % % % 
% % % %     flagIR = set to 1 if you want the sequence to start with an IR
% % % %              it is set to 1 by default
% % % % 
% % % %     filenameDictionary = the dictionary's filename
% % % % 
% % % % 
% % % % OUTPUTS:
% % % %     materialProperties = the material properties
% % % %     sequenceProperties = the generated sequence properties
% % % %     materialTuples = (3xM double) set of material properties
% % % %     M = cardinal of set of material tuples
% % % %     dictionaryMRF = the dictionary
% % % %                  (M x N_timepoints x 3 array)


% % % % % % % % % % % % % % % % % % % % % % % % %
% % % % %     START PREREQUISITES   % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % %

% % % Verify existence of all parameters
% 1. Verify existence of 3rd parameter
if nargin < 3
    SEQFLAG = 1;
    disp(['You did not provide a sequence flag. ', ...
             'Restoring to SEQFLAG = 1']);
end
% 2. Verify existence of 4th parameter
if nargin < 4 
    % Custom Values
    customValuesSequence.FA = 10;
    customValuesSequence.TR = 400;
    customValuesSequence.RO = customValuesSequence.TR/2;
    if SEQFLAG > 3
        toDisplay = {['You did not provide a custom sequence.', ...
             ' Restoring to default values: '], ...
            ['FA = ', num2str(customValuesSequence.FA)], ...
            ['TR = ', num2str(customValuesSequence.TR)], ...
            ['RO = ', num2str(customValuesSequence.RO)]};
    else
        switch SEQFLAG
            case 0
                toDisplay = {['Sequence (SEQFLAG = 0) ', ...
                    'will be generated with ', ...
                    'sinusoidal FA, 0 PhA, perlin TR, RO = 2ms (FISP)']};
            case 1
                toDisplay = {['Sequence (SEQFLAG = 1) ', ...
                    'will be generated with ', ...
                    'urandom FA, alternate PhA, urandom TR, RO = TR./2']};
            case 2
                toDisplay = {['Sequence (SEQFLAG = 2) ', ...
                    'will be generated with ', ...
               'perlin noise FA, alternate PhA, urandom TRs, RO = TR./2']};
            case 3
                toDisplay = {['Sequence (SEQFLAG = 3) ', ...
                    'will be generated with ', ...
               'sinusoidal FA, alternate PhA, perlin TRs, RO = TR./2']};
           otherwise
                toDisplay = {'SEQUENCE: Something went wrong, muahaha!'};
        end
    end
    fprintf('%s\n', toDisplay{:});
end
% 3. Verify existence of 5th parameter
if nargin < 5
    MATFLAG = 3;
    disp(['You did not provide a material flag.', ...
             ' Restoring to MATFLAG = 3']);
end
% 4. Verify existence of 6th parameter
if nargin < 6
    % % % INFO:
    % % % 1.5T
    % % %  WM: T1 =  600 T2 =   80
    % % %  GM: T1 =  950 T2 =  100
    % % % CSF: T1 = 4500 T2 = 2200
    % % % Fat: T1 =  250 T2 =   60
    customValuesMaterial.T1 = [250 600 660 950 1200 4500 5000];
    customValuesMaterial.T2 = [60 70 80 90 100 700 2200];
    customValuesMaterial.offRes = [-0.5 -0.3 -0.15 -0.1 -0.05 -0.03 0 ...
                                    0.03 0.05 0.1  0.15 0.3 0.5]; %in kHz
	if MATFLAG > 1
        toDisplay = {['You did not provide a custom material.', ...
             ' Restoring to default values:'], ...
             ['T1 = ', num2str(customValuesMaterial.T1)], ...
             ['T2 = ', num2str(customValuesMaterial.T2)], ...
             ['df = ', num2str(customValuesMaterial.offRes)]};
    else
        switch MATFLAG
            case 1
                toDisplay = {['Material (MATFLAG = 1) ', ...
                    'will be generated with ', ...
                    'The entire range as per MRF paper']};
            otherwise
                toDisplay = {'MATERIAL: Something went wrong, muahaha!'};
        end
    end
    
	fprintf('%s\n', toDisplay{:});
end
% 7. Verify existence of 7th parameter
if nargin < 7
    flagIR = 1;
end
% 8. Verify existence of 8th parameter
if nargin < 8
    dateForMat = datestr(now, '-yymmdd-HHMMSS'); % with chosen format
    filenameDictionary   = ['../data/dictionary',dateForMat,'.mat'];
    % filenameDictionary = '../data/dictionaryAll_posOffRes.mat';
end
% % % % % % % % % % % % % % % % % % % % % % % % %
% % % % %    END OF PREREQUISITES   % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % %



% % % 
% % % MATERIAL PROPERTIES
% Create set of material properties
tic
disp('Creating Material Properties');
materialProperties = retrieveMaterialProperties(FLAGPLOT, ...
                     MATFLAG, customValuesMaterial);
toc
disp('DONE with Creating Material Properties');

% % % 
% % % SEQUENCE PROPERTIES
% Create array of sequence properties
% N = data points, sequence flag, plot flag
tic
disp('Creating Sequence Properties');
sequenceProperties = retrieveSequenceProperties(N, FLAGPLOT, ...
                     SEQFLAG, customValuesSequence);
toc
disp('DONE with Creating Sequence Properties');

% % % 
% % % CREATE DICTIONARY
% The dictionary is an array of 
%  (M x N x 2 array), where M = cardinal of set of tissue properties 
%                       and N = timepoints for each signal
%  materialTuples = (3xM double) set of material properties
%               M = cardinal of set of tissue properties
tic
disp('Creating dictionary');
[dictionaryMRF, materialTuples, M] = ...
    createDictionary(materialProperties, sequenceProperties, flagIR);
toc
disp('DONE with creating dictionary');
% % % 
% % % SAVE DICTIONARY
% Save materialProperties, sequenceProperties and dictionaryMRF
% save(filenameDictionary, ...
%      'materialTuples', ...     % materialTuples     (3xM double) 
%      'sequenceProperties', ... % sequenceProperties (structure)
%      'dictionaryMRF', ...      % dictionary         (MxNx2)
%      '-v7.3');                 % compression flag

    
end

