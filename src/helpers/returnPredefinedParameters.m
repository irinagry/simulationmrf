function params = returnPredefinedParameters()
% IRINA GRIGORESCU
% DATE: 13-Feb-2017
% Config parameters
% 
% Just runs some stuff needed to be stored in memory 
% of a different program

% The parameters which will be passed as output
params = struct;

% Tissue structure with spin density, T1 and T2 for 1.5T
params.tissue = struct;

dfOfInterest = 0;

params.tissue.csf.rho =    1.0;
params.tissue.csf.t1  = 5000; %4500;
params.tissue.csf.t2  =  700; %2200;
params.tissue.csf.df  =    dfOfInterest ;

params.tissue.wm.rho =    0.65;
params.tissue.wm.t1  =  600; %660;
params.tissue.wm.t2  =   80; %70;
params.tissue.wm.df  =    dfOfInterest ;

params.tissue.gm.rho =    0.8;
params.tissue.gm.t1  =  950; %1200;
params.tissue.gm.t2  =  100; % 90;
params.tissue.gm.df  =    dfOfInterest ;

params.tissue.fat.rho =  0.9;
params.tissue.fat.t1 = 250; 
params.tissue.fat.t2 =  60; 
params.tissue.fat.df  =   dfOfInterest ;

params.tissue.cst.rho =   1;
params.tissue.cst.t1  =  10^10;
params.tissue.cst.t2  =  10^10;
params.tissue.cst.df  = dfOfInterest;

params.gamma = 2.68 * (10^8);

end
