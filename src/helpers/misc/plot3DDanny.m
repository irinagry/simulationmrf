function [ varargout ] = plot3D( volume, varargin )
%% plot3D
% DESCRIPTION:
%   Visualisation of a 3D volume - i.e plotting an error space. 
%
% USAGE:
%   Made for visualising the error space for an optimisation over 3
%   parameters. However can be used to visualised any 3D volume
%
% INPUTS:
%   The inputs are the 3D volume, plus the vectors of the range of values
%   in the order of the dimensions of the 3D volume.
%
%   varargin:
%       1 : i-vec [double] - same size as size(volume,1)
%       2 : j-vec [double] - same size as size(volume,2)
%       3 : k-vec [double] - same size as size(volume,3)
%       4 : [cell of strings] {title, ivec_title, jvec_title, kvec_title}
%
%   Change DWNSAMP - if the volume is large
%
% OUTPUTS:
%   A figure handle and a new image.
%
% ABOUT:
%  author       -   Danny Ramasawmy
%                   rmapdrr@ucl.ac.uk
%                   dannyramasawmy@gmail.com
%  date         -   2017 - 03 - 13
%  last update  -

%% Open figure
figureHandle = figure;
if nargout == 1
    varargout{1} = figureHandle;
end

% sort inputs
try 
    % assign the volume vectors
    ivec = varargin{1};
    jvec = varargin{2};
    kvec = varargin{3};
catch
    % if no varargin then assign it arbitrarily
    ivec = 1:size(volume,1);
    jvec = 1:size(volume,2);
    kvec = 1:size(volume,3);
end
% get lengths of the vectors
inum = length(ivec);
jnum = length(jvec);
knum = length(kvec);

% downsample size
DOWNSAMP = 1;

% downsample the volume
v = volume(1:DOWNSAMP:inum,...
    1:DOWNSAMP:jnum,...
    1:DOWNSAMP:knum);

% permute because MATLAB likes to change dimensions
v = permute(v,[2 1 3]);

% create a mesh to plot with
[x, y, z] = meshgrid(ivec(1:DOWNSAMP:inum),...
    jvec(1:DOWNSAMP:jnum),...
    kvec(1:DOWNSAMP:knum));
% [x,y,z] = meshgrid(1-8:15-8);


% Determine the range of the volume 
% by finding the minimum and maximum of the coordinate data.
xmin = min( x(:) );
ymin = min( y(:) );
zmin = min( z(:) );

xmax = max( x(:) );
ymax = max( y(:) );
zmax = max( z(:) );

colormap(jet)
% h = slice(x,y,z,v,xd,yd,zd);
% h.FaceColor = 'interp';
% h.EdgeColor = 'none';
% % h.DiffuseStrength = 1;

hold on
hx = slice(x,y,z,v,xmax,[],[]);
hx.FaceColor = 'interp';
hx.EdgeColor = 'none';

hy = slice(x,y,z,v,[],ymax,[]);
hy.FaceColor = 'interp';
hy.EdgeColor = 'none';

hz = slice(x,y,z,v,[],[],zmin);
hz.FaceColor = 'interp';
hz.EdgeColor = 'none';

% for a different slice selection - redundant
%{
% hz = slice(x,y,z,v,[],[],zmin);
% hz.FaceColor = 'interp';
% hz.EdgeColor = 'none';

% alcount = 1;
% for slicedx = linspace(zmin,zmax,7);
%     alcount = alcount - 01/7;
%     hz = slice(x,y,z,v,[],[],slicedx);
%     hz.FaceColor = 'interp';
%     hz.EdgeColor = 'none';
%     alpha(hz,alcount)
% end
%}

counter = 1;
for slicedx = linspace(xmin,xmax,7);
    % transparency settings
    alcount = 0.1 + 0.9*counter/7;
    counter = counter +1;
    
    hz = slice(x,y,z,v,slicedx,[],[]);
    hz.FaceColor = 'interp';
    hz.EdgeColor = 'none';
    alpha(hz,alcount)
end

% labels and limits
axis([xmin xmax ymin ymax zmin zmax])

% title
try
    title(varargin{4}{1})
catch
    title('3D Plot')
end

% x-y-z label
str = 'xyz';
for idx = 1:3
    try
        eval([str(idx),'label(varargin{4}{idx + 1})']);
    catch
        eval([str(idx),'label(str(idx));']);
    end
end


end

