function [ varargout ] = makeVideo(matrix3D, filename, varargin )
%% writeVideo - v1.0 Date: 2017-03-20
%
% Author    :   Danny Ramasawmy
%               rmapdrr@ucl.ac.uk
%               dannyramasawmy@gmail.com
% Date      :   2017-03-20  -   created
%
% 
% Description
%   Takes a 3D (not 4D) matrix, of something moving through time, the third
%   dimension of the matrix is used as the time dimension.
%
% FUTURE     :   very basic cannot handle colours yet or plotting in a
%               forloop
    

%%
% % Testing / Raw code
v = VideoWriter([filename,'.avi'],'Uncompressed AVI');

% make it the right shape for the video write class m-n-1-t / m-n-3-t
input2write = reshape(matrix3D,...
    [size(matrix3D,1),...
    size(matrix3D,2),...
    1,...
    size(matrix3D,3)]);

% set frame rate
try 
    frameRate = varargin{1};
    v.set('FrameRate',frameRate);
catch
    warning('Could not set frame rate as specified')
    frameRate = 5;
    v.set('FrameRate',frameRate);
end



% open to write
open(v)

% write
writeVideo(v,input2write)

% close
close(v)

disp(['Video: ',filename,' has been written as .avi'])

if nargout > 0;
    varargout{1} = v;
end





end

