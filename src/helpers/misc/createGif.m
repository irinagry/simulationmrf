%{
CreateGIF

Author:
    Danny Ramasawmy
Date created:x
    2/06/2016

Adapted from http://uk.mathworks.com/matlabcentral/answers/94495-how...
-can-i-create-animated-gif-images-in-matlab
%}


% Want to read in loads of figures, to write as a GIF

% Number of figures
figTotal = 250;

% Prename - assuming just number is incremented
figPreFix = '../../data/forGIF/PlotDict1_';

% file type
figPostfix = '.png';

% Open figure and choose output name
figure(1)
filename = ['../../data/', ...
    'SEQ_1_MAT_Fat_WM.gif'];

% Loop over figure
for figDx = 1:figTotal;
    
    % Read in and print to screen
    myFig = imread([figPreFix,num2str(figDx),figPostfix]);
    
    % The writing part of the figure
    [imind,cm] = rgb2ind(myFig, 256);
    
    if figDx == 1;
        imwrite(imind, cm, filename, 'gif', 'Loopcount', inf);
    else
        imwrite(imind, cm, filename, 'gif', 'WriteMode', 'append');
    end
    
    figDx
end
