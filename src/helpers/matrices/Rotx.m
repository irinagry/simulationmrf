function Rx = Rotx(theta, flag)
% IRINA GRIGORESCU
% Rotation about x in anti-clockwise fashion
% This is the classic mathematical rotation.
% Thus, for MRI, the argument should be negated or the flag used
% 
% INPUT:
%     theta = angle to rotate (needs to be given in RADIANS)
%     flag  = 'aclock' by default
%             'clock'  as the other option
% 
% OUTPUT:
%     Rx = [ 1         0            0     ; 
%            0   cos(theta)  -sin(theta)  ;
%            0   sin(theta)   cos(theta) ];

if nargin < 2
    flag = 'aclock';
end

% For clockwise, change theta sign
if strcmp(flag, 'clock') 
    theta = -theta;
end

Rx = [ 1         0            0     ; 
       0   cos(theta)  -sin(theta)  ;
       0   sin(theta)   cos(theta) ];

end