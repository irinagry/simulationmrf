function T = Tmatrix(phi, alpha)
% IRINA GRIGORESCU
% Date created: 02-08-2017
% Date updated: 02-08-2017
% 
% This is the general RF pulse matrix operator for complex 
% configuration states.
% Rotations are performed anti-clockwise (mathematical rotation)
%        T = Tz(phi)Tx(alpha)Tz(-phi)
% where: Tz = SRS^-1
% 
% For more information check 
%   "Extended Phase Graphs: Dephasing, RF Pulses, and Echoes - 
%    Pure and Simple" by Matthias Weigel
% 
% INPUT:
%     phi   = phase angle of RF PULSE (about z axis)
%     alpha = flip angle of RF PULSE (about x axis)
% 
% OUTPUT:
%     T matrix operator

T = [ (cos(alpha/2))^2   ...
              exp(2*1i*phi)*(sin(alpha/2))^2  ...
                      -1i*exp( 1i*phi)*(sin(alpha))    ; ...
      exp(-2*1i*phi)*(sin(alpha/2))^2  ...
              (cos(alpha/2))^2  ...
                       1i*exp(-1i*phi)*(sin(alpha))    ; ...
	  -1i/2 * exp(-1i*phi)*(sin(alpha))  ...
              1i/2 * exp( 1i*phi)*(sin(alpha))  ...
                       cos(alpha)                    ] ;

end