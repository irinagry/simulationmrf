function R_offRes2 = createRoffres2(materialTuples, beta)
% IRINA GRIGORESCU
% Date created: 12-04-2017
% Date updated: 21-04-2017
% 
% This script creates the Roffres2 3xM matrix
% See report for details
% 
% INPUT:
%     materialTuples = (3xM double) set of material properties
%                       M = cardinal of set of material prop
% 
%     beta = (struct) with
%         .RO (1xM double) of (2*pi * RO(seqID)) .* df(3,:)
%         .TR (1xM double) of (2*pi * TR(seqID)) .* df(3,:)
% 
% 
% OUTPUT:
%     R_offRes2 = (struct) with
%         .RO (3xM double) 
%               [ -sin(beta1_RO) -sin(beta2_RO) ... -sin(betaM_RO) ;
%                  sin(beta1_RO)  sin(beta2_RO) ...  sin(betaM_RO) ;
%                         1              1     ...          1      ]
% 
%         .TR (3xM double) 
%               [ -sin(beta1_TR) -sin(beta2_TR) ... -sin(betaM_TR) ;
%                  sin(beta1_TR)  sin(beta2_TR) ...  sin(betaM_TR) ;
%                         1              1     ...          1      ]

%disp('R_offRes2');
[~, M] = size(materialTuples);

% Create Rotation matrix for off resonance effects
R_offRes2 = struct;

% TR
R_offRes2.TR = zeros(3,M);
R_offRes2.TR(2,:) =  sin(-beta.TR);
R_offRes2.TR(1,:) = -R_offRes2.TR(2,:);

% Read-out
R_offRes2.RO = zeros(3,M);
R_offRes2.RO(2,:) =  sin(-beta.RO);
R_offRes2.RO(1,:) = -R_offRes2.RO(2,:);

