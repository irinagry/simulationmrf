function RFPulseMatrices = createRFPulseMatrices(seqProps)
% IRINA GRIGORESCU
% Date created: 10-04-2017
% Date updated: 10-04-2017
% 
% This function precomputes the RFPulseMatrices
% 
% INPUT:
%     seqProps = (struct) with
%         RF = (struct) RF pulse with
%              FA  = (Nx1 double) flip angle (in deg) and 
%              PhA = (Nx1 double) phase angle (in deg)
%         TR = (Nx1 double) repetition times (in ms)
%         RO = (Nx1 double) readout times (in ms)
% 
% OUTPUT:
%     RFPulseMatrices = (Nx3x3 double)
%                     each entry from this array will be a
%                     precomputed matrix of the form: 
%                     R_z(phi_i)*R_x(-alpha_i)*R_z(-phi_i)
%                     where i = [1,N]

% Retrieve size of sequence
N = numel(seqProps.TR);

% Preallocate memory for the matrices
RFPulseMatrices = zeros(N,3,3);

% Calculate RF pulse matrices
for idx = 1:N
RFPulseMatrices(idx,:,:) = ...
    RzRxRz( deg2rad(seqProps.RF.FA(idx))  , ... % alpha
            deg2rad(seqProps.RF.PhA(idx)) ) ;   %   phi
end



end