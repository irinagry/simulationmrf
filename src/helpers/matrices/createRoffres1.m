function [R_offRes1, beta] = createRoffres1(materialTuples, seqProps, seqID)
% IRINA GRIGORESCU
% Date created: 12-04-2017
% Date updated: 12-04-2017
% 
% This script creates the Roffres1 3xM matrix
% See report for details
% 
% INPUT:
%     materialTuples = (3xM double) set of material properties
%                       M = cardinal of set of material prop
% 
%     seqProps = (struct) with
%         RF = RF pulse (struct) with
%              FA  = flip angle (in deg) (Nx1 array) and 
%              PhA = phase angle (in deg) (Nx1 array)
%         TR = repetition time (in ms) (Nx1 array)
%         RO = read-out time (in ms) (Nx1 array)
% 
%     seqID = (integer) current sequence block ID
% 
% 
% OUTPUT:
%     R_offRes1 = (struct) with 
%         .RO (3xM double) of the form:
%               [ cos(beta1_RO) cos(beta2_RO) ... cos(betaM_RO) ;
%                 cos(beta1_RO) cos(beta2_RO) ... cos(betaM_RO) ;
%                           1          1     ...     1      ]
%         .TR (3xM double) of the form:
%               [ cos(beta1_TR) cos(beta2_TR) ... cos(betaM_TR) ;
%                 cos(beta1_TR) cos(beta2_TR) ... cos(betaM_TR) ;
%                           1          1     ...     1      ]
%     beta = (struct) with
%         .RO (1xM double) of (2*pi * RO(seqID)) .* df(3,:)
%         .TR (1xM double) of (2*pi * TR(seqID)) .* df(3,:)

%disp('R_offRes1');
[~, M] = size(materialTuples);

% Calculate off-resonance angle for TR and RO
beta = struct;
beta.TR = (2*pi * seqProps.TR(seqID)) .* materialTuples(3, :); % 2pi TR df
beta.RO = (2*pi * seqProps.RO(seqID)) .* materialTuples(3, :); % 2pi RO df

% Create Rotation matrix for off resonance effects
R_offRes1 = struct;

% TR
R_offRes1.TR = zeros(3,M) + 1;
R_offRes1.TR(1,:) = cos(-beta.TR);
R_offRes1.TR(2,:) = R_offRes1.TR(1,:);

% Read-out
R_offRes1.RO = zeros(3,M) + 1;
R_offRes1.RO(1,:) = cos(-beta.RO);
R_offRes1.RO(2,:) = R_offRes1.RO(1,:);


