function Rz = Rotz(theta, flag)
% IRINA GRIGORESCU
% Rotation about z in anti-clockwise fashion
% This is the classic mathematical rotation
% Thus, for MRI, the argument should be negated or the flag used
% 
% INPUT:
%     theta = angle to rotate (needs to be given in radians)
%     flag  = 'aclock' by default
%             'clock'  as the other option
% 
% OUTPUT:
%     Rz = [ cos(theta) -sin(theta) 0 ; 
%            sin(theta)  cos(theta) 0 ;
%               0            0      1 ];  

if nargin < 2
    flag = 'aclock';
end

% For clockwise, change theta sign
if strcmp(flag, 'clock') 
    theta = -theta;
end

Rz = [ cos(theta) -sin(theta) 0 ; 
       sin(theta)  cos(theta) 0 ;
          0            0      1 ];  
    
end