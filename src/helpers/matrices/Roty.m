function Ry = Roty(theta, flag)
% IRINA GRIGORESCU
% Rotation about y in anti-clockwise fashion
% This is the classic mathematical rotation
% Thus, for MRI, the argument should be negated or the flag used
% 
% INPUT:
%     theta = angle to rotate (needs to be given in radians)
%     flag  = 'aclock' by default
%             'clock'  as the other option
% 
% OUTPUT:
%     Ry = [ cos(theta)  0   sin(theta) ; 
%               0        1         0    ;
%           -sin(theta)  0   cos(theta) ];

if nargin < 2
    flag = 'aclock';
end

% For clockwise, change theta sign
if strcmp(flag, 'clock') 
    theta = -theta;
end

Ry = [ cos(theta)  0   sin(theta) ; 
          0        1         0    ;
      -sin(theta)  0   cos(theta) ];
    
end