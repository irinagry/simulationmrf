function D = Drelz(t, T1, M0)
% IRINA GRIGORESCU
% This function returns the column relaxation matrix that affects only the
% longitudinal components of the magnetization
% INPUT:
%   Tt = total time of relaxation
%   T1 = longitudinal relaxation
%   M0 = equilibrium magnetization
% OUTPUT:
%   D  = the column relaxation matrix
% 

E1 = exp(-t/T1);

D  = [      0        ;
            0        ;
       M0 * (1-E1) ] ;
             
end