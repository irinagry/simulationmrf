function Rot = RzRxRz(alpha, phi)
% IRINA GRIGORESCU
% This effectively does Rotz(phi)*Rotx(-alpha)*Rotz(-phi)
% Therefore, input positive angles for MRI-type rotations
% 
% This script creates a rotation matrix equal to applying the
% three rotation matrices as above.
% 
% Reasoning: Classically, a change of coordinate system + rotation
% about an axis is represented as Rz(phi)Rx(alpha)Rz(-phi).
% I keep the change of coordinate system as the anti-clockwise
% mathematically correct change of basis (a polar angle phi with
% respect to the x-axis and going towards the positive y axis, but I
% negate the alpha angle as to resemble clockwise rotations as they
% happen in MR phenomena.
% This is correct and shows good agreement with other simulator
% implementations I have seen.
% 
% Note: When this functio is used, please input positive angles (as
% they are given)
%       For example, see createRFPulseMatrices function usage.
% 
% INPUT: 
%     alpha = flip angle in radians
%     phi   = phase angle in radians
% 
% OUTPUT:
%     Rot = [ cos(phi)^2 + cos(alpha)*sin(phi)^2, 
%                 cos(phi)*sin(phi) - cos(alpha)*cos(phi)*sin(phi), 
%                 -sin(alpha)*sin(phi)], 
%           [ cos(phi)*sin(phi) - cos(alpha)*cos(phi)*sin(phi),
%                 cos(alpha)*cos(phi)^2 + sin(phi)^2,
%                 cos(phi)*sin(alpha)],
%           [ sin(alpha)*sin(phi),
%                 -cos(phi)*sin(alpha),
%                 cos(alpha)]


Rot = [cos(phi)^2 + cos(alpha)*sin(phi)^2, ...
       cos(phi)*sin(phi) - cos(alpha)*cos(phi)*sin(phi), ...
       -sin(alpha)*sin(phi) ; ...
       cos(phi)*sin(phi) - cos(alpha)*cos(phi)*sin(phi), ...
       cos(alpha)*cos(phi)^2 + sin(phi)^2, ...
       cos(phi)*sin(alpha) ; ...
       sin(alpha)*sin(phi), ...
       -cos(phi)*sin(alpha), ...
       cos(alpha) ];
   
   
   