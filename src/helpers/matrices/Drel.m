function D = Drel(Tt, T1, T2)
% IRINA GRIGORESCU
% This function returns the diagonal relaxation matrix
% INPUT:
%   Tt = total time of relaxation
%   T1 = longitudinal relaxation
%   T2 = transverse relaxation
% OUTPUT:
%   D  = the diagonal relaxation matrix
% 

E1 = exp(-Tt/T1);
E2 = exp(-Tt/T2);

D  = [  E2    0    0   ;
         0   E2    0   ;
         0    0    E1 ] ;
             
end