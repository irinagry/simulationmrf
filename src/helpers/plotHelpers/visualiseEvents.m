function visualiseEvents(sequenceProperties, materialProperties, ...
                            materialOfInterest, N_events)
% IRINA GRIGORESCU
% Date created: 20-03-2017
% Date updated: 20-03-2017
% 
% This function is used to visualise the events for a given sequence
% and event matrix
% 

% Extract indices for a custom material of interest 
tissueIndices = extractIndices(materialProperties, materialOfInterest);

% %
% % Visualise signal evolution for this sequence
% % for a custom set material of interest
% Relaxation Times
T1 = materialProperties.T1(tissueIndices.cst{1});
T2 = materialProperties.T2(tissueIndices.cst{2});
% Off-resonance frequency
df = materialProperties.offRes(tissueIndices.cst{3});
% Timestep for update
dt = 1; 

% RUN SIMULATION
% Calculate event history of the sequence being applied to the vector
[ mu, b1field, eventMatrix, newFAs, phAngles, newTRs ] = ...
    calculateVectorHistory(sequenceProperties.RF.FA, ...
                               sequenceProperties.RF.PhA, ...
                               sequenceProperties.TR, ...
                               sequenceProperties.RO, ...
                               T1, T2, df, dt, N_events);
    
plotSequence(eventMatrix, mu, b1field, newFAs, ...
             T1, T2, df, dt, N_events, 1, 1);

end