function plotSequence(eventMatrix, mu, b1field, FAs, T1, T2, ...
                      df, dt, N, speedUp, flagAnimate)
% IRINA GRIGORESCU
% Function to plot the events
% This is an old function that seems to be obsolete now

% Preprocessing
if nargin < 10
    speedUp = 1;
    flagAnimate = 1;
end

% NUMBER OF EVENTS
[noEvs noPr] = size(eventMatrix);

% FAs POSITIONS
% % Find from the event matrix the positions of the FAs 
% Find where FAs and TRs are
eventsFAsIdx = find(eventMatrix(:,1) ~= 0);   % col 1 == FAs
eventsFAs    = eventMatrix(eventsFAsIdx, 5);  % col 5 == dt

% Find where the phases are different than 0
eventsPhasesIdx = find(eventMatrix(:,2)~=0);       % col 2 == Phase angle
eventsPhases    = eventMatrix(eventsPhasesIdx, 5); % col 5 == dt
positionsPhases = eventMatrix(eventsPhasesIdx, 1); % col 1 == FAs
anglesForPhases = eventMatrix(eventsPhasesIdx, 2); % col 2 == PhAs

% Find where the TEs are
eventsROsIdx = find(eventMatrix(:,4)~=0);         % col 4 == RO
eventsROs    = eventMatrix(eventsROsIdx, 5) + ... % col 5 == dt
               eventMatrix(eventsROsIdx, 4);   

% PLOT
figHandle = figure;
set(figHandle, 'Position', [20, 60, 1400, 700]);
% 1. Plot FAs and TRs
subplot(2,2,1)

% % Plot IR pulse
stem(eventsFAs(1), FAs(1), 'r')
hold on

% % Plot all the other pulses
stem(eventsFAs(2:end), FAs(2:end), 'b') 
hold on

% % Plot all the phases
scatter(eventsPhases, positionsPhases, 'o', 'MarkerFaceColor', 'b')
%text(eventsPhases, ...                    % at each RF pulse
%     zeros(size(eventsPhases)) + 110, ... % and position 110 on the yaxis
%     ['PhA = ', num2str(anglesForPhases), '^o'])     % write the angle
hold on

% % Plot ROs
scatter(eventsROs, zeros(1,numel(eventsROs)), '*k')
hold on

% % Plot when simulation should end
scatter(eventMatrix(end, 5), 0, '.k')
title({['Simulation of IR-bSSFP sequence with N = ', num2str(N), ...
    ' TRs'], ...
    ['for T_1 = ', num2str(T1), ' ms, T_2 = ', ...
      num2str(T2), ' ms,', ' df = ', num2str(df), ...
      ' kHz and dt = ', num2str(dt), ' ms']})

grid on
xlabel('time(ms)')
ylabel('FA(\alpha^o)')

% PLOT evolution of M components
subplot(2,2,2)
if N > length(eventMatrix(:,1))
    N = length(eventMatrix(:,5));
    axis([0 eventMatrix(N,5) -1 1]);
else
    axis([0 eventMatrix(N+1,5) -1 1]);
end

% PLOT evolution of absolute values of the M components
subplot(2,2,4)
if N > length(eventMatrix(:,1))
    N = length(eventMatrix(:,5));
    axis([0 eventMatrix(N,5) -1 1]);
else
    axis([0 eventMatrix(N+1,5) -1 1]);
end


% 2. Plot 3D view of things
subplot(2,2,3)


[xax, yax, zax] = createAxis();
% Viewer position
view([35 55 10]);

%  Plot what happens to the magnetisation
idxPlot = 1;
for idx = 1:speedUp:noEvs
    
    % SUBPLOT 1: Plot position during simulation
    subplot(2,2,1)
    timeSteps = eventMatrix(idx, 5);
    markerForEachTimeStep = eventMatrix(idx,1)+10;
    tpPlot = stem(timeSteps, markerForEachTimeStep, '--k');

    % SUBPLT 2: Plot Mx,My and Mz values for each 
    subplot(2,2,2)
    timeSteps = eventMatrix(idx, 5);
    Mx = (eventMatrix(idx,6));
    My = (eventMatrix(idx,7));
    Mz = (eventMatrix(idx,8));
    scatter(timeSteps, Mx, '.', ...
        'MarkerEdgeColor',[1 0.5 0.3]); hold on
    scatter(timeSteps, My, '.b'); hold on
    scatter(timeSteps, Mz, '.', ...
        'MarkerEdgeColor',[0.4 0.8 0.4]); hold on
    grid on
    legend('M_x','M_y','M_z')
    xlabel('time(ms)')
    ylabel('M_i')
    
    % SUBPLOT 4: Plot |Mx|, |My| and |Mz| values for each 
    subplot(2,2,4)
    scatter(timeSteps, abs(Mx), ...
        '.','MarkerEdgeColor',[1 0.5 0.3]); hold on
    scatter(timeSteps, abs(My), '.b'); hold on
    scatter(timeSteps, abs(Mz), ...
        '.', 'MarkerEdgeColor',[0.4 0.8 0.4]); hold on
    grid on
    legend('|M_x|','|M_y|','|M_z|')
    xlabel('time(ms)')
    ylabel('|M_i|')
    
    
    % SUBPLOT 3: VISUALISE 
    % Plot Magnetisation Vector and B1 field
    subplot(2,2,3)
    muvec = [[0 0 0]'  mu(:, idx)]; 
    b1vec = [[0 0 0]'  b1field(:,idx)]; 
    
    % DURING RF PULSE 
    % Verify if it's time for an RF Pulse ~=IR
    if (idx)~=2 && eventMatrix(idx,1) ~=0 
        
        RFSteps = 10;
        muRF = eventMatrix(idx-1, 6:8)';
        rotAngle = eventMatrix(idx, 1) / RFSteps;
        
        for idxL = 1:RFSteps
            
            % VISUALISE
            muRF = Rotz( deg2rad(eventMatrix(idx,2))) * ... %PhA
              Rotx(-deg2rad(rotAngle)) * ...
              Rotz(-deg2rad(eventMatrix(idx,2))) ...        %PhA
              * muRF;
          
            subplot(2,2,3)
            muvec = [[0 0 0]'  muRF];
            muvecplotRF = plot3(muvec(1,:), muvec(2,:), muvec(3,:), ...
                                'r'); hold on
            mutipplotRF = scatter3(muRF(1), muRF(2), muRF(3), ...
                                'r^'); hold on
            mutipplothistRF = scatter3(muRF(1), muRF(2), muRF(3), ...
                                '.b'); hold on
                            
            % for B1 field
            b1vecplot = plot3(b1vec(1,:), b1vec(2,:), b1vec(3,:), ...
                        '--g'); hold on
            b1tipplot = scatter3(b1field(1,idx), ...
                        b1field(2,idx), b1field(3,idx), 'go'); hold on
            
            
            % PLOTS for MxMyMz
            % SUBPLT 2: Plot Mx,My and Mz values for each 
            subplot(2,2,2)
            timeSteps = eventMatrix(idx, 5);
            Mx = muRF(1);
            My = muRF(2);
            Mz = muRF(3);
            scatter(timeSteps, Mx, ...
                '.', 'MarkerEdgeColor',[1 0.5 0.3]); hold on
            scatter(timeSteps, My, '.b'); hold on
            scatter(timeSteps, Mz, ...
                '.', 'MarkerEdgeColor',[0.4 0.8 0.4]); hold on
            grid on
            legend('M_x','M_y','M_z')
            xlabel('time(ms)')
            ylabel('M_i')

            % SUBPLOT 4: Plot |Mx|, |My| and |Mz| values for each 
            subplot(2,2,4)
            scatter(timeSteps, abs(Mx), ...
                '.', 'MarkerEdgeColor',[1 0.5 0.3]); hold on
            scatter(timeSteps, abs(My), '.b'); hold on
            scatter(timeSteps, abs(Mz), ...
                '.', 'MarkerEdgeColor',[0.4 0.8 0.4]); hold on
            grid on
            legend('|M_x|','|M_y|','|M_z|')
            xlabel('time(ms)')
            ylabel('|M_i|')
            
            % SAVE FIGURE Before deleting plots
%             print(gcf, ['../data/forGIF/Plot_2blocks_', ...
%                  num2str(idxPlot)], '-dpng');
%             idxPlot = idxPlot + 1;
            
            pause(0.0001)
            deletePlots(muvecplotRF, mutipplotRF, b1vecplot, b1tipplot);
            
        end
    end
    
    subplot(2,2,3)
    % Continue with plotting normally
    muvecplot = plot3(muvec(1,:), muvec(2,:), muvec(3,:), 'r'); hold on
    mutipplot = scatter3(mu(1,idx), mu(2,idx), mu(3,idx), 'r^'); hold on
    mutipplothist = scatter3(mu(1,idx), mu(2,idx), mu(3,idx), '.k'); hold on
    
    % for B1 field
    b1vecplot = plot3(b1vec(1,:), b1vec(2,:), b1vec(3,:), '--g'); hold on
    b1tipplot = scatter3(b1field(1,idx), b1field(2,idx), b1field(3,idx), 'go'); hold on
    
    % Pause and delete history
    if flagAnimate
        pause(0.0001)
    else
        disp(['Iteration ', num2str(idx), '/', num2str(noEvs)]);
    end
    
        
    % SAVE FIGURE Before deleting plots
%     print(gcf, ['../data/forGIF/Plot_2blocks_', ...
%                  num2str(idxPlot)], '-dpng');
%     idxPlot = idxPlot + 1;
    
    Delete plots
    deletePlots(tpPlot, muvecplot, mutipplot, b1tipplot, b1vecplot);
    VISUALISATION PART ENDED

    
end  


end