function [ mu, b1field, eventMatrix, newFAs, newPhAngles, newTRs ] = ...
             calculateVectorHistory(FAs, PhAs, TRs, ROs, T1, T2, df, dt, N_TRs)
% 
% INPUT:
%     FAs - array of FA values (without IR)
% 
%     PhAs - array of phase angles values (without IR)
% 
%     TRs - array of TR values (without the tau<<T1 delay 
%                               between IR and first pulse)
%     ROs - array of RO values 
%                               
%     T1  - T1 value
% 
%     T2  - T2 value
% 
%     df  - off-resonance value
% 
%     dt  - time step for update/simulation
% 
%     N_TRs - number of events you want to plot for
% 
% OUTPUT:
%     mu  - magnetisation vector values through time during sequence
% 
%     b1field  - B1 field values through time during sequence
% 
%     eventMatrix - event matrix for the sequence simulation
% 
%     newFAs - FAs + first IR (for when I need to plot)
% 
%     phAngles - phases for RF pulses
% 
%     newTRs - TRs + first delay (for plotting)
%



% %  Useful parameters
% Initial time delay tau between IR and first FA
tauDelay = 5;
% Unit vectors:
x = [1 0 0]'; y = [0 1 0]'; z = [0 0 1]';
% Number of events will be N_TRs + 1 because of IR pulse
Nevents = N_TRs + 1;

% New TRs will incorporate some small delay for visualisation purposes
newTRs = zeros(Nevents, 1);
newTRs(1) = tauDelay; newTRs(2:end) = TRs(1:N_TRs);

% New ROs will incorporate some small delay for visualisation purposes
newROs = zeros(Nevents, 1);
newROs(2:end) = ROs(1:N_TRs);
                    
% New FAs will incorporate the initial IR pulse
newFAs = zeros(Nevents, 1);
newFAs(1) = 180; newFAs(2:end) = FAs(1:N_TRs);

% Phase Angles
newPhAngles = zeros(Nevents, 1);
newPhAngles(1) = 0; newPhAngles(2:end) = PhAs(1:N_TRs);

% Total time for sequence
TTseq = sum(newTRs./dt);

%% Create event matrix
NevM = ceil(TTseq);
eventMatrix = zeros(NevM, 11); % FA, phase, TR, TE, dt, Mx,y,z, B1x,y,z
eventMatrix(2,1) = 180;      % The IR pulse
eventMatrix(2,2) =   0;      % The phase of the pulse
eventMatrix(2,3) = tauDelay; % First TR will be chosen by us
eventMatrix(:,5) = (0:NevM-1).*dt;

for idx = 2:Nevents
    % Calculate position in matrix
    posInMatrix = sum(ceil(newTRs(1:idx-1)./dt))+1;
    
    eventMatrix(posInMatrix, 1) = newFAs(idx);      % FA 
    eventMatrix(posInMatrix, 2) = newPhAngles(idx); % PhA
    eventMatrix(posInMatrix, 3) = newTRs(idx);      % TR
    eventMatrix(posInMatrix, 4) = newROs(idx);      % RO
    
    % Calculate position in matrix for RO
%     posInMatrixForRO = ceil(posInMatrix + newROs(idx)./dt);
%     eventMatrix(posInMatrixForRO, 4) = newROs(idx);
end

%% Calculated Variables
% Initialise variables

% MAGNETIC MOMENT
mu      = zeros(3, NevM);
mu(:,1) = 0*x + 0*y + 1*z;
eventMatrix(1,6:8) = mu(:,1); % eventMatrix position for mu

% B1 FIELD
b1field      = zeros(3, NevM);
b1field(:,1) = 1*x + 0*y + 0*z;
eventMatrix(1,9:11) = b1field(:,1); % eventMatrix position for B1


%% Calculate vector movement history 
% Find events in the event matrix which correspond to FAs
eventsFAsIdx = find(eventMatrix(:,1) ~= 0); 

% Until first FA keep M and B1 at same position
% Do IR-pulse
faEv     = deg2rad(eventMatrix(2, 1));
phEv     = deg2rad(eventMatrix(2, 2));
mu(:,2) = Rotz(phEv) * Rotx(-faEv) * Rotz(-phEv) * mu(:,1);
% Keep Mu the same
mu(1, 3:eventsFAsIdx(2)-1) = mu(1,2);
mu(2, 3:eventsFAsIdx(2)-1) = mu(2,2);
mu(3, 3:eventsFAsIdx(2)-1) = mu(3,2);
eventMatrix(:, 6:8) = mu';
% Keep B1 the same
b1field(1, 2:eventsFAsIdx(2)-1) = b1field(1,1);
b1field(2, 2:eventsFAsIdx(2)-1) = b1field(2,1);
b1field(3, 2:eventsFAsIdx(2)-1) = b1field(3,1);
eventMatrix(:, 9:11) = b1field';

% Go through all TR blocks
for event = 2 : length(eventsFAsIdx)
    
    % Event start (right before RF pulse)
    eventStart = eventsFAsIdx(event  )-1;
    % Event end (right before next RF pulse)
    if event == length(eventsFAsIdx)
        eventEnd   = NevM;
    else
        eventEnd   = eventsFAsIdx(event+1)-1;
    end
    
    % Prepare variables for sequenceBlock
    % Mprev
    Mprev = eventMatrix(eventStart, 6:8)';
    % Material Properties
    matProperty.T1 = T1;
    matProperty.T2 = T2;
    matProperty.offRes = df;
    % Sequence Properties
    seqProperty.RF.FA  = eventMatrix(eventStart+1, 1); %FA
    seqProperty.RF.PhA = eventMatrix(eventStart+1, 2); %PhA
    seqProperty.TR = eventMatrix(eventStart+1, 3);     %TR
    seqProperty.RO = eventMatrix(eventStart+1, 4);     %RO
    % Number of timepoints
    Ntp = eventEnd - eventStart;
    
    % RUN SEQUENCE BLOCK
    [Mnext, Mro, Mhist] = ...
        sequenceBlock(Mprev, matProperty, seqProperty, Ntp, dt);
    
    % SAVE RF FIELD DIRECTION
    B1prev = b1field(:,1);
    phEv = deg2rad(seqProperty.RF.PhA);
    if (phEv ~= 0)
        B1next = Rotz(phEv) * B1prev;
    else
        B1next = B1prev;
    end
    
    % POPULATE EVENT MATRIX
    eventMatrix(eventStart+1:eventEnd, 6:8)  = Mhist';
    eventMatrix(eventStart+1:eventEnd, 9:11) = repmat( B1next', [Ntp,1] );
    
end

% Populate B1 and mu
mu      = eventMatrix(:, 6: 8)';
b1field = eventMatrix(:, 9:11)';

disp('In calculate vector history');
% ROUND OFF TO zero unpleasant values
% eventMatrix(eventMatrix < 1e-05) = 0;

end



% %% Calculate vector movement history
% % For each event in the event matrix
% faEv = 0; phEv = 0; 
% 
% for ev = 2:NevM    
%     
%     % RF PULSE
%     if eventMatrix(ev, 1) ~= 0 % if FA ~= 0
%         % Get all the needed values from the event matrix
%         faEv     = deg2rad(eventMatrix(ev, 1));
%         phEv     = deg2rad(eventMatrix(ev, 2));
%         
%         % Do instantaneous RF pulse
%         mu(:,ev) = Rotz(phEv) * Rotx(-faEv) * Rotz(-phEv) * mu(:,ev-1);
%         
%         % Save RF Field direction
%         if (phEv ~= 0)
%             b1field(:, ev) = Rotz(phEv) * b1field(:,1);
%         else
%             b1field(:, ev) = b1field(:,1);
%         end
%         
%         %disp([num2str(ev), ' doing RF pulse']);
%         
%     % RELAXATION After RF PULSE
%     else
%         % Relax and offresonance (only if it's not in the first part)
%         if ev > newTRs(1)/dt
%             
%             offResAngle = 2*pi*df*dt;
%             mu(:,ev) = Rotz(-offResAngle) * Drel(dt,T1,T2) * mu(:,ev-1) + ...
%                 Drelz(dt,T1,1);
%             
%         % For the first part just save the same value
%         else
%             mu(:,ev) = mu(:,ev-1);
%         end
%         
%         % During relaxation b1 is the same
%         b1field(:, ev) = b1field(:, ev-1);
%         
%         %disp([num2str(ev), ' doing relaxation']);
%     end
% 
% end
