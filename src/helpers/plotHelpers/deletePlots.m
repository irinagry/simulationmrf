function deletePlots(plotToDelete, varargin)
% IRINA GRIGORESCU
% This function deletes all plots sent as params

    % at least one parameter is needed and therefore deleted here
    delete(plotToDelete);

    % the rest are deleted here
    nVarargs = length(varargin);
    for k = 1:nVarargs
        delete(varargin{k});
    end
end