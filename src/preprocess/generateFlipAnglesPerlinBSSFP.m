function FAs = generateFlipAnglesPerlinBSSFP(N, lowFA, highFA, flagToPlot)
% IRINA GRIGORESCU
% Date Created: 24-03-2017
% Date Updated: 24-03-2017
%
% Function that generates flip angles in a Perlin smooth fashion in the 
% [lowFA, highFA] range.
% 
% INPUTS:
%   N = number of data points
%   lowFA  = lowest FA
%   highFA = highest FA
%
% OUPUTS:
%   FAs = (1xN) values of flip angles
%               generated between [lowFA, highFA]


if nargin < 4
    flagToPlot = 0;
end

% Create 1xN array of flip angles generate between [lowFA, highFA]
% with Perlin noise
M = ceil(10/100*N); % 10% of N
if M < 2
    M = 2;
end
FAs = perlin(N, M, lowFA, highFA);

% Add ramp between half-10%N -> half
lowerHalfN = floor(N/4) - floor(5*N/100);
upperHalfN = floor(N/4);
noValuesRamp = upperHalfN - lowerHalfN;
FAs(lowerHalfN:upperHalfN-1) = linspace(FAs(lowerHalfN), 0, noValuesRamp);


if flagToPlot
    figure,
    plot(FAs);
    title('FAs')
    axis equal
    xlim([0 N])
    ylim([min(FAs)-1 max(FAs)+1])
    grid on
end

end
