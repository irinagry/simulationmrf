function materialProperties = retrieveMaterialProperties(flagPlot, ...
                              matFlag, customMAT)
% % % % IRINA GRIGORESCU
% % % % Date created: 02-03-2017
% % % % Date updated: 15-03-2017
% % % % 
% % % % INPUTS:
% % % %     flagPlot = a flag set to 1 if you want to plot
% % % %     matFlag = a flag for how to generate the material properties
% % % %         0  - The entire range as per MRF FISP  paper
% % % %         1  - The entire range as per MRF BSFFP paper
% % % %         2  - Just WM/GM/CSF/Fat for 1.5T and df = custom
% % % %         3  - Custom material properties
% % % % 
% % % %         Hardcoded:
% % % %         ------
% % % %         11  - dense sampling of tissue space
% % % %               T1 <= 2000, T2 <= 200, df <= 10Hz
% % % %         111 - subset of 11 (for testing), but df fixed
% % % %               T1 <= 2000, T2 <= 200, df <= 10Hz
% % % %         112 - shifted subset of 11 (for testing extrapolation)
% % % %               T1 <= 2000, T2 <= 200, df <= 10Hz
% % % %         ------
% % % %         12  - dense sampling of tissue space
% % % %               T1 <= 2000, T2 <= 200, df <= 1Hz
% % % %         121 - subset of 12 (for testing), but df fixed
% % % %               T1 <= 2000, T2 <= 200, df <= 1Hz
% % % %         122 - shifted subset of 12 (for testing extrapolation)
% % % %               T1 <= 2000, T2 <= 200, df <= 1Hz
% % % %         ------
% % % %         13  - dense sampling of tissue space,
% % % %               but SCARCE sampling of df
% % % %               T1 <= 2000, T2 <= 200, df <= 50Hz
% % % %         131 - subset of 13 (for testing)
% % % %               T1 <= 2000, T2 <= 200, df <= 50Hz
% % % %         132 - shifted subset of 13 (for testing extrapolation)
% % % %               T1 <= 2000, T2 <= 200, df <= 50Hz
% % % %         ------
% % % %         etc.
% % % % 
% % % %     customMAT = (struct) with arrays of T1,T2,df
% % % % 
% % % % 
% % % % OUTPUTS:
% % % %     materialProperties = (struct) with
% % % %         T1 (N_T1x1 array)
% % % %         T2 (N_T2x1 array)
% % % %         offRes (N_offResx1 array)

% % % INFO:
% % % 1.5T
% % %  WM: T1 =  600 T2 =   80
% % %  GM: T1 =  950 T2 =  100
% % % CSF: T1 = 4500 T2 = 2200
% % % Fat: T1 =  250 T2 =   60


% % 0.
% % PREREQUISITES
% Create structure for material properties
materialProperties = struct;

% % For each type of material properties wanted
switch matFlag
    
    % The entire range of T1,T2 and df as per MRF-FISP paper
    case 0
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from 20ms to 5000ms with different timesteps
        T1 = 20 : 10 : 3000;         % from   20ms to 3000ms dt =  10ms
        T1 = [T1 3200 : 200 : 5000]; % from 3000ms to 5000ms dt = 200ms

        % % 
        % % T2 ranges from 20ms to 3000ms with different timesteps
        T2 = 10 :  5 : 300;          % from  10ms to 300ms dt =  5ms
        T2 = [T2 350:50:500];        % from 300ms to 500ms dt = 50ms

        % % 
        % % Off-resonance is 0.
        offRes = 0;
    
    % The entire range of T1,T2 and df as per MRF-bSSFP paper
    case 1
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from 100ms to 5000ms with different timesteps
        T1 = 100:20:2000;        % from 100ms to 2000ms dt = 20ms
        T1 = [T1 2300:300:5000]; % from 2300ms to 5000ms dt = 300ms

        % % 
        % % T2 ranges from 20ms to 3000ms with different timesteps
        T2 = 20:5:100;          % from 20ms to 100ms dt = 5ms
        T2 = [T2 110:10:200];   % from 110ms to 200ms dt = 10ms
        T2 = [T2 400:200:3000]; % from 400ms to 3000ms dt = 200ms

        % % 
        % % Off-resonance freq from -400Hz to 400Hz with different steps
        offRes = 1:40;                % from 1Hz to 40Hz df = 1Hz
        offRes = [offRes 42:2:80];    % from 42Hz to 80Hz df = 2Hz
        offRes = [offRes 90:10:250];  % from 90Hz to 250Hz df = 10Hz
        offRes = [offRes 270:20:400]; % from 270Hz to 400Hz df = 20Hz
        offRes = [-offRes(end:-1:1) 0 offRes];  % added the negative side and 0Hz
        offRes = offRes .* 1e-03;     % Keep in kHz because kHz = ms^-1
    
    % T1,T2 values for WM/GM/CSF/Fat for 1.5T and df = custom
    case 2
        T1 = [250 600 950 4500];
        T2 = [60 80 100 2200];
        offRes = customMAT.offRes;
        
    % Custom material properties
    case 3
        T1     = customMAT.T1;
        T2     = customMAT.T2;
        offRes = customMAT.offRes;
        
        
        
	% % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % % % CUSTOM VALUES FOR LINEAR MAP LEARNING 
    % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % % % % % % % % % % % % % % % % % % % % % % % % % % %
	% DENSE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 10Hz
    case 11
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from: 
        T1 = 100:20:2000;  

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        
        % % 
        % % Off-resonance freq from:
        offRes = 1:10;     
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1

	% DENSE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 10Hz
    % This is used for testing:
    % T1/T2 the same range as case "11" but values chosen in between
    % df kept fixed as case "11"
    case 111
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:20:2000;  
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from 20ms to 300ms with different timesteps
        T2 = 20:5:200;          % from 20ms to 400ms dt = 5ms
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from -80Hz to 80Hz with different steps
        offRes = 1:10;           % from 5Hz to 50Hz df = 5Hz
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
    % DENSE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 10Hz
    % This is used for testing:
    % T1/T2/df the same range as case "11" but values chosen in between
	case 112
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:20:2000;  
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from:
        offRes = 1:10;          
        offRes = offRes - 0.5;
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
	% % % % % % % % % % % % % % % % % % % % % % % %
	% DENSE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 1Hz
    case 12
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:20:2000;  

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        
        % % 
        % % Off-resonance freq from:
        offRes = 0.1:0.1:1;         
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1      
       
    % DENSE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 1Hz
    % This is used for testing:
    % T1/T2 the same range as case "12" but values chosen in between
    % df kept fixed as case "12"
    case 121
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:20:2000;  
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from:
        offRes = 0.1:0.1:1;         
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
    
	% DENSE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 1Hz
    % This is used for testing:
    % T1/T2/df the same range as case "12" but values chosen in between
    case 122
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:20:2000;  
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from:
        offRes = 0.1:0.1:1;         
        offRes = offRes - 0.05;
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
	% % % % % % % % % % % % % % % % % % % %
    % DENSE SAMPLING OF TISSUE SPACE, but SCARCE sampling of df
    % T1 <= 2000, T2 <= 200, df <= 50Hz
    case 13
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from: 
        T1 = 100:20:2000;   

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        
        % % 
        % % Off-resonance freq from:
        offRes = 5:5:50;          
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
    % DENSE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 50Hz
    % Used for testing so less values than 13
    case 130
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from: 
        T1 = 100:60:2000;   

        % % 
        % % T2 ranges from:
        T2 = 20:20:200;     
        
        % % 
        % % Off-resonance freq from:
        offRes = 20:20:50;          
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1  
        
    % DENSE SAMPLING OF TISSUE SPACE, but SCARCE sampling of df
    % T1 <= 2000, T2 <= 200, df <= 50Hz
    % This is used for testing:
    % T1/T2 the same range as case "13" but values chosen in between
    % df kept fixed as case "13"
    case 131
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:20:2000;  
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from:
        offRes = 5:5:50;           
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
    
	% DENSE SAMPLING OF TISSUE SPACE, but SCARCE sampling of df
    % T1 <= 2000, T2 <= 200, df <= 50Hz
    % This is used for testing:
    % T1/T2/df the same range as case "13" but values chosen in between
    case 132
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from 100ms to 1000ms with different timesteps
        T1 = 100:20:2000;        % from  100ms to 1000ms dt = 10ms
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from 20ms to 300ms with different timesteps
        T2 = 20:5:200;          % from 20ms to 400ms dt = 5ms
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from -80Hz to 80Hz with different steps
        offRes = 5:5:50;           % from 1Hz to 40Hz df = 1Hz
        offRes = offRes - 2.5;
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1  
        
        
	% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
	% SCARCE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 400Hz 
	case 14
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:100:2000; 

        % % 
        % % T2 ranges from:
        T2 = 20:25:200;    
        
        % % 
        % % Off-resonance freq from:
        offRes = 50:50:400;         
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
    % SCARCE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 400Hz 
    % This is used for testing:
    % T1/T2 the same range as case "14" but values chosen in between
    % df fixed
    case 141
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:100:2000; 
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from:
        T2 = 20:25:200;    
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from:
        offRes = 50:50:400;         
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
    
	% SCARCE SAMPLING OF TISSUE SPACE
    % T1 <= 2000, T2 <= 200, df <= 400Hz 
    % This is used for testing:
    % T1/T2/df the same range as case "14" but values chosen in between
    case 142
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:100:2000; 
        T1 = T1(2:2:end) - 10;

        % % 
        % % T2 ranges from:
        T2 = 20:25:200;    
        T2 = T2(2:2:end) - 2.5;
        
        % % 
        % % Off-resonance freq from:
        offRes = 50:50:400;         
        offRes = offRes - 25;
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1

        
	% Uninteresting test: SCARCE SAMPLING OF TISSUE SPACE
    % T1 <= 5000, T2 <= 3000, df <= 400Hz
    % T1/T2/df are scarcely sampled for big balues
    case 22
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 2000:300:5000;

        % % 
        % % T2 ranges from:
        T2 = 200:200:3000; 
        
        % % 
        % % Off-resonance freq from:
        offRes = 50:50:400;         
        offRes = [-offRes(end:-1:1) 0 offRes];  % added the negative side and 0Hz
        offRes = offRes .* 1e-03;     % Keep in kHz because kHz = ms^-1
        
    % Uninteresting test: SCARCE SAMPLING OF TISSUE SPACE
    % T1 <= 5000, T2 <= 3000, df <= 400Hz
    % T1/T2/df are scarcely sampled for big balues
    % This is used for testing:
    % T1/T2 the same range as case "22" but values chosen in between
    % df is kept fixed
    case 222
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 2000:300:5000;
        T1 = T1(2:2:end) - 150;

        % % 
        % % T2 ranges from:
        T2 = 200:200:3000; 
        T2 = T2(2:2:end) - 100;
        
        % % 
        % % Off-resonance freq from:
        offRes = 50:50:400;         
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];  % added the negative side and 0Hz
        offRes = offRes .* 1e-03;     % Keep in kHz because kHz = ms^-1
        
    % Uninteresting test: SCARCE SAMPLING OF TISSUE SPACE
    % T1 <= 5000, T2 <= 3000, df <= 400Hz
    % T1/T2/df are scarcely sampled for big balues
    % This is used for testing:
    % T1/T2/df the same range as case "22" but values chosen in between
    case 223
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 2000:300:5000;
        T1 = T1(2:2:end) - 150;

        % % 
        % % T2 ranges from:
        T2 = 200:200:3000; 
        T2 = T2(2:2:end) - 100;
        
        % % 
        % % Off-resonance freq from:
        offRes = 50:50:400;         
        offRes = offRes - 25;
        offRes = [-offRes(end:-2:2) 0 offRes(2:2:end)];  % added the negative side and 0Hz
        offRes = offRes .* 1e-03;     % Keep in kHz because kHz = ms^-1
        
        
	% DENSE SAMPLING OF TISSUE SPACE, but every other 10th entry missing
    % T1 <= 2000, T2 <= 200, df <= 50Hz
    case 44
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:10:2000;  
        T1temp = T1(10:10:end-1);
        T1 = setdiff(T1, T1temp);

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        T2temp = T2(10:10:end-1);
        T2 = setdiff(T2, T2temp);
        
        % % 
        % % Off-resonance freq from:
        offRes = 2.5:2.5:50; %0.1:0.1:2;         
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offResTemp = offRes(10:10:end-1);
        offRes = setdiff(offRes, offResTemp);
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
    % DENSE SAMPLING OF TISSUE SPACE, but keep only every other 10th entry
    % T1 <= 2000, T2 <= 200, df <= 50Hz
    % Test data for case "44"
    % Having only every other 10th for T1, T2 
    % keep df fixed as case "44"
    case 441
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:10:2000;  
        T1 = T1(10:10:end-1);

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        T2 = T2(10:10:end-1);
        
        % % 
        % % Off-resonance freq from:
        offRes = 2.5:2.5:50; %0.1:0.1:2;         
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offResTemp = offRes(10:10:end-1);
        offRes = setdiff(offRes, offResTemp);
        offRes = offRes(2:4:end);
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
	% DENSE SAMPLING OF TISSUE SPACE, but keep only every other 10th entry
    % T1 <= 2000, T2 <= 200, df <= 50Hz
    % Test data for case "44"
    % Having only every other 10th for T1, T2, df
	case 442
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:10:2000;  
        T1 = T1(10:10:end-1);

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;    
        T2 = T2(10:10:end-1);
        
        % % 
        % % Off-resonance freq from:
        offRes = 2.5:2.5:50; %0.1:0.1:2;         
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offRes = offRes(10:10:end-1);
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
    % EXTRAPOLATION - Finely sampled
    % T1 <= 1000, T2 <= 200, df <= 1Hz
    case 55
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 100:10:2000;  

        % % 
        % % T2 ranges from:
        T2 = 20:5:200;     
        
        % % 
        % % Off-resonance freq from -80Hz to 80Hz with different steps
        offRes = 0.1:0.1:1;           % from 1Hz to 40Hz df = 1Hz
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) 0 offRes];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
	% EXTRAPOLATION - Finely sampled
    % Test data with:
    % T1 > 1000, T2 > 200
    % df fixed
    case 551
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 2100:10:2300; 

        % % 
        % % T2 ranges from:
        T2 = 205:5:250;
        
        % % 
        % % Off-resonance freq from:
        offRes = 0.1:0.1:1;
        % added the negative side and 0Hz
        offRes = [-offRes(end:-2:2) offRes(2:2:end)];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
	% EXTRAPOLATION - Finely sampled
    % Test data with:
    % T1 > 1000, T2 > 200, df > 1Hz
    case 552
        % Create ranges for T1, T2 and off-resonance frequency

        % % 
        % % T1 ranges from:
        T1 = 1100:10:1300; 

        % % 
        % % T2 ranges from:
        T2 = 205:5:250;
        
        % % 
        % % Off-resonance freq from:
        offRes = 1.1:0.1:1.5;
        % added the negative side and 0Hz
        offRes = [-offRes(end:-1:1) offRes];
        offRes = offRes .* 1e-03;  % Keep in kHz because kHz = ms^-1
        
end


% % LAST STEP: Populate structure
materialProperties.T1 = T1; 
materialProperties.T2 = T2; 
materialProperties.offRes = offRes;

% % % % % % % % % 
% Plot the values
if nargin > 0 && flagPlot
    figure
    
    % T1
    subplot(1,3,1)
    scatter(1:length(T1), T1)
    title('T_1')
    xlim([0 length(T1)+1]);
    ylim([min(T1)-1 max(T1)+1]);
    grid on
    
    % T2
    subplot(1,3,2)
    scatter(1:length(T2), T2)
    title('T_2')
    xlim([0 length(T2)+1]);
    ylim([min(T2)-1 max(T2)+1]);
    grid on
    
    % T2
    subplot(1,3,3)
    scatter(1:length(offRes), offRes)
    title('\Delta f')
    xlim([0 length(offRes)+1]);
    ylim([min(offRes)-1 max(offRes)+1]);
    grid on
end

clear T1 T2 offRes

end