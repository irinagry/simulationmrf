function TRs = generateRepetitionTimesPerlinBSSFP(N, lowTR, highTR, flagToPlot)
% IRINA GRIGORESCU
% Date Created: 24-03-2017
% Date Updated: 24-03-2017
%
% Function that generates repetition times in a Perlin smooth fashion in 
% the [lowTR, highTR] range.
% 
% INPUTS:
%   N = number of data points
%   lowTR  = lowest TR 
%   highTR = highest TR
%
% OUPUTS:
%   TRs = (1xN) values of flip angles
%               generated between [lowTR, highTR]

if nargin < 4
    flagToPlot = 0;
end

% Create 1xN array of repetition times generate between [lowTR, highTR]
% with Perlin noise
M = ceil(1/100*N); % 1% of N
if M < 2
    M = 2;
end
TRs = perlin(N, M, lowTR, highTR);

if flagToPlot
    figure,
    plot(TRs);
    title('TRs')
    axis square
    daspect([20 1 1])
    xlim([0 N])
    ylim([min(TRs)-1 max(TRs)+1])
    grid on
end

end