function sequenceProperties = retrieveSequenceProperties(N, flagPlot, ...
                                seqFlag, customSEQ)
% % % % IRINA GRIGORESCU
% % % % Date created: 02-03-2017
% % % % Date updated: 15-03-2017
% % % % 
% % % % INPUTS: 
% % % %     N = number of timepoints
% % % %     flagPlot = a flag set to 1 if you want to plot
% % % %     seqFlag = a flag for how to generate the sequence parameters
% % % %         0 - sinusoidal FA, 0 PhA, perlin TR, RO = 2ms
% % % %         1 - urandom FA, alternate PhA, urandom TR, RO = TR./2
% % % %         2 - perlin noise FA, alternate PhA, urandom TRs, RO = TR./2
% % % %         3 - sinusoidal FA, alternate PhA, perlin TRs, RO = TR./2
% % % %         4 - custom FA, alternate PhA, customTRs, custom ROs
% % % %         5 - custom FA, custom PhA, customTRs, custom ROs
% % % %         6 - sinusoidal FA, 0 PhA, fixed TRs, fixed ROs
% % % %     customSEQ = (struct) with FA,TR,RO
% % % % 
% % % % OUTPUTS:
% % % %     sequenceProperties = (struct) with
% % % %         RF = RF pulse (struct) with
% % % %              FA  = flip angle (in deg) (Nx1 array) and 
% % % %              PhA = phase angle (in deg) (Nx1 array)
% % % %         TR = repetition time (in ms) (Nx1 array)
% % % %         RO = read-out time (in ms) (Nx1 array)
% % % % 

% % 0.
% % PREREQUISITES
% Structure for sequence
sequenceProperties = struct;

% % For each type of sequence generation
switch seqFlag
    
    % MRF-FISP values
    case 0
        % Sinusoidal MRF-FISP type FAs
        FA = generateFlipAnglesSinusoidalFISP(N);
        
        % Perlin TRs
        lowTR = 11.5; highTR = 14.5;
        TR = generateRepetitionTimesPerlinBSSFP(N, lowTR, highTR);
        
        % Phase angles = 0
        PhA = zeros(N,1);
        
        % Read out values are 2ms everywhere
        RO  = zeros(N,1) + 2;
        
    % URandom everywhere
    case 1 
        % Uniformly Random FAs [0,90]
        lowFA = 0; highFA = 90;
        FA = lowFA + (highFA - lowFA) .* rand(N,1);
        
        % Uniformly Random TRs [10.5ms, 14ms] 
        lowTR = 10.5; highTR = 14;
        TR = lowTR + (highTR - lowTR) .* rand(N,1);
        
        % Alternate between 0 and 180 Phase Angles, starting with 0
        PhA = zeros(N,1);
        PhA(2:2:end) = 180;
        
        % Read out values are TR./2 everywhere
        RO  = TR./2;
        
	% Perlin FA, urandom TRs, alternate FAs, RO=TR/2
    case 2
        % Perlin noise FAs [20,80]
        lowFA = 20; highFA = 80;
        FA = generateFlipAnglesPerlinBSSFP(N, lowFA, highFA, flagPlot);
        
        % Uniformly Random TRs [10.5ms, 14ms] 
        lowTR = 10.5; highTR = 14;
        TR = lowTR + (highTR - lowTR) .* rand(N,1);
        
        % Alternate between 0 and 180 Phase Angles, starting with 0
        PhA = zeros(N,1);
        PhA(2:2:end) = 180;
        
        % Read out values are TR./2 everywhere
        RO  = TR./2;
            
	% sinusoidal FA, alternate PhA, perlin TRs, RO = TR./2
    case 3
        % Sinusoidal FAs between [0,90]
        flagRandom = 0; % with randomness around it if == 1
        FA = generateFlipAnglesSinusoidalBSSFP(N, flagRandom);
        
        % Perlin noise TRs [10.5ms, 14ms] 
        % % % % % TODO: sort of implemented
        lowTR = 10.5; highTR = 14;
        TR = generateRepetitionTimesPerlinBSSFP(N, lowTR, highTR, flagPlot);
        
        % Alternate between 0 and 180 Phase Angles, starting with 0
        PhA = zeros(N,1);
        PhA(2:2:end) = 180;
        
        % Read out values are TR./2 everywhere
        RO  = TR./2;    
        
    % custom FA, alternate PhA, customTRs, custom RO
    case 4
        % Verify existence of 4th parameter
        if nargin < 4
            warning(['You did not provide a custom sequence.', ...
                     'Restoring to default values: FA = ', ...
                     num2str(customSEQ.RF.FA), ' TR = ', ...
                     num2str(customSEQ.TR), ' RO = half TR']);
            customSEQ.RF.FA = 10;
            customSEQ.TR = 400;
            customSEQ.RO = customSEQ.TR/2;
        else
            % Custom FAs
            FA = zeros(N,1) + customSEQ.RF.FA;

            % Custom TRs
            TR = zeros(N,1) + customSEQ.TR;

            % Alternate between 0 and 180 Phase Angles, starting with 0
            PhA = zeros(N,1);
            PhA(2:2:end) = 180;

            % Custom RO
            RO  = zeros(N,1) + customSEQ.RO;
        end
        
	% custom FA, custom PhA, customTRs, custom RO
    case 5
        % Verify existence of 4th parameter
        if nargin < 4
            warning(['You did not provide a custom sequence.', ...
                     'Restoring to default values: FA = ', ...
                     num2str(customSEQ.RF.FA), ' TR = ', ...
                     num2str(customSEQ.TR), ' RO = half TR']);
            customSEQ.RF.FA = 10;
            customSEQ.TR = 400;
            customSEQ.RO = customSEQ.TR/2;
        else
            % Custom FAs
            FA = zeros(N,1) + customSEQ.RF.FA;

            % Custom TRs
            TR = zeros(N,1) + customSEQ.TR;

            % PhA = 0 all the way
            PhA = zeros(N,1) + customSEQ.RF.PhA;

            % Custom RO
            RO  = zeros(N,1) + customSEQ.RO;
        end
        
        
    % sinusoidal FA, 0 PhA, fixed TRs, fixed ROs
    case 6
        % Sinusoidal FAs between [0,90]
        flagRandom = 1; % with randomness around it if == 1
        FA = generateFlipAnglesSinusoidalBSSFPSmallerPeriod(N, flagRandom);
        
        % TRs = 15ms
        TR  = zeros(N,1) + 15;
        
        % PhA = 0 all the way
        PhA = zeros(N,1);
        
        % ROs = 6ms
        RO  = zeros(N,1) + 6;
end

% % LAST STEP: Populate structure
sequenceProperties.RF.FA  = FA;
sequenceProperties.RF.PhA = PhA;
sequenceProperties.TR  = TR;
sequenceProperties.RO  = RO;

% % % % % % % % % 
% % PLOT VALUES IF FLAG SET
if nargin > 1 && flagPlot
    figure
    
    if ~exist('lowFA','var') || ~exist('highFA','var')
        lowFA = min(FA); highFA = max(FA);
        if lowFA == highFA
            lowFA = 0; highFA = 100;
        end
    end
    
    if ~exist('lowTR','var') || ~exist('highTR','var')
        lowTR = min(TR); highTR = max(TR);
        if lowTR == highTR
            lowTR = 10; highTR = 30;
        end
    end
    
    % FAs
    subplot(1,2,1)
    plot(1:N, FA)
    title('FA (and phase angle)')
    ylim([lowFA highFA]);
    
    % TRs
    subplot(1,2,2)
    plot(1:N, TR)
    title('TR')
    ylim([lowTR highTR]);
end

clear FA TR lowFA lowTR highFA highTR

end

