function FA = generateFlipAnglesSinusoidalFISP(N)
% IRINA GRIGORESCU
% 
% Date created: 24-08-2017
% Date updated: 24-08-2017
% 
% This script will generate the FAs in a sinusoidal fashion
% 
% INPUTS:
%     N = number of data points
% 
% OUTPUTS:
%     FA = (Nx1 array) of flip angles
% 

    Nrf   = 200;    % periods of 200 values
    maxFA =  85; 
    minFA =   5;

    % Function handle that generates FAs with periodicity
    % periodicity
    randForFAs = @(n) minFA + sin(n*pi/Nrf) .* ...
                      ((maxFA-minFA).*rand + minFA);

	% t goes from 1 to Nrf
    t     = 1:Nrf;  
    FA    = [];

    % How many times to repeat the periodicity
    for i = 1:ceil(N/210)
        FArand = randForFAs(t);
        FA     = [FA ; FArand.' ; zeros(10,1)];
    end
    FA = FA(1:N);
    FA(FA<0) = 0;


end