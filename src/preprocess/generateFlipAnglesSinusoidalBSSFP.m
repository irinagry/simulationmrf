function FA = generateFlipAnglesSinusoidalBSSFP(N, flagRandom)
% IRINA GRIGORESCU
% 
% Date created: 15-03-2017
% Date updated: 15-03-2017
% 
% This script will generate the FAs in a sinusoidal fashion
% For the bSSFP paper
% 
% INPUTS:
%     N = number of data points
%     flagRandom = whether to have randomness around the FAs
%                  Should take either 0 or 1 values 
%                  0 (no) 1(yes) 
% 
% OUTPUTS:
%     FA = (Nx1 array) of flip angles
% 

    % Function handle that generates FAs with periodicity
    % periodicity   +   randomness around current value 
    randForFAs = @(t, period) ...
        10 + rad2deg(sin((2*pi/500) .* t)') + ...
        flagRandom.*(-5 + 10.*rand(period,1));

    period = 250;   % periods of 250 values
    t = 1:period;   % t goes from 1 to 250
    FA = [];

    % How many times to repeat the periodicity
    for i = 1:ceil(N/500)
        FArand = randForFAs(t,period);
        FA     = [FA ; FArand ; zeros(50,1) ; FArand./2 ; zeros(50,1)];
    end
    FA = FA(1:N);
    FA(FA<0) = 0;


end