% % % % This script creates the material properties
% % % % for my GPU simulations
% % % % It consists of a 7 column matrix with:
% % % % T1, T2, PD, DW, posx, posy, posz
% % % %
% % % % Author: Irina Grigorescu, irinagry@gmail.com

% Number of isochromats
Nisochromats  = 101;

% T1, T2 relaxation times
T1   =  repmat(1000, [Nisochromats,1]);
T2   =  repmat( 100, [Nisochromats,1]);

% Proton density values
PD   =  ones(Nisochromats,1);

% Off-resonance frequency
DW   = zeros(Nisochromats,1); %linspace(-0.1,0.1,N).'

% Position of isochromats
posx = linspace(-0.5,0.5,Nisochromats).';
posy = zeros(Nisochromats,1);
posz = zeros(Nisochromats,1);

% Write to file
dlmwrite('/Users/irina/OneDrive - University College London/Work/Simulation/simulationGPU/input/input_material_properties', ...
          [T1, T2, PD, DW, posx, posy, posz], 'delimiter',' ');