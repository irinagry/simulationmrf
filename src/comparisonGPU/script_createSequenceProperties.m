% % % % This script creates the sequence properties
% % % % for my GPU simulations
% % % % It consists of a 7 column matrix with:
% % % % dt, FA, PA, GxCumm, GyCumm, GzCumm, RO
% % % %
% % % % Author: Irina Grigorescu, irinagry@gmail.com

% Constants
gammabar = (42.58*1e03);    %kHz/T
gamma    = (2*pi*gammabar);

% Sequence timepoints
Nsequencetp = 100;

% Delta ts
dt        = zeros(1, Nsequencetp);
dt(2:end) = dt(2:end) + 7.5;

% Flip angles
FA    = repmat([90,0], [1, ceil(Nsequencetp/2)]); FA = FA(1:Nsequencetp);
FA(FA>0) = FA(FA>0);% - 10*rand(1,length(FA(FA>0)));

% Phase angles
PA    = zeros(1, Nsequencetp);

% Gx gradient moment
GxMom = repmat([0, 1/gammabar],[1,ceil(Nsequencetp/2)]);
GxMom(2:end) = GxMom(1:Nsequencetp-1);

% Gy gradient moment
GyMom = zeros(1,Nsequencetp);

% Gz gradient moment
GzMom = zeros(1,Nsequencetp);

% Readout points
RO    = repmat([0,1],[1,ceil(Nsequencetp/2)]); RO = RO(1:Nsequencetp);

% Merge them
mySequence = [dt ; FA ; PA ; GxMom ; GyMom ; GzMom ; RO];

% Write to file
dlmwrite('/Users/irina/OneDrive - University College London/Work/Simulation/simulationGPU/input/input_sequence_properties', ...
         mySequence.', ' ');
     
     
     
     
     
     
     
     
     