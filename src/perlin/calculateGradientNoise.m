function gradNoise = calculateGradientNoise(x, perm, TABMASK, gradientTab)
% % % % IRINA GRIGORESCU
% % % % 
% % % % This function returns the value of gradient Noise at coord x
% % % % based on the table of previously computed PRNs
% % % % for the gradient lattice

% Function handle for smoothing
smoothstep = @(x) (x.^2) .* (3 - 2.* x);

% Function handle for a different smoothing
smoothstep2 = @(x) x .* x .* x .* (x .* ( x.* 6 - 15) + 10);

% Function handle for linear interpolation
lerp = @(t,y0,y1) (y0 + t.*(y1-y0));

% Gradient lattice function handle
% Dot product between lattice gradient 
% and fractional part of the input point relative to the lattice point
glattice = @(ix, fx) gradientTab(perm(bitand(ix-1, TABMASK)+1)+1) .* fx;

% Smoothed linear interpolation between 2 glattice values and their
% fractional parts

ix  = floor(x); % Find left integer lattice point
fx0 = x - ix;   % Fractional part from left integer to point
fx1 = fx0 - 1;  % Fractional part from right integer to point
                % (always negative)
wx0  = smoothstep(fx0); % Weight used for smoothing 
% wx1  = smoothstep(fx1); 

vx0 = glattice(ix  , fx0); % Dot product between left and left fract
vx1 = glattice(ix+1, fx1); % Dot product between right and right fract

% Gradient noise at position x is
% calculated as a smoothed linear interpolation 
gradNoise = lerp(wx0, vx0, vx1); 





