figure,
[xax, yax, zax] = createAxis();
% Viewer position
view([45 45 10]);

pos = 271; %15^o = 196 / 211

[betas Ns coords] = size(subsetOfMRF1);


subsetAll = zeros(betas,3*Ns,coords);
for i = 1:Ns
    subsetAll(:,(i-1)*3+1,:) = subsetOfMRF1(:,i,:);
    subsetAll(:,(i-1)*3+2,:) = subsetOfMRF2(:,i,:);
    subsetAll(:,(i-1)*3+3,:) = subsetOfMRF3(:,i,:);
end

% Plot connecting points
plot3(subsetAll(pos,:,1), ...
      subsetAll(pos,:,2), ...
      subsetAll(pos,:,3), 'k')

% Plot points of different colors for all
hold on
scatter3(subsetOfMRF1(pos,:,1), ...
             subsetOfMRF1(pos,:,2), ...
             subsetOfMRF1(pos,:,3), 'k')
hold on    
scatter3(subsetOfMRF2(pos,:,1), ...
             subsetOfMRF2(pos,:,2), ...
             subsetOfMRF2(pos,:,3), 'r')

hold on
scatter3(subsetOfMRF3(pos,:,1), ...
             subsetOfMRF3(pos,:,2), ...
             subsetOfMRF3(pos,:,3), 'b')

% xlim([-1.5, 1.5])
% ylim([-1.5, 1.5])
% zlim([-1.5, 1.5])


