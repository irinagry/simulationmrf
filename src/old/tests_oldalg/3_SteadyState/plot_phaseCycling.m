function plot_phaseCycling(betaPrecessionTR, ...
            subsetOfMRF1, subsetOfMRF2, subsetOfMRF3)
% % % % IRINA GRIGORESCU
% % % % Date created: 20-03-2017
% % % % Date updated: 20-03-2017
% % % % 
% % % % This function plots the signal amplitude for a range of
% % % % off-resonance frequencies and for different 
% % % % 
% % % % INPUT:
% % % %     betaPrecessionTR = 1 x N_df array
% % % %         set of resonance offset angles 
% % % %     
% % % %     subsetOfMRF1/2/3 = N_df x N_TRs x 3 array
% % % %         component-wise signal evolution for the entire set of
% % % %         off-res frequencies used and events in the sequence
% % % %         1 = df = 0   (0 -   0 -   0 -   0 - 0 - ...)
% % % %         2 = df = 90  (0 -  90 - 180 - 270 - 0 - ...)
% % % %         3 = df = 180 (0 - 180 -   0 - 180 - 0 - ...)

    [N_off, N_ev, N_vect] = size(subsetOfMRF1);
    figure,

    % Signal Amplitude
    subplot(2,1,1)
    plot(betaPrecessionTR, ...
         abs(subsetOfMRF1(:,N_ev,1) + 1i.*subsetOfMRF1(:,N_ev,2)), 'b-')
    hold on
    plot(betaPrecessionTR, ...
         abs(subsetOfMRF2(:,N_ev,1) + 1i.*subsetOfMRF2(:,N_ev,2)), 'g--')
    hold on
    plot(betaPrecessionTR, ...
         abs(subsetOfMRF3(:,N_ev,1) + 1i.*subsetOfMRF3(:,N_ev,2)), 'r--')
    
    grid on
    xlabel({'\beta(TR)', 'resonance offset angle', ...
            'Precession per TR (deg)'})
    ylabel('Signal Magnitude')
    legend({'\Delta \phi = 0','\Delta \phi = 90','\Delta \phi = 180'})
    xlim([-250 250])

    % Signal phase
    subplot(2,1,2) 
    
    % d phi = 0 
    phaseCycle1 = rad2deg(atan2(...
        subsetOfMRF1(:,N_ev,2), ...
       -subsetOfMRF1(:,N_ev,1)));
    scatter(betaPrecessionTR, phaseCycle1, '.b');
    hold on
    
    % d phi = 90
    phaseCycle2 = rad2deg(atan2(...
        subsetOfMRF2(:,N_ev,2), ...
       -subsetOfMRF2(:,N_ev,1)));
%     phaseCycle2(2:4:end) = rad2deg(atan2(...
%         -subsetOfMRF2(2:4:end,N_ev,1), ...
%         -subsetOfMRF2(2:4:end,N_ev,2)));
%     
%     phaseCycle2(3:4:end) = rad2deg(atan2(...
%         -subsetOfMRF2(3:4:end,N_ev,2), ...
%          subsetOfMRF2(3:4:end,N_ev,1)));
%     
%      phaseCycle2(4:4:end) = rad2deg(atan2(...
%          subsetOfMRF2(4:4:end,N_ev,1), ...
%          subsetOfMRF2(4:4:end,N_ev,2)));
     
    scatter(betaPrecessionTR, phaseCycle2, '.g');
    hold on
    
    % d phi = 180
    phaseCycle3 = rad2deg(atan2(...
        subsetOfMRF3(:,N_ev,2), ...
       -subsetOfMRF3(:,N_ev,1)));
%     phaseCycle3(2:2:end) = rad2deg(atan2(...
%         -subsetOfMRF3(2:2:end,N_ev,1), ...
%         -subsetOfMRF3(2:2:end,N_ev,2)));
    
    scatter(betaPrecessionTR, phaseCycle3, '.r');
    
    grid on
    xlabel({'\beta(TR)', 'resonance offset angle', ...
            'Precession per TR (deg)'})
    ylabel('Signal Phase')
    legend({'\Delta \phi = 0','\Delta \phi = 90','\Delta \phi = 180'})
    axis([-250 250 -200 200])
    disp('phaseCycling')
end