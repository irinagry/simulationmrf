function plot_phasePrecessionPerTR(betaPrecessionTR, subsetOfMRF)
% % % % IRINA GRIGORESCU
% % % % Date created: 20-03-2017
% % % % Date updated: 20-03-2017
% % % % 
% % % % This function plots the signal amplitude for a range of
% % % % off-resonance frequencies 
% % % % 
% % % % INPUT:
% % % %     betaPrecessionTR = 1 x N_df array
% % % %         set of resonance offset angles 
% % % %     
% % % %     subsetOfMRF = N_df x N_TRs x 3 array
% % % %         component-wise signal evolution for the entire set of
% % % %         off-res frequencies used and events in the sequence

    [N_off, N_ev, N_vect] = size(subsetOfMRF);
    figure,

    subplot(2,1,1)
    plot(betaPrecessionTR, ...
         abs(subsetOfMRF(:,N_ev,1) + 1i.*subsetOfMRF(:,N_ev,2)))
    grid on
    xlabel({'\beta(TR)', 'resonance offset angle', ...
            'Precession per TR (deg)'})
    ylabel('Signal Magnitude')

    subplot(2,1,2) 
    scatter(betaPrecessionTR, ...
        rad2deg(atan2(...
            subsetOfMRF(:,N_ev,2), ...
           -subsetOfMRF(:,N_ev,1))), ...
        '.k')
    grid on
    xlabel({'\beta(TR)', 'resonance offset angle', ...
            'Precession per TR (deg)'})
    ylabel('Signal Phase')
    % axis([-250 250 -200 200])
    
end