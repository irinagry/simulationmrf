function [sequenceProperties, materialProperties, materialOfInterest] = ...
            test_phasePrecessionPerTR(configParams, N)
% % % % Author: IRINA GRIGORESCU
% % % % Date created: 17-03-2017
% % % % Date updated: 20-03-2017
% % % % 
% % % % This function creates a dictionary for a small set of tissue
% % % % properties to demonstrate the evolution of the 
% % % % amplitude and phase of the signal at TR=TE/2 
% % % % for a range of off-resonance values
% % % % 
% % % % INPUT:
% % % %     configParams = (struct) with the fields (at least): 
% % % %                    .tissue.cst.t1 (cst == custom tissue)
% % % %                    .tissue.cst.t2
% % % %                    .tissue.cst.df
% % % %     N = number of timepoints for the sequence
% % % % 
% % % % OUTPUT: 
% % % %     sequenceProperties = the sequence used (struct) with
% % % %         RF = RF pulse (struct) with
% % % %              FA  = flip angle (in deg) (Nx1 array) and 
% % % %              PhA = phase angle (in deg) (Nx1 array)
% % % %         TR = repetition time (in ms) (Nx1 array)
% % % %         RO = read-out time (in ms) (Nx1 array)
% % % % 
% % % %     materialProperties = (struct) with
% % % %         T1 (double)
% % % %         T2 (double)
% % % %         offRes (array double)
% % % %     
% % % %     materialOfInterest = (struct) with the fields (at least): 
% % % %                    .cst.t1 (cst == custom tissue)
% % % %                    .cst.t2
% % % %                    .cst.df



% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % CUSTOM SEQUENCE 
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
customValuesSequence.RF.FA  =   70;
customValuesSequence.RF.PhA =   90; % +y
customValuesSequence.TR  =   20;
customValuesSequence.RO  =  customValuesSequence.TR/2;
% % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial.T1     = 20 * customValuesSequence.TR; 
customValuesMaterial.T2     = 15 * customValuesSequence.TR; 
betaPrecessionTR            = -400:400; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR); 

% % % % % % % %
% % % % RUN MRF FOR THE CUSTOM VALUES PROVIDED
tic
[materialProperties, sequenceProperties, ...
    dictionaryMRF] = simulateMRF(N, 0, ...
                             SEQFLAG, customValuesSequence, ...
                             MATFLAG, customValuesMaterial);
toc

% % % % % % % %
% % % % Let's extract the signal for a given tissue but for all df values
% Extract indices for a custom material of interest 
materialOfInterest = configParams.tissue;
materialOfInterest.cst.t1 = customValuesMaterial.T1;
materialOfInterest.cst.t2 = customValuesMaterial.T2;
materialOfInterest.cst.df = customValuesMaterial.offRes;
tissueIndices = extractIndices(materialProperties, materialOfInterest);

% % % dictionaryMRF(N_T1 x N_T2 x N_offRes x N_timepoints x 3 array)
subsetOfMRF = squeeze(dictionaryMRF(...
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));


% % % % % % % %
% % % % Plot the signal amplitude and phase at TE = TR/2
% for the range of precession angles per TR generated earlier
plot_phasePrecessionPerTR(betaPrecessionTR, subsetOfMRF);


end