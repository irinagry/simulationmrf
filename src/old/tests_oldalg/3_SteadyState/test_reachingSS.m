function test_reachingSS(params)
% % % % 
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 16-03-2017
% % % % Date updated: 16-03-2017
% % % % 
% % % % TEST x : multiple blocks
% % % %          Test how the signal reaches steady state for a beta = pi
% % % % 
% % % % INPUT:
% % % %     
% % % % 
% % % % 

% % % % % % % % 
% % % % Sequence parameters
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence

% % % % % % % %
% % % % Generate Dictionary for a given number of N timepoints
N = 60;      % timepoints for FAs and TRs of the sequence
customValuesSequence.TR.FA  =  90;
customValuesSequence.TR.PhA =   0;
customValuesSequence.TR  =  20;
customValuesSequence.RO  =  customValuesSequence.TR./2;

tic
[materialProperties, sequenceProperties, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, SEQFLAG, customValuesSequence);
toc

% Extract indices for WM/GM/CSF from the materialProperties set
% USED here is params.tissue for 
tissueIndices = extractIndices(materialProperties, params.tissue);

% % % % % % % %
% % Visualise signal evolution for this sequence
N_events = 60;
% Relaxation Times
T1 = materialProperties.T1(tissueIndices.wm{1});
T2 = materialProperties.T2(tissueIndices.wm{2});
% Off-resonance frequency
df = materialProperties.offRes(tissueIndices.wm{3});
% Timestep for update
dt = 5e-01; 

% % % % RUN SIMULATION
% % % % Calculate event history of the sequence being applied to the vector
[ mu, b1field, eventMatrix, newFAs, phAngles, newTRs ] = ...
    calculateVectorHistory(sequenceProperties.RF.FA, ...
                           sequenceProperties.RF.PhA, ...
                           sequenceProperties.TR, ...
                           sequenceProperties.RO, ...
                           T1, T2, df, dt, N_events);
    
% % % % % Plotting
plotSequence(eventMatrix, ...
             mu, b1field, newFAs, ...
             T1, T2, df, dt, N_events, 30, 0);


end