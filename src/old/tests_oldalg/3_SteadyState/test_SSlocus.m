function test_SSlocus(N)
% % % % Author: IRINA GRIGORESCU
% % % % Date created: 23-03-2017
% % % % Date updated: 23-03-2017
% % % % 
% % % % This function creates a dictionary for a small set of tissue
% % % % properties to demonstrate the evolution of the 
% % % % magnetic moment vector at TE = 0, TE = TR/2 and TE = TR 
% % % % for beta = [-pi,pi]
% % % % 
% % % % The purpose of this test is to look at how the T1/T2 ratio
% % % % influences the eccentricity of the locus.
% % % % Validation will be done against Gary's analytical solutions
% % % % 
% % % % INPUT:
% % % %     N = number of timepoints for the sequence
% % % % 

% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % CUSTOM SEQUENCE 
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence

% % % %
% % % % SEQUENCES
% % CUSTOM SEQUENCE for FA = 15
customValuesSequence.RF.FA  =   15;
customValuesSequence.RF.PhA =    0; % B1 field direction +x
customValuesSequence.TR  =    8;
customValuesSequence.RO  =    0;

% % CUSTOM MATERIAL 1 AND 2
% % material 1 has T2 = 50 TR and T1 =  1 T2
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial1.T2     = 15 * customValuesSequence.TR; 
customValuesMaterial1.T1     =  1 * customValuesMaterial1.T2;

betaPrecessionTR             = -180:180; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial1.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR);

% % material 2 has T2 = 50 TR and T1 =  2 T2
customValuesMaterial2    =     customValuesMaterial1;
customValuesMaterial2.T1 = 2 * customValuesMaterial1.T2;
             
% % material 3 has T2 = 50 TR and T1 =  5 T2
customValuesMaterial3    =     customValuesMaterial1;
customValuesMaterial3.T1 = 5 * customValuesMaterial1.T2;

% % material 4 has T2 = 50 TR and T1 =  10 T2
customValuesMaterial4    =      customValuesMaterial1;
customValuesMaterial4.T1 = 10 * customValuesMaterial1.T2;

% % % % % % % %
% % % % RUN MRF FOR THE CUSTOM VALUES PROVIDED

% % % % dictionaryMRF1(N_TEs, N_T1 x N_T2 x N_offRes x N_timepoints x 3)
% % dictionaryMRF-1/2/3/4 for each material
dictionaryMRF1 = zeros(3,1,1,length(betaPrecessionTR),N,3);
dictionaryMRF2 = zeros(3,1,1,length(betaPrecessionTR),N,3);
dictionaryMRF3 = zeros(3,1,1,length(betaPrecessionTR),N,3);
dictionaryMRF4 = zeros(3,1,1,length(betaPrecessionTR),N,3);

% % Do for all three TEs
for i = 1:3
    % % % % % % % % % % % MATERIAL 1
    tic
    [materialProperties1, sequenceProperties, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial1);
    toc
    dictionaryMRF1(i,:,:,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % % % % % % % % MATERIAL 2
    tic
    [materialProperties2, sequenceProperties, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial2);
    toc
    dictionaryMRF2(i,:,:,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % % % % % % % % MATERIAL 3
    tic
    [materialProperties3, sequenceProperties, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial3);
    toc
    dictionaryMRF3(i,:,:,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    % % % % % % % % % % % MATERIAL 4
    tic
    [materialProperties4, sequenceProperties, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial4);
    toc
    dictionaryMRF4(i,:,:,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    
    
    % % % % Change RO
    customValuesSequence.RO = customValuesSequence.RO   + ...
                              customValuesSequence.TR/2 ;
	
    clear sequenceProperties
end


% % % % % % % %
% % % % Let's extract the signal for a given tissue but for all df values
% % % % For first material
materialOfInterest1 = struct;
materialOfInterest1.cst = struct;
materialOfInterest1.cst.t1 = customValuesMaterial1.T1;
materialOfInterest1.cst.t2 = customValuesMaterial1.T2;
materialOfInterest1.cst.df = customValuesMaterial1.offRes;
materialOfInterest2 = materialOfInterest1;
materialOfInterest2.cst.t1 = customValuesMaterial2.T1;
materialOfInterest3 = materialOfInterest1;
materialOfInterest3.cst.t1 = customValuesMaterial3.T1;
materialOfInterest4 = materialOfInterest1;
materialOfInterest4.cst.t1 = customValuesMaterial4.T1;
tissueIndices1 = extractIndices(materialProperties1, materialOfInterest1);
tissueIndices2 = extractIndices(materialProperties2, materialOfInterest2);
tissueIndices3 = extractIndices(materialProperties3, materialOfInterest3);
tissueIndices4 = extractIndices(materialProperties4, materialOfInterest4);


% % % remind me how the data looks like:
% % % dictionaryMRF11(3,N_T1 x N_T2 x N_offRes x N_timepoints x 3 array)
% % % % subsetOfMRFAll(N_mat,N_TEs,N_T1 x N_T2 x N_offRes x N_timepoints x 3 array)

subsetOfMRFAll = zeros(4,3,1,1,length(betaPrecessionTR),N,3);
% % % % % % % % % % MATERIAL 1
% % % Extracting indices of interest (TE=0)
subsetOfMRFAll(1,1,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF1(1,... % TE=0
                            tissueIndices1.cst{1}, ...
                            tissueIndices1.cst{2}, ...
                            tissueIndices1.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR/2)
subsetOfMRFAll(1,2,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF1(2,... % TE=TR/2
                            tissueIndices1.cst{1}, ...
                            tissueIndices1.cst{2}, ...
                            tissueIndices1.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR)
subsetOfMRFAll(1,3,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF1(3,... % TE=TR
                            tissueIndices1.cst{1}, ...
                            tissueIndices1.cst{2}, ...
                            tissueIndices1.cst{3}, ...
                            :, :));
% % % % % % % % % % MATERIAL 2
% % % Extracting indices of interest (TE=0)
subsetOfMRFAll(2,1,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF2(1,... % TE=0
                            tissueIndices2.cst{1}, ...
                            tissueIndices2.cst{2}, ...
                            tissueIndices2.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR/2)
subsetOfMRFAll(2,2,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF2(2,... % TE=TR/2
                            tissueIndices2.cst{1}, ...
                            tissueIndices2.cst{2}, ...
                            tissueIndices2.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR)
subsetOfMRFAll(2,3,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF2(3,... % TE=TR
                            tissueIndices2.cst{1}, ...
                            tissueIndices2.cst{2}, ...
                            tissueIndices2.cst{3}, ...
                            :, :));
% % % % % % % % % % MATERIAL 3
% % % Extracting indices of interest (TE=0)
subsetOfMRFAll(3,1,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF3(1,... % TE=0
                            tissueIndices3.cst{1}, ...
                            tissueIndices3.cst{2}, ...
                            tissueIndices3.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR/2)
subsetOfMRFAll(3,2,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF3(2,... % TE=TR/2
                            tissueIndices3.cst{1}, ...
                            tissueIndices3.cst{2}, ...
                            tissueIndices3.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR)
subsetOfMRFAll(3,3,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF3(3,... % TE=TR
                            tissueIndices3.cst{1}, ...
                            tissueIndices3.cst{2}, ...
                            tissueIndices3.cst{3}, ...
                            :, :));
% % % % % % % % % % MATERIAL 4
% % % Extracting indices of interest (TE=0)
subsetOfMRFAll(4,1,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF4(1,... % TE=0
                            tissueIndices4.cst{1}, ...
                            tissueIndices4.cst{2}, ...
                            tissueIndices4.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR/2)
subsetOfMRFAll(4,2,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF4(2,... % TE=TR/2
                            tissueIndices4.cst{1}, ...
                            tissueIndices4.cst{2}, ...
                            tissueIndices4.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR)
subsetOfMRFAll(4,3,:,:,:,:,:) = ... 
                        squeeze(dictionaryMRF4(3,... % TE=TR
                            tissueIndices4.cst{1}, ...
                            tissueIndices4.cst{2}, ...
                            tissueIndices4.cst{3}, ...
                            :, :));

% % % % % % % %
% % % % Plot: 
% 1. If it reached steady state
% 2. The 3d plot of the steady transient signal for a few
% precession angles per TR generated earlier

figureIdx = 333;
figHandle = figure(figureIdx);
set(figHandle, 'Position', [10, 30, 1000, 800]);

subplotRows = 2;
subplotCols = 2;


T1 = [customValuesMaterial1.T1 customValuesMaterial2.T1 ...
      customValuesMaterial3.T1 customValuesMaterial4.T1];
T2 = [customValuesMaterial1.T2 customValuesMaterial2.T2 ...
      customValuesMaterial3.T2 customValuesMaterial4.T2];
T1T2ratio = [1 2 5 10];
         
legendText  = {};

flagPlotSS = 1;

% % % % PLOT
for i = 1:4
    
    % Plot locus computationally and analytically
    plot_SSlocus(betaPrecessionTR, ...
      squeeze(subsetOfMRFAll(i,1,:,:,:,:,:)), ... % M+
      squeeze(subsetOfMRFAll(i,2,:,:,:,:,:)), ... % M TE=TR/2
      squeeze(subsetOfMRFAll(i,3,:,:,:,:,:)), ... % M-
            T1T2ratio(i), ...         % plotting for a T1/T2 ratio
            T1(i), ...
            T2(i), ...
            customValuesSequence.TR, ...
            customValuesSequence.RF.FA, ...
            figureIdx, ...   %subplotRows,subplotCols,subplotid
            subplotRows, subplotCols, 1, flagPlotSS);  % ...,toPlotSSFlag
    

    % Place legend for second figure
    if flagPlotSS
        legendText = [legendText ...
            {['T_1 / T_2 = ', num2str(T1T2ratio(i))]} ];
        figure(figureIdx+1)
        legend(legendText);
    end
    
    % Pause to see it happening
    pause(2)
        
end
        
% figure(figureIdx)
% subplot(subplotRows,subplotCols,1)
% legend(legendText);
% subplot(subplotRows,subplotCols,2)
% legend(legendText);
        
disp(['DONE TEST: Locus plotted for different values of T1/T2 ratio']);

end