function plot_SSlocus(betaPrecessionTR, ...
            subsetOfMRF1, subsetOfMRF2, subsetOfMRF3, ...
            T1T2ratio, T1, T2, TR, FA, ...
            figureIdx, ...
            subplotRows, subplotCols, subplotIdx, ...
            flagPlotSS)
% % % % IRINA GRIGORESCU
% % % % Date created: 23-03-2017
% % % % Date updated: 23-03-2017
% % % % 
% % % % This function plots in 3D the vector position at steady-state 
% % % % for a range of resonance offset angles
% % % % 
% % % % INPUT:
% % % %     betaPrecessionTR = 1 x N_df array
% % % %         set of resonance offset angles 
% % % %     
% % % %     subsetOfMRF1/2/3 = N_df x N_TRs x 3 array
% % % %         component-wise signal evolution for the entire set of
% % % %         off-res frequencies used and events in the sequence
% % % %         1 => TE = 0
% % % %         2 => TE = TR/2
% % % %         3 => TE = TR
% % % %     T1T2ratio = T1/T2 ratio
% % % %     TR = (double)
% % % %     FA = (double) flip angle
% % % %     figureIdx = figure id for the big figure (the 3d)
% % % %     subplotRows = no rows in subplot
% % % %     subplotCols = no cols in subplot
% % % %     subplotIdx = the subplot id in the figure (the 3d)

    % % Retrieve some needed params
    [N_off, N_ev, N_vect] = size(subsetOfMRF1);
    
    legendText = ['T_1 / T_2 = ', num2str(T1T2ratio)];
    
	titlePlot = {[ '(\alpha = ', num2str(FA), '^o, ', ...
                    'TR = ', num2str(TR), 'ms)' ] };
    
    % % % % % %
    % % % % Plot the LOCUS 
    figure(figureIdx)
    
    % I want to plot in a 4x4 way showing different views
    % so all you have to do is change the view and subplotIdx
    for i = 1:2
        % % % % Plot the locus computationally
        subplot(subplotRows, subplotCols, subplotIdx+(i-1));
        [xax, yax, zax] = createAxis();
        % Viewer position
        if i == 1
            view([0 90 0]);
        else
            view([90 0 0]);
        end
        % M+
        scatter3(subsetOfMRF1(:,N_ev,1), ...
                 subsetOfMRF1(:,N_ev,2), ...
                 subsetOfMRF1(:,N_ev,3), ...
                 'k.', 'MarkerFaceAlpha', 1/T1T2ratio, ...
                       'MarkerEdgeAlpha', 1/T1T2ratio);
        hold on
        % M TE=TR/2
    %     scatter3(subsetOfMRF2(:,N_ev,1), ...
    %              subsetOfMRF2(:,N_ev,2), ...
    %              subsetOfMRF2(:,N_ev,3), ...
    %              'r.', 'MarkerFaceAlpha', 1/T1T2ratio, ...
    %                    'MarkerEdgeAlpha', 1/T1T2ratio);
    %     hold on
        % M-
        scatter3(subsetOfMRF3(:,N_ev,1), ...
                 subsetOfMRF3(:,N_ev,2), ...
                 subsetOfMRF3(:,N_ev,3), ...
                 'b.', 'MarkerFaceAlpha', 1/T1T2ratio, ...
                       'MarkerEdgeAlpha', 1/T1T2ratio);
        grid on
        axis equal
        axis([-0.8 0.8 -0.8 0.8 -0.2 1])
        title('Locus Computationally');
    
        % % % % Plot the LOCUS analytically
        % Call gary's function
        [Mminus, Mplus] = steadyStateMagnetisationOfbSSFP(...
            T1, T2, TR, deg2rad(FA), deg2rad(betaPrecessionTR)); 
        
        % Same figure, different subplots
        figure(figureIdx);
        subplot(subplotRows, subplotCols, subplotIdx+(i-1)+2);
        [xax, yax, zax] = createAxis();
        % Viewer position
        if i == 1
            view([0 90 0]);
        else
            view([90 0 0]);
        end

        scatter3(Mplus(1,:), Mplus(2,:), Mplus(3,:), ...
            'k.', 'MarkerFaceAlpha', 1/T1T2ratio, ...
                  'MarkerEdgeAlpha', 1/T1T2ratio);
        hold on
        scatter3(Mminus(1,:),  Mminus(2,:),  Mminus(3,:), ...
            'b.', 'MarkerFaceAlpha', 1/T1T2ratio, ...
                  'MarkerEdgeAlpha', 1/T1T2ratio);

        grid on
        axis equal
        axis([-0.8 0.8 -0.8 0.8 -0.2 1])
        title('Locus Analytically');
     
    end
    
    disp('I am here inside plot SS locus')
    
    % % % % Verify if analytical and computational are the same
    figure,
    subplot(2,1,1)
    
    MminusComp = squeeze(subsetOfMRF3(:,end,:))';
    for idx = 1:3
        plot(Mminus(idx,:)), hold on
        plot(MminusComp(idx,:),'--'), hold on
        plot(Mminus(idx,:)-MminusComp(idx,:))
    end
    title({'Analytical vs Computational M^-', legendText});
    legend('M_x^-a', 'M_x^-c', 'M_x^-a - M_x^-c', ...
           'M_y^-a', 'M_y^-c', 'M_y^-a - M_y^-c', ...
           'M_z^-a', 'M_z^-c', 'M_z^-a - M_z^-c');
       
    subplot(2,1,2)   
    MplusComp  = squeeze(subsetOfMRF1(:,end,:))';
    for idx = 1:3
        plot(Mplus(idx,:)), hold on
        plot(MplusComp(idx,:),'--'), hold on
        plot(Mplus(idx,:)-MplusComp(idx,:))
    end
    title({'Analytical vs Computational M^+', legendText});
    legend('M_x^+a', 'M_x^+c', 'M_x^+a - M_x^+c', ...
           'M_y^+a', 'M_y^+c', 'M_y^+a - M_y^+c', ...
           'M_z^+a', 'M_z^+c', 'M_z^+a - M_z^+c');
    
     % % % % See if it reached steady state
    % THIS IS A New Figure
    if flagPlotSS
        isSteadyStateBeta(subsetOfMRF2, 181, ... % beta = 0
            figureIdx+1, ...
            1, 1, 1,     ... % subplotRows, subplotCols, subplotIdx
            legendText, titlePlot);
    end
    
    disp('I am here inside plot SS locus')
    
end