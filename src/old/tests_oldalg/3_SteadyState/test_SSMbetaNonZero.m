function test_SSMbetaNonZero(N)
% % % % Author: IRINA GRIGORESCU
% % % % Date created: 22-03-2017
% % % % Date updated: 23-03-2017
% % % % 
% % % % This function creates a dictionary for one set of tissue
% % % % properties to demonstrate the evolution of the 
% % % % magnetic moment vector at TE = 0, TE = TR/2 and TE = TR 
% % % % for beta = [-pi,pi]
% % % % 
% % % % INPUT:
% % % %     N = number of timepoints for the sequence
% % % % 


% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % CUSTOM SEQUENCE 
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
flagIR = 0;   % no inversion
% % % %
% % % % SEQUENCES
% % CUSTOM SEQUENCE for FA = 15
customValuesSequence.RF.FA  =   15;
customValuesSequence.RF.PhA =    0; % B1 field direction +x
customValuesSequence.TR  =    8;
customValuesSequence.RO  =    0;

% % CUSTOM MATERIAL 
% % material properties: T2 = 50 TR and T1 =  5 T2
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial.T2     = 15 * customValuesSequence.TR; 
customValuesMaterial.T1     =  5 * customValuesMaterial.T2;

betaPrecessionTR             = -180:180; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR);
             
% % % % % % % %
% % % % RUN MRF FOR THE CUSTOM VALUES PROVIDED

% % % % dictionaryMRFAll(N_TEs, N_T1 x N_T2 x N_offRes x N_timepoints x 3)
% % dictionaryMRFAll
dictionaryMRFAll = zeros(3,1,1,length(betaPrecessionTR),N,3);

% % Do for all three TEs
for i = 1:3
    % % % % % % % % % % % COLUMN A
    % % % % A1
    tic
    [materialProperties, sequenceProperties, ...
        dictionaryMRFtemp] = simulateMRF(N, 0, ...
                                 SEQFLAG, customValuesSequence, ...
                                 MATFLAG, customValuesMaterial, flagIR);
    toc
    dictionaryMRFAll(i,:,:,:,:,:) = dictionaryMRFtemp; % SAVE DICTIONARY
    
    
    % % % % Change RO
    customValuesSequence.RO = customValuesSequence.RO   + ...
                               customValuesSequence.TR/2 ;
	
    clear sequenceProperties
end


% % % % % % % %
% % % % Let's extract the signal for a given tissue but for all df values
% % % % For first material
materialOfInterest = struct;
materialOfInterest.cst = struct;
materialOfInterest.cst.t1 = customValuesMaterial.T1;
materialOfInterest.cst.t2 = customValuesMaterial.T2;
materialOfInterest.cst.df = customValuesMaterial.offRes;
tissueIndices = extractIndices(materialProperties, materialOfInterest);

% % % remind me how the data looks like:
% % % dictionaryMRF1A(3,N_T1 x N_T2 x N_offRes x N_timepoints x 3 array)

% % % % % % % % % % COLUMN A
% % % % % Extracting indices of interest from 1A
% % % Extracting indices of interest (TE=0)
subsetOfMRF1 = squeeze(dictionaryMRFAll(1,... % TE=0
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR/2)
subsetOfMRF2 = squeeze(dictionaryMRFAll(2,... % TE=TR/2
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));
% % % Extracting indices of interest (TE=TR)
subsetOfMRF3 = squeeze(dictionaryMRFAll(3,... % TE=TR
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));
                        

% % % % % % % %
% % % % Plot: 
% 1. If it reached steady state
% 2. The 3d plot of the steady transient signal for a few
% precession angles per TR generated earlier

figureIdx = 222;
figure(figureIdx);

subplotRows = 2;
subplotCols = 1;

N_off = length(betaPrecessionTR);
betaIdx = ceil(N_off/2);

stepBeta = 20;
betaIndices = [betaIdx : stepBeta : N_off  ...
                     1 : stepBeta : betaIdx-stepBeta];
         
legendText  = {};


% % % % PLOT
for i = 1:length(betaIndices)
    
    % Plot phase trajectory and locus and plot magnitude history
    plot_SSMbetaNonZero(betaPrecessionTR, ...
      subsetOfMRF1(:,1:end,:), ...
      subsetOfMRF2(:,1:end,:), ...
      subsetOfMRF3(:,1:end,:), ...
            betaIndices(i), ...         % plotting for a particular beta
            materialProperties.T1, ...
            materialProperties.T2, ...
            customValuesSequence.RF.FA, ...
            figureIdx, subplotRows, subplotCols, 1);
    

    % Place legend for second figure
    legendText = [legendText ...
        {['\beta = ', num2str(betaPrecessionTR(betaIndices(i))),'^o']} ];
    
    figure(figureIdx+1)
    legend(legendText);
    
    pause(2)
        
end
        
% figure(figureIdx)
% subplot(subplotRows,subplotCols,1)
% legend(legendText);
% subplot(subplotRows,subplotCols,2)
% legend(legendText);
        
disp(['DONE TEST: Phase trajectories of the total magnetisation ', ...
      'in the presence of repeated RF tipping about the x axis ', ... '
      'for different FA values and for T1/T2 = 5']);
        
end