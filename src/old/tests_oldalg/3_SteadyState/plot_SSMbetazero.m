function plot_SSMbetazero(subsetOfMRF1, subsetOfMRF2, subsetOfMRF3, ...
            T1, T2, FA, figureIdx, ...
            subplotRows, subplotCols, subplotIdx)
% % % % IRINA GRIGORESCU
% % % % Date created: 22-03-2017
% % % % Date updated: 22-03-2017
% % % % 
% % % % This function plots in 3D the vector position at transient-state 
% % % % for an resonance offset angle of 0
% % % % 
% % % % INPUT:
% % % %     subsetOfMRF1/2/3 = N_df x N_TRs x 3 array
% % % %         component-wise signal evolution for the entire set of
% % % %         off-res frequencies used and events in the sequence
% % % %         1 => TE = 0
% % % %         2 => TE = TR/2
% % % %         3 => TE = TR
% % % %     T1 = relaxation parameter for figure title
% % % %     T2 = relaxation parameter for figure title
% % % %     FA = (double) flip angle
% % % %     figureIdx = figure id for the big figure (the 3d)
% % % %     subplotRows = no rows in subplot
% % % %     subplotCols = no cols in subplot
% % % %     subplotIdx = the subplot id in the figure (the 3d)

    % % Retrieve some needed params
    [N_off, N_ev, N_vect] = size(subsetOfMRF1);
    beta0Idx = ceil(N_off/2);

    subsetAll = zeros(N_off, 3*N_ev, N_vect);
    for i = 1 : N_ev
        subsetAll(:,(i-1)*3+1,:) = subsetOfMRF1(:,i,:);
        subsetAll(:,(i-1)*3+2,:) = subsetOfMRF2(:,i,:);
        subsetAll(:,(i-1)*3+3,:) = subsetOfMRF3(:,i,:);
    end
    
    legendText = 'Signal at TE = TR/2';
    
	titlePlot = {[ '(T_1 = ', num2str(T1), 'ms, ', ...
                    'T_2 = ', num2str(T2), 'ms, ', ...
                    '\beta = 0^o) '], ...
                 [ '(\alpha = ', num2str(FA), '^o, ', ...
                    'TR = 8ms)' ] };

    % % % % 3D plot
    figure(figureIdx)
    subplot(subplotRows,subplotCols,subplotIdx);
    
    [xax, yax, zax] = createAxis();
    % Viewer position
    view([90 0 0]);
    
    % Plot connecting points
    plot3(subsetAll(beta0Idx,:,1), ...
          subsetAll(beta0Idx,:,2), ...
          subsetAll(beta0Idx,:,3), 'k')
    
    % TE = 0
    scatter3(subsetOfMRF1(beta0Idx,:,1), ...
             subsetOfMRF1(beta0Idx,:,2), ...
             subsetOfMRF1(beta0Idx,:,3), 'k'); 
    hold on
    % TE = TR/2
    scatter3(subsetOfMRF2(beta0Idx,:,1), ...
             subsetOfMRF2(beta0Idx,:,2), ...
             subsetOfMRF2(beta0Idx,:,3), 'r'); 
    hold on
    % TE = TR
    scatter3(subsetOfMRF3(beta0Idx,:,1), ...
             subsetOfMRF3(beta0Idx,:,2), ...
             subsetOfMRF3(beta0Idx,:,3), 'b'); 
    axis equal
    axis([-1 1 -1 1 -1 1])
    title(titlePlot);
%     title({['alpha = ', num2str(FA), '^o']});
    
    
    
    % % % % See if it reached steady state
    isSteadyStateBeta(subsetOfMRF2, beta0Idx, ...
        figureIdx+1, ...
        subplotRows, subplotCols, subplotIdx, ...
        legendText, titlePlot);
    
%     % % % % The signal amplitude at TE = TR/2
%     figure
%     plot(betaPrecessionTR, ...
%          abs(subsetOfMRF2(:,N_ev,1) + 1i.*subsetOfMRF2(:,N_ev,2)), 'b-')
%     
%     grid on
%     xlabel({'\beta(TR)', 'resonance offset angle', ...
%             'Precession per TR (deg)'})
%     ylabel('Signal Magnitude')
%     legend({'TE = TR/2'})
%     axis([-250 250 0 0.6])
    
    
end