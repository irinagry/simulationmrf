function [sequenceProperties, materialProperties, materialOfInterest] = ...
            test_phaseCycling(configParams, N)
% % % % Author: IRINA GRIGORESCU
% % % % Date created: 20-03-2017
% % % % Date updated: 20-03-2017
% % % % 
% % % % This function creates a dictionary for a small set of tissue
% % % % properties to demonstrate the evolution of the 
% % % % amplitude and phase of the signal at TE/2 
% % % % for incrementally different RF phases
% % % % 
% % % % INPUT:
% % % %     configParams = (struct) with the fields (at least): 
% % % %                    .tissue.cst.t1 (cst == custom tissue)
% % % %                    .tissue.cst.t2
% % % %                    .tissue.cst.df
% % % %     N = number of timepoints for the sequence
% % % % 
% % % % OUTPUT: 
% % % %     sequenceProperties = the sequence used (struct) with
% % % %         RF = RF pulse (struct) with
% % % %              FA  = flip angle (in deg) (Nx1 array) and 
% % % %              PhA = phase angle (in deg) (Nx1 array)
% % % %         TR = repetition time (in ms) (Nx1 array)
% % % %         RO = read-out time (in ms) (Nx1 array)
% % % % 
% % % %     materialProperties = (struct) with
% % % %         T1 (double)
% % % %         T2 (double)
% % % %         offRes (array double)
% % % %     
% % % %     materialOfInterest = (struct) with the fields (at least): 
% % % %                    .cst.t1 (cst == custom tissue)
% % % %                    .cst.t2
% % % %                    .cst.df



% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY
SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence

% % CUSTOM SEQUENCE for delta phi = 0
customValuesSequence1.RF.FA  =   45;
customValuesSequence1.RF.PhA =   90; % in the paper they use B1 field +y
customValuesSequence1.TR  =   20;
customValuesSequence1.RO  =  customValuesSequence1.TR/2;

% % CUSTOM SEQUENCE for delta phi = 90
customValuesSequence2.RF.FA  =  customValuesSequence1.RF.FA;
customValuesSequence2.RF.PhA =  zeros(N,1) + 90; 
customValuesSequence2.RF.PhA(2:4:end) = 180;
customValuesSequence2.RF.PhA(3:4:end) = 270;
customValuesSequence2.RF.PhA(4:4:end) =   0;
customValuesSequence2.TR  =  customValuesSequence1.TR;
customValuesSequence2.RO  =  customValuesSequence1.RO;

% % CUSTOM SEQUENCE for delta phi = 180
customValuesSequence3.RF.FA  =  customValuesSequence1.RF.FA;
customValuesSequence3.RF.PhA =  zeros(N,1) + 90;
customValuesSequence3.RF.PhA(2:2:end) = 270;
customValuesSequence3.TR  =  customValuesSequence1.TR;
customValuesSequence3.RO  =  customValuesSequence1.RO;

% % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial.T2     = 7 * customValuesSequence1.TR; 
customValuesMaterial.T1     = 5 * customValuesMaterial.T2; 
betaPrecessionTR            = -250:250; % values for the angle (in deg) of
                                        % precession during TR
% df = beta / (2*180*TR) 
customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence1.TR); 

% % % % % % % %
% % % % RUN MRF FOR THE CUSTOM VALUES PROVIDED
tic
[materialProperties1, sequenceProperties1, ...
    dictionaryMRF1] = simulateMRF(N, 0, ...
                             SEQFLAG, customValuesSequence1, ...
                             MATFLAG, customValuesMaterial);
toc

tic
[materialProperties2, sequenceProperties2, ...
    dictionaryMRF2] = simulateMRF(N, 0, ...
                             SEQFLAG, customValuesSequence2, ...
                             MATFLAG, customValuesMaterial);
toc

tic
[materialProperties3, sequenceProperties3, ...
    dictionaryMRF3] = simulateMRF(N, 0, ...
                             SEQFLAG, customValuesSequence3, ...
                             MATFLAG, customValuesMaterial, 0);
toc

% % % % % % % %
% % % % Let's extract the signal for a given tissue but for all df values
% Extract indices for a custom material of interest 
materialOfInterest = configParams.tissue;
materialOfInterest.cst.t1 = customValuesMaterial.T1;
materialOfInterest.cst.t2 = customValuesMaterial.T2;
materialOfInterest.cst.df = customValuesMaterial.offRes;
tissueIndices = extractIndices(materialProperties1, materialOfInterest);

% % % dictionaryMRF(N_T1 x N_T2 x N_offRes x N_timepoints x 3 array)
subsetOfMRF1 = squeeze(dictionaryMRF1(...
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));
subsetOfMRF2 = squeeze(dictionaryMRF2(...
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));
subsetOfMRF3 = squeeze(dictionaryMRF3(...
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));

% % % % % % % %
% % % % Plot the signal amplitude and phase at TE = TR/2
% for the range of precession angles per TR generated earlier
plot_phaseCycling(betaPrecessionTR, ...
                  subsetOfMRF1, subsetOfMRF2, subsetOfMRF3);
disp('ana')

end