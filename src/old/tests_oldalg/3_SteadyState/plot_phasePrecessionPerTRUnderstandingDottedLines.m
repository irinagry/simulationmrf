function plot_phasePrecessionPerTRUnderstandingDottedLines(betaPrecessionTR, ...
            subsetOfMRF1, subsetOfMRF2, subsetOfMRF3)
% % % % IRINA GRIGORESCU
% % % % Date created: 20-03-2017
% % % % Date updated: 20-03-2017
% % % % 
% % % % This function plots in 3D the vector position at steady-state 
% % % % for a range of resonance offset angles
% % % % 
% % % % INPUT:
% % % %     betaPrecessionTR = 1 x N_df array
% % % %         set of resonance offset angles 
% % % %     
% % % %     subsetOfMRF1/2/3 = N_df x N_TRs x 3 array
% % % %         component-wise signal evolution for the entire set of
% % % %         off-res frequencies used and events in the sequence
% % % %         1 => TE = 0
% % % %         2 => TE = TR/2
% % % %         3 => TE = TR

    [N_off, N_ev, N_vect] = size(subsetOfMRF1);
%     N_ev=5;
    figure,

    % Signal phase
%     subplot(2,1,1)
    scatter(betaPrecessionTR, ...
        rad2deg(atan2(...
            subsetOfMRF1(:,N_ev,2), ...
           -subsetOfMRF1(:,N_ev,1))), ...
           '.k');
	hold on
    scatter(betaPrecessionTR, ...
        rad2deg(atan2(...
            subsetOfMRF2(:,N_ev,2), ...
           -subsetOfMRF2(:,N_ev,1))), ...
           '*r');
	hold on
    scatter(betaPrecessionTR, ...
        rad2deg(atan2(...
            subsetOfMRF3(:,N_ev,2), ...
           -subsetOfMRF3(:,N_ev,1))), ...
           'ob');
    
    grid on
    xlabel({'\beta(TR)', 'resonance offset angle', ...
            'Precession per TR (deg)'})
    ylabel('Signal Phase')
    axis([-360 360 -200 200])

    % 3D plot
%     subplot(2,1,2) 
    figure
    [xax, yax, zax] = createAxis();
    % Viewer position
    view([45 45 10]);
    
    % TE = 0
    scatter3(subsetOfMRF1(:,N_ev,1), ...
             subsetOfMRF1(:,N_ev,2), ...
             subsetOfMRF1(:,N_ev,3), 'k^'); 
    hold on
    % TE = TR/2
    scatter3(subsetOfMRF2(:,N_ev,1), ...
             subsetOfMRF2(:,N_ev,2), ...
             subsetOfMRF2(:,N_ev,3), 'r*'); 
    hold on
    % TE = TR
    scatter3(subsetOfMRF3(:,N_ev,1), ...
             subsetOfMRF3(:,N_ev,2), ...
             subsetOfMRF3(:,N_ev,3), 'bo'); 
    hold on
    
    figure
    plot(betaPrecessionTR, ...
         abs(subsetOfMRF2(:,N_ev,1) + 1i.*subsetOfMRF2(:,N_ev,2)), 'b-')
    
    grid on
    xlabel({'\beta(TR)', 'resonance offset angle', ...
            'Precession per TR (deg)'})
    ylabel('Signal Magnitude')
    legend({'TE = TR/2'})
    axis([-250 250 0 0.6])
    
    disp('a')
    
end