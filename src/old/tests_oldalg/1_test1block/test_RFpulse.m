function test_RFpulse(configParams, FA_userDefined, PhA_userDefined)
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 16-03-2017
% % % % Date updated: 28-03-2017
% % % % 
% % % % TEST 1 : 1 block of sequence
% % % %          Test RF pulse
% % % % 
% % % % INPUT:
% % % %     
% % % % 
% % % % USES OLD ALGORITHM

% % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % % % CUSTOM SEQUENCE
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
customValuesSequence.FA  =  FA_userDefined;
customValuesSequence.PhA =  PhA_userDefined;
customValuesSequence.TR  =  20;
customValuesSequence.RO  =  customValuesSequence.TR./2;
% % % % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial.T2     = 1000 * customValuesSequence.TR;
customValuesMaterial.T1     = 1000 * customValuesSequence.TR;
betaPrecessionTR            = 0;

% df = beta / (2*180*TR) 
customValuesMaterial.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * customValuesSequence.TR); 


N = 5;      % timepoints for FAs and TRs of the sequence
tic
[materialProperties, sequenceProperties, ...
    dictionaryMRF] = simulateMRF(N, 0, ...
                        SEQFLAG, customValuesSequence, ...
                        MATFLAG, customValuesMaterial);
toc

% Extract indices for the material of interest
materialOfInterest = configParams.tissue;
materialOfInterest.cst.t1 = customValuesMaterial.T1;
materialOfInterest.cst.t2 = customValuesMaterial.T2;
materialOfInterest.cst.df = customValuesMaterial.offRes;
tissueIndices = extractIndices(materialProperties, materialOfInterest);

% % % % % % % %
% % % % % % % %
% % Visualise signal evolution for this sequence for the custom material
N_events = 1;
% Relaxation Times
T1 = materialProperties.T1(tissueIndices.cst{1});
T2 = materialProperties.T2(tissueIndices.cst{2});
% Off-resonance frequency
df = materialProperties.offRes(tissueIndices.cst{3});
% Timestep for update
dt = 0.5; 

% % % % RUN SIMULATION
% % % % Calculate event history of the sequence being applied to the vector
[ mu, b1field, eventMatrix, newFAs, phAngles, newTRs ] = ...
    calculateVectorHistory(sequenceProperties.RF.FA, ...
                           sequenceProperties.RF.PhA, ...
                           sequenceProperties.TR, ...
                           sequenceProperties.RO, ...
                           T1, T2, df, dt, N_events);
    
% % % % % Plotting
plotSequence(eventMatrix, mu, b1field, newFAs, ...
             T1, T2, df, dt, N_events, 2, 1);

end