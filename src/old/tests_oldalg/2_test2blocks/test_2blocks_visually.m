function test_2blocks_visually(params, FA_userDefined, PhA_userDefined)
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 16-03-2017
% % % % Date updated: 16-03-2017
% % % % 
% % % % Looking at 2 blocks of sequence
% % % % 


% % % % % % % % %
% % % % GENERATE CUSTOM DICTIONARY

% % CUSTOM SEQUENCE 
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
customValuesSequence.FA  =  FA_userDefined;
customValuesSequence.PhA =  PhA_userDefined;
customValuesSequence.TR  =  40;
customValuesSequence.RO  =  customValuesSequence.TR./2;
% % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial
customValuesMaterial.T1     = 600; % T1 for WM at 1.5T
customValuesMaterial.T2     =  80; % T2 for WM at 1.5T
customValuesMaterial.offRes = [0 0.025 0.5];

N = 5;      % timepoints for FAs and TRs of the sequence
tic
[materialProperties, sequenceProperties, ...
    dictionaryMRF] = simulateMRF(N, 0, ...
                             SEQFLAG, customValuesSequence, ...
                             MATFLAG, customValuesMaterial);
toc

% Extract indices for a custom material of interest 
materialOfInterest = params.tissue;
materialOfInterest.cst.t1 = 600;
materialOfInterest.cst.t2 = 80;
materialOfInterest.cst.df = 0.025;
tissueIndices = extractIndices(materialProperties, materialOfInterest);


% % Visualise signal evolution for this sequence
N_events = 2;
% Relaxation Times
T1 = materialProperties.T1(tissueIndices.cst{1});
T2 = materialProperties.T2(tissueIndices.cst{2});
% Off-resonance frequency
df = materialProperties.offRes(tissueIndices.cst{3});
% Timestep for update
dt = 5e-01; 

% % % % RUN SIMULATION
% % % % Calculate event history of the sequence being applied to the vector
[ mu, b1field, eventMatrix, newFAs, phAngles, newTRs ] = ...
    calculateVectorHistory(sequenceProperties.RF.FA, ...
                           sequenceProperties.RF.PhA, ...
                           sequenceProperties.TR, ...
                           sequenceProperties.RO, ...
                           T1, T2, df, dt, N_events);
    
% % % % % Plotting
plotSequence(eventMatrix, mu, b1field, newFAs, ...
             T1, T2, df, dt, N_events);
         
end        
         
