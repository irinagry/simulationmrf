% % % % 
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 16-03-2017
% % % % Date updated: 16-03-2017
% % % % 
% % % % TEST miscelaneous
% % % % 

% Preprocessing steps
clear all; close all; clc
addpath ../helpers/ % Helper functions
addpath ../algs_slow/    % Algs functions
addpath ../preprocess/ % Preprocessing steps
addpath ../perlin/ % Perlin

% Run cofig file to have the parameters of interest at hand
params = returnPredefinedParameters();

%% Generate Dictionary for a given number of N timepoints
FLAGPLOT = 1; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 5; % If == 5 need to provide customValuesSequence

customValuesSequence.FA  =  90;
customValuesSequence.PhA =   0;
customValuesSequence.TR  =  20;
customValuesSequence.RO  =  10;

N = 50;      % timepoints for FAs and TRs of the sequence

tic
[materialProperties, sequenceProperties, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, SEQFLAG, customValuesSequence);
toc

%% Plot signal for WM
% Extract indices for WM/GM/CSF from the materialProperties set
tissueIndices = extractIndices(materialProperties, params.tissue);

% WM_x
M_x = extractSubset(dictionaryMRF(:,:,:,:,1), ... % 1 == x
                  tissueIndices.wm);
% WM_y
M_y = extractSubset(dictionaryMRF(:,:,:,:,2), ... % 2 == y
                  tissueIndices.wm);
% WM_z
M_z = extractSubset(dictionaryMRF(:,:,:,:,3), ... % 3 == z
                  tissueIndices.wm);
              
% Plot
figure
plot(1:N, M_x);
hold on
plot(1:N, M_y);
hold on
plot(1:N, M_z);
hold on
legend({'M_x', 'M_y', 'M_z'})
xlabel('pulse number')
ylabel('M^+_x M^+_y M^+_z')
title([{'M component-wise evolution for'}, ...
    {['FA = ', num2str(customValuesSequence.FA), ...
    '^o and TR = ', num2str(customValuesSequence.TR), 'ms']}]);




%% Visualise signal evolution for this sequence
N_events = 4;
% Relaxation Times
T1 = materialProperties.T1(tissueIndices.wm{1});
T2 = materialProperties.T2(tissueIndices.wm{2});
% Off-resonance frequency
df = materialProperties.offRes(tissueIndices.wm{3});
% Timestep for update
dt = 9e-01; 

% % % % RUN SIMULATION
% % % % Calculate event history of the sequence being applied to the vector
[ mu, b1field, eventMatrix, newFAs, phAngles, newTRs ] = ...
    calculateVectorHistory(sequenceProperties.RF.FA, ...
                           sequenceProperties.RF.PhA, ...
                           sequenceProperties.TR, ...
                           sequenceProperties.RO, ...
                           T1, T2, df, dt, N_events);

% % % % % Plotting
plotSequence(eventMatrix, mu, b1field, newFAs, ...
             T1, T2, df, dt, N_events);
         
         
%
%% Plot signal for WM
% Extract indices for WM/GM/CSF from the materialProperties set
% USED here is params.tissue for 
tissueIndices = extractIndices(materialProperties, params.tissue);

% WM_x
M_x = extractSubset(dictionaryMRF(:,:,:,:,1), ... % 1 == x
                  tissueIndices.wm);
% WM_y
M_y = extractSubset(dictionaryMRF(:,:,:,:,2), ... % 2 == y
                  tissueIndices.wm);
% WM_z
M_z = extractSubset(dictionaryMRF(:,:,:,:,3), ... % 3 == z
                  tissueIndices.wm);
              
% % % Plot
% % figure
% % plot(1:N, M_x);
% % hold on
% % plot(1:N, M_y);
% % hold on
% % plot(1:N, M_z);
% % hold on
% % legend({'M_x', 'M_y', 'M_z'})
% % xlabel('pulse number')
% % ylabel('M^+_x M^+_y M^+_z')
% % title([{'M component-wise evolution at RO for'}, ...
% %     {['FA = ', num2str(customValuesSequence.FA), ...
% %     '^o and TR = ', num2str(customValuesSequence.TR), 'ms']}]);
