% % % % 
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 17-03-2017
% % % % Date updated: 17-03-2017
% % % % 
% % % %      TEST PRESENTATION
% % % % This script will present the MRF functionality visually
% % % % 


% % % % % % % 
% Preprocessing steps
clear all; close all; clc

addpath ../../exchanged/       % Exchanged with Gary
addpath ../../helpers/         % Helper functions
addpath ../../algs_slow/            % Algs functions
addpath ../../preprocess/ % Preprocessing steps
addpath ../../perlin/     % Perlin

% Run cofig file to have the parameters of interest at hand
configParams = returnPredefinedParameters();

FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
isSteadyState = 0;

% % % % % % % 
%% Generate dictionary for a predefined set of tissue properties
% 2 - perlin noise FA, alternate PhA, urandom TRs, RO = TR./2
% 3 - sinusoidal FA, alternate PhA, perlin TRs, RO = TR./2
SEQFLAG  = 3; 
N = 500;      % timepoints for FAs and TRs of the sequence

% % % % Trying with a custom sequence
% % % SEQFLAG  = 5; % If == 5 need to provide customValuesSequence
% % % customValuesSequence.FA  =   70;
% % % customValuesSequence.PhA =    0; % B1 field direction +x
% % % customValuesSequence.TR  =   19;
% % % customValuesSequence.RO  =    0;
% % % N = 200;

% The dictionary is an array of 
%  (N_T1 x N_T2 x N_offRes x N_timepoints x 3 array)
tic
[materialProperties, sequenceProperties, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, SEQFLAG);
%                     SEQFLAG, customValuesSequence);
toc

% % % % % % % 
%% Extract only fingerprints of interest
% Create material properties of interest
materialOfInterest     = configParams.tissue;
materialOfInterest.cst = materialOfInterest.wm; % want my custom to be WM     

materials = {'wm', 'gm', 'csf', 'fat', 'cst'};
dfPerMat  = [  0,    0,     0,     0,   0.1];

for idxMat = 1:length(materials)
    materialOfInterest.(materials{idxMat}).df = dfPerMat(idxMat);
end

% Extract indices of dictionary for the materials of interest
tissueIndices = extractIndices(materialProperties, materialOfInterest);

% Save the subset of the dictionary in a structure
subsetOfMRF = struct;
% CSF
subsetOfMRF.csf = squeeze(dictionaryMRF(... 
                            tissueIndices.csf{1}, ...
                            tissueIndices.csf{2}, ...
                            tissueIndices.csf{3}, ...
                            :, :));
% WM
subsetOfMRF.wm = squeeze(dictionaryMRF(... 
                            tissueIndices.wm{1}, ...
                            tissueIndices.wm{2}, ...
                            tissueIndices.wm{3}, ...
                            :, :));

% WM-Offresonance
subsetOfMRF.wmoff = squeeze(dictionaryMRF(... 
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));

% GM
subsetOfMRF.gm = squeeze(dictionaryMRF(... 
                            tissueIndices.gm{1}, ...
                            tissueIndices.gm{2}, ...
                            tissueIndices.gm{3}, ...
                            :, :));
% Fat
subsetOfMRF.fat = squeeze(dictionaryMRF(... 
                            tissueIndices.fat{1}, ...
                            tissueIndices.fat{2}, ...
                            tissueIndices.fat{3}, ...
                            :, :));

                        
                        
                        
% % % % % % % 
%% Plot Just FAs, TRs and signal for different tissue types
% Number of data points to plot
M = ceil(N);

noRows =  7;
noCols = 15;
figHandle = figure;
set(figHandle, 'Position', [20, 60, 1400, 700]);

mat = {'wm', 'gm', 'fat', 'wmoff'};%, 'csf'};
colors = ['r', 'g', 'b', 'm'];%, 'y'];
signal = struct;

for idx = 1:length(mat)
    subsetTemp = subsetOfMRF.(mat{idx});
    signal.(mat{idx}) = abs(subsetTemp(:,1) + ...
                   1i.*subsetTemp(:,2));
end

% SUBPLOTS
FAs_subplot = [ 1, 35];
TRs_subplot = [61, 95];
signal_subplot = [22, 90];

% % % % 1) FAs
subplot(noRows, noCols, FAs_subplot)
plot(1:M, sequenceProperties.RF.FA(1:M), 'b'); hold on
grid on
xlabel('TR index')
ylabel('FA (deg)')
axis([0 M 0 90]);
title('FAs')

% % % % 2) TRs
subplot(noRows, noCols, TRs_subplot)
plot(1:M, sequenceProperties.TR(1:M), 'b'); hold on
grid on
xlabel('TR index')
ylabel('TR (ms)')
axis([0 M ...
    min(sequenceProperties.TR)-0.5 max(sequenceProperties.TR)+0.5]);
title('TRs')

% % % % 3) Signal
subplot(noRows, noCols, signal_subplot)
for idx = 1:length(mat)
    signalTemp = signal.(mat{idx});
    scatter([1, M], ...
        [signalTemp(1), signalTemp(M)], ...
        ['.', colors(idx)]); hold on
end
legend(mat)
grid on
axis([0 M 0 1]);
xlabel('TR index')
ylabel('Signal intensity')
title('Signal')

for idxTR = 2:M
    % % % % 1) FAs
    subplot(noRows, noCols, FAs_subplot)
    tipFA = stem(idxTR, max(sequenceProperties.RF.FA(idxTR)), ...
        'r', 'filled');
    
    % % % % 2) TRs
    subplot(noRows, noCols, TRs_subplot)
    tipTR = stem(idxTR, max(sequenceProperties.TR(idxTR)), ...
        'r', 'filled');
    
    % % % % 3) Signal
    subplot(noRows, noCols, signal_subplot)
    for idx = 1:length(mat)
        signalTemp = signal.(mat{idx});
        plot([idxTR-1 idxTR], ...
            [signalTemp(idxTR-1), signalTemp(idxTR)], ...
            colors(idx)); hold on
    end
    
    
    % Save figure
%     print(gcf, ['../data/forGIF/PlotDict',num2str(SEQFLAG-1),'_All_', ...
%                                  num2str(idxTR)], '-dpng');
    
    % Pause drawing
    pause(0.0001)
    drawnow
    deletePlots(tipFA, tipTR);
    idxTR
    
end
                        
% % % % % % % 
%% Create figure subplots

% Number of data points to plot
M = ceil(N);

% I want to plot only a certain material
mat = 'wmoff';
subsetToPlot = subsetOfMRF.(mat);
df = materialOfInterest.cst.df;

signal = abs(subsetToPlot(:,1) + 1i.*subsetToPlot(:,2));

noRows =  7;
noCols = 15;
figHandle = figure;
set(figHandle, 'Position', [20, 60, 1400, 700]);


% SUBPLOTS
FAs_subplot = [ 1, 34];
TRs_subplot = [61, 94];
phaseSpace_subplot = [21, 85];
%text_subplot = [81, 100];
transv_subplot = [12, 45];
signal_subplot = [72, 105];

% % % % % 0) TEXT about subplots
% subplot(noRows, noCols, text_subplot)
% plot(1:M, zeros(1,M)); hold on
% set(gca,'XTickLabel','');
% set(gca,'YTickLabel','');
% 
% scatter(M/4, 2, 'or')
% scatter(M/4, 3, 'ob')
% 
% text(M/4+N/50, 2, [legendPlots{2}, ' ', matpropoffWM])
% text(M/4+N/50, 3, [legendPlots{1}, ' ', matpropnooffWM])
% 
% axis([0 M 1 4])

% % % % 1) FAs
subplot(noRows, noCols, FAs_subplot)
plot(1:M, sequenceProperties.RF.FA(1:M), 'b'); hold on
grid on
xlabel('TR index')
ylabel('FA (deg)')
axis([0 M 0 90]);
title('FAs')

% % % % 2) TRs
subplot(noRows, noCols, TRs_subplot)
plot(1:M, sequenceProperties.TR(1:M), 'b'); hold on
grid on
xlabel('TR index')
ylabel('TR (ms)')
axis([0 M ...
    min(sequenceProperties.TR)-0.5 max(sequenceProperties.TR)+0.5]);
title('TRs')

% % % % 3) 3D visualisation
subplot(noRows, noCols, phaseSpace_subplot)
% Create xyz axis
[xax, yax, zax] = createAxis();
% Viewer position
view([40 40 10]);
xlabel('M_x'); ylabel('M_y'); zlabel('M_z');
axis equal
axis([-1 1 -1 1 -1 1])
title('Phase Space')
    
% % % % 4) 2D transverse plane
subplot(noRows, noCols, transv_subplot)
% Plot first value
scatter(subsetToPlot(1,1), subsetToPlot(1,2), ...
        '.r', 'filled'); hold on
grid on
axis([-1 1 -1 1]);
xlabel('M_x')
ylabel('M_y')
title('Transverse Plane')

% % % % 5) Signal
subplot(noRows, noCols, signal_subplot)
scatter([1, M], [signal(1), signal(M)], 'r.'); hold on
grid on
axis([0 M 0 1]);
xlabel('TR index')
ylabel('Signal intensity')
title('Signal')


for idxTR = 2:M
    % % % % 1) FAs
    subplot(noRows, noCols, FAs_subplot)
    tipFA = stem(idxTR, max(sequenceProperties.RF.FA(idxTR)), ...
        'r', 'filled');
    
    % % % % 2) TRs
    subplot(noRows, noCols, TRs_subplot)
    tipTR = stem(idxTR, max(sequenceProperties.TR(idxTR)), ...
        'r', 'filled');
    

    % % % % 3) 3D visualisation
    subplot(noRows, noCols, phaseSpace_subplot)
    % % % Fat
    % History
    plot3(...
          [subsetToPlot(idxTR-1,1), subsetToPlot(idxTR,1)], ...
          [subsetToPlot(idxTR-1,2), subsetToPlot(idxTR,2)], ...
          [subsetToPlot(idxTR-1,3), subsetToPlot(idxTR,3)], ...
          'Color', [0 1 1 0.2]); hold on
    % Tip
	plotTip = scatter3(subsetToPlot(idxTR,1), ...
             subsetToPlot(idxTR,2), ...
             subsetToPlot(idxTR,3), ...
             'or', 'filled'); hold on

         
    % % % % 4) 2D transverse plane
    subplot(noRows, noCols, transv_subplot)
    % % % Fat
    % History
    plot([subsetToPlot(idxTR-1,1), subsetToPlot(idxTR,1)], ...
         [subsetToPlot(idxTR-1,2), subsetToPlot(idxTR,2)], ...
          'Color', [0 1 1 0.2]); hold on
    % Tip
	plotTipTr = scatter(subsetToPlot(idxTR,1), ...
             subsetToPlot(idxTR,2), ...
             'or', 'filled'); hold on


    % % % % 5) Signal
    subplot(noRows, noCols, signal_subplot)
    plot([idxTR-1 idxTR], ...
         [signal(idxTR-1) signal(idxTR)], 'r'); hold on
    
    
    % Save figure
%     print(gcf, ['../data/forGIF/PlotDict',num2str(SEQFLAG-1), ...
%                                  '_', mat, '_', ...
%                                  num2str(idxTR)], '-dpng');
    
    % Pause drawing
    pause(0.0001)
    drawnow
    deletePlots(tipFA, tipTR, plotTip, plotTipTr);
    idxTR
    
end













% % %% See if it reached steady state
% % if isSteadyState 
% %     [Mm, Mp] = ...
% %      steadyStateMagnetisationOfbSSFP(materialOfInterest.cst.t1, ...
% %                                      materialOfInterest.cst.t2, ...
% %                                      customValuesSequence.TR, ...
% %                                      deg2rad(customValuesSequence.FA), ...
% %                2*pi * materialOfInterest.cst.df * customValuesSequence.RO);
% % 
% %     MmCalc = [Mx.off(end-1) My.off(end-1) Mz.off(end-1)]';
% %     MpCalc = Rotz(deg2rad(customValuesSequence.PhA)) * ...
% %             Rotx(-deg2rad(customValuesSequence.FA)) * ...
% %             Rotz(-deg2rad(customValuesSequence.PhA)) ...
% %             * MmCalc;
% % 
% % 
% %     Mm
% %     MmCalc
% % 
% %     Mp
% %     MpCalc
% % end






