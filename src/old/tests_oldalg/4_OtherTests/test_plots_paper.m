% % % % 
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 15-03-2017
% % % % Date updated: 30-03-2017
% % % % 
% % % % This script will generate the dictionary for the 
% % % % IR-bSSFP sequence for one magnetic moment vector
% % % % for a predefined set of material properties
% % % % (not the entire set)
% % % % 

% Preprocessing steps
clear all; close all; clc
addpath ../helpers/ % Helper functions
addpath ../algs_slow/    % Algs functions
addpath ../preprocess/ % Preprocessing steps
addpath ../perlin/     % Perlin stuff

% Run cofig file to have the parameters of interest at hand
configParams = returnPredefinedParameters();


%% Generate dictionary for a predefined set of tissue properties
FLAGPLOT = 1; % Flag for plotting sequence parameters and material prop
SEQFLAG  = 3; % 3 = sinusoidal FAs and perlin TRs

N = 1000;      % timepoints for FAs and TRs of the sequence


% The dictionary is an array of 
%  (N_T1 x N_T2 x N_offRes x N_timepoints x 3 array)
tic
[materialProperties, sequenceProperties, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, SEQFLAG);
toc


%%
% Extract only fingerprints of interest
materialOfInterest     = struct;
materialOfInterest.csf = configParams.tissue.csf;
materialOfInterest.wm  = configParams.tissue.wm;
materialOfInterest.cst = configParams.tissue.wm;
materialOfInterest.gm  = configParams.tissue.gm;
materialOfInterest.fat = configParams.tissue.fat;

dfToPlot = 0;
materialOfInterest.fat.df = dfToPlot;
materialOfInterest.gm.df  = dfToPlot;
materialOfInterest.wm.df  = dfToPlot;
materialOfInterest.csf.df = dfToPlot;
materialOfInterest.cst.df = -0.03;

tissueIndices = extractIndices(materialProperties, materialOfInterest);

dfToPlot2 = 0.5;
materialOfInterest2 = materialOfInterest;
materialOfInterest2.fat.df = dfToPlot2;
materialOfInterest2.gm.df  = dfToPlot2;
materialOfInterest2.wm.df  = dfToPlot2;
materialOfInterest2.csf.df = dfToPlot2;
materialOfInterest2.cst.df = -0.15;

tissueIndices2 = extractIndices(materialProperties, materialOfInterest2);

subsetOfMRF = struct;
% CSF
subsetOfMRF.csf = squeeze(dictionaryMRF(... 
                            tissueIndices.csf{1}, ...
                            tissueIndices.csf{2}, ...
                            tissueIndices.csf{3}, ...
                            :, :));
% WM
subsetOfMRF.wm = squeeze(dictionaryMRF(... 
                            tissueIndices.wm{1}, ...
                            tissueIndices.wm{2}, ...
                            tissueIndices.wm{3}, ...
                            :, :));

% WM-Offresonance
subsetOfMRF.wmoff = squeeze(dictionaryMRF(... 
                            tissueIndices.cst{1}, ...
                            tissueIndices.cst{2}, ...
                            tissueIndices.cst{3}, ...
                            :, :));

% GM
subsetOfMRF.gm = squeeze(dictionaryMRF(... 
                            tissueIndices.gm{1}, ...
                            tissueIndices.gm{2}, ...
                            tissueIndices.gm{3}, ...
                            :, :));
% Fat
subsetOfMRF.fat = squeeze(dictionaryMRF(... 
                            tissueIndices.fat{1}, ...
                            tissueIndices.fat{2}, ...
                            tissueIndices.fat{3}, ...
                            :, :));

% Fat with off res 500kHz
subsetOfMRF.fatoff = squeeze(dictionaryMRF(... 
                            tissueIndices2.fat{1}, ...
                            tissueIndices2.fat{2}, ...
                            tissueIndices2.fat{3}, ...
                            :, :));
% WM with off res 50kHz
subsetOfMRF.wm50off = squeeze(dictionaryMRF(... 
                            tissueIndices2.wm{1}, ...
                            tissueIndices2.wm{2}, ...
                            tissueIndices2.wm{3}, ...
                            :, :));
                        
                        
%% Plot figure with signals for different tissue types                        
figure, 

% Fat
plot(1:N/2, abs(subsetOfMRF.fat(1:N/2,1) + 1i.*subsetOfMRF.fat(1:N/2,2)))
hold on
% GM
plot(1:N/2, abs(subsetOfMRF.gm(1:N/2,1) + 1i.*subsetOfMRF.gm(1:N/2,2)))
hold on
% WM
plot(1:N/2, abs(subsetOfMRF.wm(1:N/2,1) + 1i.*subsetOfMRF.wm(1:N/2,2)))
hold on
% CSF 
plot(1:N/2, abs(subsetOfMRF.csf(1:N/2,1) + 1i.*subsetOfMRF.csf(1:N/2,2)))
hold on
% WM -30Hz off-res
plot(1:N/2, abs(subsetOfMRF.wmoff(1:N/2,1) + 1i.*subsetOfMRF.wmoff(1:N/2,2)))

legend(['fat ',num2str(materialOfInterest.fat.df .* 10^3),'Hz'], ...
       ['gm ',num2str(materialOfInterest.gm.df .* 10^3),'Hz'], ...
       ['wm ',num2str(materialOfInterest.wm.df .* 10^3),'Hz'], ...
       ['csf ',num2str(materialOfInterest.csf.df .* 10^3),'Hz'], ...
       ['wm ',num2str(materialOfInterest.cst.df .* 10^3),'Hz']);

xlabel('TR index')
ylabel('Signal intensity')
grid on




%%
% Plot the phase space for a few tissue types
figure,

% Choose one tissue type
[xax, yax, zax] = createAxis();
% Viewer position
view([90 0 0]);

subsets = {subsetOfMRF.wm, ...
           subsetOfMRF.gm, ...
           subsetOfMRF.wm50off, ...
           subsetOfMRF.fatoff};
       
colors = ['r','b','k','g'];
       
for i = 1:3
    toPlotMRF = subsets{i};
    % TE = TR/2
    hold on
    scatter3(toPlotMRF(:,1), ...
             toPlotMRF(:,2), ...
             toPlotMRF(:,3), [colors(i), '.']);
	hold on
    scatter3(toPlotMRF(end,1), ...
             toPlotMRF(end,2), ...
             toPlotMRF(end,3), [colors(i), 'o']);

	hold on
    plot3(toPlotMRF(:,1), ...
          toPlotMRF(:,2), ...
          toPlotMRF(:,3), colors(i));
end
         
         
axis equal
axis([-1 1 -1 1 -1 1])



% titlePlot = {[ '(T_1 = ', num2str(T1), 'ms, ', ...
%                 'T_2 = ', num2str(T2), 'ms, ', ...
%                 '\beta = 0^o) '], ...
%              [ '(\alpha = ', num2str(FA), '^o, ', ...
%                 'TR = 8ms)' ] };
% 
% title(titlePlot);























%% Generate Dictionary for a given number of N timepoints
% FLAGPLOT = 1; % Flag for plotting sequence parameters and material prop
% SEQFLAG  = 4; % If == 4 need to provide customValuesSequence
% customValuesSequence.FA = 90;
% customValuesSequence.TR = 20;
% customValuesSequence.RO = 20;
% N = 50;      % timepoints for FAs and TRs of the sequence
% 
% tic
% [materialProperties, sequenceProperties, ...
%     dictionaryMRF, dictionaryMRFmvect] = ...
%                  simulateMRF(N, FLAGPLOT, SEQFLAG, customValuesSequence);
% toc
% 
% %% Plot signal for WM/GM/CSF
% % Extract indices for WM/GM/CSF from the materialProperties set
% tissueIndices = extractIndices(materialProperties, params.tissue);
% 
% % Plot
% component = 2; % x/y/z
% figure
% % plot(1:N, myNorm(abs(extractSubset(dictionaryMRF, tissueIndices.wm))));
% 
% % WM
% plot(1:N, (...
%     extractSubset(dictionaryMRFmvect(:,:,:,:,component), ...
%                   tissueIndices.wm)));
% hold on
% 
% % GM
% plot(1:N, (...
%     extractSubset(dictionaryMRFmvect(:,:,:,:,component), ...
%                   tissueIndices.gm)));
% hold on
% 
% % % CSF
% % plot(1:N, (...
% %     extractSubset(dictionaryMRFmvect(:,:,:,:,component), ...
% %                   tissueIndices.csf)));
% % hold on
% 
% % % Fat
% plot(1:N, (...
%     extractSubset(dictionaryMRFmvect(:,:,:,:,component), ...
%                   tissueIndices.fat)));
% hold on
% legend({'WM','GM','fat'})
% 
% % %% Load Data from data
% % fileName = '../data/dictionary-170315-175532.mat';
% % save(fileName, ...
% %      'materialProperties', 'sequenceProperties', 'dictionaryMRF');
% 

