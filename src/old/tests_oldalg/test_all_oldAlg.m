% % % % 
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 17-03-2017
% % % % Date updated: 17-03-2017
% % % % 
% % % %      TEST ALL
% % % % This script will run all the other testing scripts
% % % % 

% Preprocessing steps
clear all; close all; clc

addpath 1_test1block/
addpath 2_test2blocks/
addpath 3_SteadyState/

addpath ../exchanged/       % Exchanged with Gary
addpath ../helpers/         % Helper functions
addpath ../algs_slow/            % Algs functions
addpath ../preprocess/ % Preprocessing steps
addpath ../perlin/ % Perlin


% % % GLOBAL VARS
TEST1BLOCK  = 0;
TEST2BLOCKS = 1;
TESTNBLOCKS = 0;

% Run cofig file to have the parameters of interest at hand
params = returnPredefinedParameters();


flagToPlot = 0;

%% TEST 1 BLOCK of sequence
if TEST1BLOCK
% % % % 1. Test RF pulse 
% % 1. Set T1, T2 >> TR and df = 0

% %    1.a Keep flip angle fixed and change phase of rf pulse
PhA = [0, 45, 90, 135, 180, 225, 270, 315];
for idx = 1:length(PhA)
    test_RFpulse(params, 90, PhA(idx));
    close all
end

% %    1.b Keep phase angle fixed and change flip angle
FA = [0.1, 45, 90, 135, 180];
for idx = 1:length(FA)
    test_RFpulse(params, FA(idx), 0);
    close all
end


% % 2. Test Relaxation behaviour

% %    2.a For a 


end
%% TEST 2 BLOCKs of sequence
% % % % TODO
if TEST2BLOCKS
    
FA  = [45, 60, 90];
PhA = [0, 45, 90];
test_2blocks_visually(params, FA(1), PhA(1));
    
end
%%  TEST N BLOCKs of sequence
if TESTNBLOCKS
    
    
% % TODO: fig 18.13 from book 
    
    
    
% % % % 
% % % % 1. TESTS BASED ON
% % % % "Understanding SSFP: A geometric perspective"
% % % % by Rohan Dharmakumar

% % % % 
% % % % a. Steady-State ensemble magnetization when beta = 0
test_SSMbetazero(300);



%%
% % % % 
% % % % b. Steady-State ensemble magnetization when beta = -180:180

% % % % b.0 Keep one material property and one sequence design 
% % % %     plot approach to SS for different betas
test_SSMbetaNonZero(300);

%%
% % % %
% % % % b.1 Having a closer look at the LOCUS (SS Mx,My,Mz for betas)
% % % %     Eccentricity of the ellipse is related to T1/T2
test_SSlocus(300);

%%

% % % % Fast Gradient Echo Sequences Including bSSFP
% % % % by Brian Hargreaves

% % 1. Plotting Figure 2c) from the paper
test_phasePrecessionPerTRUnderstandingDottedLines(params, 300);


%%
% % 2. Plotting Figure 2c) from the paper just for TE=TR/2
[sequencePropertiesP, materialPropertiesP, materialOfInterestP] = ...
    test_phasePrecessionPerTR(params, 100);

% % % Doesn't work:
if flagToPlot
    visualiseEvents(sequencePropertiesP, materialPropertiesP, ...
                            materialOfInterestP, 2);
end

%%
% 3. RF phase cycling 
% % % Needs revisiting because 
% % % i don't understand how they get the dphi/2 shift
[sequencePropertiesC, materialPropertiesC, materialOfInterestC] = ...
    test_phaseCycling(params, 200);

if flagToPlot 
    visualiseEvents(sequencePropertiesC, materialPropertiesC, ...
                            materialOfInterestC, 2);
end






end