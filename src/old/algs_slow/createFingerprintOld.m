function fingerprint = createFingerprintOld(matProperty, seqProps, flagIR)
% % % % IRINA GRIGORESCU
% % % % Date created: 02-03-2017
% % % % Date updated: 15-03-2017
% % % % 
% % % % INPUTS:
% % % %     matProperty = struct with
% % % %         T1 (double)
% % % %         T2 (double)
% % % %         offRes (double)
% % % % 
% % % %     seqProps = (struct) with
% % % %         RF = (struct) RF pulse with
% % % %              FA  = (Nx1 double) flip angle (in deg) and 
% % % %              PhA = (Nx1 double) phase angle (in deg)
% % % %         TR = (Nx1 double) repetition times (in ms)
% % % %         RO = (Nx1 double) readout times (in ms)
% % % % 
% % % %     flagIR = set to 1 if you want the sequence to start with an IR
% % % %              it is set to 1 by default
% % % % 
% % % % OUTPUTS:
% % % %     fingerprint = (Nx3 double) with
% % % %             Mx,My,Mz at each time point for a given
% % % %             set of material properties (T1, T2, etc)
% % % % 

% % % % Some preprocessing steps
if nargin < 3  
    flagIR = 1;
end

% Unit vectors:
x = [1 0 0]'; y = [0 1 0]'; z = [0 0 1]';
% Get number of events
Nevents = numel(seqProps.RF.FA);

% % % % FINGERPRINT
fingerprint = zeros(Nevents,3);

% % % % MAGNETIC MOMENT
if flagIR == 1
    mu0  = 0*x + 0*y - 1*z;     % Start with    inversion pulse
else
    mu0  = 0*x + 0*y + 1*z;     % Start without inversion pulse
end

% % % % RUN SIMULATION OF EVENTS
for idx = 1 : Nevents
   
    % % % % Prepare sequence structure for sequenceBlock
    seqProperty = extractSequenceBlock(seqProps, idx);
    
    % % % % Simulate one TR block
    [mu0, muRO, ~] = sequenceBlock(mu0, matProperty, seqProperty);
    
    % % % % Save Mx, My, Mz at RO
    fingerprint(idx,1) = muRO(1);
    fingerprint(idx,2) = muRO(2);
    fingerprint(idx,3) = muRO(3);
    
end



end