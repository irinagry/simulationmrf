function [Mnext, Mro, Mhist] = sequenceBlockOld(Mprev, ...
                        matProperty, seqProperty, ...
                        Ntp, dt)
% % % % IRINA GRIGORESCU
% % % % Date created: 14-03-2017
% % % % Date updated: 15-03-2017
% % % % 
% % % % INPUTS:
% % % %     Mprev = (3x1 double)
% % % %             M- magnetisation vector values before RF pulse 
% % % %     matProperty = (struct) with
% % % %         T1 (double) ms
% % % %         T2 (double) ms
% % % %         offRes (double) kHz (Why? Because 1/ms = kHz)
% % % %     seqProperty = (struct) with
% % % %         RF = RF pulse (struct) with
% % % %              FA  = flip angle (in deg) (double) and 
% % % %              PhA = phase angle (in deg) (double)
% % % %         TR = repetition time (in ms) (double)
% % % %         RO = read-out time (in ms) (double)
% % % %     Ntp = number of timepoints you want to calculate within TR
% % % %     dt  = delta t between the points represented by Ntp
% % % % 
% % % % OUTPUTS:
% % % %     Mnext = (3x1 double) 
% % % %             M- magnetisation vector values before next RF pulse
% % % %     Mro   = (3x1 double)
% % % %             M+ magnetisation vector values at RO
% % % %     Mhist = (3xNtp double)
% % % %             Magnetisation vector evolution for Ntp timepoints

% % % Prerequisites and verifications
if nargin < 4
    % Set everything to 0
    Ntp = 0;
    dt = 0;
    Mhist = zeros(3, Ntp);
end

if seqProperty.RO > seqProperty.TR
    seqProperty.RO = seqProperty.TR;
    disp('Warning: Readout Time > TR. Changed RO == TR');
end

% % 1. Do RF PULSE
Mnext = (Rotz( deg2rad(seqProperty.RF.PhA)) * ...
         (Rotx(-deg2rad(seqProperty.RF.FA)) * ...
          (Rotz(-deg2rad(seqProperty.RF.PhA)) ...
            * Mprev)));
% Mnext = RzRxRz(deg2rad(seqProperty.RF.FA), ...  % alpha
%                deg2rad(seqProperty.RF.PhA)) ... %   phi
%         * Mprev; 

% % 2. Relaxation with OFF-RESONANCE for RO
phiTE = 2*pi * matProperty.offRes * seqProperty.RO; % dw*t

Mro = Rotz(-phiTE) * ...
      Drel(seqProperty.RO, matProperty.T1, matProperty.T2) * ...
      Mnext + ...
      Drelz(seqProperty.RO, matProperty.T1, 1);
  
% % 3. Relaxation with OFF-RESONANCE for entire TR
phiTR = 2*pi * matProperty.offRes * seqProperty.TR; % dw*t

Mnext = Rotz(-phiTR) * ...
        Drel(seqProperty.TR, matProperty.T1, matProperty.T2) * ...
        Mnext + ...
        Drelz(seqProperty.TR, matProperty.T1, 1);
 
% % % % %  
% % % % %  
% % 4. Do the same for the entire evolution of a certain dt if necessary
if Ntp ~= 0
    % Do RF PULSE FIRST
    Mhist(:, 1) = Rotz( deg2rad(seqProperty.RF.PhA)) * ...
              Rotx(-deg2rad(seqProperty.RF.FA)) * ...
              Rotz(-deg2rad(seqProperty.RF.PhA)) ...
              * Mprev;
          
    % DO RELAXATION with off-resonance
    phiDT = 2*pi * matProperty.offRes * dt; % dw*dt
    for idx = 2:Ntp
        Mhist(:, idx) = Rotz(-phiDT) * ...
                    Drel(dt, matProperty.T1, matProperty.T2) * ...
                    Mhist(:, idx-1) + ...
                        Drelz(dt, matProperty.T1, 1);
    end
end
  
  
end




