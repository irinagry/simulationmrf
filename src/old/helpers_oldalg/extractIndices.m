function tissueIndices = extractIndices(materialProperties, tissues)
% IRINA GRIGORESCU
% Date created: 15-03-2017
% Date updated: 15-03-2017
% 
% For old version of the algorithm
% This function extracts the indices (position in space) from 
% the entire set of tissue properties,
% given a structure with tissue properties
% 
% INPUTS:
%     materialProperties = (struct) with
%         T1 (N_T1x1 array)
%         T2 (N_T2x1 array)
%         offRes (N_offResx1 array)
%     tissues  = (struct) with values for T1/T2/df for
%                csf, wm, gm, fat, custom
% 
% OUTPUT:
%     tissueIndices = the indices in the materialProperties set
%   

% % Verify WM is wanted
if isfield(tissues,'wm')
    % % Find WM indices in the material properties
    tissueIndices.wm = { find(materialProperties.T1 == tissues.wm.t1), ...
                     find(materialProperties.T2 == tissues.wm.t2), ...
                     find(materialProperties.offRes == tissues.wm.df) };
end

% % Verify GM is wanted
if isfield(tissues,'gm')
    % % Find GM indices in the material properties
    tissueIndices.gm = { find(materialProperties.T1 == tissues.gm.t1), ...
                     find(materialProperties.T2 == tissues.gm.t2), ...
                     find(materialProperties.offRes == tissues.gm.df) };
end

% % Verify CSF is wanted
if isfield(tissues,'csf')
    % % Find CSF indices in the material properties
    tissueIndices.csf = {find(materialProperties.T1 == tissues.csf.t1), ...
                      find(materialProperties.T2 == tissues.csf.t2), ...
                      find(materialProperties.offRes == tissues.csf.df) };
end
                  
% % Verify Fat is wanted
if isfield(tissues,'fat')
    % % Find Fat indices in the material properties
    tissueIndices.fat = {find(materialProperties.T1 == tissues.fat.t1), ...
                      find(materialProperties.T2 == tissues.fat.t2), ...
                      find(materialProperties.offRes == tissues.fat.df) };
end
                  
% % Verify Custom is wanted
if isfield(tissues,'cst')
    % % Find Custom indices in the material properties
    tissueIndices.cst = {find(materialProperties.T1 == tissues.cst.t1), ...
                      find(materialProperties.T2 == tissues.cst.t2), ...
                      find(materialProperties.offRes == tissues.cst.df) };
end
                 
end