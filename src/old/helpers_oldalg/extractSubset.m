function subset = extractSubset(multiDimArray, indices)
% IRINA GRIGORESCU
% Date created: 15-03-2017
% Date updated: 15-03-2017
% 
% For old version of the algorithm
% This function extracts a subset from a set (bigArray).
% The subset is parameterised with indices
% 
% INPUTS:
%     multiDimArray = a (N1,N2,N3,N4,...,Nm) array
%     indices  = {i1<N1, i2<N2, ..., ip<Np} cell array
%                representing the indices for the subset
%                where p<=m
% 
% OUTPUT:
%     subset = the [ndims(multiDimArray) - length(indices)] 
%              dimensional array extracted from multiDimArray

    lenInd = length(indices);
    % Verify condition p < m (number of indices should be smaller than
    % dimensions of the multiDimArray)
    if lenInd > ndims(multiDimArray)
        warning(['You are trying to extract a subset', ...
                 'of dimension higher than the set']);
    else
    % If condition met then extract subset
        subset = squeeze(multiDimArray(indices{:}, :));
    end
    
% % TODO: verify for each index < each dimension in the multiDimArray    
    
end