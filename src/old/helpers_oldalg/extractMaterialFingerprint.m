function materialProperty = extractMaterialFingerprint(...
                        materialProperties, ...
                        idxT1, idxT2, idxOffRes)
% IRINA GRIGORESCU
% Date created: 14-03-2017
% Date updated: 27-03-2017
% 
% For old version of the algorithm
% This function returns a single structure with material properties
% 
% INPUT
%     materialProperties = the entire material properties structure
%     idxT1      = the T1 index
%     idxT2      = the T2 index
%     idxOffRes  = the off resonance index
% 
% OUTPUT
%     materialProperty = (struct) one material property structure
%                         .T1 (double)
%                         .T2 (double)
%                         .offRes (double)

% Create structure
materialProperty = struct;
% Populate structure
materialProperty.T1 = materialProperties.T1(idxT1);
materialProperty.T2 = materialProperties.T2(idxT2);
materialProperty.offRes = materialProperties.offRes(idxOffRes);
    
end