function seqProperty = extractSequenceBlock(seqProps, idx)
% IRINA GRIGORESCU
% 14-03-2017
% 
% For old version of the algorithm
% This function returns a sequence structure for one specific block
% 
% INPUT
%     seqProps = the entire structure
%     idx      = the block index
% 
% OUTPUT
%     seqProperty = one sequence block structure

seqProperty = struct;
seqProperty.RF.FA  = seqProps.RF.FA(idx);
seqProperty.RF.PhA = seqProps.RF.PhA(idx);
seqProperty.TR     = seqProps.TR(idx);
seqProperty.RO     = seqProps.RO(idx);
    
end