% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 24-04-2017
% % % % Date updated: 24-04-2017
% % % % 
% % % % This is a script to test regression on the MRF dictionary.
% % % % It has a lot of dumped code that needed refactoring, but it's nice
% % % % to have it around for future reference.


% % Preprocessing steps
clear all; close all; clc
addpath ../helpers/ % Helper functions
addpath ../helpers/misc/
addpath ../algs/    % Algs functions
addpath ../preprocess/ % Preprocessing steps
addpath ../perlin/     % Perlin stuff
addpath ../learning/   % Learning stuff

% Run cofig file to have the parameters of interest at hand
configParams = returnPredefinedParameters();

%% Generate dictionary for a range of timepoints

% CUSTOM Set of Material Tuples
% % % % Custom Material
MATFLAG               = 3;               % need to provide customMaterial
customMaterial.T1     =  100:20:2000 ;     
customMaterial.T2     = [ 20:5:300 350:50:1000];     
customMaterial.offRes = -0.05:0.01:0.05; 

% SEQUENCE
SEQFLAG  = 3; % sinusoidal FAs and perlin noise TRs

% Plot flag
FLAGPLOT = 0;

% RUN SIMULATION for a given of N
tic
N = 1000;
% Run simulation
[materialProperties, sequenceProperties, ...
    materialTuples, M, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, ...
                 SEQFLAG, struct, ...
                 MATFLAG, customMaterial, 1);
             
N_T1     = numel(materialProperties.T1);
N_T2     = numel(materialProperties.T2);
N_offRes = numel(materialProperties.offRes);


% Create dictionary of signal values (M x 2N)             
signalDictionaryMRF = [ squeeze(dictionaryMRF(1:M, :, 1))  , ...
                        squeeze(dictionaryMRF(1:M, :, 2)) ];  
% signalDictionaryMRFMagnitude = abs(signalDictionaryMRF(:,   1:  N) + ...
%                                1i.*signalDictionaryMRF(:, N+1:end));
                      
% % Save data
% save('dictionaryMRFMay', ...
%      'materialTuples', ...      % materialTuples     (Mx3 double) 
%      'signalDictionaryMRF', ... % sequenceProperties (NxM double)
%      '-v7.3');
                      
% Learn Mapping
A = learnMap(materialTuples, signalDictionaryMRF');
%rank(A)


% % Learn Mapping with Ridge Regression
% XXT = signalDictionaryMRF' * signalDictionaryMRF; % XX'
% 
% % B = (Y X^T) (X X^T + kI)^-1
% k = 0.00000001; %0.00001; % norm(signalDictionaryMRF); % 0.1;
% B = (materialTuples * signalDictionaryMRF) * ... 
%      pinv(XXT + k.*eye(length(XXT)));
% C = A;

toc

%% Having a look at the mapping
% Do SVD on A
% A=B;
% % Don't do it because A is too big, use 'econ' flag
[U, S, V] = svd(A, 'econ'); 

figure
% Crete axis of 3D plot
createAxis(0.15);
% Label axis
xlabel('T_1'); ylabel('T_2'); zlabel('\Delta f');
hold on
% Plot A
scatter3(A(1,:)./S(1,1), A(2,:)./S(2,2), A(3,:)./S(3,3), '.');

% Plot linear map as image
figure
imagesc(A)
% indices = (A(3,:) < 1e-07);
% imagesc([A(1,:)./S(1,1) ; A(2,:)./S(2,2) ; A(3,:)./S(3,3) ; indices])
colorbar 

%%
[M, N] = size(signalDictionaryMRF);

Y_test      = materialTuples;
Y_predicted = A * signalDictionaryMRF';
% load('~/Work/pythonTest/predictedMaterialTuples.mat');
% Y_predicted_py = predictedMaterialTuples';
% Y_predicted = Y_predicted_py;

residuals    = Y_predicted - Y_test;
percError    = [ (residuals(1, :))./(Y_test(1, :)).*100  ; ...
                 (residuals(2, :))./(Y_test(2, :)).*100] ;
% residuals_py = Y_test - Y_predicted_py;
SSE          = sum(residuals.^2, 2);
SST          = sum((Y_test - repmat(mean(Y_test, 2), [1, M])).^2, 2);
Rsq          = 1 - (SSE./SST);

resultsT1 = [Y_test(1,:)' , Y_predicted(1,:)'];
resultsT2 = [Y_test(2,:)' , Y_predicted(2,:)'];
resultsdf = [Y_test(3,:)' , Y_predicted(3,:)'];

%%
% Plots percentage error 
figure
scatter3(Y_test(1, 5:11:end), Y_test(2, 5:11:end), ...
         percError(1, 5:11:end), '.'); 
hold on
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1');ylabel('T_2');zlabel('%err T_1');
title('%err in T_1')


%% PLOT in 1D the errors
% % % % Plot
Mshow = M; %200; %50; % Mtest

figure

% T_1 real vs predicted
subplot(2,3,1)
resultsT1ToShow = sortrows(resultsT1(1:Mshow,:),1); clear resultsT1
plot(1:Mshow, resultsT1ToShow(:,1), 'o', 'MarkerSize', 2);
hold on
plot(1:Mshow, resultsT1ToShow(:,2), '.');
xlabel('test points');
ylabel('T_1 (ms)');
grid on
legend('real', 'predicted', 'Location', 'southeast');
title('$T_1$ real and $T_1$ predicted', 'interpreter', 'latex');

% T_2 real vs predicted
subplot(2,3,2)
resultsT2ToShow = sortrows(resultsT2(1:Mshow,:),1); clear resultsT2
plot(1:Mshow, resultsT2ToShow(:,1), 'o', 'MarkerSize', 2);
hold on
plot(1:Mshow, resultsT2ToShow(:,2), '.');
xlabel('test points');
ylabel('T_2 (ms)');
grid on
legend('real', 'predicted', 'Location', 'southeast');
title('$T_2$ real and $T_2$ predicted', 'interpreter', 'latex');

% df real vs predicted
subplot(2,3,3)
resultsdfToShow = sortrows(resultsdf(1:Mshow,:),1); clear resultsdf
plot(1:Mshow, resultsdfToShow(:,1).*1e+03, 'o', 'MarkerSize', 2);
hold on
plot(1:Mshow, resultsdfToShow(:,2).*1e+03, '.');
xlabel('test points');
title('$\Delta f$ real and $\Delta f$ predicted', 'interpreter', 'latex');
grid on
legend('real', 'predicted', 'Location', 'southeast');
title('$\Delta f$ real and $\Delta f$ predicted', 'interpreter', 'latex');

% Percentage Error of T_1
subplot(2,3,4)
scatter(1:Mshow, percError(1, 1:Mshow), '.');
xlabel('test points');
ylabel('%error T_1 (ms)');
title({['$SSE_{T_1} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE(1))], ...
       ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                     '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
    num2str(Rsq(1))]}, 'interpreter', 'latex');
grid on

% Percentage Error of T_2
subplot(2,3,5)
scatter(1:Mshow, percError(2, 1:Mshow), '.');
xlabel('test points');
ylabel('%error T_2 (ms)');
title({['$SSE_{T_2} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE(2))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq(2))]}, 'interpreter', 'latex');
grid on

% Residuals df
subplot(2,3,6)
scatter(1:Mshow, residuals(3, 1:Mshow), '.');
xlabel('test points');
ylabel('Residuals \Delta f (kHz)');
title({['$SSE_{\Delta f} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE(3))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq(3))]}, 'interpreter', 'latex');
grid on



%% Plot 3D errors with slice

[x,y] = meshgrid(customMaterial.T1, customMaterial.T2);
[mT2, mT1] = size(x); % m - T2, n - T1
mdf = length(customMaterial.offRes);

materials = {'T_1', 'T_2', '\Delta f'};

figure
residualsVolume = zeros(mT2, mT1, mdf) - 1000;
lala = zeros(mT1, mT2);
% For each off resonance frequency
for idxdf = 1:mdf
    
    transp = 1 - 0.7*idxdf/mdf;
    
    % For each material property 
    for idMatProp = 1:3
        
        % Reziduals at current df for current material property
        if idMatProp < 3
            resz = percError(idMatProp, idxdf:mdf:M);
        else
            resz = residuals(idMatProp, idxdf:mdf:M);
        end
    
        % Put them in a 2D matrix
        idx = 1;
        for iT2 = 1:N_T2                       % T_1 (mT1)
            for iT1 = 1:N_T1                   % T_2 (mT2)
                if x(iT2,iT1) >= y(iT2,iT1)
                    residualsVolume(iT2,iT1,idxdf) = resz(idx);
                    lala(iT1,iT2) = 1;
                    idx = idx + 1;
                else
                    residualsVolume(iT2,iT1,idxdf) = NaN;
                end
            end
        end
        

        % Plot Slice
        subplot(1,3,idMatProp);
        view([75 15 10]);
        view(25, 7);      % az = 25, el = 7
        hz = slice(residualsVolume, ...
                   [], [], idxdf);
        alpha(hz, transp);
        colormap jet ; colorbar
        set(findobj(gca,'Type','Surface'),'EdgeColor','none')
        hold on
        if idMatProp < 3
            title(['Percentage error for ', materials{idMatProp}]);
        else
            title(['Residuals for ', materials{idMatProp}]);
        end
        xlabel('T_1'); ylabel('T_2'); zlabel('\Delta f');
        
        % Values on the X-axis
        dT1 = ceil(N_T1/5);
        set(gca,'XTick', 1 : dT1 : N_T1); 
        set(gca,'XTickLabels', ...
         strread(num2str(customMaterial.T1(1:dT1:end)), '%s'));
        % Values on the Y-axis
        dT2 = ceil(N_T2/10);
        set(gca,'YTick', 1 : dT2 : N_T2); 
        set(gca,'YTickLabels', ...
         strread(num2str(customMaterial.T2(1:dT2:end)), '%s'));
        % Values on the Z-axis
        doffRes = ceil(N_offRes/10);
        set(gca,'ZTick', 1 : doffRes : N_offRes);
        set(gca,'ZTickLabels', ...
         strread(num2str(customMaterial.offRes(1:doffRes:end)), '%s'));
    end
end



% % % %% Plot 3D-plot of errors
% % % % Version with scatter
% % % 
% % % MM = ceil(M*0.1);
% % % 
% % % % First try was with scatter plot
% % % figure
% % % for idxdf = 1:1
% % % 
% % %     subplot(1,3,1) % T_1 error
% % %     scatter3(materialTuples(3,idxdf:11:M), ...     % df
% % %              materialTuples(1,idxdf:11:M), ...     % T_1
% % %              materialTuples(2,idxdf:11:M), ...     % T_2
% % %              ones(1, M/11) + 10, ...               % size
% % %              residuals(1,idxdf:11:M),      ...  % color
% % %              'filled'); 
% % % 	xlabel('\Delta f'); ylabel('T_1'); zlabel('T_2');
% % %     hold on
% % %     
% % % %     subplot(1,3,2) % T_2 error
% % % %     scatter3(materialTuples(3,idxdf:11:M), ...     % df
% % % %              materialTuples(1,idxdf:11:M), ...     % T_1
% % % %              materialTuples(2,idxdf:11:M), ...     % T_2
% % % %              ones(1, 4130) + 10, ...               % size
% % % %              residuals(2,idxdf:11:M),      ...  % color
% % % %              'filled');     
% % % % 	xlabel('\Delta f'); ylabel('T_1'); zlabel('T_2');
% % % %     hold on
% % % %     
% % % %     subplot(1,3,3) % df error
% % % %     scatter3(materialTuples(3,idxdf:11:M), ...     % df
% % % %              materialTuples(1,idxdf:11:M), ...     % T_1
% % % %              materialTuples(2,idxdf:11:M), ...     % T_2
% % % %              ones(1, 4130) + 10, ...               % size
% % % %              residuals(3,idxdf:11:M),      ...  % color
% % % %              'filled');     
% % % % 	xlabel('\Delta f'); ylabel('T_1'); zlabel('T_2');
% % % %     hold on
% % % end
% % % 
% % % 
% % % % For df = constant
% % % % resultsT1 = [Y_test(1,:)' , Y_predicted(1,:)'];
% % % % resultsT2 = [Y_test(2,:)' , Y_predicted(2,:)'];
% % % % resultsdf = [Y_test(3,:)' , Y_predicted(3,:)'];















