% % % % Playing with SVD
% % % % Temporary file: Needs data to be in memory


% % % % % % % % %
% % % % % % % % % SVD Stuff
% % % % % % % % %
%%
Xtrain = signalsNorm.train';
% Ytrain = materialTuplesScaled.train;
% [Ux, Sx, Vx] = svd(Xtrain*Xtrain');
% [Uy, Sy, Vy] = svd(Ytrain*Ytrain');
% 
% totalSizex = size(Sx,1);
% 
% eigvsS = zeros(1,totalSizex);
% for i = 1:totalSizex
%     eigvsS(i) = Sx(i,i);
% end

%% Looking at a signal and its approximations


% Xt              = Xtrain(1:2*N, 1:kMax);
% [Uxt, Sxt, Vxt] = svd(Xt);
% 
tic
[Uxt, Sxt, Vxt] = svd(Xtrain, 'econ'); %kMax);
toc

Xtapp  = zeros(size(Xtrain));

energK = zeros(1, kMax);
totalEnergy = sum(diag(Sxt));

%%
kMax            =  150;
nMin            =    1;
nMax            =  1000;

figure('units','normalized','outerposition', [0 0 1 1]);   

signalNumber = 1;
% Plot Signal Real signal
subplot(1,2,1)
plot(nMin:nMax, abs(Xtrain(nMin:nMax, signalNumber).^2 + ...
                    Xtrain(nMax+1:end, signalNumber).^2), 'k'), hold on
xlim([nMin, nMax])
ylim([0, 0.005])
xlabel('T_R index')
ylabel('M_x')
grid on
title('Real Signal')

pause

for k = kMax:-1:1

    % Calculate approximation
    Xtapp = Uxt(:, 1:k) * Sxt(1:k, 1:k) * Vxt(:, 1:k)';
    
    % Calculate energy retained
    energK(k) = (1/totalEnergy) * sum(diag(Sxt(1:k,1:k)));

    % % % Plot Signal
    subplot(1,2,1)
    plot(nMin:nMax, abs(Xtapp(nMin:nMax, signalNumber).^2 + ...
                        Xtapp(nMax+1:end, signalNumber).^2))
    hold on
    grid on
    xlim([nMin, nMax])
    ylim([0, 0.005])
    % Title
    title({ 'Approximated Signal', ...
           ['k = ', num2str(k), ' singular values used'], ...
           ['Energy retained = ', num2str(energK(k))]})
    
    % % % Plot energy for k retained singular values
    subplot(1,2,2)
    scatter(k, energK(k), '.b')
	xlabel('k')
    ylabel('e(k)')
    title('Energy ratio')
    hold on
	grid on
    xlim([0, kMax])
    ylim([0, 1])
    
    pause(0.1)
    
end

%%
figure

subplot(1,2,1)
plot(1:size(Sxt,1), diag(Sxt))
xlabel('i')
ylabel('\lambda_i')

subplot(1,2,2)
plot(1:size(energK,2), energK)
xlabel('k')
ylabel('e(k)')

%%  Looking at the left singular vectors, 
% % singular values and 
% % right singular vectors
% % of the signals and material tuples
% % figure
% % 
% % subplot(2,3,1)
% % imagesc(Ux), colormap jet, colorbar
% % title({'U', 'Signals'})
% % subplot(2,3,4)
% % imagesc(Uy), colormap jet, colorbar
% % title({'U', 'Tissue Properties'})
% % 
% % subplot(2,3,2)
% % imagesc(Sx(1:10,1:10)), colormap jet, colorbar
% % title({'S', 'Signals'})
% % subplot(2,3,5)
% % imagesc(Sy), colormap jet, colorbar
% % title({'S', 'Tissue Properties'})
% % 
% % subplot(2,3,3)
% % imagesc(Vx), colormap jet, colorbar
% % title({'V', 'Signals'})
% % subplot(2,3,6)
% % imagesc(Vy), colormap jet, colorbar
% % title({'V', 'Tissue Properties'})

% % %% Plot the singular vectors 
% % figure
% % 
% % for i = 1:size(Vy,1)
% %     % % V 
% %     plot3([0, Vy(i,1)],[0, Vy(i,2)],[0, Vy(i,3)], 'r')
% %     hold on
% %     
% %     % Tips of vectors
% %     scatter3(Vy(i,1), Vy(i,2), Vy(i,3), 'or')
% %     
% %     grid on
% % end
% % 
% % pause
% % 
% % for i = 1:size(Vy,1)
% %     % % V 
% %     plot3([0, Sy(i,i)*Vy(i,1)], ...
% %           [0, Sy(i,i)*Vy(i,2)], ...
% %           [0, Sy(i,i)*Vy(i,3)], 'b--')
% %     hold on
% %     
% %     % Tips of vectors
% %     scatter3(Sy(i,i)*Vy(i,1), Sy(i,i)*Vy(i,2), Sy(i,i)*Vy(i,3), 'ob')
% %     
% %     grid on
% % end
% % 
% % pause
% % 
% % for i = 1:size(Vy,1)
% %     % % V 
% %     plot3([0, Uy(i,1)*Sy(i,i)*Vy(i,1)], ...
% %           [0, Uy(i,2)*Sy(i,i)*Vy(i,2)], ...
% %           [0, Uy(i,3)*Sy(i,i)*Vy(i,3)], 'g--')
% %     hold on
% %     
% %     % Tips of vectors
% %     scatter3(Uy(i,1)*Sy(i,i)*Vy(i,1), ...
% %              Uy(i,2)*Sy(i,i)*Vy(i,2), ...
% %              Uy(i,3)*Sy(i,i)*Vy(i,3), 'og')
% %     
% %     grid on
% % end

% % %% Eigenvalues
% % figure
% % 
% % % All
% % subplot(2,3,1:3)
% % startL = 1; endL = totalSizex;
% % plot(startL:endL, eigvsS(startL:endL), 'r.')
% % xlabel('i');
% % ylabel('\lambda_i');
% % grid on
% % 
% % % First 5
% % subplot(2,3,4)
% % startL = 1; endL = 5;
% % plot(startL:endL, eigvsS(startL:endL), 'ro')
% % xlabel('i');
% % ylabel('\lambda_i');
% % grid on
% % 
% % % Next 25
% % subplot(2,3,5)
% % startL = 6; endL = 30;
% % plot(startL:endL, eigvsS(startL:endL), 'ro')
% % xlabel('i');
% % ylabel('\lambda_i');
% % grid on
% % 
% % % Next 70
% % subplot(2,3,6)
% % startL = 31; endL = 100;
% % plot(startL:endL, eigvsS(startL:endL), 'ro')
% % xlabel('i');
% % ylabel('\lambda_i');
% % grid on
% %                   




