% % IRINA GRIGORESCU
% % This script is to be run when you want to add paths needed for this 
% % folder
% % Andt to start from a clean slate

% % Addpaths for helpers
addpath(genpath('../helpers/'))

% Addpaths for preprocess
addpath(genpath('../preprocess/'))

% Addpaths for create set of tuples and whatnot
addpath(genpath('../algs/'))

% Addpaths for exchanged 
addpath(genpath('../extendedPhaseGraph/'))
