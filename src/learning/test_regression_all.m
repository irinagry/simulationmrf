% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 04-05-2017
% % % % Date updated: 04-05-2017
% % % % 
% % % % This is a script to test regression on the MRF dictionary.
% % % % 


% % Preprocessing steps
clear all; close all; clc
addpath ../helpers/      % Helper functions
addpath ../helpers/misc/ % Misc functions
addpath ../algs/         % Algs functions
addpath ../preprocess/   % Preprocessing steps
addpath ../perlin/       % Perlin stuff
addpath ../learning/     % Learning stuff

% Run cofig file to have the parameters of interest at hand
configParams = returnPredefinedParameters();

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% TRAINING AND TESTING ON THE SAME DATA %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST 1 
% % % trainTestSame/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%  -50 <= df <=   50, dt =  5
% Mttrain = 
% % % Test on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%  -50 <= df <=   50, dt =  5
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 13; % see case  "13" in retrieveMaterialProperties.m
MATFLAG.test = 130; % see case "130" in retrieveMaterialProperties.m

test_regression_bycase(N, MATFLAG, 'trainTestSame/df50_T12000_T2200_', 0);     



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% INTERPOLATION for small values of T1/T2/df | DENSE SAMPLING %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST 2 (DENSE sampling of tissue space) - Keep df FIXED
% % % % interpDense_df0-10Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%  -10 <= df <=   10, dt =  1
% Mttrain = 73332
% % % Test on:
%  110   <= T1 <= 1990,   dt = 40
%   22.5 <= T2 <=  192.5, dt = 10
%  -10   <= df <=   10,   dt =  2
% Mtest = 9339
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 11; % see case  "11" in retrieveMaterialProperties.m
MATFLAG.test = 111; % see case "111" in retrieveMaterialProperties.m

test_regression_bycase(N, MATFLAG, 'interpDense_df0-10Hz/fixedOffRes_', 0); 
    
%% TEST 3 (DENSE sampling of tissue space) - df CHANGED
% % % % interpDense_df0-10Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%  -10 <= df <=   10, dt =  1
% Mttrain = 73332
% % % Test on:
%  110   <= T1 <= 1990,   dt = 40
%   22.5 <= T2 <=  192.5, dt = 10
%  - 9.5 <= df <=    9.5, dt =  2
% Mtest = 9339
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 11; % see case  "11" in retrieveMaterialProperties.m
MATFLAG.test = 112; % see case "112" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, 'interpDense_df0-10Hz/changedOffRes-0.5_', 1); 

%% TEST 4 (VERY DENSE sampling of df) - Keep df FIXED
% % % % interpDense_df0-1Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%   -1 <= df <=    1, dt =  0.1
% Mttrain = 
% % % Test on:
%  110   <= T1 <= 1990,   dt = 40
%   22.5 <= T2 <=  192.5, dt = 10
%   -1   <= df <=    1,   dt =  0.1
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 12; % see case  "12" in retrieveMaterialProperties.m
MATFLAG.test = 121; % see case "121" in retrieveMaterialProperties.m

test_regression_bycase(N, MATFLAG, 'interpDense_df0-1Hz/fixedOffRes_', 1); 
    
%% TEST 5 (VERY DENSE sampling of df) - df CHANGED
% % % % interpDense_df0-1Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%   -1 <= df <=    1, dt =  0.1
% Mttrain = 
% % % Test on:
%  110   <= T1 <= 1990,   dt = 40
%   22.5 <= T2 <=  192.5, dt = 10
%   -1   <= df <=    1,   dt =  0.1
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 12; % see case  "11" in retrieveMaterialProperties.m
MATFLAG.test = 122; % see case "122" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, 'interpDense_df0-1Hz/changedOffRes-0.05_', 1);  


%% TEST 6 (SPARSE sampling of df) - Keep df FIXED
% % % % interpDense_df0-50Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%  -50 <= df <=   50, dt =  5
% Mttrain = 
% % % Test on:
%  110   <= T1 <= 1990,   dt = 40
%   22.5 <= T2 <=  192.5, dt = 10
%  -50   <= df <=   50,   dt = 10
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 13; % see case  "13" in retrieveMaterialProperties.m
MATFLAG.test = 131; % see case "131" in retrieveMaterialProperties.m

test_regression_bycase(N, MATFLAG, 'interpDense_df0-50Hz/fixedOffRes_', 1); 
    
%% TEST 7 (SPARSE sampling of df) - df CHANGED
% % % % interpDense_df0-50Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%  -50 <= df <=   50, dt =  5
% Mttrain = 
% % % Test on:
%  110   <= T1 <= 1990,   dt = 40
%   22.5 <= T2 <=  192.5, dt = 10
%  -47.5 <= df <=   47.5, dt = 10
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 13; % see case  "13" in retrieveMaterialProperties.m
MATFLAG.test = 132; % see case "132" in retrieveMaterialProperties.m

test_regression_bycase(N, MATFLAG, 'interpDense_df0-50Hz/changedOffRes-2.5_', 1); 

%% TESTS 8 and 9 are a bit uninteresting 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% INTERPOLATION for big values of T1/T2/df | SCARCE SAMPLING %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST 8 (SCARCE sampling of tissue space)  
% interpScarce_bigT1T2df_df0-400Hz/
% % % Training on:
% 2000 <= T1 <= 5000, dt = 300
%  200 <= T2 <= 3000, dt = 200 
% -400 <= df <=  400, dt =  50
% Mttrain = 
% % % Test on:
% 2150 <= T1 <= 4550, dt = 600
%  300 <= T2 <= 2700, dt = 400 
% -400 <= df <=  400, dt = 100
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  22; % see case  "22" in retrieveMaterialProperties.m
MATFLAG.test  = 222; % see case "222" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, ...
    'interpScarce_bigT1T2df_df0-400Hz/fixedOffRes_', 1); 
    

%% TEST 9 (SCARCE sampling of tissue space) - changed df  
% interpScarce_bigT1T2df_df0-400Hz/
% % % Training on:
% 2000 <= T1 <= 5000, dt = 300
%  200 <= T2 <= 3000, dt = 200 
% -400 <= df <=  400, dt =  50
% Mttrain = 
% % % Test on:
% 2150 <= T1 <= 4550, dt = 600
%  300 <= T2 <= 2700, dt = 400 
% -375 <= df <=  375, dt = 100
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  22; % see case  "22" in retrieveMaterialProperties.m
MATFLAG.test  = 223; % see case "222" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, ...
    'interpScarce_bigT1T2df_df0-400Hz/changedOffRes-25_', 1); 

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% INTERPOLATION for small values of T1/T2/df | SCARCE SAMPLING %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST 8 (SCARCE sampling of tissue space)  
% interpScarce_df0-1Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 100
%   20 <= T2 <=  200, dt =  25
%  -50 <= df <=   50, dt =  5
% Mttrain = 
% % % Test on:
%  190   <= T1 <= 1990,   dt = 200
%   42.5 <= T2 <=   92.5, dt =  50
%  -47.5 <= df <=   47.5, dt =  10
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  14; % see case  "14" in retrieveMaterialProperties.m
MATFLAG.test  = 141; % see case "141" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, 'interpScarce_df0-400Hz/fixedOffRes_', 1); 
    

%% TEST 9 (SCARCE sampling of tissue space) - changed df  
% interpScarce_df0-1Hz/
% % % Training on:
%  100 <= T1 <= 2000, dt = 100
%   20 <= T2 <=  200, dt =  25
%  -50 <= df <=   50, dt =  5
% Mttrain = 
% % % Test on:
%  190   <= T1 <= 1990,   dt = 200
%   42.5 <= T2 <=   92.5, dt =  50
%  -47.5 <= df <=   47.5, dt =  10
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  14; % see case  "14" in retrieveMaterialProperties.m
MATFLAG.test  = 142; % see case "142" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, 'interpScarce_df0-400Hz/changedOffRes-25_', 1); 



%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% INTERPOLATION for small values of T1/T2/df | 10th missing    %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST 10 (Missing every other 10th entry) - fixed df  
% interDense_10thMissing/
% % % Training on:
%  100 <= T1 <= 2000, dt = 10 (missing every other 10th entry)
%   20 <= T2 <=  200, dt =  5 (missing every other 10th entry)
%   -2 <= df <=    2, dt = 	0.1 (missing every other 10th entry)
% Mttrain = 
% % % Test on:
%  190   <= T1 <= 1990,   dt = 100 (the missing entries)
%   65   <= T2 <=  165,   dt =  50 (the missing entries)
%   -1.9 <= df <=    1.6, dt = 	 1 not changed, just less
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  44; % see case  "44" in retrieveMaterialProperties.m
MATFLAG.test  = 441; % see case "442" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, 'interpDense_10thMissing/fixedOffRes_', 0); 





%% TEST 11 (Missing every other 10th entry) - changed df  
% interpDense_10thMissing/
% % % Training on:
%  100 <= T1 <= 2000, dt = 10 (missing every other 10th entry)
%   20 <= T2 <=  200, dt =  5 (missing every other 10th entry)
%  -50 <= df <=   50, dt = 	2.5 (missing every other 10th entry)
% Mttrain = 
% % % Test on:
%  190   <= T1 <= 1990,   dt = 100 (the missing entries)
%   65   <= T2 <=  165,   dt =  50 (the missing entries)
%  -50   <= df <=   50,   dt = 	 2.5 (the missing entries)
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  44; % see case  "44" in retrieveMaterialProperties.m
MATFLAG.test  = 442; % see case "442" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, ...
    'interpDense_10thMissing/changedOffRes50Hz_', 1); 


%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% EXTRAPOLATION for small values of T1/T2/df                   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST 12 (Test data is outside the range of training data)
% extrapDense/fixedOffRes_
% % % Training on:
%  100 <= T1 <= 2000, dt = 10
%   20 <= T2 <=  200, dt =  5
%   -1 <= df <=    1, dt = 	0.1
% Mttrain = 
% % % Test on:
% 2100   <= T1 <= 2300,   dt = 100 
%  205   <= T2 <=  250,   dt =  50 
%   -1   <= df <=    1,   dt = 	0.1
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  55; % see case  "55" in retrieveMaterialProperties.m
MATFLAG.test  = 551; % see case "551" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, 'extrapDense/fixedOffRes_', 0); 


%% TEST 13 (Test data is outside the range of training data)
% extrapDense/fixedOffRes_
% % % Training on:
%  100 <= T1 <= 2000, dt = 10
%   20 <= T2 <=  200, dt =  5
%   -1 <= df <=    1, dt = 	0.1
% Mttrain = 
% % % Test on:
% 2100   <= T1 <= 2300,   dt = 100 
%  205   <= T2 <=  250,   dt =  50 
%    1.1 <= df <=  1.5,   dt = 	0.1
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  55; % see case  "55" in retrieveMaterialProperties.m
MATFLAG.test  = 552; % see case "552" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG, 'extrapDense/changedOffRes_', 1); 


%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% NOISE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST 14 (TEST data is NOISY TRAINING DATA)
% noise/
% % % Training on:
%  100 <= T1 <= 2000, dt = 20
%   20 <= T2 <=  200, dt =  5
%  -50 <= df <=   50, dt =  5
% Mttrain = 
% % % Test on:
%  100 <= T1 <= 2000, dt = 60
%   20 <= T2 <=  200, dt = 20
%  -50 <= df <=   50, dt = 20
% Mtest = 
N = 1000;
MATFLAG = struct; 
MATFLAG.train = 13; % see case  "13" in retrieveMaterialProperties.m
MATFLAG.test = 130; % see case "130" in retrieveMaterialProperties.m

% 1  , 0.5     - too much noise
% 0.1, 0.05    - visibly noisy

noiseSD = [0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001];
noiseSD = 0.0005;
for i = 1:length(noiseSD)
    test_regression_bycase(N, MATFLAG, ...
        ['noise/OffRes1Hz_noiseSD', num2str(noiseSD(i)), '_'], 0, ...
        noiseSD(i));
end













    
    
%% 
GOFURTHERFLAG = 0; 
    
if GOFURTHERFLAG    
    
    
%% TEST 6 (DENSE sampling of tissue space)   
% % % Training on
% T1 <= 2000
% T2 <=  200 and 
% df in [-10, 10] 
% missing every other 10-th entry
% % % Testing on
% those missed 10-th entries
N = 1000;
MATFLAG = struct; 
MATFLAG.train =  44; % see case  "22" in retrieveMaterialProperties.m
MATFLAG.test  = 444; % see case "222" in retrieveMaterialProperties.m
test_regression_bycase(N, MATFLAG);
    
%% TEST 2 - Dense
% % % DICTIONARY: Dense part of data set

% % % % % % % % % % % % % % % DICTIONARY PART
tic
% Run simulation on DENSE (case 11)
[materialPropertiesDense.train, sequencePropertiesDense, ...
    materialTuplesDense.train, MDense.train, ...
    dictionaryMRF] = ...
                simulateMRF(1000, 1, ... % N = #timepoints, FLAGPLOT
                 3, struct, ...          % SEQFLAG, SEQstruct
                11, struct, 1);          % MATFLAG, MATstruct, flagIR

% Create dictionary of signal values (M x 2N)             
signalDictionaryDenseMRF.train = [ ...
                        squeeze(dictionaryMRF(1:MDense.train, :, 1))  , ...
                        squeeze(dictionaryMRF(1:MDense.train, :, 2)) ]; 
                         
                         
% Run simulation on DENSE for test (case 111)
[materialPropertiesDense.test, ~, ...
    materialTuplesDense.test, MDense.test, ...
    dictionaryMRF] = ...
                simulateMRF(1000, 1, ... % N = #timepoints, FLAGPLOT
                 5, sequencePropertiesDense, ...      % SEQFLAG, SEQstruct
               111, struct, 1);          % MATFLAG, MATstruct, flagIR
            
% Create dictionary of signal values (M x 2N)             
signalDictionaryDenseMRF.test = [ ...
                        squeeze(dictionaryMRF(1:MDense.test, :, 1))  , ...
                        squeeze(dictionaryMRF(1:MDense.test, :, 2)) ]; 
                         
% Normalize signals Train
[signalsDenseNorm.train, signalsDenseNormCplx.train] = ...
    normalizeSignals(signalDictionaryDenseMRF.train);

% Normalize signals Test
[signalsDenseNorm.test, signalsDenseNormCplx.test] = ...
    normalizeSignals(signalDictionaryDenseMRF.test);

% Look at the entire dictionary as an image
figure
subplot(1,2,1)
imagesc(sqrt(signalsDenseNorm.train(:,    1:1000).^2 + ...
             signalsDenseNorm.train(:, 1001:2000).^2));
xlabel('#timepoints'); ylabel('#(dictionary entries)');
title('Dictionary Dense TRAIN');

subplot(1,2,2)
imagesc(sqrt(signalsDenseNorm.test(:,    1:1000).^2 + ...
             signalsDenseNorm.test(:, 1001:2000).^2));
xlabel('#timepoints'); ylabel('#(dictionary entries)');
title('Dictionary Dense TEST');

% Look at one signal from the dictionary train vs test
figure,
plot(sqrt(signalsDenseNorm.train(1,    1:1000).^2 + ...
          signalsDenseNorm.train(1, 1001:2000).^2));
hold on
plot(sqrt(signalsDenseNorm.test(1,    1:1000).^2 + ...
          signalsDenseNorm.test(1, 1001:2000).^2));
legend(['T_1 = ', num2str(materialTuplesDense.train(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuplesDense.train(2,1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuplesDense.train(3,1).*1000),' Hz'], ...
       ['T_1 = ', num2str(materialTuplesDense.test(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuplesDense.test(2,1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuplesDense.test(3,1).*1000),' Hz']);
      
clear dictionaryMRF
toc

%% TEST 3 - Sparse
% % % DICTIONARY: Sparse part of data set

% % % % % % % % % % % % % % % DICTIONARY PART
tic
% Run simulation on SPARSE - train data (case 22)
[materialPropertiesSparse.train, sequencePropertiesSparse, ...
    materialTuplesSparse.train, MSparse.train, ...
    dictionaryMRF] = ...
                simulateMRF(1000, 1, ... % N = #timepoints, FLAGPLOT
                 3, struct, ...          % SEQFLAG, SEQstruct
                22, struct, 1);          % MATFLAG, MATstruct, flagIR

% Create dictionary of signal values (M x 2N)             
signalDictionarySparseMRF.train = [...
                    squeeze(dictionaryMRF(1:MSparse.train, :, 1))  , ...
                    squeeze(dictionaryMRF(1:MSparse.train, :, 2)) ]; 

% Run simulation on SPARSE - test data (case 222)
[materialPropertiesSparse.test, ~, ...
    materialTuplesSparse.test, MSparse.test, ...
    dictionaryMRF] = ...
                simulateMRF(1000, 1, ...       % N = #timepoints, FLAGPLOT
                 5, sequencePropertiesSparse, ... % SEQFLAG, SEQstruct
               222, struct, 1);              % MATFLAG, MATstruct, flagIR

% Create dictionary of signal values (M x 2N)             
signalDictionarySparseMRF.test = [...
                    squeeze(dictionaryMRF(1:MSparse.test, :, 1))  , ...
                    squeeze(dictionaryMRF(1:MSparse.test, :, 2)) ]; 

% Normalize signals TRAIN
[signalsSparseNorm.train, signalsSparseNormCplx.train] = ...
    normalizeSignals(signalDictionarySparseMRF.train);

% Normalize signals TEST
[signalsSparseNorm.test, signalsSparseNormCplx.test] = ...
    normalizeSignals(signalDictionarySparseMRF.test);

% Look at the entire dictionary as an image
figure
subplot(1,2,1)
imagesc(sqrt(signalsSparseNorm.train(:,    1:1000).^2 + ...
             signalsSparseNorm.train(:, 1001:2000).^2));
xlabel('#timepoints'); ylabel('#(dictionary entries)');
title('Dictionary Sparse TRAIN');

subplot(1,2,2)
imagesc(sqrt(signalsSparseNorm.test(:,    1:1000).^2 + ...
             signalsSparseNorm.test(:, 1001:2000).^2));
xlabel('#timepoints'); ylabel('#(dictionary entries)');
title('Dictionary Sparse TEST');

% Look at one signal from the dictionary train vs test
figure,
plot(sqrt(signalsSparseNorm.train(1,    1:1000).^2 + ...
          signalsSparseNorm.train(1, 1001:2000).^2));
hold on
plot(sqrt(signalsSparseNorm.test(1,    1:1000).^2 + ...
          signalsSparseNorm.test(1, 1001:2000).^2));
legend(['T_1 = ', num2str(materialTuplesSparse.train(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuplesSparse.train(2,1)),' ms, ', ...
  '\Delta f = ', num2str(materialTuplesSparse.train(3,1).*1000),' Hz'], ...
       ['T_1 = ', num2str(materialTuplesSparse.test(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuplesSparse.test(2,1)),' ms, ', ...
  '\Delta f = ', num2str(materialTuplesSparse.test(3,1).*1000),' Hz']);
         
clear dictionaryMRF
toc

%%
% % % % % % % % % % % % % % % VISUALIZE MATERIAL TUPLES
figure

subplot(1,2,1)
scatter3(materialTuplesDense.train(1, :), ...
         materialTuplesDense.train(2, :), ...
         materialTuplesDense.train(3, :).*1e+03, '.b')
hold on
scatter3(materialTuplesDense.test(1, :), ...
         materialTuplesDense.test(2, :), ...
         materialTuplesDense.test(3, :).*1e+03, '.r')
view(-11,14)     
legend('train', 'test');
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('\Delta f [Hz]');
title('Tissue combinations space (dense)');
axis square

subplot(1,2,2)
scatter3(materialTuplesSparse.train(1, :), ...
         materialTuplesSparse.train(2, :), ...
         materialTuplesSparse.train(3, :).*1e+03, '.b')
hold on
scatter3(materialTuplesSparse.test(1, :), ...
         materialTuplesSparse.test(2, :), ...
         materialTuplesSparse.test(3, :).*1e+03, '.r')
view(-11,14)     
legend('train', 'test');
view(-11,14)
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('\Delta f [Hz]');
title('Tissue combinations space (sparse)');
axis square

%%
% % % % % % % % % % % % % % % LEARN MAP PART
tic
AmapDense = learnMap(materialTuplesDense.train, ... % Y ( 3xM)
                     signalsDenseNorm.train');      % X (2NxM)
toc

tic
AmapSparse = learnMap(materialTuplesSparse.train, ... % Y ( 3xM)
                      signalsSparseNorm.train');      % X (2NxM)
toc

%%
% % % % % % % % % % % % % % % PLOT MAP
figure

subplot(1,2,1)
scatter3(AmapDense(1,1:1000), ...
         AmapDense(2,1:1000), ...
         AmapDense(3,1:1000), '.b')
hold on
scatter3(AmapDense(1,1001:2000), ...
         AmapDense(2,1001:2000), ...
         AmapDense(3,1001:2000), '.r')
title('Map for dense')

subplot(1,2,2)
scatter3(AmapSparse(1,1:1000), ...
         AmapSparse(2,1:1000), ...
         AmapSparse(3,1:1000), '.b')
hold on
scatter3(AmapSparse(1,1001:2000), ...
         AmapSparse(2,1001:2000), ...
         AmapSparse(3,1001:2000), '.r')
title('Map for sparse')


%%
% % % % % % % % % % % % % % % PREDICT MATERIAL PROPERTIES
% % % % Apply map for sparse on DENSE test data
Y_test_dense      = materialTuplesDense.test;
Y_pred_norm_dense = AmapDense * signalsDenseNorm.test';

residuals_dense = Y_pred_norm_dense - Y_test_dense;
percError_dense = [ ...
              (residuals_dense(1, :))./(Y_test_dense(1, :)).*100  ; ...
              (residuals_dense(2, :))./(Y_test_dense(2, :)).*100] ;
SSE_dense       = sum(residuals_dense.^2, 2);   % sum of squared errors
% mean(Y_test, 2) - mean for each row (meanT_1, meanT_2, meandf)
SST_dense       = sum((Y_test_dense - repmat(mean(Y_test_dense, 2), ...
                                      [1, size(Y_test_dense, 2)])).^2, 2);
Rsq_dense       = 1 - (SSE_dense./SST_dense);

% % % % Apply map for dense on SPARSE test data
Y_test_sparse      = materialTuplesSparse.test;
Y_pred_norm_sparse = AmapSparse * signalsSparseNorm.test';

residuals_sparse = Y_pred_norm_sparse - Y_test_sparse;
percError_sparse = [ ...
              (residuals_sparse(1, :))./(Y_test_sparse(1, :)).*100  ; ...
              (residuals_sparse(2, :))./(Y_test_sparse(2, :)).*100] ;
SSE_sparse       = sum(residuals_sparse.^2, 2);   % sum of squared errors
% mean(Y_test, 2) - mean for each row (meanT_1, meanT_2, meandf)
SST_sparse       = sum((Y_test_sparse - repmat(mean(Y_test_sparse, 2), ...
                                      [1, size(Y_test_sparse, 2)])).^2, 2);
Rsq_sparse       = 1 - (SSE_sparse./SST_sparse);


%%
% % % % % % % % % % % % % % % PLOT RESIDUALS AND PERCENTAGE ERROR

% % % % % DENSE
figure

subplot(2,3,1)
scatter3(  Y_test_dense(1, :), ...   % T_1
           Y_test_dense(2, :), ...   % T_2
           percError_dense(1,:), ... % percentage error
           '.'); 
view(31, 10)
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('%err T_1');
title('%err in T_1')

% % % % T_2
subplot(2,3,2)
scatter3(  Y_test_dense(1, :), ...   % T_1
           Y_test_dense(2, :), ...   % T_2
           percError_dense(2,:), ... % percentage error
           '.'); 
view(31, 10)
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('%err T_2');
title('%err in T_2')



% % % % residuals in T_1
subplot(2,3,4)
scatter3(  Y_test_dense(1, :), ...   % T_1
           Y_test_dense(2, :), ...   % T_2
           residuals_dense(1,:), ... % residuals
           '.'); 
view(31, 10)
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('residuals T_1');
title('residuals in T_1')

% % % % residuals in T_2
subplot(2,3,5)
scatter3(  Y_test_dense(1, :), ...   % T_1
           Y_test_dense(2, :), ...   % T_2
           residuals_dense(2,:), ... % residuals
           '.'); 
view(31, 10)
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('residuals T_2');
title('residuals in T_2')

% % % % df
subplot(2,3,6)
scatter3(  Y_test_dense(1, :), ...   % T_1
           Y_test_dense(2, :), ...   % T_2
           residuals_dense(3,:), ... % residuals
           '.'); 
view(31, 10)
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('residuals \Delta f');
title('Residuals in \Delta f')

% % % % % % % % % % % % % % % % % % % % 
% % % % % SPARSE
figure

subplot(2,3,1)
scatter3(  Y_test_sparse(1, :), ...   % T_1
           Y_test_sparse(2, :), ...   % T_2
           percError_sparse(1, :), ... % percentage error
           '.'); 
view(31, 10)
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('%err T_1');
title('%err in T_1')

% % % % T_2
subplot(2,3,2)
scatter3(  Y_test_sparse(1, :), ...   % T_1
           Y_test_sparse(2, :), ...   % T_2
           percError_sparse(2,:), ... % percentage error
           '.'); 
view(31, 10)
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('%err T_2');
title('%err in T_2')



% % % % residuals in T_1
subplot(2,3,4)
scatter3(  Y_test_sparse(1, :), ...   % T_1
           Y_test_sparse(2, :), ...   % T_2
           residuals_sparse(1,:), ... % residuals
           '.'); 
view(31, 10)
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('residuals T_1');
title('residuals in T_1')

% % % % residuals in T_2
subplot(2,3,5)
scatter3(  Y_test_sparse(1, :), ...   % T_1
           Y_test_sparse(2, :), ...   % T_2
           residuals_sparse(2,:), ... % residuals
           '.'); 
view(31, 10)
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('residuals T_2');
title('residuals in T_2')

% % % % df
subplot(2,3,6)
scatter3(  Y_test_sparse(1, :), ...   % T_1
           Y_test_sparse(2, :), ...   % T_2
           residuals_sparse(3,:), ... % residuals
           '.'); 
view(31, 10)
xlabel('T_1 [ms]');ylabel('T_2 [ms]');zlabel('residuals \Delta f');
title('Residuals in \Delta f')


%%
% % % % % % % % % % % % % % % PLOT PREDICTED VS REAL

% % % % % % % DENSE
figure

% Plot Real vs Predicted T_1
subplot(1,3,1)
scatter(Y_test_dense(1, :), ...
        Y_pred_norm_dense(1, :), 'o')
hold on
plot(1:max(materialPropertiesDense.test.T1), ...
     1:max(materialPropertiesDense.test.T1))
grid on
axis equal
axis square
% axis([0 max(materialPropertiesDense.test.T1) ...
%       0 max(materialPropertiesDense.test.T1)])
  
xlabel('real T_1 [ms]');ylabel('predicted T_1 [ms]');
title({['$SSE_{T_1} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE_dense(1))], ...
       ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                     '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
    num2str(Rsq_dense(1))]}, 'interpreter', 'latex');

% Plot Real vs Predicted T_2
subplot(1,3,2)
scatter(Y_test_dense(2, :), ...
        Y_pred_norm_dense(2, :), 'o')
hold on
plot(1:max(materialPropertiesDense.test.T2), ...
     1:max(materialPropertiesDense.test.T2))
grid on
axis equal
axis square
% axis([0 max(materialPropertiesDense.test.T2) ...
%       0 max(materialPropertiesDense.test.T2)])

xlabel('real T_2 [ms]');ylabel('predicted T_2 [ms]');
title({['$SSE_{T_2} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE_dense(2))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq_dense(2))]}, 'interpreter', 'latex');

% Plot Real vs Predicted df
subplot(1,3,3)
scatter(Y_test_dense(3, :).*1e+03, ...
        Y_pred_norm_dense(3, :).*1e+03, 'o')
hold on
dfPlt = max(materialPropertiesDense.test.offRes)*1e+03;
plot(-dfPlt : dfPlt, ...
     -dfPlt : dfPlt);
grid on
axis equal
axis square
% axis([0 max(materialPropertiesDense.test.offRes).*1e+03 ...
%       0 max(materialPropertiesDense.test.offRes).*1e+03])

xlabel('real \Delta f [Hz]');ylabel('predicted \Delta f [Hz]');
title({['$SSE_{\Delta f} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE_dense(3))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq_dense(3))]}, 'interpreter', 'latex');


% % % % % % % SPARSE
figure

% Plot Real vs Predicted T_1
subplot(1,3,1)
scatter(Y_test_sparse(1, :), ...
        Y_pred_norm_sparse(1, :), 'o')
hold on
plot(1:max(materialPropertiesSparse.test.T1), ...
     1:max(materialPropertiesSparse.test.T1))
grid on
axis equal
axis square
% axis([0 max(materialPropertiesSparse.test.T1) ...
%       0 max(materialPropertiesSparse.test.T1)])

xlabel('real T_1 [ms]');ylabel('predicted T_1 [ms]');
title({['$SSE_{T_1} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE_sparse(1))], ...
       ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                     '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
    num2str(Rsq_sparse(1))]}, 'interpreter', 'latex');

% Plot Real vs Predicted T_2
subplot(1,3,2)
scatter(Y_test_sparse(2, :), ...
        Y_pred_norm_sparse(2, :), 'o')
hold on
plot(1:max(materialPropertiesSparse.test.T2), ...
     1:max(materialPropertiesSparse.test.T2))
grid on
axis equal
axis square
% axis([0 max(materialPropertiesSparse.test.T2) ...
%       0 max(materialPropertiesSparse.test.T2)])

xlabel('real T_2 [ms]');ylabel('predicted T_2 [ms]');
title({['$SSE_{T_2} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE_sparse(2))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq_sparse(2))]}, 'interpreter', 'latex');

% Plot Real vs Predicted df
subplot(1,3,3)
scatter(Y_test_sparse(3, :).*1e+03, ...
        Y_pred_norm_sparse(3, :).*1e+03, 'o')
hold on
dfPlt = max(materialPropertiesSparse.test.offRes)*1e+03;
plot(-dfPlt : dfPlt, ...
     -dfPlt : dfPlt);
grid on
axis equal
axis square
% axis([0 max(materialPropertiesSparse.test.offRes).*1e+03 ...
%       0 max(materialPropertiesSparse.test.offRes).*1e+03])
xlabel('real \Delta f [Hz]');ylabel('predicted \Delta f [Hz]');
title({['$SSE_{\Delta f} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE_sparse(3))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq_sparse(3))]}, 'interpreter', 'latex');
    
    
%% Plot 3D errors with slice - DENSE TEST DATA
% % % % % % % % % % % DENSE
[x,y] = meshgrid(materialPropertiesDense.test.T1, ...
                 materialPropertiesDense.test.T2);
[mT2, mT1] = size(x); % m - T2, n - T1
mdf = length(materialPropertiesDense.test.offRes);

N_T1     = size(materialPropertiesDense.test.T1,     2);
N_T2     = size(materialPropertiesDense.test.T2,     2);
N_offRes = size(materialPropertiesDense.test.offRes, 2);

materials = {'T_1', 'T_2', '\Delta f'};

figure
residualsVolume = zeros(mT2, mT1, mdf) - 1000;
lala = zeros(mT1, mT2);
% For each off resonance frequency
for idxdf = 1:mdf
    
    transp = 1 - 0.7*idxdf/mdf;
    
    % For each material property 
    for idMatProp = 1:3
        
        % Reziduals at current df for current material property
        if idMatProp < 3
            resz = percError_dense(idMatProp, idxdf:mdf:MDense.test);
        else
            resz = residuals_dense(idMatProp, idxdf:mdf:MDense.test);
        end
    
        % Put them in a 2D matrix
        idx = 1;
        for iT2 = 1:N_T2                       % T_1 (mT1)
            for iT1 = 1:N_T1                   % T_2 (mT2)
                if x(iT2,iT1) >= y(iT2,iT1)
                    residualsVolume(iT2,iT1,idxdf) = resz(idx);
                    lala(iT1,iT2) = 1;
                    idx = idx + 1;
                else
                    residualsVolume(iT2,iT1,idxdf) = NaN;
                end
            end
        end
        

        % Plot Slice
        subplot(1, 3, idMatProp);
        view([75 15 10]);
        view( 18, 11);      % az = 25, el = 7
        hz = slice(residualsVolume, ...
                   [], [], idxdf);
        alpha(hz, transp);
        colormap jet ; colorbar
        set(findobj(gca,'Type','Surface'),'EdgeColor','none')
        hold on
        if idMatProp < 3
            title(['Percentage error for ', materials{idMatProp}]);
        else
            title(['Residuals for ', materials{idMatProp}]);
        end
        xlabel('T_1 [ms]'); ylabel('T_2 [ms]'); zlabel('\Delta f [Hz]');
        
        % Values on the X-axis
        dT1 = ceil(N_T1/5);
        set(gca,'XTick', 1 : dT1 : N_T1); 
        set(gca,'XTickLabels', ...
         strread( num2str( ...
                  materialPropertiesDense.test.T1(1:dT1:end)), '%s'));
        % Values on the Y-axis
        dT2 = ceil(N_T2/5);
        set(gca,'YTick', 1 : dT2 : N_T2); 
        set(gca,'YTickLabels', ...
         strread( num2str( ...
                  materialPropertiesDense.test.T2(1:dT2:end)), '%s'));
        % Values on the Z-axis
        doffRes = ceil(N_offRes/6);
        set(gca,'ZTick', 1 : doffRes : N_offRes);
        set(gca,'ZTickLabels', ...
         strread(...
           num2str( ...
            materialPropertiesDense.test.offRes(1:doffRes:end).*1000), ...
           '%s'));
    end
end


% % % % % % % % % % %
% % % % % % % % % % % SPARSE
% % % % % % % % % % % 
[x,y] = meshgrid(materialPropertiesSparse.test.T1, ...
                 materialPropertiesSparse.test.T2);
[mT2, mT1] = size(x); % m - T2, n - T1
mdf = length(materialPropertiesSparse.test.offRes);

N_T1     = size(materialPropertiesSparse.test.T1,     2);
N_T2     = size(materialPropertiesSparse.test.T2,     2);
N_offRes = size(materialPropertiesSparse.test.offRes, 2);

materials = {'T_1', 'T_2', '\Delta f'};

figure
residualsVolume = zeros(mT2, mT1, mdf) - 1000;
lala = zeros(mT1, mT2);
% For each off resonance frequency
for idxdf = 1:mdf
    
    transp = 1 - 0.7*idxdf/mdf;
    
    % For each material property 
    for idMatProp = 1:3
        
        % Reziduals at current df for current material property
        if idMatProp < 3
            resz = percError_sparse(idMatProp, idxdf:mdf:MSparse.test);
        else
            resz = residuals_sparse(idMatProp, idxdf:mdf:MSparse.test);
        end
    
        % Put them in a 2D matrix
        idx = 1;
        for iT2 = 1:N_T2                       % T_1 (mT1)
            for iT1 = 1:N_T1                   % T_2 (mT2)
                if x(iT2,iT1) >= y(iT2,iT1)
                    residualsVolume(iT2,iT1,idxdf) = resz(idx);
                    lala(iT1,iT2) = 1;
                    idx = idx + 1;
                else
                    residualsVolume(iT2,iT1,idxdf) = NaN;
                end
            end
        end
        

        % Plot Slice
        subplot(1, 3, idMatProp);
        view([75 15 10]);
        view( 18, 11);      % az = 25, el = 7
        hz = slice(residualsVolume, ...
                   [], [], idxdf);
        alpha(hz, transp);
        colormap jet ; colorbar
        set(findobj(gca,'Type','Surface'),'EdgeColor','none')
        hold on
        if idMatProp < 3
            title(['Percentage error for ', materials{idMatProp}]);
        else
            title(['Residuals for ', materials{idMatProp}]);
        end
        xlabel('T_1 [ms]'); ylabel('T_2 [ms]'); zlabel('\Delta f [Hz]');
        
        % Values on the X-axis
        dT1 = ceil(N_T1/5);
        set(gca,'XTick', 1 : dT1 : N_T1); 
        set(gca,'XTickLabels', ...
         strread( num2str( ...
                  materialPropertiesSparse.test.T1(1:dT1:end)), '%s'));
        % Values on the Y-axis
        dT2 = ceil(N_T2/5);
        set(gca,'YTick', 1 : dT2 : N_T2); 
        set(gca,'YTickLabels', ...
         strread( num2str( ...
                  materialPropertiesSparse.test.T2(1:dT2:end)), '%s'));
        % Values on the Z-axis
        doffRes = ceil(N_offRes/6);
        set(gca,'ZTick', 1 : doffRes : N_offRes);
        set(gca,'ZTickLabels', ...
         strread(...
           num2str( ...
            materialPropertiesSparse.test.offRes(1:doffRes:end).*1000), ...
           '%s'));
    end
end
    
   
%% Generate Dictionary for entire dataset as Nature Paper

% Choose material and sequence type
% MATFLAG = 1; % The entire range as per MRF paper
MATFLAG = 3; % Custom Material
customMaterial.T1     =  100:20:1000 ;           % 100:20:2000
customMaterial.T2     =  20:5:400;    % [ 20:5:300 350:50:1000]    
customMaterial.offRes = -0.05:0.01:0.05; 
SEQFLAG = 3; % sinusoidal FA, alternate PhA, perlin TRs, RO = TR./2

% Plot flag
FLAGPLOT = 0;

% RUN SIMULATION for a given of N
tic
N = 1000;
% Run simulation
[materialPropertiesDense, sequencePropertiesDense, ...
    materialTuplesDense, MDense, ...
    dictionaryMRF] = ...
                 simulateMRF(N, FLAGPLOT, ...
                 SEQFLAG, struct, ...
                 MATFLAG, customMaterial, 1);
             
N_T1     = length(materialPropertiesDense.T1);
N_T2     = length(materialPropertiesDense.T2);
N_offRes = length(materialPropertiesDense.offRes);

% Create dictionary of signal values (M x 2N)             
signalDictionaryMRF = [ squeeze(dictionaryMRF(1:MDense, :, 1))  , ...
                        squeeze(dictionaryMRF(1:MDense, :, 2)) ]; 
clear dictionaryMRF
toc


% % % % % % % % % NOISE FREE DATA % % % % % % % % % 
%% 
% % % % % TEST 2
% % Training Data 
% % includes small values of T1 (T1<=2000) and T2(T2<=200), 
% % excludes bigger ones

% % Find small values of T1/T2 and the reciprocal set
colsCondT1 = find(materialTuplesDense(1,:) <= 2000);    % T1 condition <= 2000
colsCondT2 = find(materialTuplesDense(2,:) <=  200);    % T2 condition <=  200
colsCondTrain = intersect(colsCondT1, colsCondT2);
colsCondTest  = setdiff(1:MDense, colsCondTrain);

materialTuplesTrain = materialTuplesDense(:, colsCondTrain);
materialTuplesTest  = materialTuplesDense(:, colsCondTest);

signalDictionaryMRFTrain = signalDictionaryMRF(colsCondTrain, :);
signalDictionaryMRFTest  = signalDictionaryMRF(colsCondTest,  :);

figure
scatter3(materialTuplesDense(1,:), ...
         materialTuplesDense(2,:), ...
         materialTuplesDense(3,:), '.b'), hold on

% scatter3(materialTuples(1,colsCondT1), ...
%          materialTuples(2,colsCondT1), ...
%          materialTuples(3,colsCondT1), '.g'), hold on
%      
% scatter3(materialTuples(1,colsCondT2), ...
%          materialTuples(2,colsCondT2), ...
%          materialTuples(3,colsCondT2), '.r'), hold on

scatter3(materialTuplesTrain(1,:), ...
         materialTuplesTrain(2,:), ...
         materialTuplesTrain(3,:), 'ok'), hold on

scatter3(materialTuplesTest(1,:), ...
         materialTuplesTest(2,:), ...
         materialTuplesTest(3,:), 'or'), hold on


%%
Xsig = signalDictionaryMRF(1:MTrain, :)'; % (2N x M)
[~ , ~, Xsignorm] = matching(Xsig', Xsig(:,1)', materialTuplesDense);

Xsignorm = Xsignorm';

tic
Bmapnorm = learnMap( Ymat, ...  % Y = ( 3 x M)
                 Xsignorm);     % X = (2N x M) 
toc
     

%%
Ymat_pred_norm = Bmapnorm * (Xsignorm);

figure
scatter3(Ymat(1, :), ...
         Ymat(2, :), ...
         ((Ymat_pred_norm(1, :) - Ymat(1, :)) ./ Ymat(1,:)) .* 100, '.'); 
hold on
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1');ylabel('T_2');zlabel('%err T_1');
title('%err in T_1')

% Ymat_pred = Bmap * (Xsig);
% figure
% scatter3(Ymat(1, :), ...
%          Ymat(2, :), ...
%          ((Ymat_pred(1, :) - Ymat(1, :)) ./ Ymat(1,:)) .* 100, '.'); 
% hold on
% % Y_predicted(1,:)-Y_test(1,:),'.'); 
% xlabel('T_1');ylabel('T_2');zlabel('%err T_1');
% title('%err in T_1')

     
%% 
% % % % % TEST 1
% Training Data = 100% of Data Set 
% Testing  Data = 100% of Data Set
% Compare with Pattern Matching algorithm of Paper

% Learn Mapping from entire data set
MTrain = MDense;     % ceil(M*10/100); 
Ymat = materialTuplesDense(:, 1:MTrain);       % ( 3 x M)
Xsig = signalDictionaryMRF(1:MTrain, :)'; % (2N x M)

tic
Bmap = learnMap( Ymat, ...  % Y = ( 3 x M)
                 Xsig);     % X = (2N x M) 
toc

tic
AmapDense = Xsig * pinv(Ymat);
toc

%%
Xsig_pred = (AmapDense * Ymat);
% figure
% scatter(Xsig(1,:), Xsig_pred(1,:))
Ymat_pred = Bmap * (Xsig_pred);

figure
scatter3(Ymat(1, :), ...
         Ymat(2, :), ...
         ((Ymat_pred(1, :) - Ymat(1, :)) ./ Ymat(1,:)) .* 100, '.'); 
hold on
% Y_predicted(1,:)-Y_test(1,:),'.'); 
xlabel('T_1');ylabel('T_2');zlabel('%err T_1');
title('%err in T_1')

%% A(B(sig)) = sigpred
Ymat_pred2 = Bmap * Xsig;
Xsig_pred2 = AmapDense * (Ymat_pred2);

% 
figure
plot(1:1000, Xsig_pred2(1:1000,1))
hold on
plot(1:1000, Xsig(1:1000,1))

% 
figure 
plot(AmapDense(1:1000,1).^2 + AmapDense(1001:2000,1).^2)
hold on
plot(AmapDense(1:1000,2).^2 + AmapDense(1001:2000,2).^2)
plot(AmapDense(1:1000,3).^2 + AmapDense(1001:2000,3).^2)

%%
figure,
scatter(Ymat(1,:), Ymat_pred(1,:))
hold on
plot(0:2500,0:2500)

%%
% % % % COMMENTS
% % For N =  500
%  10% M takes   21 seconds N =  500
%  50% M takes  115 seconds N =  500
% 100% M takes  256 seconds N =  500

% % For N = 1000
%  10% M takes   70 seconds N = 1000 (pinv)
%  10% M takes   80 seconds N = 1000 (  \ )
%  15% M takes   93 seconds N = 1000 
%  25% M takes  159 seconds N = 1000
%  35% M takes  220 seconds N = 1000
%  45% M takes  299 seconds N = 1000
%  50% M takes  368 seconds N = 1000
%  55% M takes  425 seconds N = 1000
%  65% M takes  596 seconds N = 1000
% 100% M takes 1599 seconds N = 1000

% figure
% plot([ 10, 15, 25, 35, 45, 50, 55, 65], ...
%      [ 70, 93,159,220,299,368,425,596]./60, 'o');
% xlabel('% of M')
% ylabel('[minutes]')
% grid on

%%


end








