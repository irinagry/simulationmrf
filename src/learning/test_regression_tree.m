% % Irina Grigorescu
% % See how the regression tree class of matlab performs

% Regression Trees seem to be slightly better at noise tolerance than
% simple linear regression, although they don't perform well at realistic
% noise levels. I think it needs some fiddling because I think it can get
% better than this. Maybe PCA or maybe manifold learning.
% Anyway, the first thing to do is to look at how the algorithm can be
% twitched.
% Also, you could try increasing the size of the dictionary by using a few
% randomly generated sequence properties

% Data is loaded from:
varLoaded = load('../runScripts/epgdictionary-FISP-MRFFISPpaper-bigger.mat');

%% DO SVD on dictionary
F0states = varLoaded.F0states;
[M,N] = size(F0states);
tic
[Uxt, Sxt, Vxt] = svd(F0states(:,1:N/2), 'econ'); %kMax);
totalEnergy = sum(diag(Sxt));
t = toc;
fprintf('SVD on dictionary took --- %f s\n\n', t);

%%
for k = 120 %:-1:38
    % Calculate approximation
    Xtapp = Uxt(:, 1:k) * Sxt(1:k, 1:k) * Vxt(:, 1:k)';
    % Calculate retained energy
    energK = (1/totalEnergy) * sum(diag(Sxt(1:k,1:k)));
    % Print
    fprintf('For k = %d --- energy = %f\n\n', k, energK);
end

%%
% Create datasets
dictionary_X      =     abs(Xtapp); %abs(varLoaded.F0states);
material_tuples_y =     varLoaded.materialTuples.';

% Reduce dataset to only T1 values <= 3000 and T2 <= 300
indicesT1Small = find(material_tuples_y(:,2) <=  300);
indicesT1Big   = find(material_tuples_y(:,1) <= 3000);
indices = intersect(indicesT1Small, indicesT1Big);

material_tuples_y = material_tuples_y(indices,:);
dictionary_X      = dictionary_X(indices, :);

[M, N] = size(dictionary_X);
%%
% Create Train datasets
dictionary_X_train = dictionary_X(setdiff(1:M,50:50:M),:);
dictionary_X_test  = dictionary_X(50:50:M,:);

% Create Test datasets
material_tuples_y_train = material_tuples_y(setdiff(1:M,50:50:M),:);
material_tuples_y_test  = material_tuples_y(50:50:M,:);

% Noise:
[Mtest, ~] = size(dictionary_X_test);
noiseModel = zeros(Mtest,N)*1e-05;

% % % % T1
% Fit Linear Model for T1
linModelT1 = fitlm(dictionary_X_train, material_tuples_y_train(:, 1));
% Predict T1 - linear model
T1_lm_pred = predict(linModelT1, dictionary_X_test + noiseModel);

% Fit Tree regressor for T1
treeT1 = fitrtree(dictionary_X_train, material_tuples_y_train(:, 1), ...
                  'QuadraticErrorTolerance', 1e-8);
% Predict T1
T1_tree_pred = predict(treeT1, dictionary_X_test + noiseModel);

% MSE
mse_tree_T1 = 1/length(T1_tree_pred) .* ...
           sum((T1_tree_pred - material_tuples_y_test(:,1)).^2);
mse_lm_T1 = 1/length(T1_lm_pred) .* ...
           sum((T1_lm_pred - material_tuples_y_test(:,1)).^2);
           
% Print time it passed
t = toc;
fprintf('Fitting and predicting T_1 --- took %f s\n', t);
       
tic
% % % % T2
% Fit Linear Model for T1
linModelT2 = fitlm(dictionary_X_train, material_tuples_y_train(:, 2));
% Predict T2 - linear model
T2_lm_pred = predict(linModelT2, dictionary_X_test + noiseModel);

% Fit Tree regressor for T2
treeT2 = fitrtree(dictionary_X_train, material_tuples_y_train(:,2), ...
                  'QuadraticErrorTolerance', 1e-8);
% Predict T2
T2_tree_pred = predict(treeT2, dictionary_X_test + noiseModel);

% MSE
mse_tree_T2 = 1/length(T2_tree_pred) .* ...
           sum((T2_tree_pred - material_tuples_y_test(:,2)).^2);
mse_lm_T2 = 1/length(T1_lm_pred) .* ...
           sum((T1_lm_pred - material_tuples_y_test(:,1)).^2);

% Print time it passed
t = toc;
fprintf('Fitting and predicting T_2 --- took %f s\n', t);
fprintf('\nTREE: MSE_{T_1} = %f, MSE_{T_1} = %f\n\n', ...
         mse_tree_T1, mse_tree_T2);
fprintf('\nLM  : MSE_{T_1} = %f, MSE_{T_1} = %f\n\n', ...
         mse_lm_T1, mse_lm_T2);


%%
% Plot predicted vs actual
figure('Position',[10,10,1400,800])

subplot(3,4,1)
limMaxT1 = 3000;
plot([0, limMaxT1], [0,limMaxT1]), hold on
scatter(material_tuples_y_test(:,1), T1_tree_pred, '.')
xlabel('T_{1real}');
ylabel('T_{1pred}');
grid on
axis square;
xlim([0,limMaxT1]); ylim([0,limMaxT1]);
title('Regression Tree')


subplot(3,4,2)
limMaxT2 = 300;
plot([0, limMaxT2], [0,limMaxT2]), hold on
scatter(material_tuples_y_test(:,2), T2_tree_pred, '.')
xlabel('T_{2real}');
ylabel('T_{2pred}');
grid on
axis square;
xlim([0,limMaxT2]); ylim([0,limMaxT2]);
title('Regression Tree')


subplot(3,4,[5,6])
scatter(material_tuples_y_test(:,1), ...
        material_tuples_y_test(:,1) - T1_tree_pred, '.')
xlabel('T_{1real}');
ylabel('T_{1real} - T_{1pred}');
grid on


subplot(3,4,[9,10])
scatter(material_tuples_y_test(:,2), ...
        material_tuples_y_test(:,2) - T2_tree_pred, '.')
xlabel('T_{2real}');
ylabel('T_{2real} - T_{2pred}');
grid on

% % % % LINEAR MODEL
subplot(3,4,3)
limMaxT1 = 3000;
plot([0, limMaxT1], [0,limMaxT1]), hold on
scatter(material_tuples_y_test(:,1), T1_lm_pred, '.')
xlabel('T_{1real}');
ylabel('T_{1pred}');
grid on
axis square;
xlim([0,limMaxT1]); ylim([0,limMaxT1]);
title('Linear Model')


subplot(3,4,4)
limMaxT2 = 300;
plot([0, limMaxT2], [0,limMaxT2]), hold on
scatter(material_tuples_y_test(:,2), T2_lm_pred, '.')
xlabel('T_{2real}');
ylabel('T_{2pred}');
grid on
axis square;
xlim([0,limMaxT2]); ylim([0,limMaxT2]);
title('Linear Model')

subplot(3,4,[7,8])
scatter(material_tuples_y_test(:,1), ...
        material_tuples_y_test(:,1) - T1_lm_pred, '.')
xlabel('T_{1real}');
ylabel('T_{1real} - T_{1pred}');
grid on


subplot(3,4,[11,12])
scatter(material_tuples_y_test(:,2), ...
        material_tuples_y_test(:,2) - T2_lm_pred, '.')
xlabel('T_{2real}');
ylabel('T_{2real} - T_{2pred}');
grid on



% % %%
% % figure
% % subplot(1,2,1)
% % plot(splitsT1, mseT1, 'k-', 'LineWidth', 0.5), hold on
% % scatter(splitsT1, mseT1, 'bo', 'LineWidth', 2)
% % grid on
% % xlabel('regression tree splits');
% % ylabel('MSE_{T_1}');
% % 
% % subplot(1,2,2)
% % plot(splitsT1, mseT2, 'k-', 'LineWidth', 0.5), hold on
% % scatter(splitsT1, mseT2, 'bo', 'LineWidth', 2)
% % grid on
% % xlabel('regression tree splits');
% % ylabel('MSE_{T_2}');