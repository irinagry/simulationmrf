function A = learnMapAndTest(signalDictionaryMRF, materialTuples)
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 19-04-2017
% % % % Date updated: 21-04-2017
% % % % 
% % % % This script will perform linear regression on some subset of the
% % % % dictionary and try to predict the other values
% % % % 
% % % % The equation is: Y = A H
% % % %  where: Y in R^(3xM), Y = [  y1  | ... |   yM ],   yi  in R^(3x1)
% % % %    and: H in R^(NxM), H = [h(y1) | ... | h(yM)], h(yi) in R^(Nx1)
% % % % We are solving for A => A = Y H^T (H H^T)^-1
% % % % 
% % % % 
% % % % INPUT:
% % % %     signalDictionaryMRF = (M x N) array
% % % %     materialTuples      = (3 x M) array 
% % % % 
% % % % OUTPUT: 
% % % %     A
% % % %     

%% Create datasets (training + test)
[M, N] = size(signalDictionaryMRF);
Mtraining = floor(M * 0.95);  % number of values in training dataset
Mtest     = M - Mtraining;    % number of values in test     dataset

% Create permuted set of indices
permidx = randperm(M); 

% Take the entire data as training data
H_training = signalDictionaryMRF(:, :)'; % H ? R^(NxM)
Y_training = materialTuples(:, :);       % Y ? R^(3xM)

% Take 90% of data as training data
% H_training = signalDictionaryMRF(permidx(1:Mtraining), :)'; % H ? R^(NxM)
% Y_training = materialTuples(:, permidx(1:Mtraining));       % Y ? R^(3xM)

% Take 10% of data as testing data
H_test = signalDictionaryMRF(permidx(Mtraining+1:end), :)'; % H ? R^(NxM)
Y_test = materialTuples(:, permidx(Mtraining+1:end));       % Y ? R^(3xM)

%%  LINEAR REGRESSION      Y = A H               =>  
% %                        A = Y H^T (H H^T)^-1

% % % % LEARNING
%     Y   (3xM) = A(3xN) *   H   (NxM)
% material(3xM) = A(3xN) * signal(NxM) 
%               =>
%     A         = Y H^T (H H^T)^-1
% H_training = [ones(1,Mtraining) ; H_training];
A = (Y_training * H_training');
K = pinv(H_training * H_training');
A = A * K;


% % % % TESTING
Y_predicted = A * H_test; %[ones(1, Mtest) ; H_test];
residuals   = Y_test - Y_predicted;
SSE         = sum(residuals.^2, 2);
SST         = sum((Y_test - repmat(mean(Y_test,2), [1, Mtest])).^2, 2);
Rsq         = 1 - (SSE./SST);

figure
imagesc(A), colormap jet, colorbar
title('A');

% % % % TEST
resultsT1 = [Y_test(1,:)' , Y_predicted(1,:)'];
resultsT2 = [Y_test(2,:)' , Y_predicted(2,:)'];
resultsdf = [Y_test(3,:)' , Y_predicted(3,:)'];
% disp('Real [T_1, T_2, \Delta f], Predicted [T_1, T_2, \Delta f]')
% for i = 1:10
%     fprintf(['Real      [%f, %f, %f]\n', ...
%              'Predicted [%f, %f, %f]\n\n'], ...
%         results(i,1), results(i,2), results(i,3), ...
%         results(i,4), results(i,5), results(i,6));
% end


% % % % Plot
Mshow = Mtest; %250; %50; % Mtest

figure

% T_1
subplot(2,3,1)
resultsT1ToShow = sortrows(resultsT1(1:Mshow,:),1);
plot(1:Mshow, resultsT1ToShow(:,1), 'o', 'MarkerSize', 2);
hold on
plot(1:Mshow, resultsT1ToShow(:,2), '.');
xlabel('test points');
ylabel('T_1 (ms)');
grid on
legend('real', 'predicted', 'Location', 'southeast');
title('$T_1$ real and $T_1$ predicted', 'interpreter', 'latex');
% cc1 = corrcoef(resultsT1ToShow(:,1), resultsT1ToShow(:,2));
% cc1(1,2)


% T_2
subplot(2,3,2)
resultsT2ToShow = sortrows(resultsT2(1:Mshow,:),1);
plot(1:Mshow, resultsT2ToShow(:,1), 'o', 'MarkerSize', 2);
hold on
plot(1:Mshow, resultsT2ToShow(:,2), '.');
xlabel('test points');
ylabel('T_2 (ms)');
grid on
legend('real', 'predicted', 'Location', 'southeast');
title('$T_2$ real and $T_2$ predicted', 'interpreter', 'latex');
cc2 = corrcoef(resultsT2ToShow(:,1), resultsT2ToShow(:,2));
cc2(1,2)

% df
subplot(2,3,3)
resultsdfToShow = sortrows(resultsdf(1:Mshow,:),1);
plot(1:Mshow, resultsdfToShow(:,1).*1e+03, 'o', 'MarkerSize', 2);
hold on
plot(1:Mshow, resultsdfToShow(:,2).*1e+03, '.');
xlabel('test points');
title('$\Delta f$ real and $\Delta f$ predicted', 'interpreter', 'latex');
grid on
legend('real', 'predicted', 'Location', 'southeast');
title('$\Delta f$ real and $\Delta f$ predicted', 'interpreter', 'latex');
cc3 = corrcoef(resultsdfToShow(:,1), resultsdfToShow(:,2));
cc3(1,2)

% Residuals T_1
subplot(2,3,4)
scatter(1:Mshow, residuals(1, 1:Mshow), '.');
xlabel('test points');
ylabel('Residuals T_1 (ms)');
title({['$SSE_{T_1} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE(1))], ...
       ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                     '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
    num2str(Rsq(1))]}, 'interpreter', 'latex');
% set(gca,'XTick',0:4);
% set(gca,'XTickLabels', {'','T_1','T_2','\Delta f',''});
% xlim([0 4])
grid on

% Residuals T_2
subplot(2,3,5)
scatter(1:Mshow, residuals(2, 1:Mshow), '.');
xlabel('test points');
ylabel('Residuals T_2 (ms)');
title({['$SSE_{T_2} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE(2))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq(2))]}, 'interpreter', 'latex');
% set(gca,'XTick',0:4);
% set(gca,'XTickLabels', {'','T_1','T_2','\Delta f',''});
% xlim([0 4])
grid on

% Residuals df
subplot(2,3,6)
scatter(1:Mshow, residuals(3, 1:Mshow), '.');
xlabel('test points');
ylabel('Residuals \Delta f (kHz)');
title({['$SSE_{\Delta f} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
        num2str(SSE(3))], ...
        ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                        '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq(3))]}, 'interpreter', 'latex');
grid on

disp('Finish Regression')




































% %% Linear Regression with fminunc
% disp('fminunc');
% 
% % % % % Learn
% theta2 = theta; %zeros(N+1, 3);
% start_theta = theta;
% best_cost  = Inf;
% alpha = 1;
% 
% % for iter = 1:100
% %   [cost, grad] = costFunction(theta2, X, y);
% % 
% %   if (cost < best_cost)
% %     theta2 = theta2 - alpha * grad;
% %     best_cost = cost;
% %   else
% %     alpha = alpha * 0.99;
% %   end
% % end
% 
% h = optimset('MaxIter', 2, ...
%              'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'iter');
% 
% [theta2, functionVal, exitFlag] = fminunc(@(t) costFunction(t, X, y), ...
%                                   start_theta, h);
% 
% % % % % TESTING
% xNew2 = [ones(Mtest, 1) signalDictionaryMRFtest]; 
% yNew2 = xNew2 * theta2;
% residuals2 = (1/Mtraining) .* sum((materialTuplestest - yNew2).^2);
% 
% % % % % TEST
% results2 = [materialTuplestest , yNew2];
% disp('Real [T_1, T_2, \Delta f], Predicted [T_1, T_2, \Delta f]')
% for i = 1:Mtest
%     fprintf(['Real      [%f, %f, %f]\n', ...
%              'Predicted [%f, %f, %f]\n\n'], ...
%         results2(i,1), results2(i,2), results2(i,3), ...
%         results2(i,4), results2(i,5), results2(i,6));
% end
% 
% 













