function A = learnMap(Y, X)
% % % % IRINA GRIGORESCU
% % % % 
% % % % Date created: 19-04-2017
% % % % Date updated: 21-04-2017
% % % % 
% % % % This script will solve the Y = AX equation for A.
% % % % 
% % % % The equation is: Y = A X
% % % %  where: Y in R^(3xM), Y = [  y1  | ... |   yM ],   yi  in R^(3x1)
% % % %    and: X in R^(NxM), H = [h(y1) | ... | h(yM)], h(yi) in R^(Nx1)
% % % % We are solving for A => A = Y X^+
% % % % 
% % % % 
% % % % INPUT:
% % % %     Y = (3 x M) array (material tuples)
% % % %     X = (N x M) array (N-vector signals)
% % % % 
% % % % OUTPUT: 
% % % %     A = (3 x N) array A = Y X^+
% % % %          where X^+ = Moore-Penrose Inverse
% % % %     

fprintf('X is a 2NxM (%d x %d) matrix and rank(X*X^T) = %d\n', ...
         size(X,1), size(X,2), rank(X*X'));
   

A = Y * pinv(X);  %(X' * pinv(X*X'));

condA = norm(A)*norm(pinv(A));
fprintf('cond(A) = %f\n', condA);  

% [~, S, ~] = svd(A);
% fprintf('A is a 3 x %d matrix with cond(A) = %f = %f\n', ...
%          size(A,2), ...
%          max(S(1,1), max(S(2,2),S(3,3))) ./ ...
%          min(S(1,1), min(S(2,2),S(3,3))), ...
%          condA);

% A = (X' \ Y')';

% A = X \ Y;   % gets X and Y in a different shape
% A = At';

disp('Done with Learn Map');

end