% % % % Irina Grigorescu
% % % % 
% % % % In this script I look at the FISP dictionary

% Preprocessing steps
clear all;
close all;
clc;

% Add needed paths:
run('runForPaths');

% Filename and path to dictionary
filenameDict = '../runScripts/epgdictionary-FISP-MRFFISPpaper-bigger.mat';
varToLoad = load(filenameDict);

%% GET DICTIONARY
% F0states - Discard first value which is after IR pulse
F0states = varToLoad.F0states(:, 2:end); 
% Material tuples
materialTuples = varToLoad.materialTuples;
M = size(materialTuples, 2);
% Sequence 
N = length(varToLoad.TRsEPG) - 1;
% Create 2 channel data (real + imag)
F0statesReIm = [ real(F0states), imag(F0states)];

% Normalize signals
[dictionaryMRFFISPNorm2Ch, dictionaryMRFFISPNorm] = ...
        normalizeSignals(F0statesReIm);

% Calculate absolute signal
sigMRFFISPAbsNorm = abs(dictionaryMRFFISPNorm(:, 1:end));

%% SVD
Xtrain = dictionaryMRFFISPNorm.';
tic
[Uxt, Sxt, Vxt] = svd(Xtrain, 'econ'); %kMax);
toc

totalEnergy = sum(diag(Sxt));

%% PLOT SINGULAR VALUES
maxVal = 50;
figure
plot(1:maxVal, diag(Sxt(1:maxVal,1:maxVal)), 'ko-', 'LineWidth', 2)
xlabel('i')
ylabel('\lambda_i')

%%
kMax            =    30;
nMin            =     1;
nMax            =  1039;

figure('units','normalized','outerposition', [0 0 1 1]);   

signalNumber = 1;
% Plot Signal Real signal
subplot(1,2,1)
plot(nMin:nMax, ...
     abs(Xtrain(nMin:nMax,  signalNumber)), 'k'), hold on
xlim([nMin, nMax])
ylim([0, 0.05])
xlabel('T_R index')
ylabel('M_x')
grid on
title('Real Signal')

pause

for k = kMax %:-1:1

    % Calculate approximation
    Xtapp = Uxt(:, 1:k) * Sxt(1:k, 1:k) * Vxt(:, 1:k)';
    
    % Calculate energy retained
    energK(k) = (1/totalEnergy) * sum(diag(Sxt(1:k,1:k)));

    % % % Plot Signal
    subplot(1,2,1)
    % Real signal
    p1 = plot(nMin:nMax, ...
     abs(Xtrain(nMin:nMax,  signalNumber)), 'k', ...
     'LineWidth', 2); 
    hold on
    % Approximated Signal
    p2 = plot(nMin:nMax, abs(Xtapp(nMin:nMax, signalNumber)), ...
              'LineWidth', 2);
    grid on
    xlim([nMin, nMax])
    ylim([0, 0.05])
    % Title
    title({ 'Approximated Signal', ...
           ['k = ', num2str(k), ' singular values used'], ...
           ['Energy retained = ', num2str(energK(k))]})
    
    % % % Plot energy for k retained singular values
    subplot(1,2,2)
    scatter(k, energK(k), '.b')
	xlabel('k')
    ylabel('e(k)')
    title('Energy ratio')
    hold on
	grid on
    xlim([0, kMax])
    ylim([0, 1])
    
    pause(0.1)
    delete(p1), delete(p2)
end


%% Learn map
tic
indicesT1Small = find(materialTuples(1,:) <=  200);
indicesT1Big   = find(materialTuples(1,:) <= 3000);
indices = union(indicesT1Small, indicesT1Big(1:2:end));
Amap = learnMap(materialTuples(:, indices), ...           % Y ( 2xM)
                dictionaryMRFFISPNorm2Ch(indices,:).');   % X (2NxM)
toc

%% Plot map
% %% % % JUST the scaled map
fig40 = figure('units','normalized','outerposition',[0 0 1 1]);
% Real part of matrix
scatter(Amap(1,1:N), ...
        Amap(2,1:N), '.b')
hold on
% Imaginary part of matrix
scatter(Amap(1,(N+1):(2*N)), ...
        Amap(2,(N+1):(2*N)), '.r')
% Axis for map
createAxis(max(Amap(1,:)), ...
           max(Amap(2,:)));

legend('real part', 'imaginary part');
title('The map')
xlabel('x'); ylabel('y');
axis square

%% Predict
indicesTest = indices;

dictionaryTest = dictionaryMRFFISPNorm2Ch(indicesTest, :).';
noiseSD = 0.000000000001;

noiseForSignals = noiseSD .* randn(size(dictionaryTest));
Y_pred_norm = Amap * (dictionaryTest + noiseForSignals);

figure
subplot(2,2,1)
% identity line
plot([0,5000],[0,5000]), hold on
% real vs predicted
plot(materialTuples(1,indicesTest), Y_pred_norm(1,:), '.');
axis equal
xlim([0, 5000]); ylim([0, 5000]); 
xlabel('Real T_1(ms)');
ylabel('Predicted T_1(ms)');
title('Comparing T_1')

subplot(2,2,2)
% identity line
plot([0,2200],[0,2200]), hold on
% real vs predicted
plot(materialTuples(2,indicesTest), Y_pred_norm(2,:), '.');
axis equal
xlim([0, 2200]); ylim([0, 2200]); 
xlabel('Real T_2(ms)');
ylabel('Predicted T_2(ms)');
title('Comparing T_2')

subplot(2,2,3)
plot(materialTuples(1,indicesTest), ...
     materialTuples(1,indicesTest) - Y_pred_norm(1,:), '.');
xlabel('T_1(ms)');
ylabel('T_{1 real} - T_{1 pred}');
title('T_{1 real} vs T_{1 pred}')

subplot(2,2,4)
plot(materialTuples(2,indicesTest), ...
     materialTuples(2,indicesTest) - Y_pred_norm(2,:), '.');
xlabel('T_2(ms)');
ylabel('T_{2 real} - T_{2 pred}');
title('T_{2 real} vs T_{2 pred}')


