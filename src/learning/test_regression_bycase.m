function test_regression_bycase(N, MATFLAG, dirname, toSave, noiseSD)
% % % % IRINA GRIGORESCU
% % % % Date created: 08-05-2017
% % % % Date updated: 08-05-2017
% % % % 
% % % % 
% % % % This script tests regression with different train and test data
% % % % 
% % % % INPUT:
% % % %     N = number of timepoints 
% % % %     MATFLAG = (struct) with
% % % %               .train = the case in retrieveMaterialProperties.m
% % % %               .test  = the case in retrieveMaterialProperties.m
% % % %     dirname = name of folder to save data
% % % %     noiseLevel = the level of noise to add on the signals
% % % % 
% % % %     % EXAMPLE USAGE:
% % % %         N = 1000;
% % % %         MATFLAG = struct; 
% % % %         MATFLAG.train =  11; 
% % % %         MATFLAG.test  = 111; 
% % % %         test_regression_2_fixedOffRes(N, MATFLAG)
% % % % 
% % % % OUTPUT:
% % % %     ~

% % % Verify existence of parameters
if nargin < 4
    toSave = 0;
end
if nargin < 5
    noiseSD = 0;
end

% % % When you want to use this code as a script:
% N = 1000;
% MATFLAG = struct; 
% MATFLAG.train =  13; 
% MATFLAG.test  = 130; 
% dirname = 'trainTestSame/df50_T12000_T2200_';
% 
% toSave  = 0;
% noiseSD = 0;

%% 
% % % % % % % % % % % % % % % DICTIONARY GENERATION PART

% % % % % % % % % % % Run simulation for case (training)
[materialProperties.train, sequenceProperties, ...
    materialTuples.train, M.train, ...
    dictionaryMRF] = ...
                simulateMRF(N, 0, ...   % N = #timepoints, FLAGPLOT
                 3, struct, ...         % SEQFLAG, SEQstruct
             MATFLAG.train, struct, 1); % MATFLAG, MATstruct, flagIR

% Create dictionary of signal values (Mtrain x 2N)             
signalDictionaryMRF.train = [ ...
                        squeeze(dictionaryMRF(1:M.train, :, 1))  , ...
                        squeeze(dictionaryMRF(1:M.train, :, 2)) ]; 
                         
                         
% % % % % % % % % % % Run simulation for case (testing)
[materialProperties.test, ~, ...
    materialTuples.test, M.test, ...
    dictionaryMRF] = ...
                simulateMRF(N, 0, ...       % N = #timepoints, FLAGPLOT
                 5, sequenceProperties, ... % SEQFLAG, SEQstruct
               MATFLAG.test, struct, 1);    % MATFLAG, MATstruct, flagIR
            
% Create dictionary of signal values (M x 2N)             
signalDictionaryMRF.test = [ ...
                        squeeze(dictionaryMRF(1:M.test, :, 1))  , ...
                        squeeze(dictionaryMRF(1:M.test, :, 2)) ]; 
                         

% % Add noise to Signals in Test Data before normalization
noiseForSignals = noiseSD .* randn(size(signalDictionaryMRF.test));
signalDictionaryMRF.test = signalDictionaryMRF.test + noiseForSignals;                    
                    
% % % % % % % % % % % Normalize signals 
% % Train
tic
disp('Normalize signals for train data set');
disp([num2str(M.train), ' values in train data set']);
[signalsNorm.train, signalsNormCplx.train] = ...
    normalizeSignals(signalDictionaryMRF.train);
disp('DONE with normalizing signals for train data set');
toc

% % Test
tic
disp('Normalize signals for test data set');
disp([num2str(M.test), ' values in test data set']);
[signalsNorm.test, signalsNormCplx.test] = ...
    normalizeSignals(signalDictionaryMRF.test);
disp('DONE with normalizing signals for test data set');

clear dictionaryMRF
disp('---- Done with simulating data sets');

%% 
% % % % % % % % % % % Make Test Directory
dateFormat = datestr(now, 'yymmdd_HHMMSS'); % with chosen format
dirName = ['../../plots/learning_tests/', dirname, dateFormat,'/'];
if toSave
    disp('~~~ Creating folder ~~~');
    mkdir(dirName)
end
disp(['---- Done with creating plots directory for this test: ', dirName]);

%%
% % % % % % % % % % % Look at the entire dictionary as an image
fig1 = figure('units','normalized','outerposition',[0 0 1 1]);

subplot(1,2,1)
imagesc(sqrt(signalsNorm.train(:,    1:N).^2 + ...
             signalsNorm.train(:, (N+1):(2*N)).^2));
xlabel('#timepoints'); ylabel('#(dictionary entries)');
title('Dictionary TRAIN');

subplot(1,2,2)
imagesc(sqrt(signalsNorm.test(:,    1:N).^2 + ...
             signalsNorm.test(:, (N+1):(2*N)).^2));
xlabel('#timepoints'); ylabel('#(dictionary entries)');
title('Dictionary TEST');
% % % SAVE FIGURE:
if toSave
    disp('~~~ Saving figure ~~~');
    saveas(fig1, [dirName, 'dictionaryTRAINandTEST'], 'png');
end


%% EXAMPLE SIGNALS

fig2 = figure('units','normalized','outerposition',[0 0 1 1]);

% % % % % % % % % % % Look at one signal from the dictionary train vs test
subplot(2,2,1)
plot(sqrt(signalsNorm.train(1,    1:N).^2 + ...
          signalsNorm.train(1, (N+1):(2*N)).^2));
hold on
plot(sqrt(signalsNorm.test(1,    1:N).^2 + ...
          signalsNorm.test(1, (N+1):(2*N)).^2));
legend(['T_1 = ', num2str(materialTuples.train(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.train(2,1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.train(3,1).*1000),' Hz'], ...
       ['T_1 = ', num2str(materialTuples.test(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.test(2,1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.test(3,1).*1000),' Hz']);
xlabel('TR index'); ylabel('signal');
title('Signals from train and test');


% % % % % % % % % % % Look at 2 signals from the dictionary train with
% different off resonance frequencies
subplot(2,2,2)

T11 = materialTuples.train(1,1);
T21 = materialTuples.train(2,1);
df1 = materialTuples.train(3,1);

for i = 2:M.train
    if (materialTuples.train(1,i) == T11) && ...
            (materialTuples.train(2,i) == T21) && ...
                (materialTuples.train(3,i) ~= df1)
            indexDF = i;
            break
    end
end

plot(sqrt(signalsNorm.train(1,    1:N).^2 + ...
          signalsNorm.train(1, (N+1):(2*N)).^2));
hold on
plot(sqrt(signalsNorm.train(indexDF,    1:N).^2 + ...
          signalsNorm.train(indexDF, (N+1):(2*N)).^2));
legend(['T_1 = ', num2str(materialTuples.train(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.train(2,1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.train(3,1).*1000),' Hz'], ...
       ['T_1 = ', num2str(materialTuples.train(1,indexDF)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.train(2,indexDF)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.train(3,indexDF).*1000),' Hz']);
xlabel('TR index'); ylabel('signal');
title('Signals with the same T_1 and T_2 and different \Delta f');


% % % % % % % % % % % Look at 2 signals from the dictionary train with
% different T1s
subplot(2,2,3)

T11 = materialTuples.train(1,1);
T21 = materialTuples.train(2,1);
df1 = materialTuples.train(3,1);

for i = 2:M.train
    if (materialTuples.train(1,i) ~= T11) && ...
            (materialTuples.train(2,i) == T21) && ...
                (materialTuples.train(3,i) == df1)
            indexT1 = i;
            break
    end
end

plot(sqrt(signalsNorm.train(1,    1:N).^2 + ...
          signalsNorm.train(1, (N+1):(2*N)).^2));
hold on
plot(sqrt(signalsNorm.train(indexT1,    1:N).^2 + ...
          signalsNorm.train(indexT1, (N+1):(2*N)).^2));
legend(['T_1 = ', num2str(materialTuples.train(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.train(2,1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.train(3,1).*1000),' Hz'], ...
       ['T_1 = ', num2str(materialTuples.train(1,indexT1)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.train(2,indexT1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.train(3,indexT1).*1000),' Hz']);
xlabel('TR index'); ylabel('signal');
title('Signals with the same T_2 and \Delta f and different T_1');


% % % % % % % % % % % Look at 2 signals from the dictionary train with
% different T2s
subplot(2,2,4)

T11 = materialTuples.train(1,1);
T21 = materialTuples.train(2,1);
df1 = materialTuples.train(3,1);

for i = 2:M.train
    if (materialTuples.train(1,i) == T11) && ...
            (materialTuples.train(2,i) ~= T21) && ...
                (materialTuples.train(3,i) == df1)
            indexT2 = i;
            break
    end
end

plot(sqrt(signalsNorm.train(1,    1:N).^2 + ...
          signalsNorm.train(1, (N+1):(2*N)).^2));
hold on
plot(sqrt(signalsNorm.train(indexT2,    1:N).^2 + ...
          signalsNorm.train(indexT2, (N+1):(2*N)).^2));
legend(['T_1 = ', num2str(materialTuples.train(1,1)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.train(2,1)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.train(3,1).*1000),' Hz'], ...
       ['T_1 = ', num2str(materialTuples.train(1,indexT2)),' ms, ', ...
        'T_2 = ', num2str(materialTuples.train(2,indexT2)),' ms, ', ...
   '\Delta f = ', num2str(materialTuples.train(3,indexT2).*1000),' Hz']);
xlabel('TR index'); ylabel('signal');
title('Signals with the same T_1 and \Delta f and different T_2');
% % % SAVE FIGURE:
if toSave
    disp('~~~ Saving figure ~~~');
    savefig(fig2, [dirName, 'exampleSignals.fig']);
    saveas(fig2, [dirName, 'exampleSignals.png'], 'png');
end

disp('---- Done with plotting dictionary signals');
%%
% % % % % % % % % % % % % % % VISUALIZE MATERIAL TUPLES
fig3 = figure('units','normalized','outerposition',[0 0 1 1]);

subplots = {'tissueTrain', 'tissueTest', ...
            'tissue3D', 'tissueXY', 'tissueYZ'};
titlesSubplots = {'Tissue combinations train data', ...
                  'Tissue combinations test data', ...
                  'Tissue combinations space 3D', ...
                  'Tissue combinations space (x-y view)', ...
                  'Tissue combinations space (y-z view)'};        
sbpltIndices.(subplots{1}) = [ 1,  8];
sbpltIndices.(subplots{2}) = [ 5, 12];
sbpltIndices.(subplots{3}) = [ 3, 10];
sbpltIndices.(subplots{4}) = [19, 33];
sbpltIndices.(subplots{5}) = [22, 36];

for i = 1:5
    
    % Subplot number and position
    subplot(6, 6, sbpltIndices.(subplots{i}))
    
    if i == 1
        scatter3(materialTuples.train(1, :), ...
                 materialTuples.train(2, :), ...
                 materialTuples.train(3, :).*1000, '.b')
        legend('train');
    else if i == 2
        scatter3(materialTuples.test(1, :), ...
                 materialTuples.test(2, :), ...
                 materialTuples.test(3, :).*1000, '.r')
        legend('test');
        else
            scatter3(materialTuples.train(1, :), ...
                 materialTuples.train(2, :), ...
                 materialTuples.train(3, :).*1000, '.b')
            hold on
            scatter3(materialTuples.test(1, :), ...
                 materialTuples.test(2, :), ...
                 materialTuples.test(3, :).*1000, '.r')
            legend('train','test');
        end
    end
       
    % View
    if i == 4
        view( 0, 90); % x - y
    else
        if i == 5
            view( 90, 0); % y - z
        end
    end
    % Labels
    xlabel('T_1 [ms]'); ylabel('T_2 [ms]'); zlabel('\Delta f [Hz]');
    
    % Title and axis
    title(titlesSubplots{i});
    axis equal
    axis square
end
% % % SAVE FIGURE:
if toSave
    disp('~~~ Saving figure ~~~');
    savefig(fig3, [dirName, 'materialProperties.fig']);
    saveas(fig3, [dirName, 'materialProperties.png'], 'png');
end

disp('---- Done with plotting material tuples');
%%
% % % % % % % % % % % % % % % LEARN MAP PART
tic
% Amap = learnMap(materialTuples.train, ... % Y ( 3xM)
%                    signalsNorm.train');   % X (2NxM)

% Scaling the parameters before learning the map:
scalingMatrix = [0.1  0     0 ; ...
                  0   1     0 ; ...
                  0   0  10000];

materialTuplesScaled.train = scalingMatrix * materialTuples.train;

AmapScaled = learnMap(materialTuplesScaled.train, ...  % Y ( 3xM)
                      signalsNorm.train');             % X (2NxM)

                  
toc
disp('---- Done with learning map');
%%
% % % % % % % % % % % % % % % PLOT MAP
% fig4 = figure('units','normalized','outerposition',[0 0 1 1]);
% 
% maps = zeros(size(Amap,1), size(Amap,2), 2);
% maps(:,:,1) = Amap;
% maps(:,:,2) = AmapScaled;
% titles = {'Map', 'Map (scaling applied)'};
% 
% for i = 1:2
% 
%     % Subplot 
%     subplot(1,2,i)
%     
%     % Real part of matrix
%     scatter3(maps(1,1:N,i), ...
%              maps(2,1:N,i), ...
%              maps(3,1:N,i), '.b')
%     hold on
%     % Imaginary part of matrix
%     scatter3(maps(1,(N+1):(2*N),i), ...
%              maps(2,(N+1):(2*N),i), ...
%              maps(3,(N+1):(2*N),i), '.r')
% 	% Axis for map
%     createAxis(max(maps(1,:,i)), ...
%                max(maps(2,:,i)), ...
%                max(maps(3,:,i)));
%     view(-20,15)
%     legend('real part', 'imaginary part');
%     title(titles{i})
%     xlabel('x'); ylabel('y'); zlabel('z');
%     axis square
% end
% clear maps
% 
% % % % SAVE FIGURE:
% if toSave
%     disp('~~~ Saving figure ~~~');
%     savefig(fig4, [dirName, ...
%         'map_cond', num2str(round(cond(Amap))), ...
%         'mapScaled_cond', num2str(round(cond(AmapScaled))), '.fig']);
%     saveas(fig4, [dirName, ...
%         'map_cond', num2str(round(cond(Amap))), ...
%         'mapScaled_cond', num2str(round(cond(AmapScaled))), '.png'], ...
%         'png');
% end


% %% % % JUST the scaled map
fig40 = figure('units','normalized','outerposition',[0 0 1 1]);
% Real part of matrix
scatter3(AmapScaled(1,1:N), ...
         AmapScaled(2,1:N), ...
         AmapScaled(3,1:N), '.b')
hold on
% Imaginary part of matrix
scatter3(AmapScaled(1,(N+1):(2*N)), ...
         AmapScaled(2,(N+1):(2*N)), ...
         AmapScaled(3,(N+1):(2*N)), '.r')
% Axis for map
createAxis(max(AmapScaled(1,:)), ...
           max(AmapScaled(2,:)), ...
           max(AmapScaled(3,:)));
view(-20,15)
legend('real part', 'imaginary part');
title('The map')
xlabel('x'); ylabel('y'); zlabel('z');
axis square

% % % SAVE FIGURE:
if toSave
    disp('~~~ Saving figure ~~~');
    savefig(fig40, [dirName, ...
        'mapScaled_cond', num2str(round(cond(AmapScaled))), '.fig']);
    saveas(fig40, [dirName, ...
        'mapScaled_cond', num2str(round(cond(AmapScaled))), '.png'], ...
        'png');
end

disp(['---- Done with plot map -- cond(A) = ', ...
      num2str(round(cond(AmapScaled)))]);
%%
% % % % % % % % % % % % % % % PREDICT MATERIAL PROPERTIES
Y_test            = materialTuples.test;

% % % % Apply the scaled map
scalingBackMatrix = [10   0     0 ; ...
                      0   1     0 ; ...
                      0   0  0.0001];
Y_pred_norm.map   = scalingBackMatrix * ...
                    (AmapScaled * signalsNorm.test');

% % % FOR now:
% Y_pred_norm.match = Amap * signalsNorm.test';


% % % % 
% % % % Matching algorithm
tic
disp('Matching alg');
Y_pred_norm.match = [];

chunks = ceil(M.test/1000);
dchunk = ceil(M.test/chunks);
for i = 1:chunks
    
    idxstart = (i-1)*dchunk+1;
    if i < chunks
        idxend = i*dchunk;
    else
        idxend = M.test;
    end
    
    [~, indicesMatch, matchedStructure] = matching(signalsNorm.train, ...
                               signalsNorm.test(idxstart : idxend, :), ...
                               materialTuples.train);
    % Add to predicted
    tempMatch = [matchedStructure.T1 ; ...
                 matchedStructure.T2 ; ...
                 matchedStructure.offRes];
    Y_pred_norm.match = [Y_pred_norm.match tempMatch];
    disp(['* Chunk ', num2str(i), ' / ', num2str(chunks)]);
end
toc
disp('DONE with Matching alg');
% % % % 
% % % % 


residuals.map   = Y_pred_norm.map   - Y_test;
residuals.match = Y_pred_norm.match - Y_test;

percError.map = [ ...
              (residuals.map(1, :))./(Y_test(1, :)).*100  ; ...
              (residuals.map(2, :))./(Y_test(2, :)).*100] ;
percError.match = [ ...
              (residuals.match(1, :))./(Y_test(1, :)).*100  ; ...
              (residuals.match(2, :))./(Y_test(2, :)).*100] ;          
          
SSE.map   = sum(residuals.map.^2, 2);   % sum of squared errors map
SSE.match = sum(residuals.match.^2, 2); % sum of squared errors match

RMSE.map   = sqrt((1/size(Y_test,2)).*SSE.map);
RMSE.match = sqrt((1/size(Y_test,2)).*SSE.match);

% mean(Y_test, 2) - mean for each row (meanT_1, meanT_2, meandf)
SST   = sum((Y_test - repmat(mean(Y_test, 2), ...
                          [1, size(Y_test, 2)])).^2, 2);
                      
Rsq.map   = 1 - ((SSE.map)  ./ SST);
Rsq.match = 1 - ((SSE.match)./ SST);

disp('---- Done with calculating residuals');
%%
% % % % % % % % % % % % % % % PLOT RESIDUALS AND PERCENTAGE ERROR

typesOfAlgs = {'map', 'match'};
materials   = {'T1', 'T2', 'df'};
labels      = {'T_1 [ms]', 'T_2 [ms]', '\Delta f [kHz]'};
typesOfX    = [1 1 2];
typesOfY    = [2 3 3];
azim = 70;
elev = 25;

% For each type of algorithm
for iType = 1:length(typesOfAlgs)
    
    % For each type of combination of parameters
    % Error in T1/T2/df for each combination of:
    % 1. T1/T2
    % 2. T1/df
    % 3. T2/df
    for iComb = 1:3
        
        figRes = figure('units','normalized','outerposition',[0 0 1 1]);

        % % % % Percentage error in T_1
        subplot(2,3,1)
        scatter3(  Y_test(typesOfX(iComb), :), ...   % T_1/T_1/T_2
                   Y_test(typesOfY(iComb), :), ...   % T_2/ df/ df
                   percError.(typesOfAlgs{iType})(1,:), ... % percentage error
                   '.'); 
        view(azim, elev)
        % Y_predicted(1,:)-Y_test(1,:),'.'); 
        xlabel(labels{typesOfX(iComb)}); 
        ylabel(labels{typesOfY(iComb)});
        zlabel('%err T_1');
        title('%err in T_1')

        % % % % Percentage error in T_2
        subplot(2,3,2)
        scatter3(  Y_test(typesOfX(iComb), :), ...   % T_1/T_1/T_2
                   Y_test(typesOfY(iComb), :), ...   % T_2/ df/ df
                   percError.(typesOfAlgs{iType})(2,:), ... % percentage error
                   '.'); 
        view(azim, elev)
        xlabel(labels{typesOfX(iComb)}); 
        ylabel(labels{typesOfY(iComb)});
        zlabel('%err T_2');
        title({[upper(typesOfAlgs{iType}), ' algorithm'], '', '%err in T_2'})


        % % % % Residuals in T_1
        subplot(2,3,4)
        scatter3(  Y_test(typesOfX(iComb), :), ...   % T_1/T_1/T_2
                   Y_test(typesOfY(iComb), :), ...   % T_2/ df/ df
                   residuals.(typesOfAlgs{iType})(1,:), ... % residuals
                   '.'); 
        view(azim, elev)
        % Y_predicted(1,:)-Y_test(1,:),'.'); 
        xlabel(labels{typesOfX(iComb)}); 
        ylabel(labels{typesOfY(iComb)});
        zlabel('residuals T_1');
        title('Residuals in T_1')

        % % % % Residuals in T_2
        subplot(2,3,5)
        scatter3(  Y_test(typesOfX(iComb), :), ...   % T_1/T_1/T_2
                   Y_test(typesOfY(iComb), :), ...   % T_2/ df/ df
                   residuals.(typesOfAlgs{iType})(2,:), ... % residuals
                   '.'); 
        view(azim, elev)
        % Y_predicted(1,:)-Y_test(1,:),'.'); 
        xlabel(labels{typesOfX(iComb)}); 
        ylabel(labels{typesOfY(iComb)});
        zlabel('residuals T_2');
        title('residuals in T_2')

        % % % % Residuals in df
        subplot(2,3,6)
        scatter3(  Y_test(typesOfX(iComb), :), ...   % T_1/T_1/T_2
                   Y_test(typesOfY(iComb), :), ...   % T_2/ df/ df
                   residuals.(typesOfAlgs{iType})(3,:), ... % residuals
                   '.'); 
        view(azim, elev)
        xlabel(labels{typesOfX(iComb)}); 
        ylabel(labels{typesOfY(iComb)});
        zlabel('residuals \Delta f');
        title('Residuals in \Delta f');

        % % SAVE FIGURE
        if toSave
            disp('~~~ Saving figure ~~~');
            savefig(figRes, [dirName, 'residuals_', ...
                            materials{typesOfX(iComb)}, 'vs', ...
                            materials{typesOfY(iComb)}, '_' , ...
                         upper(typesOfAlgs{iType}), '.fig']);
            saveas(figRes, [dirName, 'residuals_', ...
                            materials{typesOfX(iComb)}, 'vs', ...
                            materials{typesOfY(iComb)}, '_' , ...
                         upper(typesOfAlgs{iType}), '.png'], 'png');
        end
    end
end

disp('---- Done with plotting residuals');
%%
% % % % % % % % % % % % % % % PLOT PREDICTED VS REAL

typesOfAlgs = {'map', 'match'};

for iType = 1:length(typesOfAlgs)
    
    figRsq = figure('units','normalized','outerposition',[0 0 1 1]);

    % % % % % T_1
    % Plot Real Values vs Predicted Values 
    subplot(2,3,1)
    scatter(Y_test(1, :), ...
            Y_pred_norm.(typesOfAlgs{iType})(1, :), 'o')
    hold on
    plot(min(materialProperties.test.T1):max(materialProperties.test.T1), ...
         min(materialProperties.test.T1):max(materialProperties.test.T1))
    grid on
    axis equal
    axis square
%     axis([min(materialProperties.test.T1)-1 ...
%           max(materialProperties.test.T1)+1 ...
%           min(materialProperties.test.T1)-1 ...
%           max(materialProperties.test.T1)+1]);
    xlabel('real T_1 [ms]');ylabel('predicted T_1 [ms]');
    title({['$SSE_{T_1} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
            num2str(SSE.(typesOfAlgs{iType})(1))], '', ...
           ['$RMSE_{T_1} = \sqrt{\frac{1}{M} SSE_{T_1}} = $ ', ...
            num2str(RMSE.(typesOfAlgs{iType})(1))], '', ...
           ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                         '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
        num2str(Rsq.(typesOfAlgs{iType})(1))]}, 'interpreter', 'latex');
    
    % Plot Real Values vs Percentage Error
    subplot(2,3,4)
    scatter(Y_test(1, :), ...
            percError.(typesOfAlgs{iType})(1,:), '.');
    grid on
    axis equal
    axis square
    xlabel('real T_1 [ms]'); ylabel('%error T_1 [ms]');
    title({'Real T_1 vs', 'Percentage Error in T_1'});

    % % % % T_2
    % Plot Real Values vs Predicted Values 
    subplot(2,3,2)
    scatter(Y_test(2, :), ...
            Y_pred_norm.(typesOfAlgs{iType})(2, :), 'o')
    hold on
    plot(min(materialProperties.test.T2):max(materialProperties.test.T2), ...
         min(materialProperties.test.T2):max(materialProperties.test.T2))
    grid on
    axis equal
    axis square
%     axis([min(materialProperties.test.T2)-1 ...
%           max(materialProperties.test.T2)+1 ...
%           min(materialProperties.test.T2)-1 ...
%           max(materialProperties.test.T2)+1]);

    xlabel('real T_2 [ms]');ylabel('predicted T_2 [ms]');
    title({[upper(typesOfAlgs{iType}), ' algorithm'], '', ...
           ['$SSE_{T_2} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
            num2str(SSE.(typesOfAlgs{iType})(2))], '', ...
           ['$RMSE_{T_2} = \sqrt{\frac{1}{M} SSE_{T_2}} = $ ', ...
            num2str(RMSE.(typesOfAlgs{iType})(2))], '', ...
           ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                            '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
            num2str(Rsq.(typesOfAlgs{iType})(2))]}, 'interpreter', 'latex');

    % Plot Real Values vs Percentage Error in T_2
	subplot(2,3,5)
    scatter(Y_test(2, :), ...
            percError.(typesOfAlgs{iType})(2,:), '.')
    grid on
    axis equal
    axis square
    xlabel('real T_2 [ms]'); ylabel('%error T_2 [ms]');
    title({'Real T_2 vs', 'Percentage Error in T_2'});
        
    % % % % Delta f
    % Plot Real Values vs Predicted Values
    subplot(2,3,3)
    scatter(Y_test(3, :).*1000, ...
            Y_pred_norm.(typesOfAlgs{iType})(3, :).*1000, 'o')
    hold on
    dfPlt = max(materialProperties.test.offRes)*1000;
    plot(-dfPlt-1 : dfPlt+1, ...
         -dfPlt-1 : dfPlt+1);
    grid on
    axis equal
    axis square
    % axis([0 max(materialProperties.test.offRes).*1000 ...
    %       0 max(materialProperties.test.offRes).*1000])

    xlabel('real \Delta f [Hz]');ylabel('predicted \Delta f [Hz]');
    title({['$SSE_{\Delta f} = \sum_{i = 1}^{M} (y_i - A h(y_i))^2 =$ ', ...
            num2str(SSE.(typesOfAlgs{iType})(3))], '', ...
           ['$RMSE_{\Delta f} = \sqrt{\frac{1}{M} SSE_{\Delta f}} = $ ', ...
            num2str(RMSE.(typesOfAlgs{iType})(3))], '', ...
            ['$R^2 = 1 - \frac{\sum_{i = 1}^{M} (y_i - A h(y_i))^2}', ...
                            '{\sum_{i = 1}^{M} (y_i - \bar{y}_{i})^2} =$ ', ...
            num2str(Rsq.(typesOfAlgs{iType})(3))]}, 'interpreter', 'latex');
       
    % Plot Real Values vs Residuals
    subplot(2,3,6)
    scatter(Y_test(3, :), ...
            residuals.(typesOfAlgs{iType})(3,:), '.')
    grid on
    axis equal
    axis square
    % axis([0 max(materialProperties.test.offRes).*1000 ...
    %       0 max(materialProperties.test.offRes).*1000])
    xlabel('real \Delta f [kHz]');ylabel('residuals \Delta f [kHz]');
    title({'Real \Delta f', 'vs Residuals in \Delta f'});
        
        
	% % SAVE FIGURE
    if toSave
        disp('~~~ Saving figure ~~~');
        savefig(figRsq, [dirName, 'Rsq_', ...
                     upper(typesOfAlgs{iType}), '.fig']);
        saveas(figRsq, [dirName, 'Rsq_', ...
                     upper(typesOfAlgs{iType}), '.png'], 'png');
    end
end

disp('---- Done with plotting R squared');
%% Plot 3D errors with slice 
if M.test == size(Y_test, 2)

    [x,y] = meshgrid(materialProperties.test.T1, ...
                 materialProperties.test.T2);
    [mT2, mT1] = size(x); % m - T2, n - T1
    mdf = length(materialProperties.test.offRes);

    N_T1     = size(materialProperties.test.T1,     2);
    N_T2     = size(materialProperties.test.T2,     2);
    N_offRes = size(materialProperties.test.offRes, 2);

    materials = {'T_1', 'T_2', '\Delta f'};

    typesOfAlgs = {'map', 'match'};

    for iType = 1:length(typesOfAlgs)

        fig3D = figure('units','normalized','outerposition',[0 0 1 1]);
        residualsVolume = zeros(mT2, mT1, mdf) - 1000;
        lala = zeros(mT1, mT2);
        % For each off resonance frequency
        for idxdf = 1:mdf

            transp = 1 - 0.7*idxdf/mdf;

            % For each material property 
            for idMatProp = 1:3

                % Reziduals at current df for current material property
                if idMatProp < 3
                    resz = ...
                        percError.(typesOfAlgs{iType})(idMatProp, ...
                                                       idxdf:mdf:M.test);
                else
                    resz = ...
                        residuals.(typesOfAlgs{iType})(idMatProp, ...
                                                       idxdf:mdf:M.test);
                end

                % Put them in a 2D matrix
                idx = 1;
                for iT2 = 1:N_T2                       % T_1 (mT1)
                    for iT1 = 1:N_T1                   % T_2 (mT2)
                        if x(iT2,iT1) >= y(iT2,iT1)
                            residualsVolume(iT2,iT1,idxdf) = resz(idx);
                            lala(iT1,iT2) = 1;
                            idx = idx + 1;
                        else
                            residualsVolume(iT2,iT1,idxdf) = NaN;
                        end
                    end
                end


                % Plot Slice
                subplot(1, 3, idMatProp);
                view([75 15 10]);
                view( 18, 11);      % az = 25, el = 7
                hz = slice(residualsVolume, ...
                           [], [], idxdf);
                alpha(hz, transp);
                colormap jet ; colorbar
                set(findobj(gca,'Type','Surface'),'EdgeColor','none')
                hold on
                if idMatProp < 3
                    if idMatProp == 2
                        title({...
                            [upper(typesOfAlgs{iType}), ' algorithm'], ...
                            '', ...
                            ['Percentage error for ', ...
                                materials{idMatProp}]});
                    else
                        title(['Percentage error for ', ...
                                    materials{idMatProp}]);
                    end
                else
                    title(['Residuals for ', materials{idMatProp}]);
                end
                xlabel('T_1 [ms]'); 
                ylabel('T_2 [ms]'); 
                zlabel('\Delta f [Hz]');

                % Values on the X-axis
                dT1 = ceil(N_T1/5);
                set(gca,'XTick', 1 : dT1 : N_T1); 
                set(gca,'XTickLabels', ...
                 strread( num2str( ...
                          materialProperties.test.T1(1:dT1:end)), '%s'));
                % Values on the Y-axis
                dT2 = ceil(N_T2/5);
                set(gca,'YTick', 1 : dT2 : N_T2); 
                set(gca,'YTickLabels', ...
                 strread( num2str( ...
                          materialProperties.test.T2(1:dT2:end)), '%s'));
                % Values on the Z-axis
                doffRes = ceil(N_offRes/6);
                set(gca,'ZTick', 1 : doffRes : N_offRes);
                set(gca,'ZTickLabels', ...
                 strread(...
                   num2str( ...
                    materialProperties.test.offRes(1:doffRes:end).*1000), ...
                   '%s'));
            end
        end
        
        % % SAVE FIGURE
        if toSave
            disp('~~~ Saving figure ~~~');
            savefig(fig3D, [dirName, 'errors3D_', ...
                        upper(typesOfAlgs{iType}), '.fig']);
            saveas(fig3D, [dirName, 'errors3D_', ...
                        upper(typesOfAlgs{iType}), '.png'], 'png');
        end
    end

end

disp('---- Done with plotting 3D errors');


disp(M.train);
disp(M.test);

disp('End of test');

%% Write to README text file
if toSave
    fileID = fopen([dirName, 'README.txt'], 'w');

    % NAME OF TEST
    fprintf(fileID, ...
         'REGRESSION TEST for:\nCase %5d for Train\nCase %5d for Test\n\n', ...
          MATFLAG.train, MATFLAG.test);
    % Noise level
    fprintf(fileID, ...
            '\nNOISE stddev = %d\n', noiseSD);
    % Save test and train size
    fprintf(fileID,'Mtest = %d\nMtrain = %d\n', M.test, M.train);
    % Save T1s T2s and dfs for train
    fprintf(fileID,'\nTrain Dataset: \nT_1 = ');
    fprintf(fileID,'%.3f | ', materialProperties.train.T1);
    fprintf(fileID,'\nNumber of T1 Values = %d\n', ...
        length(materialProperties.train.T1));
    fprintf(fileID,'\nT_2 = ');
    fprintf(fileID,'%.3f | ', materialProperties.train.T2);
    fprintf(fileID,'\nNumber of T2 Values = %d\n', ...
        length(materialProperties.train.T2));
    fprintf(fileID,'\ndf = ');
    fprintf(fileID,'%.3f | ', materialProperties.train.offRes);
    fprintf(fileID,'\nNumber of offRes Values = %d\n', ...
        length(materialProperties.train.offRes));
    
    % Save T1s T2s and dfs for test
    fprintf(fileID,'\n\nTest Dataset: \nT_1 = ');
    fprintf(fileID,'%.3f | ', materialProperties.test.T1);
    fprintf(fileID,'\nNumber of T1 Values = %d\n', ...
        length(materialProperties.test.T1));
    fprintf(fileID,'\nT_2 = ');
    fprintf(fileID,'%.3f | ', materialProperties.test.T2);
    fprintf(fileID,'\nNumber of T2 Values = %d\n', ...
        length(materialProperties.test.T2));
    fprintf(fileID,'\ndf = ');
    fprintf(fileID,'%.3f | ', materialProperties.test.offRes);
    fprintf(fileID,'\nNumber of offRes Values = %d\n', ...
        length(materialProperties.test.offRes));

    % Some Conditioning things:
    fprintf(fileID,'\n\nConditioning: \n');
    fprintf(fileID,'k(A) = %f \n', round(cond(AmapScaled)));
    fprintf(fileID,['Angle between predicted and real\n', ...
                    'T_1 = %f\nT_2 = %f\ndf = %f\n\n'], ...
                    acos(norm(Y_pred_norm.map(1,:) / norm(Y_test(1,:)) )), ...
                    acos(norm(Y_pred_norm.map(2,:) / norm(Y_test(2,:)) )), ...
                    acos(norm(Y_pred_norm.map(3,:) / norm(Y_test(3,:)) )));

    fclose(fileID);
    
    close all
end












