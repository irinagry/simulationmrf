% % % % IRINA GRIGORESCU
% % % % Date Created: 23-03-2017
% % % % Date Updated: 23-03-2017
% % % % 
% % % % Tiny script to plot the values calculated by
% % % % steadyStateMagnetisationOfbSSFP.m
% % % % 

T1 = 1000;
T2 = 1000;
TR =  200;
FA = pi/4;
betas = [0:pi/100:2*pi];

[Mminus, Mplus] = steadyStateMagnetisationOfbSSFP(T1, T2, TR, FA, betas); 

% Plot
figHandle = figure(10);
set(figHandle, 'Position', [10, 10, 600, 800]);

[xax, yax, zax] = createAxis();
% Viewer position
view([20 70 10]);

plot3(Mplus(1,:),  Mplus(2,:),  Mplus(3,:));
hold on
plot3(Mminus(1,:), Mminus(2,:), Mminus(3,:));
hold on
title({ ['T_1 = ', num2str(T1), ' ms, T_2 = ', num2str(T2), ' ms'], ...
        ['FA = ', num2str(rad2deg(FA)), '^o, TR = ', num2str(TR), ' ms']});

grid on
axis equal
xlim([-1, 1])
ylim([-1, 1])
zlim([-1, 1])