function [Mminus, Mplus] = steadyStateMagnetisationOfbSSFP(T1, T2, TR, alpha, betas)
%
% [Mminus, Mplus] = steadyStateMagnetisationOfbSSFP(T1, T2, TR, alpha, betas)
%
% This function computes the steady-state magnetisation of a balanced
% steady-state free-precession (bSSFP) sequence.
%
% Assumptions:
% 
% 1) The M0 is the magnetisation in the thermal equilibrium ([0 0 1]').
% 2) The RF pulses are along the x-axis throughout the sequence.
% 3) The RF pulses all have the same flip angle.
%
% Inputs:
% 
% T1: T1 of the tissue substrate
% T2: T2 of the tissue substrate
% 
% TR: The repetition time, the time between the consecutive RF pulses.
% alpha: The flip angle of the RF pulses.
% betas: The set of off-resonance rotations to sample.
%
% Outputs:
%
% Mminus: The steady-state magnetisation immediately before each RF pulse.
% Mplus: The steady-state magnetisation immediately after each RF pulse.
%
% By Gary Zhang (gary.zhang@ucl.ac.uk)
%
% Reference: Sekihara K (1987) IEEE TMI; 6(12):157-64
%

% pre-compute the relaxation terms
E1 = repmat(exp(-TR/T1), size(betas));
E2 = repmat(exp(-TR/T2), size(betas));

% pre-compute the trigonometry terms
cosa = repmat(cos(alpha), size(betas));
sina = repmat(sin(alpha), size(betas));

cosb = cos(betas);
sinb = sin(betas);

% a vector of ones
I = ones(size(betas));

% compute the constant divider, which is the determinant of the matrix that
% needs inverting to compute the steady-state magnetisation.  If this value
% is equal to zero, no steady states can be achieved.
D = (I-E1.*cosa).*(I-E2.*cosb) - E2.*(E1-cosa).*(E2-cosb);

% the magnetisation assumed at the thermal equilibrium
M0 = I;

% compute the common multiplier
common = M0.*(I - E1);

% compute the steady-state magnetisation immediately before each RF pulse
Mminus(1,:) = (common.*E2.*sina.*sinb)./D;
Mminus(2,:) = (common.*E2.*sina.*(cosb-E2))./D;
Mminus(3,:) = (common.*((I-E2.*cosb)-E2.*cosa.*(cosb-E2)))./D;

% compute the steady-state magnetisation immediately after each RF pulse
Mplus(1,:) = Mminus(1,:);
Mplus(2,:) = (common.*sina.*(I-E2.*cosb))./D;
Mplus(3,:) = (common.*(E2.*(E2-cosb) + cosa.*(I-E2.*cosb)))./D;

