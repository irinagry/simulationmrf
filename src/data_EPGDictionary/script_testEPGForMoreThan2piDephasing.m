% IRINA GRIGORESCU
% Date created: 13-09-2017
% Date updated: 13-09-2017
%
% This script is to verify how more than 2pi dephasing per TR behaves

% Observations: I have verified how 2pi vs 8pi dephasing per TR behaves
% My conclusions were that the signals differ within 10^-15, which I think
% is needless to say small enough to be disregarded

% Preprocessing steps
clear all;
close all;
clc;

% Add needed paths:
run('runForPaths');

%% 
% % % SEQUENCE PROPERTIES for 2pi dephasing
N = 100;
seqPropertiesMatrix = zeros(N, 4); % 300 blocks and 
                                   %   4 sequence params
seqPropertiesMatrix(:,1) = 15;       % FA (deg)
seqPropertiesMatrix(:,2) = 15;       % TR (ms)
seqPropertiesMatrix(:,3) =  0;       % PhA(deg)
seqPropertiesMatrix(:,4) =  6;       % TE (ms)

FAsEPG2pi  = seqPropertiesMatrix(:,1);
PhAsEPG2pi = seqPropertiesMatrix(:,3);
TRsEPG2pi  = seqPropertiesMatrix(:,2);
TEsEPG2pi  = seqPropertiesMatrix(:,4);

% % % SEQUENCE PROPERTIES for 8pi dephasing
seqPropertiesMatrix = zeros(N*4, 4); % 300 blocks and 
                                   %   4 sequence params
seqPropertiesMatrix(:,1) = repmat([15  0 0 0], [1, N]);       % FA (deg)
seqPropertiesMatrix(:,2) = repmat([ 9  2 2 2], [1, N]);       % TR (ms)
seqPropertiesMatrix(:,3) = repmat([ 0  0 0 0], [1, N]);       % PhA(deg)
seqPropertiesMatrix(:,4) = repmat([ 6  1 1 1], [1, N]);       % TE (ms)

FAsEPG8pi  = seqPropertiesMatrix(:,1);
PhAsEPG8pi = seqPropertiesMatrix(:,3);
TRsEPG8pi  = seqPropertiesMatrix(:,2);
TEsEPG8pi  = seqPropertiesMatrix(:,4);

% % % MATERIAL PROPERTIES
materialProperties = struct;
materialProperties.T1     = 5000;
materialProperties.T2     = 2200;
materialProperties.offRes =    0; 

% Retrieve set of material properties in a (3xM) form
[materialTuples, M] = createSetOfTuples(materialProperties);
materialTuples = materialTuples(1:2, :);

%%
% % % RUN EPG
T1 = materialTuples(1,1);
T2 = materialTuples(2,1);

% 2pi dephasing
[ChiF, ChiZ] = epgMatrix(N, PhAsEPG2pi, FAsEPG2pi, TRsEPG2pi, ...
                         T1, ...    % T_1
                         T2, ...    % T_2
                         1, 1);     % M0, flagDephase
F0states2pi = ChiF(N, :) .* exp(-TEsEPG2pi ./ T2).';

% 8pi dephasing
[ChiF, ChiZ] = epgMatrix(4*N, PhAsEPG8pi, FAsEPG8pi, TRsEPG8pi, ...
                         T1, ...    % T_1
                         T2, ...    % T_2
                         1, 1);     % M0, flagDephase
F0states8pi = ChiF(4*N, :) .* exp(-TEsEPG8pi ./ T2).';

%%
figure
subplot(2,1,1)
plot(1:N, abs(F0states2pi), '-o'), hold on
plot(1:N, abs(F0states8pi(1:4:end)), '-.')
xlabel('timepoints'); ylabel('signal magnitude')

subplot(2,1,2)
plot(1:N, abs(F0states2pi)-abs(F0states8pi(1:4:end)), '-o'), hold on
xlabel('timepoints'); ylabel('signal 2*pi - signal 8*pi')