% IRINA GRIGORESCU
% Date created: 30-08-2017
% Date updated: 30-08-2017
%
% This script is to create an MRF dictionary with EPG
% 
% To reproduce plots from FIGURE 5 from MRF-FISP paper, see last lines of
% code

% Preprocessing steps
% clear all;
% close all;
% clc;

% Add needed paths:
run('runForPaths');

% Create dictionary from scratch (1)
toCreate = 1;

% Save dictionary to file (1)
toSave   = 1;
filenameDict = 'syntheticDictionary';

% Retrieve material properties from FISP database (0) or 
%                                 choose your own (1) (GOTO line 110)
toCreateMaterialProp = 1;

% Start with an inversion recovery pulse yes - (1) no - (0)
flagIR = 1;

% Retrieve sequence properties from file (0) or (goto line 60)
%                        choose your own (1)    (goto line 50)
toCreateSequenceProp = 0;
isCSVFile = 1;           % (1) csv file - needs fiddling
                         %                      (goto line 81)
                         % (0) individual files for each parameter 
                         %                      (goto line 64)
isRepeatSequence = 0;    % Based on MRF with Short Relaxation Intervals 
                         % by Anthor, Doneva
                         % (1) repeating the sequence NRep times and
                         % keeping only the last fingerprint
                         % (0) not repeating the sequence

% % % 1 = FISP | 0 = bSSFP
flagDephase = 0; 

% % % PLOTS FOR MRF_FISP PAPER
toPlotSomePlots = 0; % this doesn't work unless you are reproducing the 
                     % sequence and materials used in the MRF Fisp paper

%%
% % % % % % % % 
% % % % % % % % SEQUENCE PROPERTIES
% % % % % % % % 
% % % Load Sequence Properties
% % % FA(deg) | TR(ms) | PhA(deg) | TE(ms)
if toCreateSequenceProp == 1
    % % Example with constant values
    fprintf('\n-----> Getting sequence properties from user\n ');
    seqPropertiesMatrix = zeros(100, 4); % 300 blocks and 
                                         %   4 sequence params
    seqPropertiesMatrix(:,1) = 90;      % FA (deg)
    seqPropertiesMatrix(:,2) = 15;      % TR (ms)
    seqPropertiesMatrix(:,3) =  0;      % PhA(deg)
    seqPropertiesMatrix(:,4) =  6;      % TE (ms)
else
    if isCSVFile == 0
        % % 
        % % Values from MRF-FISP paper
        % % 
        fileFAs = 'FAs_mrf_maryia'; %'FAs_mrf_fisp'; 
        fileTRs = 'TRs_mrf_maryia'; %'TRs_mrf_fisp';
        
        fprintf(['\n-----> Getting sequence properties from files: ', ...
                 '%s and %s \n'], fileFAs, fileTRs);
        FAs_mrf_fisp = dlmread(fileFAs);
        TRs_mrf_fisp = dlmread(fileTRs);
        N = size(FAs_mrf_fisp, 1);
        seqPropertiesMatrix = zeros(N, 4);
        seqPropertiesMatrix(:,1) = FAs_mrf_fisp;           % FA (deg)
        seqPropertiesMatrix(:,2) = TRs_mrf_fisp;           % TR (ms)
        seqPropertiesMatrix(:,3) =  0;                     % PhA(deg)
        seqPropertiesMatrix(:,4) =  6;                     % TE (ms)
    else
        % % 
        % % From a CSV File
        % % 
        fileCSV = 'sequenceParametersMaryia.csv';
        fprintf(['\n-----> Getting sequence properties from file: ', ...
                 '%s\n'], fileCSV);
        seqPropertiesMatrix = ...
              csvread(fileCSV, 1);
    end
end
N = size(seqPropertiesMatrix, 1);

% % SEQUENCE properties 
if flagIR == 1
    fprintf('\n-----> Adding inversion pulse\n');
    FAsEPG  = [ 180;   seqPropertiesMatrix(:,1)];
    PhAsEPG = [   0;   seqPropertiesMatrix(:,3)];
    TRsEPG  = [  15;   seqPropertiesMatrix(:,2)];
    TEsEPG  = [   6;   seqPropertiesMatrix(:,4)];
    N = N + 1;
else
    FAsEPG  = seqPropertiesMatrix(:,1);
    PhAsEPG = seqPropertiesMatrix(:,3);
    TRsEPG  = seqPropertiesMatrix(:,2);
    TEsEPG  = seqPropertiesMatrix(:,4);
end

% % TISSUE properties
if toCreateMaterialProp == 0
    fprintf('\n-----> Material properties retrieved from code.\n');
    materialProperties = retrieveMaterialProperties(0, 0, struct);
else
    fprintf('\n-----> Custom chosen material properties.\n');
    materialProperties = struct;
    materialProperties.T1     = 500:100:1600;
    %[500, 1078, 496, 1422, 1007, 666, 334, 1576, 1153, 831, 516, 1407, 1615];
    %[20: 5:2000]; %, 3250:250:5000]; %[700:50:900 795]; %[400, 800, 1300, 3800]; %300:10:2000; %[700:50:900 795]; %
    materialProperties.T2     = [50:10:100, 200:100:700];
    %[360, 212, 151, 190, 128, 129, 70, 161, 156, 149, 49, 171, 373];
    %[10: 5: 400]; %,  350: 50:2200]; %[ 85 60:10:100];  %[ 70, 80, 110, 1400]; %50: 5: 300; %[  85 60:10:100]; %
    materialProperties.offRes =   0; 
end

% % FLAG DEPHASE
if flagDephase == 1
    fprintf('\n-----> FISP Sequence.\n');
else
    fprintf('\n-----> bSSFP Sequence.\n');
end

% Retrieve set of material properties in a (3xM) form
[materialTuples, M] = createSetOfTuples(materialProperties);
materialTuples = materialTuples(1:2, :);

% Repeat the sequence
if isRepeatSequence == 1
    % Save the block that you want to repeat
    FAsEPGBlock  = FAsEPG;
    PhAsEPGBlock = PhAsEPG;
    TRsEPGBlock  = TRsEPG;
    TEsEPGBlock  = TEsEPG;
    
    N_Block = size(TRsEPGBlock, 1); % Number per block
    
    % Repeat it
    N_FingerprintRep = 3;
    FAsEPG = []; PhAsEPG = []; TRsEPG = []; TEsEPG = [];
    for i = 1:N_FingerprintRep
        % 2.1 Repeat sequence parameters
        FAsEPG   = [ FAsEPG;   FAsEPGBlock];
        PhAsEPG  = [ PhAsEPG;  PhAsEPGBlock];
        TRsEPG   = [ TRsEPG;   TRsEPGBlock];
        TEsEPG   = [ TEsEPG;   TEsEPGBlock];

        % 2.2 Recalculate N
        N = size(TRsEPG,1);
        
        % 2.3 The relaxation interval Td is added to the last block
        Td = 5000;
        fprintf('\n-----> Adding relaxation time T_d\n');
        TRsEPG(N) = TRsEPG(N) + Td;

    end
    
end

%%
% EPG - MRF-FISP
if isRepeatSequence == 1
    F0states = zeros(M, N_Block);
else
    F0states = zeros(M, N);
end

tic;
percDone = 5;
fprintf('\n\n\n-----> Starting dictionary creation\n');

if toCreate == 1
    for i = 1:M
        T1 = materialTuples(1,i);
        T2 = materialTuples(2,i);

        [ChiF, ChiZ] = epgMatrix(N, PhAsEPG, FAsEPG, TRsEPG, ...
                                   T1, ...    % T_1
                                   T2, ...    % T_2
                                   1, flagDephase);     % M0, flagDephase
       
        % If we repeated the sequence keep only the last fingerprint 
        if isRepeatSequence == 1
            F0states(i, :) = ChiF(N, N-N/N_FingerprintRep+1:N) .* ...
                           exp(-TEsEPG(N-N/N_FingerprintRep+1:N) ./ T2).';
        else
            F0states(i, :) = ChiF(N, :) .* exp(-TEsEPG ./ T2).';
        end

        if (mod(i, ceil(M/20)) == 0)
            t = toc;
            fprintf(' --- %3d %% done in %3.2f seconds ... \n', ...
                    percDone, t);
            percDone = percDone + 5;
        end
    end
    t = toc;
    
    disp(['Dictionary generation with EPG took ', num2str(t), ' s']);
    N_T1 = size(unique(materialTuples(1,:)),2); 
    N_T2 = size(unique(materialTuples(2,:)),2);
    readme = ['file=', filenameDict, ...
             ' N=',num2str(N),' M=', num2str(M), ...
             ' N_T1=',num2str(N_T1), ...
             ' N_T2=',num2str(N_T2), ...
             ' IR=',num2str(flagIR),' dephase=',num2str(flagDephase)];
    if toSave == 1
        save(filenameDict, 'F0states', 'materialTuples', ...
                           'FAsEPG', 'PhAsEPG', ...
                           'TRsEPG', 'TEsEPG', 'readme');     
        fprintf('\n\n\n-----> Dictionary saved to %s \n', filenameDict);
    end
else
    disp('Set _toCreate_ flag to 1');
end







%% Reproducing plots from FIGURE 5 from MRF-FISP paper
if toPlotSomePlots == 1
    NtoPlot = 100;
    
    % Normalize signals
    F0statesReIm = [real(F0states) imag(F0states)];
    [~, F0Norm] = normalizeSignals(F0statesReIm);
    
    % Find the indices for the plots 
    % See figure 5 from MRF-FISP paper, Jiang et al, 2015, MRM
    indicesPlot1 = find((materialTuples(2,:) ==  85) & ...
                        (materialTuples(1,:) ~= 795));
    indicesPlot2 = find((materialTuples(1,:) == 795) & ...
                        (materialTuples(2,:) ~=  85));

	% Plot the figures
    figure('Position', [10,10,800,1000])
    % First subplot: T1 = 700->900, T2 =  85
    subplot(2,1,1)
    plot(0:NtoPlot-1, abs(F0Norm(indicesPlot1,2:NtoPlot+1))); 
    xlabel('Number of images');
    ylabel('Signal Intensity');
    legend('T_1 = 700ms', 'T_1 = 750ms', 'T_1 = 800ms', 'T_1 = 850ms', ...
        'T_1 = 900ms', 'Location','northwest','Orientation','horizontal');
    title('T_1 from range, T_2 = 85ms');
        
    % Second subplot: T1 = 795, T2 = 60->100
    subplot(2,1,2)
    plot(0:NtoPlot-1, abs(F0Norm(indicesPlot2,2:NtoPlot+1))); 
    xlabel('Number of images');
    ylabel('Signal Intensity');
    legend('T_2 = 60ms', 'T_2 = 70ms', 'T_2 = 80ms', 'T_2 = 90ms', ...
        'T_2 = 100ms', 'Location','northwest','Orientation','horizontal');
    title('T_1 = 795ms T_2 = from range');

end







