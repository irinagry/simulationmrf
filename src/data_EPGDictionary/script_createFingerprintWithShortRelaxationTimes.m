% IRINA GRIGORESCU
% Date created: 13-09-2017
% Date updated: 13-09-2017
%
% This script is to create an MRF database of fingerprints based on the
% idea of MRF with short relaxation intervals

% Based on MRF with Short Relaxation Intervals by Anthor, Doneva
% and the sequence parameters given by Maryia
% 
% I have decided to take 
% N repetiotions = 3 for Tdelay = 5000ms
% Because in this case for the largest T1,T2 pairs 
% the signal difference between the 5th repetition fingerprint and the
% previous one is of the order of 10^-3
% Of course, for smaller T1s the difference goes lower.
% 

% Preprocessing steps
clear all;
close all;
clc;

% Add needed paths:
run('runForPaths');
%%
% % % % % % % % 
% % % % % % % % SEQUENCE PROPERTIES
% % % % % % % % 
% % 
% % From a CSV File
% % 

% 1. The sequence parameters are extracted from the CSV file
fileCSV = 'sequenceParametersMaryia.csv';
fprintf(['\n-----> Getting sequence properties from file: ', ...
         '%s\n'], fileCSV);
seqPropertiesMatrix = ...
      csvread(fileCSV, 1);
Nro = size(seqPropertiesMatrix, 1);
% 1.1 Add the inversion pulse
fprintf('\n-----> Adding inversion pulse\n');
FAsEPGBlock  = [ 180;   seqPropertiesMatrix(:,1)];
PhAsEPGBlock = [   0;   seqPropertiesMatrix(:,3)];
TRsEPGBlock  = [  15;   seqPropertiesMatrix(:,2)];
TEsEPGBlock  = [   6;   seqPropertiesMatrix(:,4)];
N = size(TRsEPGBlock,1);

% 2. Repeat this for a couple of times
N_FingerprintRep = 10;
FAsEPG = []; PhAsEPG = []; TRsEPG = []; TEsEPG = [];
for i = 1:N_FingerprintRep
    % 2.1 Repeat sequence parameters
    FAsEPG   = [ FAsEPG;   FAsEPGBlock];
    PhAsEPG  = [ PhAsEPG;  PhAsEPGBlock];
    TRsEPG   = [ TRsEPG;   TRsEPGBlock];
    TEsEPG   = [ TEsEPG;   TEsEPGBlock];

    % 2.2 Recalculate N
    N = size(TRsEPG,1);
    
    % 2.3 The relaxation interval Td is added to the last block
    Td = 5000;
    fprintf('\n-----> Adding relaxation time T_d\n');
    TRsEPG(N) = TRsEPG(N) + Td;
   
end

% figure
% plot(((0:N-1).*TRsEPG.'), FAsEPG)
% xlabel('time (ms)'); ylabel('FA (deg)');

%%
% % TISSUE properties
materialProperties = struct;
materialProperties.T1     = 5000;
materialProperties.T2     = 3000;
materialProperties.offRes =    0; 
% Retrieve set of material properties in a (3xM) form
[materialTuples, M] = createSetOfTuples(materialProperties);
materialTuples = materialTuples(1:2, :);


% % FLAG DEPHASE
flagDephase = 1;

%%
% EPG - MRF-FISP
F0states = zeros(M, N);

tic;
percDone = 5;
fprintf('\n\n\n-----> Starting dictionary creation (FISP)\n');

% For each material
for i = 1:M
    T1 = materialTuples(1,i);
    T2 = materialTuples(2,i);

    [ChiF, ChiZ] = epgMatrix(N, PhAsEPG, FAsEPG, TRsEPG, ...
                               T1, ...    % T_1
                               T2, ...    % T_2
                               1, flagDephase);     % M0, flagDephase

    F0states(i, :) = ChiF(N, :) .* exp(-TEsEPG ./ T2).';


end
t = toc;
disp(['Dictionary generation with EPG took ', num2str(t), ' s']);

% Store each of them individually:
fingerprints = zeros(Nro+1, N_FingerprintRep);

for i = 1:N_FingerprintRep
    fingerprints(:,i) = F0states( (1:Nro+1) + (i-1).*(Nro+1) ); 
end

%%
figure
for i = 1:N_FingerprintRep
    
    subplot(2,1,1)
    plot(2:(Nro+1), abs(fingerprints(2:Nro+1,i))); hold on
    ylabel('signal (a.u.)');
    xlabel('step number');
    title(['Repetition number: ', num2str(i)]);
    
    subplot(2,1,2)
    if i > 1
        plot(2:(Nro+1), abs(fingerprints(2:Nro+1,i-1)) - ...
                        abs(fingerprints(2:Nro+1,i)));
    end
    ylabel('previous - current');
    xlabel('step number');
    
    pause
end




