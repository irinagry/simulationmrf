function [alpha1, alpha2, n_t] = func_calcAlphas(Nmatrix, dr, Td)
% % Irina Grigorescu
% % Function that calculates alpha1, alpha2 and number of readout points
% % based on the field of view, the required delta x = delta y = delta r
% % and the duration of the readout
% % Example: 
% For  96x 96 matrix: Nmatrix= 96; dr=1.25; Td=6; [a1,a2,nt]=func_calcAlphas(Nmatrix, dr, Td)
% For 128x128 matrix: Nmatrix=128; dr=1.25; Td=6; [a1,a2,nt]=func_calcAlphas(Nmatrix, dr, Td)
% For 128x128 matrix: Nmatrix=128; dr=1;    Td=6; [a1,a2,nt]=func_calcAlphas(Nmatrix, dr, Td)

% Calculates field of view for that Nmatrix size
L = Nmatrix * dr;
% Calculates number of points in the radial direction
n_r     = ceil(L / (2 * dr))
% Calculates number of points in the theta direction
n_theta = ceil(pi * L / dr)
% Calculates dwell time (time in between 2 samples)
deltaT  = Td / (n_r * n_theta);

% Calculates spiral parameters
alpha1 = pi / (Td * dr);
alpha2 = 2 * pi / (n_theta * deltaT);

% Calculates total number of readout points
n_t = n_r * n_theta;

end

