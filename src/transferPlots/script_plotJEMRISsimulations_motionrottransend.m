% 
% Irina Grigorescu
% Date created: 17-03-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps
% 
% NO MOTION CASE FOR FULLY SAMPLED SPIRAL


% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% For new colormaps
addpath(genpath('/Users/irina/Tools/MatlabStuff/Colormaps/'));
% % Load new colormaps: 
m = 500;
cm_magma = magma(m);
cm_inferno = inferno(m);
cm_plasma = plasma(m);
cm_viridis = viridis(m);

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
% COMMON ROOT FOLDER 
commonRoot = '~/OneDrive - University College London/Work/JEMRISSimulations/'; 

PATHFOLDER = ['~/OneDrive - University College London/', ...
                'Work/Simulation/simulationMRF/']; 

% DICTIONARY FOLDER            
dictionaryFolder = [PATHFOLDER, 'src/transferPlots/dictionaries/'];
% NO MOTION RECONSTRUCTIONS
reconstructionsFolder = [PATHFOLDER, 'src/transferPlots/reconstructions/'];

% % % % % % % % % % % % % % % % 
% % % % % % % % % % LOAD DATA
% % % % % % % % % % % % % % % % 

% % % % % % DICTIONARY
dictionary = load([ dictionaryFolder, 'dictionaryMore.mat' ]);
NTRs = dictionary.NTRs;
M    = dictionary.M;
Nx = 96; Ny = Nx;
dictionaryMRFNorm    = dictionary.dictionaryMRFNorm;
dictionaryMRFNorm2Ch = dictionary.dictionaryMRFNorm2Ch;
materialTuples = dictionary.materialTuples;
clear dictionary

% % % % % % IMAGE RECONSTRUCTIONS
imagesRec = load([ reconstructionsFolder, 'recMotionrottrans_end.mat' ]);

% Normalize the reconstructed images
[~, imagesBART_motrottrans] = normalizeSignals( ...
                    [ real(reshape(imagesRec.imagesBART_motrottrans, ...
                                   [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesRec.imagesBART_motrottrans, ...
                                   [Nx*Ny, NTRs])) ]);
imagesBART_motrottrans = (reshape(imagesBART_motrottrans, [Nx, Ny, NTRs]));
clear imagesRec

% %
% % % % Create GROUND TRUTH MAPS

% For the 96x96 phantom with 4145 spins
centres = [41,27 ; 56,27 ; ...
           27,41 ; 41,41 ; 56,41 ; 70,41 ; ...
           27,56 ; 41,56 ; 56,56 ; 70,56 ; ...
           41,69 ; 56,69 ; ...
           49,49 ]; % big circle
radii   = [zeros(1,12) + 4 , 32];

realMap = struct;
myPhantomT1Values = [500, 225, 335, 330, 1010, 1615, 675, ...
                     665, 840, 830, 1150, 1420, 1580];
myPhantomT2Values = [360,  50,  70, 110,  130,  370,  90, ...
                     130, 110, 150,  155,  190,  160];

[realMap.T1map, realMap.T2map, maskAllCircles, maskHollow] = ...
         func_createRealT1T2Maps(myPhantomT1Values, myPhantomT2Values, ...
                                 Nx, Ny, centres, radii);



% % DICTIONARY MATCHING FOR NO MOTION
imagesBARTReshaped_nomot = [ real(reshape(imagesBART_motrottrans, [Nx*Ny, NTRs])) ...
                             imag(reshape(imagesBART_motrottrans, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped_nomot);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB_nomot = matTuplesMatchedB1;
resultsB_nomot.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                         valuesOfMatchB3, valuesOfMatchB4];
resultsB_nomot.scoremap = reshape(resultsB_nomot.score, [Nx, Ny]);                      
resultsB_nomot.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB_nomot.indicesmap = reshape(resultsB_nomot.indices, [Nx, Ny]); 
resultsB_nomot.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB_nomot.T1map = reshape(resultsB_nomot.T1, [Nx, Ny]);
resultsB_nomot.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB_nomot.T2map = reshape(resultsB_nomot.T2, [Nx, Ny]);                
   
% % % % Save results
mapsFolder = [PATHFOLDER, 'src/transferPlots/maps/'];
save([mapsFolder, 'mapsMotionrottrans_end.mat'], 'resultsB_nomot');
return


%% % % % % Reconstructed maps of T1 and T2 and the real (ground truth) maps
padv = 0;

figure('units','normalized','outerposition',[0 0 1 1])
% T1 real
subplot(2,3,1)
imagesc(padarray((realMap.T1map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(padarray((realMap.T2map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(padarray((resultsB_nomot.T1map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Reconstructed')

% BART - T2
subplot(2,3,5)
imagesc(padarray((resultsB_nomot.T2map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Reconstructed')

% BART - Score
subplot(2,3,6)   
imagesc(padarray((resultsB_nomot.scoremap.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0 1])
axis off
title('Matching Scores')


pause(2)

% % % Save figure
savefig([reconstructionsFolder, 'reconstructions_MotionRotTransEnd.fig']);


%% % % Compare nomotion vs motionrotbeg - only works if the nomotion map exists
% you need to run this first: script_plotJEMRISsimulations_nomotion
% and set resultsB_NOMOTIONMAPS=resultsB_nomot;
dothis = 1; % set to 0 if otherwise

if dothis
    figure('units','normalized','outerposition',[0 0 1 1])
    
    subplot(1,2,1)
    imagesc((resultsB_nomot.T1map - resultsB_NOMOTIONMAPS.T1map).*maskAllCircles)
    colorbar, axis square
    colormap(cm_magma) 
    axis off
    title('T_1 map (motion corrupted) - T_1 map (motion free)')
    caxis([-2000 2000])
    
    subplot(1,2,2)
    imagesc((resultsB_nomot.T2map - resultsB_NOMOTIONMAPS.T2map).*maskAllCircles)
    colorbar, axis square
    colormap(cm_magma) 
    axis off
    title('T_2 map (motion corrupted) - T_2 map (motion free)')
    caxis([-400 400])
    
end

savefig([reconstructionsFolder, 'comparison_motionfreeVSmotionrottransend.fig']);
pause(2)
saveas(gcf,'~/Desktop/comparison_motionfreeVSmotionrottransend','png')

%% % % Mean Values for each block
% % T1
[ meanValuesReal.T1 , meanValuesSimu.T1, stdValuesSimu.T1 ] = ...
    func_retrieveMeanValueInROI(realMap.T1map, resultsB_nomot.T1map, ...
                            centres, [radii(1:end-1), 4], 0);
                            %centres(1:end-1,:), radii(1:end-1)-1, 0);
% % T2
[ meanValuesReal.T2 , meanValuesSimu.T2, stdValuesSimu.T2 ] = ...
    func_retrieveMeanValueInROI(realMap.T2map, resultsB_nomot.T2map, ...
                            centres, [radii(1:end-1), 4], 0);
                            %centres(1:end-1,:), radii(1:end-1)-1, 0);


% % Calculate R-squared for T1s and T2s
rsqT1 = fitlm(meanValuesReal.T1, meanValuesSimu.T1);
rsqT2 = fitlm(meanValuesReal.T2, meanValuesSimu.T2);

% % Plot real vs simulated values
                        
figure('units','normalized','outerposition',[0 0 1 1])

% T1 real vs T1 reconstructions
subplot(1,2,1); hold on
% Identity line
il = plot(0:max(meanValuesReal.T1)+250, ...
          0:max(meanValuesReal.T1)+250);
il.Color = [0,0,0,0.1];
% Fitted line
plot( (min(meanValuesReal.T1):max(meanValuesReal.T1)), ...
       rsqT1.Coefficients.Estimate(1) + ...
       rsqT1.Coefficients.Estimate(2).*(min(meanValuesReal.T1):max(meanValuesReal.T1)), 'k');
% Real vs simulated values
errorbar(meanValuesReal.T1 , meanValuesSimu.T1, stdValuesSimu.T1, ...
                             'o', 'MarkerFaceColor', 'red');
% Rsq statistics
text(200, 1600, ['R^2 = ', num2str(round(rsqT1.Rsquared.Adjusted,4))])
t1coef2 = rsqT1.Coefficients.Estimate(2);
t1coef1 = rsqT1.Coefficients.Estimate(1);
if t1coef1 < 0
    text(200, 1700, ['y = ', num2str(t1coef2,4), ...
                         'x - ', ...
                         num2str(abs(round(t1coef1,4)))]);
else
    text(200, 1700, ['y = ', num2str(t1coef2,4), ...
                         'x + ', ...
                         num2str(abs(round(t1coef1,4)))]);
end
axis equal, axis square
xlim([0 max(meanValuesReal.T1)+250])
ylim([0 max(meanValuesReal.T1)+250])
xlabel('Real T_1 (ms)'); ylabel('Simulated T_1 (ms)');
title('Ground truth T_1 vs simulated T_1')


% T2 real vs T2 reconstructions
subplot(1,2,2); hold on
% Identity line
il = plot(0:max(meanValuesReal.T2)+20, ...
          0:max(meanValuesReal.T2)+20);
il.Color = [0,0,0,0.1];
% Fitted line
plot( (min(meanValuesReal.T2):max(meanValuesReal.T2)), ...
       rsqT2.Coefficients.Estimate(1) + ...
       rsqT2.Coefficients.Estimate(2).*(min(meanValuesReal.T2):max(meanValuesReal.T2)), 'k');
% Real vs simulated values
errorbar(meanValuesReal.T2 , meanValuesSimu.T2, stdValuesSimu.T2, ...
                             'o', 'MarkerFaceColor', 'red');
% Rsq statistics
text(50, 325, ['R^2 = ', num2str(rsqT2.Rsquared.Adjusted)])
t2coef2 = rsqT2.Coefficients.Estimate(2);
t2coef1 = rsqT2.Coefficients.Estimate(1);
if t2coef1 < 0
    text(50, 350, ['y = ', num2str(t2coef2,4), ...
                         'x - ', ...
                         num2str(abs(round(t2coef1,4)))]);
else
    text(50, 350, ['y = ', num2str(t2coef2,4), ...
                         'x + ', ...
                         num2str(abs(round(t2coef1,4)))]);
end
axis equal, axis square
xlim([0 max(meanValuesReal.T2)+20])
ylim([0 max(meanValuesReal.T2)+20])
xlabel('Real T_2 (ms)'); ylabel('Simulated T_2 (ms)');
title('Ground truth T_2 vs simulated T_2')

rsqT1.Coefficients
rsqT2.Coefficients

pause(2)

% % % Save figure
savefig([reconstructionsFolder, 'avgROIsimVSGT_MotionRotTransEnd.fig']);

save(['meanValues/', 'meanValsMotRotTransEnd.mat'], ...
      'meanValuesReal' , 'meanValuesSimu', 'stdValuesSimu');

%% Have a look at individual signals NO MOTION

imagesBARTReshaped1Ch_nomot = reshape(imagesBART_motrottrans, [Nx*Ny, NTRs ]);

% % % Get the signals from the nomotion case
load([reconstructionsFolder, 'signalsNOMotion.mat'], 'nomotionSignals');

% Scores for the 96x96 phantom with 4145 spins
% MOTION rottransATION EndINNING
scoreIdxB = [(41-1)*Nx+49, ... %big circle
             (59-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (53-1)*Nx+69, ... %c12
             (42-1)*Nx+27, ... %c1
             (43-1)*Nx+39];    %c4
                       
scoreValB = resultsB_nomot.score(scoreIdxB);
% Titles
titles = {'bigCircle', 'c5', 'c8', 'c12', 'c1', 'c4'};

NTRStart = 350;    %350
NTREnd   = NTRs; %NTRs;
for idxTR = 1:size(scoreIdxB,2)
    
    figure('Position',[1,1,700,1200])
    
    % The voxel we are looking for
    subplot(2,2,1)
    llPhantom = squeeze(abs(imagesBART_motrottrans(:,:,1)));
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray
    
    % ABS
    subplot(2,2,[3,4]); hold on
    % Plot image signal
    p1 = plot(NTRStart:NTREnd, abs(imagesBARTReshaped1Ch_nomot(...
                 scoreIdxB(idxTR), NTRStart:NTREnd)), ...
          '-', 'LineWidth', 1.2);
	p1.Color = [0.8500    0.3250    0.0980];
    xlabel('#image'); ylabel('normalised signal intensity (a.u.)');
    title(['BART (abs) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
    % Plot dictionary signal
    p2 = plot(NTRStart:NTREnd, ...
         abs(dictionaryMRFNorm( ...
             resultsB_nomot.indices(scoreIdxB(idxTR)), NTRStart:NTREnd)), ...
         '--', 'LineWidth', 1.4);
    p2.Color = [0.3010    0.7450    0.9330];
    % Plot original signal NO MOTION
    p3 = plot( NTRStart:NTREnd, ...
               abs(nomotionSignals.(titles{idxTR}).imgSignal(NTRStart:NTREnd)), ...
          'k-.', 'LineWidth', 0.8);
    % Plot original dictionary match NO MOTION
    p4 = plot( NTRStart:NTREnd, ...
               abs(nomotionSignals.(titles{idxTR}).dictMatch(NTRStart:NTREnd)), ...
          '--.', 'LineWidth', 0.8);
	p4.Color = [0.2 0.8 0.2];
      
      
	title({['BART (abs) ',  titles{idxTR}, ...
            ' Score = ', num2str(scoreValB(idxTR))], ...
   ['Matched to: T_1 = ', num2str(resultsB_nomot.T1(scoreIdxB(idxTR))), ...
           ' T_2 = ', num2str(resultsB_nomot.T2(scoreIdxB(idxTR)))], ...
   ['Should match to: T_1 = ', num2str(nomotionSignals.(titles{idxTR}).T1matched), ...
           ' T_2 = ', num2str(nomotionSignals.(titles{idxTR}).T2matched)]})
      
	legend('motion signal',    'motion dictionary match', ...
           'no motion signal', 'no motion dictionary match')
    
    
    pause(2)

    % % % Save figure
    savefig([reconstructionsFolder, ...
             'sigCompare_', titles{idxTR}, '_MotionRotTransEnd.fig']); %AllTRs
         
	pause(2)
    

end


