% % IRINA GRIGORESCU
% % This script inspects the dictionaries generated for the transfer

PATHFOLDER = ['~/OneDrive - University College London/', ...
                'Work/Simulation/simulationMRF/'];

% Addpaths for EPG 
addpath(genpath([PATHFOLDER, 'src/extendedPhaseGraph/']));

% Addpaths for helpers
addpath(genpath([PATHFOLDER, 'src/helpers/']));

% Addpaths for preprocess
addpath(genpath([PATHFOLDER, 'src/preprocess/']));

% Addpaths for algs
addpath(genpath([PATHFOLDER, 'src/algs/']));

% Addpaths for exchanged to compare with analytical signal
addpath(genpath([PATHFOLDER, 'src/exchanged/']));


load('dictionaries/dictionaryAll.mat', 'M', 'NTRs', ...
                'dictionaryMRFNorm', 'dictionaryMRFNorm2Ch', ...
                'materialTuples')

T1_mrf = sort(unique(materialTuples(1,:)));
T2_mrf = sort(unique(materialTuples(2,:)));

%% Make video if flag set to 1
videoFlag = struct;
videoFlag.flag = 1;
videoFlag.nameVideo = 'CorrCoefMapT2Fixed';


% SET VIDEO PROPERTIES
if videoFlag.flag == 1
    % Make a frame by frame movie
    % need to set this up
    videoClass = VideoWriter([videoFlag.nameVideo, '.mp4'], ...
                             'MPEG-4'); % #need this for video

    frameRate = 5;                         % optional
    videoClass.set('FrameRate',frameRate); % optional
    open(videoClass)                       % #need this for video
end
            
% START PLOTTING
figHand = figure('units','normalized','outerposition',[0 0 1 1]);
pause(1)
for i = 1:length(T2_mrf)
    
    % % % % T2 fixed, T1 ranging
    % Fix T2 and range T1 
    T2indx = find(materialTuples(2,:) == T2_mrf(i));
    T1indx = find(materialTuples(1,:) >  400); % 400
    T1rangeT2fixindx = intersect(T1indx,T2indx);
    corrMatrix2 = dictionaryMRFNorm(T1rangeT2fixindx,:) * ...
                  dictionaryMRFNorm(T1rangeT2fixindx,:)';
    % Plot
    subplot(1,2,1)
    s = surf( corrMatrix2 ); %hold on
    s.EdgeColor = 'none';
    ax = gca;
    ax.XTick = 1:40:length(T1_mrf)-60; ax.XTickLabel = T1_mrf(61:40:end);
    ax.YTick = 1:40:length(T1_mrf)-60; ax.YTickLabel = T1_mrf(61:40:end);
    xlabel('T_1(ms)'); ylabel('T_1(ms)'); 
    caxis([0.5 1]); 
    xlim([0 320]);
    ylim([0 320]);
    zlim([0.5 1]);
    axis square;
    view(-25, 10)
    title(['T_2 = ', num2str(T2_mrf(i)), 'ms'])
    
    % Plot
    subplot(1,2,2)
    s = surf( corrMatrix2 ); %hold on
    colorbar
    s.EdgeColor = 'none';
    ax = gca;
    ax.XTick = 1:40:length(T1_mrf)-60; ax.XTickLabel = T1_mrf(61:40:end);
    ax.YTick = 1:40:length(T1_mrf)-60; ax.YTickLabel = T1_mrf(61:40:end);
    xlabel('T_1(ms)'); ylabel('T_1(ms)'); 
    caxis([0.5 1]); 
    xlim([0 321]);
    ylim([0 321]);
    zlim([0.5 1]);
    axis square; 
    view(0, 90)

    title(['T_2 = ', num2str(T2_mrf(i)), 'ms'])
    pause(0.1)
    
    % % Save into video if flag set to 1
    if videoFlag.flag == 1
        frame = getframe(figHand);                 % #need this for video
        writeVideo(videoClass,frame)               % #need this for video
    end
    
end

pause(3)

% % Close video handle if video flag set to 1
if videoFlag.flag == 1
    close(videoClass)                              % #need this for video
end

close all
pause(2)

%% Make video if flag set to 1
videoFlag = struct;
videoFlag.flag = 0;
videoFlag.nameVideo = 'CorrCoefMapT1Fixed';


% SET VIDEO PROPERTIES
if videoFlag.flag == 1
    % Make a frame by frame movie
    % need to set this up
    videoClass = VideoWriter([videoFlag.nameVideo, '.mp4'], ...
                             'MPEG-4'); % #need this for video

    frameRate = 5;                         % optional
    videoClass.set('FrameRate',frameRate); % optional
    open(videoClass)                       % #need this for video
end
            
% START PLOTTING
figHand = figure('units','normalized','outerposition',[0 0 1 1]);
pause(1)

for i = length(T1_mrf):-5:61
    % % % % T1 fixed, T2 ranging

    % Fix T1 and range T2 
    T1indx = find(materialTuples(1,:) == T1_mrf(i));
    T2indx = 1:M;
    T2rangeT1fixindx = intersect(T1indx,T2indx);
    corrMatrix2 = dictionaryMRFNorm(T2rangeT1fixindx,:) * ...
                  dictionaryMRFNorm(T2rangeT1fixindx,:)';
    % Plot
    subplot(1,2,1)
    s = surf(  corrMatrix2 );% hold on
    s.EdgeColor = 'none';
    ax = gca;
    ax.XTick = 1:8:length(T2_mrf); ax.XTickLabel = T2_mrf(1:8:end);
    ax.YTick = 1:8:length(T2_mrf); ax.YTickLabel = T2_mrf(1:8:end);
    xlabel('T_2(ms)'); ylabel('T_2(ms)'); 
    caxis([0.5 1]); 
    xlim([0 57]);
    ylim([0 57]);
    zlim([0.5 1]);
    axis square
    view(-25, 10);

    title(['T_1 = ', num2str(T1_mrf(i)), 'ms'])
    
    % Plot
    subplot(1,2,2)
    s = surf(  corrMatrix2 );% hold on
    colorbar
    s.EdgeColor = 'none';
    ax = gca;
    ax.XTick = 1:8:length(T2_mrf); ax.XTickLabel = T2_mrf(1:8:end);
    ax.YTick = 1:8:length(T2_mrf); ax.YTickLabel = T2_mrf(1:8:end);
    xlabel('T_2(ms)'); ylabel('T_2(ms)'); 
    caxis([0.5 1]); 
    xlim([0 58]);
    ylim([0 58]);
    zlim([0.5 1]);
    axis square
    view(0,90);

    title(['T_1 = ', num2str(T1_mrf(i)), 'ms'])
    
    pause(0.1)
    
    % % Save into video if flag set to 1
    if videoFlag.flag == 1
        frame = getframe(figHand);                 % #need this for video
        writeVideo(videoClass,frame)               % #need this for video
    end
    
end

% % Close video handle if video flag set to 1
if videoFlag.flag == 1
    close(videoClass)                              % #need this for video
end

