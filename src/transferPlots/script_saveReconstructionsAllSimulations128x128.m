% % % IRINA GRIGORESCU
% % % This script reconstructs all the simulations so far and 
% % % stores them in .mat files
% % % Date created: 17-03-2018


% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
% COMMON ROOT FOLDER 
commonRoot = '~/jemrisSims/march2018/'; 

PATHFOLDER = ['~/OneDrive - University College London/', ...
                'Work/Simulation/simulationMRF/'];

 
% SEQUENCE PARAMETERS
folderNameSeq      = [commonRoot, 'nomot_500TRs/']; %~/jemrisSims/

% NO MOTION FOLDER
folderNameNOMOTION = [commonRoot, 'nomot_500TRs/']; %~/jemrisSims/

% MOTION FOLDERS rotations
folderNameMOTIONrotbeg   = [commonRoot, 'mot_rot_beg/']; %~/jemrisSims
folderNameMOTIONrotmid   = [commonRoot, 'mot_rot_mid/']; 
folderNameMOTIONrotend   = [commonRoot, 'mot_rot_end/']; 

% MOTION FOLDERS translations
folderNameMOTIONtransbeg   = [commonRoot, 'mot_trans_beg/']; %~/jemrisSims
folderNameMOTIONtransmid   = [commonRoot, 'mot_trans_mid/']; 
folderNameMOTIONtransend   = [commonRoot, 'mot_trans_end/']; 

% MOTION FOLDERS rotations-translations
folderNameMOTIONrottransbeg   = [commonRoot, 'mot_rottrans_beg/']; %~/jemrisSims
folderNameMOTIONrottransmid   = [commonRoot, 'mot_rottrans_mid/']; 
folderNameMOTIONrottransend   = [commonRoot, 'mot_rottrans_end/']; 
 
%%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % K-space coordinates
% % % % % PROPERTIES OF THE SEQUENCE
% Set number of TR blocks
NTRs = 500; 
% Set effective image size
Nx   = 128; Ny = Nx; 
% Set number of redout points per TR
nROPoints = 25792; 


% %
% % % % % PROPERTIES OF THE SPIRAL TAKEN FROM JEMRIS

% Get sequence values
RXP = h5read([folderNameSeq,'seq.h5'],'/seqdiag/RXP');  % Read-Out (-1 no RO / 0 RO)
KX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KX');   % KX (rad/mm)
KY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KY');   % KY (rad/mm)
KZ  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/KZ');   % KZ (rad/mm)
GX  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GX');   % GX
GY  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/GY');   % GY
FA  = h5read([folderNameSeq,'seq.h5'],'/seqdiag/TXM');  % FA
% Get k-space trajectory
KX = KX(RXP==0); KY = KY(RXP==0); KZ = KZ(RXP==0);

% Allocate memory for k-space coordinates in x,y,z directions
kspace_x = zeros(nROPoints, NTRs); % NROpoints x Nrepetitions x NTRs
kspace_y = zeros(nROPoints, NTRs);
kspace_z = zeros(nROPoints, 1);

% Rescale the spiral k-spaces so it falls onto a grid
% Plot them for each spiral turn
figure(111)
for idxTR = 1:NTRs
    
    % Populate the spiral k-spaces from jemris
    kspace_x(:,idxTR) = KX((idxTR-1)*nROPoints+1 : idxTR*nROPoints); 
    kspace_y(:,idxTR) = KY((idxTR-1)*nROPoints+1 : idxTR*nROPoints); 
    
    % % % % % % 
    % Plot the coordinates before scaling (taken from JEMRIS)
    if mod(idxTR,100) == 0 || idxTR == 1
        subplot(1,2,1); xl = 2;
        % Coordinates from JEMRIS
        scatter(kspace_x(:,idxTR), ...
                kspace_y(:,idxTR), '.');
        axis equal, axis square;
        xlabel('k_x mm^{-1}'); ylabel('k_y mm^{-1}');
        title(['Spiral ', num2str(idxTR), ' (JEMRIS)'])
        xlim([-xl xl]); ylim([-xl xl]);
    end
    
    % % % % % % 
    % Transform the kspace coordinates to the desired effective imaging matrix
    % by scaling the values so that they fit into the matrix
    dist_ksp = sqrt( (kspace_x(1,idxTR) - kspace_x(end,idxTR) ).^2 + ... 
                     (kspace_y(1,idxTR) - kspace_y(end,idxTR) ).^2 ) ;
	x0_ksp = kspace_x(1,idxTR); y0_ksp = kspace_y(1,idxTR);
    traj_spiral = [ kspace_x(:,idxTR) - x0_ksp, ...
                    kspace_y(:,idxTR) - y0_ksp, ...
                    kspace_z               ].';
    traj_spiral2 = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_spiral);
    kspace_x(:, idxTR) = traj_spiral2(1,:) + x0_ksp;
    kspace_y(:, idxTR) = traj_spiral2(2,:) + y0_ksp;
    
    % % % % % % 
    % Plot the coordinates after scaling
    if mod(idxTR,100) == 0 || idxTR == 1
        subplot(1,2,2)
        scatter(kspace_x(:, idxTR), kspace_y(:, idxTR),'.'), axis equal, axis square;
        xlabel('N_x'); ylabel('N_y');
        title(['Spiral ', num2str(idxTR), ' (JEMRIS scaled for reco)'])
        xlim([-Nx/2-1 Nx/2+1]); ylim([-Ny/2-1 Ny/2+1]);
    end

    
end



% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % NO MOTION
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation
% Allocate memory for kspace values
kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

% % % % % % Get signal values from JEMRIS simulation
filenameSignal = [folderNameNOMOTION, 'signals.h5']; 

% % % Read in the vector values
A = (h5read(filenameSignal, '/signal/channels/00'))';
t = h5read(filenameSignal, sprintf('/signal/times'));
[t, J] = sort(t); 
% % % Store the signal
Mvecs  = A(J,:);
 
% % % Get kspace as one signal
for idxTR = 1:NTRs
    % Get signal values
    kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
            sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
end

 
% Reshape kspace data to have all the readout points from all repetitions
kspaces = reshape(kspaces, [nROPoints, NTRs]);


% % 3. Reconstruct with BART
% Save all images
imagesBART_nomot = zeros(Nx, Ny, NTRs);

figure(115)
for idxTR = 1:NTRs
    tic
    disp(['Image number ', num2str(idxTR)]);
    % Retrieve current values for current reconstruction
    currentKspace_x     = kspace_x(:, idxTR);
    currentKspace_y     = kspace_y(:, idxTR);
    currentKspaceToReco = (kspaces(:, idxTR)).';
    
    % Prepare trajectory for current image
    traj_kspace = [ currentKspace_x, ...
                    currentKspace_y, ...
                    kspace_z               ].';

                
    % Reconstruct with BART
    igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
    igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
    imagesBART_nomot(:, :, idxTR) = igrid;
    toc

    % Plot images (ABS)
    if mod(idxTR, 25) == 0 || (idxTR == 1)
        subplot(1,3,1), imagesc(abs(igrid))
        title('BART reconstruction (abs)')
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,2), imagesc(real(igrid))
        title(['BART reconstruction (real) ', num2str(idxTR)])
        axis square; axis off;
        colormap gray;
        colorbar;

        subplot(1,3,3), imagesc(imag(igrid))
        title('BART reconstruction (imag)')
        axis square; axis off;
        colormap gray;
        colorbar;
    end
    
    
    pause(0.01)
    
    clear igrid traj_kspace
end


save([ PATHFOLDER, ...
       'src/transferPlots/reconstructions/', 'recNOMotion128x128.mat'], ...
       'imagesBART_nomot');


 
 
 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % MOTION ROTATIONS
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation

rotSimulations = { folderNameMOTIONrotbeg, ...
                   folderNameMOTIONrotmid, ...
                   folderNameMOTIONrotend };
               
filenamesRecs = {'rot_beg', 'rot_mid', 'rot_end'};

% Three types of rotations
for idxSim = 1:3
    
    % Allocate memory for kspace values
    kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

    % % % % % % Get signal values from JEMRIS simulation
    filenameSignal = [rotSimulations{idxSim}, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    t = h5read(filenameSignal, sprintf('/signal/times'));
    [t, J] = sort(t); 
    % % % Store the signal
    Mvecs  = A(J,:);

    % % % Get kspace as one signal
    for idxTR = 1:NTRs
        % Get signal values
        kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
                sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
    end


    % Reshape kspace data to have all the readout points from all repetitions
    kspaces = reshape(kspaces, [nROPoints, NTRs]);


    % % 3. Reconstruct with BART
    % Save all images
    imagesBART_motrot = zeros(Nx, Ny, NTRs);

    figure(115)
    for idxTR = 1:NTRs
        tic
        disp(['Image number ', num2str(idxTR)]);
        disp(['Reconstructing from ', filenameSignal]);
        % Retrieve current values for current reconstruction
        currentKspace_x     = kspace_x(:, idxTR);
        currentKspace_y     = kspace_y(:, idxTR);
        currentKspaceToReco = (kspaces(:, idxTR)).';

        % Prepare trajectory for current image
        traj_kspace = [ currentKspace_x, ...
                        currentKspace_y, ...
                        kspace_z               ].';


        % Reconstruct with BART
        igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
        igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
        imagesBART_motrot(:, :, idxTR) = igrid;
        toc

        % Plot images (ABS)
        if mod(idxTR, 25) == 0 || (idxTR == 1)
            subplot(1,3,1), imagesc(abs(igrid))
            title('BART reconstruction (abs)')
            axis square; axis off;
            colormap gray;
            colorbar;

            subplot(1,3,2), imagesc(real(igrid))
            title(['BART reconstruction (real) ', num2str(idxTR)])
            axis square; axis off;
            colormap gray;
            colorbar;

            subplot(1,3,3), imagesc(imag(igrid))
            title('BART reconstruction (imag)')
            axis square; axis off;
            colormap gray;
            colorbar;
        end


        pause(0.01)

        clear igrid traj_kspace
    end

    % % % % Save reconstructions
    save([ PATHFOLDER, ...
          'src/transferPlots/reconstructions/', 'recMotion', ...
          filenamesRecs{idxSim}, '128x128.mat'], ...
         'imagesBART_motrot');
    
end




 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % MOTION TRANSLATIONS
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation

transSimulations = { folderNameMOTIONtransbeg, ...
                   folderNameMOTIONtransmid, ...
                   folderNameMOTIONtransend };
               
filenamesRecs = {'trans_beg', 'trans_mid', 'trans_end'};

% Three types of rotations
for idxSim = 1:3
    
    % Allocate memory for kspace values
    kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

    % % % % % % Get signal values from JEMRIS simulation
    filenameSignal = [transSimulations{idxSim}, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    t = h5read(filenameSignal, sprintf('/signal/times'));
    [t, J] = sort(t); 
    % % % Store the signal
    Mvecs  = A(J,:);

    % % % Get kspace as one signal
    for idxTR = 1:NTRs
        % Get signal values
        kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
                sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
    end


    % Reshape kspace data to have all the readout points from all repetitions
    kspaces = reshape(kspaces, [nROPoints, NTRs]);


    % % 3. Reconstruct with BART
    % Save all images
    imagesBART_mottrans = zeros(Nx, Ny, NTRs);

    figure(115)
    for idxTR = 1:NTRs
        tic
        disp(['Image number ', num2str(idxTR)]);
        disp(['Reconstructing from ', filenameSignal]);
        % Retrieve current values for current reconstruction
        currentKspace_x     = kspace_x(:, idxTR);
        currentKspace_y     = kspace_y(:, idxTR);
        currentKspaceToReco = (kspaces(:, idxTR)).';

        % Prepare trajectory for current image
        traj_kspace = [ currentKspace_x, ...
                        currentKspace_y, ...
                        kspace_z               ].';


        % Reconstruct with BART
        igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
        igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
        imagesBART_mottrans(:, :, idxTR) = igrid;
        toc

        % Plot images (ABS)
        if mod(idxTR, 25) == 0 || (idxTR == 1)
            subplot(1,3,1), imagesc(abs(igrid))
            title('BART reconstruction (abs)')
            axis square; axis off;
            colormap gray;
            colorbar;

            subplot(1,3,2), imagesc(real(igrid))
            title(['BART reconstruction (real) ', num2str(idxTR)])
            axis square; axis off;
            colormap gray;
            colorbar;

            subplot(1,3,3), imagesc(imag(igrid))
            title('BART reconstruction (imag)')
            axis square; axis off;
            colormap gray;
            colorbar;
        end


        pause(0.01)

        clear igrid traj_kspace
    end

    % % % % Save reconstructions
    save([ PATHFOLDER, ...
          'src/transferPlots/reconstructions/', 'recMotion', ...
          filenamesRecs{idxSim}, '128x128.mat'], ...
         'imagesBART_mottrans');
    
end


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % MOTION ROTATIONS-TRANSLATIONS
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% 2. Get the signal values from JEMRIS simulation

transSimulations = { folderNameMOTIONrottransbeg, ...
                     folderNameMOTIONrottransmid, ...
                     folderNameMOTIONrottransend };
               
filenamesRecs = {'rottrans_beg', 'rottrans_mid', 'rottrans_end'};

% Three types of rotations
for idxSim = 1:3
    
    % Allocate memory for kspace values
    kspaces = zeros(nROPoints, NTRs); % NROpoints x NTRs x Nrepetitions 

    % % % % % % Get signal values from JEMRIS simulation
    filenameSignal = [transSimulations{idxSim}, 'signals.h5']; 

    % % % Read in the vector values
    A = (h5read(filenameSignal, '/signal/channels/00'))';
    t = h5read(filenameSignal, sprintf('/signal/times'));
    [t, J] = sort(t); 
    % % % Store the signal
    Mvecs  = A(J,:);

    % % % Get kspace as one signal
    for idxTR = 1:NTRs
        % Get signal values
        kspaces(:,idxTR) = Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 1) + ...
                sqrt(-1) * Mvecs((idxTR-1)*nROPoints+1 : idxTR*nROPoints, 2);
    end


    % Reshape kspace data to have all the readout points from all repetitions
    kspaces = reshape(kspaces, [nROPoints, NTRs]);


    % % 3. Reconstruct with BART
    % Save all images
    imagesBART_motrottrans = zeros(Nx, Ny, NTRs);

    figure(115)
    for idxTR = 1:NTRs
        tic
        disp(['Image number ', num2str(idxTR)]);
        disp(['Reconstructing from ', filenameSignal]);
        % Retrieve current values for current reconstruction
        currentKspace_x     = kspace_x(:, idxTR);
        currentKspace_y     = kspace_y(:, idxTR);
        currentKspaceToReco = (kspaces(:, idxTR)).';

        % Prepare trajectory for current image
        traj_kspace = [ currentKspace_x, ...
                        currentKspace_y, ...
                        kspace_z               ].';


        % Reconstruct with BART
        igrid = bart('nufft -i -t', traj_kspace, currentKspaceToReco);
        igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Ny)], igrid);
        imagesBART_motrottrans(:, :, idxTR) = igrid;
        toc

        % Plot images (ABS)
        if mod(idxTR, 25) == 0 || (idxTR == 1)
            subplot(1,3,1), imagesc(abs(igrid))
            title('BART reconstruction (abs)')
            axis square; axis off;
            colormap gray;
            colorbar;

            subplot(1,3,2), imagesc(real(igrid))
            title(['BART reconstruction (real) ', num2str(idxTR)])
            axis square; axis off;
            colormap gray;
            colorbar;

            subplot(1,3,3), imagesc(imag(igrid))
            title('BART reconstruction (imag)')
            axis square; axis off;
            colormap gray;
            colorbar;
        end


        pause(0.01)

        clear igrid traj_kspace
    end

    % % % % Save reconstructions
    save([ PATHFOLDER, ...
          'src/transferPlots/reconstructions/', 'recMotion', ...
          filenamesRecs{idxSim}, '128x128.mat'], ...
         'imagesBART_motrottrans');
    
end
