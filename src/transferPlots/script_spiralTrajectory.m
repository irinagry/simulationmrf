% Irina Grigorescu
% This script is to show the spiral trajectory

% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')


% Taken from JEMRIS
kx = h5read('~/jemrisSims/march2018/nomot_500TRs/seq.h5','/seqdiag/KX');
ky = h5read('~/jemrisSims/march2018/nomot_500TRs/seq.h5','/seqdiag/KY');
rx = h5read('~/jemrisSims/march2018/nomot_500TRs/seq.h5','/seqdiag/RXP');
kx = kx(rx==0).'; kx = kx - kx(1);
ky = ky(rx==0).'; ky = ky - ky(1);
NTRs = 1;
NROs = size(kx,2) / NTRs; %126756
kx = kx(1:NROs);
ky = ky(1:NROs);

%%
% Calculated in MATLAB
Td = 6; %ms
dt = Td / NROs;
t = 0/2:dt:Td-dt;
spiralPower = 1;
spiralAmplitude =  0.5235; 
spiralFrequency = 67.0206;  

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);

% Plot here
figure

subplot(2,3,1)
plot(kx./(2*pi), ky./(2*pi),'.')
axis equal
title('JEMRIS')

subplot(2,3,2)
plot(k_x./(2*pi), k_y./(2*pi),'-'); hold on
plot(k_x./(2*pi), k_y./(2*pi),'.');
axis equal
title('MATLAB')

subplot(2,3,3)
plot(0.4 .* t .* cos(20 .* t) ./ (2*pi), ...
     0.4 .* t .* sin(20 .* t) ./ (2*pi),  '.')
axis equal

subplot(2,3,[4,6]); hold on
plot(1:NROs,k_x - kx)
plot(1:NROs,k_y - ky)
title('MATLAB-JEMRIS')
ylim([-0.1, 0.1])


A = (h5read('~/jemrisSims/feb2018/testSpiral/signals.h5', '/signal/channels/00'))';
tt = h5read('~/jemrisSims/feb2018/testSpiral/signals.h5', sprintf('/signal/times'));
[tt, J] = sort(tt); 
% % % Store the signal
Mvecs  = A(J,:);
 
% Signals
kspaces = Mvecs(1:NROs, 1) + sqrt(-1) * Mvecs(1:NROs, 2);

% Prepare trajectory for current image
traj_kspace = [ kx.', ...
                ky.', ...
                zeros(size(kx)).' ];


% Resize trajectory
Nx = 128;
dist_ksp = sqrt( (kx(1) - kx(NROs) ).^2 + ... 
                 (ky(1) - ky(NROs) ).^2 ) ;
traj_kspace = bart(['scale ', num2str(Nx/(2*dist_ksp))], traj_kspace);            

            
% Reconstruct with BART
igrid = bart('nufft -i -t', traj_kspace.', kspaces.');
igrid = bart(['resize -c 0 ', num2str(Nx), ' 1 ', num2str(Nx)], igrid);
figure
imagesc(abs(igrid)), axis equal, axis off
colormap gray

%% Plot a Spiral so that you can show the parameters in your transfer
t = linspace(0,5.03,10000);
figure

kkkx = 0.5 .* t .* cos(10 .* t) ;
kkky = 0.5 .* t .* sin(10 .* t) ;
plot(kkkx, kkky,  '.'); hold on
xlim([-3, 3]); ylim([-3, 3])

axis equal


figure

Gx = 0.5 .* cos(10 .* t) - 0.5 .* 10 .* t .* sin(10 .* t) ;
Gy = 0.5 .* sin(10 .* t) + 0.5 .* 10 .* t .* cos(10 .* t) ;

plot(t, Gx, 'LineWidth', 2); hold on 
plot(t, Gy, 'LineWidth', 2);
plot(t, zeros(1,10000), 'k'); 
xlim([-0.5,5.5]);ylim([-26 26]); axis off
legend('G_x', 'G_y')

% subplot(2,1,2)
% plot(t, Gy, 'LineWidth', 2); hold on
% plot(t, zeros(1,10000), 'k'); 
% xlim([-0.5,5.5]);ylim([-26 26]); axis off






%%
t = 0/2:dt:Td-dt;
spiralPower = 1;
spiralAmplitude =  0.4; 
spiralFrequency =  120;  

k_x = spiralAmplitude .* (t.^spiralPower) .* cos(spiralFrequency .* t);
k_y = spiralAmplitude .* (t.^spiralPower) .* sin(spiralFrequency .* t);

figure
plot(k_x(end-52:end)./(2*pi), k_y(end-52:end)./(2*pi),'-'); hold on
plot(k_x(end-52:end)./(2*pi), k_y(end-52:end)./(2*pi),'.');
axis equal


dist_ksp = sqrt( (kx(5897) - kx(NROs) ).^2 + ... 
                 (ky(5897) - ky(NROs) ).^2 ) ./ (2*pi)
             
             
             