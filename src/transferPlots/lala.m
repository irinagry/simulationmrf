imagesBARTReshaped1Ch_nomot = reshape(imagesBART_mottrans, [Nx*Ny, NTRs ]);

% % % Get the signals from the nomotion case
load([reconstructionsFolder, 'signalsNOMotion.mat'], 'nomotionSignals');

% Scores for the 96x96 phantom with 4145 spins
% MOTION transATION MidINNING
scoreIdxB = [(41-1)*Nx+49, ... %big circle
             (53-1)*Nx+69, ... %c12
             (59-1)*Nx+41, ... %c5
             (39-1)*Nx+56, ... %c8
             (42-1)*Nx+27, ... %c1
             (43-1)*Nx+39];    %c4
                       
scoreValB = resultsB_nomot.score(scoreIdxB);
% Titles
titles = {'bigCircle', 'c12', 'c5', 'c8', 'c1', 'c4'};

NTRStart = 200; % 1;
NTREnd   = 350; %NTRs;
for idxTR = 1:size(scoreIdxB,2)
    
    figure('Position',[1,1,700,1200])
    
    % The voxel we are looking for
    subplot(2,2,1)
    llPhantom = squeeze(abs(imagesBART_mottrans(:,:,1)));
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray
    
    % ABS
    subplot(2,2,[3,4]); hold on
    % Plot image signal
    p1 = plot(NTRStart:NTREnd, abs(imagesBARTReshaped1Ch_nomot(...
                 scoreIdxB(idxTR), NTRStart:NTREnd)), ...
          '-', 'LineWidth', 1.2);
	p1.Color = [0.8500    0.3250    0.0980];
    xlabel('#image'); ylabel('normalised signal intensity (a.u.)');
    title(['BART (abs) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
    % Plot dictionary signal
    p2 = plot(NTRStart:NTREnd, ...
         abs(dictionaryMRFNorm( ...
             resultsB_nomot.indices(scoreIdxB(idxTR)), NTRStart:NTREnd)), ...
         '--', 'LineWidth', 1.4);
    p2.Color = [0.3010    0.7450    0.9330];
    % Plot original signal NO MOTION
    p3 = plot( NTRStart:NTREnd, ...
               abs(nomotionSignals.(titles{idxTR}).imgSignal(NTRStart:NTREnd)), ...
          'k-.', 'LineWidth', 0.8);
    % Plot original dictionary match NO MOTION
    p4 = plot( NTRStart:NTREnd, ...
               abs(nomotionSignals.(titles{idxTR}).dictMatch(NTRStart:NTREnd)), ...
          '--.', 'LineWidth', 0.8);
	p4.Color = [0.2 0.8 0.2];
      
      
	title({['BART (abs) ',  titles{idxTR}, ...
            ' Score = ', num2str(scoreValB(idxTR))], ...
   ['Matched to: T_1 = ', num2str(resultsB_nomot.T1(scoreIdxB(idxTR))), ...
           ' T_2 = ', num2str(resultsB_nomot.T2(scoreIdxB(idxTR)))], ...
   ['Should match to: T_1 = ', num2str(nomotionSignals.(titles{idxTR}).T1matched), ...
           ' T_2 = ', num2str(nomotionSignals.(titles{idxTR}).T2matched)]})
      
	legend('motion signal',    'motion dictionary match', ...
           'no motion signal', 'no motion dictionary match')
    
    
    pause(2)

    % % % Save figure
    savefig([reconstructionsFolder, ...
             'sigCompare_', titles{idxTR}, '_MotionTransMid.fig']); %AllTRs
         
	pause(2)
    

end


