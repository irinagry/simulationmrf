% % Irina Grigorescu
% % 31 May 2018
% % 
% % Plotting T1 and T2 values for every motion type and onset
%%

% Types of motion
motions = {'nomot', ...
           'rotbeg', 'rotmid', 'rotend', ...
           'transbeg', 'transmid', 'transend', ...
           'rottransbeg', 'rottransmid', 'rottransend'};
NT = size(motions,2);
NVals = 13; % you have 13 t1 and t2 pairs from the 12 tubes + big tube phantom

% Load results
resultsReconstructions = struct;
resultsReconstructions.(motions{1}) = load('meanValsNOMOTION.mat');

resultsReconstructions.(motions{2}) = load('meanValsMotRotBeg.mat');
resultsReconstructions.(motions{3}) = load('meanValsMotRotMid.mat');
resultsReconstructions.(motions{4}) = load('meanValsMotRotEnd.mat');

resultsReconstructions.(motions{5}) = load('meanValsMotTransBeg.mat');
resultsReconstructions.(motions{6}) = load('meanValsMotTransMid.mat');
resultsReconstructions.(motions{7}) = load('meanValsMotTransEnd.mat');

resultsReconstructions.(motions{8}) = load('meanValsMotRotTransBeg.mat');
resultsReconstructions.(motions{9}) = load('meanValsMotRotTransMid.mat');
resultsReconstructions.(motions{10}) = load('meanValsMotRotTransEnd.mat');

colors =  [[     0    0.4470    0.7410 ]; ... 
           [0.8500    0.3250    0.0980 ]; ... 
           [0.9290    0.6940    0.1250 ]; ... 
           [0.4940    0.1840    0.5560 ]; ... 
           [0.4660    0.6740    0.1880 ]; ... 
           [0.9290    0.6940    0.1250 ]; ... 
           [0.4940    0.1840    0.5560 ]; ... 
           [0.4660    0.6740    0.1880 ]; ... 
           [0.9290    0.6940    0.1250 ]; ... 
           [0.4940    0.1840    0.5560 ]; ... 
           [0.4660    0.6740    0.1880 ]];

% %
% Plot for all tubes
for j = 1:NVals
    tubeno = j;

    figure('Position',[1,1,800,800])
    
    subplot(2,1,1) ; hold on% T1
    for i = 1:NT+1   % GT + Nomotion + 3 Rot + 3 Trans + 3 RotTrans
        if i == 1
            bar(i, resultsReconstructions.nomot.meanValuesReal.T1(tubeno), ...
                0.2, 'FaceColor', colors(i,:));
        else
            bar(i, resultsReconstructions.(motions{i-1}).meanValuesSimu.T1(tubeno), ...
                   0.2, 'FaceColor', colors(i,:));
            errorbar(i, resultsReconstructions.(motions{i-1}).meanValuesSimu.T1(tubeno), ...
                        resultsReconstructions.(motions{i-1}).stdValuesSimu.T1(tubeno), 'ok')
        end
        ylabel('T_1 (ms)')
        ax = gca;
        ax.XTick = [1,2,4,7,10];
        ax.XTickLabel = {'GT','NoMot','Rot','Trans','RotTrans'};
    %     legend('ground truth','no motion','','beginning','','middle','','end',...
    %            'Location', 'eastoutside')
        title({['Tube no ', num2str(tubeno)], ...
               ['T_1 = ', num2str(resultsReconstructions.nomot.meanValuesReal.T1(tubeno)), ...
             'ms T_2 = ', num2str(resultsReconstructions.nomot.meanValuesReal.T2(tubeno)), 'ms']})
    end

    subplot(2,1,2) ; hold on % T2
    for i = 1:NT+1   % GT + Nomotion + 3 Rot + 3 Trans + 3 RotTrans
        if i == 1
            bar(i, resultsReconstructions.nomot.meanValuesReal.T2(tubeno), ...
                0.2, 'FaceColor', colors(i,:));
        else
            bar(i, resultsReconstructions.(motions{i-1}).meanValuesSimu.T2(tubeno), ...
                   0.2, 'FaceColor', colors(i,:));
            errorbar(i, resultsReconstructions.(motions{i-1}).meanValuesSimu.T2(tubeno), ...
                        resultsReconstructions.(motions{i-1}).stdValuesSimu.T2(tubeno), 'ok')
        end
        ylabel('T_2 (ms)')
        ax = gca;
        ax.XTick = [1,2,4,7,10];
        ax.XTickLabel = {'GT','NoMot','Rot','Trans','RotTrans'};
    %     legend('ground truth','no motion','','beginning','','middle','','end',...
    %            'Location', 'eastoutside')
    end
    
    pause(2)
    savefig(['tubesAllMotion_Tube', num2str(j), '.fig']); 
    pause(2)
    
end
