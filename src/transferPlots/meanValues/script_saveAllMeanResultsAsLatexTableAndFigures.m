% % Irina Grigorescu
% % 31 May 2018
% % 
% % save all results as latex table
% For new colormaps
addpath(genpath('/Users/irina/Tools/MatlabStuff/Colormaps/'));
% % Load new colormaps: 
m = 500;
cm_magma = magma(m);
cm_inferno = inferno(m);
cm_plasma = plasma(m);
cm_viridis = viridis(m);

%%
% Types of motion
motions = {'nomot', ...
           'rotbeg', 'transbeg', 'rottransbeg', ...
           'rotmid', 'transmid', 'rottransmid', ...
           'rotend', 'transend', 'rottransend' };
       
% % GT & NoMot & RotBeg & TransBeg & RotTransBeg & RotMid & TransMid & RotTransMid & RotEnd & TransEnd & RotTransEnd \\ \hline 

NT = size(motions,2);
NVals = 13; % you have 13 t1 and t2 pairs from the 12 tubes + big tube phantom

% Load results
resultsReconstructions = struct;
resultsReconstructions.(motions{1}) = load('meanValsNOMOTION.mat');

resultsReconstructions.(motions{2}) = load('meanValsMotRotBeg.mat');
resultsReconstructions.(motions{3}) = load('meanValsMotTransBeg.mat');
resultsReconstructions.(motions{4}) = load('meanValsMotRotTransBeg.mat');

resultsReconstructions.(motions{5}) = load('meanValsMotRotMid.mat');
resultsReconstructions.(motions{6}) = load('meanValsMotTransMid.mat');
resultsReconstructions.(motions{7}) = load('meanValsMotRotTransMid.mat');

resultsReconstructions.(motions{8}) = load('meanValsMotRotEnd.mat');
resultsReconstructions.(motions{9}) = load('meanValsMotTransEnd.mat');
resultsReconstructions.(motions{10}) = load('meanValsMotRotTransEnd.mat');

%%
% T_1: GT | NoMot | RotBeg | TransBeg | RotTransBeg |
%                 | RotMid | TransMid | RotTransMid |
%                 | RotEnd | TransEnd | RotTransEnd |

T1results = [ resultsReconstructions.(motions{1}).meanValuesReal.T1(:) , ... % GT
              resultsReconstructions.(motions{1}).meanValuesSimu.T1(:) ];    % nomot
for i = 2:NT
    T1results = [ T1results , resultsReconstructions.(motions{i}).meanValuesSimu.T1(:) ];
end


T2results = [ resultsReconstructions.(motions{1}).meanValuesReal.T2(:) , ... % GT
              resultsReconstructions.(motions{1}).meanValuesSimu.T2(:) ];    % nomot
for i = 2:NT
    T2results = [ T2results , resultsReconstructions.(motions{i}).meanValuesSimu.T2(:) ];
end


% tablatex(T1results)


% % % % %%
% % % % 
% % % % figure('units','normalized','outerposition',[0 0 1 1])
% % % % 
% % % % colors =  [[     0    0.4470    0.7410 ]; ... 
% % % %            [0.8500    0.3250    0.0980 ]; ... 
% % % %            [0.9290    0.6940    0.1250 ]; ... 
% % % %            [0.4940    0.1840    0.5560 ]; ... 
% % % %            [0.4660    0.6740    0.1880 ]; ... 
% % % %            [0.9921    0.9290    0.0220 ]; ... 
% % % %            [0.4320    0.2130    0.6360 ]; ... 
% % % %            [0.1230    0.6340    0.9383 ]; ... 
% % % %            [0.9290    0.6940    0.1834 ]; ... 
% % % %            [0.9990    0.2340    0.2120 ]];
% % % % 
% % % % plot([0,2000],[0,2000]); hold on
% % % % for i = 2:NT+1
% % % %     scatter(T2results(:,1), T2results(:,i), 'ok', 'filled') ; hold on
% % % %     axis square
% % % % end
% % % % xlim([0 2000]);ylim([0 2000]);







%% Difference table
diffT1 = zeros(size(T1results,1),size(T1results,2));
diffT2 = zeros(size(T2results,1),size(T2results,2));

for i = 1 : size(T1results, 2) - 1
    
    diffT1(:,i) = abs(T1results(:,1)-T1results(:,i+1));%./T1results(:,1).*100;
    diffT2(:,i) = abs(T2results(:,1)-T2results(:,i+1));%./T2results(:,1).*100;
    
end

diffT1(:,end) = sum(diffT1,2);
diffT2(:,end) = sum(diffT2,2);
    
figure('units','normalized','outerposition',[0 0 1 1])

subplot(1,2,1)
imagesc(diffT1(:,1:end-1))
ax = gca;
% Columns are the types of motion
ax.XTick = 1:10;
ax.XTickLabel = {'NoMot','R-Beg','T-Beg','RT-Beg', ...
                         'R-Mid','T-Mid','RT-Mid', ...
                         'R-End','T-End','RT-End'};
% Lines are different tubes
ax.YTick = 1:13;
ax.YTickLabel = {'T1', 'T2', 'T3', 'T4', 'T5', ...
                 'T6', 'T7', 'T8', 'T9', 'T10', ...
                 'T11', 'T12', 'Mid'};
axis square
colormap(cm_magma)
colorbar
title('|T_1^{GT} - T_1^{simu}|')


% DIfferences in T2
subplot(1,2,2)
imagesc(diffT2(:,1:end-1))
ax = gca;
% Columns are the types of motion
ax.XTick = 1:10;
ax.XTickLabel = {'NoMot','R-Beg','T-Beg','RT-Beg', ...
                         'R-Mid','T-Mid','RT-Mid', ...
                         'R-End','T-End','RT-End'};
% Lines are different tubes
ax.YTick = 1:13;
ax.YTickLabel = {'T1', 'T2', 'T3', 'T4', 'T5', ...
                 'T6', 'T7', 'T8', 'T9', 'T10', ...
                 'T11', 'T12', 'Mid'};
axis square
colormap(cm_magma)
colorbar
title('|T_2^{GT} - T_2^{simu}|')

savefig('allTubesAllMotionAbsoluteDifferences.fig'); 

%%

figure('units','normalized','outerposition',[0 0 1 1])

subplot(1,2,1)
imagesc(diffT1(:,end))
ax = gca;
% Columns are the types of motion
ax.XTick = 1;
ax.XTickLabel = {'All'};
% Lines are different tubes
ax.YTick = 1:13;
ax.YTickLabel = {'T1', 'T2', 'T3', 'T4', 'T5', ...
                 'T6', 'T7', 'T8', 'T9', 'T10', ...
                 'T11', 'T12', 'Mid'};
axis equal
colormap(cm_magma)
colorbar


subplot(1,2,2)
imagesc(diffT2(:,end))
ax = gca;
% Columns are the types of motion
ax.XTick = 1;
ax.XTickLabel = {'All'};
% Lines are different tubes
ax.YTick = 1:13;
ax.YTickLabel = {'T1', 'T2', 'T3', 'T4', 'T5', ...
                 'T6', 'T7', 'T8', 'T9', 'T10', ...
                 'T11', 'T12', 'Mid'};
axis equal
colormap(cm_magma)
colorbar




