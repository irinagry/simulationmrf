% % IRINA GRIGORESCU
% % This script generate the dictionaries for visualizing them for transfer


PATHFOLDER = ['~/OneDrive - University College London/', ...
                'Work/Simulation/simulationMRF/'];

% Addpaths for EPG 
addpath(genpath([PATHFOLDER, 'src/extendedPhaseGraph/']));

% Addpaths for helpers
addpath(genpath([PATHFOLDER, 'src/helpers/']));

% Addpaths for preprocess
addpath(genpath([PATHFOLDER, 'src/preprocess/']));

% Addpaths for algs
addpath(genpath([PATHFOLDER, 'src/algs/']));

% Addpaths for exchanged to compare with analytical signal
addpath(genpath([PATHFOLDER, 'src/exchanged/']));


%% %%%%%%%%%%% CUSTOM MATERIAL AND CUSTOM SEQUENCE
NTRs = 500;

% % % % Flags
FLAGPLOT = 0; % Flag for plotting sequence parameters and material prop
% SEQFLAG  = 6; % sinusoidal FA, 0 PhA, fixed TRs, fixed ROs
SEQFLAG  = 5; % if == 5 need to provide customSequence 
MATFLAG  = 3; % if == 3 need to provide customMaterial

% % % % Custom Material

% % T1 ranges from 100ms to 5000ms with different timesteps
T1_mrf = 100:5:2000;            % from 100ms to 2000ms dt = 20ms
% T1_mrf = [T1_mrf 2300:300:5000]; % from 2300ms to 5000ms dt = 300ms
T1_mrf = [T1_mrf, 225, 335, 330, 1010, 1615, 675, 665, 830, 1150];
% T1_mrf = [ 500, 225, 335, 330, 1010, 1615, 675, 665, 840, 830, 1150, 1420, 1580];
T1_mrf = unique(sort(T1_mrf));

% % 
% % T2 ranges from 20ms to 3000ms with different timesteps
T2_mrf = 20:5:200;          % from 20ms to 100ms dt = 5ms
% T2_mrf = [T2_mrf 350:10:400];   % from 110ms to 400ms dt = 10ms
T2_mrf = [T2_mrf 210:10:400];   % from 110ms to 400ms dt = 10ms
% T2_mrf = [T2_mrf 400:200:3000]; % from 400ms to 3000ms dt = 200ms
T2_mrf = [T2_mrf, 360,  370,  155];
% T2_mrf = [ 360,  50,  70, 110,  130,  370,  90, 130, 110, 150,  155,  190,  160];
T2_mrf = unique(sort(T2_mrf));

df_mrf =   0;

flagIR =   1;

% % % % Custom Material
customMaterial.T1     = T1_mrf;
customMaterial.T2     = T2_mrf;
customMaterial.offRes = df_mrf;

% % % % Custom Sequence
customSequence        = struct;
customSequence.RF.FA  = dlmread([...
  '/Users/irina/OneDrive - University College London/Work/',...
  'Simulation/simulationMRF/src/preprocess/FAsMaryia']).';
                                           
customSequence.RF.FA  = customSequence.RF.FA(1:NTRs);
customSequence.RF.PhA = zeros(NTRs,1);
customSequence.TR     = zeros(NTRs,1) + 15;
customSequence.RO     = zeros(NTRs,1) +  7.5; %6; %7.5; % changed from 6 to 7.5

[materialTuples, M] = createSetOfTuples(customMaterial);

% Fix T1 and range T2
T1indx =         find(materialTuples(1,:) == 100);
T2indx = union ( find(materialTuples(2,:) ==  20), ...
          union ( find(materialTuples(2,:) ==  30), ...
           union ( find(materialTuples(2,:) ==  40), ...
            union ( find(materialTuples(2,:) ==  50), ...
             union ( find(materialTuples(2,:) ==  60), ...
              union ( find(materialTuples(2,:) ==  70), ...
                       find(materialTuples(2,:) ==  80)))))));
% T1indx =         find(materialTuples(1,:) == 1615);
% T2indx = union ( find(materialTuples(2,:) ==  150), ...
%           union ( find(materialTuples(2,:) ==  200), ...
%            union ( find(materialTuples(2,:) ==  250), ...
%             union ( find(materialTuples(2,:) ==  300), ...
%              ( find(materialTuples(2,:) ==  350))))));%, ...
%               union ( find(materialTuples(2,:) ==  300), ...
%                        find(materialTuples(2,:) ==  350)))))));
T1fixT2rangeindx = intersect(T1indx,T2indx);
% Fix T2 and range T1
T2indx =         find(materialTuples(2,:) == 20);
T1indx = union ( find(materialTuples(1,:) ==  200), ...
          union ( find(materialTuples(1,:) ==  300), ...
           union ( find(materialTuples(1,:) ==  400), ...
            union ( find(materialTuples(1,:) ==  500), ...
             union ( find(materialTuples(1,:) ==  600), ...
              union ( find(materialTuples(1,:) ==  700), ...
                       find(materialTuples(1,:) ==  800)))))));
                   
% T2indx =         find(materialTuples(2,:) == 370);
% T1indx = union ( find(materialTuples(1,:) ==  1100), ...
%           union ( find(materialTuples(1,:) ==  1200), ...
%            union ( find(materialTuples(1,:) ==  1300), ...
%             union ( find(materialTuples(1,:) ==  1400), ...
%              ( find(materialTuples(1,:) ==  1500))))));%, ...
%               union ( find(materialTuples(1,:) ==  1600), ...
%                        find(materialTuples(1,:) ==  1700)))))));

                   
T1rangeT2fixindx = intersect(T1indx,T2indx);

%% %%%%%% bSSFP Dictionary
tic

[ materialProperties, sequenceProperties, ...
  materialTuples, M, ...
  dictionaryMRF] = simulateMRF(NTRs, FLAGPLOT, ...
                                     SEQFLAG, customSequence, ...
                                     MATFLAG, customMaterial, flagIR);
t=toc; 

% % % % All dictionary signals are transformed into a dictionary matrix
% % % % where both real and imaginary channels are stored
dictionaryMRFComplex = [ squeeze(dictionaryMRF(1:M, :, 1))  , ...
                         squeeze(dictionaryMRF(1:M, :, 2)) ];

% % % % Normalize signals:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[dictionaryMRFNorm2Ch, dictionaryMRFNorm] = ...
                                 normalizeSignals(dictionaryMRFComplex);
                             
% Create dictionary of signal values (M x N) from the normalized signals

% Keep normal signal
% signalDictionaryMRF = abs( squeeze(dictionaryMRF(1:M, :, 1)) + ...
%                        1i.*squeeze(dictionaryMRF(1:M, :, 2)) );
% Or keep normalised signal
signalDictionaryMRF = abs(dictionaryMRFNorm);

% save('dictionaries/dictionaryAll.mat', 'M', 'NTRs', ...
%                   'dictionaryMRFNorm', 'dictionaryMRFNorm2Ch', ...
%                   'materialTuples')

%% Plot bSSFP signals
try
    figure('Position',[10,10,800,1000])
    
    % Plot example fingerprints for T2 fixed and a range of T1s
    subplot(2,1,1)
    plot(1:NTRs, signalDictionaryMRF(T1rangeT2fixindx, :), ...
                 '.-', 'LineWidth', 1.4);
    text(401, 0.175, 'Fixed T_2 = 20ms');
    xlabel('TR index');
    ylabel('Signal (a.u.)');
    grid on
    legend(strsplit(sprintf('T_1 = %dms, ', ...
                   (materialTuples(1,T1rangeT2fixindx))),','), ...
               'Location', 'northoutside', ...
               'Orientation', 'horizontal');
    
    
	% Plot example fingerprints for T1 fixed and a range of T2s
    subplot(2,1,2)
    plot(1:NTRs, signalDictionaryMRF(T1fixT2rangeindx, :), ...
                 '.-', 'LineWidth', 1.4);
	text(401, 0.09, 'Fixed T_1 = 100ms');
    xlabel('TR index');
    ylabel('Signal (a.u.)');
    grid on
    legend(strsplit(sprintf('T_2 = %dms, ', ...
                   (materialTuples(2,T1fixT2rangeindx))),','), ...
               'Location', 'northoutside', ...
               'Orientation', 'horizontal');
	
    
catch
    warning('You are using SMALL DICTIONARY SIZE.');
end
    


% savefig([PATHFOLDER,'src/transferPlots/dictionaries/', ...
%                     'mrfDictionaryBSSFPDiffT1Norm.fig']);

%% %%%%%% Create dictionary FISP
% Retrieve set of material properties without off-resonance
materialTuplesFISP = materialTuples(1:2, T1rangeT2fixindx);
materialTuplesFISP = [ materialTuplesFISP materialTuples(1:2, T1fixT2rangeindx)];

% Create F0states
NTRs = NTRs + 1;
F0states = zeros(14, NTRs);
customSequence.RF.PhA = zeros(NTRs,1);
customSequence.RF.FA  = [ 180    ; customSequence.RF.FA];
customSequence.TR     = [   1    ; customSequence.TR];
customSequence.RO     = [   0.5  ; customSequence.RO];

tic;
percDone = 5;
fprintf('\n\n\n-----> Starting dictionary creation\n');

% Run for every material tuple
for i = 1:14
    T1 = materialTuplesFISP(1,i);
    T2 = materialTuplesFISP(2,i);

    [ChiF, ChiZ] = epgMatrix(NTRs, customSequence.RF.PhA, ...
                                   customSequence.RF.FA, ...
                                   customSequence.TR, ...
                                   T1, ...    % T_1
                                   T2, ...    % T_2
                                   1, 1);     % M0, flagDephase

    
    F0states(i, :) = ChiF(NTRs, :) .* exp(-customSequence.RO ./ T2).';
    
    if (mod(i, ceil(M/20)) == 0)
        t = toc;
        fprintf(' --- %3d %% done in %3.2f seconds ... \n', ...
                percDone, t);
        percDone = percDone + 5;
    end
end
t = toc;
    
disp(['Dictionary generation with EPG took ', num2str(t), ' s']);

% % % % Normalize signals:
% output is: 
% [Dnorm      , Dnormcplx]
%  (Mx2N)     ,   (MxN)
[F0statesNorm2Ch, F0statesNorm] = ...
          normalizeSignals( [ real(F0states(:,2:NTRs)) imag(F0states(:,2:NTRs)) ] );


%% Plot FISP signals
F0statesTP = F0states(:,2:end);
% F0statesTP = F0statesNorm;

try
    figure('Position',[10,10,800,1000])
    
    % Plot example fingerprints for T2 fixed and a range of T1s
    subplot(2,1,1)
    plot(1:NTRs-1, abs(F0statesTP(1:7, :)), ...
                 '.-', 'LineWidth', 1.4);
	text(401, 0.165, 'Fixed T_2 = 20ms');
    xlabel('TR index');
    ylabel('Signal (a.u.)');
    grid on
    legend(strsplit(sprintf('T_1 = %dms, ', ...
                   (materialTuplesFISP(1,1:7))),','), ...
               'Location', 'northoutside', ...
               'Orientation', 'horizontal');
	xlim([1,500])
    
    
	% Plot example fingerprints for T1 fixed and a range of T2s
    subplot(2,1,2)
    plot(1:NTRs-1, abs(F0statesTP(8:end, :)), ...
                 '.-', 'LineWidth', 1.4); 
    text(401, 0.3, 'Fixed T_1 = 100ms');
    xlabel('TR index');
    ylabel('Signal (a.u.)');
    grid on
    legend(strsplit(sprintf('T_2 = %dms, ', ...
                   (materialTuplesFISP(2,8:end))),','), ...
               'Location', 'northoutside', ...
               'Orientation', 'horizontal');
	xlim([1,500])
	
    
catch
    warning('You are using SMALL DICTIONARY SIZE.');
end

% savefig([PATHFOLDER,'src/transferPlots/dictionaries/', ...
%                     'mrfDictionaryFISP.fig']);

%% PLOT FAs
NTRs = NTRs-1;
FAsMaryia = dlmread([...
  '/Users/irina/OneDrive - University College London/Work/',...
  'Simulation/simulationMRF/src/preprocess/FAsMaryia']).';

figure('Position',[10,150,1000,400])
plot(1:NTRs, FAsMaryia, '.-', 'LineWidth', 2);
grid on
xlabel('TR index');
ylabel('Flip Angles (deg)');

% savefig([PATHFOLDER,'src/transferPlots/dictionaries/', ...
%                     'FAsMaryia.fig']);



% 
% %% SAVE DICTIONARIES
% toSave = 1;
% if toSave == 1
%     save('dictionaries/dictionaryAll.mat', ...
%          'dictionaryMRFNorm','dictionaryMRFNorm2Ch','NTRs','M','materialTuples');
% end



%%
% % % % % toplot={rhomap, T1map, T2map, T2smap, DB};
% % % % % toplottitle={'PD','T_1','T_2','T_2*','DB'};
% % % % % figure
% % % % % for i = 1:5
% % % % %     subplot(1,5,i)
% % % % %     imagesc(rot90(toplot{i}));
% % % % %     axis equal, axis off, 
% % % % %     colormap(cm_viridis)
% % % % %     colorbar('Location','south')
% % % % %     title(toplottitle{i})
% % % % % end
