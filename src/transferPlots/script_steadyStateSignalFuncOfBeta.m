% % % Script to generate the steady-state signal for different tissue types

PATHFOLDER = ['~/OneDrive - University College London/', ...
                'Work/Simulation/simulationMRF/'];

% Addpaths for EPG 
addpath(genpath([PATHFOLDER, 'src/extendedPhaseGraph/']));

% Addpaths for helpers
addpath(genpath([PATHFOLDER, 'src/helpers/']));

% Addpaths for preprocess
addpath(genpath([PATHFOLDER, 'src/preprocess/']));

% Addpaths for algs
addpath(genpath([PATHFOLDER, 'src/algs/']));

% Addpaths for exchanged to compare with analytical signal
addpath(genpath([PATHFOLDER, 'src/exchanged/']));

%%
% % % %
% % % % SEQUENCE
NTRs   = 3000; % number of timepoints
flagIR =    0; % no inversion recovery pulse

SEQFLAG  = 5; % If == 4 or 5 need to provide customValuesSequence
cstmSeq.RF.FA  =  zeros(NTRs, 1) + 10;
cstmSeq.RF.PhA =  zeros(NTRs, 1);
cstmSeq.TR     =  zeros(NTRs, 1) + 10;
cstmSeq.RO     =  zeros(NTRs, 1) +  5;

% % % % 
% % CUSTOM MATERIAL
MATFLAG  = 3; % If == 2 or 3 you need to provide customValuesMaterial

% Values for the angle (in deg) of precession during TR
betaPrecessionTR            = 0:360; 
% df = beta / (2*180*TR) 
cstmMat.offRes = betaPrecessionTR ... % off resonance values
                 ./ (360 * cstmSeq.TR(1));

% Types of materials for which to plot                          
titles = {'GM','Fat','CSF'};
T1 = [950, 250, 4500]; 
T2 = [100,  60, 2200];

% Create empty array of 3 x 361 x NTRs
signalDictionaryMRF = zeros(size(T1,2), size(betaPrecessionTR,2), NTRs);

for i = 1:size(T1,2)
    
    % Material for this index
    cstmMat.T1 = T1(i);
    cstmMat.T2 = T2(i);

    % % % % % % % %
    % % % % RUN SIMULATIONS FOR N timepoints
    [materialProperties, sequenceProperties, ...
        materialTuples, M, ...
        dictionaryMRF] = simulateMRF(NTRs, 0, ...
                             SEQFLAG, cstmSeq, ...
                             MATFLAG, cstmMat, flagIR);
    
    dictionaryMRFComplex =     squeeze(dictionaryMRF(:,:,1)) + ...
                           1i.*squeeze(dictionaryMRF(:,:,2));
    signalDictionaryMRF(i,:,:) = abs(dictionaryMRFComplex);

end


% % % % Plot the results
figure('Position',[50,50,1200,400]); hold on
for i = 1:size(T1,2)
    plot(deg2rad(betaPrecessionTR), signalDictionaryMRF(i, :, end), ...
         'LineWidth', 2)
end
xlabel({'\beta','angle of precession for each TR (rad)'}); 
ylabel('Signal Intensity (a.u.)');
title({'SS Signal for different precession angles ',' '});
xlim([0,2*pi]); grid on
legend(titles);

savefig([PATHFOLDER,'src/transferPlots/steadyState/', ...
                    'ssFA', num2str(cstmSeq.RF.FA(1)), '.fig']);

                
                
                