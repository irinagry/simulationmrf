% 
% Irina Grigorescu
% Date created: 18-03-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps for all motion cases
% 



% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% For new colormaps
addpath(genpath('/Users/irina/Tools/MatlabStuff/Colormaps/'));
% % Load new colormaps: 
m = 500;
cm_magma = magma(m);
cm_inferno = inferno(m);
cm_plasma = plasma(m);
cm_viridis = viridis(m);

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
% COMMON ROOT FOLDER 
commonRoot = '~/OneDrive - University College London/Work/JEMRISSimulations/'; 

PATHFOLDER = ['~/OneDrive - University College London/', ...
                'Work/Simulation/simulationMRF/']; 

% DICTIONARY FOLDER            
dictionaryFolder = [PATHFOLDER, 'src/transferPlots/dictionaries/'];
% NO MOTION RECONSTRUCTIONS
reconstructionsFolder = [PATHFOLDER, 'src/transferPlots/reconstructions/'];
% MAPS FOLDER
mapsFolder = [PATHFOLDER, 'src/transferPlots/maps/'];

% % % % % % % % % % % % % % % % 
% % % % % % % % % % LOAD DATA
% % % % % % % % % % % % % % % % 

% % % % % % DICTIONARY
dictionary = load([ dictionaryFolder, 'dictionaryMore.mat' ]);
NTRs = dictionary.NTRs;
M    = dictionary.M;
Nx = 96; Ny = Nx;
dictionaryMRFNorm    = dictionary.dictionaryMRFNorm;
dictionaryMRFNorm2Ch = dictionary.dictionaryMRFNorm2Ch;
materialTuples = dictionary.materialTuples;
clear dictionary



motions = {'NOMOTION', ...
           'ROTBEG', 'ROTMID', 'ROTEND', ...
           'TRANSBEG', 'TRANSMID', 'TRANSEND', ...
           'ROTTRANSBEG', 'ROTTRANSMID', 'ROTTRANSEND'};

%%
% % % % Create GROUND TRUTH MAPS

% For the 96x96 phantom with 4145 spins
centres = [41,27 ; 56,27 ; ...
           27,41 ; 41,41 ; 56,41 ; 70,41 ; ...
           27,56 ; 41,56 ; 56,56 ; 70,56 ; ...
           41,69 ; 56,69 ; ...
           49,49 ]; % big circle
radii   = [zeros(1,12) + 4 , 32];

realMap = struct;
myPhantomT1Values = [500, 225, 335, 330, 1010, 1615, 675, ...
                     665, 840, 830, 1150, 1420, 1580];
myPhantomT2Values = [360,  50,  70, 110,  130,  370,  90, ...
                     130, 110, 150,  155,  190,  160];

[realMap.T1map, realMap.T2map, maskAllCircles, maskHollow] = ...
         func_createRealT1T2Maps(myPhantomT1Values, myPhantomT2Values, ...
                                 Nx, Ny, centres, radii);

masks = [ [ maskAllCircles , maskAllCircles, maskAllCircles ] ; ...
          [ maskAllCircles , maskAllCircles, maskAllCircles ] ; ...
          [ maskAllCircles , maskAllCircles, maskAllCircles ] ];

%% LOAD MAPS
motionMap.NOMOTION = load([mapsFolder, 'mapsNoMotionAll.mat'], 'resultsB_nomot');

% Rotations
motionMap.ROTBEG = load([mapsFolder, 'mapsMotionrot_beg.mat'], 'resultsB_nomot');
motionMap.ROTMID = load([mapsFolder, 'mapsMotionrot_mid.mat'], 'resultsB_nomot');
motionMap.ROTEND = load([mapsFolder, 'mapsMotionrot_end.mat'], 'resultsB_nomot');

% Translations
motionMap.TRANSBEG = load([mapsFolder, 'mapsMotiontrans_beg.mat'], 'resultsB_nomot');
motionMap.TRANSMID = load([mapsFolder, 'mapsMotiontrans_mid.mat'], 'resultsB_nomot');
motionMap.TRANSEND = load([mapsFolder, 'mapsMotiontrans_end.mat'], 'resultsB_nomot');

% Rotations-Translations
motionMap.ROTTRANSBEG = load([mapsFolder, 'mapsMotionrottrans_beg.mat'], 'resultsB_nomot');
motionMap.ROTTRANSMID = load([mapsFolder, 'mapsMotionrottrans_mid.mat'], 'resultsB_nomot');
motionMap.ROTTRANSEND = load([mapsFolder, 'mapsMotionrottrans_end.mat'], 'resultsB_nomot');


%% Plot 3x3 for T1 motion maps

% % Motions maps for T1
figure             
motionMapsT1 = [ [ motionMap.ROTBEG.resultsB_nomot.T1map,  motionMap.TRANSBEG.resultsB_nomot.T1map,  motionMap.ROTTRANSBEG.resultsB_nomot.T1map ] ; ...
                 [ motionMap.ROTMID.resultsB_nomot.T1map,  motionMap.TRANSMID.resultsB_nomot.T1map,  motionMap.ROTTRANSMID.resultsB_nomot.T1map ] ; ...
                 [ motionMap.ROTEND.resultsB_nomot.T1map,  motionMap.TRANSEND.resultsB_nomot.T1map,  motionMap.ROTTRANSEND.resultsB_nomot.T1map ] ];

imagesc(abs(motionMapsT1).*masks)
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT1Values)])

% % No motion map and GT for T1
figure
imagesc([ abs(motionMap.NOMOTION.resultsB_nomot.T1map).*maskAllCircles.*maskHollow, ...
          abs(realMap.T1map).*maskAllCircles.*maskHollow ])
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT1Values)])


% % Motions maps for T2
figure             
motionMapsT2 = [ [ motionMap.ROTBEG.resultsB_nomot.T2map,  motionMap.TRANSBEG.resultsB_nomot.T2map,  motionMap.ROTTRANSBEG.resultsB_nomot.T2map ] ; ...
                 [ motionMap.ROTMID.resultsB_nomot.T2map,  motionMap.TRANSMID.resultsB_nomot.T2map,  motionMap.ROTTRANSMID.resultsB_nomot.T2map ] ; ...
                 [ motionMap.ROTEND.resultsB_nomot.T2map,  motionMap.TRANSEND.resultsB_nomot.T2map,  motionMap.ROTTRANSEND.resultsB_nomot.T2map ] ];

imagesc(abs(motionMapsT2).*masks)
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT2Values)])

% % No motion map and GT for T2
figure
imagesc([ abs(motionMap.NOMOTION.resultsB_nomot.T2map).*maskAllCircles.*maskHollow, ...
          abs(realMap.T2map).*maskAllCircles.*maskHollow ])
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT2Values)])


% % Score maps 
figure             
motionMapsScore = [ [ motionMap.ROTBEG.resultsB_nomot.scoremap,  motionMap.TRANSBEG.resultsB_nomot.scoremap,  motionMap.ROTTRANSBEG.resultsB_nomot.scoremap ] ; ...
                 [ motionMap.ROTMID.resultsB_nomot.scoremap,  motionMap.TRANSMID.resultsB_nomot.scoremap,  motionMap.ROTTRANSMID.resultsB_nomot.scoremap ] ; ...
                 [ motionMap.ROTEND.resultsB_nomot.scoremap,  motionMap.TRANSEND.resultsB_nomot.scoremap,  motionMap.ROTTRANSEND.resultsB_nomot.scoremap ] ];

imagesc(abs(motionMapsScore).*masks)
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, 1])

% % No motion map and GT for Score
figure
imagesc([ abs(motionMap.NOMOTION.resultsB_nomot.scoremap).*maskAllCircles.*maskHollow, ...
          ones(Nx,Ny).*maskAllCircles.*maskHollow ])
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, 1])

%%
% % % T1/T2/Score for no motion and ground truth
figure
subplot(3,2,[1,2])
imagesc([ abs(motionMap.NOMOTION.resultsB_nomot.T1map).*maskAllCircles.*maskHollow, ...
          abs(realMap.T1map).*maskAllCircles.*maskHollow ])
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'eastoutside')
caxis([0, max(myPhantomT1Values)])

subplot(3,2,[3,4])
imagesc([ abs(motionMap.NOMOTION.resultsB_nomot.T2map).*maskAllCircles.*maskHollow, ...
          abs(realMap.T2map).*maskAllCircles.*maskHollow ])
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'eastoutside')
caxis([0, max(myPhantomT2Values)])

subplot(3,2,[5,6])
imagesc([ abs(motionMap.NOMOTION.resultsB_nomot.scoremap).*maskAllCircles.*maskHollow, ...
          ones(Nx,Ny).*maskAllCircles.*maskHollow ])
axis square, axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'eastoutside')
caxis([0, 1])



%% Plot 1x3 for T1 motion maps for beginning (zoomed in)
% left upper  : 14:49
% right upper : 50:65
lines = 14:49; columns = 50:85;
motionMapsT1ZoomIn = [ [ maskAllCircles(lines,columns).*motionMap.ROTBEG.resultsB_nomot.T1map(lines,columns),  ... 
                         maskAllCircles(lines,columns).*motionMap.TRANSBEG.resultsB_nomot.T1map(lines,columns),  ...
                         maskAllCircles(lines,columns).*motionMap.ROTTRANSBEG.resultsB_nomot.T1map(lines,columns) ] ; ...
                       [ maskAllCircles(lines,columns).*motionMap.ROTMID.resultsB_nomot.T1map(lines,columns),  ... 
                         maskAllCircles(lines,columns).*motionMap.TRANSMID.resultsB_nomot.T1map(lines,columns),  ...
                         maskAllCircles(lines,columns).*motionMap.ROTTRANSMID.resultsB_nomot.T1map(lines,columns) ] ; ...
                       [ maskAllCircles(lines,columns).*motionMap.ROTEND.resultsB_nomot.T1map(lines,columns),  ... 
                         maskAllCircles(lines,columns).*motionMap.TRANSEND.resultsB_nomot.T1map(lines,columns),  ...
                         maskAllCircles(lines,columns).*motionMap.ROTTRANSEND.resultsB_nomot.T1map(lines,columns) ] ];
figure             
imagesc(abs(motionMapsT1ZoomIn))
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT1Values)])

figure             
imagesc(abs(motionMap.NOMOTION.resultsB_nomot.T1map(lines,columns)).*maskAllCircles(lines,columns).*maskHollow(lines,columns))
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT1Values)])

%% Plot 1x3 for T2 motion maps for beginning (zoomed in)
lines = 14:49; columns = 50:85;
motionMapsT2ZoomIn = [ [ maskAllCircles(lines,columns).*motionMap.ROTBEG.resultsB_nomot.T2map(lines,columns),  ... 
                         maskAllCircles(lines,columns).*motionMap.TRANSBEG.resultsB_nomot.T2map(lines,columns),  ...
                         maskAllCircles(lines,columns).*motionMap.ROTTRANSBEG.resultsB_nomot.T2map(lines,columns) ] ; ...
                       [ maskAllCircles(lines,columns).*motionMap.ROTMID.resultsB_nomot.T2map(lines,columns),  ... 
                         maskAllCircles(lines,columns).*motionMap.TRANSMID.resultsB_nomot.T2map(lines,columns),  ...
                         maskAllCircles(lines,columns).*motionMap.ROTTRANSMID.resultsB_nomot.T2map(lines,columns) ] ; ...
                       [ maskAllCircles(lines,columns).*motionMap.ROTEND.resultsB_nomot.T2map(lines,columns),  ... 
                         maskAllCircles(lines,columns).*motionMap.TRANSEND.resultsB_nomot.T2map(lines,columns),  ...
                         maskAllCircles(lines,columns).*motionMap.ROTTRANSEND.resultsB_nomot.T2map(lines,columns) ] ];
figure             
imagesc(abs(motionMapsT2ZoomIn))
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT2Values)])

figure             
imagesc(abs(motionMap.NOMOTION.resultsB_nomot.T2map(lines,columns)).*maskAllCircles(lines,columns))%.*maskHollow(lines,columns))
axis equal, axis off
colormap(cm_magma) 
colorbar('Location', 'southoutside')
caxis([0, max(myPhantomT2Values)])


%% Plot bar plots for no motion and motion cases for a few tubes

for i = 1:size(motions,2)

    if i == 1
        % % T1 average values for 
        [ meanValuesReal.T1 , meanValuesSimu.(motions{i}).T1, stdValuesSimu.(motions{i}).T1 ] = ...
            func_retrieveMeanValueInROI(realMap.T1map, motionMap.(motions{i}).resultsB_nomot.T1map, ...
                                    centres, [radii(1:end-1)-1, 4], 0);
        % % T2
        [ meanValuesReal.T2 , meanValuesSimu.(motions{i}).T2, stdValuesSimu.(motions{i}).T2 ] = ...
            func_retrieveMeanValueInROI(realMap.T2map, motionMap.(motions{i}).resultsB_nomot.T2map, ...
                                    centres, [radii(1:end-1)-1, 4], 0);
    else
        % % T1 average values for 
        [ meanValuesReal.T1 , meanValuesSimu.(motions{i}).T1, stdValuesSimu.(motions{i}).T1 ] = ...
            func_retrieveMeanValueInROI(realMap.T1map, motionMap.(motions{i}).resultsB_nomot.T1map, ...
                                    centres, [radii(1:end-1), 4], 0);
        % % T2
        [ meanValuesReal.T2 , meanValuesSimu.(motions{i}).T2, stdValuesSimu.(motions{i}).T2 ] = ...
            func_retrieveMeanValueInROI(realMap.T2map, motionMap.(motions{i}).resultsB_nomot.T2map, ...
                                    centres, [radii(1:end-1), 4], 0);
    end
end

colors =  [[     0    0.4470    0.7410 ]; ... 
           [0.8500    0.3250    0.0980 ]; ... 
           [0.9290    0.6940    0.1250 ]; ... 
           [0.4940    0.1840    0.5560 ]; ... 
           [0.4660    0.6740    0.1880 ]; ... 
           [0.3010    0.7450    0.9330 ]; ... 
           [0.6350    0.0780    0.1840 ] ] ;

% % % % look only at tubes 1/3/10/12 and middle
idxTubes = [13, 1, 3, 5, 12]; 
tickLabels = {'midPhantom', ['t', num2str(idxTubes(2))], ...
                            ['t', num2str(idxTubes(3))], ...
                            ['t', num2str(idxTubes(4))], ...
                            ['t', num2str(idxTubes(5))]};
indicesForTubes = 1:30; % compare GT with no motion with motion beginning rot ///trans/rot-trans
                        % 5 tubes GT 
                        %            + 5 tubes no motion 
                        %            + 5 for motion beg rot
                        %            + 5 for motion beg mid
                        %            + 5 for motion beg end

% % %% % %% % %% % % % % %% % %% % %% % % % % %% % %% % %% % % 
% % %% % %% % %% % % % % %% % %% % %% % % % % %% % %% % %% % % 
% % %% % %% % %% % % PLOT GT + NO MOTION + ROTATIONS
figure

% % %% % %% % %% % % T1 values
subplot(2,1,1)
% Plot for Ground truth
bar(indicesForTubes(1:6:end-1), meanValuesReal.T1(idxTubes), 0.1, 'FaceColor', colors(1,:)); hold on ;

% Plot for no motion
i = 1;
bar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;

% Plot bars for motion rot beginning/middle/end
for i = 2:4
    bar(indicesForTubes(i+1:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;
end
legend('ground truth', 'no motion', 'rotation beginning', 'rotation middle', 'rotation end', ...
       'Location', 'eastoutside')
% Plot errors for no motion
i = 1;
errorbar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), stdValuesSimu.(motions{i}).T1(idxTubes), 'ok')
% Plot errors for motion rot beginning/middle/end
for i = 2:4
   errorbar(indicesForTubes(i+1:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), stdValuesSimu.(motions{i}).T1(idxTubes), 'ok')
end   

% ticks
ylabel('T_1 (ms)')
ax = gca;
ax.XTick = 3:6:27;
ax.XTickLabel = tickLabels;

title('Rotations')

% % %% % %% % %% % % T2 values
subplot(2,1,2)
% Plot for Ground truth
bar(indicesForTubes(1:6:end-1), meanValuesReal.T2(idxTubes), 0.1, 'FaceColor', colors(1,:)); hold on ;

% Plot for no motion
i = 1;
bar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;

% Plot bars for motion rot beginning/middle/end
for i = 2:4
    bar(indicesForTubes(i+1:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;
end
legend('ground truth', 'no motion', 'rotation beginning', 'rotation middle', 'rotation end', ...
       'Location', 'eastoutside')
% Plot errors for no motion
i = 1;
errorbar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), stdValuesSimu.(motions{i}).T2(idxTubes), 'ok')
% Plot errors for motion rot beginning/middle/end
for i = 2:4
   errorbar(indicesForTubes(i+1:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), stdValuesSimu.(motions{i}).T2(idxTubes), 'ok')
end   

% ticks
ylabel('T_2 (ms)')
ax = gca;
ax.XTick = 3:6:27;
ax.XTickLabel = tickLabels;



% %
% % %% % %% % %% % % % % %% % %% % %% % % % % %% % %% % %% % % 
% % %% % %% % %% % % % % %% % %% % %% % % % % %% % %% % %% % % 
% % %% % %% % %% % % PLOT GT + NO MOTION + TRANSLATIONS
figure

% % %% % %% % %% % % T1 values
subplot(2,1,1)
% Plot for Ground truth
bar(indicesForTubes(1:6:end-1), meanValuesReal.T1(idxTubes), 0.1, 'FaceColor', colors(1,:)); hold on ;

% Plot for no motion
i = 1;
bar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;

% Plot bars for motion trans beginning/middle/end
for i = 5:7
    bar(indicesForTubes(i-2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), 0.1, 'FaceColor', colors(i-2,:)); hold on ;
end
legend('ground truth', 'no motion', 'translation beginning', 'translation middle', 'translation end', ...
       'Location', 'eastoutside')
% Plot errors for no motion
i = 1;
errorbar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), stdValuesSimu.(motions{i}).T1(idxTubes), 'ok')
% Plot errors for motion rot beginning/middle/end
for i = 5:7
   errorbar(indicesForTubes(i-2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), stdValuesSimu.(motions{i}).T1(idxTubes), 'ok')
end   

% ticks
ylabel('T_1 (ms)')
ax = gca;
ax.XTick = 3:6:27;
ax.XTickLabel = tickLabels;

title('Translations')


% % %% % %% % %% % % T2 values
subplot(2,1,2)
% Plot for Ground truth
bar(indicesForTubes(1:6:end-1), meanValuesReal.T2(idxTubes), 0.1, 'FaceColor', colors(1,:)); hold on ;

% Plot for no motion
i = 1;
bar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;

% Plot bars for motion trans beginning/middle/end
for i = 5:7
    bar(indicesForTubes(i-2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), 0.1, 'FaceColor', colors(i-2,:)); hold on ;
end
legend('ground truth', 'no motion', 'translation beginning', 'translation middle', 'translation end', ...
       'Location', 'eastoutside')
% Plot errors for no motion
i = 1;
errorbar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), stdValuesSimu.(motions{i}).T2(idxTubes), 'ok')
% Plot errors for motion rot beginning/middle/end
for i = 5:7
   errorbar(indicesForTubes(i-2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), stdValuesSimu.(motions{i}).T2(idxTubes), 'ok')
end   

% ticks
ylabel('T_2 (ms)')
ax = gca;
ax.XTick = 3:6:27;
ax.XTickLabel = tickLabels;



% %
% % %% % %% % %% % % % % %% % %% % %% % % % % %% % %% % %% % % 
% % %% % %% % %% % % % % %% % %% % %% % % % % %% % %% % %% % % 
% % %% % %% % %% % % PLOT GT + NO MOTION + ROTATIONS+TRANSLATIONS
figure

% % %% % %% % %% % % T1 values
subplot(2,1,1)
% Plot for Ground truth
bar(indicesForTubes(1:6:end-1), meanValuesReal.T1(idxTubes), 0.1, 'FaceColor', colors(1,:)); hold on ;

% Plot for no motion
i = 1;
bar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;

% Plot bars for motion trans beginning/middle/end
for i = 7:9
    bar(indicesForTubes(i-4:6:end-1), meanValuesSimu.(motions{i+1}).T1(idxTubes), 0.1, 'FaceColor', colors(i-4,:)); hold on ;
end
legend('ground truth', 'no motion', 'rotation+translation beginning', 'rotation+translation middle', 'rotation+translation end', ...
       'Location', 'eastoutside')
   
   
% Plot errors for no motion
i = 1;
errorbar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T1(idxTubes), stdValuesSimu.(motions{i}).T1(idxTubes), 'ok')
% Plot errors for motion rot beginning/middle/end
for i = 7:9
   errorbar(indicesForTubes(i-4:6:end-1), meanValuesSimu.(motions{i+1}).T1(idxTubes), stdValuesSimu.(motions{i}).T1(idxTubes), 'ok')
end   

% ticks
ylabel('T_1 (ms)')
ax = gca;
ax.XTick = 3:6:27;
ax.XTickLabel = tickLabels;

title('Rotations/Translations')


% % %% % %% % %% % % T2 values
subplot(2,1,2)
% Plot for Ground truth
bar(indicesForTubes(1:6:end-1), meanValuesReal.T2(idxTubes), 0.1, 'FaceColor', colors(1,:)); hold on ;

% Plot for no motion
i = 1;
bar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), 0.1, 'FaceColor', colors(i+1,:)); hold on ;

% Plot bars for motion trans beginning/middle/end
for i = 7:9
    bar(indicesForTubes(i-4:6:end-1), meanValuesSimu.(motions{i+1}).T2(idxTubes), 0.1, 'FaceColor', colors(i-4,:)); hold on ;
end
legend('ground truth', 'no motion', 'rotation+translation beginning', 'rotation+translation middle', 'rotation+translation end', ...
       'Location', 'eastoutside')
% Plot errors for no motion
i = 1;
errorbar(indicesForTubes(2:6:end-1), meanValuesSimu.(motions{i}).T2(idxTubes), stdValuesSimu.(motions{i}).T2(idxTubes), 'ok')
% Plot errors for motion rot beginning/middle/end
for i = 7:9
   errorbar(indicesForTubes(i-4:6:end-1), meanValuesSimu.(motions{i+1}).T2(idxTubes), stdValuesSimu.(motions{i}).T2(idxTubes), 'ok')
end   

% ticks
ylabel('T_2 (ms)')
ax = gca;
ax.XTick = 3:6:27;
ax.XTickLabel = tickLabels;
