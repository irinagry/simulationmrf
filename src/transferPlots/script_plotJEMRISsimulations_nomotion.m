% 
% Irina Grigorescu
% Date created: 17-03-2018
% 
% Script to reconstruct a JEMRIS simulation with BART
% And create maps
% 
% NO MOTION CASE FOR FULLY SAMPLED SPIRAL


% Set environment variable for BART
addpath(genpath('/Users/irina/Tools/bart-0.3.01/'))
setenv('TOOLBOX_PATH','/Users/irina/Tools/bart-0.3.01/')

% For new colormaps
addpath(genpath('/Users/irina/Tools/MatlabStuff/Colormaps/'));
% % Load new colormaps: 
m = 500;
cm_magma = magma(m);
cm_inferno = inferno(m);
cm_plasma = plasma(m);
cm_viridis = viridis(m);

% Prerequisites
run(['~/OneDrive - University College London/Work/Simulation/', ...
     'simulationMRF/possumHelpers/runPathsForPOSSUMandEPG']);
 
% COMMON ROOT FOLDER 
commonRoot = '~/OneDrive - University College London/Work/JEMRISSimulations/'; 

PATHFOLDER = ['~/OneDrive - University College London/', ...
                'Work/Simulation/simulationMRF/']; 

% DICTIONARY FOLDER            
dictionaryFolder = [PATHFOLDER, 'src/transferPlots/dictionaries/'];
% NO MOTION RECONSTRUCTIONS
reconstructionsFolder = [PATHFOLDER, 'src/transferPlots/reconstructions/'];
% MAPS FOLDER
mapsFolder = [PATHFOLDER, 'src/transferPlots/maps/'];

% % % % % % % % % % % % % % % % 
% % % % % % % % % % LOAD DATA
% % % % % % % % % % % % % % % % 

% % % % % % DICTIONARY
dictionary = load([ dictionaryFolder, 'dictionaryMore.mat' ]); %All.mat' ]);
NTRs = dictionary.NTRs;
M    = dictionary.M;
Nx = 96; Ny = Nx;
dictionaryMRFNorm    = dictionary.dictionaryMRFNorm;
dictionaryMRFNorm2Ch = dictionary.dictionaryMRFNorm2Ch;
materialTuples = dictionary.materialTuples;
clear dictionary

% % % % % % IMAGE RECONSTRUCTIONS
imagesRec = load([ reconstructionsFolder, 'recNOMotion.mat' ]);
% Normalize the reconstructed images
[~, imagesBART_nomot] = normalizeSignals( ...
                    [ real(reshape(imagesRec.imagesBART_nomot, ...
                                   [Nx*Ny, NTRs])) ...
                      imag(reshape(imagesRec.imagesBART_nomot, ...
                                   [Nx*Ny, NTRs])) ]);
imagesBART_nomot = (reshape(imagesBART_nomot, [Nx, Ny, NTRs]));
% clear imagesRec

%%
% % % % Create GROUND TRUTH MAPS

% For the 96x96 phantom with 4145 spins
centres = [41,27 ; 56,27 ; ...
           27,41 ; 41,41 ; 56,41 ; 70,41 ; ...
           27,56 ; 41,56 ; 56,56 ; 70,56 ; ...
           41,69 ; 56,69 ; ...
           49,49 ]; % big circle
radii   = [zeros(1,12) + 4 , 32];

realMap = struct;
myPhantomT1Values = [500, 225, 335, 330, 1010, 1615, 675, ...
                     665, 840, 830, 1150, 1420, 1580];
myPhantomT2Values = [360,  50,  70, 110,  130,  370,  90, ...
                     130, 110, 150,  155,  190,  160];

[realMap.T1map, realMap.T2map, maskAllCircles, maskHollow] = ...
         func_createRealT1T2Maps(myPhantomT1Values, myPhantomT2Values, ...
                                 Nx, Ny, centres, radii);



% % DICTIONARY MATCHING FOR NO MOTION
imagesBARTReshaped_nomot = [ real(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ...
                             imag(reshape(imagesBART_nomot, [Nx*Ny, NTRs])) ];

            
% Do the match (complex values both)
fprintf(' -----> Start matching.\n');

[K, ~] = size(imagesBARTReshaped_nomot);

% % % PART 1
partOfPhantom = 1 : K/4;
tic;
% BART
[valuesOfMatchB1, indicesToMatchB1, matTuplesMatchedB1] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);

t=toc;
fprintf(' -----> Done with matching 1. Took %3.2f seconds\n', t);

% % % PART 2
partOfPhantom = K/4+1 : K/2;
tic;
% BART
[valuesOfMatchB2, indicesToMatchB2, matTuplesMatchedB2] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 2. Took %3.2f seconds\n', t);

% % % PART 3
partOfPhantom = K/2+1 : 3*K/4;
tic;
% BART
[valuesOfMatchB3, indicesToMatchB3, matTuplesMatchedB3] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 3. Took %3.2f seconds\n', t);

% % % PART 4
partOfPhantom = 3*K/4 + 1 : K;
tic;
% BART
[valuesOfMatchB4, indicesToMatchB4, matTuplesMatchedB4] = ...
                        matching(dictionaryMRFNorm2Ch, ...
                                 imagesBARTReshaped_nomot(partOfPhantom,:), ...
                                 materialTuples, 0);
t=toc;
fprintf(' -----> Done with matching 4. Took %3.2f seconds\n', t);

                   
% Merge results for BART:
resultsB_nomot = matTuplesMatchedB1;
resultsB_nomot.score  = [valuesOfMatchB1, valuesOfMatchB2, ...
                         valuesOfMatchB3, valuesOfMatchB4];
resultsB_nomot.scoremap = reshape(resultsB_nomot.score, [Nx, Ny]);                      
resultsB_nomot.indices = [indicesToMatchB1, indicesToMatchB2, ...
                    indicesToMatchB3, indicesToMatchB4]; 
resultsB_nomot.indicesmap = reshape(resultsB_nomot.indices, [Nx, Ny]); 
resultsB_nomot.T1 = [matTuplesMatchedB1.T1, matTuplesMatchedB2.T1, ...
               matTuplesMatchedB3.T1, matTuplesMatchedB4.T1];
resultsB_nomot.T1map = reshape(resultsB_nomot.T1, [Nx, Ny]);
resultsB_nomot.T2 = [matTuplesMatchedB1.T2, matTuplesMatchedB2.T2, ...
               matTuplesMatchedB3.T2, matTuplesMatchedB4.T2];
resultsB_nomot.T2map = reshape(resultsB_nomot.T2, [Nx, Ny]);                
   
% % % % Save results
save([mapsFolder, 'mapsNoMotionMore.mat'], 'resultsB_nomot');
return

%% % % % % Reconstructed maps of T1 and T2 and the real (ground truth) maps
padv = 0;

figure('units','normalized','outerposition',[0 0 1 1])
% T1 real
subplot(2,3,1)
imagesc(padarray((realMap.T1map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Real')

% T2 real
subplot(2,3,2)
imagesc(padarray((realMap.T2map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Real')

% BART - T1
subplot(2,3,4)
imagesc(padarray((resultsB_nomot.T1map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT1Values)])
axis off
title('T_1 Reconstructed')

% BART - T2
subplot(2,3,5)
imagesc(padarray((resultsB_nomot.T2map.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0, max(myPhantomT2Values)])
axis off
title('T_2 Reconstructed')

% BART - Score
subplot(2,3,6)   
imagesc(padarray((resultsB_nomot.scoremap.*maskAllCircles.*maskHollow),[padv,padv],0,'both'))
colormap(cm_magma) 
colorbar, axis square
caxis([0 1])
axis off
title('Matching Scores')


pause(2)

% % % Save figure
savefig([reconstructionsFolder, 'reconstructionsNOMotion.fig']);
resultsB_NOMOTIONMAPS = resultsB_nomot;


%% % % Mean Values for each block
% % T1
[ meanValuesReal.T1 , meanValuesSimu.T1, stdValuesSimu.T1 ] = ...
    func_retrieveMeanValueInROI(realMap.T1map, resultsB_nomot.T1map, ...
                            centres, [radii(1:end-1)-1, 4], 0);
                            %centres(1:end-1,:), radii(1:end-1)-1, 0);
% % T2
[ meanValuesReal.T2 , meanValuesSimu.T2, stdValuesSimu.T2 ] = ...
    func_retrieveMeanValueInROI(realMap.T2map, resultsB_nomot.T2map, ...
                            centres, [radii(1:end-1)-1, 4], 0);
                            %centres(1:end-1,:), radii(1:end-1)-1, 0);


% % Calculate R-squared for T1s and T2s
rsqT1 = fitlm(meanValuesReal.T1, meanValuesSimu.T1);
rsqT2 = fitlm(meanValuesReal.T2, meanValuesSimu.T2);

% % Plot real vs simulated values
                        
figure('units','normalized','outerposition',[0 0 1 1])

% T1 real vs T1 reconstructions
subplot(1,2,1); hold on
% Identity line
il = plot(0:max(meanValuesReal.T1)+100, ...
          0:max(meanValuesReal.T1)+100);
il.Color = [0,0,0,0.1];
% Fitted line
plot( (min(meanValuesReal.T1):max(meanValuesReal.T1)), ...
       rsqT1.Coefficients.Estimate(1) + ...
       rsqT1.Coefficients.Estimate(2).*(min(meanValuesReal.T1):max(meanValuesReal.T1)), 'k');
% Real vs simulated values
errorbar(meanValuesReal.T1 , meanValuesSimu.T1, stdValuesSimu.T1, ...
                             'o', 'MarkerFaceColor', 'red');
% Rsq statistics
text(200, 1400, ['R^2 = ', num2str(round(rsqT1.Rsquared.Adjusted,4))])
t1coef2 = rsqT1.Coefficients.Estimate(2);
t1coef1 = rsqT1.Coefficients.Estimate(1);
if t1coef1 < 0
    text(200, 1500, ['y = ', num2str(t1coef2,4), ...
                         'x - ', ...
                         num2str(abs(round(t1coef1,4)))]);
else
    text(200, 1500, ['y = ', num2str(t1coef2,4), ...
                         'x + ', ...
                         num2str(abs(round(t1coef1,4)))]);
end
axis equal, axis square
xlim([0 max(meanValuesReal.T1)+100])
ylim([0 max(meanValuesReal.T1)+100])
xlabel('Real T_1 (ms)'); ylabel('Simulated T_1 (ms)');
title('Ground truth T_1 vs simulated T_1')


% T2 real vs T2 reconstructions
subplot(1,2,2); hold on
% Identity line
il = plot(0:max(meanValuesReal.T2)+20, ...
          0:max(meanValuesReal.T2)+20);
il.Color = [0,0,0,0.1];
% Fitted line
plot( (min(meanValuesReal.T2):max(meanValuesReal.T2)), ...
       rsqT2.Coefficients.Estimate(1) + ...
       rsqT2.Coefficients.Estimate(2).*(min(meanValuesReal.T2):max(meanValuesReal.T2)), 'k');
% Real vs simulated values
errorbar(meanValuesReal.T2 , meanValuesSimu.T2, stdValuesSimu.T2, ...
                             'o', 'MarkerFaceColor', 'red');
% Rsq statistics
text(50, 325, ['R^2 = ', num2str(rsqT2.Rsquared.Adjusted)])
t2coef2 = rsqT2.Coefficients.Estimate(2);
t2coef1 = rsqT2.Coefficients.Estimate(1);
if t2coef1 < 0
    text(50, 350, ['y = ', num2str(t2coef2,4), ...
                         'x - ', ...
                         num2str(abs(round(t2coef1,4)))]);
else
    text(50, 350, ['y = ', num2str(t2coef2,4), ...
                         'x + ', ...
                         num2str(abs(round(t2coef1,4)))]);
end
axis equal, axis square
xlim([0 max(meanValuesReal.T2)+20])
ylim([0 max(meanValuesReal.T2)+20])
xlabel('Real T_2 (ms)'); ylabel('Simulated T_2 (ms)');
title('Ground truth T_2 vs simulated T_2')

rsqT1.Coefficients
rsqT2.Coefficients

pause(2)

% % % Save figure
savefig([reconstructionsFolder, 'avgROIsimVSGT_NOMotion.fig']);
save(['meanValues/', 'meanValsNOMOTION.mat'], ...
      'meanValuesReal' , 'meanValuesSimu', 'stdValuesSimu');


%% Have a look at individual signals NO MOTION

imagesBARTReshaped1Ch_nomot = reshape(imagesBART_nomot, [Nx*Ny, NTRs ]);

% Scores for the 96x96 phantom with 4145 spins
scoreIdxB = [(41-1)*Nx+49, ... %big circle
             (42-1)*Nx+27, ... %c1
             (56-1)*Nx+39, ... %c5
             (39-1)*Nx+56, ... %c8
             (57-1)*Nx+69, ... %c12
             (41-1)*Nx+42];    %c4
                       
scoreValB = resultsB_nomot.score(scoreIdxB);
% Titles
titles = {'bigCircle', 'c1', 'c5', 'c8', 'c12', 'c4'};

% Save these results to compare with motion cases
nomotionSignals = struct;

NTRStart =  1;
NTREnd   = NTRs;
for idxTR = 1:size(scoreIdxB,2)
    
    figure('Position',[1,1,700,1200])
    
    % The voxel we are looking for
    subplot(2,2,1)
    llPhantom = squeeze(abs(imagesBART_nomot(:,:,1)));
    llPhantom = llPhantom(:);
    llPhantom(scoreIdxB(idxTR)) = 0; 
    colormap hot; colorbar
    llPhantom = reshape(llPhantom, [Nx, Ny]);
    imagesc(llPhantom), axis square, colormap gray
    
    % ABS
    subplot(2,2,[3,4]); hold on
    % Plot dictionary signal
    dictionaryMatched = dictionaryMRFNorm( ...
             resultsB_nomot.indices(scoreIdxB(idxTR)), :);
    p1 = plot(NTRStart:NTREnd, ...
         abs(dictionaryMatched(NTRStart:NTREnd)), ...
         '-', 'LineWidth', 2);
    p1.Color = [0.8500    0.3250    0.0980];
    % Plot image signal
    imgSignal = imagesBARTReshaped1Ch_nomot(...
                 scoreIdxB(idxTR), :);
    p2 = plot(NTRStart:NTREnd, abs(imgSignal(NTRStart:NTREnd)), ...
          '--', 'LineWidth', 1);
    p2.Color = [0.3010    0.7450    0.9330];  
    legend('dictionary', 'phantom')
    xlabel('#image'); ylabel('normalised signal intensity (a.u.)');
    title({['BART (abs) ',  titles{idxTR}, ...
            ' Score = ', num2str(scoreValB(idxTR))], ...
           ['T_1 = ', num2str(resultsB_nomot.T1(scoreIdxB(idxTR))), ...
           ' T_2 = ', num2str(resultsB_nomot.T2(scoreIdxB(idxTR)))]})
    
    
    pause(2)

    % % % Save values for comparison to motion cases
    nomotionSignals.(titles{idxTR}).dictMatch = dictionaryMatched;
    nomotionSignals.(titles{idxTR}).imgSignal = imgSignal;
    nomotionSignals.(titles{idxTR}).T1matched = resultsB_nomot.T1(scoreIdxB(idxTR));
    nomotionSignals.(titles{idxTR}).T2matched = resultsB_nomot.T2(scoreIdxB(idxTR));
    
    % % % Save figure
    savefig([reconstructionsFolder, ...
             'sigCompare_', titles{idxTR}, '_NOMotion.fig']);
         
	pause(2)
    
% % % %     % BY CHANNELS
% % % %     subplot(4,2,[5,6])
% % % %     plot(1:2*NTRs, dictionaryMRFNorm2Ch(resultsB_nomot.indices(scoreIdxB(idxTR)), :), '.--')
% % % %     hold on
% % % %     plot(1:2*NTRs, imagesBARTReshaped_nomot(scoreIdxB(idxTR), :), '.-')
% % % %     legend('dictionary', 'image')
% % % %     title(['BART (by channels) ',  titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
% % % % 
% % % %     % DIFFERENCE
% % % %     subplot(4,2,[7,8])
% % % %     plot(1:2*NTRs, ...
% % % %          dictionaryMRFNorm2Ch(resultsB_nomot.indices(scoreIdxB(idxTR)), :) - ...
% % % %          imagesBARTReshaped_nomot(scoreIdxB(idxTR), :), '.-')
% % % %     title(['BART (by channels) difference ',  ...
% % % %         titles{idxTR}, ' Score = ', num2str(scoreValB(idxTR))])
% % % %     

end


% % % Save the results of matching
save([reconstructionsFolder, 'signalsNOMotion.mat'], 'nomotionSignals');


% % % % % % % % % % % 
% % % % % % % % % % % %%
% % % % % % % % % % % % % % % % Difference maps of T1 and T2 between reconstructed and GT
% % % % % % % % % % % 
% % % % % % % % % % % figure
% % % % % % % % % % % % BART - T1
% % % % % % % % % % % subplot(1,2,1)
% % % % % % % % % % % imagesc((resultsB_nomot.T1map-realMap.T1map) .* maskAllCircles .* maskHollow)
% % % % % % % % % % % colormap hot; colorbar, axis square, axis off
% % % % % % % % % % % title('T_1 Reconstructed - T_1 Ground Truth')
% % % % % % % % % % % caxis([-500 500])
% % % % % % % % % % % 
% % % % % % % % % % % % BART - T2
% % % % % % % % % % % subplot(1,2,2)
% % % % % % % % % % % imagesc((resultsB_nomot.T2map-realMap.T2map) .* maskAllCircles .* maskHollow)
% % % % % % % % % % % colormap hot; colorbar, axis square, axis off
% % % % % % % % % % % title('T_2 Reconstructed - T_2 Ground Truth')
% % % % % % % % % % % caxis([-150 150])
% % % % % % % % % % % 
% % % % % % % % % % % 
% % % % % % % % % % % 
% % % % % % % % % % % 
% % % % % % % % % % % %%
% % % % % % % % % % % % % % % % PERCENTAGE ERROR NO MOTION
% % % % % % % % % % % figure
% % % % % % % % % % % % BART - T1
% % % % % % % % % % % subplot(1,2,1)
% % % % % % % % % % % normMatrix = realMap.T1map; normMatrix(normMatrix==0) = 1;
% % % % % % % % % % % imagesc(((abs((resultsB_nomot.T1map-realMap.T1map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
% % % % % % % % % % % colormap hot; colorbar, axis square, axis off
% % % % % % % % % % % title('PercErr(T_1)')
% % % % % % % % % % % 
% % % % % % % % % % % % BART - T2
% % % % % % % % % % % subplot(1,2,2)
% % % % % % % % % % % normMatrix = realMap.T2map; normMatrix(normMatrix==0) = 1;
% % % % % % % % % % % imagesc(((abs((resultsB_nomot.T2map-realMap.T2map)./normMatrix)).* 100) .* maskAllCircles .* maskHollow)
% % % % % % % % % % % colormap hot; colorbar, axis square, axis off
% % % % % % % % % % % title('PercErr(T_2)')
% % % % % % % % % % % 
