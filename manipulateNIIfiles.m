% IRINA GRIGORESCU
% Script to manipulate NII files

% Prerequisites
addpath ~/Tools/MatlabStuff/matlabnifti/

%%
% Load file:
filename = ['/Users/irina/OneDrive - University College London/Work/', ...
            'MRF/MaryiaDonevaMRF_ForUCL/NIFTI/', ...
            'Fipri_2D_best_slen500_cart_full_1x1x3mm_fp_.nii.gz'];
        
iniData = load_nii(filename, ...
              [], [], [], [], [], 0.5);
              
%%
% Get information about the file 
dataSize = size(iniData.img);

%%
% Create absolute value image
dataReal = iniData.img(:,:,:,  1:500);
dataImag = iniData.img(:,:,:,501:end);
dataAbs  = sqrt(dataReal.^2 + dataImag.^2);
maxAbs   = max(max(max(max(dataAbs))));

%%
% Plot absolute value image
figure
montage(dataAbs(:,:,1,1:N)./maxAbs)


%% 
% Save data abs
outputData = iniData;
outputData.img = dataAbs;
outputData.hdr.dime.dim(5) = 500;
outputData.original.hdr.dime.dim(5) = 500;
filename = ['/Users/irina/OneDrive - University College London/', ...
            'Work/MRF/MaryiaDonevaMRF_ForUCL/NIFTI_rec/mrfabs'];
outputData.fileprefix = filename;

save_nii(outputData, [filename, '.nii.gz']);





